package com.rsmurniteguh.webservice.dep.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GetPatientData {

	public Patient getPatientData(String CardNo)
	{
		Patient p = new Patient();
		
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

		} catch (ClassNotFoundException e) {

			e.printStackTrace();
			return null;

		}

		System.out.println("Oracle JDBC Driver Registered!");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:oracle:thin:@192.168.1.254:1521:DB2015", "ihis",
					"ihis");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return null;

		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
		
		 Statement stmt = null;
		    String query =
		        "SELECT PS.PERSON_ID, "+
			       "PT.PATIENT_ID, "+
			       "CD.CARD_NO, "+
			       "PS.PERSON_NAME, "+
			       "pgCOMMON.fxGetCodeDesc(PS.SEX) as SEX, "+
			       "TRUNC((TO_NUMBER(TO_CHAR(SYSDATE, 'yyyymmdd')) - "+
			       "      TO_NUMBER(TO_CHAR(PS.BIRTH_DATE, 'yyyymmdd'))) / 10000) || "+
			       "' Tahun ' || "+
			       "TRUNC(months_between(sysdate, PS.BIRTH_DATE) - "+
			       "      12 * trunc(months_between(sysdate, PS.BIRTH_DATE) / 12)) || "+
			       "' Bulan' AGE, "+
			       "PS.BIRTH_DATE, "+
			       "PS.STAKEHOLDER_ID "+
			  "FROM PERSON PS, PATIENT PT, CARD CD "+
			 "WHERE PT.PERSON_ID = PS.PERSON_ID "+
			   "AND CD.PERSON_ID = PS.PERSON_ID "+
			   "AND CD.CARD_NO = '"+CardNo+"'";

		    try {
		        stmt = connection.createStatement();
		        ResultSet rs = stmt.executeQuery(query);
		        while (rs.next()) {
		        	p.setPersonId(rs.getBigDecimal("PERSON_ID"));
		        	p.setPersonName(rs.getString("PERSON_NAME"));
		        	p.setPatientId(rs.getBigDecimal("PATIENT_ID"));
		        	p.setSex(rs.getString("SEX"));
		        	p.setCardNo(rs.getString("CARD_NO"));
		        	p.setAge(rs.getString("AGE"));
		        	p.setBirthDate(rs.getDate("BIRTH_DATE"));
		        	p.setStakeholderId(rs.getBigDecimal("STAKEHOLDER_ID"));
		        }
		    } catch (SQLException e ) {
		        System.out.println(e.getMessage());
		    } finally {
		        if (stmt != null) { try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} }
		    }
		    
		    return p;
	}
	
}
