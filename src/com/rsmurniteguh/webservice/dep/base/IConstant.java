package com.rsmurniteguh.webservice.dep.base;

import java.math.BigDecimal;

public interface IConstant {
	String IND_YES = "Y";
	String IND_Y = "Y";
	
	String IND_NO = "N";
	String IND_N = "N";
	
	String STATUS_CONNECTED = "CONNECTED";
	String STATUS_SUCCESSFULL = "SUCCESSFULL";
	String STATUS_ERROR = "ERROR";
	
	BigDecimal WS_USER = BigDecimal.ZERO;
	
	BigDecimal LOCATIONMSTR_OPCASHIER = new BigDecimal("311097397");
	
	String OP = "OP";
	String MRT = "MRT";
	
	BigDecimal COMPANY_SELFPAY = new BigDecimal("410187518");
}
