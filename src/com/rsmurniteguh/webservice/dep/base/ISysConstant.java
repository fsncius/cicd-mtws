package com.rsmurniteguh.webservice.dep.base;

public interface ISysConstant {
	public static final String LANGUAGE_EN = "en_US";
	public static final String LANGUAGE_CN = "zh_CN";
	public static final String LANGUAGE_TW = "zh_TW";
    
	public static final String REGISTRY_LOCATION = "SOFTWARE\\WEBSERVICE";
	public static final String REGISTRY_NAME = "Production";
	public static final String ENV_PRODUCTION = "1";
	
	public static final String LAB_FILE_LOCATION = "http://192.168.1.203/lis/"; ///1708300109.pdf";
	public static final String SEP_FILE_LOCATION = "\\\\192.168.222.101\\kthis_eclaim_upload\\";
	
	
	public static final String ECLAIM_REGISTRY_TYPE_FINAL = "FINAL";
	public static final String ECLAIM_REGISTRY_TYPE_TEMPORARY = "TEMPORARY";
	
	public static final String FORMAT_RECEIVE_DATE = "yyyyMMdd";
}
