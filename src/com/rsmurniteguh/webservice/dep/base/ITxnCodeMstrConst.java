package com.rsmurniteguh.webservice.dep.base;

public interface ITxnCodeMstrConst {
    String TXN_CODE_DSY = "DSY";
    String TXN_CODE_ZCY = "ZCY";
    String TXN_CODE_CDDEP = "CDDEP";
    String TXN_CODE_CDRFD = "CDRFD";
    String TXN_CODE_HMCCHG = "HMCCHG";
    String TXN_CODE_NONE = "NONE";
}
