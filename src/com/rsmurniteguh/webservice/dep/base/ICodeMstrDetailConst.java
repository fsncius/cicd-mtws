package com.rsmurniteguh.webservice.dep.base;

public interface ICodeMstrDetailConst {
    String CDT_HMC = ICodeMstrConst.CDT + "HMC";
    
    String CDS_VLD = ICodeMstrConst.CDS + "VLD";
    
    String STK_1 = ICodeMstrConst.STK + "1";
    
    String ACS_1 = ICodeMstrConst.ACS + "1";
    
    String PLC_1 = ICodeMstrConst.PLC + "1";
    
    String CCS_1 = ICodeMstrConst.CCS + "1";
    
    String FCC_1 = ICodeMstrConst.FCC + "1";
    
    String PYM_1 = ICodeMstrConst.PYM + "1";
    
    String CTY_13 = ICodeMstrConst.CTY + "13";
    
    String RCS_2 = ICodeMstrConst.RCS + "2";
    String RCS_3 = ICodeMstrConst.RCS + "3";
    
    String RTY_1 = ICodeMstrConst.RTY + "1";
    
    String RCC_CD = ICodeMstrConst.RCC + "CD";
    
    String ADR_PRE = ICodeMstrConst.ADR + "PRE";
    
    String ADR_RES = ICodeMstrConst.ADR + "RES";
    
    String PTC_114 = ICodeMstrConst.PTC + "114";
}
