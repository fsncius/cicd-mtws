package com.rsmurniteguh.webservice.dep.base;

public interface ISequenceNoConst {
	String MRN_OP = "MRN_OP";
	String PATIENT_NO = "PATIENT_NO";
	String USR_RECEIPT_NO = "USR_RECEIPT_NO";
	String SYS_RECEIPT_NO = "SYS_RECEIPT_NO";
	String TXN_NO_PAT = "TXN_NO_PAT";
	String CARD_NO = "CARD_NO";
}