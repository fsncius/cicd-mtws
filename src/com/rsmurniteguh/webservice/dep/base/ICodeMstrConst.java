package com.rsmurniteguh.webservice.dep.base;

public interface ICodeMstrConst {
	String IIT = "IIT";
	String RAC = "RAC";
	String ETC = "ETC";
	String PLE = "PLE";
	
    String CDT = "CDT";
    String CDS = "CDS";
    
    String STK = "STK";
    
    String ACS = "ACS";
    
    String PLC = "PLC";
    
    String CCS = "CCS";
    
    String FCC = "FCC";
    
    String PYM = "PYM";
    
    String CTY = "CTY";
    
    String RCS = "RCS";
    
    String RTY = "RTY";
    
    String RCC = "RCC";
    
    String ADR = "ADR";
    
    String PTC = "PTC";
}
