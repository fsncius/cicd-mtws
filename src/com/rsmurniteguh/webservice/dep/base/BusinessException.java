package com.rsmurniteguh.webservice.dep.base;

public class BusinessException extends Exception {
	public BusinessException() {
        super();
    }

	public BusinessException(String message) {
        super(message);
    }
}
