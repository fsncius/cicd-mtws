package com.rsmurniteguh.webservice.dep.all.model;

import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Metadata;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInfoResponse.Response;

public class InacbgResponse {
	private Metadata metadata;
	private Response response;
	
	public Metadata getMetadata() {
		return metadata;
	}
	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}
	public Response getResponse() {
		return response;
	}
	public void setResponse(Response response) {
		this.response = response;
	}
	
	

}
