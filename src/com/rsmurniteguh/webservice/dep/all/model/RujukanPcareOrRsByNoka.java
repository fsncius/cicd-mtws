package com.rsmurniteguh.webservice.dep.all.model;

import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Metadata;

public class RujukanPcareOrRsByNoka {

	private Metadata metaData;
	private Response response;
	
	public static RujukanPcareOrRsByNoka getDefault() {
		RujukanPcareOrRsByNoka result = new RujukanPcareOrRsByNoka();
		return result;
	}
	
	public RujukanPcareOrRsByNoka() {
		metaData = new Metadata();
		response = new Response();
	}
	
	public Metadata getMetaData() {
		return metaData;
	}


	public void setMetaData(Metadata metaData) {
		this.metaData = metaData;
	}


	public Response getResponse() {
		return response;
	}


	public void setResponse(Response response) {
		this.response = response;
	}


	public class Response{
		private Rujukan rujukan;
		
		public Response() {
			rujukan = new Rujukan();
		}

	    public Rujukan getRujukan() { return rujukan; }
	    public void setRujukan(Rujukan value) { this.rujukan = value; }
	}
	
	public class Rujukan {
	    private Info diagnosa;
	    private String keluhan;
	    private String noKunjungan;
	    private Info pelayanan;
	    private Peserta peserta;
	    private Info poliRujukan;
	    private Info provPerujuk;
	    private String tglKunjungan;
		private String faskes;
	    
	    public Rujukan() {
	    	diagnosa = new Info();
	    	pelayanan = new Info();
	    	poliRujukan = new Info();
	    	provPerujuk = new Info();
	    }

	    public Info getDiagnosa() { return diagnosa; }
	    public void setDiagnosa(Info value) { this.diagnosa = value; }

	    public String getKeluhan() { return keluhan; }
	    public void setKeluhan(String value) { this.keluhan = value; }

	    public String getNoKunjungan() { return noKunjungan; }
	    public void setNoKunjungan(String value) { this.noKunjungan = value; }

	    public Info getPelayanan() { return pelayanan; }
	    public void setPelayanan(Info value) { this.pelayanan = value; }

	    public Peserta getPeserta() { return peserta; }
	    public void setPeserta(Peserta value) { this.peserta = value; }

	    public Info getPoliRujukan() { return poliRujukan; }
	    public void setPoliRujukan(Info value) { this.poliRujukan = value; }

	    public Info getProvPerujuk() { return provPerujuk; }
	    public void setProvPerujuk(Info value) { this.provPerujuk = value; }

	    public String getTglKunjungan() { return tglKunjungan; }
	    public void setTglKunjungan(String value) { this.tglKunjungan = value; }

		public String getFaskes() {
			return faskes;
		}

		public void setFaskes(String faskes) {
			this.faskes = faskes;
		}
	}
	
	public class Info{
		private String kode;
	    private String nama;

	    public String getKode() { return kode; }
	    public void setKode(String value) { this.kode = value; }

	    public String getNama() { return nama; }
	    public void setNama(String value) { this.nama = value; }
	}
	
	public class Peserta {
	    private Cob cob;
	    private HakKelas hakKelas;
	    private Informasi informasi;
	    private HakKelas jenisPeserta;
	    private Mr mr;
	    private String nama;
	    private String nik;
	    private String noKartu;
	    private String pisa;
	    private ProvUmum provUmum;
	    private String sex;
	    private HakKelas statusPeserta;
	    private String tglCetakKartu;
	    private String tglLahir;
	    private String tglTAT;
	    private String tglTMT;
	    private Umur umur;

	    public Cob getCob() { return cob; }
	    public void setCob(Cob value) { this.cob = value; }

	    public HakKelas getHakKelas() { return hakKelas; }
	    public void setHakKelas(HakKelas value) { this.hakKelas = value; }

	    public Informasi getInformasi() { return informasi; }
	    public void setInformasi(Informasi value) { this.informasi = value; }

	    public HakKelas getJenisPeserta() { return jenisPeserta; }
	    public void setJenisPeserta(HakKelas value) { this.jenisPeserta = value; }

	    public Mr getMr() { return mr; }
	    public void setMr(Mr value) { this.mr = value; }

	    public String getNama() { return nama; }
	    public void setNama(String value) { this.nama = value; }

	    public String getNik() { return nik; }
	    public void setNik(String value) { this.nik = value; }

	    public String getNoKartu() { return noKartu; }
	    public void setNoKartu(String value) { this.noKartu = value; }

	    public String getPisa() { return pisa; }
	    public void setPisa(String value) { this.pisa = value; }

	    public ProvUmum getProvUmum() { return provUmum; }
	    public void setProvUmum(ProvUmum value) { this.provUmum = value; }

	    public String getSex() { return sex; }
	    public void setSex(String value) { this.sex = value; }

	    public HakKelas getStatusPeserta() { return statusPeserta; }
	    public void setStatusPeserta(HakKelas value) { this.statusPeserta = value; }

	    public String getTglCetakKartu() { return tglCetakKartu; }
	    public void setTglCetakKartu(String value) { this.tglCetakKartu = value; }

	    public String getTglLahir() { return tglLahir; }
	    public void setTglLahir(String value) { this.tglLahir = value; }

	    public String getTglTAT() { return tglTAT; }
	    public void setTglTAT(String value) { this.tglTAT = value; }

	    public String getTglTMT() { return tglTMT; }
	    public void setTglTMT(String value) { this.tglTMT = value; }

	    public Umur getUmur() { return umur; }
	    public void setUmur(Umur value) { this.umur = value; }
	}

	public class Cob {
	    private String nmAsuransi;
	    private String noAsuransi;
	    private String tglTAT;
	    private String tglTMT;

	    public String getNmAsuransi() { return nmAsuransi; }
	    public void setNmAsuransi(String value) { this.nmAsuransi = value; }

	    public String getNoAsuransi() { return noAsuransi; }
	    public void setNoAsuransi(String value) { this.noAsuransi = value; }

	    public String getTglTAT() { return tglTAT; }
	    public void setTglTAT(String value) { this.tglTAT = value; }

	    public String getTglTMT() { return tglTMT; }
	    public void setTglTMT(String value) { this.tglTMT = value; }
	}

	public class HakKelas {
	    private String keterangan;
	    private String kode;

	    public String getKeterangan() { return keterangan; }
	    public void setKeterangan(String value) { this.keterangan = value; }

	    public String getKode() { return kode; }
	    public void setKode(String value) { this.kode = value; }
	}

	public class Informasi {
	    private String dinsos;
	    private String noSKTM;
	    private String prolanisPRB;

	    public String getDinsos() { return dinsos; }
	    public void setDinsos(String value) { this.dinsos = value; }

	    public String getNoSKTM() { return noSKTM; }
	    public void setNoSKTM(String value) { this.noSKTM = value; }

	    public String getProlanisPRB() { return prolanisPRB; }
	    public void setProlanisPRB(String value) { this.prolanisPRB = value; }
	}


	public class Mr {
	    private String noMR;
	    private String noTelepon;

	    public String getNoMR() { return noMR; }
	    public void setNoMR(String value) { this.noMR = value; }

	    public String getNoTelepon() { return noTelepon; }
	    public void setNoTelepon(String value) { this.noTelepon = value; }
	}

	public class ProvUmum {
	    private String kdProvider;
	    private String nmProvider;

	    public String getKdProvider() { return kdProvider; }
	    public void setKdProvider(String value) { this.kdProvider = value; }

	    public String getNmProvider() { return nmProvider; }
	    public void setNmProvider(String value) { this.nmProvider = value; }
	}


	public class Umur {
	    private String umurSaatPelayanan;
	    private String umurSekarang;

	    public String getUmurSaatPelayanan() { return umurSaatPelayanan; }
	    public void setUmurSaatPelayanan(String value) { this.umurSaatPelayanan = value; }

	    public String getUmurSekarang() { return umurSekarang; }
	    public void setUmurSekarang(String value) { this.umurSekarang = value; }
	}
}
