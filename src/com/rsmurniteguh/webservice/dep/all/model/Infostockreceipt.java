package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class Infostockreceipt {
	private String INVOICE_NO;
	private String VENDOR_NAME;
	private String MATERIAL_ITEM_NAME;
	private String RECEIPT_QTY_OF_BASE_UOM;
	private String BASE_UOM;
	private String totalWHOLESALE_PRICEperunit;
	private String UNIT_PRICE;
	private String DISC_UNIT;
	private String PPN_RATE;
	private String TOTAL_PRICE;
	private BigDecimal totaldiscountperunit;
	private BigDecimal totaldiscountkeseluruhan;
	private BigDecimal totalWHOLESALE_PRICEkeseluruhan;
	private BigDecimal totalppnkeseluruhan;
	
		
	public BigDecimal getTotalppnkeseluruhan() {
		return totalppnkeseluruhan;
	}
	public void setTotalppnkeseluruhan(BigDecimal totalppnkeseluruhan) {
		this.totalppnkeseluruhan = totalppnkeseluruhan;
	}
	public BigDecimal getTotalWHOLESALE_PRICEkeseluruhan() {
		return totalWHOLESALE_PRICEkeseluruhan;
	}
	public void setTotalWHOLESALE_PRICEkeseluruhan(BigDecimal totalWHOLESALE_PRICEkeseluruhan) {
		this.totalWHOLESALE_PRICEkeseluruhan = totalWHOLESALE_PRICEkeseluruhan;
	}
	public BigDecimal getTotaldiscountperunit() {
		return totaldiscountperunit;
	}
	public void setTotaldiscountperunit(BigDecimal totaldiscountperunit) {
		this.totaldiscountperunit = totaldiscountperunit;
	}
	public BigDecimal getTotaldiscountkeseluruhan() {
		return totaldiscountkeseluruhan;
	}
	public void setTotaldiscountkeseluruhan(BigDecimal totaldiscountkeseluruhan) {
		this.totaldiscountkeseluruhan = totaldiscountkeseluruhan;
	}
	public String getINVOICE_NO() {
		return INVOICE_NO;
	}
	public void setINVOICE_NO(String iNVOICE_NO) {
		INVOICE_NO = iNVOICE_NO;
	}
	public String getVENDOR_NAME() {
		return VENDOR_NAME;
	}
	public void setVENDOR_NAME(String vENDOR_NAME) {
		VENDOR_NAME = vENDOR_NAME;
	}
	public String getMATERIAL_ITEM_NAME() {
		return MATERIAL_ITEM_NAME;
	}
	public void setMATERIAL_ITEM_NAME(String mATERIAL_ITEM_NAME) {
		MATERIAL_ITEM_NAME = mATERIAL_ITEM_NAME;
	}
	public String getRECEIPT_QTY_OF_BASE_UOM() {
		return RECEIPT_QTY_OF_BASE_UOM;
	}
	public void setRECEIPT_QTY_OF_BASE_UOM(String rECEIPT_QTY_OF_BASE_UOM) {
		RECEIPT_QTY_OF_BASE_UOM = rECEIPT_QTY_OF_BASE_UOM;
	}
	public String getBASE_UOM() {
		return BASE_UOM;
	}
	public void setBASE_UOM(String bASE_UOM) {
		BASE_UOM = bASE_UOM;
	}
	
	public String getTotalWHOLESALE_PRICEperunit() {
		return totalWHOLESALE_PRICEperunit;
	}
	public void setTotalWHOLESALE_PRICEperunit(String totalWHOLESALE_PRICEperunit) {
		this.totalWHOLESALE_PRICEperunit = totalWHOLESALE_PRICEperunit;
	}
	public String getUNIT_PRICE() {
		return UNIT_PRICE;
	}
	public void setUNIT_PRICE(String uNIT_PRICE) {
		UNIT_PRICE = uNIT_PRICE;
	}
	public String getDISC_UNIT() {
		return DISC_UNIT;
	}
	public void setDISC_UNIT(String dISC_UNIT) {
		DISC_UNIT = dISC_UNIT;
	}
	public String getPPN_RATE() {
		return PPN_RATE;
	}
	public void setPPN_RATE(String pPN_RATE) {
		PPN_RATE = pPN_RATE;
	}
	public String getTOTAL_PRICE() {
		return TOTAL_PRICE;
	}
	public void setTOTAL_PRICE(String tOTAL_PRICE) {
		TOTAL_PRICE = tOTAL_PRICE;
	}
	
}
