package com.rsmurniteguh.webservice.dep.all.model;

public class ListInfoData {
	
	private String PERSON_NAME;
	private String SEX;
	private String BIRTH_DATE;
	private String ID_NO;
	private String MOBILE_PHONE_NO;
	private String COMPANY_NAME;
	private String NIK;
	private String BPJS_NO;
	private String PASIEN_CLASS;
	private String VISIT_ID;
	private String ADMISSION_DATETIME;
	private String DISCHARGE_DATETIME;
	
	private String PERSON_ID;
	
	
	
	public String getPERSON_ID() {
		return PERSON_ID;
	}
	public void setPERSON_ID(String pERSON_ID) {
		PERSON_ID = pERSON_ID;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getSEX() {
		return SEX;
	}
	public void setSEX(String sEX) {
		SEX = sEX;
	}
	public String getBIRTH_DATE() {
		return BIRTH_DATE;
	}
	public void setBIRTH_DATE(String bIRTH_DATE) {
		BIRTH_DATE = bIRTH_DATE;
	}
	public String getID_NO() {
		return ID_NO;
	}
	public void setID_NO(String iD_NO) {
		ID_NO = iD_NO;
	}
	public String getMOBILE_PHONE_NO() {
		return MOBILE_PHONE_NO;
	}
	public void setMOBILE_PHONE_NO(String mOBILE_PHONE_NO) {
		MOBILE_PHONE_NO = mOBILE_PHONE_NO;
	}
	public String getCOMPANY_NAME() {
		return COMPANY_NAME;
	}
	public void setCOMPANY_NAME(String cOMPANY_NAME) {
		COMPANY_NAME = cOMPANY_NAME;
	}
	public String getNIK() {
		return NIK;
	}
	public void setNIK(String nIK) {
		NIK = nIK;
	}
	public String getBPJS_NO() {
		return BPJS_NO;
	}
	public void setBPJS_NO(String bPJS_NO) {
		BPJS_NO = bPJS_NO;
	}
	public String getPASIEN_CLASS() {
		return PASIEN_CLASS;
	}
	public void setPASIEN_CLASS(String pASIEN_CLASS) {
		PASIEN_CLASS = pASIEN_CLASS;
	}
	public String getVISIT_ID() {
		return VISIT_ID;
	}
	public void setVISIT_ID(String vISIT_ID) {
		VISIT_ID = vISIT_ID;
	}
	public String getADMISSION_DATETIME() {
		return ADMISSION_DATETIME;
	}
	public void setADMISSION_DATETIME(String aDMISSION_DATETIME) {
		ADMISSION_DATETIME = aDMISSION_DATETIME;
	}
	public String getDISCHARGE_DATETIME() {
		return DISCHARGE_DATETIME;
	}
	public void setDISCHARGE_DATETIME(String dISCHARGE_DATETIME) {
		DISCHARGE_DATETIME = dISCHARGE_DATETIME;
	}
	
}
