package com.rsmurniteguh.webservice.dep.all.model;

public class ListQueueBpjs {
	private String queue_no;

	public String getQueue_no() {
		return queue_no;
	}

	public void setQueue_no(String queue_no) {
		this.queue_no = queue_no;
	}
	
}
