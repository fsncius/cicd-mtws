package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class PaymentModel {

	private String accountbalance_id;
	private String cardNo;
	private String nfcNo;
	private String cardName;
//	public BigDecimal trxType;
	private String invoiceNo;
	private Double trxAmount;
	private String userId;
	private String paymentNo;
	private String pin;
	private String nik;
	private String statuskr;
	private String statusperusahaan;
	private String userdetail;
	private Boolean otherCashier;
	
	public String getStatusperusahaan() {
		return statusperusahaan;
	}
	public void setStatusperusahaan(String statusperusahaan) {
		this.statusperusahaan = statusperusahaan;
	}
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getStatuskr() {
		return statuskr;
	}
	public void setStatuskr(String statuskr) {
		this.statuskr = statuskr;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getCardName() {
		return cardName;
	}
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public Double getTrxAmount() {
		return trxAmount;
	}
	public void setTrxAmount(Double trxAmount) {
		this.trxAmount = trxAmount;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPaymentNo() {
		return paymentNo;
	}
	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getNfcNo() {
		return nfcNo;
	}
	public void setNfcNo(String nfcNo) {
		this.nfcNo = nfcNo;
	}
	public String getAccountbalance_id() {
		return accountbalance_id;
	}
	public void setAccountbalance_id(String accountbalance_id) {
		this.accountbalance_id = accountbalance_id;
	}
	public String getUserdetail() {
		return userdetail;
	}
	public void setUserdetail(String userdetail) {
		this.userdetail = userdetail;
	}
	
	
	
	
}
