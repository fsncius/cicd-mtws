package com.rsmurniteguh.webservice.dep.all.model;

public class NamaRadiologi {
	public String ORDERITEMMSTR_ID;
	public String TXNCODEMSTR_ID;
	public String TXN_CODE;
	public String TXN_DESC;
	public String TXN_DESC_LANG1;
	public String SHORT_CODE;
	public String PARENT_TXNCODEMSTR_ID;
	public String ITEM_TYPE_DRUG;
	public String ITEM_CAT_DESC;
	public String ITEM_CAT_DESC_LANG1;
	public String ITEM_CAT_CODE;
	public String ITEM_TYPE;
	public String ITEM_SUBCAT_CODE;
	public String ITEM_SUBCAT_DESC;
	public String ITEM_SUBCAT_DESC_LANG1;
	public String UNIT_PRICE;
	public String PRICE_UOM;
	public String CIM_TXN_CODE;
	public String CHARGEITEMMSTR_ID;
	public String PRICE;
	public String UNIT;
	public String UNIT_DESC;
	public String ITEMTYPE_DESC_NORMAL;

	public String getTXNCODEMSTR_ID() {
		return TXNCODEMSTR_ID;
	}

	public void setTXNCODEMSTR_ID(String tXNCODEMSTR_ID) {
		TXNCODEMSTR_ID = tXNCODEMSTR_ID;
	}

	public String getTXN_CODE() {
		return TXN_CODE;
	}

	public void setTXN_CODE(String tXN_CODE) {
		TXN_CODE = tXN_CODE;
	}

	public String getTXN_DESC() {
		return TXN_DESC;
	}

	public void setTXN_DESC(String tXN_DESC) {
		TXN_DESC = tXN_DESC;
	}

	public String getTXN_DESC_LANG1() {
		return TXN_DESC_LANG1;
	}

	public void setTXN_DESC_LANG1(String tXN_DESC_LANG1) {
		TXN_DESC_LANG1 = tXN_DESC_LANG1;
	}

	public String getSHORT_CODE() {
		return SHORT_CODE;
	}

	public void setSHORT_CODE(String sHORT_CODE) {
		SHORT_CODE = sHORT_CODE;
	}

	public String getPARENT_TXNCODEMSTR_ID() {
		return PARENT_TXNCODEMSTR_ID;
	}

	public void setPARENT_TXNCODEMSTR_ID(String pARENT_TXNCODEMSTR_ID) {
		PARENT_TXNCODEMSTR_ID = pARENT_TXNCODEMSTR_ID;
	}

	public String getITEM_TYPE_DRUG() {
		return ITEM_TYPE_DRUG;
	}

	public void setITEM_TYPE_DRUG(String iTEM_TYPE_DRUG) {
		ITEM_TYPE_DRUG = iTEM_TYPE_DRUG;
	}

	public String getITEM_CAT_DESC() {
		return ITEM_CAT_DESC;
	}

	public void setITEM_CAT_DESC(String iTEM_CAT_DESC) {
		ITEM_CAT_DESC = iTEM_CAT_DESC;
	}

	public String getITEM_CAT_DESC_LANG1() {
		return ITEM_CAT_DESC_LANG1;
	}

	public void setITEM_CAT_DESC_LANG1(String iTEM_CAT_DESC_LANG1) {
		ITEM_CAT_DESC_LANG1 = iTEM_CAT_DESC_LANG1;
	}

	public String getITEM_CAT_CODE() {
		return ITEM_CAT_CODE;
	}

	public void setITEM_CAT_CODE(String iTEM_CAT_CODE) {
		ITEM_CAT_CODE = iTEM_CAT_CODE;
	}

	public String getITEM_TYPE() {
		return ITEM_TYPE;
	}

	public void setITEM_TYPE(String iTEM_TYPE) {
		ITEM_TYPE = iTEM_TYPE;
	}

	public String getITEM_SUBCAT_CODE() {
		return ITEM_SUBCAT_CODE;
	}

	public void setITEM_SUBCAT_CODE(String iTEM_SUBCAT_CODE) {
		ITEM_SUBCAT_CODE = iTEM_SUBCAT_CODE;
	}

	public String getITEM_SUBCAT_DESC() {
		return ITEM_SUBCAT_DESC;
	}

	public void setITEM_SUBCAT_DESC(String iTEM_SUBCAT_DESC) {
		ITEM_SUBCAT_DESC = iTEM_SUBCAT_DESC;
	}

	public String getITEM_SUBCAT_DESC_LANG1() {
		return ITEM_SUBCAT_DESC_LANG1;
	}

	public void setITEM_SUBCAT_DESC_LANG1(String iTEM_SUBCAT_DESC_LANG1) {
		ITEM_SUBCAT_DESC_LANG1 = iTEM_SUBCAT_DESC_LANG1;
	}

	public String getUNIT_PRICE() {
		return UNIT_PRICE;
	}

	public void setUNIT_PRICE(String uNIT_PRICE) {
		UNIT_PRICE = uNIT_PRICE;
	}

	public String getPRICE_UOM() {
		return PRICE_UOM;
	}

	public void setPRICE_UOM(String pRICE_UOM) {
		PRICE_UOM = pRICE_UOM;
	}

	public String getCIM_TXN_CODE() {
		return CIM_TXN_CODE;
	}

	public void setCIM_TXN_CODE(String cIM_TXN_CODE) {
		CIM_TXN_CODE = cIM_TXN_CODE;
	}

	public String getCHARGEITEMMSTR_ID() {
		return CHARGEITEMMSTR_ID;
	}

	public void setCHARGEITEMMSTR_ID(String cHARGEITEMMSTR_ID) {
		CHARGEITEMMSTR_ID = cHARGEITEMMSTR_ID;
	}

	public String getPRICE() {
		return PRICE;
	}

	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}

	public String getUNIT() {
		return UNIT;
	}

	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}

	public String getUNIT_DESC() {
		return UNIT_DESC;
	}

	public void setUNIT_DESC(String uNIT_DESC) {
		UNIT_DESC = uNIT_DESC;
	}

	public String getITEMTYPE_DESC_NORMAL() {
		return ITEMTYPE_DESC_NORMAL;
	}

	public void setITEMTYPE_DESC_NORMAL(String iTEMTYPE_DESC_NORMAL) {
		ITEMTYPE_DESC_NORMAL = iTEMTYPE_DESC_NORMAL;
	}

	public String getORDERITEMMSTR_ID() {
		return ORDERITEMMSTR_ID;
	}

	public void setORDERITEMMSTR_ID(String oRDERITEMMSTR_ID) {
		ORDERITEMMSTR_ID = oRDERITEMMSTR_ID;
	}
	
}
