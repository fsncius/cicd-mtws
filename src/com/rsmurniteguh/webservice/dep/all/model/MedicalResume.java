package com.rsmurniteguh.webservice.dep.all.model;

public class MedicalResume {
	private IpMedicalResume ipMedicalResume;
	private OpMedicalResume opMedicalResume;
	private BpjsMedicalResume bpjsMedicalResume;
	
	public IpMedicalResume getIpMedicalResume() {
		return ipMedicalResume;
	}
	public void setIpMedicalResume(IpMedicalResume ipMedicalResume) {
		this.ipMedicalResume = ipMedicalResume;
	}
	public OpMedicalResume getOpMedicalResume() {
		return opMedicalResume;
	}
	public void setOpMedicalResume(OpMedicalResume opMedicalResume) {
		this.opMedicalResume = opMedicalResume;
	}
	public BpjsMedicalResume getBpjsMedicalResume() {
		return bpjsMedicalResume;
	}
	public void setBpjsMedicalResume(BpjsMedicalResume bpjsMedicalResume) {
		this.bpjsMedicalResume = bpjsMedicalResume;
	}
	
	

}
