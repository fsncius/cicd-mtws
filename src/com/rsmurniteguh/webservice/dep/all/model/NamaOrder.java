package com.rsmurniteguh.webservice.dep.all.model;

public class NamaOrder {
	private String ORDERITEMMSTR_ID;
	private String LABGROUP_DESC;
	private String specimen;
	private String TXN_DESC;

	public String getLABGROUP_DESC() {
		return LABGROUP_DESC;
	}

	public void setLABGROUP_DESC(String lABGROUP_DESC) {
		LABGROUP_DESC = lABGROUP_DESC;
	}

	public String getSpecimen() {
		return specimen;
	}

	public void setSpecimen(String specimen) {
		this.specimen = specimen;
	}

	public String getTXN_DESC() {
		return TXN_DESC;
	}

	public void setTXN_DESC(String tXN_DESC) {
		TXN_DESC = tXN_DESC;
	}

	public String getORDERITEMMSTR_ID() {
		return ORDERITEMMSTR_ID;
	}

	public void setORDERITEMMSTR_ID(String oRDERITEMMSTR_ID) {
		ORDERITEMMSTR_ID = oRDERITEMMSTR_ID;
	}
	
	
}
