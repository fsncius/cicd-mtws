package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class SepBpjs {
    private BigDecimal cardNo;
    private String bpjsNo;
    private String sepNo;
    private BigDecimal visitId;
    private BigDecimal careproviderId;
    private String queueNo;
    private BigDecimal createdBy;
    private Timestamp createdAt;
    private BigDecimal modifiedBy;
    private Timestamp modifiedAt;
    private String defunctInd;
    private String lakaLantas;
    private Timestamp tglLantas;
    private String sepLakaLantas;
    private String asuransiLakaLantas;
    private String letterNo;
    private String placeLakaLantas;
    private String descriptLakaLantas;
    private String tipeRujukan;
    
	public BigDecimal getCardNo() {
		return cardNo;
	}
	public void setCardNo(BigDecimal cardNo) {
		this.cardNo = cardNo;
	}
	public String getBpjsNo() {
		return bpjsNo;
	}
	public void setBpjsNo(String bpjsNo) {
		this.bpjsNo = bpjsNo;
	}
	public String getSepNo() {
		return sepNo;
	}
	public void setSepNo(String sepNo) {
		this.sepNo = sepNo;
	}
	public BigDecimal getVisitId() {
		return visitId;
	}
	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}
	public BigDecimal getCareproviderId() {
		return careproviderId;
	}
	public void setCareproviderId(BigDecimal careproviderId) {
		this.careproviderId = careproviderId;
	}
	public String getQueueNo() {
		return queueNo;
	}
	public void setQueueNo(String queueNo) {
		this.queueNo = queueNo;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public BigDecimal getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(BigDecimal modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Timestamp getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public String getDefunctInd() {
		return defunctInd;
	}
	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}
	public String getLakaLantas() {
		return lakaLantas;
	}
	public void setLakaLantas(String lakaLantas) {
		this.lakaLantas = lakaLantas;
	}
	public Timestamp getTglLantas() {
		return tglLantas;
	}
	public void setTglLantas(Timestamp tglLantas) {
		this.tglLantas = tglLantas;
	}
	public String getSepLakaLantas() {
		return sepLakaLantas;
	}
	public void setSepLakaLantas(String sepLakaLantas) {
		this.sepLakaLantas = sepLakaLantas;
	}
	public String getAsuransiLakaLantas() {
		return asuransiLakaLantas;
	}
	public void setAsuransiLakaLantas(String asuransiLakaLantas) {
		this.asuransiLakaLantas = asuransiLakaLantas;
	}
	public String getLetterNo() {
		return letterNo;
	}
	public void setLetterNo(String letterNo) {
		this.letterNo = letterNo;
	}
	public String getPlaceLakaLantas() {
		return placeLakaLantas;
	}
	public void setPlaceLakaLantas(String placeLakaLantas) {
		this.placeLakaLantas = placeLakaLantas;
	}
	public String getDescriptLakaLantas() {
		return descriptLakaLantas;
	}
	public void setDescriptLakaLantas(String descriptLakaLantas) {
		this.descriptLakaLantas = descriptLakaLantas;
	}
	public String getTipeRujukan() {
		return tipeRujukan;
	}
	public void setTipeRujukan(String tipeRujukan) {
		this.tipeRujukan = tipeRujukan;
	}
}
