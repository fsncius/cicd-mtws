package com.rsmurniteguh.webservice.dep.all.model;

public class ListPasien {
	private String medicalResumeId;
	private String visitId;
	private String personName;
	private String cardNo;
	private String ruangan;
	private String tipe;
	
	public String getMedicalResumeId() {
		return medicalResumeId;
	}
	public void setMedicalResumeId(String medicalResumeId) {
		this.medicalResumeId = medicalResumeId;
	}
	public String getVisitId() {
		return visitId;
	}
	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getRuangan() {
		return ruangan;
	}
	public void setRuangan(String ruangan) {
		this.ruangan = ruangan;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	
	
}
