package com.rsmurniteguh.webservice.dep.all.model;

public class NamaLabUser {
	private String ResourcemstrID;
	private String ResourceCode;
	private String ResourceshortCode;
	private String ResourcequickCode;
	private String ResourceName;
	private String ResourceNameLang;
	private String RegistrationType;
	private String SubspecialmstrId;
	private String SubspecialtyDesc;
	private String SubspecialtyDesclang;
	private String CareproviderID;
	private String PatientClassRegnType;
	public String getResourcemstrID() {
		return ResourcemstrID;
	}
	public void setResourcemstrID(String resourcemstrID) {
		ResourcemstrID = resourcemstrID;
	}
	public String getResourceCode() {
		return ResourceCode;
	}
	public void setResourceCode(String resourceCode) {
		ResourceCode = resourceCode;
	}
	public String getResourceshortCode() {
		return ResourceshortCode;
	}
	public void setResourceshortCode(String resourceshortCode) {
		ResourceshortCode = resourceshortCode;
	}
	public String getResourcequickCode() {
		return ResourcequickCode;
	}
	public void setResourcequickCode(String resourcequickCode) {
		ResourcequickCode = resourcequickCode;
	}
	public String getResourceName() {
		return ResourceName;
	}
	public void setResourceName(String resourceName) {
		ResourceName = resourceName;
	}
	public String getResourceNameLang() {
		return ResourceNameLang;
	}
	public void setResourceNameLang(String resourceNameLang) {
		ResourceNameLang = resourceNameLang;
	}
	public String getRegistrationType() {
		return RegistrationType;
	}
	public void setRegistrationType(String registrationType) {
		RegistrationType = registrationType;
	}
	public String getSubspecialmstrId() {
		return SubspecialmstrId;
	}
	public void setSubspecialmstrId(String subspecialmstrId) {
		SubspecialmstrId = subspecialmstrId;
	}
	public String getSubspecialtyDesc() {
		return SubspecialtyDesc;
	}
	public void setSubspecialtyDesc(String subspecialtyDesc) {
		SubspecialtyDesc = subspecialtyDesc;
	}
	public String getSubspecialtyDesclang() {
		return SubspecialtyDesclang;
	}
	public void setSubspecialtyDesclang(String subspecialtyDesclang) {
		SubspecialtyDesclang = subspecialtyDesclang;
	}
	public String getCareproviderID() {
		return CareproviderID;
	}
	public void setCareproviderID(String careproviderID) {
		CareproviderID = careproviderID;
	}
	public String getPatientClassRegnType() {
		return PatientClassRegnType;
	}
	public void setPatientClassRegnType(String patientClassRegnType) {
		PatientClassRegnType = patientClassRegnType;
	}
	

	
	
}
