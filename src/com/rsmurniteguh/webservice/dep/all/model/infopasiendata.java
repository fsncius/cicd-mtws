package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;
import java.sql.Date;

public class infopasiendata {

	private BigDecimal CardNo;
	private String Sex;
	private Date Birthdate;
	private String PersonName;	
	private String Opmrn;
	private String Ipmrn;
	public BigDecimal getCardNo() {
		return CardNo;
	}
	public void setCardNo(BigDecimal cardNo) {
		CardNo = cardNo;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public Date getBirthdate() {
		return Birthdate;
	}
	public void setBirthdate(Date birthdate) {
		Birthdate = birthdate;
	}
	public String getPersonName() {
		return PersonName;
	}
	public void setPersonName(String personName) {
		PersonName = personName;
	}
	public String getOpmrn() {
		return Opmrn;
	}
	public void setOpmrn(String opmrn) {
		Opmrn = opmrn;
	}
	public String getIpmrn() {
		return Ipmrn;
	}
	public void setIpmrn(String ipmrn) {
		Ipmrn = ipmrn;
	}
	
	
}
