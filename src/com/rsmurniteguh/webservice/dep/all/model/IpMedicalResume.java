package com.rsmurniteguh.webservice.dep.all.model;

public class IpMedicalResume {
	private String medicalResumeId;
	private String visitId;
	private String inDiagnosismstrId;
	private String inDiagnosisCode;
	private String inDiagnosisDesc;
	private String mainDiagnosisId;
	private String mainDiagnosisCode;
	private String mainDiagnosisDesc;
	private String secDiagnosisDesc;
	private String anamneseDesc;
	private String physicCheckDesc;
	private String supportCheckDesc;
	private String therapyDesc;
	private String lastCondition;
	private String visitDatetime;
	private String controlDatetime;
	private String resumeDatetime;
	private String dischargedDatetime;
	private String careproviderId;
	private String careproviderCode;
	private String careproviderName;
	private String wardDesc;
	private String bedNo;
	private String ipIndicationDesc;
	private String statusKesadaran;
	private String therapyDischargedDesc;
	private String personName;
	private String cardNo;
	private String tglLahir;
	private String umur;
	private String bloodPleasure;
	private String pulse;
	private String pulseType;
	private String temperature;
	private String breathing;
	private String spo2;
	private String urgentControl;
	private String sex;
	private String diagnosa0;
	private String diagIX;
	private String noSip;
	private String noControl;
	
	public String getMedicalResumeId() {
		return medicalResumeId;
	}
	public void setMedicalResumeId(String medicalResumeId) {
		this.medicalResumeId = medicalResumeId;
	}
	public String getVisitId() {
		return visitId;
	}
	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}
	public String getInDiagnosismstrId() {
		return inDiagnosismstrId;
	}
	public void setInDiagnosismstrId(String inDiagnosismstrId) {
		this.inDiagnosismstrId = inDiagnosismstrId;
	}
	public String getInDiagnosisCode() {
		return inDiagnosisCode;
	}
	public void setInDiagnosisCode(String inDiagnosisCode) {
		this.inDiagnosisCode = inDiagnosisCode;
	}
	public String getInDiagnosisDesc() {
		return inDiagnosisDesc;
	}
	public void setInDiagnosisDesc(String inDiagnosisDesc) {
		this.inDiagnosisDesc = inDiagnosisDesc;
	}
	public String getMainDiagnosisId() {
		return mainDiagnosisId;
	}
	public void setMainDiagnosisId(String mainDiagnosisId) {
		this.mainDiagnosisId = mainDiagnosisId;
	}
	public String getMainDiagnosisCode() {
		return mainDiagnosisCode;
	}
	public void setMainDiagnosisCode(String mainDiagnosisCode) {
		this.mainDiagnosisCode = mainDiagnosisCode;
	}
	public String getMainDiagnosisDesc() {
		return mainDiagnosisDesc;
	}
	public void setMainDiagnosisDesc(String mainDiagnosisDesc) {
		this.mainDiagnosisDesc = mainDiagnosisDesc;
	}
	public String getSecDiagnosisDesc() {
		return secDiagnosisDesc;
	}
	public void setSecDiagnosisDesc(String secDiagnosisDesc) {
		this.secDiagnosisDesc = secDiagnosisDesc;
	}
	public String getAnamneseDesc() {
		return anamneseDesc;
	}
	public void setAnamneseDesc(String anamneseDesc) {
		this.anamneseDesc = anamneseDesc;
	}
	public String getPhysicCheckDesc() {
		return physicCheckDesc;
	}
	public void setPhysicCheckDesc(String physicCheckDesc) {
		this.physicCheckDesc = physicCheckDesc;
	}
	public String getSupportCheckDesc() {
		return supportCheckDesc;
	}
	public void setSupportCheckDesc(String supportCheckDesc) {
		this.supportCheckDesc = supportCheckDesc;
	}
	public String getTherapyDesc() {
		return therapyDesc;
	}
	public void setTherapyDesc(String therapyDesc) {
		this.therapyDesc = therapyDesc;
	}
	public String getLastCondition() {
		return lastCondition;
	}
	public void setLastCondition(String lastCondition) {
		this.lastCondition = lastCondition;
	}
	public String getVisitDatetime() {
		return visitDatetime;
	}
	public void setVisitDatetime(String visitDatetime) {
		this.visitDatetime = visitDatetime;
	}
	public String getControlDatetime() {
		return controlDatetime;
	}
	public void setControlDatetime(String controlDatetime) {
		this.controlDatetime = controlDatetime;
	}
	public String getResumeDatetime() {
		return resumeDatetime;
	}
	public void setResumeDatetime(String resumeDatetime) {
		this.resumeDatetime = resumeDatetime;
	}
	public String getDischargedDatetime() {
		return dischargedDatetime;
	}
	public void setDischargedDatetime(String dischargedDatetime) {
		this.dischargedDatetime = dischargedDatetime;
	}
	public String getCareproviderId() {
		return careproviderId;
	}
	public void setCareproviderId(String careproviderId) {
		this.careproviderId = careproviderId;
	}
	public String getCareproviderCode() {
		return careproviderCode;
	}
	public void setCareproviderCode(String careproviderCode) {
		this.careproviderCode = careproviderCode;
	}
	public String getCareproviderName() {
		return careproviderName;
	}
	public void setCareproviderName(String careproviderName) {
		this.careproviderName = careproviderName;
	}
	public String getWardDesc() {
		return wardDesc;
	}
	public void setWardDesc(String wardDesc) {
		this.wardDesc = wardDesc;
	}
	public String getBedNo() {
		return bedNo;
	}
	public void setBedNo(String bedNo) {
		this.bedNo = bedNo;
	}
	public String getIpIndicationDesc() {
		return ipIndicationDesc;
	}
	public void setIpIndicationDesc(String ipIndicationDesc) {
		this.ipIndicationDesc = ipIndicationDesc;
	}
	public String getStatusKesadaran() {
		return statusKesadaran;
	}
	public void setStatusKesadaran(String statusKesadaran) {
		this.statusKesadaran = statusKesadaran;
	}
	public String getTherapyDischargedDesc() {
		return therapyDischargedDesc;
	}
	public void setTherapyDischargedDesc(String therapyDischargedDesc) {
		this.therapyDischargedDesc = therapyDischargedDesc;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getTglLahir() {
		return tglLahir;
	}
	public void setTglLahir(String tglLahir) {
		this.tglLahir = tglLahir;
	}
	public String getUmur() {
		return umur;
	}
	public void setUmur(String umur) {
		this.umur = umur;
	}
	public String getBloodPleasure() {
		return bloodPleasure;
	}
	public void setBloodPleasure(String bloodPleasure) {
		this.bloodPleasure = bloodPleasure;
	}
	public String getPulse() {
		return pulse;
	}
	public void setPulse(String pulse) {
		this.pulse = pulse;
	}
	public String getPulseType() {
		return pulseType;
	}
	public void setPulseType(String pulseType) {
		this.pulseType = pulseType;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getBreathing() {
		return breathing;
	}
	public void setBreathing(String breathing) {
		this.breathing = breathing;
	}
	public String getSpo2() {
		return spo2;
	}
	public void setSpo2(String spo2) {
		this.spo2 = spo2;
	}
	public String getUrgentControl() {
		return urgentControl;
	}
	public void setUrgentControl(String urgentControl) {
		this.urgentControl = urgentControl;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getDiagnosa0() {
		return diagnosa0;
	}
	public void setDiagnosa0(String diagnosa0) {
		this.diagnosa0 = diagnosa0;
	}
	public String getDiagIX() {
		return diagIX;
	}
	public void setDiagIX(String diagIX) {
		this.diagIX = diagIX;
	}
	public String getNoSip() {
		return noSip;
	}
	public void setNoSip(String noSip) {
		this.noSip = noSip;
	}
	public String getNoControl() {
		return noControl;
	}
	public void setNoControl(String noControl) {
		this.noControl = noControl;
	}
	
	

}
