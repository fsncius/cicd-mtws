package com.rsmurniteguh.webservice.dep.all.model;

public class StatusLab {
	private String orderid;
	private String nadok;
	private String nood;
	private String cdt;
	
	
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getNadok() {
		return nadok;
	}
	public void setNadok(String nadok) {
		this.nadok = nadok;
	}
	public String getNood() {
		return nood;
	}
	public void setNood(String nood) {
		this.nood = nood;
	}
	public String getCdt() {
		return cdt;
	}
	public void setCdt(String cdt) {
		this.cdt = cdt;
	}
	
	
	

}
