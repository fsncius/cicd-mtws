package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class Informasi {
    private String iuran;
    private Object dinsos;
    private Object noSKTM;
    private Object prolanisPRB;

    public String getIuran() { return iuran; }
    public void setIuran(String value) { this.iuran = value; }

    public Object getDinsos() { return dinsos; }
    public void setDinsos(Object value) { this.dinsos = value; }

    public Object getNoSKTM() { return noSKTM; }
    public void setNoSKTM(Object value) { this.noSKTM = value; }

    public Object getProlanisPRB() { return prolanisPRB; }
    public void setProlanisPRB(Object value) { this.prolanisPRB = value; }
}