package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BpjsRujukanResponse {
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private Metadata metaData;
	private Response response;
	
	public Metadata getMetaData() {
		return metaData;
	}

	public void setMetaData(Metadata metaData) {
		this.metaData = metaData;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
	
	public static BpjsRujukanResponse success(ResponseBuilder builder) {
		BpjsRujukanResponse result = new BpjsRujukanResponse();
		Metadata metaData = new Metadata();
		metaData.setCode("200");
		metaData.setMessage("OK");
		result.setMetaData(metaData);
		result.setResponse(Response.from(builder));
		return result;
	}
	
	public static BpjsRujukanResponse success(Rujukan rujukan) {
		BpjsRujukanResponse result = new BpjsRujukanResponse();
		Metadata metaData = new Metadata();
		metaData.setCode("200");
		metaData.setMessage("OK");
		result.setMetaData(metaData);
		result.setResponse(Response.from(rujukan));
		return result;
	}
	
	public static BpjsRujukanResponse fail(String message) {
		BpjsRujukanResponse result = new BpjsRujukanResponse();
		Metadata metaData = new Metadata();
		metaData.setCode("1001");
		metaData.setMessage(message);
		result.setMetaData(metaData);
		result.setResponse(null);
		return result;
	}
	
	public static BpjsRujukanResponse fail(String code, String message) {
		BpjsRujukanResponse result = new BpjsRujukanResponse();
		Metadata metaData = new Metadata();
		metaData.setCode(code);
		metaData.setMessage(message);
		result.setMetaData(metaData);
		result.setResponse(null);
		return result;
	}
	
	public static class ResponseBuilder {
		private Info diagnosa;
		private String keluhan;
	    private String noKunjungan;
	    private Info pelayanan;
	    private BpjsInfoResponse.Peserta peserta;
	    private Info poliRujukan;
	    private Info provPerujuk;
	    
	    public Info getDiagnosa() {
			return diagnosa;
		}
		public void setDiagnosa(Info diagnosa) {
			this.diagnosa = diagnosa;
		}
		public String getKeluhan() {
			return keluhan;
		}
		public void setKeluhan(String keluhan) {
			this.keluhan = keluhan;
		}
		public String getNoKunjungan() {
			return noKunjungan;
		}
		public void setNoKunjungan(String noKunjungan) {
			this.noKunjungan = noKunjungan;
		}
		public Info getPelayanan() {
			return pelayanan;
		}
		public void setPelayanan(Info pelayanan) {
			this.pelayanan = pelayanan;
		}
		public BpjsInfoResponse.Peserta getPeserta() {
			return peserta;
		}
		public void setPeserta(BpjsInfoResponse.Peserta peserta) {
			this.peserta = peserta;
		}
		public Info getPoliRujukan() {
			return poliRujukan;
		}
		public void setPoliRujukan(Info poliRujukan) {
			this.poliRujukan = poliRujukan;
		}
		public Info getProvPerujuk() {
			return provPerujuk;
		}
		public void setProvPerujuk(Info provPerujuk) {
			this.provPerujuk = provPerujuk;
		}
		public String getTglKunjungan() {
			return tglKunjungan;
		}
		public void setTglKunjungan(String tglKunjungan) {
			this.tglKunjungan = tglKunjungan;
		}
		private String tglKunjungan;
	}

	public static class Response{
		public static Response from(ResponseBuilder builder) {
			Response result = new Response();
			result.setRujukan(Rujukan.from(builder));
			return result;
		}
		
		public static Response from(Rujukan rujukan) {
			Response result = new Response();
			result.setRujukan(rujukan);
			return result;
		}
		private Rujukan rujukan;
		public Rujukan getRujukan() {
			return rujukan;
		}

		public void setRujukan(Rujukan rujukan) {
			this.rujukan = rujukan;
		}
	}
	
	public static class Rujukan  implements Comparable<Rujukan>{
		public static Rujukan from(ResponseBuilder builder) {
			Rujukan result = new Rujukan();
			result.setDiagnosa(builder.getDiagnosa());
			result.setKeluhan(builder.getKeluhan());
			result.setNoKunjungan(builder.getNoKunjungan());
			result.setPelayanan(builder.getPelayanan());
			result.setPeserta(builder.getPeserta());
			result.setPoliRujukan(builder.getPoliRujukan());
			result.setProvPerujuk(builder.getProvPerujuk());
			result.setTglKunjungan(builder.getTglKunjungan());
			return result;
		}
		private Info diagnosa;
		private String keluhan;
	    private String noKunjungan;
	    private Info pelayanan;
	    private BpjsInfoResponse.Peserta peserta;
	    private Info poliRujukan;
	    private Info provPerujuk;
	    private String tglKunjungan;
		private String faskes;
	    
		public Info getDiagnosa() {
			return diagnosa;
		}
		public void setDiagnosa(Info diagnosa) {
			this.diagnosa = diagnosa;
		}
		public String getKeluhan() {
			return keluhan;
		}
		public void setKeluhan(String keluhan) {
			this.keluhan = keluhan;
		}
		public String getNoKunjungan() {
			return noKunjungan;
		}
		public void setNoKunjungan(String noKunjungan) {
			this.noKunjungan = noKunjungan;
		}
		public Info getPelayanan() {
			return pelayanan;
		}
		public void setPelayanan(Info pelayanan) {
			this.pelayanan = pelayanan;
		}
		public BpjsInfoResponse.Peserta getPeserta() {
			return peserta;
		}
		public void setPeserta(BpjsInfoResponse.Peserta peserta) {
			this.peserta = peserta;
		}
		public Info getPoliRujukan() {
			return poliRujukan;
		}
		public void setPoliRujukan(Info poliRujukan) {
			this.poliRujukan = poliRujukan;
		}
		public Info getProvPerujuk() {
			return provPerujuk;
		}
		public void setProvPerujuk(Info provPerujuk) {
			this.provPerujuk = provPerujuk;
		}
		public String getTglKunjungan() {
			return tglKunjungan;
		}
		public void setTglKunjungan(String tglKunjungan) {
			this.tglKunjungan = tglKunjungan;
		}
		public String getFaskes() {
			return faskes;
		}
		public void setFaskes(String faskes) {
			this.faskes = faskes;
		}
		
		@Override
		public int compareTo(Rujukan o) {
			int result = 0;
			try {
				Date date = dateFormat.parse(this.getTglKunjungan());
				Date compare = dateFormat.parse(o.getTglKunjungan());
				result = compare.compareTo(date);
			} catch (ParseException ex) {
				ex.printStackTrace();
				System.out.print(ex.getMessage());
			}
			return result;
		}
	}
	
	public static class Info {
		public static Info from (String kode, String nama) {
			Info result = new Info();
			result.setKode(kode);
			result.setNama(nama);
			return result;
		}
	    private String kode;
	    private String nama;

	    public String getKode() { return kode; }
	    public void setKode(String value) { this.kode = value; }

	    public String getNama() { return nama; }
	    public void setNama(String value) { this.nama = value; }
	}
}
