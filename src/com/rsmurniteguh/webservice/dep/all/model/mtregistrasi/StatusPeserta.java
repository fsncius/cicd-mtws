package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class StatusPeserta {
    private String keterangan;
    private String kode;

    public String getKeterangan() { return keterangan; }
    public void setKeterangan(String value) { this.keterangan = value; }

    public String getKode() { return kode; }
    public void setKode(String value) { this.kode = value; }
}
