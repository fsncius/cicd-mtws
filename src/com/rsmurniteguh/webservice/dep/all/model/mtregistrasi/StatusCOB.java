package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class StatusCOB {
	private String kodeCOB;
    private String namaCOB;

    public String getKodeCOB() { return kodeCOB; }
    public void setKodeCOB(String value) { this.kodeCOB = value; }

    public String getNamaCOB() { return namaCOB; }
    public void setNamaCOB(String value) { this.namaCOB = value; }
}
