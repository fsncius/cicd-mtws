package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class DetailSep {
	private Metadata metadata;
    private ResponseDetailSep response;

    public Metadata getMetadata() { return metadata; }
    public void setMetadata(Metadata value) { this.metadata = value; }

    public ResponseDetailSep getResponse() { return response; }
    public void setResponse(ResponseDetailSep value) { this.response = value; }
}
