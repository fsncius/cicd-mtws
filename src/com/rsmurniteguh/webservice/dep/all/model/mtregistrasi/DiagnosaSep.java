package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class DiagnosaSep {
	private String kodeDiagnosa;
    private String namaDiagnosa;

    public String getKodeDiagnosa() { return kodeDiagnosa; }
    public void setKodeDiagnosa(String value) { this.kodeDiagnosa = value; }

    public String getNamaDiagnosa() { return namaDiagnosa; }
    public void setNamaDiagnosa(String value) { this.namaDiagnosa = value; }
}
