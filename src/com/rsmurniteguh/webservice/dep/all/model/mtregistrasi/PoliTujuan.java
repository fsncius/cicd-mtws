package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class PoliTujuan {
	private String kdPoli;
    private String nmPoli;

    public String getKdPoli() { return kdPoli; }
    public void setKdPoli(String value) { this.kdPoli = value; }

    public String getNmPoli() { return nmPoli; }
    public void setNmPoli(String value) { this.nmPoli = value; }
}
