package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class CreateSep {
	private Metadata metadata;
    private String response;

    public Metadata getMetadata() { return metadata; }
    public void setMetadata(Metadata value) { this.metadata = value; }
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}

}
