package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Informasi;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.JenisPeserta;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.KelasTanggungan;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.ProvUmum;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.StatusPeserta;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Umur;

public class Peserta {
    private String pisa;
    private String nama;
    private JenisPeserta jenisPeserta;
    private Informasi informasi;
    private KelasTanggungan kelasTanggungan;
    private String noKartu;
    private String nik;
    private String noMr;
    private String tglCetakKartu;
    private String sex;
    private ProvUmum provUmum;
    private StatusPeserta statusPeserta;
    private String tglTAT;
    private String tglLahir;
    private String tglTMT;
    private Umur umur;

    public String getPisa() { return pisa; }
    public void setPisa(String value) { this.pisa = value; }

    public String getNama() { return nama; }
    public void setNama(String value) { this.nama = value; }

    public JenisPeserta getJenisPeserta() { return jenisPeserta; }
    public void setJenisPeserta(JenisPeserta value) { this.jenisPeserta = value; }

    public Informasi getInformasi() { return informasi; }
    public void setInformasi(Informasi value) { this.informasi = value; }

    public KelasTanggungan getKelasTanggungan() { return kelasTanggungan; }
    public void setKelasTanggungan(KelasTanggungan value) { this.kelasTanggungan = value; }

    public String getNoKartu() { return noKartu; }
    public void setNoKartu(String value) { this.noKartu = value; }

    public String getNik() { return nik; }
    public void setNik(String value) { this.nik = value; }

    public String getNoMr() { return noMr; }
    public void setNoMr(String value) { this.noMr = value; }

    public String getTglCetakKartu() { return tglCetakKartu; }
    public void setTglCetakKartu(String value) { this.tglCetakKartu = value; }

    public String getSex() { return sex; }
    public void setSex(String value) { this.sex = value; }

    public ProvUmum getProvUmum() { return provUmum; }
    public void setProvUmum(ProvUmum value) { this.provUmum = value; }

    public StatusPeserta getStatusPeserta() { return statusPeserta; }
    public void setStatusPeserta(StatusPeserta value) { this.statusPeserta = value; }

    public String getTglTAT() { return tglTAT; }
    public void setTglTAT(String value) { this.tglTAT = value; }

    public String getTglLahir() { return tglLahir; }
    public void setTglLahir(String value) { this.tglLahir = value; }

    public String getTglTMT() { return tglTMT; }
    public void setTglTMT(String value) { this.tglTMT = value; }

    public Umur getUmur() { return umur; }
    public void setUmur(Umur value) { this.umur = value; }
}
