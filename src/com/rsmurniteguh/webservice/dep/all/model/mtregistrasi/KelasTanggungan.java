package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class KelasTanggungan {
    private String kdKelas;
    private String nmKelas;

    public String getKdKelas() { return kdKelas; }
    public void setKdKelas(String value) { this.kdKelas = value; }

    public String getNmKelas() { return nmKelas; }
    public void setNmKelas(String value) { this.nmKelas = value; }
}
