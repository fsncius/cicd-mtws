package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class InsertRegGeneral {

	private String careProviderId; 
	private String patient_name; 
	private String tgl_berobat; 
	private String patientId; 
	private String location; 
	private String group_id;
	private String mrn; 
	private String userid;
	private String source;
	private String nomorDokter;
    private String nomorSpesialis;
    private Boolean online;
    private String onlineQueueNo;

	public String getOnlineQueueNo() {
		return onlineQueueNo;
	}
	public void setOnlineQueueNo(String onlineQueueNo) {
		this.onlineQueueNo = onlineQueueNo;
	}
	public String getNomorDokter() {
		return nomorDokter;
	}
	public void setNomorDokter(String nomorDokter) {
		this.nomorDokter = nomorDokter;
	}
	public String getNomorSpesialis() {
		return nomorSpesialis;
	}
	public void setNomorSpesialis(String nomorSpesialis) {
		this.nomorSpesialis = nomorSpesialis;
	}
	public Boolean getOnline() {
		return online;
	}
	public void setOnline(Boolean online) {
		this.online = online;
	}
	public String getCareProviderId() {
		return careProviderId;
	}
	public void setCareProviderId(String careProviderId) {
		this.careProviderId = careProviderId;
	}
	public String getPatient_name() {
		return patient_name;
	}
	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}
	public String getTgl_berobat() {
		return tgl_berobat;
	}
	public void setTgl_berobat(String tgl_berobat) {
		this.tgl_berobat = tgl_berobat;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getGroup_id() {
		return group_id;
	}
	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}
	public String getMrn() {
		return mrn;
	}
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
}
