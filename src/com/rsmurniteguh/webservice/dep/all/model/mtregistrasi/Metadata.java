package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class Metadata {
    private String code;
    private String message;

    public String getCode() { return code; }
    public void setCode(String value) { this.code = value; }

    public String getMessage() { return message; }
    public void setMessage(String value) { this.message = value; }
}
