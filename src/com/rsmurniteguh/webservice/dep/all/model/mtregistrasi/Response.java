package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Peserta;

public class Response {
    private Peserta peserta;
    private Item item;

    public Item getItem() { return item; }
    public void setItem(Item value) { this.item = value; }
    public Peserta getPeserta() { return peserta; }
    public void setPeserta(Peserta value) { this.peserta = value; }
}