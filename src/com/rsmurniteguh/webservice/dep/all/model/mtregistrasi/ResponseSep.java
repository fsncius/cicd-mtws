package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.SepList;

public class ResponseSep {
	private String limit;
    private String count;
    private SepList[] list;
    private String start;

    public String getLimit() { return limit; }
    public void setLimit(String value) { this.limit = value; }

    public String getCount() { return count; }
    public void setCount(String value) { this.count = value; }

    public SepList[] getList() { return list; }
    public void setList(SepList[] value) { this.list = value; }

    public String getStart() { return start; }
    public void setStart(String value) { this.start = value; }
}
