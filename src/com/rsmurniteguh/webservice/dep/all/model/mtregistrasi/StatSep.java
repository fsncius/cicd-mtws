package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class StatSep {
	private Object ketStatSep;
    private String kdStatSep;
    private String nmStatSep;

    public Object getKetStatSep() { return ketStatSep; }
    public void setKetStatSep(Object value) { this.ketStatSep = value; }

    public String getKdStatSep() { return kdStatSep; }
    public void setKdStatSep(String value) { this.kdStatSep = value; }

    public String getNmStatSep() { return nmStatSep; }
    public void setNmStatSep(String value) { this.nmStatSep = value; }
}
