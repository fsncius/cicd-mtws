package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;
import java.util.Map;

public class BpjsSepDetailResponse {
	private Metadata metadata;
	private Response response;
	
	public Metadata getMetadata() {
		return metadata;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
	
	public class Response{
		private String ppkPerujuk;
	    private String catatan;
	    private String diagnosa;
	    private String jnsPelayanan;
	    private String kelasRawat;
	    private String noSep;
	    private String penjamin;
	    private Map<String, String> peserta;
	    private String poli;
	    private String poliEksekutif;
	    private String tglSep;

	    public String getPpkPerujuk() { return ppkPerujuk; }
	    public void setPpkPerujuk(String value) { this.ppkPerujuk = value; }

	    public String getCatatan() { return catatan; }
	    public void setCatatan(String value) { this.catatan = value; }

	    public String getDiagnosa() { return diagnosa; }
	    public void setDiagnosa(String value) { this.diagnosa = value; }

	    public String getJnsPelayanan() { return jnsPelayanan; }
	    public void setJnsPelayanan(String value) { this.jnsPelayanan = value; }

	    public String getKelasRawat() { return kelasRawat; }
	    public void setKelasRawat(String value) { this.kelasRawat = value; }

	    public String getNoSep() { return noSep; }
	    public void setNoSep(String value) { this.noSep = value; }

	    public String getPenjamin() { return penjamin; }
	    public void setPenjamin(String value) { this.penjamin = value; }

	    public Map<String, String> getPeserta() { return peserta; }
	    public void setPeserta(Map<String, String> value) { this.peserta = value; }

	    public String getPoli() { return poli; }
	    public void setPoli(String value) { this.poli = value; }

	    public String getPoliEksekutif() { return poliEksekutif; }
	    public void setPoliEksekutif(String value) { this.poliEksekutif = value; }

	    public String getTglSep() { return tglSep; }
	    public void setTglSep(String value) { this.tglSep = value; }
	}
}
