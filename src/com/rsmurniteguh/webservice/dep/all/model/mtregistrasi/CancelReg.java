package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class CancelReg {
	private String mrn; 
	private String tgl_berobat; 
	private String userid;
	private String visitid; 
	private String remarks; 
	private String cancelreason;
	private String noSep;
	
	public String getMrn() {
		return mrn;
	}
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	public String getTgl_berobat() {
		return tgl_berobat;
	}
	public void setTgl_berobat(String tgl_berobat) {
		this.tgl_berobat = tgl_berobat;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getVisitid() {
		return visitid;
	}
	public void setVisitid(String visitid) {
		this.visitid = visitid;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getCancelreason() {
		return cancelreason;
	}
	public void setCancelreason(String cancelreason) {
		this.cancelreason = cancelreason;
	}
	public String getNoSep() {
		return noSep;
	}
	public void setNoSep(String noSep) {
		this.noSep = noSep;
	}
}
