package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;


import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.ResponseSep;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Metadata;

public class GetSep {
	private Metadata metadata;
    private ResponseSep response;

    public Metadata getMetadata() { return metadata; }
    public void setMetadata(Metadata value) { this.metadata = value; }

    public ResponseSep getResponse() { return response; }
    public void setResponse(ResponseSep value) { this.response = value; }
}
