package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Metadata;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Response;

public class MemberInfo {
	private Metadata metadata;
    private Response response;

    public Metadata getMetadata() { return metadata; }
    public void setMetadata(Metadata value) { this.metadata = value; }

    public Response getResponse() { return response; }
    public void setResponse(Response value) { this.response = value; }
}


