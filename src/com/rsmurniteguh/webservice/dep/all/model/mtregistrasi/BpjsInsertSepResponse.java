package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;
import java.util.Map;

public class BpjsInsertSepResponse {
	private Metadata metaData;
	private Response response;
	
	public Metadata getMetaData() {
		return metaData;
	}

	public void setMetaData(Metadata metaData) {
		this.metaData = metaData;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public class Response{
		private SEP sep;

		public SEP getSep() {
			return sep;
		}

		public void setSep(SEP sep) {
			this.sep = sep;
		}
		
	}
	
	public class SEP{
		private String PPKPerujuk;
	    private String catatan;
	    private String diagnosa;
	    private String jnsPelayanan;
	    private String kelasRawat;
	    private String noSep;
	    private String penjamin;
	    private Peserta peserta;
	    private String poli;
	    private String poliEksekutif;
	    private String tglSep;

	    public String getPpkPerujuk() { return PPKPerujuk; }
	    public void setPpkPerujuk(String value) { this.PPKPerujuk = value; }

	    public String getCatatan() { return catatan; }
	    public void setCatatan(String value) { this.catatan = value; }

	    public String getDiagnosa() { return diagnosa; }
	    public void setDiagnosa(String value) { this.diagnosa = value; }

	    public String getJnsPelayanan() { return jnsPelayanan; }
	    public void setJnsPelayanan(String value) { this.jnsPelayanan = value; }

	    public String getKelasRawat() { return kelasRawat; }
	    public void setKelasRawat(String value) { this.kelasRawat = value; }

	    public String getNoSep() { return noSep; }
	    public void setNoSep(String value) { this.noSep = value; }

	    public String getPenjamin() { return penjamin; }
	    public void setPenjamin(String value) { this.penjamin = value; }

	    public Peserta getPeserta() { return peserta; }
	    public void setPeserta(Peserta value) { this.peserta = value; }

	    public String getPoli() { return poli; }
	    public void setPoli(String value) { this.poli = value; }

	    public String getPoliEksekutif() { return poliEksekutif; }
	    public void setPoliEksekutif(String value) { this.poliEksekutif = value; }

	    public String getTglSep() { return tglSep; }
	    public void setTglSep(String value) { this.tglSep = value; }
	}
	
	public class Peserta{
		private String asuransi;
		private String hakKelas;
		private String jnsPeserta;
		private String kelamin;
		private String nama;
		private String noKartu;
		private String noMr;
		private String tglLahir;
		
		public String getAsuransi() {
			return asuransi;
		}
		public void setAsuransi(String asuransi) {
			this.asuransi = asuransi;
		}
		public String getHakKelas() {
			return hakKelas;
		}
		public void setHakKelas(String hakKelas) {
			this.hakKelas = hakKelas;
		}
		public String getJnsPeserta() {
			return jnsPeserta;
		}
		public void setJnsPeserta(String jnsPeserta) {
			this.jnsPeserta = jnsPeserta;
		}
		public String getKelamin() {
			return kelamin;
		}
		public void setKelamin(String kelamin) {
			this.kelamin = kelamin;
		}
		public String getNama() {
			return nama;
		}
		public void setNama(String nama) {
			this.nama = nama;
		}
		public String getNoKartu() {
			return noKartu;
		}
		public void setNoKartu(String noKartu) {
			this.noKartu = noKartu;
		}
		public String getNoMr() {
			return noMr;
		}
		public void setNoMr(String noMr) {
			this.noMr = noMr;
		}
		public String getTglLahir() {
			return tglLahir;
		}
		public void setTglLahir(String tglLahir) {
			this.tglLahir = tglLahir;
		}
	}
}
