package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class ResponseDetailSep {
	private String noSep;
    private String jnsPelayanan;
    private String catatan;
    private String byTagihan;
    private Diagnosa diagAwal;
    private LakaLantas lakaLantas;
    private KelasTanggungan klsRawat;
    private String noRujukan;
    private ProvUmum provRujukan;
    private PoliTujuan poliTujuan;
    private Peserta peserta;
    private ProvUmum provPelayanan;
    private StatusCOB statusCOB;
    private String tglRujukan;
    private StatSep statSep;
    private String tglPulang;
    private String tglSep;

    public String getNoSep() { return noSep; }
    public void setNoSep(String value) { this.noSep = value; }

    public String getJnsPelayanan() { return jnsPelayanan; }
    public void setJnsPelayanan(String value) { this.jnsPelayanan = value; }

    public String getCatatan() { return catatan; }
    public void setCatatan(String value) { this.catatan = value; }

    public String getByTagihan() { return byTagihan; }
    public void setByTagihan(String value) { this.byTagihan = value; }

    public Diagnosa getDiagAwal() { return diagAwal; }
    public void setDiagAwal(Diagnosa value) { this.diagAwal = value; }

    public LakaLantas getLakaLantas() { return lakaLantas; }
    public void setLakaLantas(LakaLantas value) { this.lakaLantas = value; }

    public KelasTanggungan getKlsRawat() { return klsRawat; }
    public void setKlsRawat(KelasTanggungan value) { this.klsRawat = value; }

    public String getNoRujukan() { return noRujukan; }
    public void setNoRujukan(String value) { this.noRujukan = value; }

    public ProvUmum getProvRujukan() { return provRujukan; }
    public void setProvRujukan(ProvUmum value) { this.provRujukan = value; }

    public PoliTujuan getPoliTujuan() { return poliTujuan; }
    public void setPoliTujuan(PoliTujuan value) { this.poliTujuan = value; }

    public Peserta getPeserta() { return peserta; }
    public void setPeserta(Peserta value) { this.peserta = value; }

    public ProvUmum getProvPelayanan() { return provPelayanan; }
    public void setProvPelayanan(ProvUmum value) { this.provPelayanan = value; }

    public StatusCOB getStatusCOB() { return statusCOB; }
    public void setStatusCOB(StatusCOB value) { this.statusCOB = value; }

    public String getTglRujukan() { return tglRujukan; }
    public void setTglRujukan(String value) { this.tglRujukan = value; }

    public StatSep getStatSep() { return statSep; }
    public void setStatSep(StatSep value) { this.statSep = value; }

    public String getTglPulang() { return tglPulang; }
    public void setTglPulang(String value) { this.tglPulang = value; }

    public String getTglSep() { return tglSep; }
    public void setTglSep(String value) { this.tglSep = value; }
}
