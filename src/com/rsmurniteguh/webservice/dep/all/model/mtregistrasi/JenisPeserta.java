package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class JenisPeserta {
    private String kdJenisPeserta;
    private String nmJenisPeserta;

    public String getKdJenisPeserta() { return kdJenisPeserta; }
    public void setKdJenisPeserta(String value) { this.kdJenisPeserta = value; }

    public String getNmJenisPeserta() { return nmJenisPeserta; }
    public void setNmJenisPeserta(String value) { this.nmJenisPeserta = value; }
}
