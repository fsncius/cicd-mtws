package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class Diagnosa {
	private String kdDiag;
    private String nmDiag;

    public String getKdDiag() { return kdDiag; }
    public void setKdDiag(String value) { this.kdDiag = value; }

    public String getNmDiag() { return nmDiag; }
    public void setNmDiag(String value) { this.nmDiag = value; }
}
