package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class ProvUmum {
    private String kdProvider;
    private String kdCabang;
    private String nmCabang;
    private String nmProvider;

    public String getKdProvider() { return kdProvider; }
    public void setKdProvider(String value) { this.kdProvider = value; }

    public String getKdCabang() { return kdCabang; }
    public void setKdCabang(String value) { this.kdCabang = value; }

    public String getNmCabang() { return nmCabang; }
    public void setNmCabang(String value) { this.nmCabang = value; }

    public String getNmProvider() { return nmProvider; }
    public void setNmProvider(String value) { this.nmProvider = value; }
}
