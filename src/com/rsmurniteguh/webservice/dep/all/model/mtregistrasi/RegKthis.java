package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.ListGetQueue;
import com.rsmurniteguh.webservice.dep.all.model.ListSelfReg;
import com.rsmurniteguh.webservice.dep.all.model.RegData;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInsertSepResponse.SEP;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.ResponseSep;;

public class RegKthis {
	private boolean response;
	private int level;
	private String description;
	private BpjsSepDetailResponse retSep;
	private SEP sepResponse;
	private ListGetQueue listQueue;
	private ListSelfReg listSelfReg;
	private RegData regdata;
	
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public boolean isResponse() {
		return response;
	}
	public void setResponse(boolean response) {
		this.response = response;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ListGetQueue getListQueue() {
		return listQueue;
	}
	public void setListQueue(ListGetQueue listQueue) {
		this.listQueue = listQueue;
	}
	public ListSelfReg getListSelfReg() {
		return listSelfReg;
	}
	public void setListSelfReg(ListSelfReg listSelfReg) {
		this.listSelfReg = listSelfReg;
	}
	public BpjsSepDetailResponse getRetSep() {
		return retSep;
	}
	public void setRetSep(BpjsSepDetailResponse retSep) {
		this.retSep = retSep;
	}
	public RegData getRegdata() {
		return regdata;
	}
	public void setRegdata(RegData regdata) {
		this.regdata = regdata;
	}
	public SEP getSepResponse() {
		return sepResponse;
	}
	public void setSepResponse(SEP sepResponse) {
		this.sepResponse = sepResponse;
	}
}
