package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class Umur {
    private String umurSaatPelayanan;
    private String umurSekarang;

    public String getUmurSaatPelayanan() { return umurSaatPelayanan; }
    public void setUmurSaatPelayanan(String value) { this.umurSaatPelayanan = value; }

    public String getUmurSekarang() { return umurSekarang; }
    public void setUmurSekarang(String value) { this.umurSekarang = value; }
}
