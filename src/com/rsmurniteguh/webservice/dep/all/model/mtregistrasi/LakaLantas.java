package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class LakaLantas {
	private String keterangan;
    private String status;

    public String getKeterangan() { return keterangan; }
    public void setKeterangan(String value) { this.keterangan = value; }

    public String getStatus() { return status; }
    public void setStatus(String value) { this.status = value; }
}
