package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class InsertRegBpjs {
	    private String noBpjs;
	    private String groupId;
	    private String careProviderId;
	    private String patientId;
	    private String catatan;
	    private String location;
	    private String klsRawat;
	    private String mrn;
	    private String ppkRujukan;
	    private String patientName;
	    private String noRujukan;
	    private String poliTujuan;
	    private String tglSep;
	    private String tglRujukan;
	    private String tglBerobat;
	    private String userid;
	    private String source;
	    private String lakaLantas;
	    private String noHP;
	    private String user;
	    private String diagAwal;
	    private String jnsPelayanan;
	    private String asalRujuk;
	    private Boolean fakses;
	    private String nomorDokter;
	    private String nomorSpesialis;
	    private Boolean online;
	    private String onlineQueueNo;
	    
	    public String getOnlineQueueNo() {
			return onlineQueueNo;
		}
		public void setOnlineQueueNo(String onlineQueueNo) {
			this.onlineQueueNo = onlineQueueNo;
		}
		public String getNomorDokter() {
			return nomorDokter;
		}
		public void setNomorDokter(String nomorDokter) {
			this.nomorDokter = nomorDokter;
		}
		public String getNomorSpesialis() {
			return nomorSpesialis;
		}
		public void setNomorSpesialis(String nomorSpesialis) {
			this.nomorSpesialis = nomorSpesialis;
		}
	    
	    public Boolean getFakses() {
			return fakses;
		}
		public void setFakses(Boolean fakses) {
			this.fakses = fakses;
		}
		public String getJnsPelayanan() {
			return jnsPelayanan;
		}
		public void setJnsPelayanan(String jnsPelayanan) {
			this.jnsPelayanan = jnsPelayanan;
		}
		public String getAsalRujuk() {
			return asalRujuk;
		}
		public void setAsalRujuk(String asalRujuk) {
			this.asalRujuk = asalRujuk;
		}
	    public String getDiagAwal() {
			return diagAwal;
		}
		public void setDiagAwal(String diagAwal) {
			this.diagAwal = diagAwal;
		}
		public String getUser() {
			return user;
		}
		public void setUser(String user) {
			this.user = user;
		}
		public String getNoHP() {
			return noHP;
		}
		public void setNoHP(String noHP) {
			this.noHP = noHP;
		}
		public String getNoBpjs() { return noBpjs; }
	    public void setNoBpjs(String value) { this.noBpjs = value; }

	    public String getGroupId() { return groupId; }
	    public void setGroupId(String value) { this.groupId = value; }

	    public String getCareProviderId() { return careProviderId; }
	    public void setCareProviderId(String value) { this.careProviderId = value; }

	    public String getPatientId() { return patientId; }
	    public void setPatientId(String value) { this.patientId = value; }

	    public String getCatatan() { return catatan; }
	    public void setCatatan(String value) { this.catatan = value; }

	    public String getLocation() { return location; }
	    public void setLocation(String value) { this.location = value; }

	    public String getKlsRawat() { return klsRawat; }
	    public void setKlsRawat(String value) { this.klsRawat = value; }

	    public String getMrn() { return mrn; }
	    public void setMrn(String value) { this.mrn = value; }

	    public String getPpkRujukan() { return ppkRujukan; }
	    public void setPpkRujukan(String value) { this.ppkRujukan = value; }

	    public String getPatientName() { return patientName; }
	    public void setPatientName(String value) { this.patientName = value; }

	    public String getNoRujukan() { return noRujukan; }
	    public void setNoRujukan(String value) { this.noRujukan = value; }

	    public String getPoliTujuan() { return poliTujuan; }
	    public void setPoliTujuan(String value) { this.poliTujuan = value; }

	    public String getTglSep() { return tglSep; }
	    public void setTglSep(String value) { this.tglSep = value; }

	    public String getTglRujukan() { return tglRujukan; }
	    public void setTglRujukan(String value) { this.tglRujukan = value; }

	    public String getTglBerobat() { return tglBerobat; }
	    public void setTglBerobat(String value) { this.tglBerobat = value; }

	    public String getUserid() { return userid; }
	    public void setUserid(String value) { this.userid = value; }
		public String getSource() {
			return source;
		}
		public void setSource(String source) {
			this.source = source;
		}
		public String getLakaLantas() {
			return lakaLantas;
		}
		public void setLakaLantas(String lakaLantas) {
			this.lakaLantas = lakaLantas;
		}
		public Boolean getOnline() {
			return online;
		}
		public void setOnline(Boolean online) {
			this.online = online;
		}
}
