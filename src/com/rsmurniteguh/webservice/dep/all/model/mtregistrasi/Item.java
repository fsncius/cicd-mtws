package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class Item {
	private String noKunjungan;
    private ProvUmum ProvUmumKunjungan;
    private Diagnosa diagnosa;
    private String catatan;
    private String keluhan;
    private Peserta peserta;
    private String pemFisikLain;
    private PoliRujukan poliRujukan;
    private String tglKunjungan;
    private ProvUmum ProvUmumRujukan;
    private TktPelayanan tktPelayanan;

    public String getNoKunjungan() { return noKunjungan; }
    public void setNoKunjungan(String value) { this.noKunjungan = value; }

    public ProvUmum getProvUmumKunjungan() { return ProvUmumKunjungan; }
    public void setProvUmumKunjungan(ProvUmum value) { this.ProvUmumKunjungan = value; }

    public Diagnosa getDiagnosa() { return diagnosa; }
    public void setDiagnosa(Diagnosa value) { this.diagnosa = value; }

    public String getCatatan() { return catatan; }
    public void setCatatan(String value) { this.catatan = value; }

    public String getKeluhan() { return keluhan; }
    public void setKeluhan(String value) { this.keluhan = value; }

    public Peserta getPeserta() { return peserta; }
    public void setPeserta(Peserta value) { this.peserta = value; }

    public String getPemFisikLain() { return pemFisikLain; }
    public void setPemFisikLain(String value) { this.pemFisikLain = value; }

    public PoliRujukan getPoliRujukan() { return poliRujukan; }
    public void setPoliRujukan(PoliRujukan value) { this.poliRujukan = value; }

    public String getTglKunjungan() { return tglKunjungan; }
    public void setTglKunjungan(String value) { this.tglKunjungan = value; }

    public ProvUmum getProvUmumRujukan() { return ProvUmumRujukan; }
    public void setProvUmumRujukan(ProvUmum value) { this.ProvUmumRujukan = value; }

    public TktPelayanan getTktPelayanan() { return tktPelayanan; }
    public void setTktPelayanan(TktPelayanan value) { this.tktPelayanan = value; }
}
