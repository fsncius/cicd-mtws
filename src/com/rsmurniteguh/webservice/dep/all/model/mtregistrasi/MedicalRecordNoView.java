package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class MedicalRecordNoView {
	private BigDecimal medicalRecordNoId;
    private BigDecimal patientId;
    private String medicalRecordNoType;
    private String medicalRecordNo;
    private String remarks;
    private String systemInd;
    private String defunctInd;
    private BigDecimal lastUpdatedBy;
    private Timestamp lastUpdatedDateTime;
    private BigDecimal EntityMstrId;
    
    
	public BigDecimal getMedicalRecordNoId() {
		return medicalRecordNoId;
	}
	public void setMedicalRecordNoId(BigDecimal medicalRecordNoId) {
		this.medicalRecordNoId = medicalRecordNoId;
	}
	public BigDecimal getPatientId() {
		return patientId;
	}
	public void setPatientId(BigDecimal patientId) {
		this.patientId = patientId;
	}
	public String getMedicalRecordNoType() {
		return medicalRecordNoType;
	}
	public void setMedicalRecordNoType(String medicalRecordNoType) {
		this.medicalRecordNoType = medicalRecordNoType;
	}
	public String getMedicalRecordNo() {
		return medicalRecordNo;
	}
	public void setMedicalRecordNo(String medicalRecordNo) {
		this.medicalRecordNo = medicalRecordNo;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getSystemInd() {
		return systemInd;
	}
	public void setSystemInd(String systemInd) {
		this.systemInd = systemInd;
	}
	public String getDefunctInd() {
		return defunctInd;
	}
	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}
	public BigDecimal getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	public Timestamp getLastUpdatedDateTime() {
		return lastUpdatedDateTime;
	}
	public void setLastUpdatedDateTime(Timestamp lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
	}
	public BigDecimal getEntityMstrId() {
		return EntityMstrId;
	}
	public void setEntityMstrId(BigDecimal entityMstrId) {
		EntityMstrId = entityMstrId;
	}
    
    
    
}
