package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsRujukanResponse.ResponseBuilder;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsRujukanResponse.Rujukan;

public class BpjsRujukanListResponse{
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private Metadata metaData;
	private Response response;
	
	public Metadata getMetaData() {
		return metaData;
	}

	public void setMetaData(Metadata metaData) {
		this.metaData = metaData;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
	
	public static BpjsRujukanListResponse success(ResponseBuilder builder) {
		BpjsRujukanListResponse result = new BpjsRujukanListResponse();
		Metadata metaData = new Metadata();
		metaData.setCode("200");
		metaData.setMessage("OK");
		result.setMetaData(metaData);
		result.setResponse(Response.from(builder));
		return result;
	}
	
	public static BpjsRujukanListResponse fail(String message) {
		BpjsRujukanListResponse result = new BpjsRujukanListResponse();
		Metadata metaData = new Metadata();
		metaData.setCode("1001");
		metaData.setMessage(message);
		result.setMetaData(metaData);
		result.setResponse(null);
		return result;
	}
	
	public static BpjsRujukanListResponse fail(String code, String message) {
		BpjsRujukanListResponse result = new BpjsRujukanListResponse();
		Metadata metaData = new Metadata();
		metaData.setCode(code);
		metaData.setMessage(message);
		result.setMetaData(metaData);
		result.setResponse(null);
		return result;
	}
	

	public static class Response{
		public static Response from(ResponseBuilder builder) {
			Response result = new Response();
			Rujukan rujukan = Rujukan.from(builder);
			ArrayList<Rujukan> listRujukan = new ArrayList<Rujukan>();
			listRujukan.add(rujukan);
			result.setRujukan(listRujukan);
			return result;
		}
		private ArrayList<Rujukan> rujukan;
		public ArrayList<Rujukan> getRujukan() {
			return rujukan;
		}

		public void setRujukan(ArrayList<Rujukan> rujukan) {
			this.rujukan = rujukan;
		}
	}
	
	public static class Info {
		public static Info from (String kode, String nama) {
			Info result = new Info();
			result.setKode(kode);
			result.setNama(nama);
			return result;
		}
	    private String kode;
	    private String nama;

	    public String getKode() { return kode; }
	    public void setKode(String value) { this.kode = value; }

	    public String getNama() { return nama; }
	    public void setNama(String value) { this.nama = value; }
	}
}
