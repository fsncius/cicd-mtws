package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.DiagnosaSep;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.PoliTujuan;

public class SepList {
	private String noSEP;
    private DiagnosaSep diagnosa;
    private String biayaTagihan;
    private String jnsPelayanan;
    private String tglPulang;
    private PoliTujuan poliTujuan;
    private String tglSEP;

    public String getNoSEP() { return noSEP; }
    public void setNoSEP(String value) { this.noSEP = value; }

    public DiagnosaSep getDiagnosa() { return diagnosa; }
    public void setDiagnosa(DiagnosaSep value) { this.diagnosa = value; }

    public String getBiayaTagihan() { return biayaTagihan; }
    public void setBiayaTagihan(String value) { this.biayaTagihan = value; }

    public String getJnsPelayanan() { return jnsPelayanan; }
    public void setJnsPelayanan(String value) { this.jnsPelayanan = value; }

    public String getTglPulang() { return tglPulang; }
    public void setTglPulang(String value) { this.tglPulang = value; }

    public PoliTujuan getPoliTujuan() { return poliTujuan; }
    public void setPoliTujuan(PoliTujuan value) { this.poliTujuan = value; }

    public String getTglSEP() { return tglSEP; }
    public void setTglSEP(String value) { this.tglSEP = value; }
}
