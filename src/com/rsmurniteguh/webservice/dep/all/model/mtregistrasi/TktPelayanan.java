package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class TktPelayanan {
	private String nmPelayanan;
    private String tktPelayanan;

    public String getNmPelayanan() { return nmPelayanan; }
    public void setNmPelayanan(String value) { this.nmPelayanan = value; }

    public String getTktPelayanan() { return tktPelayanan; }
    public void setTktPelayanan(String value) { this.tktPelayanan = value; }
}
