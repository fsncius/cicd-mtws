package com.rsmurniteguh.webservice.dep.all.model.mtregistrasi;

public class DataRujukan {
	private Metadata metadata;
    private Response response;

    public Metadata getMetadata() { return metadata; }
    public void setMetadata(Metadata value) { this.metadata = value; }

    public Response getResponse() { return response; }
    public void setResponse(Response value) { this.response = value; }
}
