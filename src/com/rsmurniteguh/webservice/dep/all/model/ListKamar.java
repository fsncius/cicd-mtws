package com.rsmurniteguh.webservice.dep.all.model;

public class ListKamar {
	private String kodekelas;
	private String kapasitas;
	private String tersedia;
		
	public String getKodekelas() {
		return kodekelas;
	}
	public void setKodekelas(String kodekelas) {
		this.kodekelas = kodekelas;
	}
	public String getKapasitas() {
		return kapasitas;
	}
	public void setKapasitas(String kapasitas) {
		this.kapasitas = kapasitas;
	}
	public String getTersedia() {
		return tersedia;
	}
	public void setTersedia(String tersedia) {
		this.tersedia = tersedia;
	}
	
	
	
}
