package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class PasienDok {
	private BigDecimal Visitid;
	private String Cardno;
	private String Personname;
	private String Wardds;
	private String Bedno;
	
	public BigDecimal getVisitid()
	{
		return Visitid;
	}
	public void setVisitid(BigDecimal visitid)
	{
		this.Visitid=visitid;
	}
	
	public String getCardno()
	{
		return Cardno;
	}
	public void setCardno(String cardno)
	{
		this.Cardno=cardno;
	}
	
	public String getPersonname()
	{
		return Personname;
	}
	public void setPersonname(String personname)
	{
		this.Personname=personname;
	}
	
	public String getWardds()
	{
		return Wardds;
	}
	public void setWardds(String wardds)
	{
		this.Wardds=wardds;
	}
	
	public String getBedno()
	{
		return Bedno;
	}
	public void setBedno(String bedno)
	{
		this.Bedno=bedno;
	}
}
