package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class CareProvider {
	private BigDecimal careProviderId;
	private String personName;
	private BigDecimal usermstrId;
	
	
	public BigDecimal getUsermstrId() {
		return usermstrId;
	}
	public void setUsermstrId(BigDecimal usermstrId) {
		this.usermstrId = usermstrId;
	}
	public BigDecimal getCareProviderId() {
		return careProviderId;
	}
	public void setCareProviderId(BigDecimal careProviderId) {
		this.careProviderId = careProviderId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	
	
}
