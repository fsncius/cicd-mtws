package com.rsmurniteguh.webservice.dep.all.model;

public class StoreItemMst {

	private long StoreItemId;

	public long getStoreItemId() {
		return StoreItemId;
	}

	public void setStoreItemId(long storeItemId) {
		StoreItemId = storeItemId;
	}
	
}
