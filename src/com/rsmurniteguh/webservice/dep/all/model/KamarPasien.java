package com.rsmurniteguh.webservice.dep.all.model;

public class KamarPasien {
	public String Ward_Desc;
	public String Bed_No;
	public String Visit_Id;
	public String getWard_Desc() {
		return Ward_Desc;
	}
	public void setWard_Desc(String ward_Desc) {
		Ward_Desc = ward_Desc;
	}
	public String getBed_No() {
		return Bed_No;
	}
	public void setBed_No(String bed_No) {
		Bed_No = bed_No;
	}
	public String getVisit_Id() {
		return Visit_Id;
	}
	public void setVisit_Id(String visit_Id) {
		Visit_Id = visit_Id;
	}

}
