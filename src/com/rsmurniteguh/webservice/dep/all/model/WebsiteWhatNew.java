package com.rsmurniteguh.webservice.dep.all.model;

public class WebsiteWhatNew {
	private String subject;
	private String go_live_date;
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getGo_live_date() {
		return go_live_date;
	}
	public void setGo_live_date(String go_live_date) {
		this.go_live_date = go_live_date;
	}
	
}
