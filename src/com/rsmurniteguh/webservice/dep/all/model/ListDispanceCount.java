package com.rsmurniteguh.webservice.dep.all.model;

public class ListDispanceCount {

	private String dispensed_datetime;
	private String tipe;
	private String kelas;
	private String subspecialty_desc;
	private String dispense_batch_no;
	private String store;
	private String total;
	
	
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getDispensed_datetime() {
		return dispensed_datetime;
	}
	public void setDispensed_datetime(String dispensed_datetime) {
		this.dispensed_datetime = dispensed_datetime;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public String getKelas() {
		return kelas;
	}
	public void setKelas(String kelas) {
		this.kelas = kelas;
	}
	public String getSubspecialty_desc() {
		return subspecialty_desc;
	}
	public void setSubspecialty_desc(String subspecialty_desc) {
		this.subspecialty_desc = subspecialty_desc;
	}
	public String getDispense_batch_no() {
		return dispense_batch_no;
	}
	public void setDispense_batch_no(String dispense_batch_no) {
		this.dispense_batch_no = dispense_batch_no;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	
	
}
