package com.rsmurniteguh.webservice.dep.all.model;

public class ListInfoTransaksi {
	private String invoice_no;
	private String payment_no;
	private String trx_date;
	private String old_amount;
	private String trx_amount;
	private String created_at;
	private String created_by;
	private String new_amount;
		
	
	public String getNew_amount() {
		return new_amount;
	}
	public void setNew_amount(String new_amount) {
		this.new_amount = new_amount;
	}
	public String getInvoice_no() {
		return invoice_no;
	}
	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}
	public String getPayment_no() {
		return payment_no;
	}
	public void setPayment_no(String payment_no) {
		this.payment_no = payment_no;
	}
	public String getTrx_date() {
		return trx_date;
	}
	public void setTrx_date(String trx_date) {
		this.trx_date = trx_date;
	}
	public String getOld_amount() {
		return old_amount;
	}
	public void setOld_amount(String old_amount) {
		this.old_amount = old_amount;
	}
	public String getTrx_amount() {
		return trx_amount;
	}
	public void setTrx_amount(String trx_amount) {
		this.trx_amount = trx_amount;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}	
	
}
