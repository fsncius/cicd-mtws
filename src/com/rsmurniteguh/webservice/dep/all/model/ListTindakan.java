package com.rsmurniteguh.webservice.dep.all.model;

public class ListTindakan {
	private String TXN_CODE;
	private String ORDERED_CAREPROVIDER_ID;
	private String TGL;
	
	
	public String getTXN_CODE() {
		return TXN_CODE;
	}
	public void setTXN_CODE(String tXN_CODE) {
		TXN_CODE = tXN_CODE;
	}
	public String getORDERED_CAREPROVIDER_ID() {
		return ORDERED_CAREPROVIDER_ID;
	}
	public void setORDERED_CAREPROVIDER_ID(String oRDERED_CAREPROVIDER_ID) {
		ORDERED_CAREPROVIDER_ID = oRDERED_CAREPROVIDER_ID;
	}
	public String getTGL() {
		return TGL;
	}
	public void setTGL(String tGL) {
		TGL = tGL;
	}

}
