package com.rsmurniteguh.webservice.dep.all.model;

public class VisitId {
	private String VISIT_ID;

	public String getVISIT_ID() {
		return VISIT_ID;
	}

	public void setVISIT_ID(String vISIT_ID) {
		VISIT_ID = vISIT_ID;
	} 
}
