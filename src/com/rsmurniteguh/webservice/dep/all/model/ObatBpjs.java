package com.rsmurniteguh.webservice.dep.all.model;

public class ObatBpjs {
	private String namaObat;

	public String getNamaObat() {
		return namaObat;
	}

	public void setNamaObat(String namaObat) {
		this.namaObat = namaObat;
	}

}
