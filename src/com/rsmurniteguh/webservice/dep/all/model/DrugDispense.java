package com.rsmurniteguh.webservice.dep.all.model;

public class DrugDispense {
	private String DRUGDISPENSE_ID;
	private String visit_id;
	private String MATERIAL_ITEM_NAME;
	private String QUEUE_NO;
	private String PREPARE_BATCH_NO;
	private String prepared_datetime;
	private String DISPENSE_BATCH_NO;
	private String dispensed_datetime;
	private String bill_status;
	private String rn;
	private String careprovider_id;
	private String usermstr_id;
	private String person_name;
	private String RECEIVE_STATUS;
	private String RECEIVE_DATETIME;
	
	public String getVisit_id() {
		return visit_id;
	}
	public void setVisit_id(String visit_id) {
		this.visit_id = visit_id;
	}
	public String getQUEUE_NO() {
		return QUEUE_NO;
	}
	public void setQUEUE_NO(String qUEUE_NO) {
		QUEUE_NO = qUEUE_NO;
	}
	public String getPREPARE_BATCH_NO() {
		return PREPARE_BATCH_NO;
	}
	public void setPREPARE_BATCH_NO(String pREPARE_BATCH_NO) {
		PREPARE_BATCH_NO = pREPARE_BATCH_NO;
	}
	public String getPrepared_datetime() {
		return prepared_datetime;
	}
	public void setPrepared_datetime(String prepared_datetime) {
		this.prepared_datetime = prepared_datetime;
	}
	public String getDISPENSE_BATCH_NO() {
		return DISPENSE_BATCH_NO;
	}
	public void setDISPENSE_BATCH_NO(String dISPENSE_BATCH_NO) {
		DISPENSE_BATCH_NO = dISPENSE_BATCH_NO;
	}
	public String getDispensed_datetime() {
		return dispensed_datetime;
	}
	public void setDispensed_datetime(String dispensed_datetime) {
		this.dispensed_datetime = dispensed_datetime;
	}
	public String getBill_status() {
		return bill_status;
	}
	public void setBill_status(String bill_status) {
		this.bill_status = bill_status;
	}
	public String getRn() {
		return rn;
	}
	public void setRn(String rn) {
		this.rn = rn;
	}
	public String getCareprovider_id() {
		return careprovider_id;
	}
	public void setCareprovider_id(String careprovider_id) {
		this.careprovider_id = careprovider_id;
	}
	public String getUsermstr_id() {
		return usermstr_id;
	}
	public void setUsermstr_id(String usermstr_id) {
		this.usermstr_id = usermstr_id;
	}
	public String getPerson_name() {
		return person_name;
	}
	public void setPerson_name(String person_name) {
		this.person_name = person_name;
	}
	
	public String getRECEIVE_STATUS() {
		return RECEIVE_STATUS;
	}
	
	public void setRECEIVE_STATUS(String Receive_Status) {
		RECEIVE_STATUS = Receive_Status;
	}
	
	public String getRECEIVE_DATETIME() {
		return RECEIVE_DATETIME;
	}
	
	public void setRECEIVE_DATETIME(String Receive_datetime) {
		RECEIVE_DATETIME = Receive_datetime;
	}
	
	public String getDRUGDISPENSE_ID() {
		return DRUGDISPENSE_ID;
	}
	
	public void setDRUGDISPENSE_ID(String DrugDispense_id) {
		DRUGDISPENSE_ID = DrugDispense_id;
	}
	
	public String getMATERIAL_ITEM_NAME() {
		return MATERIAL_ITEM_NAME;
	}
	
	public void setMATERIAL_ITEM_NAME(String material_item_name) {
		// TODO Auto-generated method stub
		MATERIAL_ITEM_NAME = material_item_name;
	}
}

