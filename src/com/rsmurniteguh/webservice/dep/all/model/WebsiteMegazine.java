package com.rsmurniteguh.webservice.dep.all.model;

public class WebsiteMegazine {
	private String id_megazine;
	private String edisi;
	private String download;
	private String folder;
	
	
	public String getId_megazine() {
		return id_megazine;
	}
	public void setId_megazine(String id_megazine) {
		this.id_megazine = id_megazine;
	}
	public String getEdisi() {
		return edisi;
	}
	public void setEdisi(String edisi) {
		this.edisi = edisi;
	}
	public String getDownload() {
		return download;
	}
	public void setDownload(String download) {
		this.download = download;
	}
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	
	
}
