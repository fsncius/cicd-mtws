package com.rsmurniteguh.webservice.dep.all.model;

public class ListDataRgPasien {
	
	private String card_NO;
	private String PERSON_NAME;
	private String VISIT_ID;
	private String ward_desc;
	private String Bed_No;
	private String NAMA_DOKTER;
	private String patient_class;
	public String getCard_NO() {
		return card_NO;
	}
	public void setCard_NO(String card_NO) {
		this.card_NO = card_NO;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getVISIT_ID() {
		return VISIT_ID;
	}
	public void setVISIT_ID(String vISIT_ID) {
		VISIT_ID = vISIT_ID;
	}
	public String getWard_desc() {
		return ward_desc;
	}
	public void setWard_desc(String ward_desc) {
		this.ward_desc = ward_desc;
	}
	public String getBed_No() {
		return Bed_No;
	}
	public void setBed_No(String bed_No) {
		Bed_No = bed_No;
	}
	public String getNAMA_DOKTER() {
		return NAMA_DOKTER;
	}
	public void setNAMA_DOKTER(String nAMA_DOKTER) {
		NAMA_DOKTER = nAMA_DOKTER;
	}
	public String getPatient_class() {
		return patient_class;
	}
	public void setPatient_class(String patient_class) {
		this.patient_class = patient_class;
	}
	
	
}
