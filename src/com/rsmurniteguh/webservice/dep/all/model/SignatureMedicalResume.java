package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class SignatureMedicalResume {
	private BigDecimal medicalResumeId;
	private BigDecimal visitId;
	private String personName;
	private String cardNo;
	private String ruangan;
	public BigDecimal getMedicalResumeId() {
		return medicalResumeId;
	}
	public void setMedicalResumeId(BigDecimal medicalResumeId) {
		this.medicalResumeId = medicalResumeId;
	}
	public BigDecimal getVisitId() {
		return visitId;
	}
	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getRuangan() {
		return ruangan;
	}
	public void setRuangan(String ruangan) {
		this.ruangan = ruangan;
	}
}
