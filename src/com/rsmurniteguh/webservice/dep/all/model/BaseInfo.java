package com.rsmurniteguh.webservice.dep.all.model;

import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.SepList;

public class BaseInfo {
	private String PERSON_NAME;
	private String SEX;
	private String BIRTH_DATE;
	private String patientId;
	private String PATIENT_CLASS;
	private String MARITAL_STATUS;
	private String ADDRESS_1;
	private boolean response;
	private String scanCode;
	private String mrn;
	private String department;
	private String description;
	private String NomorRujukan;
	private String TanggalRujukan;
	private String ppkrujukan;
	private String klsrawat;
	private SepList lastSep;
	private String diagAwal;
	private String jnsPelayanan;
    private String asalRujuk;
    private String noHP;
    private String keluhan;
    private boolean rujukanresp;
    
    public String getDiagAwal() {
		return diagAwal;
	}
	public void setDiagAwal(String diagAwal) {
		this.diagAwal = diagAwal;
	}
	public String getJnsPelayanan() {
		return jnsPelayanan;
	}
	public void setJnsPelayanan(String jnsPelayanan) {
		this.jnsPelayanan = jnsPelayanan;
	}
	public String getAsalRujuk() {
		return asalRujuk;
	}
	public void setAsalRujuk(String asalRujuk) {
		this.asalRujuk = asalRujuk;
	}
	public String getNoHP() {
		return noHP;
	}
	public void setNoHP(String noHP) {
		this.noHP = noHP;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getSEX() {
		return SEX;
	}
	public void setSEX(String sEX) {
		SEX = sEX;
	}
	public String getBIRTH_DATE() {
		return BIRTH_DATE;
	}
	public void setBIRTH_DATE(String bIRTH_DATE) {
		BIRTH_DATE = bIRTH_DATE;
	}
	public String getPATIENT_CLASS() {
		return PATIENT_CLASS;
	}
	public void setPATIENT_CLASS(String pATIENT_CLASS) {
		PATIENT_CLASS = pATIENT_CLASS;
	}
	public String getMARITAL_STATUS() {
		return MARITAL_STATUS;
	}
	public void setMARITAL_STATUS(String mARITAL_STATUS) {
		MARITAL_STATUS = mARITAL_STATUS;
	}
	public String getADDRESS_1() {
		return ADDRESS_1;
	}
	public void setADDRESS_1(String aDDRESS_1) {
		ADDRESS_1 = aDDRESS_1;
	}
	public boolean isResponse() {
		return response;
	}
	public void setResponse(boolean response) {
		this.response = response;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getScanCode() {
		return scanCode;
	}
	public void setScanCode(String scanCode) {
		this.scanCode = scanCode;
	}
	public String getMrn() {
		return mrn;
	}
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getNomorRujukan() {
		return NomorRujukan;
	}
	public void setNomorRujukan(String nomorRujukan) {
		NomorRujukan = nomorRujukan;
	}
	public String getTanggalRujukan() {
		return TanggalRujukan;
	}
	public void setTanggalRujukan(String tanggalRujukan) {
		TanggalRujukan = tanggalRujukan;
	}
	public String getKlsrawat() {
		return klsrawat;
	}
	public void setKlsrawat(String klsrawat) {
		this.klsrawat = klsrawat;
	}
	public String getPpkrujukan() {
		return ppkrujukan;
	}
	public void setPpkrujukan(String ppkrujukan) {
		this.ppkrujukan = ppkrujukan;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public SepList getLastSep() {
		return lastSep;
	}
	public void setLastSep(SepList lastSep) {
		this.lastSep = lastSep;
	}
	public boolean isRujukanresp() {
		return rujukanresp;
	}
	public void setRujukanresp(boolean rujukanresp) {
		this.rujukanresp = rujukanresp;
	}
	public String getKeluhan() {
		return keluhan;
	}
	public void setKeluhan(String keluhan) {
		this.keluhan = keluhan;
	}
	
	

}
