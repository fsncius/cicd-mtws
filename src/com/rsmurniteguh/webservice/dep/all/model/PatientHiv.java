package com.rsmurniteguh.webservice.dep.all.model;

public class PatientHiv {
	private String nadok;
	private String cdt;
	private String person_name;
	private String ward_desc;
	private String subspeciality_desc;
	private String card_no;

	public String getNadok() {
		return nadok;
	}
	public void setNadok(String nadok) {
		this.nadok = nadok;
	}
	public String getCdt() {
		return cdt;
	}
	public void setCdt(String cdt) {
		this.cdt = cdt;
	}
	public String getPerson_name() {
		return person_name;
	}
	public void setPerson_name(String person_name) {
		this.person_name = person_name;
	}
	public String getWard_desc() {
		return ward_desc;
	}
	public void setWard_desc(String ward_desc) {
		this.ward_desc = ward_desc;
	}
	public String getSubspeciality_desc() {
		return subspeciality_desc;
	}
	public void setSubspeciality_desc(String subspeciality_desc) {
		this.subspeciality_desc = subspeciality_desc;
	}
	public String getCard_no() {
		return card_no;
	}
	public void setCard_no(String card_no) {
		this.card_no = card_no;
	}
}
