package com.rsmurniteguh.webservice.dep.all.model.queue;

import java.math.BigDecimal;

public class QueueCall {
	private BigDecimal caller_id;
	private BigDecimal counter_no;
	private BigDecimal queue_id;
	private String queue_no;
	private String created_at;
	private int tipe;
	

	public BigDecimal getCaller_id() {
		return caller_id;
	}
	public void setCaller_id(BigDecimal caller_id) {
		this.caller_id = caller_id;
	}
	public BigDecimal getQueue_id() {
		return queue_id;
	}
	public void setQueue_id(BigDecimal queue_id) {
		this.queue_id = queue_id;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public int getTipe() {
		return tipe;
	}
	public void setTipe(int tipe) {
		this.tipe = tipe;
	}
	public String getQueue_no() {
		return queue_no;
	}
	public void setQueue_no(String queue_no) {
		this.queue_no = queue_no;
	}
	public BigDecimal getCounter_no() {
		return counter_no;
	}
	public void setCounter_no(BigDecimal counter_no) {
		this.counter_no = counter_no;
	}
	
	
}
