package com.rsmurniteguh.webservice.dep.all.model.queue;

import java.util.List;

public class ListWaitingQueue {
	private String counterId;
	private String counterNo;
	private String personName;
	private String currentNumber;
	private List<String> listNumber;
	public String getCounterId() {
		return counterId;
	}
	public void setCounterId(String counterId) {
		this.counterId = counterId;
	}
	public String getCounterNo() {
		return counterNo;
	}
	public void setCounterNo(String counterNo) {
		this.counterNo = counterNo;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getCurrentNumber() {
		return currentNumber;
	}
	public void setCurrentNumber(String currentNumber) {
		this.currentNumber = currentNumber;
	}
	public List<String> getListNumber() {
		return listNumber;
	}
	public void setListNumber(List<String> listNumber) {
		this.listNumber = listNumber;
	}
}
