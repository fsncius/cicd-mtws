package com.rsmurniteguh.webservice.dep.all.model.queue;

public class Location {
	private String location_name;
	private String code;
	public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
