package com.rsmurniteguh.webservice.dep.all.model.queue;

public class RequestTicket {
	String no_doctor;
	String no_subspecialis;
	String date;
	
	public String getNo_doctor() {
		return no_doctor;
	}
	public void setNo_doctor(String no_doctor) {
		this.no_doctor = no_doctor;
	}
	public String getNo_subspecialis() {
		return no_subspecialis;
	}
	public void setNo_subspecialis(String no_subspecialis) {
		this.no_subspecialis = no_subspecialis;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
}
