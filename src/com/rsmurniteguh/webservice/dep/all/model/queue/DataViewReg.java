package com.rsmurniteguh.webservice.dep.all.model.queue;

public class DataViewReg {
	private long counter_id ;
    private int counter_no ;
    private String queue_no ;
    private String location ;
    private String resource_name ;
    private String queue_code;
    
	public long getCounter_id() {
		return counter_id;
	}
	public void setCounter_id(long counter_id) {
		this.counter_id = counter_id;
	}
	public int getCounter_no() {
		return counter_no;
	}
	public void setCounter_no(int counter_no) {
		this.counter_no = counter_no;
	}
	public String getQueue_no() {
		return queue_no;
	}
	public void setQueue_no(String queue_no) {
		this.queue_no = queue_no;
	}
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name(String resource_name) {
		this.resource_name = resource_name;
	}
	public String getQueue_code() {
		return queue_code;
	}
	public void setQueue_code(String queue_code) {
		this.queue_code = queue_code;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
    
}
