package com.rsmurniteguh.webservice.dep.all.model.queue;

import java.math.BigDecimal;

public class Counter{
	private BigDecimal counter_id;
	private String location;
	private int counter_no;
	private String user_name;
	private BigDecimal usermstr_id;
	private String status;
	
	public BigDecimal getCounter_id() {
		return counter_id;
	}
	public void setCounter_id(BigDecimal counter_id) {
		this.counter_id = counter_id;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getCounter_no() {
		return counter_no;
	}
	public void setCounter_no(int counter_no) {
		this.counter_no = counter_no;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public BigDecimal getUsermstr_id() {
		return usermstr_id;
	}
	public void setUsermstr_id(BigDecimal usermstr_id) {
		this.usermstr_id = usermstr_id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
