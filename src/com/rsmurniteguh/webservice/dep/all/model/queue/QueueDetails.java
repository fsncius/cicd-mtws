package com.rsmurniteguh.webservice.dep.all.model.queue;

public class QueueDetails {
	private String queue_id;
	private String queue_no;
	private String queue_date;
	private String card_no;
	private String patient_name;
	private String created_at;
	private String created_by;
	private String updated_at;
	private String updated_by;
	private String status_queue;
	private String resourcemstr;
	private String resource_name;
	private String location;
	private String counter_id;
	private String counter_no;
	private String user_name;
	private String status_counter;
	private String start_time;
	private String end_time;

	public String getQueue_id() {
		return queue_id;
	}
	public void setQueue_id(String queue_id) {
		this.queue_id = queue_id;
	}
	public String getQueue_date() {
		return queue_date;
	}
	public void setQueue_date(String queue_date) {
		this.queue_date = queue_date;
	}
	public String getStatus_queue() {
		return status_queue;
	}
	public void setStatus_queue(String status_queue) {
		this.status_queue = status_queue;
	}
	public String getResourcemstr() {
		return resourcemstr;
	}
	public void setResourcemstr(String resourcemstr) {
		this.resourcemstr = resourcemstr;
	}
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name(String resource_name) {
		this.resource_name = resource_name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getQueue_no() {
		return queue_no;
	}
	public void setQueue_no(String queue_no) {
		this.queue_no = queue_no;
	}
	public String getCounter_id() {
		return counter_id;
	}
	public void setCounter_id(String counter_id) {
		this.counter_id = counter_id;
	}
	public String getCounter_no() {
		return counter_no;
	}
	public void setCounter_no(String counter_no) {
		this.counter_no = counter_no;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getStatus_counter() {
		return status_counter;
	}
	public void setStatus_counter(String status_counter) {
		this.status_counter = status_counter;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getCard_no() {
		return card_no;
	}
	public void setCard_no(String card_no) {
		this.card_no = card_no;
	}
	public String getPatient_name() {
		return patient_name;
	}
	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
}
