package com.rsmurniteguh.webservice.dep.all.model.queue;

import java.util.List;

public class DataConfig {
	
	private List<Location> listLocation;
	private List<Counter> listCounter;
	
	public List<Location> getListLocation() {
		return listLocation;
	}

	public void setListLocation(List<Location> listLocation) {
		this.listLocation = listLocation;
	}

	public List<Counter> getListCounter() {
		return listCounter;
	}

	public void setListCounter(List<Counter> listCounter) {
		this.listCounter = listCounter;
	}
}
