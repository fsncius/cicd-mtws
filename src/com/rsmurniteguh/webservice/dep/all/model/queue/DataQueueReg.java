package com.rsmurniteguh.webservice.dep.all.model.queue;

import java.math.BigDecimal;

public class DataQueueReg {
	private BigDecimal queue_id;
	private String queue_date;
	private String status_queue;
	private String resource_name;
	private BigDecimal resourcemstr;
	private String location;
	private String queue_no;

	public BigDecimal getQueue_id() {
		return queue_id;
	}
	public void setQueue_id(BigDecimal queue_id) {
		this.queue_id = queue_id;
	}
	public String getQueue_date() {
		return queue_date;
	}
	public void setQueue_date(String queue_date) {
		this.queue_date = queue_date;
	}
	public String getStatus_queue() {
		return status_queue;
	}
	public void setStatus_queue(String status_queue) {
		this.status_queue = status_queue;
	}
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name(String resource_name) {
		this.resource_name = resource_name;
	}
	public BigDecimal getResourcemstr() {
		return resourcemstr;
	}
	public void setResourcemstr(BigDecimal resourcemstr) {
		this.resourcemstr = resourcemstr;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getQueue_no() {
		return queue_no;
	}
	public void setQueue_no(String queue_no) {
		this.queue_no = queue_no;
	}
	
	
}
