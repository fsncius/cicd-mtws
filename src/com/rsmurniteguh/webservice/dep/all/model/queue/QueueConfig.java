package com.rsmurniteguh.webservice.dep.all.model.queue;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author benyamin
 * getConfigDataByAddress
 */
public class QueueConfig {
	private BigDecimal config_id;
	private String computer_name;
	private String ip;
	private String items;
	private String mac;
	private String location;
	private BigDecimal counter;
	private List<String> macaddress;
	
	public BigDecimal getConfig_id() {
		return config_id;
	}
	public void setConfig_id(BigDecimal config_id) {
		this.config_id = config_id;
	}
	public String getComputer_name() {
		return computer_name;
	}
	public void setComputer_name(String computer_name) {
		this.computer_name = computer_name;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getItems() {
		return items;
	}
	public void setItems(String items) {
		this.items = items;
	}
	public List<String> getMacaddress() {
		return macaddress;
	}
	public void setMacaddress(List<String> macaddress) {
		this.macaddress = macaddress;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public BigDecimal getCounter() {
		return counter;
	}
	public void setCounter(BigDecimal counter) {
		this.counter = counter;
	}
}
