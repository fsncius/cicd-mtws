package com.rsmurniteguh.webservice.dep.all.model;

import java.util.ArrayList;

public class PengajuanKlaim {
	private String noSep;
	private String tglMasuk;
	private String tglKeluar;
	private String poli;
	private String ruangRawat;
	private String kelasRawat;
	private String spesialistik;
	private ArrayList<Diagnosa> diagnosa;
	private ArrayList<Procedure> procedure;
	private String tindakLanjut;
	private String tglKontrol;
	private String dpjp;
	private String user;
	private String caraKeluar;
	private String kondisiPulang;
	
	public String getNoSep() {
		return noSep;
	}
	public void setNoSep(String noSep) {
		this.noSep = noSep;
	}
	public String getTglMasuk() {
		return tglMasuk;
	}
	public void setTglMasuk(String tglMasuk) {
		this.tglMasuk = tglMasuk;
	}
	public String getTglKeluar() {
		return tglKeluar;
	}
	public void setTglKeluar(String tglKeluar) {
		this.tglKeluar = tglKeluar;
	}
	public String getPoli() {
		return poli;
	}
	public void setPoli(String poli) {
		this.poli = poli;
	}
	public String getRuangRawat() {
		return ruangRawat;
	}
	public void setRuangRawat(String ruangRawat) {
		this.ruangRawat = ruangRawat;
	}
	public String getKelasRawat() {
		return kelasRawat;
	}
	public void setKelasRawat(String kelasRawat) {
		this.kelasRawat = kelasRawat;
	}
	public String getSpesialistik() {
		return spesialistik;
	}
	public void setSpesialistik(String spesialistik) {
		this.spesialistik = spesialistik;
	}
	public ArrayList<Diagnosa> getDiagnosa() {
		return diagnosa;
	}
	public void setDiagnosa(ArrayList<Diagnosa> diagnosa) {
		this.diagnosa = diagnosa;
	}
	public ArrayList<Procedure> getProcedure() {
		return procedure;
	}
	public void setProcedure(ArrayList<Procedure> procedure) {
		this.procedure = procedure;
	}
	public String getTindakLanjut() {
		return tindakLanjut;
	}
	public void setTindakLanjut(String tindakLanjut) {
		this.tindakLanjut = tindakLanjut;
	}
	public String getTglKontrol() {
		return tglKontrol;
	}
	public void setTglKontrol(String tglKontrol) {
		this.tglKontrol = tglKontrol;
	}
	public String getDpjp() {
		return dpjp;
	}
	public void setDpjp(String dpjp) {
		this.dpjp = dpjp;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getCaraKeluar() {
		return caraKeluar;
	}
	public void setCaraKeluar(String caraKeluar) {
		this.caraKeluar = caraKeluar;
	}
	public String getKondisiPulang() {
		return kondisiPulang;
	}
	public void setKondisiPulang(String kondisiPulang) {
		this.kondisiPulang = kondisiPulang;
	}
	
	
	
}
