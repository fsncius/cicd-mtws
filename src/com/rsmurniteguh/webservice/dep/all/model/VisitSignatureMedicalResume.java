package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class VisitSignatureMedicalResume {
	private BigDecimal medicalResumeId;
	private String careproviderId;
	private String personName;
	private BigDecimal visitId;
	private String cardNo;
	private String patientId;
	private String personId;
	private String admisionDate;
	private String patientType;
	private String patientClass;
	
	public BigDecimal getMedicalResumeId() {
		return medicalResumeId;
	}
	public void setMedicalResumeId(BigDecimal medicalResumeId) {
		this.medicalResumeId = medicalResumeId;
	}
	public String getCareproviderId() {
		return careproviderId;
	}
	public void setCareproviderId(String careproviderId) {
		this.careproviderId = careproviderId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public BigDecimal getVisitId() {
		return visitId;
	}
	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public String getAdmisionDate() {
		return admisionDate;
	}
	public void setAdmisionDate(String admisionDate) {
		this.admisionDate = admisionDate;
	}
	public String getPatientType() {
		return patientType;
	}
	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}
	public String getPatientClass() {
		return patientClass;
	}
	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}
	
	
}
