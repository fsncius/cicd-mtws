package com.rsmurniteguh.webservice.dep.all.model;

public class JobsRecruitmentEmail {
	private String fullName;
    private String email;
    private String idRegist;
    private String kodeKonfirm;
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdRegist() {
		return idRegist;
	}
	public void setIdRegist(String idRegist) {
		this.idRegist = idRegist;
	}
	public String getKodeKonfirm() {
		return kodeKonfirm;
	}
	public void setKodeKonfirm(String kodeKonfirm) {
		this.kodeKonfirm = kodeKonfirm;
	}
    
    
    
}
