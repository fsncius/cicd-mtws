package com.rsmurniteguh.webservice.dep.all.model;

public class InfoRekonObatForCat {

	private String ENTERED_DATETIME;
	private String cara;
	private String PLANNED_DATETIME;
	private String ORDERSTATUSJOURNAL_ID;
	private String PERFORMED_DATETIME;
	private String tipe;
	private String VISIT_ID;
	private String ORDERENTRY_ID;
	private String ORDERENTRYITEM_ID;
	private String LOCATION_DESC;
	private String PERSON_NAME;
	private String TXN_DESC;
	private String DOSAGE_QTY;
	private String dosage_unit;
	private String FREQUENCY_DESC;
	private String REMARKS;
	private String perawat;
	private String STOPPED_DATETIME;
	private String stopperawat;
	
	
	public String getPerawat() {
		return perawat;
	}
	public void setPerawat(String perawat) {
		this.perawat = perawat;
	}
	public String getSTOPPED_DATETIME() {
		return STOPPED_DATETIME;
	}
	public void setSTOPPED_DATETIME(String sTOPPED_DATETIME) {
		STOPPED_DATETIME = sTOPPED_DATETIME;
	}
	public String getStopperawat() {
		return stopperawat;
	}
	public void setStopperawat(String stopperawat) {
		this.stopperawat = stopperawat;
	}
	public String getENTERED_DATETIME() {
		return ENTERED_DATETIME;
	}
	public void setENTERED_DATETIME(String eNTERED_DATETIME) {
		ENTERED_DATETIME = eNTERED_DATETIME;
	}
	public String getCara() {
		return cara;
	}
	public void setCara(String cara) {
		this.cara = cara;
	}
	public String getPLANNED_DATETIME() {
		return PLANNED_DATETIME;
	}
	public void setPLANNED_DATETIME(String pLANNED_DATETIME) {
		PLANNED_DATETIME = pLANNED_DATETIME;
	}
	public String getORDERSTATUSJOURNAL_ID() {
		return ORDERSTATUSJOURNAL_ID;
	}
	public void setORDERSTATUSJOURNAL_ID(String oRDERSTATUSJOURNAL_ID) {
		ORDERSTATUSJOURNAL_ID = oRDERSTATUSJOURNAL_ID;
	}
	public String getPERFORMED_DATETIME() {
		return PERFORMED_DATETIME;
	}
	public void setPERFORMED_DATETIME(String pERFORMED_DATETIME) {
		PERFORMED_DATETIME = pERFORMED_DATETIME;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public String getVISIT_ID() {
		return VISIT_ID;
	}
	public void setVISIT_ID(String vISIT_ID) {
		VISIT_ID = vISIT_ID;
	}
	public String getORDERENTRY_ID() {
		return ORDERENTRY_ID;
	}
	public void setORDERENTRY_ID(String oRDERENTRY_ID) {
		ORDERENTRY_ID = oRDERENTRY_ID;
	}
	public String getORDERENTRYITEM_ID() {
		return ORDERENTRYITEM_ID;
	}
	public void setORDERENTRYITEM_ID(String oRDERENTRYITEM_ID) {
		ORDERENTRYITEM_ID = oRDERENTRYITEM_ID;
	}
	public String getLOCATION_DESC() {
		return LOCATION_DESC;
	}
	public void setLOCATION_DESC(String lOCATION_DESC) {
		LOCATION_DESC = lOCATION_DESC;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getTXN_DESC() {
		return TXN_DESC;
	}
	public void setTXN_DESC(String tXN_DESC) {
		TXN_DESC = tXN_DESC;
	}
	public String getDOSAGE_QTY() {
		return DOSAGE_QTY;
	}
	public void setDOSAGE_QTY(String dOSAGE_QTY) {
		DOSAGE_QTY = dOSAGE_QTY;
	}
	public String getDosage_unit() {
		return dosage_unit;
	}
	public void setDosage_unit(String dosage_unit) {
		this.dosage_unit = dosage_unit;
	}
	public String getFREQUENCY_DESC() {
		return FREQUENCY_DESC;
	}
	public void setFREQUENCY_DESC(String fREQUENCY_DESC) {
		FREQUENCY_DESC = fREQUENCY_DESC;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	
	
}