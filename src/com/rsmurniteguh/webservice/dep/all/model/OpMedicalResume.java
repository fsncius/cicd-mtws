package com.rsmurniteguh.webservice.dep.all.model;

public class OpMedicalResume {
	private String medicalResumeId;
	private String visitId;
	private String recontrol;
	private String visitDrug;
	private String fisioterapi;
	private String hemodialisa;
	private String kemoterapi;
	private String radioterapi;
	private String pengobatanUlang;
	private String mainDiagnosisId;
	private String mainDiagnosisCode;
	private String mainDiagnosisDesc;
	private String secDiagnosisDesc;
	private String anamneseDesc;
	private String physicCheckDesc;
	private String supportCheckDesc;
	private String therapyDesc;
	private String lastCondition;
	private String criticalFinding;
	private String visitDatetime;
	private String controlDatetime;
	private String careproviderId;
	private String careproviderCode;
	private String inputBy;
	private String careproviderName;
	private String sexl;
	private String birthDate;
	private String days;
	private String address1;
	private String stakeholderId;
	private String personName;
	private String cardNo;
	private String umur;
	private String therapyDischargedDesc;
	private String diagnosa0;
	private String diagIX;
	private String noSip;
	private String noControl;
	public String getMedicalResumeId() {
		return medicalResumeId;
	}
	public void setMedicalResumeId(String medicalResumeId) {
		this.medicalResumeId = medicalResumeId;
	}
	public String getVisitId() {
		return visitId;
	}
	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}
	public String getMainDiagnosisId() {
		return mainDiagnosisId;
	}
	public void setMainDiagnosisId(String mainDiagnosisId) {
		this.mainDiagnosisId = mainDiagnosisId;
	}
	public String getMainDiagnosisCode() {
		return mainDiagnosisCode;
	}
	public void setMainDiagnosisCode(String mainDiagnosisCode) {
		this.mainDiagnosisCode = mainDiagnosisCode;
	}
	public String getMainDiagnosisDesc() {
		return mainDiagnosisDesc;
	}
	public void setMainDiagnosisDesc(String mainDiagnosisDesc) {
		this.mainDiagnosisDesc = mainDiagnosisDesc;
	}
	public String getSecDiagnosisDesc() {
		return secDiagnosisDesc;
	}
	public void setSecDiagnosisDesc(String secDiagnosisDesc) {
		this.secDiagnosisDesc = secDiagnosisDesc;
	}
	public String getAnamneseDesc() {
		return anamneseDesc;
	}
	public void setAnamneseDesc(String anamneseDesc) {
		this.anamneseDesc = anamneseDesc;
	}
	public String getPhysicCheckDesc() {
		return physicCheckDesc;
	}
	public void setPhysicCheckDesc(String physicCheckDesc) {
		this.physicCheckDesc = physicCheckDesc;
	}
	public String getSupportCheckDesc() {
		return supportCheckDesc;
	}
	public void setSupportCheckDesc(String supportCheckDesc) {
		this.supportCheckDesc = supportCheckDesc;
	}
	public String getTherapyDesc() {
		return therapyDesc;
	}
	public void setTherapyDesc(String therapyDesc) {
		this.therapyDesc = therapyDesc;
	}
	public String getLastCondition() {
		return lastCondition;
	}
	public void setLastCondition(String lastCondition) {
		this.lastCondition = lastCondition;
	}
	public String getCriticalFinding() {
		return criticalFinding;
	}
	public void setCriticalFinding(String criticalFinding) {
		this.criticalFinding = criticalFinding;
	}
	public String getVisitDatetime() {
		return visitDatetime;
	}
	public void setVisitDatetime(String visitDatetime) {
		this.visitDatetime = visitDatetime;
	}
	public String getControlDatetime() {
		return controlDatetime;
	}
	public void setControlDatetime(String controlDatetime) {
		this.controlDatetime = controlDatetime;
	}
	public String getCareproviderId() {
		return careproviderId;
	}
	public void setCareproviderId(String careproviderId) {
		this.careproviderId = careproviderId;
	}
	public String getCareproviderCode() {
		return careproviderCode;
	}
	public void setCareproviderCode(String careproviderCode) {
		this.careproviderCode = careproviderCode;
	}
	public String getInputBy() {
		return inputBy;
	}
	public void setInputBy(String inputBy) {
		this.inputBy = inputBy;
	}
	public String getCareproviderName() {
		return careproviderName;
	}
	public void setCareproviderName(String careproviderName) {
		this.careproviderName = careproviderName;
	}
	public String getSexl() {
		return sexl;
	}
	public void setSexl(String sexl) {
		this.sexl = sexl;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getStakeholderId() {
		return stakeholderId;
	}
	public void setStakeholderId(String stakeholderId) {
		this.stakeholderId = stakeholderId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getUmur() {
		return umur;
	}
	public void setUmur(String umur) {
		this.umur = umur;
	}
	public String getRecontrol() {
		return recontrol;
	}
	public void setRecontrol(String recontrol) {
		this.recontrol = recontrol;
	}
	public String getVisitDrug() {
		return visitDrug;
	}
	public void setVisitDrug(String visitDrug) {
		this.visitDrug = visitDrug;
	}
	public String getFisioterapi() {
		return fisioterapi;
	}
	public void setFisioterapi(String fisioterapi) {
		this.fisioterapi = fisioterapi;
	}
	public String getHemodialisa() {
		return hemodialisa;
	}
	public void setHemodialisa(String hemodialisa) {
		this.hemodialisa = hemodialisa;
	}
	public String getKemoterapi() {
		return kemoterapi;
	}
	public void setKemoterapi(String kemoterapi) {
		this.kemoterapi = kemoterapi;
	}
	public String getRadioterapi() {
		return radioterapi;
	}
	public void setRadioterapi(String radioterapi) {
		this.radioterapi = radioterapi;
	}
	public String getPengobatanUlang() {
		return pengobatanUlang;
	}
	public void setPengobatanUlang(String pengobatanUlang) {
		this.pengobatanUlang = pengobatanUlang;
	}
	public String getTherapyDischargedDesc() {
		return therapyDischargedDesc;
	}
	public void setTherapyDischargedDesc(String therapyDischargedDesc) {
		this.therapyDischargedDesc = therapyDischargedDesc;
	}
	public String getDiagnosa0() {
		return diagnosa0;
	}
	public void setDiagnosa0(String diagnosa0) {
		this.diagnosa0 = diagnosa0;
	}
	public String getDiagIX() {
		return diagIX;
	}
	public void setDiagIX(String diagIX) {
		this.diagIX = diagIX;
	}
	public String getNoSip() {
		return noSip;
	}
	public void setNoSip(String noSip) {
		this.noSip = noSip;
	}
	public String getNoControl() {
		return noControl;
	}
	public void setNoControl(String noControl) {
		this.noControl = noControl;
	}	
	


}
