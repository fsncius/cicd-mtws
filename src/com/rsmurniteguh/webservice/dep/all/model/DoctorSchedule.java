package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

public class DoctorSchedule {
	
	private BigDecimal resourcemstrId;
	private Date regnDate;
	private String specialyst;
	private String resourceName;
	private Date startTime;
	private Date endTime;
	private int maxReg;
	public BigDecimal getResourcemstrId() {
		return resourcemstrId;
	}
	public void setResourcemstrId(BigDecimal resourcemstrId) {
		this.resourcemstrId = resourcemstrId;
	}
	public Date getRegnDate() {
		return regnDate;
	}
	public void setRegnDate(Date regnDate) {
		this.regnDate = regnDate;
	}
	public String getSpecialyst() {
		return specialyst;
	}
	public void setSpecialyst(String specialyst) {
		this.specialyst = specialyst;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public int getMaxReg() {
		return maxReg;
	}
	public void setMaxReg(int maxReg) {
		this.maxReg = maxReg;
	}
	
	
	
	
	
}
