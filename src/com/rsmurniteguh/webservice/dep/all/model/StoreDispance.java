package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class StoreDispance {

	private String batchNo;
	private BigDecimal StoreMaster;
	private BigDecimal Materialitemmstr_id;
	private String Material_item_name;
	private BigDecimal qty;
	private String Patient_class;
	private String uom;
	private String PREPARE_BATCH_NO;
	
//	private String Drugreturn_no;
	
	
	public String getPREPARE_BATCH_NO() {
		return PREPARE_BATCH_NO;
	}
	public void setPREPARE_BATCH_NO(String pREPARE_BATCH_NO) {
		PREPARE_BATCH_NO = pREPARE_BATCH_NO;
	}
	public BigDecimal getStoreMaster() {
		return StoreMaster;
	}
		public String getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public void setStoreMaster(BigDecimal storeMaster) {
		StoreMaster = storeMaster;
	}
	public BigDecimal getMaterialitemmstr_id() {
		return Materialitemmstr_id;
	}
	public void setMaterialitemmstr_id(BigDecimal materialitemmstr_id) {
		Materialitemmstr_id = materialitemmstr_id;
	}
	public String getMaterial_item_name() {
		return Material_item_name;
	}
	public void setMaterial_item_name(String material_item_name) {
		Material_item_name = material_item_name;
	}
	
	public BigDecimal getQty() {
		return qty;
	}
	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}
	public String getPatient_class() {
		return Patient_class;
	}
	public void setPatient_class(String patient_class) {
		Patient_class = patient_class;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	

}
