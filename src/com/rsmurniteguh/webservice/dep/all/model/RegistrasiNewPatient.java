package com.rsmurniteguh.webservice.dep.all.model;

public class RegistrasiNewPatient {
	private boolean success;
	private String registrasiId;
	private String fullName;
    private String birth;
    private String email;
    private String phone;
    private String tipecard;
    private String idcard;    
    private String agama;
    private String jenisKelamin;
    private String etnis;
    private String pendidikan;
    private String alamat;
    private String kota;
    private String provinsi;
    private String pasienType;
    private String pasienJenis;
    private String poliCode;
    private String resourceMstrId;
    private String careProviderId;
    private String doctorName;
    private String tglAppointment;
    private String noBpjs;
    private String fileSurat;
    private String tipeSurat;
    private String noSurat;
    private String tglSurat;
	private String status;
	private String source;
	private String noqueue;
	private String createdDate;
	private String alasan;
	private String barcode;
	private String nomrn;
	private String jadwal;
    
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public String getRegistrasiId() {
		return registrasiId;
	}
	public void setRegistrasiId(String registrasiId) {
		this.registrasiId = registrasiId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}	
	public String getTipecard() {
		return tipecard;
	}
	public void setTipecard(String tipecard) {
		this.tipecard = tipecard;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public String getAgama() {
		return agama;
	}
	public void setAgama(String agama) {
		this.agama = agama;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public String getEtnis() {
		return etnis;
	}
	public void setEtnis(String etnis) {
		this.etnis = etnis;
	}
	public String getPendidikan() {
		return pendidikan;
	}
	public void setPendidikan(String pendidikan) {
		this.pendidikan = pendidikan;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getKota() {
		return kota;
	}
	public void setKota(String kota) {
		this.kota = kota;
	}
	public String getProvinsi() {
		return provinsi;
	}
	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}
	
	public String getPasienType() {
		return pasienType;
	}
	public void setPasienType(String pasienType) {
		this.pasienType = pasienType;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getPoliCode() {
		return poliCode;
	}
	public void setPoliCode(String poliCode) {
		this.poliCode = poliCode;
	}
	public String getResourceMstrId() {
		return resourceMstrId;
	}
	public void setResourceMstrId(String resourceMstrId) {
		this.resourceMstrId = resourceMstrId;
	}	
	public String getCareProviderId() {
		return careProviderId;
	}
	public void setCareProviderId(String careProviderId) {
		this.careProviderId = careProviderId;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getTglAppointment() {
		return tglAppointment;
	}
	public void setTglAppointment(String tglAppointment) {
		this.tglAppointment = tglAppointment;
	}
	
	public String getNoBpjs() {
		return noBpjs;
	}
	public void setNoBpjs(String noBpjs) {
		this.noBpjs = noBpjs;
	}
	public String getFileSurat() {
		return fileSurat;
	}
	public void setFileSurat(String fileSurat) {
		this.fileSurat = fileSurat;
	}
	public String getNoSurat() {
		return noSurat;
	}
	public void setNoSurat(String noSurat) {
		this.noSurat = noSurat;
	}
	public String getTglSurat() {
		return tglSurat;
	}
	public void setTglSurat(String tglSurat) {
		this.tglSurat = tglSurat;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getNoqueue() {
		return noqueue;
	}
	public void setNoqueue(String noqueue) {
		this.noqueue = noqueue;
	}
	public String getAlasan() {
		return alasan;
	}
	public void setAlasan(String alasan) {
		this.alasan = alasan;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getNomrn() {
		return nomrn;
	}
	public void setNomrn(String nomrn) {
		this.nomrn = nomrn;
	}
	public String getJadwal() {
		return jadwal;
	}
	public void setJadwal(String jadwal) {
		this.jadwal = jadwal;
	}
	public String getPasienJenis() {
		return pasienJenis;
	}
	public void setPasienJenis(String pasienJenis) {
		this.pasienJenis = pasienJenis;
	}
	public String getTipeSurat() {
		return tipeSurat;
	}
	public void setTipeSurat(String tipeSurat) {
		this.tipeSurat = tipeSurat;
	}
    
    
    
}
