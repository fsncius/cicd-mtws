package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class ResourceList {
	private String Resourcescheme_id;
	private String Resource_name;
	private String Poli_code;
	private String Poli_name;
	private BigDecimal Resourcemstr_id;
	
	public String getResourcescheme_id() {
		return Resourcescheme_id;
	}
	public void setResourcescheme_id(String resourcescheme_id) {
		Resourcescheme_id = resourcescheme_id;
	}
	public String getResource_name() {
		return Resource_name;
	}
	public void setResource_name(String resource_name) {
		Resource_name = resource_name;
	}
	public String getPoli_code() {
		return Poli_code;
	}
	public void setPoli_code(String poli_code) {
		Poli_code = poli_code;
	}
	public String getPoli_name() {
		return Poli_name;
	}
	public void setPoli_name(String poli_name) {
		Poli_name = poli_name;
	}
	public BigDecimal getResourcemstr_id() {
		return Resourcemstr_id;
	}
	public void setResourcemstr_id(BigDecimal resourcemstr_id) {
		Resourcemstr_id = resourcemstr_id;
	}
	
	
	
}
