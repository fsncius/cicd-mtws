package com.rsmurniteguh.webservice.dep.all.model;

public class listHasBpjsNo {
	private String PERSON_NAME;
	private String CARD_NO;
	
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getCARD_NO() {
		return CARD_NO;
	}
	public void setCARD_NO(String cARD_NO) {
		CARD_NO = cARD_NO;
	}
	
}
