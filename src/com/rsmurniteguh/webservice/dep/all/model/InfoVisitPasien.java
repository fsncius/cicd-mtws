package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;
import java.sql.Date;

public class InfoVisitPasien {
 private BigDecimal visitid;
 private BigDecimal visitdiagnosadetail;
 private String person_name;
 private String sex;
 private Date birth_date;
 private String nama_dokter;
 private String tipe_pasien;
 private String alamat;
 private String agama;
 private String maritalstatus;
 private String nationality;
 private String mobilephone;
 private String cardno;
 private String AdmissionDate;
 private String wardDesc;
 
 
public String getWardDesc() {
	return wardDesc;
}
public void setWardDesc(String wardDesc) {
	this.wardDesc = wardDesc;
}
public String getAdmissionDate() {
	return AdmissionDate;
}
public void setAdmissionDate(String admissionDate) {
	AdmissionDate = admissionDate;
}
public String getAlamat() {
	return alamat;
}
public void setAlamat(String alamat) {
	this.alamat = alamat;
}
public String getAgama() {
	return agama;
}
public void setAgama(String agama) {
	this.agama = agama;
}
public BigDecimal getVisitid() {
	return visitid;
}
public void setVisitid(BigDecimal visitid) {
	this.visitid = visitid;
}
public BigDecimal getVisitdiagnosadetail() {
	return visitdiagnosadetail;
}
public void setVisitdiagnosadetail(BigDecimal visitdiagnosadetail) {
	this.visitdiagnosadetail = visitdiagnosadetail;
}
public String getPerson_name() {
	return person_name;
}
public void setPerson_name(String person_name) {
	this.person_name = person_name;
}
public String getSex() {
	return sex;
}
public void setSex(String sex) {
	this.sex = sex;
}
public Date getBirth_date() {
	return birth_date;
}
public void setBirth_date(Date birth_date) {
	this.birth_date = birth_date;
}
public String getNama_dokter() {
	return nama_dokter;
}
public void setNama_dokter(String nama_dokter) {
	this.nama_dokter = nama_dokter;
}
public String getTipe_pasien() {
	return tipe_pasien;
}
public void setTipe_pasien(String tipe_pasien) {
	this.tipe_pasien = tipe_pasien;
}
public String getMaritalstatus() {
	return maritalstatus;
}
public void setMaritalstatus(String maritalstatus) {
	this.maritalstatus = maritalstatus;
}
public String getNationality() {
	return nationality;
}
public void setNationality(String nationality) {
	this.nationality = nationality;
}
public String getMobilephone() {
	return mobilephone;
}
public void setMobilephone(String mobilephone) {
	this.mobilephone = mobilephone;
}
public String getCardno() {
	return cardno;
}
public void setCardno(String cardno) {
	this.cardno = cardno;
}


 
}
