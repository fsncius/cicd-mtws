package com.rsmurniteguh.webservice.dep.all.model;

public class DetailDokter {
	private String RESOURCE_NAME;
	private String POLI;
	
	public String getRESOURCE_NAME() {
		return RESOURCE_NAME;
	}
	public void setRESOURCE_NAME(String rESOURCE_NAME) {
		RESOURCE_NAME = rESOURCE_NAME;
	}
	public String getPOLI() {
		return POLI;
	}
	public void setPOLI(String pOLI) {
		POLI = pOLI;
	}
}
