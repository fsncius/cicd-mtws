package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class ListDoctorUser {

	private BigDecimal careprovider_id;
	private String person_name;
	private BigDecimal usermstr_id;
	
	
	public BigDecimal getCareprovider_id() {
		return careprovider_id;
	}
	public void setCareprovider_id(BigDecimal careprovider_id) {
		this.careprovider_id = careprovider_id;
	}
	public String getPerson_name() {
		return person_name;
	}
	public void setPerson_name(String person_name) {
		this.person_name = person_name;
	}
	public BigDecimal getUsermstr_id() {
		return usermstr_id;
	}
	public void setUsermstr_id(BigDecimal usermstr_id) {
		this.usermstr_id = usermstr_id;
	}
	
	
	
}
