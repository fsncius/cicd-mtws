package com.rsmurniteguh.webservice.dep.all.model;

public class ListResourceMstrPoly {
	private String RESOURCEMSTR_ID;
	private String RESOURCE_NAME;
	private String resource_code;
	private String defunct_ind;
	private String CAREPROVIDER_ID;
	private String RN;
	private String SESSION_DESC;
	
	
	public String getSESSION_DESC() {
		return SESSION_DESC;
	}
	public void setSESSION_DESC(String sESSION_DESC) {
		SESSION_DESC = sESSION_DESC;
	}
	public String getRESOURCEMSTR_ID() {
		return RESOURCEMSTR_ID;
	}
	public void setRESOURCEMSTR_ID(String rESOURCEMSTR_ID) {
		RESOURCEMSTR_ID = rESOURCEMSTR_ID;
	}
	public String getRESOURCE_NAME() {
		return RESOURCE_NAME;
	}
	public void setRESOURCE_NAME(String rESOURCE_NAME) {
		RESOURCE_NAME = rESOURCE_NAME;
	}
	public String getResource_code() {
		return resource_code;
	}
	public void setResource_code(String resource_code) {
		this.resource_code = resource_code;
	}
	public String getDefunct_ind() {
		return defunct_ind;
	}
	public void setDefunct_ind(String defunct_ind) {
		this.defunct_ind = defunct_ind;
	}
	public String getCAREPROVIDER_ID() {
		return CAREPROVIDER_ID;
	}
	public void setCAREPROVIDER_ID(String cAREPROVIDER_ID) {
		CAREPROVIDER_ID = cAREPROVIDER_ID;
	}
	public String getRN() {
		return RN;
	}
	public void setRN(String rN) {
		RN = rN;
	}
	
	
}
