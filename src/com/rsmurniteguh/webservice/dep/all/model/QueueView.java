package com.rsmurniteguh.webservice.dep.all.model;

public class QueueView extends Queue {
	private String location_group;

	public String getLocation_group() {
		return location_group;
	}

	public void setLocation_group(String location_group) {
		this.location_group = location_group;
	}
}
