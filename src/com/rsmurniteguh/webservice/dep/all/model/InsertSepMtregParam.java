package com.rsmurniteguh.webservice.dep.all.model;

public class InsertSepMtregParam {
	private String noKa;
	private String jnsPelayanan;
	private String klsRawat;
	private String mrn;
	private String asalRujuk;
	private String tglRujuk;
	private String noRujuk;
	private String catatan;
	private String diagAwal;
	private String poli;
	private String noHp;
	private String user;
	private String careProviderId;
	

	public InsertSepMtregParam() {
		
	}
	
	public InsertSepMtregParam(String noKa,String jnsPelayanan,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String poli,String noHp,String user, String careProviderId) {
		this.noKa = noKa;
		this.jnsPelayanan = jnsPelayanan;
		this.klsRawat = klsRawat;
		this.mrn = mrn;
		this.asalRujuk = asalRujuk;
		this.tglRujuk = tglRujuk;
		this.noRujuk = noRujuk;
		this.catatan = catatan;
		this.diagAwal = diagAwal;
		this.poli = poli;
		this.noHp = noHp;
		this.user = user;
		this.careProviderId = careProviderId;
	}
	
	public String getNoKa() {
		return noKa;
	}
	public void setNoKa(String noKa) {
		this.noKa = noKa;
	}
	public String getJnsPelayanan() {
		return jnsPelayanan;
	}
	public void setJnsPelayanan(String jnsPelayanan) {
		this.jnsPelayanan = jnsPelayanan;
	}
	public String getKlsRawat() {
		return klsRawat;
	}
	public void setKlsRawat(String klsRawat) {
		this.klsRawat = klsRawat;
	}
	public String getMrn() {
		return mrn;
	}
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	public String getAsalRujuk() {
		return asalRujuk;
	}
	public void setAsalRujuk(String asalRujuk) {
		this.asalRujuk = asalRujuk;
	}
	public String getTglRujuk() {
		return tglRujuk;
	}
	public void setTglRujuk(String tglRujuk) {
		this.tglRujuk = tglRujuk;
	}
	public String getNoRujuk() {
		return noRujuk;
	}
	public void setNoRujuk(String noRujuk) {
		this.noRujuk = noRujuk;
	}
	public String getCatatan() {
		return catatan;
	}
	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}
	public String getDiagAwal() {
		return diagAwal;
	}
	public void setDiagAwal(String diagAwal) {
		this.diagAwal = diagAwal;
	}
	public String getPoli() {
		return poli;
	}
	public void setPoli(String poli) {
		this.poli = poli;
	}
	public String getNoHp() {
		return noHp;
	}
	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getCareProviderId() {
		return careProviderId;
	}

	public void setCareProviderId(String careProviderId) {
		this.careProviderId = careProviderId;
	}
}
