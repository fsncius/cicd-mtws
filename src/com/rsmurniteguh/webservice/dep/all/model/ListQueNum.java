package com.rsmurniteguh.webservice.dep.all.model;


public class ListQueNum {
	private String visit_id;
	private String QUEUE_NO;
	private String PREPARE_BATCH_NO;
	private String prepared_datetime;
	private String DISPENSE_BATCH_NO;
	private String dispensed_datetime;
	private String card_no;
	private String bill_status;
	private String rn;
	private String careprovider_id;
	private String usermstr_id;
	private String person_name;
	private String Receive_by;

	
	public String getVisit_id() {
		return visit_id;
	}
	public void setVisit_id(String visit_id) {
		this.visit_id = visit_id;
	}
	public String getQUEUE_NO() {
		return QUEUE_NO;
	}
	public void setQUEUE_NO(String qUEUE_NO) {
		QUEUE_NO = qUEUE_NO;
	}
	public String getPREPARE_BATCH_NO() {
		return PREPARE_BATCH_NO;
	}
	public void setPREPARE_BATCH_NO(String pREPARE_BATCH_NO) {
		PREPARE_BATCH_NO = pREPARE_BATCH_NO;
	}
	public String getPrepared_datetime() {
		return prepared_datetime;
	}
	public void setPrepared_datetime(String prepared_datetime) {
		this.prepared_datetime = prepared_datetime;
	}
	public String getDISPENSE_BATCH_NO() {
		return DISPENSE_BATCH_NO;
	}
	public void setDISPENSE_BATCH_NO(String dISPENSE_BATCH_NO) {
		DISPENSE_BATCH_NO = dISPENSE_BATCH_NO;
	}
	public String getDispensed_datetime() {
		return dispensed_datetime;
	}
	public void setDispensed_datetime(String dispensed_datetime) {
		this.dispensed_datetime = dispensed_datetime;
	}
	public String getBill_status() {
		return bill_status;
	}
	public void setBill_status(String bill_status) {
		this.bill_status = bill_status;
	}
	public String getRn() {
		return rn;
	}
	public void setRn(String rn) {
		this.rn = rn;
	}
	public String getCareprovider_id() {
		return careprovider_id;
	}
	public void setCareprovider_id(String careprovider_id) {
		this.careprovider_id = careprovider_id;
	}
	public String getUsermstr_id() {
		return usermstr_id;
	}
	public void setUsermstr_id(String usermstr_id) {
		this.usermstr_id = usermstr_id;
	}
	public String getPerson_name() {
		return person_name;
	}
	public void setPerson_name(String person_name) {
		this.person_name = person_name;
	}
	public void setReceive_bY(String Receive_by) {
		// TODO Auto-generated method stub
		this.Receive_by = Receive_by;
	}
	
	public String getReceive_bY() {
		return Receive_by;
	}
	public String getCard_no() {
		return card_no;
	}
	public void setCard_no(String card_no) {
		this.card_no = card_no;
	}
	
}
