package com.rsmurniteguh.webservice.dep.all.model;

import java.sql.Date;

public class AntrianDispance {
	private Date ConfirmedDatetime;
	private Long PersonId;
	private Long PatientId;
	private String PersonName;
	private String OrderEntryItemId;
	private String OrderEntryId;
	private String CardNo;
	public Date getConfirmedDatetime() {
		return ConfirmedDatetime;
	}
	public void setConfirmedDatetime(Date confirmedDatetime) {
		ConfirmedDatetime = confirmedDatetime;
	}
	public Long getPersonId() {
		return PersonId;
	}
	public void setPersonId(Long personId) {
		PersonId = personId;
	}
	public Long getPatientId() {
		return PatientId;
	}
	public void setPatientId(Long patientId) {
		PatientId = patientId;
	}
	public String getPersonName() {
		return PersonName;
	}
	public void setPersonName(String personName) {
		PersonName = personName;
	}
	public String getOrderEntryItemId() {
		return OrderEntryItemId;
	}
	public void setOrderEntryItemId(String orderEntryItemId) {
		OrderEntryItemId = orderEntryItemId;
	}
	public String getOrderEntryId() {
		return OrderEntryId;
	}
	public void setOrderEntryId(String orderEntryId) {
		OrderEntryId = orderEntryId;
	}
	public String getCardNo() {
		return CardNo;
	}
	public void setCardNo(String cardNo) {
		CardNo = cardNo;
	}
	
}
