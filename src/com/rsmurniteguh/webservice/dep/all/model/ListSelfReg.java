package com.rsmurniteguh.webservice.dep.all.model;

public class ListSelfReg {

	//qr1
	private String RESOURCEMSTR_ID;
	private String CAREPROVIDER_ID;
	private String SUBSPECIALTYMSTR_ID;
	private String RESOURCE_NAME;
	private String REGISTRATION_TYPE;
	private String RESOURCE_QUEUE_IND;
	private String RESOURCE_CHECKOUT_IND;
	//qr2
	private String PERSON_ID;
	private String PATIENT_ID;
	private String CARD_NO;
	private String PERSON_NAME;
	private String sex;
	private String AGE;
	private String BIRTH_DATE;
	private String STAKEHOLDER_ID;
	private String PATIENT_CLASS;
	//qr3
	private String VISITID;
	//qr5
	private String  LAST_VISIT_NO;
	//qr 6
	private String PRIMARYKEY2;
	//qr8
	private String PRIMARYKEY3;
	//qr10
	private String REGN_NO;
	//qr11
	private String PRIMARYKEY4;
	//qr13
	private String PRIMARYKEY5;
	//qr15
	private String efek_date;
	//qr16
	private String PRIMARYKEY6;
	//qr18
	private String PRIMARYKEY7;
	//qr20
	private String PRIMARYKEY8;
	//qr21
	private String qn;
	//qr22
	private String qn2;
	//qr24
	private String tgreg;
	private boolean response;
	private String description;
	
	
	
	
	public String getVISITID() {
		return VISITID;
	}
	public void setVISITID(String vISITID) {
		VISITID = vISITID;
	}
	public String getTgreg() {
		return tgreg;
	}
	public void setTgreg(String tgreg) {
		this.tgreg = tgreg;
	}
	public String getQn2() {
		return qn2;
	}
	public void setQn2(String qn2) {
		this.qn2 = qn2;
	}
	public String getQn() {
		return qn;
	}
	public void setQn(String qn) {
		this.qn = qn;
	}
	public String getPRIMARYKEY8() {
		return PRIMARYKEY8;
	}
	public void setPRIMARYKEY8(String pRIMARYKEY8) {
		PRIMARYKEY8 = pRIMARYKEY8;
	}
	public String getPRIMARYKEY7() {
		return PRIMARYKEY7;
	}
	public void setPRIMARYKEY7(String pRIMARYKEY7) {
		PRIMARYKEY7 = pRIMARYKEY7;
	}
	public String getPRIMARYKEY6() {
		return PRIMARYKEY6;
	}
	public void setPRIMARYKEY6(String pRIMARYKEY6) {
		PRIMARYKEY6 = pRIMARYKEY6;
	}
	public String getEfek_date() {
		return efek_date;
	}
	public void setEfek_date(String efek_date) {
		this.efek_date = efek_date;
	}
	public String getPRIMARYKEY5() {
		return PRIMARYKEY5;
	}
	public void setPRIMARYKEY5(String pRIMARYKEY5) {
		PRIMARYKEY5 = pRIMARYKEY5;
	}
	public String getPRIMARYKEY4() {
		return PRIMARYKEY4;
	}
	public void setPRIMARYKEY4(String pRIMARYKEY4) {
		PRIMARYKEY4 = pRIMARYKEY4;
	}
	public String getREGN_NO() {
		return REGN_NO;
	}
	public void setREGN_NO(String rEGN_NO) {
		REGN_NO = rEGN_NO;
	}
	public String getPRIMARYKEY3() {
		return PRIMARYKEY3;
	}
	public void setPRIMARYKEY3(String pRIMARYKEY3) {
		PRIMARYKEY3 = pRIMARYKEY3;
	}
	public String getPRIMARYKEY2() {
		return PRIMARYKEY2;
	}
	public void setPRIMARYKEY2(String pRIMARYKEY2) {
		PRIMARYKEY2 = pRIMARYKEY2;
	}
	public String getLAST_VISIT_NO() {
		return LAST_VISIT_NO;
	}
	public void setLAST_VISIT_NO(String lAST_VISIT_NO) {
		LAST_VISIT_NO = lAST_VISIT_NO;
	}	
	public String getPERSON_ID() {
		return PERSON_ID;
	}
	public void setPERSON_ID(String pERSON_ID) {
		PERSON_ID = pERSON_ID;
	}
	public String getPATIENT_ID() {
		return PATIENT_ID;
	}
	public void setPATIENT_ID(String pATIENT_ID) {
		PATIENT_ID = pATIENT_ID;
	}
	public String getCARD_NO() {
		return CARD_NO;
	}
	public void setCARD_NO(String cARD_NO) {
		CARD_NO = cARD_NO;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAGE() {
		return AGE;
	}
	public void setAGE(String aGE) {
		AGE = aGE;
	}
	public String getBIRTH_DATE() {
		return BIRTH_DATE;
	}
	public void setBIRTH_DATE(String bIRTH_DATE) {
		BIRTH_DATE = bIRTH_DATE;
	}
	public String getSTAKEHOLDER_ID() {
		return STAKEHOLDER_ID;
	}
	public void setSTAKEHOLDER_ID(String sTAKEHOLDER_ID) {
		STAKEHOLDER_ID = sTAKEHOLDER_ID;
	}
	public String getPATIENT_CLASS() {
		return PATIENT_CLASS;
	}
	public void setPATIENT_CLASS(String pATIENT_CLASS) {
		PATIENT_CLASS = pATIENT_CLASS;
	}
	public String getRESOURCEMSTR_ID() {
		return RESOURCEMSTR_ID;
	}
	public void setRESOURCEMSTR_ID(String rESOURCEMSTR_ID) {
		RESOURCEMSTR_ID = rESOURCEMSTR_ID;
	}
	public String getCAREPROVIDER_ID() {
		return CAREPROVIDER_ID;
	}
	public void setCAREPROVIDER_ID(String cAREPROVIDER_ID) {
		CAREPROVIDER_ID = cAREPROVIDER_ID;
	}
	public String getSUBSPECIALTYMSTR_ID() {
		return SUBSPECIALTYMSTR_ID;
	}
	public void setSUBSPECIALTYMSTR_ID(String sUBSPECIALTYMSTR_ID) {
		SUBSPECIALTYMSTR_ID = sUBSPECIALTYMSTR_ID;
	}
	public String getRESOURCE_NAME() {
		return RESOURCE_NAME;
	}
	public void setRESOURCE_NAME(String rESOURCE_NAME) {
		RESOURCE_NAME = rESOURCE_NAME;
	}
	public String getREGISTRATION_TYPE() {
		return REGISTRATION_TYPE;
	}
	public void setREGISTRATION_TYPE(String rEGISTRATION_TYPE) {
		REGISTRATION_TYPE = rEGISTRATION_TYPE;
	}
	public String getRESOURCE_QUEUE_IND() {
		return RESOURCE_QUEUE_IND;
	}
	public void setRESOURCE_QUEUE_IND(String rESOURCE_QUEUE_IND) {
		RESOURCE_QUEUE_IND = rESOURCE_QUEUE_IND;
	}
	public String getRESOURCE_CHECKOUT_IND() {
		return RESOURCE_CHECKOUT_IND;
	}
	public void setRESOURCE_CHECKOUT_IND(String rESOURCE_CHECKOUT_IND) {
		RESOURCE_CHECKOUT_IND = rESOURCE_CHECKOUT_IND;
	}
	public boolean isResponse() {
		return response;
	}
	public void setResponse(boolean response) {
		this.response = response;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
