package com.rsmurniteguh.webservice.dep.all.model;

public class ReturnResult {

	public String message;
	public Boolean success;
	

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
