package com.rsmurniteguh.webservice.dep.all.model;

import java.sql.Timestamp;

public class Queue {
	private Long queue_id;
	private String queue_no;
	private String location;  
	private Timestamp queue_date;
	private String card_no;
	private String source;
	private String queue_type;
	private String patient_name;
	private Long resourcemstr;
	private String status;
	private Timestamp created_at;
	private Long created_by;
	private Timestamp updated_at;
	private Long updated_by;
	private Boolean deleted;
	public Long getQueue_id() {
		return queue_id;
	}
	public void setQueue_id(Long queue_id) {
		this.queue_id = queue_id;
	}
	public String getQueue_no() {
		return queue_no;
	}
	public void setQueue_no(String queue_no) {
		this.queue_no = queue_no;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Timestamp getQueue_date() {
		return queue_date;
	}
	public void setQueue_date(Timestamp queue_date) {
		this.queue_date = queue_date;
	}
	public String getCard_no() {
		return card_no;
	}
	public void setCard_no(String card_no) {
		this.card_no = card_no;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getQueue_type() {
		return queue_type;
	}
	public void setQueue_type(String queue_type) {
		this.queue_type = queue_type;
	}
	public String getPatient_name() {
		return patient_name;
	}
	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}
	public Long getResourcemstr() {
		return resourcemstr;
	}
	public void setResourcemstr(Long resourcemstr) {
		this.resourcemstr = resourcemstr;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}
	public Long getCreated_by() {
		return created_by;
	}
	public void setCreated_by(Long created_by) {
		this.created_by = created_by;
	}
	public Timestamp getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}
	public Long getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(Long updated_by) {
		this.updated_by = updated_by;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

}
