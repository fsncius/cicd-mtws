package com.rsmurniteguh.webservice.dep.all.model;

public class BpjsSepUpdateParam {
	public BpjsSepUpdateParam() {
		
	}
	private Request request;

    public Request getRequest() { return request; }
    public void setRequest(Request value) { this.request = value; }
    
  //Request.java
    public static class Request {
        private Sep t_sep;

		public Sep getT_sep() {
			return t_sep;
		}

		public void setT_sep(Sep t_sep) {
			this.t_sep = t_sep;
		}

    }

    // TSep.java
    public static  class Sep {
        private String noSep;
        private String klsRawat;
        private String noMR;
        private Rujukan rujukan;
        private String catatan;
        private String diagAwal;
        private Poli poli;
        private Cob cob;
        private Katarak katarak;
        private Skdp skdp;
        private Jaminan jaminan;
        private String noTelp;
        private String user;

        public String getNoSep() { return noSep; }
        public void setNoSep(String value) { this.noSep = value; }

        public String getKlsRawat() { return klsRawat; }
        public void setKlsRawat(String value) { this.klsRawat = value; }

        public String getNoMR() { return noMR; }
        public void setNoMR(String value) { this.noMR = value; }

        public Rujukan getRujukan() { return rujukan; }
        public void setRujukan(Rujukan value) { this.rujukan = value; }

        public String getCatatan() { return catatan; }
        public void setCatatan(String value) { this.catatan = value; }

        public String getDiagAwal() { return diagAwal; }
        public void setDiagAwal(String value) { this.diagAwal = value; }

        public Poli getPoli() { return poli; }
        public void setPoli(Poli value) { this.poli = value; }

        public Cob getCob() { return cob; }
        public void setCob(Cob value) { this.cob = value; }

        public Katarak getKatarak() { return katarak; }
        public void setKatarak(Katarak value) { this.katarak = value; }

        public Skdp getSkdp() { return skdp; }
        public void setSkdp(Skdp value) { this.skdp = value; }

        public Jaminan getJaminan() { return jaminan; }
        public void setJaminan(Jaminan value) { this.jaminan = value; }

        public String getNoTelp() { return noTelp; }
        public void setNoTelp(String value) { this.noTelp = value; }

        public String getUser() { return user; }
        public void setUser(String value) { this.user = value; }
    }

    // Cob.java
    public static  class Cob {
        private String cob;

        public String getCob() { return cob; }
        public void setCob(String value) { this.cob = value; }
    }

    // Jaminan.java
    public static  class Jaminan {
        private String lakaLantas;
        private Penjamin penjamin;

        public String getLakaLantas() { return lakaLantas; }
        public void setLakaLantas(String value) { this.lakaLantas = value; }

        public Penjamin getPenjamin() { return penjamin; }
        public void setPenjamin(Penjamin value) { this.penjamin = value; }
    }

    // Penjamin.java
    public static  class Penjamin {
        private String penjamin;
        private String tglKejadian;
        private String keterangan;
        private Suplesi suplesi;

        public String getPenjamin() { return penjamin; }
        public void setPenjamin(String value) { this.penjamin = value; }

        public String getTglKejadian() { return tglKejadian; }
        public void setTglKejadian(String value) { this.tglKejadian = value; }

        public String getKeterangan() { return keterangan; }
        public void setKeterangan(String value) { this.keterangan = value; }

        public Suplesi getSuplesi() { return suplesi; }
        public void setSuplesi(Suplesi value) { this.suplesi = value; }
    }

    // Suplesi.java
    public static  class Suplesi {
        private String suplesi;
        private String noSepSuplesi;
        private LokasiLaka lokasiLaka;

        public String getSuplesi() { return suplesi; }
        public void setSuplesi(String value) { this.suplesi = value; }

        public String getNoSepSuplesi() { return noSepSuplesi; }
        public void setNoSepSuplesi(String value) { this.noSepSuplesi = value; }

        public LokasiLaka getLokasiLaka() { return lokasiLaka; }
        public void setLokasiLaka(LokasiLaka value) { this.lokasiLaka = value; }
    }

    // LokasiLaka.java
    public static  class LokasiLaka {
        private String kdPropinsi;
        private String kdKabupaten;
        private String kdKecamatan;

        public String getKdPropinsi() { return kdPropinsi; }
        public void setKdPropinsi(String value) { this.kdPropinsi = value; }

        public String getKdKabupaten() { return kdKabupaten; }
        public void setKdKabupaten(String value) { this.kdKabupaten = value; }

        public String getKdKecamatan() { return kdKecamatan; }
        public void setKdKecamatan(String value) { this.kdKecamatan = value; }
    }

    // Katarak.java
    public static class Katarak {
        private String katarak;

        public String getKatarak() { return katarak; }
        public void setKatarak(String value) { this.katarak = value; }
    }

    // Poli.java
    public static  class Poli {
        private String eksekutif;

        public String getEksekutif() { return eksekutif; }
        public void setEksekutif(String value) { this.eksekutif = value; }
    }

    // Rujukan.java
    public static  class Rujukan {
        private String asalRujukan;
        private String tglRujukan;
        private String noRujukan;
        private String ppkRujukan;

        public String getAsalRujukan() { return asalRujukan; }
        public void setAsalRujukan(String value) { this.asalRujukan = value; }

        public String getTglRujukan() { return tglRujukan; }
        public void setTglRujukan(String value) { this.tglRujukan = value; }

        public String getNoRujukan() { return noRujukan; }
        public void setNoRujukan(String value) { this.noRujukan = value; }

        public String getPpkRujukan() { return ppkRujukan; }
        public void setPpkRujukan(String value) { this.ppkRujukan = value; }
    }

    // Skdp.java
    public static  class Skdp {
        private String noSurat;
        private String kodeDPJP;

        public String getNoSurat() { return noSurat; }
        public void setNoSurat(String value) { this.noSurat = value; }

        public String getKodeDPJP() { return kodeDPJP; }
        public void setKodeDPJP(String value) { this.kodeDPJP = value; }
    }
}
