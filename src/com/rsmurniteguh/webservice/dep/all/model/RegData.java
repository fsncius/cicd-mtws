package com.rsmurniteguh.webservice.dep.all.model;

public class RegData {
	private String REGN_NO;
	private String PERSON_NAME;
	private String CP_NAME;
	private String CASHIER_NAME;
	private String SUBSPECIALTY_DESC;
	private String JOB_NO;
	private String REGN_TYPE;
	private String ADDRESS;
	private String REGN_DATE;
	private String CARDNO;
	private String VISIT_NO;
	private String APPOINTMENT_TIME;
	private String SEQ_NO;
	private String GHF_AMT;
	private String ZLF_AMT;
	private String TOTAL_AMT;
	private String AUTO_PAY;
	private String EFFICTIVE_DATETIME;
	private String SESSION_DESC;
	private String PRINTDATE;
	
	
	public String getREGN_NO() {
		return REGN_NO;
	}
	public void setREGN_NO(String rEGN_NO) {
		REGN_NO = rEGN_NO;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getCP_NAME() {
		return CP_NAME;
	}
	public void setCP_NAME(String cP_NAME) {
		CP_NAME = cP_NAME;
	}
	public String getCASHIER_NAME() {
		return CASHIER_NAME;
	}
	public void setCASHIER_NAME(String cASHIER_NAME) {
		CASHIER_NAME = cASHIER_NAME;
	}
	public String getSUBSPECIALTY_DESC() {
		return SUBSPECIALTY_DESC;
	}
	public void setSUBSPECIALTY_DESC(String sUBSPECIALTY_DESC) {
		SUBSPECIALTY_DESC = sUBSPECIALTY_DESC;
	}
	public String getJOB_NO() {
		return JOB_NO;
	}
	public void setJOB_NO(String jOB_NO) {
		JOB_NO = jOB_NO;
	}
	public String getREGN_TYPE() {
		return REGN_TYPE;
	}
	public void setREGN_TYPE(String rEGN_TYPE) {
		REGN_TYPE = rEGN_TYPE;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getREGN_DATE() {
		return REGN_DATE;
	}
	public void setREGN_DATE(String rEGN_DATE) {
		REGN_DATE = rEGN_DATE;
	}
	public String getCARDNO() {
		return CARDNO;
	}
	public void setCARDNO(String cARDNO) {
		CARDNO = cARDNO;
	}
	public String getVISIT_NO() {
		return VISIT_NO;
	}
	public void setVISIT_NO(String vISIT_NO) {
		VISIT_NO = vISIT_NO;
	}
	public String getAPPOINTMENT_TIME() {
		return APPOINTMENT_TIME;
	}
	public void setAPPOINTMENT_TIME(String aPPOINTMENT_TIME) {
		APPOINTMENT_TIME = aPPOINTMENT_TIME;
	}
	public String getSEQ_NO() {
		return SEQ_NO;
	}
	public void setSEQ_NO(String sEQ_NO) {
		SEQ_NO = sEQ_NO;
	}
	public String getGHF_AMT() {
		return GHF_AMT;
	}
	public void setGHF_AMT(String gHF_AMT) {
		GHF_AMT = gHF_AMT;
	}
	public String getZLF_AMT() {
		return ZLF_AMT;
	}
	public void setZLF_AMT(String zLF_AMT) {
		ZLF_AMT = zLF_AMT;
	}
	public String getTOTAL_AMT() {
		return TOTAL_AMT;
	}
	public void setTOTAL_AMT(String tOTAL_AMT) {
		TOTAL_AMT = tOTAL_AMT;
	}
	public String getAUTO_PAY() {
		return AUTO_PAY;
	}
	public void setAUTO_PAY(String aUTO_PAY) {
		AUTO_PAY = aUTO_PAY;
	}
	public String getEFFICTIVE_DATETIME() {
		return EFFICTIVE_DATETIME;
	}
	public void setEFFICTIVE_DATETIME(String eFFICTIVE_DATETIME) {
		EFFICTIVE_DATETIME = eFFICTIVE_DATETIME;
	}
	public String getSESSION_DESC() {
		return SESSION_DESC;
	}
	public void setSESSION_DESC(String sESSION_DESC) {
		SESSION_DESC = sESSION_DESC;
	}
	public String getPRINTDATE() {
		return PRINTDATE;
	}
	public void setPRINTDATE(String pRINTDATE) {
		PRINTDATE = pRINTDATE;
	}
	

	
	
	
	
}
