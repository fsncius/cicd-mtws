package com.rsmurniteguh.webservice.dep.all.model;

public class ListCanReg {

	private String CANCELLED_BY;
	private String CANCELLED_DATETIME;
	private String REMARKS;
	public String getCANCELLED_BY() {
		return CANCELLED_BY;
	}
	public void setCANCELLED_BY(String cANCELLED_BY) {
		CANCELLED_BY = cANCELLED_BY;
	}
	public String getCANCELLED_DATETIME() {
		return CANCELLED_DATETIME;
	}
	public void setCANCELLED_DATETIME(String cANCELLED_DATETIME) {
		CANCELLED_DATETIME = cANCELLED_DATETIME;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	
	
}
