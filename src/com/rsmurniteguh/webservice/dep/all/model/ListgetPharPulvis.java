package com.rsmurniteguh.webservice.dep.all.model;

public class ListgetPharPulvis {
	private String period;
	private String tipe;
	private String jum;
	private String store;
	
	
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public String getJum() {
		return jum;
	}
	public void setJum(String jum) {
		this.jum = jum;
	}
	
}
