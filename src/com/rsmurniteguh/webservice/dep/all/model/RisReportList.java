package com.rsmurniteguh.webservice.dep.all.model;

public class RisReportList {
	private String admission_id;
	private String patient_source;
	private String patient_id;
	private String modality;
	private String exam_code;
	private String perfrmd_item;
	private String patient_name;
	private String completion_flag;
	private String order_status;
	private String PATIENT_CLASS;
	private String SEX;
	private String BIRTH_DATE;
	private String REQUEST_PHYSICIAN;
	private String MEDICAL_DESC;
	private String DIAGNOSIS_DESC;
	private String created_date;
	private String diagnosis_Code;
	
	
	public String getDiagnosis_Code() {
		return diagnosis_Code;
	}
	public void setDiagnosis_Code(String diagnosis_Code) {
		this.diagnosis_Code = diagnosis_Code;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getSEX() {
		return SEX;
	}
	public void setSEX(String sEX) {
		SEX = sEX;
	}
	public String getBIRTH_DATE() {
		return BIRTH_DATE;
	}
	public void setBIRTH_DATE(String bIRTH_DATE) {
		BIRTH_DATE = bIRTH_DATE;
	}
	public String getREQUEST_PHYSICIAN() {
		return REQUEST_PHYSICIAN;
	}
	public void setREQUEST_PHYSICIAN(String rEQUEST_PHYSICIAN) {
		REQUEST_PHYSICIAN = rEQUEST_PHYSICIAN;
	}
	public String getPATIENT_CLASS() {
		return PATIENT_CLASS;
	}
	public void setPATIENT_CLASS(String pATIENT_CLASS) {
		PATIENT_CLASS = pATIENT_CLASS;
	}
	public String getOrder_status() {
		return order_status;
	}
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}
	public String getCompletion_flag() {
		return completion_flag;
	}
	public void setCompletion_flag(String completion_flag) {
		this.completion_flag = completion_flag;
	}
	public String getAdmission_id() {
		return admission_id;
	}
	public void setAdmission_id(String admission_id) {
		this.admission_id = admission_id;
	}
	public String getPatient_source() {
		return patient_source;
	}
	public void setPatient_source(String patient_source) {
		this.patient_source = patient_source;
	}
	public String getModality() {
		return modality;
	}
	public void setModality(String modality) {
		this.modality = modality;
	}
	public String getExam_code() {
		return exam_code;
	}
	public void setExam_code(String exam_code) {
		this.exam_code = exam_code;
	}
	public String getPerfrmd_item() {
		return perfrmd_item;
	}
	public void setPerfrmd_item(String perfrmd_item) {
		this.perfrmd_item = perfrmd_item;
	}
	public String getPatient_name() {
		return patient_name;
	}
	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}
	public String getPatient_id() {
		return patient_id;
	}
	public void setPatient_id(String patient_id) {
		this.patient_id = patient_id;
	}
	public String getMEDICAL_DESC() {
		return MEDICAL_DESC;
	}
	public void setMEDICAL_DESC(String mEDICAL_DESC) {
		MEDICAL_DESC = mEDICAL_DESC;
	}
	public String getDIAGNOSIS_DESC() {
		return DIAGNOSIS_DESC;
	}
	public void setDIAGNOSIS_DESC(String dIAGNOSIS_DESC) {
		DIAGNOSIS_DESC = dIAGNOSIS_DESC;
	}
	
}
