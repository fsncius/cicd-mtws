package com.rsmurniteguh.webservice.dep.all.model;

public class ListInfoViewTrx {
	private String nik;
	private String card_no;
	private String user_name;
	private String trx_type;
	private String invoice_no;
	private String payment_no;
	private String trx_amount;
	private String trx_date;
	private String company;
	
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getCard_no() {
		return card_no;
	}
	public void setCard_no(String card_no) {
		this.card_no = card_no;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getTrx_type() {
		return trx_type;
	}
	public void setTrx_type(String trx_type) {
		this.trx_type = trx_type;
	}
	public String getInvoice_no() {
		return invoice_no;
	}
	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}
	public String getPayment_no() {
		return payment_no;
	}
	public void setPayment_no(String payment_no) {
		this.payment_no = payment_no;
	}
	public String getTrx_amount() {
		return trx_amount;
	}
	public void setTrx_amount(String trx_amount) {
		this.trx_amount = trx_amount;
	}
	public String getTrx_date() {
		return trx_date;
	}
	public void setTrx_date(String trx_date) {
		this.trx_date = trx_date;
	}
	
}
