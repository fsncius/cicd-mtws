package com.rsmurniteguh.webservice.dep.all.model;

public class EmoneyCardDatailInfo {
	private String name;
	private String balance;
	private String cardNo;
	private String cardNoFirst;
	private String nfcNo;
	private Boolean isHaveNewCard;
	
	public Boolean getIsHaveNewCard() {
		return isHaveNewCard;
	}
	public void setIsHaveNewCard(Boolean isHaveNewCard) {
		this.isHaveNewCard = isHaveNewCard;
	}
	public String getCardNoFirst() {
		return cardNoFirst;
	}
	public void setCardNoFirst(String cardNoFirst) {
		this.cardNoFirst = cardNoFirst;
	}
	public String getNfcNo() {
		return nfcNo;
	}
	public void setNfcNo(String nfcNo) {
		this.nfcNo = nfcNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
}
