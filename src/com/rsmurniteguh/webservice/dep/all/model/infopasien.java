package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;
import java.sql.Date;

public class infopasien {
	private BigDecimal Visitid;
	private BigDecimal CardNo;
	private BigDecimal CareProvider;
	private String Sex;
	private Date Birthdate;
	private String PersonName;
	private String Ar1,Ar2,Ar3;
	private String WardDesc;
	private String BedNo;
	private BigDecimal Visiti;
	private String Opmrn;
	private String Ipmrn;
	
	
	
	
	
	public String getOpmrn() {
		return Opmrn;
	}
	public void setOpmrn(String opmrn) {
		Opmrn = opmrn;
	}
	public String getIpmrn() {
		return Ipmrn;
	}
	public void setIpmrn(String ipmrn) {
		Ipmrn = ipmrn;
	}
	public String getWardDesc() {
		return WardDesc;
	}
	public void setWardDesc(String wardDesc) {
		WardDesc = wardDesc;
	}
	public String getBedNo() {
		return BedNo;
	}
	public void setBedNo(String bedNo) {
		BedNo = bedNo;
	}
	public BigDecimal getVisiti() {
		return Visiti;
	}
	public void setVisiti(BigDecimal visiti) {
		Visiti = visiti;
	}
	public String getAr1() {
		return Ar1;
	}
	public void setAr1(String ar1) {
		Ar1 = ar1;
	}
	public String getAr2() {
		return Ar2;
	}
	public void setAr2(String ar2) {
		Ar2 = ar2;
	}
	public String getAr3() {
		return Ar3;
	}
	public void setAr3(String ar3) {
		Ar3 = ar3;
	}
	public String getPersonName() {
		return PersonName;
	}
	public void setPersonName(String personName) {
		PersonName = personName;
	}
	public BigDecimal getVisitid() {
		return Visitid;
	}
	public void setVisitid(BigDecimal visitid) {
		Visitid = visitid;
	}
	public BigDecimal getCardNo() {
		return CardNo;
	}
	public void setCardNo(BigDecimal cardNo) {
		CardNo = cardNo;
	}
	public BigDecimal getCareProvider() {
		return CareProvider;
	}
	public void setCareProvider(BigDecimal careProvider) {
		CareProvider = careProvider;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public Date getBirthdate() {
		return Birthdate;
	}
	public void setBirthdate(Date birthdate) {
		Birthdate = birthdate;
	}
	
	
}
