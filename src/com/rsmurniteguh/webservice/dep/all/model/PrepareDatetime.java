package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigInteger;

public class PrepareDatetime {
 private String dispenseBatchNo;
 private String locationName;
 private String Doctor;
 private String PatientName;
 private String Resep;
 private String PreparedDatetime;
 private String DispenseDatetime;
public String getDispenseBatchNo() {
	return dispenseBatchNo;
}
public void setDispenseBatchNo(String dispenseBatchNo) {
	this.dispenseBatchNo = dispenseBatchNo;
}
public String getLocationName() {
	return locationName;
}
public void setLocationName(String locationName) {
	this.locationName = locationName;
}
public String getDoctor() {
	return Doctor;
}
public void setDoctor(String doctor) {
	Doctor = doctor;
}
public String getPatientName() {
	return PatientName;
}
public void setPatientName(String patientName) {
	PatientName = patientName;
}
public String getResep() {
	return Resep;
}
public void setResep(String resep) {
	Resep = resep;
}
public String getPreparedDatetime() {
	return PreparedDatetime;
}
public void setPreparedDatetime(String preparedDatetime) {
	PreparedDatetime = preparedDatetime;
}
public String getDispenseDatetime() {
	return DispenseDatetime;
}
public void setDispenseDatetime(String dispenseDatetime) {
	DispenseDatetime = dispenseDatetime;
}
 
 
}
