package com.rsmurniteguh.webservice.dep.all.model;

public class AndroidUpdate {
	private String latestVersion;
    private long latestVersionCode;
    private String url;
    private String[] releaseNotes;

    public String getLatestVersion() { return latestVersion; }
    public void setLatestVersion(String value) { this.latestVersion = value; }

    public long getLatestVersionCode() { return latestVersionCode; }
    public void setLatestVersionCode(long value) { this.latestVersionCode = value; }

    public String getURL() { return url; }
    public void setURL(String value) { this.url = value; }

    public String[] getReleaseNotes() { return releaseNotes; }
    public void setReleaseNotes(String[] value) { this.releaseNotes = value; }
}
