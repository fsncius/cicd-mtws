package com.rsmurniteguh.webservice.dep.all.model;

public class Grouper {
	private String noSep;
	private String noKa;
	private String tglMasuk;
	private String tglPulang;
	private String jnsRawat;
	private String klsRawat;
	private String adlSub;
	private String adlChro;
	private String icuInd;
	private String icuLos;
	private String ventHour;
	private String upInd;
	private String upCls;
	private String upLos;
	private String addPay;
	private String brWgt;
	private String disSts;
	private String diag;
	private String pro;
	private String trfPnb;
	private String trfBdh;
	private String trfKon;
	private String trfTa;
	private String trfKep;
	private String trfPnj;
	private String trfRad;
	private String trfLab;
	private String trfPd;
	private String trfReh;
	private String trfKmr;
	private String trfRi;
	private String trfOb;
	private String trfAl;
	private String trfBm;
	private String trfSa;
	private String trfEks;
	private String naDok;
	private String koTrf;
	private String payId;
	private String payCd;
	private String cobCd;
	private String coder;
	
	
	public String getNoSep() {
		return noSep;
	}
	public void setNoSep(String noSep) {
		this.noSep = noSep;
	}
	public String getNoKa() {
		return noKa;
	}
	public void setNoKa(String noKa) {
		this.noKa = noKa;
	}
	public String getTglMasuk() {
		return tglMasuk;
	}
	public void setTglMasuk(String tglMasuk) {
		this.tglMasuk = tglMasuk;
	}
	public String getTglPulang() {
		return tglPulang;
	}
	public void setTglPulang(String tglPulang) {
		this.tglPulang = tglPulang;
	}
	public String getJnsRawat() {
		return jnsRawat;
	}
	public void setJnsRawat(String jnsRawat) {
		this.jnsRawat = jnsRawat;
	}
	public String getKlsRawat() {
		return klsRawat;
	}
	public void setKlsRawat(String klsRawat) {
		this.klsRawat = klsRawat;
	}
	public String getAdlSub() {
		return adlSub;
	}
	public void setAdlSub(String adlSub) {
		this.adlSub = adlSub;
	}
	public String getAdlChro() {
		return adlChro;
	}
	public void setAdlChro(String adlChro) {
		this.adlChro = adlChro;
	}
	public String getIcuInd() {
		return icuInd;
	}
	public void setIcuInd(String icuInd) {
		this.icuInd = icuInd;
	}
	public String getIcuLos() {
		return icuLos;
	}
	public void setIcuLos(String icuLos) {
		this.icuLos = icuLos;
	}
	public String getVentHour() {
		return ventHour;
	}
	public void setVentHour(String ventHour) {
		this.ventHour = ventHour;
	}
	public String getUpInd() {
		return upInd;
	}
	public void setUpInd(String upInd) {
		this.upInd = upInd;
	}
	public String getUpCls() {
		return upCls;
	}
	public void setUpCls(String upCls) {
		this.upCls = upCls;
	}
	public String getUpLos() {
		return upLos;
	}
	public void setUpLos(String upLos) {
		this.upLos = upLos;
	}
	public String getAddPay() {
		return addPay;
	}
	public void setAddPay(String addPay) {
		this.addPay = addPay;
	}
	public String getBrWgt() {
		return brWgt;
	}
	public void setBrWgt(String brWgt) {
		this.brWgt = brWgt;
	}
	public String getDisSts() {
		return disSts;
	}
	public void setDisSts(String disSts) {
		this.disSts = disSts;
	}
	public String getDiag() {
		return diag;
	}
	public void setDiag(String diag) {
		this.diag = diag;
	}
	public String getPro() {
		return pro;
	}
	public void setPro(String pro) {
		this.pro = pro;
	}
	public String getTrfPnb() {
		return trfPnb;
	}
	public void setTrfPnb(String trfPnb) {
		this.trfPnb = trfPnb;
	}
	public String getTrfBdh() {
		return trfBdh;
	}
	public void setTrfBdh(String trfBdh) {
		this.trfBdh = trfBdh;
	}
	public String getTrfKon() {
		return trfKon;
	}
	public void setTrfKon(String trfKon) {
		this.trfKon = trfKon;
	}
	public String getTrfTa() {
		return trfTa;
	}
	public void setTrfTa(String trfTa) {
		this.trfTa = trfTa;
	}
	public String getTrfKep() {
		return trfKep;
	}
	public void setTrfKep(String trfKep) {
		this.trfKep = trfKep;
	}
	public String getTrfPnj() {
		return trfPnj;
	}
	public void setTrfPnj(String trfPnj) {
		this.trfPnj = trfPnj;
	}
	public String getTrfRad() {
		return trfRad;
	}
	public void setTrfRad(String trfRad) {
		this.trfRad = trfRad;
	}
	public String getTrfLab() {
		return trfLab;
	}
	public void setTrfLab(String trfLab) {
		this.trfLab = trfLab;
	}
	public String getTrfPd() {
		return trfPd;
	}
	public void setTrfPd(String trfPd) {
		this.trfPd = trfPd;
	}
	public String getTrfReh() {
		return trfReh;
	}
	public void setTrfReh(String trfReh) {
		this.trfReh = trfReh;
	}
	public String getTrfKmr() {
		return trfKmr;
	}
	public void setTrfKmr(String trfKmr) {
		this.trfKmr = trfKmr;
	}
	public String getTrfRi() {
		return trfRi;
	}
	public void setTrfRi(String trfRi) {
		this.trfRi = trfRi;
	}
	public String getTrfOb() {
		return trfOb;
	}
	public void setTrfOb(String trfOb) {
		this.trfOb = trfOb;
	}
	public String getTrfAl() {
		return trfAl;
	}
	public void setTrfAl(String trfAl) {
		this.trfAl = trfAl;
	}
	public String getTrfBm() {
		return trfBm;
	}
	public void setTrfBm(String trfBm) {
		this.trfBm = trfBm;
	}
	public String getTrfSa() {
		return trfSa;
	}
	public void setTrfSa(String trfSa) {
		this.trfSa = trfSa;
	}
	public String getTrfEks() {
		return trfEks;
	}
	public void setTrfEks(String trfEks) {
		this.trfEks = trfEks;
	}
	public String getNaDok() {
		return naDok;
	}
	public void setNaDok(String naDok) {
		this.naDok = naDok;
	}
	public String getKoTrf() {
		return koTrf;
	}
	public void setKoTrf(String koTrf) {
		this.koTrf = koTrf;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public String getPayCd() {
		return payCd;
	}
	public void setPayCd(String payCd) {
		this.payCd = payCd;
	}
	public String getCobCd() {
		return cobCd;
	}
	public void setCobCd(String cobCd) {
		this.cobCd = cobCd;
	}
	public String getCoder() {
		return coder;
	}
	public void setCoder(String coder) {
		this.coder = coder;
	}
	
	
	
}
