package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class ListViewJenisKamar {
	private BigDecimal VISIT_ID;
	private String TGL;
	private String BED_DESC;
	private String ROOM_DESC;
	private String WARD_DESC;
	private String ACCOMM_DESC;
	public BigDecimal getVISIT_ID() {
		return VISIT_ID;
	}
	public void setVISIT_ID(BigDecimal vISIT_ID) {
		VISIT_ID = vISIT_ID;
	}
	public String getTGL() {
		return TGL;
	}
	public void setTGL(String tGL) {
		TGL = tGL;
	}
	public String getBED_DESC() {
		return BED_DESC;
	}
	public void setBED_DESC(String bED_DESC) {
		BED_DESC = bED_DESC;
	}
	public String getROOM_DESC() {
		return ROOM_DESC;
	}
	public void setROOM_DESC(String rOOM_DESC) {
		ROOM_DESC = rOOM_DESC;
	}
	public String getWARD_DESC() {
		return WARD_DESC;
	}
	public void setWARD_DESC(String wARD_DESC) {
		WARD_DESC = wARD_DESC;
	}
	public String getACCOMM_DESC() {
		return ACCOMM_DESC;
	}
	public void setACCOMM_DESC(String aCCOMM_DESC) {
		ACCOMM_DESC = aCCOMM_DESC;
	}

}
