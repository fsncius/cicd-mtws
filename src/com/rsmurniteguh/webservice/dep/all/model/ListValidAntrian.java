package com.rsmurniteguh.webservice.dep.all.model;

public class ListValidAntrian {

	private String ln;
	private String keterangan;
	
	
	public String getLn() {
		return ln;
	}
	public void setLn(String ln) {
		this.ln = ln;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
}
