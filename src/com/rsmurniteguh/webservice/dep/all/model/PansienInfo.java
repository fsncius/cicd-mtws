package com.rsmurniteguh.webservice.dep.all.model;

public class PansienInfo {
	private String Person_Name;
	private String Sex;
	private String BirthDate;
	private String Marital_Status;
	private String Address;
	private String Nationality;
	private String MobilePhone;
	private String VisitId;
	private String Ward_Desc;
	private String Bed_No;

	public String getWard_Desc() {
		return Ward_Desc;
	}
	public void setWard_Desc(String ward_Desc) {
		Ward_Desc = ward_Desc;
	}
	public String getBed_No() {
		return Bed_No;
	}
	public void setBed_No(String bed_No) {
		Bed_No = bed_No;
	}
	public String getNationality() {
		return Nationality;
	}
	public void setNationality(String nationality) {
		Nationality = nationality;
	}
	public String getMobilePhone() {
		return MobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		MobilePhone = mobilePhone;
	}
	public String getPerson_Name() {
		return Person_Name;
	}
	public void setPerson_Name(String person_Name) {
		Person_Name = person_Name;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public String getBirthDate() {
		return BirthDate;
	}
	public void setBirthDate(String birthDate) {
		BirthDate = birthDate;
	}
	public String getMarital_Status() {
		return Marital_Status;
	}
	public void setMarital_Status(String marital_Status) {
		Marital_Status = marital_Status;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getVisitId() {
		return VisitId;
	}
	public void setVisitId(String visitId) {
		VisitId = visitId;
	}
	
}
