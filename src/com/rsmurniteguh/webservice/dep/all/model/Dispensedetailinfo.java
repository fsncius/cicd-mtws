package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class Dispensedetailinfo {
	private String DispensedDateTime;
	private String Visit_No;
	private String Visit_Date;
	private String PatientName;
	private String age;
	private String Sex;
	private String BirthDate;
	private String Mrtop;
	private String FormNo;
	private String PersonName;
	private String Subspecialitydesc;
	private String EnteredDatetime;
	private String Material_item_name;
	private String VendorName;
	private String ItemUomDesc;
	private BigDecimal DispenseQty;
	private String Itemuoncode;
	private String Price;
	private BigDecimal Price_amt;
	private String Money;
	private BigDecimal Money_Amt;
	private BigDecimal Amt;
	private String Retail_uom;
	private String freq_desc;
	private BigDecimal DurationQty;
	private String DurationDesc;
	private String ROA;
	private String Dosagedesc;
	private String DosageUnit;
	private String QUEUE_NO;
	private String PREPARE_BATCH_NO;
	
	
	
	public String getVisit_Date() {
		return Visit_Date;
	}
	public void setVisit_Date(String visit_Date) {
		Visit_Date = visit_Date;
	}
	
	public String getPREPARE_BATCH_NO() {
		return PREPARE_BATCH_NO;
	}
	public void setPREPARE_BATCH_NO(String pREPARE_BATCH_NO) {
		PREPARE_BATCH_NO = pREPARE_BATCH_NO;
	}
	public String getQUEUE_NO() {
		return QUEUE_NO;
	}
	public void setQUEUE_NO(String qUEUE_NO) {
		QUEUE_NO = qUEUE_NO;
	}
	public String getDispensedDateTime() {
		return DispensedDateTime;
	}
	public void setDispensedDateTime(String dispensedDateTime) {
		DispensedDateTime = dispensedDateTime;
	}
	public String getVisit_No() {
		return Visit_No;
	}
	public void setVisit_No(String visit_No) {
		Visit_No = visit_No;
	}
	public String getPatientName() {
		return PatientName;
	}
	public void setPatientName(String patientName) {
		PatientName = patientName;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public String getBirthDate() {
		return BirthDate;
	}
	public void setBirthDate(String birthDate) {
		BirthDate = birthDate;
	}
	public String getMrtop() {
		return Mrtop;
	}
	public void setMrtop(String mrtop) {
		Mrtop = mrtop;
	}
	public String getFormNo() {
		return FormNo;
	}
	public void setFormNo(String formNo) {
		FormNo = formNo;
	}
	public String getPersonName() {
		return PersonName;
	}
	public void setPersonName(String personName) {
		PersonName = personName;
	}
	public String getSubspecialitydesc() {
		return Subspecialitydesc;
	}
	public void setSubspecialitydesc(String subspecialitydesc) {
		Subspecialitydesc = subspecialitydesc;
	}
	public String getEnteredDatetime() {
		return EnteredDatetime;
	}
	public void setEnteredDatetime(String enteredDatetime) {
		EnteredDatetime = enteredDatetime;
	}
	public String getMaterial_item_name() {
		return Material_item_name;
	}
	public void setMaterial_item_name(String material_item_name) {
		Material_item_name = material_item_name;
	}
	public String getVendorName() {
		return VendorName;
	}
	public void setVendorName(String vendorName) {
		VendorName = vendorName;
	}
	public String getItemUomDesc() {
		return ItemUomDesc;
	}
	public void setItemUomDesc(String itemUomDesc) {
		ItemUomDesc = itemUomDesc;
	}
	public BigDecimal getDispenseQty() {
		return DispenseQty;
	}
	public void setDispenseQty(BigDecimal dispenseQty) {
		DispenseQty = dispenseQty;
	}
	public String getItemuoncode() {
		return Itemuoncode;
	}
	public void setItemuoncode(String itemuoncode) {
		Itemuoncode = itemuoncode;
	}
	public String getPrice() {
		return Price;
	}
	public void setPrice(String price) {
		Price = price;
	}
	public BigDecimal getPrice_amt() {
		return Price_amt;
	}
	public void setPrice_amt(BigDecimal price_amt) {
		Price_amt = price_amt;
	}
	public String getMoney() {
		return Money;
	}
	public void setMoney(String money) {
		Money = money;
	}
	public BigDecimal getMoney_Amt() {
		return Money_Amt;
	}
	public void setMoney_Amt(BigDecimal money_Amt) {
		Money_Amt = money_Amt;
	}
	public BigDecimal getAmt() {
		return Amt;
	}
	public void setAmt(BigDecimal amt) {
		Amt = amt;
	}
	public String getRetail_uom() {
		return Retail_uom;
	}
	public void setRetail_uom(String retail_uom) {
		Retail_uom = retail_uom;
	}
	public String getFreq_desc() {
		return freq_desc;
	}
	public void setFreq_desc(String freq_desc) {
		this.freq_desc = freq_desc;
	}
	public BigDecimal getDurationQty() {
		return DurationQty;
	}
	public void setDurationQty(BigDecimal durationQty) {
		DurationQty = durationQty;
	}
	public String getDurationDesc() {
		return DurationDesc;
	}
	public void setDurationDesc(String durationDesc) {
		DurationDesc = durationDesc;
	}
	public String getROA() {
		return ROA;
	}
	public void setROA(String rOA) {
		ROA = rOA;
	}
	public String getDosagedesc() {
		return Dosagedesc;
	}
	public void setDosagedesc(String dosagedesc) {
		Dosagedesc = dosagedesc;
	}
	public String getDosageUnit() {
		return DosageUnit;
	}
	public void setDosageUnit(String dosageUnit) {
		DosageUnit = dosageUnit;
	}
	
	
	
}
