package com.rsmurniteguh.webservice.dep.all.model;

public class RisAdmissionList {
	private String PATIENT_ID;
	private String PATIENT_NAME;
	private String PATIENT_BIRTH;
	private String PATIENT_AGE;
	private String PATIENT_SEX;
	private String MODALITY;
	private String REQUEST_PHYSICIAN;
	private String REQUEST_DATE;
	private String PERFRMD_START_DATE;
	private String PERFRMD_START_TIME;
	private String REPORT_START_DATE; 
	private String REPORT_START_TIME;
	private String REPORT_UID;
	private String PERFRMD_ITEM;
	private String BODY_PART;
	private String PATIENT_SOURCE;
	private String EXAM_CODE;
	private String ROOM_ID;
	private String ROOM_NAME;
	private String COMPLETION_FLAG;
	private String order_status;
	private String PATIENT_CLASS;
	private String SEX;
	private String BIRTH_DATE;
	private String MEDICAL_DESC;
	private String DIAGNOSIS_DESC;	
	private String admision_Id;	
	private String diagnosis_Code;
	
	public String getDiagnosis_Code() {
		return diagnosis_Code;
	}
	public void setDiagnosis_Code(String diagnosis_Code) {
		this.diagnosis_Code = diagnosis_Code;
	}
	public String getAdmision_Id() {
		return admision_Id;
	}
	public void setAdmision_Id(String admision_Id) {
		this.admision_Id = admision_Id;
	}
	public String getSEX() {
		return SEX;
	}
	public void setSEX(String sEX) {
		SEX = sEX;
	}
	public String getBIRTH_DATE() {
		return BIRTH_DATE;
	}
	public void setBIRTH_DATE(String bIRTH_DATE) {
		BIRTH_DATE = bIRTH_DATE;
	}
	public String getPATIENT_CLASS() {
		return PATIENT_CLASS;
	}
	public void setPATIENT_CLASS(String pATIENT_CLASS) {
		PATIENT_CLASS = pATIENT_CLASS;
	}
	public String getOrder_status() {
		return order_status;
	}
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}
	public String getPATIENT_ID() {
		return PATIENT_ID;
	}
	public void setPATIENT_ID(String pATIENT_ID) {
		PATIENT_ID = pATIENT_ID;
	}
	public String getPATIENT_NAME() {
		return PATIENT_NAME;
	}
	public void setPATIENT_NAME(String pATIENT_NAME) {
		PATIENT_NAME = pATIENT_NAME;
	}
	public String getPATIENT_BIRTH() {
		return PATIENT_BIRTH;
	}
	public void setPATIENT_BIRTH(String pATIENT_BIRTH) {
		PATIENT_BIRTH = pATIENT_BIRTH;
	}
	public String getPATIENT_AGE() {
		return PATIENT_AGE;
	}
	public void setPATIENT_AGE(String pATIENT_AGE) {
		PATIENT_AGE = pATIENT_AGE;
	}
	public String getPATIENT_SEX() {
		return PATIENT_SEX;
	}
	public void setPATIENT_SEX(String pATIENT_SEX) {
		PATIENT_SEX = pATIENT_SEX;
	}
	public String getMODALITY() {
		return MODALITY;
	}
	public void setMODALITY(String mODALITY) {
		MODALITY = mODALITY;
	}
	public String getREQUEST_PHYSICIAN() {
		return REQUEST_PHYSICIAN;
	}
	public void setREQUEST_PHYSICIAN(String rEQUEST_PHYSICIAN) {
		REQUEST_PHYSICIAN = rEQUEST_PHYSICIAN;
	}
	public String getREQUEST_DATE() {
		return REQUEST_DATE;
	}
	public void setREQUEST_DATE(String rEQUEST_DATE) {
		REQUEST_DATE = rEQUEST_DATE;
	}
	public String getPERFRMD_START_DATE() {
		return PERFRMD_START_DATE;
	}
	public void setPERFRMD_START_DATE(String pERFRMD_START_DATE) {
		PERFRMD_START_DATE = pERFRMD_START_DATE;
	}
	public String getPERFRMD_START_TIME() {
		return PERFRMD_START_TIME;
	}
	public void setPERFRMD_START_TIME(String pERFRMD_START_TIME) {
		PERFRMD_START_TIME = pERFRMD_START_TIME;
	}
	public String getREPORT_START_DATE() {
		return REPORT_START_DATE;
	}
	public void setREPORT_START_DATE(String rEPORT_START_DATE) {
		REPORT_START_DATE = rEPORT_START_DATE;
	}
	public String getREPORT_START_TIME() {
		return REPORT_START_TIME;
	}
	public void setREPORT_START_TIME(String rEPORT_START_TIME) {
		REPORT_START_TIME = rEPORT_START_TIME;
	}
	public String getREPORT_UID() {
		return REPORT_UID;
	}
	public void setREPORT_UID(String rEPORT_UID) {
		REPORT_UID = rEPORT_UID;
	}
	public String getPERFRMD_ITEM() {
		return PERFRMD_ITEM;
	}
	public void setPERFRMD_ITEM(String pERFRMD_ITEM) {
		PERFRMD_ITEM = pERFRMD_ITEM;
	}
	public String getBODY_PART() {
		return BODY_PART;
	}
	public void setBODY_PART(String bODY_PART) {
		BODY_PART = bODY_PART;
	}
	public String getPATIENT_SOURCE() {
		return PATIENT_SOURCE;
	}
	public void setPATIENT_SOURCE(String pATIENT_SOURCE) {
		PATIENT_SOURCE = pATIENT_SOURCE;
	}
	public String getEXAM_CODE() {
		return EXAM_CODE;
	}
	public void setEXAM_CODE(String eXAM_CODE) {
		EXAM_CODE = eXAM_CODE;
	}
	public String getROOM_ID() {
		return ROOM_ID;
	}
	public void setROOM_ID(String rOOM_ID) {
		ROOM_ID = rOOM_ID;
	}
	public String getROOM_NAME() {
		return ROOM_NAME;
	}
	public void setROOM_NAME(String rOOM_NAME) {
		ROOM_NAME = rOOM_NAME;
	}
	public String getCOMPLETION_FLAG() {
		return COMPLETION_FLAG;
	}
	public void setCOMPLETION_FLAG(String cOMPLETION_FLAG) {
		COMPLETION_FLAG = cOMPLETION_FLAG;
	}
	public String getMEDICAL_DESC() {
		return MEDICAL_DESC;
	}
	public void setMEDICAL_DESC(String mEDICAL_DESC) {
		MEDICAL_DESC = mEDICAL_DESC;
	}
	public String getDIAGNOSIS_DESC() {
		return DIAGNOSIS_DESC;
	}
	public void setDIAGNOSIS_DESC(String dIAGNOSIS_DESC) {
		DIAGNOSIS_DESC = dIAGNOSIS_DESC;
	}
	
}
