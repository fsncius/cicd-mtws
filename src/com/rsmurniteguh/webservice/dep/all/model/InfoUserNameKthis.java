package com.rsmurniteguh.webservice.dep.all.model;

public class InfoUserNameKthis {

	private String username;
	private String posisi;
	
	

	public String getPosisi() {
		return posisi;
	}

	public void setPosisi(String posisi) {
		this.posisi = posisi;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
