package com.rsmurniteguh.webservice.dep.all.model;

public class ListInfoBalance {
	private String accountbalance_id;
	private String nik;
	private String card_no;
	private String user_name;
	private String balance;
	private String company;
	
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getAccountbalance_id() {
		return accountbalance_id;
	}
	public void setAccountbalance_id(String accountbalance_id) {
		this.accountbalance_id = accountbalance_id;
	}
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getCard_no() {
		return card_no;
	}
	public void setCard_no(String card_no) {
		this.card_no = card_no;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	
	
	
}
