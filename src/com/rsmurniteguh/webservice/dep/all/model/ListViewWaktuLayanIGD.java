package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class ListViewWaktuLayanIGD {
	private BigDecimal VISIT_ID;
	private String ADMISSION_DATETIME;
	private String WAKTUTUNGGU;
	private String PATIENT_CLASS;
	
	public BigDecimal getVISIT_ID() {
		return VISIT_ID;
	}
	public void setVISIT_ID(BigDecimal vISIT_ID) {
		VISIT_ID = vISIT_ID;
	}
	public String getADMISSION_DATETIME() {
		return ADMISSION_DATETIME;
	}
	public void setADMISSION_DATETIME(String aDMISSION_DATETIME) {
		ADMISSION_DATETIME = aDMISSION_DATETIME;
	}
	public String getPATIENT_CLASS() {
		return PATIENT_CLASS;
	}
	public void setPATIENT_CLASS(String pATIENT_CLASS) {
		PATIENT_CLASS = pATIENT_CLASS;
	}
	public String getWAKTUTUNGGU() {
		return WAKTUTUNGGU;
	}
	public void setWAKTUTUNGGU(String wAKTUTUNGGU) {
		WAKTUTUNGGU = wAKTUTUNGGU;
	}
	
}
