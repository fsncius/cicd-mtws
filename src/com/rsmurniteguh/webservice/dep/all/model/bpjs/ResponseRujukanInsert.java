package com.rsmurniteguh.webservice.dep.all.model.bpjs;

import com.rsmurniteguh.webservice.dep.all.model.bpjs.ResponsePeserta.Peserta;

public class ResponseRujukanInsert extends Response{
	private Response response;
	public static class Response{
		private Rujukan rujukan;

		public Rujukan getRujukan() {
			return rujukan;
		}

		public void setRujukan(Rujukan rujukan) {
			this.rujukan = rujukan;
		}
	}
	
	public static class Rujukan{
		private Info AsalRujukan;
		private Info diagnosa;
		private String noRujukan;
		private Peserta peserta;
		private Info poliTujuan;
		private String tglRujukan;
		private Info tujuanRujukan;
		public Info getAsalRujukan() {
			return AsalRujukan;
		}
		public void setAsalRujukan(Info asalRujukan) {
			AsalRujukan = asalRujukan;
		}
		public Info getDiagnosa() {
			return diagnosa;
		}
		public void setDiagnosa(Info diagnosa) {
			this.diagnosa = diagnosa;
		}
		public String getNoRujukan() {
			return noRujukan;
		}
		public void setNoRujukan(String noRujukan) {
			this.noRujukan = noRujukan;
		}
		public Peserta getPeserta() {
			return peserta;
		}
		public void setPeserta(Peserta peserta) {
			this.peserta = peserta;
		}
		public Info getPoliTujuan() {
			return poliTujuan;
		}
		public void setPoliTujuan(Info poliTujuan) {
			this.poliTujuan = poliTujuan;
		}
		public String getTglRujukan() {
			return tglRujukan;
		}
		public void setTglRujukan(String tglRujukan) {
			this.tglRujukan = tglRujukan;
		}
		public Info getTujuanRujukan() {
			return tujuanRujukan;
		}
		public void setTujuanRujukan(Info tujuanRujukan) {
			this.tujuanRujukan = tujuanRujukan;
		}
	}
	
	public Response getResponse() {
		return response;
	}
	public void setResponse(Response response) {
		this.response = response;
	}
}
