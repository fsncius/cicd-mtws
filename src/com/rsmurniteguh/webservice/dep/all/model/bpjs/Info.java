package com.rsmurniteguh.webservice.dep.all.model.bpjs;


public class Info {
	public static Info from (String kode, String nama) {
		Info result = new Info();
		result.setKode(kode);
		result.setNama(nama);
		return result;
	}
    private String kode;
    private String nama;

    public String getKode() { return kode; }
    public void setKode(String value) { this.kode = value; }

    public String getNama() { return nama; }
    public void setNama(String value) { this.nama = value; }
}
