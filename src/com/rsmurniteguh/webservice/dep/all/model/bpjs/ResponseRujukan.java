package com.rsmurniteguh.webservice.dep.all.model.bpjs;


public class ResponseRujukan extends Response{
	private Response response;
	
	public static class ResponseBuilder {
		private Info diagnosa;
		private String keluhan;
	    private String noKunjungan;
	    private Info pelayanan;
	    private ResponsePeserta.Peserta peserta;
	    private Info poliRujukan;
	    private Info provPerujuk;
	    
	    public Info getDiagnosa() {
			return diagnosa;
		}
		public void setDiagnosa(Info diagnosa) {
			this.diagnosa = diagnosa;
		}
		public String getKeluhan() {
			return keluhan;
		}
		public void setKeluhan(String keluhan) {
			this.keluhan = keluhan;
		}
		public String getNoKunjungan() {
			return noKunjungan;
		}
		public void setNoKunjungan(String noKunjungan) {
			this.noKunjungan = noKunjungan;
		}
		public Info getPelayanan() {
			return pelayanan;
		}
		public void setPelayanan(Info pelayanan) {
			this.pelayanan = pelayanan;
		}
		public ResponsePeserta.Peserta getPeserta() {
			return peserta;
		}
		public void setPeserta(ResponsePeserta.Peserta peserta) {
			this.peserta = peserta;
		}
		public Info getPoliRujukan() {
			return poliRujukan;
		}
		public void setPoliRujukan(Info poliRujukan) {
			this.poliRujukan = poliRujukan;
		}
		public Info getProvPerujuk() {
			return provPerujuk;
		}
		public void setProvPerujuk(Info provPerujuk) {
			this.provPerujuk = provPerujuk;
		}
		public String getTglKunjungan() {
			return tglKunjungan;
		}
		public void setTglKunjungan(String tglKunjungan) {
			this.tglKunjungan = tglKunjungan;
		}
		private String tglKunjungan;
	}
	
	public static class Response{
		public static Response from(ResponseBuilder builder) {
			Response result = new Response();
			result.setRujukan(Rujukan.from(builder));
			return result;
		}
		private Rujukan rujukan;
		public Rujukan getRujukan() {
			return rujukan;
		}

		public void setRujukan(Rujukan rujukan) {
			this.rujukan = rujukan;
		}
	}
	
	public static class Rujukan{
		public static Rujukan from(ResponseBuilder builder) {
			Rujukan result = new Rujukan();
			result.setDiagnosa(builder.getDiagnosa());
			result.setKeluhan(builder.getKeluhan());
			result.setNoKunjungan(builder.getNoKunjungan());
			result.setPelayanan(builder.getPelayanan());
			result.setPeserta(builder.getPeserta());
			result.setPoliRujukan(builder.getPoliRujukan());
			result.setProvPerujuk(builder.getProvPerujuk());
			result.setTglKunjungan(builder.getTglKunjungan());
			return result;
		}
		private Info diagnosa;
		private String keluhan;
	    private String noKunjungan;
	    private Info pelayanan;
	    private ResponsePeserta.Peserta peserta;
	    private Info poliRujukan;
	    private Info provPerujuk;
	    private String tglKunjungan;
		private String faskes;
	    
		public Info getDiagnosa() {
			return diagnosa;
		}
		public void setDiagnosa(Info diagnosa) {
			this.diagnosa = diagnosa;
		}
		public String getKeluhan() {
			return keluhan;
		}
		public void setKeluhan(String keluhan) {
			this.keluhan = keluhan;
		}
		public String getNoKunjungan() {
			return noKunjungan;
		}
		public void setNoKunjungan(String noKunjungan) {
			this.noKunjungan = noKunjungan;
		}
		public Info getPelayanan() {
			return pelayanan;
		}
		public void setPelayanan(Info pelayanan) {
			this.pelayanan = pelayanan;
		}
		public ResponsePeserta.Peserta getPeserta() {
			return peserta;
		}
		public void setPeserta(ResponsePeserta.Peserta peserta) {
			this.peserta = peserta;
		}
		public Info getPoliRujukan() {
			return poliRujukan;
		}
		public void setPoliRujukan(Info poliRujukan) {
			this.poliRujukan = poliRujukan;
		}
		public Info getProvPerujuk() {
			return provPerujuk;
		}
		public void setProvPerujuk(Info provPerujuk) {
			this.provPerujuk = provPerujuk;
		}
		public String getTglKunjungan() {
			return tglKunjungan;
		}
		public void setTglKunjungan(String tglKunjungan) {
			this.tglKunjungan = tglKunjungan;
		}
		public String getFaskes() {
			return faskes;
		}
		public void setFaskes(String faskes) {
			this.faskes = faskes;
		}
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
	
//	public static ResponseRujukan from(ResponseRujukanInsert insertResponse) {
//		ResponseRujukan result = new ResponseRujukan();
//		result.setMetaData(insertResponse.getMetaData());
//		ResponseBuilder builder = new ResponseBuilder();
//		ResponseRujukanInsert.Rujukan rujukan = insertResponse.getResponse().getRujukan();
//		builder.setDiagnosa(rujukan.getDiagnosa());
//		builder.setKeluhan(keluhan);
//		builder.setNoKunjungan(rujukan.getNoRujukan());
//		builder.setPelayanan(rujukan.get);
//		builder.setPoliRujukan(rujukan.getPoliTujuan());
//		builder.setProvPerujuk(rujukan.getTujuanRujukan());
//		builder.setpese
//		result.setResponse(Response.from(ResponseBuilder));
//		return result;
//	}
}
