package com.rsmurniteguh.webservice.dep.all.model.bpjs;

import java.math.BigDecimal;

public class RegisterSepParam {
	private String noKa;
	private String jnsPelayanan;
	private String mrn;
	private String poli;
	private String eks;
	private String lakaLantas;
	private String penjamin;
	private String tglKejadian;
	private String ket;
	private String suplesi;
	private String noSuplesi;
	private String kdPro;
	private String kdKec;
	private String kdKab;
	private String noSurat;
	private BigDecimal userId;
	private String userName;
	private Long careProviderId;
	private BigDecimal visitId;
	private String queueNo;
	private String tempatLakaLantas;
	private String asuransi;
	private String faskes;

	public String getNoKa() {
		return noKa;
	}

	public void setNoKa(String noKa) {
		this.noKa = noKa;
	}

	public String getJnsPelayanan() {
		return jnsPelayanan;
	}

	public void setJnsPelayanan(String jenisPelayanan) {
		this.jnsPelayanan = jenisPelayanan;
	}

	public String getMrn() {
		return mrn;
	}

	public void setMrn(String mrn) {
		this.mrn = mrn;
	}

	public String getPoli() {
		return poli;
	}

	public void setPoli(String poli) {
		this.poli = poli;
	}

	public String getEks() {
		return eks;
	}

	public void setEks(String eks) {
		this.eks = eks;
	}

	public String getLakaLantas() {
		return lakaLantas;
	}

	public void setLakaLantas(String lakaLantas) {
		this.lakaLantas = lakaLantas;
	}

	public String getPenjamin() {
		return penjamin;
	}

	public void setPenjamin(String penjamin) {
		this.penjamin = penjamin;
	}

	public String getTglKejadian() {
		return tglKejadian;
	}

	public void setTglKejadian(String tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getSuplesi() {
		return suplesi;
	}

	public void setSuplesi(String suplesi) {
		this.suplesi = suplesi;
	}

	public String getNoSuplesi() {
		return noSuplesi;
	}

	public void setNoSuplesi(String noSuplesi) {
		this.noSuplesi = noSuplesi;
	}

	public String getKdPro() {
		return kdPro;
	}

	public void setKdPro(String kdPro) {
		this.kdPro = kdPro;
	}

	public String getKdKec() {
		return kdKec;
	}

	public void setKdKec(String kdKec) {
		this.kdKec = kdKec;
	}

	public String getKdKab() {
		return kdKab;
	}

	public void setKdKab(String kdKab) {
		this.kdKab = kdKab;
	}

	public String getNoSurat() {
		return noSurat;
	}

	public void setNoSurat(String noSurat) {
		this.noSurat = noSurat;
	}

	public BigDecimal getUserId() {
		return userId;
	}

	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}

	public Long getCareProviderId() {
		return careProviderId;
	}

	public void setCareProviderId(Long careProviderId) {
		this.careProviderId = careProviderId;
	}

	public BigDecimal getVisitId() {
		return visitId;
	}

	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}

	public String getQueueNo() {
		return queueNo;
	}

	public void setQueueNo(String queueNo) {
		this.queueNo = queueNo;
	}

	public String getTempatLakaLantas() {
		return tempatLakaLantas;
	}

	public void setTempatLakaLantas(String tempatLakaLantas) {
		this.tempatLakaLantas = tempatLakaLantas;
	}

	public String getAsuransi() {
		return asuransi;
	}

	public void setAsuransi(String asuransi) {
		this.asuransi = asuransi;
	}

	public String getFaskes() {
		return faskes;
	}

	public void setFaskes(String faskes) {
		this.faskes = faskes;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
