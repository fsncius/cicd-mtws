package com.rsmurniteguh.webservice.dep.all.model.bpjs;

public class Response {
	private MetaData metaData;
	public static class MetaData{
		String code;
		String message;
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		
		public static MetaData fail(String message) {
			return fail(null, message);
		}
		
		public static MetaData fail(String code, String message) {
			MetaData result = new MetaData();
			result.setCode(code == null ? "1001" : code);
			result.setMessage(message);
			return result;
		}
		
		public static MetaData oK() {
			MetaData result = new MetaData();
			result.setMessage("OK");
			result.setCode("200");
			return null;
		}
	}
	public MetaData getMetaData() {
		return metaData;
	}
	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}
	
	public static Response fail(String message) {
		return fail(null, message);
	}
	
	public static Response fail(String code, String message) {
		Response result = new Response();
		result.setMetaData(MetaData.fail(code, message));
		return result;
	}
	
}
