package com.rsmurniteguh.webservice.dep.all.model.bpjs;

public class RepeatTreatmentView {
	private String sep_no;
	private final String ppkRujukan = "0038R091"; // ppk rujukan menjadi murni teguh 
	private String diagnosaAwal  = "Z04.8";
	private String hfis_id;
	private final String faskes = "2";
	private String poli_code;
	public String getSep_no() {
		return sep_no;
	}
	public void setSep_no(String sepNo) {
		this.sep_no = sepNo;
	}
	public String getDiagnosaAwal() {
		return diagnosaAwal;
	}
	public void setDiagnosaAwal(String diagnosaAwal) {
		this.diagnosaAwal = diagnosaAwal;
	}
	public String getPpkRujukan() {
		return ppkRujukan;
	}
	public String getHfis_id() {
		return hfis_id;
	}
	public void setHfis_id(String dpjp) {
		this.hfis_id = dpjp;
	}
	public String getFaskes() {
		return faskes;
	}
	public String getPoli_code() {
		return poli_code;
	}
	public void setPoli_code(String poli_code) {
		this.poli_code = poli_code;
	}
}
