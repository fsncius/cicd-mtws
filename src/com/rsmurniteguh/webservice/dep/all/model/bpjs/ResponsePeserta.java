package com.rsmurniteguh.webservice.dep.all.model.bpjs;

public class ResponsePeserta extends Response{
	public static class Response{
		private Peserta peserta;

		public Peserta getPeserta() {
			return peserta;
		}

		public void setPeserta(Peserta peserta) {
			this.peserta = peserta;
		}

		public static Response from(Peserta peserta) {
			Response result = new Response();
			result.setPeserta(peserta);
			return result;
		}
	}

	public class Peserta{
		private Cob cob;
		private HakKelas hakKelas;
		private Informasi informasi;
		private JenisPeserta jenisPeserta;
		private MR mr;
		private String nama;
		private String nik;
		private String noKartu;
		private String pisa;
		private ProvUmum provUmum;
		private String sex;
		private StatusPeserta statusPeserta;
		private String tglCetakKartu;
		private String tglLahir;
		private String tglTAT;
		private String tglTMT;
		private Umur umur;
		
		public Cob getCob() {
			return cob;
		}
		public void setCob(Cob cob) {
			this.cob = cob;
		}
		public HakKelas getHakKelas() {
			return hakKelas;
		}
		public void setHakKelas(HakKelas hakKelas) {
			this.hakKelas = hakKelas;
		}
		public Informasi getInformasi() {
			return informasi;
		}
		public void setInformasi(Informasi informasi) {
			this.informasi = informasi;
		}
		public JenisPeserta getJenisPeserta() {
			return jenisPeserta;
		}
		public void setJenisPeserta(JenisPeserta jenisPeserta) {
			this.jenisPeserta = jenisPeserta;
		}
		public MR getMr() {
			return mr;
		}
		public void setMr(MR mr) {
			this.mr = mr;
		}
		public String getNama() {
			return nama;
		}
		public void setNama(String nama) {
			this.nama = nama;
		}
		public String getNik() {
			return nik;
		}
		public void setNik(String nik) {
			this.nik = nik;
		}
		public String getNoKartu() {
			return noKartu;
		}
		public void setNoKartu(String noKartu) {
			this.noKartu = noKartu;
		}
		public String getPisa() {
			return pisa;
		}
		public void setPisa(String pisa) {
			this.pisa = pisa;
		}
		public ProvUmum getProvUmum() {
			return provUmum;
		}
		public void setProvUmum(ProvUmum provUmum) {
			this.provUmum = provUmum;
		}
		public String getSex() {
			return sex;
		}
		public void setSex(String sex) {
			this.sex = sex;
		}
		public StatusPeserta getStatusPeserta() {
			return statusPeserta;
		}
		public void setStatusPeserta(StatusPeserta statusPeserta) {
			this.statusPeserta = statusPeserta;
		}
		public String getTglCetakKartu() {
			return tglCetakKartu;
		}
		public void setTglCetakKartu(String tglCetakKartu) {
			this.tglCetakKartu = tglCetakKartu;
		}
		public String getTglLahir() {
			return tglLahir;
		}
		public void setTglLahir(String tglLahir) {
			this.tglLahir = tglLahir;
		}
		public String getTglTAT() {
			return tglTAT;
		}
		public void setTglTAT(String tglTAT) {
			this.tglTAT = tglTAT;
		}
		public String getTglTMT() {
			return tglTMT;
		}
		public void setTglTMT(String tglTMT) {
			this.tglTMT = tglTMT;
		}
		public Umur getUmur() {
			return umur;
		}
		public void setUmur(Umur umur) {
			this.umur = umur;
		}
		
	}
	
	public class Cob{
		private String nmAsuransi;
		private String noAsuransi;
		private String tglTAT;
		private String tglTMT;
		
		public String getNmAsuransi() {
			return nmAsuransi;
		}
		public void setNmAsuransi(String nmAsuransi) {
			this.nmAsuransi = nmAsuransi;
		}
		public String getNoAsuransi() {
			return noAsuransi;
		}
		public void setNoAsuransi(String noAsuransi) {
			this.noAsuransi = noAsuransi;
		}
		public String getTglTAT() {
			return tglTAT;
		}
		public void setTglTAT(String tglTAT) {
			this.tglTAT = tglTAT;
		}
		public String getTglTMT() {
			return tglTMT;
		}
		public void setTglTMT(String tglTMT) {
			this.tglTMT = tglTMT;
		}
		
	}
	
	public class HakKelas{
		private String keterangan;
		private String kode;
		public String getKeterangan() {
			return keterangan;
		}
		public void setKeterangan(String keterangan) {
			this.keterangan = keterangan;
		}
		public String getKode() {
			return kode;
		}
		public void setKode(String kode) {
			this.kode = kode;
		}
	}
	
	public class Informasi{
		private String dinsos;
		private String noSKTM;
		private String prolanisPRB;
		
		public String getDinsos() {
			return dinsos;
		}
		public void setDinsos(String dinsos) {
			this.dinsos = dinsos;
		}
		public String getNoSKTM() {
			return noSKTM;
		}
		public void setNoSKTM(String noSKTM) {
			this.noSKTM = noSKTM;
		}
		public String getProlanisPRB() {
			return prolanisPRB;
		}
		public void setProlanisPRB(String prolanisPRB) {
			this.prolanisPRB = prolanisPRB;
		}
	}
	
	public class JenisPeserta{
		private String keterangan;
		private String kode;
		public String getKeterangan() {
			return keterangan;
		}
		public void setKeterangan(String keterangan) {
			this.keterangan = keterangan;
		}
		public String getKode() {
			return kode;
		}
		public void setKode(String kode) {
			this.kode = kode;
		}
	}
	
	public class MR{
		private String noMR;
		private String noTelepon;
		
		public String getNoMR() {
			return noMR;
		}
		public void setNoMR(String noMR) {
			this.noMR = noMR;
		}
		public String getNoTelepon() {
			return noTelepon;
		}
		public void setNoTelepon(String noTelepon) {
			this.noTelepon = noTelepon;
		}
	}
	
	public class ProvUmum{
		private String kdProvider;
		private String nmProvider;
		public String getKdProvider() {
			return kdProvider;
		}
		public void setKdProvider(String kdProvider) {
			this.kdProvider = kdProvider;
		}
		public String getNmProvider() {
			return nmProvider;
		}
		public void setNmProvider(String nmProvider) {
			this.nmProvider = nmProvider;
		}
	}
	
	public class StatusPeserta{
		private String keterangan;
		private String kode;
		public String getKeterangan() {
			return keterangan;
		}
		public void setKeterangan(String keterangan) {
			this.keterangan = keterangan;
		}
		public String getKode() {
			return kode;
		}
		public void setKode(String kode) {
			this.kode = kode;
		}
	}
	
	public class Umur{
		private String umurSaatPelayanan;
		private String umurSekarang;
		public String getUmurSaatPelayanan() {
			return umurSaatPelayanan;
		}
		public void setUmurSaatPelayanan(String umurSaatPelayanan) {
			this.umurSaatPelayanan = umurSaatPelayanan;
		}
		public String getUmurSekarang() {
			return umurSekarang;
		}
		public void setUmurSekarang(String umurSekarang) {
			this.umurSekarang = umurSekarang;
		}
	}
}
