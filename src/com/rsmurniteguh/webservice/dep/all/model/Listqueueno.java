package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class Listqueueno {
	private String CAREPROVIDER_ID;
	private BigDecimal CARD_NO;
	private String PERSON_NAME;
	public String getCAREPROVIDER_ID() {
		return CAREPROVIDER_ID;
	}
	public void setCAREPROVIDER_ID(String cAREPROVIDER_ID) {
		CAREPROVIDER_ID = cAREPROVIDER_ID;
	}
	public BigDecimal getCARD_NO() {
		return CARD_NO;
	}
	public void setCARD_NO(BigDecimal cARD_NO) {
		CARD_NO = cARD_NO;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	
}
