package com.rsmurniteguh.webservice.dep.all.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ServerPrintView {
	@JsonProperty("SERVERPRINT_ID")
	private String serverPrintId;
	@JsonProperty("REPORT_CODE")
	private String reportCode;
	@JsonProperty("FTP_FULL_PATH")
	private String ftpFullPath;
	@JsonProperty("PRINT_COUNT")
	private Integer printCount;
	@JsonProperty("CREATED_BY")
	private Long createdBy;
	@JsonProperty("CREATED_DATETIME")
	private Timestamp created_datetime;
	@JsonProperty("LAST_UPDATED_BY")
	private Long lastUpdatedBy;
	@JsonProperty("LAST_UPDATED_DATETIME")
	private Timestamp lastUpdatedDatetime;
	@JsonProperty("DEFUNCT_IND")
	private String defunctInd;
	private byte[] pdfFile;
	
	public byte[] getPdfFile() {
		return pdfFile;
	}
	public void setPdfFile(byte[] pdfFile) {
		this.pdfFile = pdfFile;
	}
	public String getServerPrintId() {
		return serverPrintId;
	}
	public void setServerPrintId(String serverPrintId) {
		this.serverPrintId = serverPrintId;
	}
	public String getReportCode() {
		return reportCode;
	}
	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}
	public String getFtpFullPath() {
		return ftpFullPath;
	}
	public void setFtpFullPath(String ftpFullPath) {
		this.ftpFullPath = ftpFullPath;
	}
	public Integer getPrintCount() {
		return printCount;
	}
	public void setPrintCount(Integer printCount) {
		this.printCount = printCount;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreated_datetime() {
		return created_datetime;
	}
	public void setCreated_datetime(Timestamp created_datetime) {
		this.created_datetime = created_datetime;
	}
	public Long getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(Long lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Timestamp getLastUpdatedDatetime() {
		return lastUpdatedDatetime;
	}
	public void setLastUpdatedDatetime(Timestamp lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}
	public String getDefunctInd() {
		return defunctInd;
	}
	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}
	
}
