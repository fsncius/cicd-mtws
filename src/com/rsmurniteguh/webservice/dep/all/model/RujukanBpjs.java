package com.rsmurniteguh.webservice.dep.all.model;

public class RujukanBpjs {
	private String asalFktp;
	private String tglRujukan;
	private String nomorRujukan;
	public String getAsalFktp() {
		return asalFktp;
	}
	public void setAsalFktp(String asalFktp) {
		this.asalFktp = asalFktp;
	}
	public String getTglRujukan() {
		return tglRujukan;
	}
	public void setTglRujukan(String tglRujukan) {
		this.tglRujukan = tglRujukan;
	}
	public String getNomorRujukan() {
		return nomorRujukan;
	}
	public void setNomorRujukan(String nomorRujukan) {
		this.nomorRujukan = nomorRujukan;
	}
	
	
}
