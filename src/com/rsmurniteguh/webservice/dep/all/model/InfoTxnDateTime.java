package com.rsmurniteguh.webservice.dep.all.model;

public class InfoTxnDateTime {

	private String Visit_id;
	private String Admission_Datetime;
	private String Discharge_Datetime;
	private String Patient_Type;
	private String Patient_Class;
	private String Txn_Datetime;
	private String Receipt;
	private String ItemCategoryMstr_Id;
	private String Item_Subcat_Desc;
	private String Txn_Desc;
	private String Gl_Account;
	private String Txn_Amount;
	private String Discount_Amount;
	private String Claimable_Percent;
	private String Person_Name;
	private String Mrn;
	private String Entered_Datetime;
	
	public String getMrn() {
		return Mrn;
	}
	public void setMrn(String mrn) {
		Mrn = mrn;
	}
	public String getPerson_Name() {
		return Person_Name;
	}
	public void setPerson_Name(String person_Name) {
		Person_Name = person_Name;
	}
	public String getVisit_id() {
		return Visit_id;
	}
	public void setVisit_id(String visit_id) {
		Visit_id = visit_id;
	}
	public String getAdmission_Datetime() {
		return Admission_Datetime;
	}
	public void setAdmission_Datetime(String admission_Datetime) {
		Admission_Datetime = admission_Datetime;
	}
	public String getDischarge_Datetime() {
		return Discharge_Datetime;
	}
	public void setDischarge_Datetime(String discharge_Datetime) {
		Discharge_Datetime = discharge_Datetime;
	}
	public String getPatient_Type() {
		return Patient_Type;
	}
	public void setPatient_Type(String patient_Type) {
		Patient_Type = patient_Type;
	}
	public String getPatient_Class() {
		return Patient_Class;
	}
	public void setPatient_Class(String patient_Class) {
		Patient_Class = patient_Class;
	}
	public String getTxn_Datetime() {
		return Txn_Datetime;
	}
	public void setTxn_Datetime(String txn_Datetime) {
		Txn_Datetime = txn_Datetime;
	}
	public String getReceipt() {
		return Receipt;
	}
	public void setReceipt(String receipt) {
		Receipt = receipt;
	}
	public String getItemCategoryMstr_Id() {
		return ItemCategoryMstr_Id;
	}
	public void setItemCategoryMstr_Id(String itemCategoryMstr_Id) {
		ItemCategoryMstr_Id = itemCategoryMstr_Id;
	}
	public String getItem_Subcat_Desc() {
		return Item_Subcat_Desc;
	}
	public void setItem_Subcat_Desc(String item_Subcat_Desc) {
		Item_Subcat_Desc = item_Subcat_Desc;
	}
	public String getTxn_Desc() {
		return Txn_Desc;
	}
	public void setTxn_Desc(String txn_Desc) {
		Txn_Desc = txn_Desc;
	}
	public String getGl_Account() {
		return Gl_Account;
	}
	public void setGl_Account(String gl_Account) {
		Gl_Account = gl_Account;
	}
	public String getTxn_Amount() {
		return Txn_Amount;
	}
	public void setTxn_Amount(String txn_Amount) {
		Txn_Amount = txn_Amount;
	}
	public String getDiscount_Amount() {
		return Discount_Amount;
	}
	public void setDiscount_Amount(String discount_Amount) {
		Discount_Amount = discount_Amount;
	}
	public String getClaimable_Percent() {
		return Claimable_Percent;
	}
	public void setClaimable_Percent(String claimable_Percent) {
		Claimable_Percent = claimable_Percent;
	}
	public String getEntered_Datetime() {
		return Entered_Datetime;
	}
	public void setEntered_Datetime(String entered_Datetime) {
		Entered_Datetime = entered_Datetime;
	}	
}
