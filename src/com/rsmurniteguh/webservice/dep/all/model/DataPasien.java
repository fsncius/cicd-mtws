package com.rsmurniteguh.webservice.dep.all.model;

public class DataPasien {
private boolean success;	
private String CARD_NO;
private String PERSON_NAME;
private String sex;
private String patient_class;
private String BIRTH_DATE;
private String VISIT_ID;
private String ADMISSION_DATETIME;
private String patient_type;
private String Dok;
private String BED_NO;
private String MOBILE_PHONE_NO;
private String religion;
private String ethnic;
private String email;
private String idType;
private String idNo;
private String nationality;
private String bpjsNo;
private String pendidikan;
private String country;
private String city;
private String state;
private String phone;
private String address;


public boolean isSuccess() {
	return success;
}
public void setSuccess(boolean success) {
	this.success = success;
}
public String getMOBILE_PHONE_NO() {
	return MOBILE_PHONE_NO;
}
public void setMOBILE_PHONE_NO(String mOBILE_PHONE_NO) {
	MOBILE_PHONE_NO = mOBILE_PHONE_NO;
}
public String getBED_NO() {
	return BED_NO;
}
public void setBED_NO(String bED_NO) {
	BED_NO = bED_NO;
}
public String getCARD_NO() {
	return CARD_NO;
}
public void setCARD_NO(String cARD_NO) {
	CARD_NO = cARD_NO;
}
public String getPERSON_NAME() {
	return PERSON_NAME;
}
public void setPERSON_NAME(String pERSON_NAME) {
	PERSON_NAME = pERSON_NAME;
}
public String getSex() {
	return sex;
}
public void setSex(String sex) {
	this.sex = sex;
}
public String getPatient_class() {
	return patient_class;
}
public void setPatient_class(String patient_class) {
	this.patient_class = patient_class;
}
public String getBIRTH_DATE() {
	return BIRTH_DATE;
}
public void setBIRTH_DATE(String bIRTH_DATE) {
	BIRTH_DATE = bIRTH_DATE;
}
public String getVISIT_ID() {
	return VISIT_ID;
}
public void setVISIT_ID(String vISIT_ID) {
	VISIT_ID = vISIT_ID;
}
public String getADMISSION_DATETIME() {
	return ADMISSION_DATETIME;
}
public void setADMISSION_DATETIME(String aDMISSION_DATETIME) {
	ADMISSION_DATETIME = aDMISSION_DATETIME;
}
public String getPatient_type() {
	return patient_type;
}
public void setPatient_type(String patient_type) {
	this.patient_type = patient_type;
}
public String getDok() {
	return Dok;
}
public void setDok(String dok) {
	Dok = dok;
}
public String getReligion() {
	return religion;
}
public void setReligion(String religion) {
	this.religion = religion;
}
public String getEthnic() {
	return ethnic;
}
public void setEthnic(String ethnic) {
	this.ethnic = ethnic;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getIdType() {
	return idType;
}
public void setIdType(String idType) {
	this.idType = idType;
}
public String getIdNo() {
	return idNo;
}
public void setIdNo(String idNo) {
	this.idNo = idNo;
}
public String getNationality() {
	return nationality;
}
public void setNationality(String nationality) {
	this.nationality = nationality;
}
public String getBpjsNo() {
	return bpjsNo;
}
public void setBpjsNo(String bpjsNo) {
	this.bpjsNo = bpjsNo;
}
public String getPendidikan() {
	return pendidikan;
}
public void setPendidikan(String pendidikan) {
	this.pendidikan = pendidikan;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}

 
}
