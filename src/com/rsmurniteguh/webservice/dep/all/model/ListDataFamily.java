package com.rsmurniteguh.webservice.dep.all.model;

public class ListDataFamily {

	private String FAMILY_NAME;
	private String BIRTH_DATE_FAMILY;
	private String MOBILE_PHONE_FAMILY;
    private String HUBUNGAN_KELUARGA;
	public String getFAMILY_NAME() {
		return FAMILY_NAME;
	}
	public void setFAMILY_NAME(String fAMILY_NAME) {
		FAMILY_NAME = fAMILY_NAME;
	}
	public String getBIRTH_DATE_FAMILY() {
		return BIRTH_DATE_FAMILY;
	}
	public void setBIRTH_DATE_FAMILY(String bIRTH_DATE_FAMILY) {
		BIRTH_DATE_FAMILY = bIRTH_DATE_FAMILY;
	}
	public String getMOBILE_PHONE_FAMILY() {
		return MOBILE_PHONE_FAMILY;
	}
	public void setMOBILE_PHONE_FAMILY(String mOBILE_PHONE_FAMILY) {
		MOBILE_PHONE_FAMILY = mOBILE_PHONE_FAMILY;
	}
	public String getHUBUNGAN_KELUARGA() {
		return HUBUNGAN_KELUARGA;
	}
	public void setHUBUNGAN_KELUARGA(String hUBUNGAN_KELUARGA) {
		HUBUNGAN_KELUARGA = hUBUNGAN_KELUARGA;
	}
    
}
