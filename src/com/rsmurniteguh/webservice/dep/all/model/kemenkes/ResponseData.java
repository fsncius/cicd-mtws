package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

import com.rsmurniteguh.webservice.dep.biz.kemenkes.NetworkConnection;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.NetworkConnection.RequestProvider.RequestException;

public class ResponseData implements IResponseClass{
	private MetaData metaData;
	private Boolean success;
	private Object data;
	
	public static ResponseData init() {
		ResponseData result = new ResponseData();
		result.setMetaData(MetaData.init());
		result.setSuccess(false);
		result.setData(null);
		return result;
	}
	
	public static ResponseData fail() {
		ResponseData result = new ResponseData();
		MetaData metaData = new MetaData();
		metaData.setCode("201");
		metaData.setMessage("Gagal");
		result.setSuccess(false);
		result.setMetaData(metaData);
		return result;
	}
	
	public static ResponseData fail(String message) {
		ResponseData result = new ResponseData();
		MetaData metaData = new MetaData();
		metaData.setCode("201");
		metaData.setMessage(message);
		result.setMetaData(metaData);
		result.setSuccess(false);
		return result;
	}
	
	public static ResponseData fail(MetaData metaData) {
		ResponseData result = new ResponseData();
		result.setMetaData(metaData);
		result.setSuccess(false);
		return result;
	}
	
	public MetaData getMetaData() {
		return metaData;
	}
	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

//	public static ResponseData error(String message) {
//		ResponseData result = new ResponseData();
//		result.setMetaData(MetaData.fail(message));
//		result.setSuccess(false);
//		result.setData(null);
//		return result;
//	}
	
	public static ResponseData error(Exception exception) {
		ResponseData result = new ResponseData();
		boolean isHttpException = exception instanceof NetworkConnection.RequestProvider.RequestException;
		if(isHttpException) result.setMetaData(MetaData.fail(String.valueOf(((RequestException)exception).getCode()), exception.getMessage()));
		else result.setMetaData(MetaData.fail(exception.getMessage()));
		result.setSuccess(false);
		result.setData(null);
		return result;
	}

	public static ResponseData success(MetaData metaData, Object data) {
		ResponseData result = new ResponseData();
		result.setMetaData(metaData);
		result.setSuccess(true);
		result.setData(data);
		return result;
	}

	public static ResponseData fail(MetaData metaData, Object data) {
		ResponseData result = new ResponseData();
		result.setMetaData(metaData);
		result.setSuccess(false);
		result.setData(data);
		return result;
	}

	@Override
	public void processResponseTarget(IResponseTarget responseTarget) {
		if(responseTarget == null)return;
		this.setData(responseTarget.getData());
		this.setSuccess(responseTarget.isSuccess());
		if(responseTarget.isSuccess())this.setMetaData(MetaData.success(responseTarget.getMessage()));
		else this.setMetaData(MetaData.fail(responseTarget.getMessage()));
		
	}
}
