package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

public interface IResponseClass {
	void processResponseTarget(IResponseTarget responseTarget);
}
