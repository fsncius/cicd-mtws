package com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis;

public class GetParam {
	public GetParam(String nik, String tgl_awal_masuk, String tgl_akhir_masuk) {
		this.setNik(nik);
		this.setTgl_awal_masuk(tgl_awal_masuk);
		this.setTgl_akhir_masuk(tgl_akhir_masuk);
	}
	private String nik;   
	private String tgl_awal_masuk;   
	private String tgl_akhir_masuk;
	
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getTgl_awal_masuk() {
		return tgl_awal_masuk;
	}
	public void setTgl_awal_masuk(String tgl_awal_masuk) {
		this.tgl_awal_masuk = tgl_awal_masuk;
	}
	public String getTgl_akhir_masuk() {
		return tgl_akhir_masuk;
	}
	public void setTgl_akhir_masuk(String tgl_akhir_masuk) {
		this.tgl_akhir_masuk = tgl_akhir_masuk;
	} 
}
