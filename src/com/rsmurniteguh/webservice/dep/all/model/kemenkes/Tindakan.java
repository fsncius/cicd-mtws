package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

public class Tindakan {
	private String kode_icd9;
    private String keterangan;

    public String getKode_icd9() { return kode_icd9; }
    public void setKode_icd9(String value) { this.kode_icd9 = value; }

    public String getKeterangan() { return keterangan; }
    public void setKeterangan(String value) { this.keterangan = value; }
}
