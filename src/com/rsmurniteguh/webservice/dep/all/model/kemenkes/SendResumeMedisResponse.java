package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

public class SendResumeMedisResponse {
	private MetaData metaData;
	private Object data;
	
	public static SendResumeMedisResponse fail() {
		SendResumeMedisResponse result = new SendResumeMedisResponse();
		MetaData metaData = new MetaData();
		metaData.setCode("201");
		metaData.setMessage("Gagal");
		result.setMetaData(metaData);
		return result;
	}
	
	public static SendResumeMedisResponse fail(String message) {
		SendResumeMedisResponse result = new SendResumeMedisResponse();
		MetaData metaData = new MetaData();
		metaData.setCode("201");
		metaData.setMessage(message);
		result.setMetaData(metaData);
		return result;
	}
	
	public static SendResumeMedisResponse fail(MetaData metaData) {
		SendResumeMedisResponse result = new SendResumeMedisResponse();
		result.setMetaData(metaData);
		return result;
	}
	
	public static SendResumeMedisResponse success(String message, Object data) {
		SendResumeMedisResponse result = new SendResumeMedisResponse();
		MetaData metaData = new MetaData();
		metaData.setCode("201");
		metaData.setMessage(message);
		result.setMetaData(metaData);
		result.setData(data);
		return result;
	}
	
	
	public MetaData getMetaData() {
		return metaData;
	}
	public Object getData() {
		return data;
	}
	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
