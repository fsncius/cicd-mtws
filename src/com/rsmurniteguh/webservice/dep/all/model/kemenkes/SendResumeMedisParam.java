package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

import java.math.BigDecimal;

public class SendResumeMedisParam {
	private BigDecimal visitId;
	private String kodeRs;
	private String namaRs;
	private String jenisRujukan;
	private String tempatLahir;

	public String getTempatLahir() {
		return tempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}

	public String getJenisRujukan() {
		return jenisRujukan;
	}

	public void setJenisRujukan(String jenisRujukan) {
		this.jenisRujukan = jenisRujukan;
	}

	public BigDecimal getVisitId() {
		return visitId;
	}

	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}

	public String getKodeRs() {
		return kodeRs;
	}

	public void setKodeRs(String kodeRs) {
		this.kodeRs = kodeRs;
	}

	public String getNamaRs() {
		return namaRs;
	}

	public void setNamaRs(String namaRs) {
		this.namaRs = namaRs;
	}
}
