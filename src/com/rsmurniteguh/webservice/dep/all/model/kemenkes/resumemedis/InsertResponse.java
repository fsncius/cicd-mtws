package com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis;

public class InsertResponse {
	private Boolean success = false;
    private String kodeRsm;
    private String kodeRujukan;
    private String message;
    private Object[] log;
	private String msg;

    public Boolean getSuccess() { return success; }
    public void setSuccess(Boolean value) { this.success = value; }

    public String getKodeRsm() { return kodeRsm; }
    public void setKodeRsm(String value) { this.kodeRsm = value; }

    public String getKodeRujukan() { return kodeRujukan; }
    public void setKodeRujukan(String value) { this.kodeRujukan = value; }

    public String getMessage() { return message; }
    public void setMessage(String value) { this.message = value; }

    public Object[] getLog() { return log; }
    public void setLog(Object[] value) { this.log = value; }
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
}
