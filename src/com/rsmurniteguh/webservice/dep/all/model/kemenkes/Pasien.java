package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

public class Pasien {
	private String no_rm;
    private String nama;
    private String jenis_kelamin;
    private String tempat_lahir;
    private String tanggal_lahir;
    private String no_telp;
    private String nik;

    public String getNo_rm() { return no_rm; }
    public void setNo_rm(String value) { this.no_rm = value; }

    public String getNama() { return nama; }
    public void setNama(String value) { this.nama = value; }

    public String getJenis_kelamin() { return jenis_kelamin; }
    public void setJenis_kelamin(String value) { this.jenis_kelamin = value; }

    public String getTempat_lahir() { return tempat_lahir; }
    public void setTempat_lahir(String value) { this.tempat_lahir = value; }

    public String getTanggal_lahir() { return tanggal_lahir; }
    public void setTanggal_lahir(String value) { this.tanggal_lahir = value; }

    public String getNo_telp() { return no_telp; }
    public void setNo_telp(String value) { this.no_telp = value; }

    public String getNik() { return nik; }
    public void setNik(String value) { this.nik = value; }
}
