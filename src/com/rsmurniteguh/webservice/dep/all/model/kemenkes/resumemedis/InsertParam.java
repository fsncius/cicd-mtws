package com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis;


import com.rsmurniteguh.webservice.dep.all.model.kemenkes.Diagnosa;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.Medis;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.Obat;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.Pasien;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.Tindakan;

public class InsertParam {
	public InsertParam() {
		
	}
	public InsertParam(Pasien pasien, String kodePelayanan, String tanggalRegistrasi, String tanggalKeluar, Diagnosa[] diagnosas) {
		this.pasien = pasien;
		Medis medis = new Medis();
		medis.setKode_pelayanan(kodePelayanan);
		medis.setTanggal_registrasi(tanggalRegistrasi);
		medis.setTanggal_keluar(tanggalKeluar);
		this.diagnosa = diagnosas;
	}
	
	public InsertParam(Pasien pasien, Medis medis, Diagnosa[] diagnosas) {
		this.pasien = pasien;
		this.medis = medis;
		this.diagnosa = diagnosas;
	}
	private Pasien pasien;
    private Medis medis;
    private Diagnosa[] diagnosa;
    private Tindakan[] tindakan;
    private Obat[] obat;
    private Object[] gambar;
    private Object[] lampiran;

    public Pasien getPasien() { return pasien; }
    public void setPasien(Pasien value) { this.pasien = value; }

    public Medis getMedis() { return medis; }
    public void setMedis(Medis value) { this.medis = value; }

    public Diagnosa[] getDiagnosa() { return diagnosa; }
    public void setDiagnosa(Diagnosa[] value) { this.diagnosa = value; }

    public Tindakan[] getTindakan() { return tindakan; }
    public void setTindakan(Tindakan[] value) { this.tindakan = value; }

    public Obat[] getObat() { return obat; }
    public void setObat(Obat[] value) { this.obat = value; }

    public Object[] getGambar() { return gambar; }
    public void setGambar(Object[] value) { this.gambar = value; }

    public Object[] getLampiran() { return lampiran; }
    public void setLampiran(Object[] value) { this.lampiran = value; }
}
