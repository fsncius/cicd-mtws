package com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis;

public class GetResponse {
	private Object[] data;
	private String message;
	public Object[] getData() {
		return data;
	}
	public void setData(Object[] data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
