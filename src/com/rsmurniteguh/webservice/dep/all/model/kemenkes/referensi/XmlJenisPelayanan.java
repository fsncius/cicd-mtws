package com.rsmurniteguh.webservice.dep.all.model.kemenkes.referensi;

import java.util.ArrayList;

public class XmlJenisPelayanan extends Xml{
	private Data data;
	
	public static class Data{
		private ArrayList<DataUmum> datum;

		public ArrayList<DataUmum> getDatum() {
			return datum;
		}

		public void setDatum(ArrayList<DataUmum> datum) {
			this.datum = datum;
		}
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
}
