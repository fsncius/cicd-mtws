package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

public interface IResponseTarget {
	public Object getData();
	public String getMessage();
	public boolean isSuccess();
}
