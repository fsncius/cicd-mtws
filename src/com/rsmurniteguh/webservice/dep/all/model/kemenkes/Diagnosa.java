package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import org.apache.http.util.TextUtils;

import com.rsmurniteguh.webservice.dep.biz.kemenkes.Utils;

public class Diagnosa {
	public static final String DIAGNOSA_MASUK = "DIAGNOSA_MASUK";
	public static final String DIAGNOSA_UTAMA = "DIAGNOSA_UTAMA";
	public static final Object DIAGNOSA_SEKUNDER_I = "DIAGNOSA_SEKUNDER";
	
	private String kode_icd10;
    private String keterangan;
    private String kode_jenis_diagnosa; //01 diagnosa masuk, 02 diagnosa utama, 03 diagnosa sekunder, 04 diagnosa sekunder II, ...
    private String nama_jenis_diagnosa;
    private String id;
    
    
    public enum DiagnosaType{
    	DIAGNOSA_MASUK("Diagnosa Masuk", "01"),
    	DIAGNOSA_UTAMA("Diagnosa Utama", "02"),
    	DIAGNOSA_SEKUNDER_I("Diagnosa Sekunder I", "03");
    	
    	private String name;
    	private String value;
		private Object id;
		DiagnosaType(String name, String value){
    		this.name = name;
    		this.value = value;
    	}
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value == null ? "" : value;
		}
		
		public void setValue(String value) {
			this.value = value;
		}

		public DiagnosaType set(HashMap data) {
			if(data ==null)return this;
			Object target = data.get(this.toString());
			if(target == null)return this;
			if(!(target instanceof BigDecimal))return this;
			this.id = (BigDecimal) data.get(this.toString());
			return this;
		}

		public Object getId() {
			return id;
		}

		private void setId(Object id) {
			this.id = id;
		}

    }

    public String getKode_icd10() { return kode_icd10; }
    public void setKode_icd10(String value) { this.kode_icd10 = value; }

    public String getKeterangan() { return keterangan; }
    public void setKeterangan(String value) { this.keterangan = value; }
	public String getKode_jenis_diagnosa() {
		return kode_jenis_diagnosa;
	}
	public void setKode_jenis_diagnosa(String kode_jenis_diagnosa) {
		this.kode_jenis_diagnosa = kode_jenis_diagnosa;
	}
	public String getNama_jenis_diagnosa() {
		return nama_jenis_diagnosa;
	}
	public void setNama_jenis_diagnosa(String nama_jenis_diagnosa) {
		this.nama_jenis_diagnosa = nama_jenis_diagnosa;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public static Diagnosa from(DiagnosaType target, List<Diagnosa> datas) {
		if(datas == null)return null;
		Diagnosa result = find(target.id, datas);
		result.setKode_jenis_diagnosa(target.getValue());
		result.setNama_jenis_diagnosa(target.getName());
		return result;
	}
	
	private static Diagnosa find(Object id, List<Diagnosa> datas) {
		Diagnosa result = null;
		for(Diagnosa diagnosa : datas) {
			if(diagnosa.getId() == null)continue;
			if(!diagnosa.getId().equals(id))
			result = diagnosa;
			break;
		}
		return result;
	}
}
