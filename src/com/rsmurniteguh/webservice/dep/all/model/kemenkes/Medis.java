package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

public class Medis {
	private String nama_cara_bayar;
    private String nama_ruang;
    private String kode_pelayanan;
    private String diet;
    private String nasehat_pulang;
    private String suhu;
    private String kode_cara_keluar;
    private String nama_dokter;
    private String nama_kelas;
    private String nama_rs_rujukan;
    private String keluhan_utama;
    private String alergi;
    private String nadi;
    private String kode_keadaan_keluar;
    private String kode_rs_rujukan;
    private String tanggal_lanjutan;
    private String skala_nyeri;
    private String frekuensi_nafas;
    private String kode_unit_instalasi;
    private String tanggal_keluar;
    private String pemeriksaan_fisik;
    private String kesadaran;
    private String tekanan_darah;
    private String pemeriksaan_penunjang;
    private String riwayat_penyakit;
    private String tanggal_registrasi;
    private String terapi;
    private String jenis_rujukan;
	public String getJenis_rujukan() {
		return jenis_rujukan;
	}
	public void setJenis_rujukan(String jenis_rujukan) {
		this.jenis_rujukan = jenis_rujukan;
	}
	public String getNama_cara_bayar() {
		return nama_cara_bayar;
	}
	public void setNama_cara_bayar(String nama_cara_bayar) {
		this.nama_cara_bayar = nama_cara_bayar;
	}
	public String getNama_ruang() {
		return nama_ruang;
	}
	public void setNama_ruang(String nama_ruang) {
		this.nama_ruang = nama_ruang;
	}
	public String getKode_pelayanan() {
		return kode_pelayanan;
	}
	public void setKode_pelayanan(String kode_pelayanan) {
		this.kode_pelayanan = kode_pelayanan;
	}
	public String getDiet() {
		return diet;
	}
	public void setDiet(String diet) {
		this.diet = diet;
	}
	public String getNasehat_pulang() {
		return nasehat_pulang;
	}
	public void setNasehat_pulang(String nasehat_pulang) {
		this.nasehat_pulang = nasehat_pulang;
	}
	public String getSuhu() {
		return suhu;
	}
	public void setSuhu(String suhu) {
		this.suhu = suhu;
	}
	public String getKode_cara_keluar() {
		return kode_cara_keluar;
	}
	public void setKode_cara_keluar(String kode_cara_keluar) {
		this.kode_cara_keluar = kode_cara_keluar;
	}
	public String getNama_dokter() {
		return nama_dokter;
	}
	public void setNama_dokter(String nama_dokter) {
		this.nama_dokter = nama_dokter;
	}
	public String getNama_kelas() {
		return nama_kelas;
	}
	public void setNama_kelas(String nama_kelas) {
		this.nama_kelas = nama_kelas;
	}
	public String getNama_rs_rujukan() {
		return nama_rs_rujukan;
	}
	public void setNama_rs_rujukan(String nama_rs_rujukan) {
		this.nama_rs_rujukan = nama_rs_rujukan;
	}
	public String getKeluhan_utama() {
		return keluhan_utama;
	}
	public void setKeluhan_utama(String keluhan_utama) {
		this.keluhan_utama = keluhan_utama;
	}
	public String getAlergi() {
		return alergi;
	}
	public void setAlergi(String alergi) {
		this.alergi = alergi;
	}
	public String getNadi() {
		return nadi;
	}
	public void setNadi(String nadi) {
		this.nadi = nadi;
	}
	public String getKode_keadaan_keluar() {
		return kode_keadaan_keluar;
	}
	public void setKode_keadaan_keluar(String kode_keadaan_keluar) {
		this.kode_keadaan_keluar = kode_keadaan_keluar;
	}
	public String getKode_rs_rujukan() {
		return kode_rs_rujukan;
	}
	public void setKode_rs_rujukan(String kode_rs_rujukan) {
		this.kode_rs_rujukan = kode_rs_rujukan;
	}
	public String getTanggal_lanjutan() {
		return tanggal_lanjutan;
	}
	public void setTanggal_lanjutan(String tanggal_lanjutan) {
		this.tanggal_lanjutan = tanggal_lanjutan;
	}
	public String getSkala_nyeri() {
		return skala_nyeri;
	}
	public void setSkala_nyeri(String skala_nyeri) {
		this.skala_nyeri = skala_nyeri;
	}
	public String getFrekuensi_nafas() {
		return frekuensi_nafas;
	}
	public void setFrekuensi_nafas(String frekuensi_nafas) {
		this.frekuensi_nafas = frekuensi_nafas;
	}
	public String getKode_unit_instalasi() {
		return kode_unit_instalasi;
	}
	public void setKode_unit_instalasi(String kode_unit_instalasi) {
		this.kode_unit_instalasi = kode_unit_instalasi;
	}
	public String getTanggal_keluar() {
		return tanggal_keluar;
	}
	public void setTanggal_keluar(String tanggal_keluar) {
		this.tanggal_keluar = tanggal_keluar;
	}
	public String getPemeriksaan_fisik() {
		return pemeriksaan_fisik;
	}
	public void setPemeriksaan_fisik(String pemeriksaan_fisik) {
		this.pemeriksaan_fisik = pemeriksaan_fisik;
	}
	public String getKesadaran() {
		return kesadaran;
	}
	public void setKesadaran(String kesadaran) {
		this.kesadaran = kesadaran;
	}
	public String getTekanan_darah() {
		return tekanan_darah;
	}
	public void setTekanan_darah(String tekanan_darah) {
		this.tekanan_darah = tekanan_darah;
	}
	public String getPemeriksaan_penunjang() {
		return pemeriksaan_penunjang;
	}
	public void setPemeriksaan_penunjang(String pemeriksaan_penunjang) {
		this.pemeriksaan_penunjang = pemeriksaan_penunjang;
	}
	public String getRiwayat_penyakit() {
		return riwayat_penyakit;
	}
	public void setRiwayat_penyakit(String riwayat_penyakit) {
		this.riwayat_penyakit = riwayat_penyakit;
	}
	public String getTanggal_registrasi() {
		return tanggal_registrasi;
	}
	public void setTanggal_registrasi(String tanggal_registrasi) {
		this.tanggal_registrasi = tanggal_registrasi;
	}
	public String getTerapi() {
		return terapi;
	}
	public void setTerapi(String terapi) {
		this.terapi = terapi;
	}
}
