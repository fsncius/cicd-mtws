package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

public class Obat {
	private String nama;
	private String jumlah;
	private String dosis;
	private String cara_pemberian;
	private String status_obat;
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getJumlah() {
		return jumlah;
	}
	public void setJumlah(String jumlah) {
		this.jumlah = jumlah;
	}
	public String getDosis() {
		return dosis;
	}
	public void setDosis(String dosis) {
		this.dosis = dosis;
	}
	public String getCara_pemberian() {
		return cara_pemberian;
	}
	public void setCara_pemberian(String cara_pemberian) {
		this.cara_pemberian = cara_pemberian;
	}
	public String getStatus_obat() {
		return status_obat;
	}
	public void setStatus_obat(String status_obat) {
		this.status_obat = status_obat;
	}
}
