package com.rsmurniteguh.webservice.dep.all.model.kemenkes.referensi;

import com.rsmurniteguh.webservice.dep.all.model.kemenkes.IResponseTarget;

public class DataUmumResponse implements IResponseTarget {
	private XmlJenisPelayanan xml;

	public XmlJenisPelayanan getXml() {
		return xml;
	}

	public void setXml(XmlJenisPelayanan xml) {
		this.xml = xml;
	}

	@Override
	public Object getData(){
		if(getXml() == null) return null;
		if(getXml().getData() ==null) return null;
		return getXml().getData();
	}

	@Override
	public String getMessage() {
		if(getXml() == null) return "";
		return getXml().getMessage();
	}

	@Override
	public boolean isSuccess() {
		if(getXml() == null) return false;
		if(getXml().getData() ==null) return false;
		return true;
	}
}
