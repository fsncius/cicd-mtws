package com.rsmurniteguh.webservice.dep.all.model.kemenkes;

public class MetaData {
	private String code;
	private String message;
	public static MetaData init() {
		MetaData result = new MetaData();
		result.setCode("1000");
		result.setMessage("Kosong.");
		return result;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public static MetaData fail(String message) {
		MetaData result = new MetaData();
		result.setCode("1001");
		result.setMessage(message);
		return result;
	}
	
	public static MetaData fail(String code, String message) {
		MetaData result = new MetaData();
		result.setCode(code);
		result.setMessage(message);
		return result;
	}

	public static MetaData success(String message) {
		MetaData result = new MetaData();
		result.setCode("200");
		result.setMessage(message);
		return result;
	}
}
