package com.rsmurniteguh.webservice.dep.all.model.eclaim;

import java.sql.Timestamp;

public class ResumeMedis {
	String reportName;
	String fileName;
	String reportCategory;
	Timestamp tglUpload;
	byte[] fileByte;
	
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public byte[] getFileByte() {
		return fileByte;
	}
	public void setFileByte(byte[] fileByte) {
		this.fileByte = fileByte;
	}
	public String getReportCategory() {
		return reportCategory;
	}
	public void setReportCategory(String reportCategory) {
		this.reportCategory = reportCategory;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Timestamp getTglUpload() {
		return tglUpload;
	}
	public void setTglUpload(Timestamp tglUpload) {
		this.tglUpload = tglUpload;
	}
	
}
