package com.rsmurniteguh.webservice.dep.all.model.eclaim;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class BillReport {
	private String personName;
	private String mrnNo;
	private String department;
	private String wardDesc;
	private String visitNo;
	private String iscmCat;
	private String txnCode;
	private String txnDesc;
	private String doctorName;
	private String qtyDesc;
	private String baseUnitPrice;
	private String qty;
	private String txnAmt;
	private String claimableAmt;
	private String depositTxnAmt;
	private String billDatetime;
	private String totalDisc;
	private String admissionDatetime;
	private String dischargeDatetime;
	private String jangka;
	private String diagnosisDesc;
	private String txnDatetime;
	private BigDecimal billId;
	private String cashierName;
	private String remarks;
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getMrnNo() {
		return mrnNo;
	}
	public void setMrnNo(String mrnNo) {
		this.mrnNo = mrnNo;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getWardDesc() {
		return wardDesc;
	}
	public void setWardDesc(String wardDesc) {
		this.wardDesc = wardDesc;
	}
	public String getVisitNo() {
		return visitNo;
	}
	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}
	public String getIscmCat() {
		return iscmCat;
	}
	public void setIscmCat(String iscmCat) {
		this.iscmCat = iscmCat;
	}
	public String getTxnCode() {
		return txnCode;
	}
	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}
	public String getTxnDesc() {
		return txnDesc;
	}
	public void setTxnDesc(String txnDesc) {
		this.txnDesc = txnDesc;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getQtyDesc() {
		return qtyDesc;
	}
	public void setQtyDesc(String qtyDesc) {
		this.qtyDesc = qtyDesc;
	}
	public String getBaseUnitPrice() {
		return baseUnitPrice;
	}
	public void setBaseUnitPrice(String baseUnitPrice) {
		this.baseUnitPrice = baseUnitPrice;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getTxnAmt() {
		return txnAmt;
	}
	public void setTxnAmt(String txnAmt) {
		this.txnAmt = txnAmt;
	}
	public String getClaimableAmt() {
		return claimableAmt;
	}
	public void setClaimableAmt(String claimableAmt) {
		this.claimableAmt = claimableAmt;
	}
	public String getDepositTxnAmt() {
		return depositTxnAmt;
	}
	public void setDepositTxnAmt(String depositTxnAmt) {
		this.depositTxnAmt = depositTxnAmt;
	}
	public String getTotalDisc() {
		return totalDisc;
	}
	public void setTotalDisc(String totalDisc) {
		this.totalDisc = totalDisc;
	}
	public String getJangka() {
		return jangka;
	}
	public void setJangka(String jangka) {
		this.jangka = jangka;
	}
	public String getDiagnosisDesc() {
		return diagnosisDesc;
	}
	public void setDiagnosisDesc(String diagnosisDesc) {
		this.diagnosisDesc = diagnosisDesc;
	}
	public BigDecimal getBillId() {
		return billId;
	}
	public void setBillId(BigDecimal billId) {
		this.billId = billId;
	}
	public String getCashierName() {
		return cashierName;
	}
	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getBillDatetime() {
		return billDatetime;
	}
	public void setBillDatetime(String billDatetime) {
		this.billDatetime = billDatetime;
	}
	public String getAdmissionDatetime() {
		return admissionDatetime;
	}
	public void setAdmissionDatetime(String admissionDatetime) {
		this.admissionDatetime = admissionDatetime;
	}
	public String getDischargeDatetime() {
		return dischargeDatetime;
	}
	public void setDischargeDatetime(String dischargeDatetime) {
		this.dischargeDatetime = dischargeDatetime;
	}
	public String getTxnDatetime() {
		return txnDatetime;
	}
	public void setTxnDatetime(String txnDatetime) {
		this.txnDatetime = txnDatetime;
	}
}
