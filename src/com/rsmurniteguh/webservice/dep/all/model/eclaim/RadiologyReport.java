package com.rsmurniteguh.webservice.dep.all.model.eclaim;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

public class RadiologyReport {
	BigDecimal admissionId;
	Timestamp tglUpload;
	
	public BigDecimal getAdmissionId() {
		return admissionId;
	}

	public void setAdmissionId(BigDecimal admissionId) {
		this.admissionId = admissionId;
	}

	public Timestamp getTglUpload() {
		return tglUpload;
	}

	public void setTglUpload(Timestamp tglUpload) {
		this.tglUpload = tglUpload;
	}
}
