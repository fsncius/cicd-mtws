package com.rsmurniteguh.webservice.dep.all.model.eclaim;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SepReport {	
	String noSep;
	String patientName;
	String patientType;
	String noBPJS;
	Timestamp tglRequest;
	Timestamp tglDischarge;
	String status;
	
	List<SepReportDetail> history = new ArrayList<SepReportDetail>();
	
	public String getNoSep() {
		return noSep;
	}
	public void setNoSep(String noSep) {
		this.noSep = noSep;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public Timestamp getTglRequest() {
		return tglRequest;
	}
	public void setTglRequest(Timestamp tglRequest) {
		this.tglRequest = tglRequest;
	}
	public String getPatientType() {
		return patientType;
	}
	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}
	public String getNoBPJS() {
		return noBPJS;
	}
	public void setNoBPJS(String noBPJS) {
		this.noBPJS = noBPJS;
	}
	public List<SepReportDetail> getHistory() {
		return history;
	}
	public void setHistory(List<SepReportDetail> history) {
		this.history = history;
	}
	public Timestamp getTglDischarge() {
		return tglDischarge;
	}
	public void setTglDischarge(Timestamp tglDischarge) {
		this.tglDischarge = tglDischarge;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
