package com.rsmurniteguh.webservice.dep.all.model.eclaim;

import java.math.BigDecimal;

public class PoctReport {
	private BigDecimal visitId;
	private BigDecimal chargeItemId;
	private BigDecimal chargeEntryId;
	private String sepNo;
	byte[] fileByte;
	
	public BigDecimal getVisitId() {
		return visitId;
	}
	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}
	public BigDecimal getChargeItemId() {
		return chargeItemId;
	}
	public void setChargeItemId(BigDecimal chargeItemId) {
		this.chargeItemId = chargeItemId;
	}
	public BigDecimal getChargeEntryId() {
		return chargeEntryId;
	}
	public void setChargeEntryId(BigDecimal chargeEntryId) {
		this.chargeEntryId = chargeEntryId;
	}
	public String getSepNo() {
		return sepNo;
	}
	public void setSepNo(String sepNo) {
		this.sepNo = sepNo;
	}
	public byte[] getFileByte() {
		return fileByte;
	}
	public void setFileByte(byte[] fileByte) {
		this.fileByte = fileByte;
	}
	
	
	
	
	
}
