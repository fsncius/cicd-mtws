package com.rsmurniteguh.webservice.dep.all.model.eclaim;

import java.sql.Timestamp;

public class LabReport {
	String reportNo;
	String labReportFile;
	Timestamp tglUpload;
	byte[] fileByte;
	
	public byte[] getFileByte() {
		return fileByte;
	}
	public void setFileByte(byte[] fileByte) {
		this.fileByte = fileByte;
	}
	public String getReportNo() {
		return reportNo;
	}
	public void setReportNo(String reportNo) {
		this.reportNo = reportNo;
	}
	public String getLabReportFile() {
		return labReportFile;
	}
	public void setLabReportFile(String labReportFile) {
		this.labReportFile = labReportFile;
	}
	public Timestamp getTglUpload() {
		return tglUpload;
	}
	public void setTglUpload(Timestamp tglUpload) {
		this.tglUpload = tglUpload;
	}
}
