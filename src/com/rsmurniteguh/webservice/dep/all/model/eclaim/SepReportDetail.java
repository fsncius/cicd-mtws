package com.rsmurniteguh.webservice.dep.all.model.eclaim;

import java.sql.Timestamp;

public class SepReportDetail {
	String status;
	String noBA;
	Timestamp createdAt;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNoBA() {
		return noBA;
	}
	public void setNoBA(String noBA) {
		this.noBA = noBA;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
}
