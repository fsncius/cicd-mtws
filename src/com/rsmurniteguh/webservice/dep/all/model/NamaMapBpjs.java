package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class NamaMapBpjs {
private String RESOURCESCHEME_ID;
private String POLI_CODE;
private BigDecimal RESOURCEMSTR_ID;
private String RESOURCE_CODE;
private String RESOURCE_SHORT_CODE;
private String RESOURCE_QUICK_CODE;
private String RESOURCE_NAME;
private String RESOURCE_NAME_LANG1;
private String REGISTRATION_TYPE;
private BigDecimal SUBSPECIALTYMSTR_ID;
private String SUBSPECIALTY_DESC;
private String SUBSPECIALTY_DESC_LANG1;
private BigDecimal CAREPROVIDER_ID;
private String SESSIONCODE;
private String SESSIONDESC;
private String SESSIONDESC1;
private String REGNLIMIT;
private String REGNCOUNT;
private String REGNAVAILABLECOUNT;
private String PATIENT_CLASS_REGN_TYPE;



public String getPOLI_CODE() {
	return POLI_CODE;
}

public void setPOLI_CODE(String pOLI_CODE) {
	POLI_CODE = pOLI_CODE;
}

public BigDecimal getRESOURCEMSTR_ID() {
	return RESOURCEMSTR_ID;
}

public void setRESOURCEMSTR_ID(BigDecimal rESOURCEMSTR_ID) {
	RESOURCEMSTR_ID = rESOURCEMSTR_ID;
}

public String getRESOURCE_CODE() {
	return RESOURCE_CODE;
}

public void setRESOURCE_CODE(String rESOURCE_CODE) {
	RESOURCE_CODE = rESOURCE_CODE;
}

public String getRESOURCE_SHORT_CODE() {
	return RESOURCE_SHORT_CODE;
}

public void setRESOURCE_SHORT_CODE(String rESOURCE_SHORT_CODE) {
	RESOURCE_SHORT_CODE = rESOURCE_SHORT_CODE;
}

public String getRESOURCE_QUICK_CODE() {
	return RESOURCE_QUICK_CODE;
}

public void setRESOURCE_QUICK_CODE(String rESOURCE_QUICK_CODE) {
	RESOURCE_QUICK_CODE = rESOURCE_QUICK_CODE;
}

public String getRESOURCE_NAME() {
	return RESOURCE_NAME;
}

public void setRESOURCE_NAME(String rESOURCE_NAME) {
	RESOURCE_NAME = rESOURCE_NAME;
}

public String getRESOURCE_NAME_LANG1() {
	return RESOURCE_NAME_LANG1;
}

public void setRESOURCE_NAME_LANG1(String rESOURCE_NAME_LANG1) {
	RESOURCE_NAME_LANG1 = rESOURCE_NAME_LANG1;
}

public String getREGISTRATION_TYPE() {
	return REGISTRATION_TYPE;
}

public void setREGISTRATION_TYPE(String rEGISTRATION_TYPE) {
	REGISTRATION_TYPE = rEGISTRATION_TYPE;
}

public BigDecimal getSUBSPECIALTYMSTR_ID() {
	return SUBSPECIALTYMSTR_ID;
}

public void setSUBSPECIALTYMSTR_ID(BigDecimal sUBSPECIALTYMSTR_ID) {
	SUBSPECIALTYMSTR_ID = sUBSPECIALTYMSTR_ID;
}

public String getSUBSPECIALTY_DESC() {
	return SUBSPECIALTY_DESC;
}

public void setSUBSPECIALTY_DESC(String sUBSPECIALTY_DESC) {
	SUBSPECIALTY_DESC = sUBSPECIALTY_DESC;
}

public String getSUBSPECIALTY_DESC_LANG1() {
	return SUBSPECIALTY_DESC_LANG1;
}

public void setSUBSPECIALTY_DESC_LANG1(String sUBSPECIALTY_DESC_LANG1) {
	SUBSPECIALTY_DESC_LANG1 = sUBSPECIALTY_DESC_LANG1;
}

public BigDecimal getCAREPROVIDER_ID() {
	return CAREPROVIDER_ID;
}

public void setCAREPROVIDER_ID(BigDecimal cAREPROVIDER_ID) {
	CAREPROVIDER_ID = cAREPROVIDER_ID;
}

public String getSESSIONCODE() {
	return SESSIONCODE;
}

public void setSESSIONCODE(String sESSIONCODE) {
	SESSIONCODE = sESSIONCODE;
}

public String getSESSIONDESC() {
	return SESSIONDESC;
}

public void setSESSIONDESC(String sESSIONDESC) {
	SESSIONDESC = sESSIONDESC;
}

public String getSESSIONDESC1() {
	return SESSIONDESC1;
}

public void setSESSIONDESC1(String sESSIONDESC1) {
	SESSIONDESC1 = sESSIONDESC1;
}

public String getREGNLIMIT() {
	return REGNLIMIT;
}

public void setREGNLIMIT(String rEGNLIMIT) {
	REGNLIMIT = rEGNLIMIT;
}

public String getREGNCOUNT() {
	return REGNCOUNT;
}

public void setREGNCOUNT(String rEGNCOUNT) {
	REGNCOUNT = rEGNCOUNT;
}

public String getREGNAVAILABLECOUNT() {
	return REGNAVAILABLECOUNT;
}

public void setREGNAVAILABLECOUNT(String rEGNAVAILABLECOUNT) {
	REGNAVAILABLECOUNT = rEGNAVAILABLECOUNT;
}

public String getPATIENT_CLASS_REGN_TYPE() {
	return PATIENT_CLASS_REGN_TYPE;
}

public void setPATIENT_CLASS_REGN_TYPE(String pATIENT_CLASS_REGN_TYPE) {
	PATIENT_CLASS_REGN_TYPE = pATIENT_CLASS_REGN_TYPE;
}

public String getRESOURCESCHEME_ID() {
	return RESOURCESCHEME_ID;
}

public void setRESOURCESCHEME_ID(String rESOURCESCHEME_ID) {
	RESOURCESCHEME_ID = rESOURCESCHEME_ID;
}


}
