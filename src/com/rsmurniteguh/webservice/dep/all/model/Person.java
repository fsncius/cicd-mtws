package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Person {
	private BigDecimal PERSON_ID;
	private BigDecimal STAKEHOLDER_ID;
	private BigDecimal ALT_PERSON_ID;
	private BigDecimal DECEASED_ID;
	private String PERSON_NAME;
	private String sex;
	private Timestamp BIRTH_DATE;
	private String ID_TYPE;
	private String idNo;
	private String title;
	private String MARITAL_STATUS;
	private String NATIONALITY;
	private String MOTHER_LANGUAGE;
	private String RACE;
	private String ETHNIC_GROUP;
	private String RELIGION;
	private String OCCUPATION_GROUP;
	private String BLOOD_GROUP;
	private String RESIDENT_TYPE;
	private String MOBILE_PHONE_NO;
	private String PAGER_NO;
	private String email;
	private String PREFERRED_NAME;
	private String ALIAS_NAME;
	private String DEFUNCT_IND;
	private BigDecimal PREV_UPDATED_BY;
	private Timestamp PREV_UPDATED_DATETIME;
	private BigDecimal LAST_UPDATED_BY;
	private Timestamp LAST_UPDATED_DATETIME;
	private String PSEUDO_AGE_IND;
	private BigDecimal HOUSEHOLD_SIZE;
	private String INCOME_RANGE;
	private String NATIONAL_SERVICE;
	private String EMPLOYEE_NO;
	private String EMPLOYER;
	private String SHORT_CODE;
	private BigDecimal FILEMSTR_ID;
	private String PINYIN_CODE;
	private String COMPANY_NAME;
	private BigDecimal COMPANY_ID;
	private String ETHNIC;
	private String NIK;
	private String REMARKS;
	public BigDecimal getPERSON_ID() {
		return PERSON_ID;
	}
	public void setPERSON_ID(BigDecimal pERSON_ID) {
		PERSON_ID = pERSON_ID;
	}
	public BigDecimal getSTAKEHOLDER_ID() {
		return STAKEHOLDER_ID;
	}
	public void setSTAKEHOLDER_ID(BigDecimal sTAKEHOLDER_ID) {
		STAKEHOLDER_ID = sTAKEHOLDER_ID;
	}
	public BigDecimal getALT_PERSON_ID() {
		return ALT_PERSON_ID;
	}
	public void setALT_PERSON_ID(BigDecimal aLT_PERSON_ID) {
		ALT_PERSON_ID = aLT_PERSON_ID;
	}
	public BigDecimal getDECEASED_ID() {
		return DECEASED_ID;
	}
	public void setDECEASED_ID(BigDecimal dECEASED_ID) {
		DECEASED_ID = dECEASED_ID;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Timestamp getBIRTH_DATE() {
		return BIRTH_DATE;
	}
	public void setBIRTH_DATE(Timestamp bIRTH_DATE) {
		BIRTH_DATE = bIRTH_DATE;
	}
	public String getID_TYPE() {
		return ID_TYPE;
	}
	public void setID_TYPE(String iD_TYPE) {
		ID_TYPE = iD_TYPE;
	}
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMARITAL_STATUS() {
		return MARITAL_STATUS;
	}
	public void setMARITAL_STATUS(String mARITAL_STATUS) {
		MARITAL_STATUS = mARITAL_STATUS;
	}
	public String getNATIONALITY() {
		return NATIONALITY;
	}
	public void setNATIONALITY(String nATIONALITY) {
		NATIONALITY = nATIONALITY;
	}
	public String getMOTHER_LANGUAGE() {
		return MOTHER_LANGUAGE;
	}
	public void setMOTHER_LANGUAGE(String mOTHER_LANGUAGE) {
		MOTHER_LANGUAGE = mOTHER_LANGUAGE;
	}
	public String getRACE() {
		return RACE;
	}
	public void setRACE(String rACE) {
		RACE = rACE;
	}
	public String getETHNIC_GROUP() {
		return ETHNIC_GROUP;
	}
	public void setETHNIC_GROUP(String eTHNIC_GROUP) {
		ETHNIC_GROUP = eTHNIC_GROUP;
	}
	public String getRELIGION() {
		return RELIGION;
	}
	public void setRELIGION(String rELIGION) {
		RELIGION = rELIGION;
	}
	public String getOCCUPATION_GROUP() {
		return OCCUPATION_GROUP;
	}
	public void setOCCUPATION_GROUP(String oCCUPATION_GROUP) {
		OCCUPATION_GROUP = oCCUPATION_GROUP;
	}
	public String getBLOOD_GROUP() {
		return BLOOD_GROUP;
	}
	public void setBLOOD_GROUP(String bLOOD_GROUP) {
		BLOOD_GROUP = bLOOD_GROUP;
	}
	public String getRESIDENT_TYPE() {
		return RESIDENT_TYPE;
	}
	public void setRESIDENT_TYPE(String rESIDENT_TYPE) {
		RESIDENT_TYPE = rESIDENT_TYPE;
	}
	public String getMOBILE_PHONE_NO() {
		return MOBILE_PHONE_NO;
	}
	public void setMOBILE_PHONE_NO(String mOBILE_PHONE_NO) {
		MOBILE_PHONE_NO = mOBILE_PHONE_NO;
	}
	public String getPAGER_NO() {
		return PAGER_NO;
	}
	public void setPAGER_NO(String pAGER_NO) {
		PAGER_NO = pAGER_NO;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPREFERRED_NAME() {
		return PREFERRED_NAME;
	}
	public void setPREFERRED_NAME(String pREFERRED_NAME) {
		PREFERRED_NAME = pREFERRED_NAME;
	}
	public String getALIAS_NAME() {
		return ALIAS_NAME;
	}
	public void setALIAS_NAME(String aLIAS_NAME) {
		ALIAS_NAME = aLIAS_NAME;
	}
	public String getDEFUNCT_IND() {
		return DEFUNCT_IND;
	}
	public void setDEFUNCT_IND(String dEFUNCT_IND) {
		DEFUNCT_IND = dEFUNCT_IND;
	}
	public BigDecimal getPREV_UPDATED_BY() {
		return PREV_UPDATED_BY;
	}
	public void setPREV_UPDATED_BY(BigDecimal pREV_UPDATED_BY) {
		PREV_UPDATED_BY = pREV_UPDATED_BY;
	}
	public Timestamp getPREV_UPDATED_DATETIME() {
		return PREV_UPDATED_DATETIME;
	}
	public void setPREV_UPDATED_DATETIME(Timestamp pREV_UPDATED_DATETIME) {
		PREV_UPDATED_DATETIME = pREV_UPDATED_DATETIME;
	}
	public BigDecimal getLAST_UPDATED_BY() {
		return LAST_UPDATED_BY;
	}
	public void setLAST_UPDATED_BY(BigDecimal lAST_UPDATED_BY) {
		LAST_UPDATED_BY = lAST_UPDATED_BY;
	}
	public Timestamp getLAST_UPDATED_DATETIME() {
		return LAST_UPDATED_DATETIME;
	}
	public void setLAST_UPDATED_DATETIME(Timestamp lAST_UPDATED_DATETIME) {
		LAST_UPDATED_DATETIME = lAST_UPDATED_DATETIME;
	}
	public String getPSEUDO_AGE_IND() {
		return PSEUDO_AGE_IND;
	}
	public void setPSEUDO_AGE_IND(String pSEUDO_AGE_IND) {
		PSEUDO_AGE_IND = pSEUDO_AGE_IND;
	}
	public BigDecimal getHOUSEHOLD_SIZE() {
		return HOUSEHOLD_SIZE;
	}
	public void setHOUSEHOLD_SIZE(BigDecimal hOUSEHOLD_SIZE) {
		HOUSEHOLD_SIZE = hOUSEHOLD_SIZE;
	}
	public String getINCOME_RANGE() {
		return INCOME_RANGE;
	}
	public void setINCOME_RANGE(String iNCOME_RANGE) {
		INCOME_RANGE = iNCOME_RANGE;
	}
	public String getNATIONAL_SERVICE() {
		return NATIONAL_SERVICE;
	}
	public void setNATIONAL_SERVICE(String nATIONAL_SERVICE) {
		NATIONAL_SERVICE = nATIONAL_SERVICE;
	}
	public String getEMPLOYEE_NO() {
		return EMPLOYEE_NO;
	}
	public void setEMPLOYEE_NO(String eMPLOYEE_NO) {
		EMPLOYEE_NO = eMPLOYEE_NO;
	}
	public String getEMPLOYER() {
		return EMPLOYER;
	}
	public void setEMPLOYER(String eMPLOYER) {
		EMPLOYER = eMPLOYER;
	}
	public String getSHORT_CODE() {
		return SHORT_CODE;
	}
	public void setSHORT_CODE(String sHORT_CODE) {
		SHORT_CODE = sHORT_CODE;
	}
	public BigDecimal getFILEMSTR_ID() {
		return FILEMSTR_ID;
	}
	public void setFILEMSTR_ID(BigDecimal fILEMSTR_ID) {
		FILEMSTR_ID = fILEMSTR_ID;
	}
	public String getPINYIN_CODE() {
		return PINYIN_CODE;
	}
	public void setPINYIN_CODE(String pINYIN_CODE) {
		PINYIN_CODE = pINYIN_CODE;
	}
	public String getCOMPANY_NAME() {
		return COMPANY_NAME;
	}
	public void setCOMPANY_NAME(String cOMPANY_NAME) {
		COMPANY_NAME = cOMPANY_NAME;
	}
	public BigDecimal getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(BigDecimal cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getETHNIC() {
		return ETHNIC;
	}
	public void setETHNIC(String eTHNIC) {
		ETHNIC = eTHNIC;
	}
	public String getNIK() {
		return NIK;
	}
	public void setNIK(String nIK) {
		NIK = nIK;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
}
