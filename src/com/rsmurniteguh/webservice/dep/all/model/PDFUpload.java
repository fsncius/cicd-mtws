package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class PDFUpload {
	String mrid;
	BigDecimal userid;
	String fileType;
	String fileName;
	String remarks;
	String defunctInd;
	byte[] fileUpload;
	String fileCat;
	String sepNo;
	
	public String getSepNo() {
		return sepNo;
	}
	public void setSepNo(String sepNo) {
		this.sepNo = sepNo;
	}
	public String getMrid() {
		return mrid;
	}
	public void setMrid(String mrid) {
		this.mrid = mrid;
	}
	public BigDecimal getUserid() {
		return userid;
	}
	public void setUserid(BigDecimal userid) {
		this.userid = userid;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getDefunctInd() {
		return defunctInd;
	}
	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}
	public byte[] getFileUpload() {
		return fileUpload;
	}
	public void setFileUpload(byte[] blob) {
		this.fileUpload = blob;
	}
	public String getFileCat() {
		return fileCat;
	}
	public void setFileCat(String fileCat) {
		this.fileCat = fileCat;
	}
	
	
	
}
