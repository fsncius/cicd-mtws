package com.rsmurniteguh.webservice.dep.all.model.dashboard;


public class InpatientBilling {
	private String person_name;
	private String mrtip;
	private String subspecialty_desc;
	private String ward_desc;
	private String patient_class;
	private String visit_no;
	private String iscm_cat;
	private String txn_code;
	private String txn_desc;
	private String ordered_careprovider_id;
	private String doctor_name;
	private String qty_uom;
	private String base_nit_price;
	private String qty;
	private String txn_amount;
	private String claimable_amount;
	private String deposit_txn_amount;
	private String bill_datetime;
	private String totaldisc;
	private String admission_datetime;
	private String discharge_datetime;
	private String datecnt;
	private String diagnosis_desc;
	private String txn_datetime;
	private String bill_id;
	private String remarks;
	
	public String getPerson_name() {
		return person_name;
	}
	public void setPerson_name(String person_name) {
		this.person_name = person_name;
	}
	public String getMrtip() {
		return mrtip;
	}
	public void setMrtip(String mrtip) {
		this.mrtip = mrtip;
	}
	public String getSubspecialty_desc() {
		return subspecialty_desc;
	}
	public void setSubspecialty_desc(String subspecialty_desc) {
		this.subspecialty_desc = subspecialty_desc;
	}
	public String getWard_desc() {
		return ward_desc;
	}
	public void setWard_desc(String ward_desc) {
		this.ward_desc = ward_desc;
	}
	public String getPatient_class() {
		return patient_class;
	}
	public void setPatient_class(String patient_class) {
		this.patient_class = patient_class;
	}
	public String getVisit_no() {
		return visit_no;
	}
	public void setVisit_no(String visit_no) {
		this.visit_no = visit_no;
	}
	public String getIscm_cat() {
		return iscm_cat;
	}
	public void setIscm_cat(String iscm_cat) {
		this.iscm_cat = iscm_cat;
	}
	public String getTxn_desc() {
		return txn_desc;
	}
	public void setTxn_desc(String txn_desc) {
		this.txn_desc = txn_desc;
	}
	public String getOrdered_careprovider_id() {
		return ordered_careprovider_id;
	}
	public void setOrdered_careprovider_id(String ordered_careprovider_id) {
		this.ordered_careprovider_id = ordered_careprovider_id;
	}
	public String getDoctor_name() {
		return doctor_name;
	}
	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}
	public String getQty_uom() {
		return qty_uom;
	}
	public void setQty_uom(String qty_uom) {
		this.qty_uom = qty_uom;
	}
	public String getBase_nit_price() {
		return base_nit_price;
	}
	public void setBase_nit_price(String base_nit_price) {
		this.base_nit_price = base_nit_price;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getTxn_amount() {
		return txn_amount;
	}
	public void setTxn_amount(String txn_amount) {
		this.txn_amount = txn_amount;
	}
	public String getClaimable_amount() {
		return claimable_amount;
	}
	public void setClaimable_amount(String claimable_amount) {
		this.claimable_amount = claimable_amount;
	}
	public String getDeposit_txn_amount() {
		return deposit_txn_amount;
	}
	public void setDeposit_txn_amount(String deposit_txn_amount) {
		this.deposit_txn_amount = deposit_txn_amount;
	}
	public String getBill_datetime() {
		return bill_datetime;
	}
	public void setBill_datetime(String bill_datetime) {
		this.bill_datetime = bill_datetime;
	}
	public String getTotaldisc() {
		return totaldisc;
	}
	public void setTotaldisc(String totaldisc) {
		this.totaldisc = totaldisc;
	}
	public String getAdmission_datetime() {
		return admission_datetime;
	}
	public void setAdmission_datetime(String admission_datetime) {
		this.admission_datetime = admission_datetime;
	}
	public String getDischarge_datetime() {
		return discharge_datetime;
	}
	public void setDischarge_datetime(String discharge_datetime) {
		this.discharge_datetime = discharge_datetime;
	}
	public String getDatecnt() {
		return datecnt;
	}
	public void setDatecnt(String datecnt) {
		this.datecnt = datecnt;
	}
	public String getDiagnosis_desc() {
		return diagnosis_desc;
	}
	public void setDiagnosis_desc(String diagnosis_desc) {
		this.diagnosis_desc = diagnosis_desc;
	}
	public String getTxn_datetime() {
		return txn_datetime;
	}
	public void setTxn_datetime(String txn_datetime) {
		this.txn_datetime = txn_datetime;
	}
	public String getBill_id() {
		return bill_id;
	}
	public void setBill_id(String bill_id) {
		this.bill_id = bill_id;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getTxn_code() {
		return txn_code;
	}
	public void setTxn_code(String txn_code) {
		this.txn_code = txn_code;
	}
}
