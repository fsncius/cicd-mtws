package com.rsmurniteguh.webservice.dep.all.model.dashboard;

public class SurgeryroomTime {
	private String utilisasi;
	private String otroommstr_id;
	private String otroom_desc;
	private String datetime;

	public String getUtilisasi() {
		return utilisasi;
	}
	public void setUtilisasi(String utilisasi) {
		this.utilisasi = utilisasi;
	}
	public String getOtroommstr_id() {
		return otroommstr_id;
	}
	public void setOtroommstr_id(String otroommstr_id) {
		this.otroommstr_id = otroommstr_id;
	}
	public String getOtroom_desc() {
		return otroom_desc;
	}
	public void setOtroom_desc(String otroom_desc) {
		this.otroom_desc = otroom_desc;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
}
