package com.rsmurniteguh.webservice.dep.all.model.dashboard;

public class DischargePlanning {
	private String status;
	private String jumlah;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJumlah() {
		return jumlah;
	}
	public void setJumlah(String jumlah) {
		this.jumlah = jumlah;
	}
}
