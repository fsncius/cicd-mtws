package com.rsmurniteguh.webservice.dep.all.model.dashboard;

public class EndoskopiAct {
	private String endoskopi;
	private String jumlah;
	public String getEndoskopi() {
		return endoskopi;
	}
	public void setEndoskopi(String endoskopi) {
		this.endoskopi = endoskopi;
	}
	public String getJumlah() {
		return jumlah;
	}
	public void setJumlah(String jumlah) {
		this.jumlah = jumlah;
	}
}
