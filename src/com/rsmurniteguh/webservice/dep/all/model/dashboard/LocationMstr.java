package com.rsmurniteguh.webservice.dep.all.model.dashboard;

public class LocationMstr {
	private String locationMstr_id;
	private String location_type;
	private String location_code;
	private String location_name;
	private String location_desc;
	private String short_code;
	private String address;
	
	public String getLocationMstr_id() {
		return locationMstr_id;
	}
	public void setLocationMstr_id(String locationMstr_id) {
		this.locationMstr_id = locationMstr_id;
	}
	public String getLocation_type() {
		return location_type;
	}
	public void setLocation_type(String location_type) {
		this.location_type = location_type;
	}
	public String getLocation_code() {
		return location_code;
	}
	public void setLocation_code(String location_code) {
		this.location_code = location_code;
	}
	public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
	public String getLocation_desc() {
		return location_desc;
	}
	public void setLocation_desc(String location_desc) {
		this.location_desc = location_desc;
	}
	public String getShort_code() {
		return short_code;
	}
	public void setShort_code(String short_code) {
		this.short_code = short_code;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
