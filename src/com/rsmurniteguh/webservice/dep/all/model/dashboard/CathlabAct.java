package com.rsmurniteguh.webservice.dep.all.model.dashboard;

public class CathlabAct {
	private String cathlab;
	private String jumlah;
	public String getJumlah() {
		return jumlah;
	}
	public void setJumlah(String jumlah) {
		this.jumlah = jumlah;
	}
	public String getCathlab() {
		return cathlab;
	}
	public void setCathlab(String cathlab) {
		this.cathlab = cathlab;
	}
}
