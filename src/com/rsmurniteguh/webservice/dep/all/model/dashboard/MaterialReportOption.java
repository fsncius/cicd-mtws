package com.rsmurniteguh.webservice.dep.all.model.dashboard;

import java.math.BigDecimal;

public class MaterialReportOption {
	private String type;
	private BigDecimal slow_moving;
	private BigDecimal dead_stock;
	private BigDecimal numberset;
	public BigDecimal getSlow_moving() {
		return slow_moving;
	}
	public void setSlow_moving(BigDecimal slow_moving) {
		this.slow_moving = slow_moving;
	}
	public BigDecimal getDead_stock() {
		return dead_stock;
	}
	public void setDead_stock(BigDecimal dead_stock) {
		this.dead_stock = dead_stock;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public BigDecimal getNumberset() {
		return numberset;
	}
	public void setNumberset(BigDecimal numberset) {
		this.numberset = numberset;
	}
}
