package com.rsmurniteguh.webservice.dep.all.model.dashboard;

public class UtilizationRadiology {
	private String prosedur;
	private String total_prosedur;
	
	public String getTotal_prosedur() {
		return total_prosedur;
	}
	public void setTotal_prosedur(String total_prosedur) {
		this.total_prosedur = total_prosedur;
	}
	public String getProsedur() {
		return prosedur;
	}
	public void setProsedur(String prosedur) {
		this.prosedur = prosedur;
	}
	
}
