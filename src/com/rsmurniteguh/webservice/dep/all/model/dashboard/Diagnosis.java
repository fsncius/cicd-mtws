package com.rsmurniteguh.webservice.dep.all.model.dashboard;

import java.math.BigDecimal;

public class Diagnosis {
	private String diagnosis_code;
	private String diagnosis_desc;
	private BigDecimal jumlah_diagnosa;
	private String jenis;
	

	public String getDiagnosis_code() {
		return diagnosis_code;
	}
	public void setDiagnosis_code(String diagnosis_code) {
		this.diagnosis_code = diagnosis_code;
	}
	public String getDiagnosis_desc() {
		return diagnosis_desc;
	}
	public void setDiagnosis_desc(String diagnosis_desc) {
		this.diagnosis_desc = diagnosis_desc;
	}
	public BigDecimal getJumlah_diagnosa() {
		return jumlah_diagnosa;
	}
	public void setJumlah_diagnosa(BigDecimal jumlah_diagnosa) {
		this.jumlah_diagnosa = jumlah_diagnosa;
	}
	public String getJenis() {
		return jenis;
	}
	public void setJenis(String jenis) {
		this.jenis = jenis;
	}
}
