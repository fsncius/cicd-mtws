package com.rsmurniteguh.webservice.dep.all.model.dashboard;

import java.math.BigDecimal;

public class PatientVisit {
	private String patient_class;
	private String visit_type;
	private BigDecimal patient_count;
	
	public String getPatient_class() {
		return patient_class;
	}
	public void setPatient_class(String patient_class) {
		this.patient_class = patient_class;
	}
	public String getVisit_type() {
		return visit_type;
	}
	public void setVisit_type(String visit_type) {
		this.visit_type = visit_type;
	}
	public BigDecimal getPatient_count() {
		return patient_count;
	}
	public void setPatient_count(BigDecimal patient_count) {
		this.patient_count = patient_count;
	}
}
