package com.rsmurniteguh.webservice.dep.all.model.dashboard;

public class TopOperation {
	private String resource_name;
	private String careprovider_id;
	private String jlh_ot;
	private String self_define_desc;
	
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name(String resource_name) {
		this.resource_name = resource_name;
	}
	public String getCareprovider_id() {
		return careprovider_id;
	}
	public void setCareprovider_id(String careprovider_id) {
		this.careprovider_id = careprovider_id;
	}
	public String getJlh_ot() {
		return jlh_ot;
	}
	public void setJlh_ot(String jlh_ot) {
		this.jlh_ot = jlh_ot;
	}
	public String getSelf_define_desc() {
		return self_define_desc;
	}
	public void setSelf_define_desc(String self_define_desc) {
		this.self_define_desc = self_define_desc;
	}
	
}
