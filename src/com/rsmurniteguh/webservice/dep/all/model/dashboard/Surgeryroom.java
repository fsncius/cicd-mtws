package com.rsmurniteguh.webservice.dep.all.model.dashboard;

public class Surgeryroom {
	private String surgeon_name;
	private String patient_name;
	private String card_no;
	private String pre_diagnosis_code;
	private String pre_diagnosis_desc;
	private String post_diagnosis_code;
	private String post_diagnosis_desc;
	private String operation_start_datetime;
	private String operation_end_datetime;
	private String duration;
	private String request_remarks;
	
	public String getSurgeon_name() {
		return surgeon_name;
	}
	public void setSurgeon_name(String surgeon_name) {
		this.surgeon_name = surgeon_name;
	}
	public String getPatient_name() {
		return patient_name;
	}
	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}
	public String getCard_no() {
		return card_no;
	}
	public void setCard_no(String card_no) {
		this.card_no = card_no;
	}
	public String getPre_diagnosis_code() {
		return pre_diagnosis_code;
	}
	public void setPre_diagnosis_code(String pre_diagnosis_code) {
		this.pre_diagnosis_code = pre_diagnosis_code;
	}
	public String getPre_diagnosis_desc() {
		return pre_diagnosis_desc;
	}
	public void setPre_diagnosis_desc(String pre_diagnosis_desc) {
		this.pre_diagnosis_desc = pre_diagnosis_desc;
	}
	public String getPost_diagnosis_code() {
		return post_diagnosis_code;
	}
	public void setPost_diagnosis_code(String post_diagnosis_code) {
		this.post_diagnosis_code = post_diagnosis_code;
	}
	public String getPost_diagnosis_desc() {
		return post_diagnosis_desc;
	}
	public void setPost_diagnosis_desc(String post_diagnosis_desc) {
		this.post_diagnosis_desc = post_diagnosis_desc;
	}
	public String getOperation_start_datetime() {
		return operation_start_datetime;
	}
	public void setOperation_start_datetime(String operation_start_datetime) {
		this.operation_start_datetime = operation_start_datetime;
	}
	public String getOperation_end_datetime() {
		return operation_end_datetime;
	}
	public void setOperation_end_datetime(String operation_end_datetime) {
		this.operation_end_datetime = operation_end_datetime;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getRequest_remarks() {
		return request_remarks;
	}
	public void setRequest_remarks(String request_remarks) {
		this.request_remarks = request_remarks;
	}
	
}
