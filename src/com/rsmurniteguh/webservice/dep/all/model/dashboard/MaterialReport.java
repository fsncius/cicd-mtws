package com.rsmurniteguh.webservice.dep.all.model.dashboard;

public class MaterialReport {
	private String material_item_type;
	private String cat;
	private String material_item_code;
	private String material_item_name;
	private String expiry_date;
	private String balance_qty;
	private String sales_price;
	private String whosale_price;
	private String location;

	public String getMaterial_item_type() {
		return material_item_type;
	}
	public void setMaterial_item_type(String material_item_type) {
		this.material_item_type = material_item_type;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public String getMaterial_item_code() {
		return material_item_code;
	}
	public void setMaterial_item_code(String material_item_code) {
		this.material_item_code = material_item_code;
	}
	public String getMaterial_item_name() {
		return material_item_name;
	}
	public void setMaterial_item_name(String material_item_name) {
		this.material_item_name = material_item_name;
	}
	public String getExpiry_date() {
		return expiry_date;
	}
	public void setExpiry_date(String expiry_date) {
		this.expiry_date = expiry_date;
	}
	public String getBalance_qty() {
		return balance_qty;
	}
	public void setBalance_qty(String balance_qty) {
		this.balance_qty = balance_qty;
	}
	public String getSales_price() {
		return sales_price;
	}
	public void setSales_price(String sales_price) {
		this.sales_price = sales_price;
	}
	public String getWhosale_price() {
		return whosale_price;
	}
	public void setWhosale_price(String whosale_price) {
		this.whosale_price = whosale_price;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
}
