package com.rsmurniteguh.webservice.dep.all.model;

public class ListKamarHiv {
	private String mrn;
	private String namaPasien;
	private String ruangan;
	private String kamar;
	private String nomor;
	private String created_at;
	private String tgltes;
	private String sgt;
	private String tglhsl;
	
	public String getMrn() {
		return mrn;
	}
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	public String getNamaPasien() {
		return namaPasien;
	}
	public void setNamaPasien(String namaPasien) {
		this.namaPasien = namaPasien;
	}
	public String getRuangan() {
		return ruangan;
	}
	public void setRuangan(String ruangan) {
		this.ruangan = ruangan;
	}
	public String getKamar() {
		return kamar;
	}
	public void setKamar(String kamar) {
		this.kamar = kamar;
	}
	public String getNomor() {
		return nomor;
	}
	public void setNomor(String nomor) {
		this.nomor = nomor;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getTgltes() {
		return tgltes;
	}
	public void setTgltes(String tgltes) {
		this.tgltes = tgltes;
	}
	public String getTglhsl() {
		return tglhsl;
	}
	public void setTglhsl(String tglhsl) {
		this.tglhsl = tglhsl;
	}
	public String getSgt() {
		return sgt;
	}
	public void setSgt(String sgt) {
		this.sgt = sgt;
	}

	
	
	
}
