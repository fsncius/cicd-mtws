package com.rsmurniteguh.webservice.dep.all.model;

import java.util.ArrayList;

public class GetServerListPrintParam {
	private ArrayList<String> reportCodes;

	public ArrayList<String> getReportCodes() {
		return reportCodes;
	}

	public void setReportCodes(ArrayList<String> reportCodes) {
		this.reportCodes = reportCodes;
	}
}
