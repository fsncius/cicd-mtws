package com.rsmurniteguh.webservice.dep.all.model;

public class ListRuangan {
	
	private String ruangan;
	private String nilai;
	private String subspecialty;
	
	public String getRuangan() {
		return ruangan;
	}
	public void setRuangan(String ruangan) {
		this.ruangan = ruangan;
	}
	public String getNilai() {
		return nilai;
	}
	public void setNilai(String nilai) {
		this.nilai = nilai;
	}
	public String getSubspecialty() {
		return subspecialty;
	}
	public void setSubspecialty(String subspecialty) {
		this.subspecialty = subspecialty;
	}
	
	

}
