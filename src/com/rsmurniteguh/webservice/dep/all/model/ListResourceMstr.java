package com.rsmurniteguh.webservice.dep.all.model;

public class ListResourceMstr {
	private String RESOURCEMSTR_ID;
	private String RESOURCE_NAME;
	private String resource_code;
	private String defunct_ind;
	private String CAREPROVIDER_ID;
	private String POLI_CODE;
	private String SESSION_DESC;
	private String MON_IND;
	private String TUE_IND;
	private String WED_IND;
	private String THU_IND;
	private String FRI_IND;
	private String SAT_IND;
	private String SUN_IND;
	
	
	
	public String getSESSION_DESC() {
		return SESSION_DESC;
	}
	public void setSESSION_DESC(String sESSION_DESC) {
		SESSION_DESC = sESSION_DESC;
	}
	public String getMON_IND() {
		return MON_IND;
	}
	public void setMON_IND(String mON_IND) {
		MON_IND = mON_IND;
	}
	public String getTUE_IND() {
		return TUE_IND;
	}
	public void setTUE_IND(String tUE_IND) {
		TUE_IND = tUE_IND;
	}
	public String getWED_IND() {
		return WED_IND;
	}
	public void setWED_IND(String wED_IND) {
		WED_IND = wED_IND;
	}
	public String getTHU_IND() {
		return THU_IND;
	}
	public void setTHU_IND(String tHU_IND) {
		THU_IND = tHU_IND;
	}
	public String getFRI_IND() {
		return FRI_IND;
	}
	public void setFRI_IND(String fRI_IND) {
		FRI_IND = fRI_IND;
	}
	public String getSAT_IND() {
		return SAT_IND;
	}
	public void setSAT_IND(String sAT_IND) {
		SAT_IND = sAT_IND;
	}
	public String getSUN_IND() {
		return SUN_IND;
	}
	public void setSUN_IND(String sUN_IND) {
		SUN_IND = sUN_IND;
	}
	public String getRESOURCEMSTR_ID() {
		return RESOURCEMSTR_ID;
	}
	public void setRESOURCEMSTR_ID(String rESOURCEMSTR_ID) {
		RESOURCEMSTR_ID = rESOURCEMSTR_ID;
	}
	public String getRESOURCE_NAME() {
		return RESOURCE_NAME;
	}
	public void setRESOURCE_NAME(String rESOURCE_NAME) {
		RESOURCE_NAME = rESOURCE_NAME;
	}
	public String getResource_code() {
		return resource_code;
	}
	public void setResource_code(String resource_code) {
		this.resource_code = resource_code;
	}
	public String getDefunct_ind() {
		return defunct_ind;
	}
	public void setDefunct_ind(String defunct_ind) {
		this.defunct_ind = defunct_ind;
	}
	public String getCAREPROVIDER_ID() {
		return CAREPROVIDER_ID;
	}
	public void setCAREPROVIDER_ID(String cAREPROVIDER_ID) {
		CAREPROVIDER_ID = cAREPROVIDER_ID;
	}
	public String getPOLI_CODE() {
		return POLI_CODE;
	}
	public void setPOLI_CODE(String pOLI_CODE) {
		POLI_CODE = pOLI_CODE;
	}
	
	
}
