package com.rsmurniteguh.webservice.dep.all.model;

public class infodatapasien {
	
	private String PERSON_ID;
	private String PERSON_NAME;
	private String SEX;
	private String BIRTH_DATE;
	private String ID_NO;
	private String STATUS_NIKAH;
	private String WARGANEGARA;
	private String RAS;
	private String ETNIS;
	private String AGAMA;
	private String STATUSKERJA;
	private String MOBILE_PHONE_NO;
	
	public String getPERSON_ID() {
		return PERSON_ID;
	}
	public void setPERSON_ID(String pERSON_ID) {
		PERSON_ID = pERSON_ID;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getSEX() {
		return SEX;
	}
	public void setSEX(String sEX) {
		SEX = sEX;
	}
	public String getBIRTH_DATE() {
		return BIRTH_DATE;
	}
	public void setBIRTH_DATE(String bIRTH_DATE) {
		BIRTH_DATE = bIRTH_DATE;
	}
	public String getID_NO() {
		return ID_NO;
	}
	public void setID_NO(String iD_NO) {
		ID_NO = iD_NO;
	}
	public String getSTATUS_NIKAH() {
		return STATUS_NIKAH;
	}
	public void setSTATUS_NIKAH(String sTATUS_NIKAH) {
		STATUS_NIKAH = sTATUS_NIKAH;
	}
	public String getWARGANEGARA() {
		return WARGANEGARA;
	}
	public void setWARGANEGARA(String wARGANEGARA) {
		WARGANEGARA = wARGANEGARA;
	}
	public String getRAS() {
		return RAS;
	}
	public void setRAS(String rAS) {
		RAS = rAS;
	}
	public String getETNIS() {
		return ETNIS;
	}
	public void setETNIS(String eTNIS) {
		ETNIS = eTNIS;
	}
	public String getAGAMA() {
		return AGAMA;
	}
	public void setAGAMA(String aGAMA) {
		AGAMA = aGAMA;
	}
	public String getSTATUSKERJA() {
		return STATUSKERJA;
	}
	public void setSTATUSKERJA(String sTATUSKERJA) {
		STATUSKERJA = sTATUSKERJA;
	}
	public String getMOBILE_PHONE_NO() {
		return MOBILE_PHONE_NO;
	}
	public void setMOBILE_PHONE_NO(String mOBILE_PHONE_NO) {
		MOBILE_PHONE_NO = mOBILE_PHONE_NO;
	}
	
	

}
