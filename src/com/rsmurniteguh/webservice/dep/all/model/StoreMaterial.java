package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class StoreMaterial {
private Long StoreItem, MaterialItem;
private String  MaterialItemCat, MaterialItemName,StoreUom,CodeDesc;
private BigDecimal MinQty,MaxQty,BalanceQty;
public Long getStoreItem() {
	return StoreItem;
}
public void setStoreItem(Long storeItem) {
	StoreItem = storeItem;
}
public Long getMaterialItem() {
	return MaterialItem;
}
public void setMaterialItem(Long materialItem) {
	MaterialItem = materialItem;
}
public String getMaterialItemCat() {
	return MaterialItemCat;
}
public void setMaterialItemCat(String materialItemCat) {
	MaterialItemCat = materialItemCat;
}
public String getMaterialItemName() {
	return MaterialItemName;
}
public void setMaterialItemName(String materialItemName) {
	MaterialItemName = materialItemName;
}
public String getStoreUom() {
	return StoreUom;
}
public void setStoreUom(String storeUom) {
	StoreUom = storeUom;
}
public String getCodeDesc() {
	return CodeDesc;
}
public void setCodeDesc(String codeDesc) {
	CodeDesc = codeDesc;
}
public BigDecimal getMinQty() {
	return MinQty;
}
public void setMinQty(BigDecimal minQty) {
	MinQty = minQty;
}
public BigDecimal getMaxQty() {
	return MaxQty;
}
public void setMaxQty(BigDecimal maxQty) {
	MaxQty = maxQty;
}
public BigDecimal getBalanceQty() {
	return BalanceQty;
}
public void setBalanceQty(BigDecimal balanceQty) {
	BalanceQty = balanceQty;
}

}
