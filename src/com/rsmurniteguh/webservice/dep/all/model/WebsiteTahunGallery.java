package com.rsmurniteguh.webservice.dep.all.model;

public class WebsiteTahunGallery {
	private int tahun;

	private String jenis;
	
	public int getTahun() {
		return tahun;
	}

	public void setTahun(int tahun) {
		this.tahun = tahun;
	}

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}
	
}
