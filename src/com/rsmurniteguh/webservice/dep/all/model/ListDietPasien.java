package com.rsmurniteguh.webservice.dep.all.model;

public class ListDietPasien {
	private String vid;
	private String pn;
	private String Age;
	private String pc;
	private String rgn;
	private String kmr;
	
	
	public String getVid() {
		return vid;
	}
	public void setVid(String vid) {
		this.vid = vid;
	}
	public String getPn() {
		return pn;
	}
	public void setPn(String pn) {
		this.pn = pn;
	}
	public String getAge() {
		return Age;
	}
	public void setAge(String age) {
		Age = age;
	}
	public String getPc() {
		return pc;
	}
	public void setPc(String pc) {
		this.pc = pc;
	}
	public String getRgn() {
		return rgn;
	}
	public void setRgn(String rgn) {
		this.rgn = rgn;
	}
	public String getKmr() {
		return kmr;
	}
	public void setKmr(String kmr) {
		this.kmr = kmr;
	}
	
	

}
