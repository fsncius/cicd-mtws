package com.rsmurniteguh.webservice.dep.all.model;

import java.util.List;

public class ListGetQueue {
	private String queue_id;
	private String queue_no;
	private String resourcemstr_id;
	private String queue_date;
	private String card_no;
	private String patient_name;
	private String created_by;
	private String created_at;
	private String updated_at;
	private String updated_by;
	private String deleted;
	private boolean response;
	private String description;
	private List<String> additional;
	
	
	public String getQueue_no() {
		return queue_no;
	}
	public void setQueue_no(String queue_no) {
		this.queue_no = queue_no;
	}
	public String getResourcemstr_id() {
		return resourcemstr_id;
	}
	public void setResourcemstr_id(String resourcemstr_id) {
		this.resourcemstr_id = resourcemstr_id;
	}
	public String getQueue_date() {
		return queue_date;
	}
	public void setQueue_date(String queue_date) {
		this.queue_date = queue_date;
	}
	public String getCard_no() {
		return card_no;
	}
	public void setCard_no(String card_no) {
		this.card_no = card_no;
	}
	public String getPatient_name() {
		return patient_name;
	}
	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public boolean getResponse() {
		return response;
	}
	public void setResponse(boolean response) {
		this.response = response;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getAdditional() {
		return additional;
	}
	public void setAdditional(List<String> additional) {
		this.additional = additional;
	}
	public String getQueue_id() {
		return queue_id;
	}
	public void setQueue_id(String queue_id) {
		this.queue_id = queue_id;
	}
	
	
	
}
