package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;
import java.sql.Date;

public class visitdiagnosisdetail {

	private BigDecimal VisitId;
	private BigDecimal CardNo;
	private String PersonName;
	private String CodeDesc;
	private String Person;
	private String Subspecialitydesc;
	private String DiagnosisDesc;
	private String Sex;
	private Date birthdate;
	
	
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public BigDecimal getVisitId() {
		return VisitId;
	}
	public void setVisitId(BigDecimal visitId) {
		VisitId = visitId;
	}
	public BigDecimal getCardNo() {
		return CardNo;
	}
	public void setCardNo(BigDecimal cardNo) {
		CardNo = cardNo;
	}
	public String getPersonName() {
		return PersonName;
	}
	public void setPersonName(String personName) {
		PersonName = personName;
	}
	public String getCodeDesc() {
		return CodeDesc;
	}
	public void setCodeDesc(String codeDesc) {
		CodeDesc = codeDesc;
	}

	public String getSubspecialitydesc() {
		return Subspecialitydesc;
	}
	public void setSubspecialitydesc(String subspecialitydesc) {
		Subspecialitydesc = subspecialitydesc;
	}
	public String getDiagnosisDesc() {
		return DiagnosisDesc;
	}
	public void setDiagnosisDesc(String diagnosisDesc) {
		DiagnosisDesc = diagnosisDesc;
	}
	public String getPerson() {
		return Person;
	}
	public void setPerson(String person) {
		Person = person;
	}
	
}
