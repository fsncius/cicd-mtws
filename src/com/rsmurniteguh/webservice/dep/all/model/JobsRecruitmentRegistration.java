package com.rsmurniteguh.webservice.dep.all.model;

public class JobsRecruitmentRegistration {
	private String email;
    private String password;
    private String namalengkap;
    private String jeniskelamin;
    private String tempatlahir;
    private String tgllahir;
    private String tinggibadan;
    private String beratbadan;
    private String statusperkawinan;
    private String kewarganegaraan;
    private String suku;
    private String hobby;
    private String agama;
    private String telepontetap;
    private String teleponsekarang;
    private String alamattetap;
    private String alamatsekarang;
    private String noktp;
    private String sima;
    private String simb1;
    private String simb2;
    private String simc;
    private String pendidikanterakhir;
    private String instansipendidikan;
    private String jurusan;
    private String nilai;
    private String bahasa;
    private String freshgraduate;
    private String tglkerja;
    private String gaji;
    private String program;
    private String proses;
    
    
    
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNamalengkap() {
		return namalengkap;
	}
	public void setNamalengkap(String namalengkap) {
		this.namalengkap = namalengkap;
	}
	public String getJeniskelamin() {
		return jeniskelamin;
	}
	public void setJeniskelamin(String jeniskelamin) {
		this.jeniskelamin = jeniskelamin;
	}
	public String getTempatlahir() {
		return tempatlahir;
	}
	public void setTempatlahir(String tempatlahir) {
		this.tempatlahir = tempatlahir;
	}
	public String getTgllahir() {
		return tgllahir;
	}
	public void setTgllahir(String tgllahir) {
		this.tgllahir = tgllahir;
	}
	public String getTinggibadan() {
		return tinggibadan;
	}
	public void setTinggibadan(String tinggibadan) {
		this.tinggibadan = tinggibadan;
	}
	public String getBeratbadan() {
		return beratbadan;
	}
	public void setBeratbadan(String beratbadan) {
		this.beratbadan = beratbadan;
	}
	public String getStatusperkawinan() {
		return statusperkawinan;
	}
	public void setStatusperkawinan(String statusperkawinan) {
		this.statusperkawinan = statusperkawinan;
	}
	public String getKewarganegaraan() {
		return kewarganegaraan;
	}
	public void setKewarganegaraan(String kewarganegaraan) {
		this.kewarganegaraan = kewarganegaraan;
	}
	public String getSuku() {
		return suku;
	}
	public void setSuku(String suku) {
		this.suku = suku;
	}
	public String getHobby() {
		return hobby;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	public String getAgama() {
		return agama;
	}
	public void setAgama(String agama) {
		this.agama = agama;
	}
	public String getTelepontetap() {
		return telepontetap;
	}
	public void setTelepontetap(String telepontetap) {
		this.telepontetap = telepontetap;
	}
	public String getTeleponsekarang() {
		return teleponsekarang;
	}
	public void setTeleponsekarang(String teleponsekarang) {
		this.teleponsekarang = teleponsekarang;
	}
	public String getAlamattetap() {
		return alamattetap;
	}
	public void setAlamattetap(String alamattetap) {
		this.alamattetap = alamattetap;
	}
	public String getAlamatsekarang() {
		return alamatsekarang;
	}
	public void setAlamatsekarang(String alamatsekarang) {
		this.alamatsekarang = alamatsekarang;
	}
	public String getNoktp() {
		return noktp;
	}
	public void setNoktp(String noktp) {
		this.noktp = noktp;
	}
	public String getSima() {
		return sima;
	}
	public void setSima(String sima) {
		this.sima = sima;
	}
	public String getSimb1() {
		return simb1;
	}
	public void setSimb1(String simb1) {
		this.simb1 = simb1;
	}
	public String getSimb2() {
		return simb2;
	}
	public void setSimb2(String simb2) {
		this.simb2 = simb2;
	}
	public String getSimc() {
		return simc;
	}
	public void setSimc(String simc) {
		this.simc = simc;
	}
	public String getPendidikanterakhir() {
		return pendidikanterakhir;
	}
	public void setPendidikanterakhir(String pendidikanterakhir) {
		this.pendidikanterakhir = pendidikanterakhir;
	}
	public String getInstansipendidikan() {
		return instansipendidikan;
	}
	public void setInstansipendidikan(String instansipendidikan) {
		this.instansipendidikan = instansipendidikan;
	}
	public String getJurusan() {
		return jurusan;
	}
	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}
	public String getNilai() {
		return nilai;
	}
	public void setNilai(String nilai) {
		this.nilai = nilai;
	}
	public String getBahasa() {
		return bahasa;
	}
	public void setBahasa(String bahasa) {
		this.bahasa = bahasa;
	}
	public String getFreshgraduate() {
		return freshgraduate;
	}
	public void setFreshgraduate(String freshgraduate) {
		this.freshgraduate = freshgraduate;
	}
	public String getTglkerja() {
		return tglkerja;
	}
	public void setTglkerja(String tglkerja) {
		this.tglkerja = tglkerja;
	}
	public String getGaji() {
		return gaji;
	}
	public void setGaji(String gaji) {
		this.gaji = gaji;
	}
	public String getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	public String getProses() {
		return proses;
	}
	public void setProses(String proses) {
		this.proses = proses;
	}
    
    
    
}
