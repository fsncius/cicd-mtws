package com.rsmurniteguh.webservice.dep.all.model;

public class WebsiteGallery {
	private String id_kegiatan;
	private String nama_kegiatan;
	private String jenis_kegiatan;
	private String tanggal_kegiatan;
	private String deskripsi;
	private String lokasi;
	private String nama;
	private String gambar;
		
	public String getId_kegiatan() {
		return id_kegiatan;
	}
	public void setId_kegiatan(String id_kegiatan) {
		this.id_kegiatan = id_kegiatan;
	}
	public String getNama_kegiatan() {
		return nama_kegiatan;
	}
	public void setNama_kegiatan(String nama_kegiatan) {
		this.nama_kegiatan = nama_kegiatan;
	}
	public String getJenis_kegiatan() {
		return jenis_kegiatan;
	}
	public void setJenis_kegiatan(String jenis_kegiatan) {
		this.jenis_kegiatan = jenis_kegiatan;
	}
	public String getTanggal_kegiatan() {
		return tanggal_kegiatan;
	}
	public void setTanggal_kegiatan(String tanggal_kegiatan) {
		this.tanggal_kegiatan = tanggal_kegiatan;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getLokasi() {
		return lokasi;
	}
	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getGambar() {
		return gambar;
	}
	public void setGambar(String gambar) {
		this.gambar = gambar;
	}
	
}
