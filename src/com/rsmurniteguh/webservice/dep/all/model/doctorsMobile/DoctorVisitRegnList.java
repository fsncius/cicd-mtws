package com.rsmurniteguh.webservice.dep.all.model.doctorsMobile;

import java.math.BigDecimal;

public class DoctorVisitRegnList {

	public String visitId;
	public BigDecimal careproviderId;
	public String subspeciality_desc;
	public String pasien;
	public String dokter;
	public String getVisitId() {
		return visitId;
	}
	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}
	public BigDecimal getCareproviderId() {
		return careproviderId;
	}
	public void setCareproviderId(BigDecimal careproviderId) {
		this.careproviderId = careproviderId;
	}
	public String getSubspeciality_desc() {
		return subspeciality_desc;
	}
	public void setSubspeciality_desc(String subspeciality_desc) {
		this.subspeciality_desc = subspeciality_desc;
	}
	public String getPasien() {
		return pasien;
	}
	public void setPasien(String pasien) {
		this.pasien = pasien;
	}
	public String getDokter() {
		return dokter;
	}
	public void setDokter(String dokter) {
		this.dokter = dokter;
	}
	
	
}
