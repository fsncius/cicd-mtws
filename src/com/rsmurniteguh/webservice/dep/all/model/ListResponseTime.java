package com.rsmurniteguh.webservice.dep.all.model;

public class ListResponseTime {
	public String queueId;
	public String queueDate;
	public String queueNo;
	public String cardNo;
	public String patientName;
	public String enterqueueDatetime;
	public String startserveDatetime;
	public String endserveDatetime;
	
	public String getQueueId() {
		return queueId;
	}
	public void setQueueId(String queueId) {
		this.queueId = queueId;
	}
	public String getQueueDate() {
		return queueDate;
	}
	public void setQueueDate(String queueDate) {
		this.queueDate = queueDate;
	}
	public String getQueueNo() {
		return queueNo;
	}
	public void setQueueNo(String queueNo) {
		this.queueNo = queueNo;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getEnterqueueDatetime() {
		return enterqueueDatetime;
	}
	public void setEnterqueueDatetime(String enterqueueDatetime) {
		this.enterqueueDatetime = enterqueueDatetime;
	}
	public String getEndserveDatetime() {
		return endserveDatetime;
	}
	public String getStartserveDatetime() {
		return startserveDatetime;
	}
	public void setStartserveDatetime(String startserveDatetime) {
		this.startserveDatetime = startserveDatetime;
	}
	public void setEndserveDatetime(String endserveDatetime) {
		this.endserveDatetime = endserveDatetime;
	}
	
	
	

}
