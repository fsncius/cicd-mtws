package com.rsmurniteguh.webservice.dep.all.model;

public class ListDataPasienSms {

	private String PERSON_ID;
	private String PERSON_NAME;
	private String MOBILE_PHONE_NO;
	
	public String getPERSON_ID() {
		return PERSON_ID;
	}
	public void setPERSON_ID(String pERSON_ID) {
		PERSON_ID = pERSON_ID;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getMOBILE_PHONE_NO() {
		return MOBILE_PHONE_NO;
	}
	public void setMOBILE_PHONE_NO(String mOBILE_PHONE_NO) {
		MOBILE_PHONE_NO = mOBILE_PHONE_NO;
	}
	
	
}
