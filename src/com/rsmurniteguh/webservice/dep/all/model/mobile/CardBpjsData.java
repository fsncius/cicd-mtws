package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class CardBpjsData {
	private String tglLahir;
	private String provumumnm;
	private String provumumkd;
	public String getTglLahir() {
		return tglLahir;
	}
	public void setTglLahir(String tglLahir) {
		this.tglLahir = tglLahir;
	}
	public String getProvumumnm() {
		return provumumnm;
	}
	public void setProvumumnm(String provumumnm) {
		this.provumumnm = provumumnm;
	}
	public String getProvumumkd() {
		return provumumkd;
	}
	public void setProvumumkd(String provumumkd) {
		this.provumumkd = provumumkd;
	}
}
