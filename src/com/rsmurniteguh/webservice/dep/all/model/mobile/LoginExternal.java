package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class LoginExternal {

	private boolean response;
	private String description;
	private String usermstr_id;
	private String user_name;
	private String usertype;
	private String typedesc;
	private String designation;
	private String logintype;
	private String mrn;
	private String salt;
	private String isAuthPR;
	
	public boolean isResponse() {
		return response;
	}
	public void setResponse(boolean response) {
		this.response = response;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUsermstr_id() {
		return usermstr_id;
	}
	public void setUsermstr_id(String usermstr_id) {
		this.usermstr_id = usermstr_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getTypedesc() {
		return typedesc;
	}
	public void setTypedesc(String typedesc) {
		this.typedesc = typedesc;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getLogintype() {
		return logintype;
	}
	public void setLogintype(String logintype) {
		this.logintype = logintype;
	}
	public String getMrn() {
		return mrn;
	}
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getIsAuthPR() {
		return isAuthPR;
	}
	public void setIsAuthPR(String isAuthPR) {
		this.isAuthPR = isAuthPR;
	}
}
