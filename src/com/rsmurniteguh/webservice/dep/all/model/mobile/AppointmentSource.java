package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.math.BigDecimal;

public class AppointmentSource {
	private String tanggal;
	private BigDecimal count_patient;
	private String doctor_name;
	private String registered_kthis;
	private BigDecimal registered;
	private BigDecimal confirm;
	private BigDecimal cancel;
	private BigDecimal wait;
	private String department;
	private String source;

	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getDoctor_name() {
		return doctor_name;
	}
	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}
	public String getRegistered_kthis() {
		return registered_kthis;
	}
	public void setRegistered_kthis(String registered_kthis) {
		this.registered_kthis = registered_kthis;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public BigDecimal getCount_patient() {
		return count_patient;
	}
	public void setCount_patient(BigDecimal count_patient) {
		this.count_patient = count_patient;
	}
	public BigDecimal getRegistered() {
		return registered;
	}
	public void setRegistered(BigDecimal registered) {
		this.registered = registered;
	}
	public BigDecimal getConfirm() {
		return confirm;
	}
	public void setConfirm(BigDecimal confirm) {
		this.confirm = confirm;
	}
	public BigDecimal getCancel() {
		return cancel;
	}
	public void setCancel(BigDecimal cancel) {
		this.cancel = cancel;
	}
	public BigDecimal getWait() {
		return wait;
	}
	public void setWait(BigDecimal wait) {
		this.wait = wait;
	}
}
