package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.util.List;

public class PurchaseRequisition {
	
	private String stockpurchase_requisitionid;
	private String storemstr_id;
	private String purchase_requisition_no;
	private String purchase_status;
	private String created_by;
	private String created_datetime;
	private String requested_by;
	private String requested_datetime;
	private String planned_by;
	private String planned_datetime;
	private String cancelled_by;
	private String cancelled_datetime;
	private String remarks;
	private String prev_updated_by;
	private String last_updated_by;
	private String prev_updated_datetime;
	private String last_updated_datetime;
	private String store_desc;
	private String store_desc_lang1;
	private String created_by_desc;
	private String confirmed_by_desc;
	private String code_desc;
	private String prdetail;
	private List<PurchaseRequisitionDetail> purchaseRequisitionDetail;
	
	
	public String getStoremstr_id() {
		return storemstr_id;
	}
	public void setStoremstr_id(String storemstr_id) {
		this.storemstr_id = storemstr_id;
	}
	public String getPurchase_requisition_no() {
		return purchase_requisition_no;
	}
	public void setPurchase_requisition_no(String purchase_requisition_no) {
		this.purchase_requisition_no = purchase_requisition_no;
	}
	public String getPurchase_status() {
		return purchase_status;
	}
	public void setPurchase_status(String purchase_status) {
		this.purchase_status = purchase_status;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getCreated_datetime() {
		return created_datetime;
	}
	public void setCreated_datetime(String created_datetime) {
		this.created_datetime = created_datetime;
	}
	public String getRequested_by() {
		return requested_by;
	}
	public void setRequested_by(String requested_by) {
		this.requested_by = requested_by;
	}
	public String getRequested_datetime() {
		return requested_datetime;
	}
	public void setRequested_datetime(String requested_datetime) {
		this.requested_datetime = requested_datetime;
	}
	public String getPlanned_by() {
		return planned_by;
	}
	public void setPlanned_by(String planned_by) {
		this.planned_by = planned_by;
	}
	public String getPlanned_datetime() {
		return planned_datetime;
	}
	public void setPlanned_datetime(String planned_datetime) {
		this.planned_datetime = planned_datetime;
	}
	public String getCancelled_by() {
		return cancelled_by;
	}
	public void setCancelled_by(String cancelled_by) {
		this.cancelled_by = cancelled_by;
	}
	public String getCancelled_datetime() {
		return cancelled_datetime;
	}
	public void setCancelled_datetime(String cancelled_datetime) {
		this.cancelled_datetime = cancelled_datetime;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getPrev_updated_by() {
		return prev_updated_by;
	}
	public void setPrev_updated_by(String prev_updated_by) {
		this.prev_updated_by = prev_updated_by;
	}
	public String getLast_updated_by() {
		return last_updated_by;
	}
	public void setLast_updated_by(String last_updated_by) {
		this.last_updated_by = last_updated_by;
	}
	public String getPrev_updated_datetime() {
		return prev_updated_datetime;
	}
	public void setPrev_updated_datetime(String prev_updated_datetime) {
		this.prev_updated_datetime = prev_updated_datetime;
	}
	public String getLast_updated_datetime() {
		return last_updated_datetime;
	}
	public void setLast_updated_datetime(String last_updated_datetime) {
		this.last_updated_datetime = last_updated_datetime;
	}
	public String getStore_desc() {
		return store_desc;
	}
	public void setStore_desc(String store_desc) {
		this.store_desc = store_desc;
	}
	public String getStore_desc_lang1() {
		return store_desc_lang1;
	}
	public void setStore_desc_lang1(String store_desc_lang1) {
		this.store_desc_lang1 = store_desc_lang1;
	}
	public String getCreated_by_desc() {
		return created_by_desc;
	}
	public void setCreated_by_desc(String created_by_desc) {
		this.created_by_desc = created_by_desc;
	}
	public String getConfirmed_by_desc() {
		return confirmed_by_desc;
	}
	public void setConfirmed_by_desc(String confirmed_by_desc) {
		this.confirmed_by_desc = confirmed_by_desc;
	}
	public String getCode_desc() {
		return code_desc;
	}
	public void setCode_desc(String code_desc) {
		this.code_desc = code_desc;
	}
	public String getPrdetail() {
		return prdetail;
	}
	public void setPrdetail(String prdetail) {
		this.prdetail = prdetail;
	}
	public String getStockpurchase_requisitionid() {
		return stockpurchase_requisitionid;
	}
	public void setStockpurchase_requisitionid(String stockpurchase_requisitionid) {
		this.stockpurchase_requisitionid = stockpurchase_requisitionid;
	}
	public List<PurchaseRequisitionDetail> getPurchaseRequisitionDetail() {
		return purchaseRequisitionDetail;
	}
	public void setPurchaseRequisitionDetail(List<PurchaseRequisitionDetail> purchaseRequisitionDetail) {
		this.purchaseRequisitionDetail = purchaseRequisitionDetail;
	}
}
