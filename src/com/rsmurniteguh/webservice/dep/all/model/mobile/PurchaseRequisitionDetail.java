package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class PurchaseRequisitionDetail {
	
	private String purchaseRequisition_detai_id;
	private String stockpurchaserequisition_id;
	private String requisition_detail_status;
	private String materialitemmstr_id;
	private String requisition_qty;
	private String requisition_uom;
	private String unit_price;
	private String vendormstr_id;
	private String created_by;
	private String created_datetime;
	private String remarks;
	private String prev_update_datetime;
	private String last_update_datetime;
	private String manufacturer_vendormstr_id;
	private String discount_rate;
	private String total_puchase_price;
	private String ppn_ind;
	private String requisition_qty_for_show;
	private String requisition_uom_for_show;
	private String complete_ind;
	private String complete_remarks;
	private String total_rs_amt;
	private String max_usage;
	private String average_usage;
	private String total_storage;
	private String pr_qty;
	private String po_qty;
	private String brand_name;
	
	public String getPurchaseRequisition_detai_id() {
		return purchaseRequisition_detai_id;
	}
	public void setPurchaseRequisition_detai_id(String purchaseRequisition_detai_id) {
		this.purchaseRequisition_detai_id = purchaseRequisition_detai_id;
	}
	public String getStockpurchaserequisition_id() {
		return stockpurchaserequisition_id;
	}
	public void setStockpurchaserequisition_id(String stockpurchaserequisition_id) {
		this.stockpurchaserequisition_id = stockpurchaserequisition_id;
	}
	public String getRequisition_detail_status() {
		return requisition_detail_status;
	}
	public void setRequisition_detail_status(String requisition_detail_status) {
		this.requisition_detail_status = requisition_detail_status;
	}
	public String getMaterialitemmstr_id() {
		return materialitemmstr_id;
	}
	public void setMaterialitemmstr_id(String materialitemmstr_id) {
		this.materialitemmstr_id = materialitemmstr_id;
	}
	public String getRequisition_qty() {
		return requisition_qty;
	}
	public void setRequisition_qty(String requisition_qty) {
		this.requisition_qty = requisition_qty;
	}
	public String getRequisition_uom() {
		return requisition_uom;
	}
	public void setRequisition_uom(String requisition_uom) {
		this.requisition_uom = requisition_uom;
	}
	public String getUnit_price() {
		return unit_price;
	}
	public void setUnit_price(String unit_price) {
		this.unit_price = unit_price;
	}
	public String getVendormstr_id() {
		return vendormstr_id;
	}
	public void setVendormstr_id(String vendormstr_id) {
		this.vendormstr_id = vendormstr_id;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getCreated_datetime() {
		return created_datetime;
	}
	public void setCreated_datetime(String created_datetime) {
		this.created_datetime = created_datetime;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getPrev_update_datetime() {
		return prev_update_datetime;
	}
	public void setPrev_update_datetime(String prev_update_datetime) {
		this.prev_update_datetime = prev_update_datetime;
	}
	public String getLast_update_datetime() {
		return last_update_datetime;
	}
	public void setLast_update_datetime(String last_update_datetime) {
		this.last_update_datetime = last_update_datetime;
	}
	public String getManufacturer_vendormstr_id() {
		return manufacturer_vendormstr_id;
	}
	public void setManufacturer_vendormstr_id(String manufacturer_vendormstr_id) {
		this.manufacturer_vendormstr_id = manufacturer_vendormstr_id;
	}
	public String getDiscount_rate() {
		return discount_rate;
	}
	public void setDiscount_rate(String discount_rate) {
		this.discount_rate = discount_rate;
	}
	public String getTotal_puchase_price() {
		return total_puchase_price;
	}
	public void setTotal_puchase_price(String total_puchase_price) {
		this.total_puchase_price = total_puchase_price;
	}
	public String getPpn_ind() {
		return ppn_ind;
	}
	public void setPpn_ind(String ppn_ind) {
		this.ppn_ind = ppn_ind;
	}
	public String getRequisition_qty_for_show() {
		return requisition_qty_for_show;
	}
	public void setRequisition_qty_for_show(String requisition_qty_for_show) {
		this.requisition_qty_for_show = requisition_qty_for_show;
	}
	public String getRequisition_uom_for_show() {
		return requisition_uom_for_show;
	}
	public void setRequisition_uom_for_show(String requisition_uom_for_show) {
		this.requisition_uom_for_show = requisition_uom_for_show;
	}
	public String getComplete_ind() {
		return complete_ind;
	}
	public void setComplete_ind(String complete_ind) {
		this.complete_ind = complete_ind;
	}
	public String getComplete_remarks() {
		return complete_remarks;
	}
	public void setComplete_remarks(String complete_remarks) {
		this.complete_remarks = complete_remarks;
	}
	public String getTotal_rs_amt() {
		return total_rs_amt;
	}
	public void setTotal_rs_amt(String total_rs_amt) {
		this.total_rs_amt = total_rs_amt;
	}
	public String getMax_usage() {
		return max_usage;
	}
	public void setMax_usage(String max_usage) {
		this.max_usage = max_usage;
	}
	public String getAverage_usage() {
		return average_usage;
	}
	public void setAverage_usage(String average_usage) {
		this.average_usage = average_usage;
	}
	public String getTotal_storage() {
		return total_storage;
	}
	public void setTotal_storage(String total_storage) {
		this.total_storage = total_storage;
	}
	public String getPr_qty() {
		return pr_qty;
	}
	public void setPr_qty(String pr_qty) {
		this.pr_qty = pr_qty;
	}
	public String getPo_qty() {
		return po_qty;
	}
	public void setPo_qty(String po_qty) {
		this.po_qty = po_qty;
	}
	public String getBrand_name() {
		return brand_name;
	}
	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}
}