package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class MainDataDoctor {
	private int inpatient_bpjs;
	private int inpatient_general;
	private int reg_today;
	private int reg_yesterday;

	public int getInpatient_bpjs() {
		return inpatient_bpjs;
	}
	public void setInpatient_bpjs(int inpatient_bpjs) {
		this.inpatient_bpjs = inpatient_bpjs;
	}
	public int getInpatient_general() {
		return inpatient_general;
	}
	public void setInpatient_general(int inpatient_general) {
		this.inpatient_general = inpatient_general;
	}
	public int getReg_today() {
		return reg_today;
	}
	public void setReg_today(int reg_today) {
		this.reg_today = reg_today;
	}
	public int getReg_yesterday() {
		return reg_yesterday;
	}
	public void setReg_yesterday(int reg_yesterday) {
		this.reg_yesterday = reg_yesterday;
	}
}
