package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.util.HashMap;
import java.util.List;

public class WeeklyScheduleDoctor {
	private String doctorname;
	private String doctor_url_image;
	private String specialist;
	private String careprovider;
	private String seq_doctor;
	private String seq_poli;
	private HashMap<Integer,List<String>> schedule;
	

	public String getDoctorname() {
		return doctorname;
	}
	public void setDoctorname(String doctorname) {
		this.doctorname = doctorname;
	}
	public String getDoctor_url_image() {
		return doctor_url_image;
	}
	public void setDoctor_url_image(String doctor_url_image) {
		this.doctor_url_image = doctor_url_image;
	}
	public String getSpecialist() {
		return specialist;
	}
	public void setSpecialist(String specialist) {
		this.specialist = specialist;
	}
	public String getCareprovider() {
		return careprovider;
	}
	public void setCareprovider(String careprovider) {
		this.careprovider = careprovider;
	}
	public HashMap<Integer, List<String>> getSchedule() {
		return schedule;
	}
	public void setSchedule(HashMap<Integer, List<String>> schedule) {
		this.schedule = schedule;
	}
	public String getSeq_doctor() {
		return seq_doctor;
	}
	public void setSeq_doctor(String seq_doctor) {
		this.seq_doctor = seq_doctor;
	}
	public String getSeq_poli() {
		return seq_poli;
	}
	public void setSeq_poli(String seq_poli) {
		this.seq_poli = seq_poli;
	}
}
