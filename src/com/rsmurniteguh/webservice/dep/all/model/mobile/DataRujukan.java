package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class DataRujukan {
	private boolean response;
	private String reference_id;
	private String mrn;
	private String images;
	private String reference_date;
	private String reference;
	private String reference_by;
	
	private String description;
	private String createdate;
	private String createby;
	private String expired_date;
	private String type;
	private String status;

	private String sortexpired_date;
	private String sortreference_date;
	private String sortcreatedate;
    
	
	public String getMrn() {
		return mrn;
	}

	public void setMrn(String mrn) {
		this.mrn = mrn;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getReference_date() {
		return reference_date;
	}

	public void setReference_date(String reference_date) {
		this.reference_date = reference_date;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public boolean isResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}

	public String getDescription() {
		return description;
	}
	
	
	public String getSortexpired_date() {
		return sortexpired_date;
	}

	public void setSortexpired_date(String sortexpired_date) {
		this.sortexpired_date = sortexpired_date;
	}

	public String getSortreference_date() {
		return sortreference_date;
	}

	public void setSortreference_date(String sortreference_date) {
		this.sortreference_date = sortreference_date;
	}

	public String getSortcreatedate() {
		return sortcreatedate;
	}

	public void setSortcreatedate(String sortcreatedate) {
		this.sortcreatedate = sortcreatedate;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCreateby() {
		return createby;
	}

	public void setCreateby(String createby) {
		this.createby = createby;
	}

	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public String getReference_id() {
		return reference_id;
	}

	public void setReference_id(String reference_id) {
		this.reference_id = reference_id;
	}

	public String getExpired_date() {
		return expired_date;
	}

	public void setExpired_date(String expired_date) {
		this.expired_date = expired_date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReference_by() {
		return reference_by;
	}

	public void setReference_by(String reference_by) {
		this.reference_by = reference_by;
	}

}
