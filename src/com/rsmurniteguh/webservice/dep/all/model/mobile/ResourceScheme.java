package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.math.BigDecimal;

public class ResourceScheme {
	
	private BigDecimal resourcemstr;
	private BigDecimal resourceSchemeId;
	private String dateSchedule;
	private String sessionDesc;
	
	public BigDecimal getResourcemstr() {
		return resourcemstr;
	}
	public void setResourcemstr(BigDecimal resourcemstr) {
		this.resourcemstr = resourcemstr;
	}
	public BigDecimal getResourceSchemeId() {
		return resourceSchemeId;
	}
	public void setResourceSchemeId(BigDecimal resourceSchemeId) {
		this.resourceSchemeId = resourceSchemeId;
	}
	public String getDateSchedule() {
		return dateSchedule;
	}
	public void setDateSchedule(String dateSchedule) {
		this.dateSchedule = dateSchedule;
	}
	public String getSessionDesc() {
		return sessionDesc;
	}
	public void setSessionDesc(String sessionDesc) {
		this.sessionDesc = sessionDesc;
	}

}
