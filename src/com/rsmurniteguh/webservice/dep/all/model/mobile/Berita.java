package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class Berita {
	private String berita_id;
	private String judul;
	private String reader_role;
	private String tanggal;
	private String simple_berita;
	private String isi_berita;
	public String getBerita_id() {
		return berita_id;
	}
	public void setBerita_id(String berita_id) {
		this.berita_id = berita_id;
	}
	public String getJudul() {
		return judul;
	}
	public void setJudul(String judul) {
		this.judul = judul;
	}
	public String getReader_role() {
		return reader_role;
	}
	public void setReader_role(String reader_role) {
		this.reader_role = reader_role;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getSimple_berita() {
		return simple_berita;
	}
	public void setSimple_berita(String simple_berita) {
		this.simple_berita = simple_berita;
	}
	public String getIsi_berita() {
		return isi_berita;
	}
	public void setIsi_berita(String isi_berita) {
		this.isi_berita = isi_berita;
	}
}
