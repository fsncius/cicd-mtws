package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class RadiologyScheduleViewModel {
	
	private String hour;
	private String patientName;
	private String examDesc;
	public String getHour() {
		return hour;
	}
	public void setHour(String hour) {
		this.hour = hour;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getExamDesc() {
		return examDesc;
	}
	public void setExamDesc(String examDesc) {
		this.examDesc = examDesc;
	}
	
	

}
