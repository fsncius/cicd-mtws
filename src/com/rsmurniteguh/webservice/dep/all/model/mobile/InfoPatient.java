package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class InfoPatient {
	private String mrn;
	private String tglLahir;
	private String sex;
	private String namaPasien;
	private String username;
	private String ktp;
	private int count;
	private String token;
	private boolean samePass;
	private String bpjs_no;
	
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getTglLahir() {
		return tglLahir;
	}
	public void setTglLahir(String tglLahir) {
		this.tglLahir = tglLahir;
	}
	public String getMrn() {
		return mrn;
	}
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	public String getNamaPasien() {
		return namaPasien;
	}
	public void setNamaPasien(String namaPasien) {
		this.namaPasien = namaPasien;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getKtp() {
		return ktp;
	}
	public void setKtp(String ktp) {
		this.ktp = ktp;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public boolean isSamePass() {
		return samePass;
	}
	public void setSamePass(boolean samePass) {
		this.samePass = samePass;
	}
	public String getBpjs_no() {
		return bpjs_no;
	}
	public void setBpjs_no(String bpjs_no) {
		this.bpjs_no = bpjs_no;
	}
}
