package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.math.BigDecimal;
import java.sql.Date;

public class Patient {

	private BigDecimal personId;
	private BigDecimal patientId;
	private String cardNo;
	private String personName;
	private String sex;
	private String age;
	private Date birthDate;
	private BigDecimal stakeholderId;
	public BigDecimal getPersonId() {
		return personId;
	}
	public void setPersonId(BigDecimal personId) {
		this.personId = personId;
	}
	public BigDecimal getPatientId() {
		return patientId;
	}
	public void setPatientId(BigDecimal patientId) {
		this.patientId = patientId;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public BigDecimal getStakeholderId() {
		return stakeholderId;
	}
	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
	}
	
	
}
