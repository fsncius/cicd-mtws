package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.math.BigDecimal;

public class DataUser {
	private boolean response;
	private BigDecimal user_id;
	private String no_mrn;
	private String no_ktp;
	private String username;
	private String fullname;	
	private String no_bpjs;
	private String password;
	private String email;
	private String contact;
	
	public boolean isResponse() {
		return response;
	}
	public void setResponse(boolean response) {
		this.response = response;
	}
	public BigDecimal getUser_id() {
		return user_id;
	}
	public void setUser_id(BigDecimal user_id) {
		this.user_id = user_id;
	}
	public String getNo_mrn() {
		return no_mrn;
	}
	public void setNo_mrn(String no_mrn) {
		this.no_mrn = no_mrn;
	}
	public String getNo_ktp() {
		return no_ktp;
	}
	public void setNo_ktp(String no_ktp) {
		this.no_ktp = no_ktp;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getNo_bpjs() {
		return no_bpjs;
	}
	public void setNo_bpjs(String no_bpjs) {
		this.no_bpjs = no_bpjs;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	
	
}
