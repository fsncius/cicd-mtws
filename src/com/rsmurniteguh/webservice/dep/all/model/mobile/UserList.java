package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class UserList {
	private String mrn;
	private String tgl_lahir;
	private String no_ktp;
	private String username;
	private String password;

	public String getTgl_lahir() {
		return tgl_lahir;
	}

	public void setTgl_lahir(String tgl_lahir) {
		this.tgl_lahir = tgl_lahir;
	}

	public String getNo_ktp() {
		return no_ktp;
	}

	public void setNo_ktp(String no_ktp) {
		this.no_ktp = no_ktp;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMrn() {
		return mrn;
	}

	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	
}
