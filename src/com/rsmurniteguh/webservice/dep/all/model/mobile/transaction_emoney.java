package com.rsmurniteguh.webservice.dep.all.model.mobile;


public class transaction_emoney {
	String trxtype;
	String trxdate;
	String trx_amount;
	String new_amount;
	String desc;

	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getTrxtype() {
		return trxtype;
	}
	public void setTrxtype(String trxtype) {
		this.trxtype = trxtype;
	}
	public String getTrxdate() {
		return trxdate;
	}
	public void setTrxdate(String trxdate) {
		this.trxdate = trxdate;
	}
	public String getTrx_amount() {
		return trx_amount;
	}
	public void setTrx_amount(String trx_amount) {
		this.trx_amount = trx_amount;
	}

	public String getNew_amount() {
		return new_amount;
	}
	public void setNew_amount(String new_amount) {
		this.new_amount = new_amount;
	}
}
