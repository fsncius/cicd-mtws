package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class DataAppointment {
		private boolean success;
		private String registrasi_id;
		private String queueNo;
	    private String resourceMstrId;
	    private String careProviderId;
		private String nameDoctor;
	    private String queeDate;
	    private String mrn;
	    private String userId;
	    private String status;
	    private String referenceId;
	    private String referenceDate;	    
	    private String department;
	    private String source;
	    private String barcode;
	    private String createdDate;
	    private String updateBy;
	    private String updateDate;
	    private String printstatus;
	    
		public String getQueueNo() {
			return queueNo;
		}
		public void setQueueNo(String queueNo) {
			this.queueNo = queueNo;
		}
		public String getResourceMstrId() {
			return resourceMstrId;
		}
		public void setResourceMstrId(String resourceMstrId) {
			this.resourceMstrId = resourceMstrId;
		}
		public String getCareProviderId() {
			return careProviderId;
		}
		public void setCareProviderId(String careProviderId) {
			this.careProviderId = careProviderId;
		}
		public String getNameDoctor() {
			return nameDoctor;
		}
		public void setNameDoctor(String nameDoctor) {
			this.nameDoctor = nameDoctor;
		}
		public String getQueeDate() {
			return queeDate;
		}
		public void setQueeDate(String queeDate) {
			this.queeDate = queeDate;
		}
		public String getMrn() {
			return mrn;
		}
		public void setMrn(String mrn) {
			this.mrn = mrn;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getReferenceId() {
			return referenceId;
		}
		public void setReferenceId(String referenceId) {
			this.referenceId = referenceId;
		}
		public String getReferenceDate() {
			return referenceDate;
		}
		public void setReferenceDate(String referenceDate) {
			this.referenceDate = referenceDate;
		}
		public String getDepartment() {
			return department;
		}
		public void setDepartment(String department) {
			this.department = department;
		}
		public String getSource() {
			return source;
		}
		public void setSource(String source) {
			this.source = source;
		}
		public String getBarcode() {
			return barcode;
		}
		public void setBarcode(String barcode) {
			this.barcode = barcode;
		}
		public String getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}
		public String getUpdateBy() {
			return updateBy;
		}
		public void setUpdateBy(String updateBy) {
			this.updateBy = updateBy;
		}
		public String getUpdateDate() {
			return updateDate;
		}
		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}
		public boolean isSuccess() {
			return success;
		}
		public void setSuccess(boolean success) {
			this.success = success;
		}
		public String getRegistrasi_id() {
			return registrasi_id;
		}
		public void setRegistrasi_id(String registrasi_id) {
			this.registrasi_id = registrasi_id;
		}
		public String getPrintstatus() {
			return printstatus;
		}
		public void setPrintstatus(String printstatus) {
			this.printstatus = printstatus;
		}
	    
	    
	    
}
