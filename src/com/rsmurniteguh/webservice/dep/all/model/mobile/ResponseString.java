package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class ResponseString {
	private String response;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}