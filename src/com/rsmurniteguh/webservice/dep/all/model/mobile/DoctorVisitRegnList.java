package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.math.BigDecimal;

public class DoctorVisitRegnList {

	private String visitId;
	private BigDecimal careproviderId;
	private String subspeciality_desc;
	private String subspecialitymstr_id;
	private String pasien;
	private String dokter;
	private BigDecimal usermstrId;
	
	public String getVisitId() {
		return visitId;
	}
	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}
	public BigDecimal getCareproviderId() {
		return careproviderId;
	}
	public void setCareproviderId(BigDecimal careproviderId) {
		this.careproviderId = careproviderId;
	}
	public String getSubspeciality_desc() {
		return subspeciality_desc;
	}
	public void setSubspeciality_desc(String subspeciality_desc) {
		this.subspeciality_desc = subspeciality_desc;
	}
	public String getPasien() {
		return pasien;
	}
	public void setPasien(String pasien) {
		this.pasien = pasien;
	}
	public String getDokter() {
		return dokter;
	}
	public void setDokter(String dokter) {
		this.dokter = dokter;
	}
	public BigDecimal getUsermstrId() {
		return usermstrId;
	}
	public void setUsermstrId(BigDecimal usermstrId) {
		this.usermstrId = usermstrId;
	}
	public String getSubspecialitymstr_id() {
		return subspecialitymstr_id;
	}
	public void setSubspecialitymstr_id(String subspecialitymstr_id) {
		this.subspecialitymstr_id = subspecialitymstr_id;
	}
	
	
}
