package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class listCardNoBpjs {
	private String PERSON_NAME;
	private String BPJS_NO;
	
	
	public String getBPJS_NO() {
		return BPJS_NO;
	}
	public void setBPJS_NO(String bPJS_NO) {
		BPJS_NO = bPJS_NO;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
}
