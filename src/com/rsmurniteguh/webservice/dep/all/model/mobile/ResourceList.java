package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.math.BigDecimal;

public class ResourceList {

	
	private String resourcescheme_id;
	private String poli_code;
	private BigDecimal resourcemstr_id;
	private String resource_code;
	private String resource_short_code;
	private String resource_quick_code;
	private String resource_name;
	private String resource_name_lang1;
	private String registration_type;
	private BigDecimal subspecialtymstr_id;
	private String subspecialty_desc;
	private String subspecialty_desc_lang1;
	private BigDecimal careprovider_id;
	private String poli_name;
	private String specialist;
	
	
	public String getPoli_name() {
		return poli_name;
	}
	public void setPoli_name(String poli_name) {
		this.poli_name = poli_name;
	}
	public String getResourcescheme_id() {
		return resourcescheme_id;
	}
	public void setResourcescheme_id(String resourcescheme_id) {
		this.resourcescheme_id = resourcescheme_id;
	}
	public String getPoli_code() {
		return poli_code;
	}
	public void setPoli_code(String poli_code) {
		this.poli_code = poli_code;
	}
	public BigDecimal getResourcemstr_id() {
		return resourcemstr_id;
	}
	public void setResourcemstr_id(BigDecimal resourcemstr_id) {
		this.resourcemstr_id = resourcemstr_id;
	}
	public String getResource_code() {
		return resource_code;
	}
	public void setResource_code(String resource_code) {
		this.resource_code = resource_code;
	}
	public String getResource_short_code() {
		return resource_short_code;
	}
	public void setResource_short_code(String resource_short_code) {
		this.resource_short_code = resource_short_code;
	}
	public String getResource_quick_code() {
		return resource_quick_code;
	}
	public void setResource_quick_code(String resource_quick_code) {
		this.resource_quick_code = resource_quick_code;
	}
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name(String resource_name) {
		this.resource_name = resource_name;
	}
	public String getResource_name_lang1() {
		return resource_name_lang1;
	}
	public void setResource_name_lang1(String resource_name_lang1) {
		this.resource_name_lang1 = resource_name_lang1;
	}
	public String getRegistration_type() {
		return registration_type;
	}
	public void setRegistration_type(String registration_type) {
		this.registration_type = registration_type;
	}
	public BigDecimal getSubspecialtymstr_id() {
		return subspecialtymstr_id;
	}
	public void setSubspecialtymstr_id(BigDecimal subspecialtymstr_id) {
		this.subspecialtymstr_id = subspecialtymstr_id;
	}
	public String getSubspecialty_desc() {
		return subspecialty_desc;
	}
	public void setSubspecialty_desc(String subspecialty_desc) {
		this.subspecialty_desc = subspecialty_desc;
	}
	public String getSubspecialty_desc_lang1() {
		return subspecialty_desc_lang1;
	}
	public void setSubspecialty_desc_lang1(String subspecialty_desc_lang1) {
		this.subspecialty_desc_lang1 = subspecialty_desc_lang1;
	}
	public BigDecimal getCareprovider_id() {
		return careprovider_id;
	}
	public void setCareprovider_id(BigDecimal careprovider_id) {
		this.careprovider_id = careprovider_id;
	}
	public String getSpecialist() {
		return specialist;
	}
	public void setSpecialist(String specialist) {
		this.specialist = specialist;
	}
	

}
