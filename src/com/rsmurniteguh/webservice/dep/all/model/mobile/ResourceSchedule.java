package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.math.BigDecimal;

public class ResourceSchedule {
	
	private BigDecimal resourcemstr_id;
	private String monday;
	private String tuesday;
	private String wednesday;
	private String thursday;
	private String friday;
	private String saturday;
	private String sunday;
	private String session_desc;
	private BigDecimal careprovide_id;
	public BigDecimal getResourcemstr_id() {
		return resourcemstr_id;
	}
	public void setResourcemstr_id(BigDecimal resourcemstr_id) {
		this.resourcemstr_id = resourcemstr_id;
	}
	public String getMonday() {
		return monday;
	}
	public void setMonday(String monday) {
		this.monday = monday;
	}
	public String getTuesday() {
		return tuesday;
	}
	public void setTuesday(String tuesday) {
		this.tuesday = tuesday;
	}
	public String getWednesday() {
		return wednesday;
	}
	public void setWednesday(String wednesday) {
		this.wednesday = wednesday;
	}
	public String getThursday() {
		return thursday;
	}
	public void setThursday(String thursday) {
		this.thursday = thursday;
	}
	public String getFriday() {
		return friday;
	}
	public void setFriday(String friday) {
		this.friday = friday;
	}
	public String getSaturday() {
		return saturday;
	}
	public void setSaturday(String saturday) {
		this.saturday = saturday;
	}
	public String getSunday() {
		return sunday;
	}
	public void setSunday(String sunday) {
		this.sunday = sunday;
	}
	public String getSession_desc() {
		return session_desc;
	}
	public void setSession_desc(String session_desc) {
		this.session_desc = session_desc;
	}
	public BigDecimal getCareprovider_id() {
		return careprovide_id;
	}
	public void setCareprovider_id(BigDecimal careprovide_id) {
		this.careprovide_id = careprovide_id;
	}
	
	
}
