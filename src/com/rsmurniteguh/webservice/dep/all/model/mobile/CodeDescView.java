package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.math.BigDecimal;

public class CodeDescView {

	private BigDecimal id;
	private String code;
	private String codeDesc;
	private BigDecimal seq;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCodeDesc() {
		return codeDesc;
	}
	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}
	public BigDecimal getSeq() {
		return seq;
	}
	public void setSeq(BigDecimal seq) {
		this.seq = seq;
	}	
	
}
