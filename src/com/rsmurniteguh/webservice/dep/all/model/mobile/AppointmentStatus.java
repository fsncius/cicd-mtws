package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class AppointmentStatus {
	private String tanggal;
	private String count_patient;
	private String doctor_name;
	private String careprovider_id;
	private String status;
	private String department;
	private String source;

	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getCount_patient() {
		return count_patient;
	}
	public void setCount_patient(String count_patient) {
		this.count_patient = count_patient;
	}
	public String getDoctor_name() {
		return doctor_name;
	}
	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getCareprovider_id() {
		return careprovider_id;
	}
	public void setCareprovider_id(String careprovider_id) {
		this.careprovider_id = careprovider_id;
	}
}
