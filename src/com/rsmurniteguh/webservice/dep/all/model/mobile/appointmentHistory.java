package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.util.List;

public class appointmentHistory {
    private String registrationCode;
    private String doctorName;
    private String careprovide_id;
	private String dateTreatment;
    private String registrationStatus;
    private String mrn;
    private String resourcemstr;
    private String lokasi;
    private String user_id;
    private String barcode;
    private String reg_id;
    private String department;
    private String queue_no;
    private String url_photo;
    private List<ListQueNum> queues; 
    private WeeklySchedule doctorSchedule;
    
    public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getRegistrationCode() {
        return registrationCode;
    }

    public void setRegistrationCode(String registrationCode) {
        this.registrationCode = registrationCode;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }
    
    public String getCareprovide_id() {
		return careprovide_id;
	}

	public void setCareprovide_id(String careprovide_id) {
		this.careprovide_id = careprovide_id;
	}
	
    public String getDateTreatment() {
        return dateTreatment;
    }

    public void setDateTreatment(String dateTreatment) {
        this.dateTreatment = dateTreatment;
    }

    public String getRegistrationStatus() {
        return registrationStatus;
    }

    public void setRegistrationStatus(String registrationStatus) {
        this.registrationStatus = registrationStatus;
    }
    public String getMrn() {
		return mrn;
	}

	public void setMrn(String mrn) {
		this.mrn = mrn;
	}

	public String getResourcemstr() {
		return resourcemstr;
	}

	public void setResourcemstr(String resourcemstr) {
		this.resourcemstr = resourcemstr;
	}

	public String getLokasi() {
		return lokasi;
	}

	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getReg_id() {
		return reg_id;
	}

	public void setReg_id(String reg_id) {
		this.reg_id = reg_id;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getQueue_no() {
		return queue_no;
	}

	public void setQueue_no(String queue_no) {
		this.queue_no = queue_no;
	}

	public String getUrl_photo() {
		return url_photo;
	}

	public void setUrl_photo(String url_photo) {
		this.url_photo = url_photo;
	}

	public List<ListQueNum> getQueues() {
		return queues;
	}

	public void setQueues(List<ListQueNum> queues) {
		this.queues = queues;
	}

	public WeeklySchedule getDoctorSchedule() {
		return doctorSchedule;
	}

	public void setDoctorSchedule(WeeklySchedule doctorSchedule) {
		this.doctorSchedule = doctorSchedule;
	}

	
}
