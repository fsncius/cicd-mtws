package com.rsmurniteguh.webservice.dep.all.model.mobile;

public class RisReportTime {

	private String id_report;
	private String request_date;
	private String patient_name;
	private String admission_id;
	private String no_rad;
	private String patient_id;
	private String patient_sex;
	private String patient_age;
	private String request_physician;
	private String modality;
	private String create_by;
	private String daftar;
	private String dipoto;
	private String dibaca;

	public String getId_report() {
		return id_report;
	}

	public void setId_report(String id_report) {
		this.id_report = id_report;
	}

	public String getRequest_date() {
		return request_date;
	}

	public void setRequest_date(String request_date) {
		this.request_date = request_date;
	}

	public String getPatient_name() {
		return patient_name;
	}

	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}

	public String getAdmission_id() {
		return admission_id;
	}

	public void setAdmission_id(String admission_id) {
		this.admission_id = admission_id;
	}

	public String getNo_rad() {
		return no_rad;
	}

	public void setNo_rad(String no_rad) {
		this.no_rad = no_rad;
	}

	public String getPatient_id() {
		return patient_id;
	}

	public void setPatient_id(String patient_id) {
		this.patient_id = patient_id;
	}

	public String getPatient_sex() {
		return patient_sex;
	}

	public void setPatient_sex(String patient_sex) {
		this.patient_sex = patient_sex;
	}

	public String getPatient_age() {
		return patient_age;
	}

	public void setPatient_age(String patient_age) {
		this.patient_age = patient_age;
	}

	public String getRequest_physician() {
		return request_physician;
	}

	public void setRequest_physician(String request_physician) {
		this.request_physician = request_physician;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public String getCreate_by() {
		return create_by;
	}

	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}

	public String getDaftar() {
		return daftar;
	}

	public void setDaftar(String daftar) {
		this.daftar = daftar;
	}

	public String getDipoto() {
		return dipoto;
	}

	public void setDipoto(String dipoto) {
		this.dipoto = dipoto;
	}

	public String getDibaca() {
		return dibaca;
	}

	public void setDibaca(String dibaca) {
		this.dibaca = dibaca;
	}

}
