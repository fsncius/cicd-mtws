package com.rsmurniteguh.webservice.dep.all.model.mobile;

import java.util.List;

public class WeeklySchedule {

	private String doctorname;
	private String doctor_url_image;
	private String specialist;
	private String careprovider;
	private String seq_doctor;
	private String seq_poli;
	private List<String> monday;
	private List<String> tuesday;
	private List<String> wednesday;
	private List<String> thursday;
	private List<String> friday;
	private List<String> saturday;
	private List<String> sunday;

	public String getDoctorname() {
		return doctorname;
	}

	public String getSeq_doctor() {
		return seq_doctor;
	}

	public void setSeq_doctor(String seq_doctor) {
		this.seq_doctor = seq_doctor;
	}

	public String getSeq_poli() {
		return seq_poli;
	}

	public void setSeq_poli(String seq_poli) {
		this.seq_poli = seq_poli;
	}

	public void setDoctorname(String doctorname) {
		this.doctorname = doctorname;
	}

	public String getSpecialist() {
		return specialist;
	}

	public void setSpecialist(String specialist) {
		this.specialist = specialist;
	}

	public List<String> getMonday() {
		return monday;
	}

	public void setMonday(List<String> monday) {
		this.monday = monday;
	}

	public List<String> getTuesday() {
		return tuesday;
	}

	public void setTuesday(List<String> tuesday) {
		this.tuesday = tuesday;
	}

	public List<String> getWednesday() {
		return wednesday;
	}

	public void setWednesday(List<String> wednesday) {
		this.wednesday = wednesday;
	}

	public List<String> getThursday() {
		return thursday;
	}

	public void setThursday(List<String> thursday) {
		this.thursday = thursday;
	}

	public List<String> getFriday() {
		return friday;
	}

	public void setFriday(List<String> friday) {
		this.friday = friday;
	}

	public List<String> getSaturday() {
		return saturday;
	}

	public void setSaturday(List<String> saturday) {
		this.saturday = saturday;
	}

	public List<String> getSunday() {
		return sunday;
	}

	public void setSunday(List<String> sunday) {
		this.sunday = sunday;
	}

	public String getCareprovider() {
		return careprovider;
	}

	public void setCareprovider(String careprovider) {
		this.careprovider = careprovider;
	}

	public String getDoctor_url_image() {
		return doctor_url_image;
	}

	public void setDoctor_url_image(String doctor_url_image) {
		this.doctor_url_image = doctor_url_image;
	}

}
