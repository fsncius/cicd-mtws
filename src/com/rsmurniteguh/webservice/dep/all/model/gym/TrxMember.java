package com.rsmurniteguh.webservice.dep.all.model.gym;

public class TrxMember {
	private String username;
	private String user_detail;
	private String balance;
	private String id_trx;
	private String payment_no;
	private String trx_amount;
	private String invoice_no;
	private String created_by;
	private String nfc_no;
	private String type;
	

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUser_detail() {
		return user_detail;
	}
	public void setUser_detail(String user_detail) {
		this.user_detail = user_detail;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getId_trx() {
		return id_trx;
	}
	public void setId_trx(String id_trx) {
		this.id_trx = id_trx;
	}
	public String getPayment_no() {
		return payment_no;
	}
	public void setPayment_no(String payment_no) {
		this.payment_no = payment_no;
	}
	public String getTrx_amount() {
		return trx_amount;
	}
	public void setTrx_amount(String trx_amount) {
		this.trx_amount = trx_amount;
	}
	public String getInvoice_no() {
		return invoice_no;
	}
	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getNfc_no() {
		return nfc_no;
	}
	public void setNfc_no(String nfc_no) {
		this.nfc_no = nfc_no;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
