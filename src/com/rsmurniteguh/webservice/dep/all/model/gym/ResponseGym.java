package com.rsmurniteguh.webservice.dep.all.model.gym;

import java.util.Map;

public class ResponseGym {
	private boolean response;
	private String message;
	private Map<String,String> data;
	public boolean isResponse() {
		return response;
	}
	public void setResponse(boolean response) {
		this.response = response;
	}
	public Map<String,String> getData() {
		return data;
	}
	public void setData(Map<String,String> data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
