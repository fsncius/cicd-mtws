package com.rsmurniteguh.webservice.dep.all.model.gym;

public class AccountBalanceParam {
	private Boolean newAccount;
	private AccountBalance accountBalance;
	private Long userId;
	

	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Boolean isNewAccount() {
		return newAccount;
	}
	public void setNewAccount(Boolean newAccount) {
		this.newAccount = newAccount;
	}
	public AccountBalance getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(AccountBalance accountBalance) {
		this.accountBalance = accountBalance;
	}
}
