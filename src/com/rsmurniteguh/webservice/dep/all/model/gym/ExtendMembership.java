package com.rsmurniteguh.webservice.dep.all.model.gym;

public class ExtendMembership {
	private String id_membership;
	private String registration_date;
	private String valid_from;
	private String valid_through;
	private String remarks;
	private String status;
	private boolean active;
	public String getRegistration_date() {
		return registration_date;
	}
	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}
	public String getValid_from() {
		return valid_from;
	}
	public void setValid_from(String valid_from) {
		this.valid_from = valid_from;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getId_membership() {
		return id_membership;
	}
	public void setId_membership(String id_membership) {
		this.id_membership = id_membership;
	}
	public String getValid_through() {
		return valid_through;
	}
	public void setValid_through(String valid_through) {
		this.valid_through = valid_through;
	}
}
