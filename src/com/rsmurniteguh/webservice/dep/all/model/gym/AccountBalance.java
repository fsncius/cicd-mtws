package com.rsmurniteguh.webservice.dep.all.model.gym;

import java.util.Map;

public class AccountBalance {
	private String accountbalance_id;
	private String username;
	private String nik;
	private String company;
	private String cardno;
	private String nfcno;
	private Map<String, String> user_detail;
	private String balance;
	private String status;
	private String remarks;
	private String created_by;
	private String serialVersionUID;
	private boolean fromNfc;
	
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getCardno() {
		return cardno;
	}
	public void setCardno(String cardno) {
		this.cardno = cardno;
	}
	public String getNfcno() {
		return nfcno;
	}
	public void setNfcno(String nfcno) {
		this.nfcno = nfcno;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getAccountbalance_id() {
		return accountbalance_id;
	}
	public void setAccountbalance_id(String accountbalance_id) {
		this.accountbalance_id = accountbalance_id;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public Map<String, String> getUser_detail() {
		return user_detail;
	}
	public void setUser_detail(Map<String, String> user_detail) {
		this.user_detail = user_detail;
	}
	public String getSerialVersionUID() {
		return serialVersionUID;
	}
	public void setSerialVersionUID(String serialVersionUID) {
		this.serialVersionUID = serialVersionUID;
	}
	public boolean isFromNfc() {
		return fromNfc;
	}
	public void setFromNfc(boolean fromNfc) {
		this.fromNfc = fromNfc;
	}
}
