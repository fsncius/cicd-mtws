package com.rsmurniteguh.webservice.dep.all.model.gym;

import java.util.List;
import java.util.Map;

public class InfoMember {
	private String accountbalance_id;
	private String valid_from;
	private String valid_through;
	private boolean valid;
	private String nfcno;
	private String firstMemberId;
	private AccountBalance info_account;
	private List<ExtendMembership> extendMemberlist;
	
	public String getAccountbalance_id() {
		return accountbalance_id;
	}
	public void setAccountbalance_id(String accountbalance_id) {
		this.accountbalance_id = accountbalance_id;
	}
	public String getValid_from() {
		return valid_from;
	}
	public void setValid_from(String valid_from) {
		this.valid_from = valid_from;
	}
	public String getValid_through() {
		return valid_through;
	}
	public void setValid_through(String valid_through) {
		this.valid_through = valid_through;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getNfcno() {
		return nfcno;
	}
	public void setNfcno(String nfcno) {
		this.nfcno = nfcno;
	}
	public AccountBalance getInfo_account() {
		return info_account;
	}
	public void setInfo_account(AccountBalance info_account) {
		this.info_account = info_account;
	}
	public List<ExtendMembership> getExtendMemberlist() {
		return extendMemberlist;
	}
	public void setExtendMemberlist(List<ExtendMembership> extendMemberlist) {
		this.extendMemberlist = extendMemberlist;
	}

	public String getFirstMemberId() {
		return firstMemberId;
	}
	public void setFirstMemberId(String firstMemberId) {
		this.firstMemberId = firstMemberId;
	}
}
