package com.rsmurniteguh.webservice.dep.all.model.gym;

public class CreateMember {
	private String valid_from;
	private String nfcno;
	private String status;
	private String created_by;
	public String getValid_from() {
		return valid_from;
	}
	public void setValid_from(String valid_from) {
		this.valid_from = valid_from;
	}
	public String getNfcno() {
		return nfcno;
	}
	public void setNfcno(String nfcno) {
		this.nfcno = nfcno;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
}
