package com.rsmurniteguh.webservice.dep.all.model;

public class ListOnlineReg {
	private String qn;
	private String rid;
	private String dt;
	private String cn;
	private String nb;
	private String tglrujuk;
	private String norujuk;
	private String dept;
	public String getQn() {
		return qn;
	}
	public void setQn(String qn) {
		this.qn = qn;
	}
	public String getRid() {
		return rid;
	}
	public void setRid(String rid) {
		this.rid = rid;
	}
	public String getDt() {
		return dt;
	}
	public void setDt(String dt) {
		this.dt = dt;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getTglrujuk() {
		return tglrujuk;
	}
	public void setTglrujuk(String tglrujuk) {
		this.tglrujuk = tglrujuk;
	}
	public String getNorujuk() {
		return norujuk;
	}
	public void setNorujuk(String norujuk) {
		this.norujuk = norujuk;
	}
	public String getNb() {
		return nb;
	}
	public void setNb(String nb) {
		this.nb = nb;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	
	
	

}
