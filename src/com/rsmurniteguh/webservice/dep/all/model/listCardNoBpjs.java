package com.rsmurniteguh.webservice.dep.all.model;

public class listCardNoBpjs {
	private String PERSON_NAME;
	//private String CARD_NO;
	private String BPJS_NO;
	
	
	public String getBPJS_NO() {
		return BPJS_NO;
	}
	public void setBPJS_NO(String bPJS_NO) {
		BPJS_NO = bPJS_NO;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
//	public String getCARD_NO() {
//		return CARD_NO;
//	}
//	public void setCARD_NO(String cARD_NO) {
//		CARD_NO = cARD_NO;
//	}
}
