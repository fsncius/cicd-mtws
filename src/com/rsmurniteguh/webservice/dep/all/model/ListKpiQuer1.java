package com.rsmurniteguh.webservice.dep.all.model;

public class ListKpiQuer1 {

	private String PERSON_ID;
	private String PERSON_NAME;
	private String SEX;
	private String BIRTH_DATE;
	private String ID_NO;
	private String TITLE;
	private String MARITAL_STATUS;
	private String NATIONALITY;
	private String RELIGION;
	private String RACE;
	private String CARD_NO;
	private String PATIENT_CLASS;
	private String CODE_DESC;
	private String UMUR;
	private String KELOMPOKUMUR;
	public String getPERSON_ID() {
		return PERSON_ID;
	}
	public void setPERSON_ID(String pERSON_ID) {
		PERSON_ID = pERSON_ID;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	public String getSEX() {
		return SEX;
	}
	public void setSEX(String sEX) {
		SEX = sEX;
	}
	public String getBIRTH_DATE() {
		return BIRTH_DATE;
	}
	public void setBIRTH_DATE(String bIRTH_DATE) {
		BIRTH_DATE = bIRTH_DATE;
	}
	public String getID_NO() {
		return ID_NO;
	}
	public void setID_NO(String iD_NO) {
		ID_NO = iD_NO;
	}
	public String getTITLE() {
		return TITLE;
	}
	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}
	public String getMARITAL_STATUS() {
		return MARITAL_STATUS;
	}
	public void setMARITAL_STATUS(String mARITAL_STATUS) {
		MARITAL_STATUS = mARITAL_STATUS;
	}
	public String getNATIONALITY() {
		return NATIONALITY;
	}
	public void setNATIONALITY(String nATIONALITY) {
		NATIONALITY = nATIONALITY;
	}
	public String getRELIGION() {
		return RELIGION;
	}
	public void setRELIGION(String rELIGION) {
		RELIGION = rELIGION;
	}
	public String getRACE() {
		return RACE;
	}
	public void setRACE(String rACE) {
		RACE = rACE;
	}
	public String getCARD_NO() {
		return CARD_NO;
	}
	public void setCARD_NO(String cARD_NO) {
		CARD_NO = cARD_NO;
	}
	public String getPATIENT_CLASS() {
		return PATIENT_CLASS;
	}
	public void setPATIENT_CLASS(String pATIENT_CLASS) {
		PATIENT_CLASS = pATIENT_CLASS;
	}
	public String getCODE_DESC() {
		return CODE_DESC;
	}
	public void setCODE_DESC(String cODE_DESC) {
		CODE_DESC = cODE_DESC;
	}	
	public String getKELOMPOKUMUR() {
		return KELOMPOKUMUR;
	}
	public void setKELOMPOKUMUR(String kELOMPOKUMUR) {
		KELOMPOKUMUR = kELOMPOKUMUR;
	}
	public String getUMUR() {
		return UMUR;
	}
	public void setUMUR(String uMUR) {
		UMUR = uMUR;
	}
	
	
	
}
