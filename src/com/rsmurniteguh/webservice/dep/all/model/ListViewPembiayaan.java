package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class ListViewPembiayaan {
	private String JENIS;
	private BigDecimal PATIENT_ID;
	private String ADMISSION_DATETIME;
	public String getJENIS() {
		return JENIS;
	}
	public void setJENIS(String jENIS) {
		JENIS = jENIS;
	}
	public BigDecimal getPATIENT_ID() {
		return PATIENT_ID;
	}
	public void setPATIENT_ID(BigDecimal pATIENT_ID) {
		PATIENT_ID = pATIENT_ID;
	}
	public String getADMISSION_DATETIME() {
		return ADMISSION_DATETIME;
	}
	public void setADMISSION_DATETIME(String aDMISSION_DATETIME) {
		ADMISSION_DATETIME = aDMISSION_DATETIME;
	}
}
