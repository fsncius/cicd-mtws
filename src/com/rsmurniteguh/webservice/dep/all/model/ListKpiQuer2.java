package com.rsmurniteguh.webservice.dep.all.model;

public class ListKpiQuer2 {
	private String DIAGNOSIS_CODE;
	private String DIAGNOSIS_DESC;
	
	public String getDIAGNOSIS_CODE() {
		return DIAGNOSIS_CODE;
	}
	public void setDIAGNOSIS_CODE(String dIAGNOSIS_CODE) {
		DIAGNOSIS_CODE = dIAGNOSIS_CODE;
	}
	public String getDIAGNOSIS_DESC() {
		return DIAGNOSIS_DESC;
	}
	public void setDIAGNOSIS_DESC(String dIAGNOSIS_DESC) {
		DIAGNOSIS_DESC = dIAGNOSIS_DESC;
	}

}
