package com.rsmurniteguh.webservice.dep.all.model;

public class UpdateCard {
	private String oldCard;
	private String typeScan;
	private String typeSetIn;
	private String newCard;
	public String getOldCard() {
		return oldCard;
	}
	public void setOldCard(String oldCard) {
		this.oldCard = oldCard;
	}
	public String getTypeScan() {
		return typeScan;
	}
	public void setTypeScan(String typeScan) {
		this.typeScan = typeScan;
	}
	public String getNewCard() {
		return newCard;
	}
	public void setNewCard(String newCard) {
		this.newCard = newCard;
	}
	public String getTypeSetIn() {
		return typeSetIn;
	}
	public void setTypeSetIn(String typeSetIn) {
		this.typeSetIn = typeSetIn;
	}
	
}
