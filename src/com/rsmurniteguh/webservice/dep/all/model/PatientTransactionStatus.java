package com.rsmurniteguh.webservice.dep.all.model;

import java.util.List;

public class PatientTransactionStatus {
	private String status = "";
	private String errorMessage = "";
	private int successCount = 0;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public int getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}
	public void addSuccessCount(){
		this.successCount += 1;
	}
}
