package com.rsmurniteguh.webservice.dep.all.model;

public class ListBedHistory {
	private String BED_NO;
	private String ROOM_DESC;
	private String WARD_DESC;
	public String getBED_NO() {
		return BED_NO;
	}
	public void setBED_NO(String bED_NO) {
		BED_NO = bED_NO;
	}
	public String getROOM_DESC() {
		return ROOM_DESC;
	}
	public void setROOM_DESC(String rOOM_DESC) {
		ROOM_DESC = rOOM_DESC;
	}
	public String getWARD_DESC() {
		return WARD_DESC;
	}
	public void setWARD_DESC(String wARD_DESC) {
		WARD_DESC = wARD_DESC;
	}
	
	
}
