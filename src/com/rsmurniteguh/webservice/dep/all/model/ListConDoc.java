package com.rsmurniteguh.webservice.dep.all.model;

public class ListConDoc {

	private String PATIENT_NAME;
	private String PATIENT_ID;
	private String VISIT_ID;
	private String CONTROL_DATETIME;
	private String CAREPROVIDER_ID;
	private String DOCTOR_NAME;
	private String RESOURCEMSTR_ID;
	private String MOBILE_PHONE_NO;
	private String CARD_NO;
	
	public String getPATIENT_NAME() {
		return PATIENT_NAME;
	}
	public void setPATIENT_NAME(String pATIENT_NAME) {
		PATIENT_NAME = pATIENT_NAME;
	}
	public String getPATIENT_ID() {
		return PATIENT_ID;
	}
	public void setPATIENT_ID(String pATIENT_ID) {
		PATIENT_ID = pATIENT_ID;
	}
	public String getVISIT_ID() {
		return VISIT_ID;
	}
	public void setVISIT_ID(String vISIT_ID) {
		VISIT_ID = vISIT_ID;
	}
	public String getCONTROL_DATETIME() {
		return CONTROL_DATETIME;
	}
	public void setCONTROL_DATETIME(String cONTROL_DATETIME) {
		CONTROL_DATETIME = cONTROL_DATETIME;
	}
	public String getCAREPROVIDER_ID() {
		return CAREPROVIDER_ID;
	}
	public void setCAREPROVIDER_ID(String cAREPROVIDER_ID) {
		CAREPROVIDER_ID = cAREPROVIDER_ID;
	}
	public String getDOCTOR_NAME() {
		return DOCTOR_NAME;
	}
	public void setDOCTOR_NAME(String dOCTOR_NAME) {
		DOCTOR_NAME = dOCTOR_NAME;
	}
	public String getRESOURCEMSTR_ID() {
		return RESOURCEMSTR_ID;
	}
	public void setRESOURCEMSTR_ID(String rESOURCEMSTR_ID) {
		RESOURCEMSTR_ID = rESOURCEMSTR_ID;
	}
	public String getMOBILE_PHONE_NO() {
		return MOBILE_PHONE_NO;
	}
	public void setMOBILE_PHONE_NO(String mOBILE_PHONE_NO) {
		MOBILE_PHONE_NO = mOBILE_PHONE_NO;
	}
	public String getCARD_NO() {
		return CARD_NO;
	}
	public void setCARD_NO(String cARD_NO) {
		CARD_NO = cARD_NO;
	}
	
	
}
