package com.rsmurniteguh.webservice.dep.all.model;

public class ListWardRoom {

	private String ward_desc;
	private String mrn;
	private String room_desc;
	private String bed_no;
	private String bad_status;
	private String patient_clas;
	private String accomm_desc;
	private String person_name;
	private String admission_datetime;
	private String discharge_datetime;
	public String getWard_desc() {
		return ward_desc;
	}
	public void setWard_desc(String ward_desc) {
		this.ward_desc = ward_desc;
	}
	public String getRoom_desc() {
		return room_desc;
	}
	public void setRoom_desc(String room_desc) {
		this.room_desc = room_desc;
	}
	public String getBed_no() {
		return bed_no;
	}
	public void setBed_no(String bed_no) {
		this.bed_no = bed_no;
	}
	public String getBad_status() {
		return bad_status;
	}
	public void setBad_status(String bad_status) {
		this.bad_status = bad_status;
	}
	public String getPatient_clas() {
		return patient_clas;
	}
	public void setPatient_clas(String patient_clas) {
		this.patient_clas = patient_clas;
	}
	public String getAccomm_desc() {
		return accomm_desc;
	}
	public void setAccomm_desc(String accomm_desc) {
		this.accomm_desc = accomm_desc;
	}
	public String getPerson_name() {
		return person_name;
	}
	public void setPerson_name(String person_name) {
		this.person_name = person_name;
	}
	public String getMrn() {
		return mrn;
	}
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	public String getAdmission_datetime() {
		return admission_datetime;
	}
	public void setAdmission_datetime(String admission_datetime) {
		this.admission_datetime = admission_datetime;
	}
	public String getDischarge_datetime() {
		return discharge_datetime;
	}
	public void setDischarge_datetime(String discharge_datetime) {
		this.discharge_datetime = discharge_datetime;
	}
	
	
}
