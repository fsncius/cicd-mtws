package com.rsmurniteguh.webservice.dep.all.model;

import java.math.BigDecimal;

public class listpolyres {

	private BigDecimal USERMSTR_ID;
	private BigDecimal RESOURCEMSTR_ID;
	private String PERSON_NAME;
	public BigDecimal getUSERMSTR_ID() {
		return USERMSTR_ID;
	}
	public void setUSERMSTR_ID(BigDecimal uSERMSTR_ID) {
		USERMSTR_ID = uSERMSTR_ID;
	}
	public BigDecimal getRESOURCEMSTR_ID() {
		return RESOURCEMSTR_ID;
	}
	public void setRESOURCEMSTR_ID(BigDecimal rESOURCEMSTR_ID) {
		RESOURCEMSTR_ID = rESOURCEMSTR_ID;
	}
	public String getPERSON_NAME() {
		return PERSON_NAME;
	}
	public void setPERSON_NAME(String pERSON_NAME) {
		PERSON_NAME = pERSON_NAME;
	}
	
	
}
