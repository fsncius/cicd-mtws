package com.rsmurniteguh.webservice.dep.all.model;

public class ListCodeCar {

	private String CODE_CAT;
	private String CODE_ABBR;
	private String CODE_DESC;
	public String getCODE_CAT() {
		return CODE_CAT;
	}
	public void setCODE_CAT(String cODE_CAT) {
		CODE_CAT = cODE_CAT;
	}
	public String getCODE_ABBR() {
		return CODE_ABBR;
	}
	public void setCODE_ABBR(String cODE_ABBR) {
		CODE_ABBR = cODE_ABBR;
	}
	public String getCODE_DESC() {
		return CODE_DESC;
	}
	public void setCODE_DESC(String cODE_DESC) {
		CODE_DESC = cODE_DESC;
	}
	
	
}
