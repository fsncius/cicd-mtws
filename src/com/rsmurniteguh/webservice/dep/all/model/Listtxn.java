package com.rsmurniteguh.webservice.dep.all.model;

public class Listtxn {

	private String TXN_DESC;
	private String TXN_AMOUNT;
	private String TXN_DATETIME;
	public String getTXN_DESC() {
		return TXN_DESC;
	}
	public void setTXN_DESC(String tXN_DESC) {
		TXN_DESC = tXN_DESC;
	}
	public String getTXN_AMOUNT() {
		return TXN_AMOUNT;
	}
	public void setTXN_AMOUNT(String tXN_AMOUNT) {
		TXN_AMOUNT = tXN_AMOUNT;
	}
	public String getTXN_DATETIME() {
		return TXN_DATETIME;
	}
	public void setTXN_DATETIME(String tXN_DATETIME) {
		TXN_DATETIME = tXN_DATETIME;
	}
	
	
	
}
