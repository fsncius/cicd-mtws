package com.rsmurniteguh.webservice.dep.all.model;

public class ListResourceMstrPolyName {
	private String POLI_CODE;
	private String POLI_NAME;
	private String subspecialty_code;
	
	public String getPOLI_CODE() {
		return POLI_CODE;
	}

	public void setPOLI_CODE(String pOLI_CODE) {
		POLI_CODE = pOLI_CODE;
	}

	public String getPOLI_NAME() {
		return POLI_NAME;
	}

	public void setPOLI_NAME(String pOLI_NAME) {
		POLI_NAME = pOLI_NAME;
	}

	public String getSubspecialty_code() {
		return subspecialty_code;
	}

	public void setSubspecialty_code(String subspecialty_code) {
		this.subspecialty_code = subspecialty_code;
	}
	

}
