package com.rsmurniteguh.webservice.dep.all.model.hris;

public class CutiAtasan {
	private String idkaryawan;
	private String kodejabatan;
	public String getKodejabatan() {
		return kodejabatan;
	}
	public void setKodejabatan(String kodejabatan) {
		this.kodejabatan = kodejabatan;
	}
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
}
