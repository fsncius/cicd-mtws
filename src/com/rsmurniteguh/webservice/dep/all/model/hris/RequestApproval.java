package com.rsmurniteguh.webservice.dep.all.model.hris;

public class RequestApproval {
	private String kode;
	private String nama;
	private String tanggal;
	private String keterangan;
	private String action;
	private String controller;
	private String lamaexpired;
	private String tipe;
	private String statusall;
	private String idrequester;
	private String fingerprintid;
	private String kodedetail;
	private String level;
	private String status;
	private String idkaryawan;
	private String notifikasi;
	private String alasan;
	private String tgl_keluar;
	private String tgl_masuk;
	private String jam_keluar;
	private String jam_masuk;
	
	public String getKode() {
		return kode;
	}
	public void setKode(String kode) {
		this.kode = kode;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getController() {
		return controller;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	public String getLamaexpired() {
		return lamaexpired;
	}
	public void setLamaexpired(String lamaexpired) {
		this.lamaexpired = lamaexpired;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public String getStatusall() {
		return statusall;
	}
	public void setStatusall(String statusall) {
		this.statusall = statusall;
	}
	public String getIdrequester() {
		return idrequester;
	}
	public void setIdrequester(String idrequester) {
		this.idrequester = idrequester;
	}
	public String getFingerprintid() {
		return fingerprintid;
	}
	public void setFingerprintid(String fingerprintid) {
		this.fingerprintid = fingerprintid;
	}
	public String getKodedetail() {
		return kodedetail;
	}
	public void setKodedetail(String kodedetail) {
		this.kodedetail = kodedetail;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getNotifikasi() {
		return notifikasi;
	}
	public void setNotifikasi(String notifikasi) {
		this.notifikasi = notifikasi;
	}
	public String getAlasan() {
		return alasan;
	}
	public void setAlasan(String alasan) {
		this.alasan = alasan;
	}
	public String getTgl_keluar() {
		return tgl_keluar;
	}
	public void setTgl_keluar(String tgl_keluar) {
		this.tgl_keluar = tgl_keluar;
	}
	public String getJam_keluar() {
		return jam_keluar;
	}
	public void setJam_keluar(String jam_keluar) {
		this.jam_keluar = jam_keluar;
	}
	public String getTgl_masuk() {
		return tgl_masuk;
	}
	public void setTgl_masuk(String tgl_masuk) {
		this.tgl_masuk = tgl_masuk;
	}
	public String getJam_masuk() {
		return jam_masuk;
	}
	public void setJam_masuk(String jam_masuk) {
		this.jam_masuk = jam_masuk;
	}
}
