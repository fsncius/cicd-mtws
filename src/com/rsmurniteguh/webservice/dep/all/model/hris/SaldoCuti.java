package com.rsmurniteguh.webservice.dep.all.model.hris;

public class SaldoCuti {
	private String idkaryawan;
	private String awal;
	private String masuk;
	private String keluar;
	private String sisa;
	private String statusupdate;
	private String periode;
	private String tglexpired;
	private String tglexpired_sisatahunlalu;
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getAwal() {
		return awal;
	}
	public void setAwal(String awal) {
		this.awal = awal;
	}
	public String getMasuk() {
		return masuk;
	}
	public void setMasuk(String masuk) {
		this.masuk = masuk;
	}
	public String getKeluar() {
		return keluar;
	}
	public void setKeluar(String keluar) {
		this.keluar = keluar;
	}
	public String getSisa() {
		return sisa;
	}
	public void setSisa(String sisa) {
		this.sisa = sisa;
	}
	public String getStatusupdate() {
		return statusupdate;
	}
	public void setStatusupdate(String statusupdate) {
		this.statusupdate = statusupdate;
	}
	public String getTglexpired() {
		return tglexpired;
	}
	public void setTglexpired(String tglexpired) {
		this.tglexpired = tglexpired;
	}
	public String getTglexpired_sisatahunlalu() {
		return tglexpired_sisatahunlalu;
	}
	public void setTglexpired_sisatahunlalu(String tglexpired_sisatahunlalu) {
		this.tglexpired_sisatahunlalu = tglexpired_sisatahunlalu;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
}
