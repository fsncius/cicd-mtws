package com.rsmurniteguh.webservice.dep.all.model.hris;

public class Karyawan {
	private String idkaryawan;
	private String fingerprintid;
	private String kodebagian;
	private String kodejabatan;
	private String kodecabang;
	private String kodecabangpembantu;
	private String noktp;
	private String nosim;
	private String npwp;
	private String nama;
	private String tempatlahir;
	private String tgllahir;
	private String email;
	private String alamatpermanent;
	private String alamatsementara;
	private String rtrw;
	private String kel;
	private String kec;
	private String agama;
	private String statusnikah;
	private String wn;
	private String kelamin;
	private String suku;
	private String hp;
	private String telepon;
	private String ukuranbaju;
	private String ukurancelana;
	private String ukuransepatu;
	private String tglbergabung;
	private String bank;
	private String norek;
	private String rekatasnama;
	private String hpkerabat;
	private String statusaktif;
	private String statuskerja;
	private String mulaikontrak;
	private String akhirkontrak;
	private String foto;
	private String tglinput;
	private String tglubah;
	private String useraktif;
	private String iduser;
	private String dlt;
	private String kodemesin;
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getFingerprintid() {
		return fingerprintid;
	}
	public void setFingerprintid(String fingerprintid) {
		this.fingerprintid = fingerprintid;
	}
	public String getKodebagian() {
		return kodebagian;
	}
	public void setKodebagian(String kodebagian) {
		this.kodebagian = kodebagian;
	}
	public String getKodejabatan() {
		return kodejabatan;
	}
	public void setKodejabatan(String kodejabatan) {
		this.kodejabatan = kodejabatan;
	}
	public String getKodecabang() {
		return kodecabang;
	}
	public void setKodecabang(String kodecabang) {
		this.kodecabang = kodecabang;
	}
	public String getKodecabangpembantu() {
		return kodecabangpembantu;
	}
	public void setKodecabangpembantu(String kodecabangpembantu) {
		this.kodecabangpembantu = kodecabangpembantu;
	}
	public String getNoktp() {
		return noktp;
	}
	public void setNoktp(String noktp) {
		this.noktp = noktp;
	}
	public String getNosim() {
		return nosim;
	}
	public void setNosim(String nosim) {
		this.nosim = nosim;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getTempatlahir() {
		return tempatlahir;
	}
	public void setTempatlahir(String tempatlahir) {
		this.tempatlahir = tempatlahir;
	}
	public String getTgllahir() {
		return tgllahir;
	}
	public void setTgllahir(String tgllahir) {
		this.tgllahir = tgllahir;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAlamatpermanent() {
		return alamatpermanent;
	}
	public void setAlamatpermanent(String alamatpermanent) {
		this.alamatpermanent = alamatpermanent;
	}
	public String getAlamatsementara() {
		return alamatsementara;
	}
	public void setAlamatsementara(String alamatsementara) {
		this.alamatsementara = alamatsementara;
	}
	public String getRtrw() {
		return rtrw;
	}
	public void setRtrw(String rtrw) {
		this.rtrw = rtrw;
	}
	public String getKel() {
		return kel;
	}
	public void setKel(String kel) {
		this.kel = kel;
	}
	public String getKec() {
		return kec;
	}
	public void setKec(String kec) {
		this.kec = kec;
	}
	public String getAgama() {
		return agama;
	}
	public void setAgama(String agama) {
		this.agama = agama;
	}
	public String getStatusnikah() {
		return statusnikah;
	}
	public void setStatusnikah(String statusnikah) {
		this.statusnikah = statusnikah;
	}
	public String getWn() {
		return wn;
	}
	public void setWn(String wn) {
		this.wn = wn;
	}
	public String getKelamin() {
		return kelamin;
	}
	public void setKelamin(String kelamin) {
		this.kelamin = kelamin;
	}
	public String getSuku() {
		return suku;
	}
	public void setSuku(String suku) {
		this.suku = suku;
	}
	public String getHp() {
		return hp;
	}
	public void setHp(String hp) {
		this.hp = hp;
	}
	public String getTelepon() {
		return telepon;
	}
	public void setTelepon(String telepon) {
		this.telepon = telepon;
	}
	public String getUkuranbaju() {
		return ukuranbaju;
	}
	public void setUkuranbaju(String ukuranbaju) {
		this.ukuranbaju = ukuranbaju;
	}
	public String getUkurancelana() {
		return ukurancelana;
	}
	public void setUkurancelana(String ukurancelana) {
		this.ukurancelana = ukurancelana;
	}
	public String getUkuransepatu() {
		return ukuransepatu;
	}
	public void setUkuransepatu(String ukuransepatu) {
		this.ukuransepatu = ukuransepatu;
	}
	public String getTglbergabung() {
		return tglbergabung;
	}
	public void setTglbergabung(String tglbergabung) {
		this.tglbergabung = tglbergabung;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getNorek() {
		return norek;
	}
	public void setNorek(String norek) {
		this.norek = norek;
	}
	public String getRekatasnama() {
		return rekatasnama;
	}
	public void setRekatasnama(String rekatasnama) {
		this.rekatasnama = rekatasnama;
	}
	public String getHpkerabat() {
		return hpkerabat;
	}
	public void setHpkerabat(String hpkerabat) {
		this.hpkerabat = hpkerabat;
	}
	public String getStatusaktif() {
		return statusaktif;
	}
	public void setStatusaktif(String statusaktif) {
		this.statusaktif = statusaktif;
	}
	public String getStatuskerja() {
		return statuskerja;
	}
	public void setStatuskerja(String statuskerja) {
		this.statuskerja = statuskerja;
	}
	public String getMulaikontrak() {
		return mulaikontrak;
	}
	public void setMulaikontrak(String mulaikontrak) {
		this.mulaikontrak = mulaikontrak;
	}
	public String getAkhirkontrak() {
		return akhirkontrak;
	}
	public void setAkhirkontrak(String akhirkontrak) {
		this.akhirkontrak = akhirkontrak;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getTglinput() {
		return tglinput;
	}
	public void setTglinput(String tglinput) {
		this.tglinput = tglinput;
	}
	public String getTglubah() {
		return tglubah;
	}
	public void setTglubah(String tglubah) {
		this.tglubah = tglubah;
	}
	public String getUseraktif() {
		return useraktif;
	}
	public void setUseraktif(String useraktif) {
		this.useraktif = useraktif;
	}
	public String getIduser() {
		return iduser;
	}
	public void setIduser(String iduser) {
		this.iduser = iduser;
	}
	public String getDlt() {
		return dlt;
	}
	public void setDlt(String dlt) {
		this.dlt = dlt;
	}
	public String getKodemesin() {
		return kodemesin;
	}
	public void setKodemesin(String kodemesin) {
		this.kodemesin = kodemesin;
	}
	
}
