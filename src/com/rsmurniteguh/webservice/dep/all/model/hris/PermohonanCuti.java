package com.rsmurniteguh.webservice.dep.all.model.hris;

public class PermohonanCuti {
	private String jeniscuti;
	private String kodecuti;
	private String tglinput;
	private String tglmulai;
	private String tglakhir;
	private String idkaryawan;
	private String infotambahan;
	private String lamacuti;
	private String bonuscuti;
	private String keterangan;
	private String kodeapprove;
	private String dlt;
	private String jlhpax;
	private String progressing;
	private String approved;
	private String rejected;
	private String statusall;
	private String lamaexpired;
	private String nama;
	private String kodemesin;
	
	public String getJeniscuti() {
		return jeniscuti;
	}
	public void setJeniscuti(String jeniscuti) {
		this.jeniscuti = jeniscuti;
	}
	public String getKodecuti() {
		return kodecuti;
	}
	public void setKodecuti(String kodecuti) {
		this.kodecuti = kodecuti;
	}
	public String getTglinput() {
		return tglinput;
	}
	public void setTglinput(String tglinput) {
		this.tglinput = tglinput;
	}
	public String getTglmulai() {
		return tglmulai;
	}
	public void setTglmulai(String tglmulai) {
		this.tglmulai = tglmulai;
	}
	public String getTglakhir() {
		return tglakhir;
	}
	public void setTglakhir(String tglakhir) {
		this.tglakhir = tglakhir;
	}
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getInfotambahan() {
		return infotambahan;
	}
	public void setInfotambahan(String infotambahan) {
		this.infotambahan = infotambahan;
	}
	public String getLamacuti() {
		return lamacuti;
	}
	public void setLamacuti(String lamacuti) {
		this.lamacuti = lamacuti;
	}
	public String getBonuscuti() {
		return bonuscuti;
	}
	public void setBonuscuti(String bonuscuti) {
		this.bonuscuti = bonuscuti;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getKodeapprove() {
		return kodeapprove;
	}
	public void setKodeapprove(String kodeapprove) {
		this.kodeapprove = kodeapprove;
	}
	public String getDlt() {
		return dlt;
	}
	public void setDlt(String dlt) {
		this.dlt = dlt;
	}
	public String getJlhpax() {
		return jlhpax;
	}
	public void setJlhpax(String jlhpax) {
		this.jlhpax = jlhpax;
	}
	public String getProgressing() {
		return progressing;
	}
	public void setProgressing(String progressing) {
		this.progressing = progressing;
	}
	public String getApproved() {
		return approved;
	}
	public void setApproved(String approved) {
		this.approved = approved;
	}
	public String getStatusall() {
		return statusall;
	}
	public void setStatusall(String statusall) {
		this.statusall = statusall;
	}
	public String getLamaexpired() {
		return lamaexpired;
	}
	public void setLamaexpired(String lamaexpired) {
		this.lamaexpired = lamaexpired;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getKodemesin() {
		return kodemesin;
	}
	public void setKodemesin(String kodemesin) {
		this.kodemesin = kodemesin;
	}
	public String getRejected() {
		return rejected;
	}
	public void setRejected(String rejected) {
		this.rejected = rejected;
	}
}
