package com.rsmurniteguh.webservice.dep.all.model.hris;

public class EmployeAttendance {
	

	private String fingerprintid;
	private String personalcalendarnotes;
	private String hari;
	private String jammasuk;
	private String jamkeluar;
	private String catatan;
	private String personalcalendarstatus;
	private String personalcalendarreason;
	private String jammulai;
	private String jampulang;
	private String terlambat;
	private String cepatpulang;
	private String jamefektif;
	private String lembur;
	private String personalcalendarid;
	private String shiftcode;
	private String kodemesin;
	private String patterncode;
	private String personalcalendardate;
	private String nonWorkingDay;
	
	
	public String getPersonalcalendardate() {
		return personalcalendardate;
	}
	public void setPersonalcalendardate(String personalcalendardate) {
		this.personalcalendardate = personalcalendardate;
	}
	public String getFingerprintid() {
		return fingerprintid;
	}
	public void setFingerprintid(String fingerprintid) {
		this.fingerprintid = fingerprintid;
	}
	public String getPersonalcalendarnotes() {
		return personalcalendarnotes;
	}
	public void setPersonalcalendarnotes(String personalcalendarnotes) {
		this.personalcalendarnotes = personalcalendarnotes;
	}
	public String getHari() {
		return hari;
	}
	public void setHari(String hari) {
		this.hari = hari;
	}
	public String getJammasuk() {
		return jammasuk;
	}
	public void setJammasuk(String jammasuk) {
		this.jammasuk = jammasuk;
	}
	public String getJamkeluar() {
		return jamkeluar;
	}
	public void setJamkeluar(String jamkeluar) {
		this.jamkeluar = jamkeluar;
	}
	public String getCatatan() {
		return catatan;
	}
	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}
	public String getPersonalcalendarstatus() {
		return personalcalendarstatus;
	}
	public void setPersonalcalendarstatus(String personalcalendarstatus) {
		this.personalcalendarstatus = personalcalendarstatus;
	}
	public String getPersonalcalendarreason() {
		return personalcalendarreason;
	}
	public void setPersonalcalendarreason(String personalcalendarreason) {
		this.personalcalendarreason = personalcalendarreason;
	}
	public String getJammulai() {
		return jammulai;
	}
	public void setJammulai(String jammulai) {
		this.jammulai = jammulai;
	}
	public String getJampulang() {
		return jampulang;
	}
	public void setJampulang(String jampulang) {
		this.jampulang = jampulang;
	}
	public String getTerlambat() {
		return terlambat;
	}
	public void setTerlambat(String terlambat) {
		this.terlambat = terlambat;
	}
	public String getCepatpulang() {
		return cepatpulang;
	}
	public void setCepatpulang(String cepatpulang) {
		this.cepatpulang = cepatpulang;
	}
	public String getJamefektif() {
		return jamefektif;
	}
	public void setJamefektif(String jamefektif) {
		this.jamefektif = jamefektif;
	}
	public String getLembur() {
		return lembur;
	}
	public void setLembur(String lembur) {
		this.lembur = lembur;
	}
	public String getPersonalcalendarid() {
		return personalcalendarid;
	}
	public void setPersonalcalendarid(String personalcalendarid) {
		this.personalcalendarid = personalcalendarid;
	}
	public String getShiftcode() {
		return shiftcode;
	}
	public void setShiftcode(String shiftcode) {
		this.shiftcode = shiftcode;
	}
	public String getKodemesin() {
		return kodemesin;
	}
	public void setKodemesin(String kodemesin) {
		this.kodemesin = kodemesin;
	}
	public String getPatterncode() {
		return patterncode;
	}
	public void setPatterncode(String patterncode) {
		this.patterncode = patterncode;
	}
	public String getNonWorkingDay() {
		return nonWorkingDay;
	}
	public void setNonWorkingDay(String nonWorkingDay) {
		this.nonWorkingDay = nonWorkingDay;
	}
	
	
}
