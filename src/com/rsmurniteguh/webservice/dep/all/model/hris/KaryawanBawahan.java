package com.rsmurniteguh.webservice.dep.all.model.hris;

public class KaryawanBawahan {
	private String idkaryawan;
	private String nama;
	private String jabatan;
	private String department;
	private String hp;
	private String email;
	private String foto;
	private String kelamin;
	private String statusaktif;
	private String tglbergabung;
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getJabatan() {
		return jabatan;
	}
	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getHp() {
		return hp;
	}
	public void setHp(String hp) {
		this.hp = hp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getStatusaktif() {
		return statusaktif;
	}
	public void setStatusaktif(String statusaktif) {
		this.statusaktif = statusaktif;
	}
	public String getTglbergabung() {
		return tglbergabung;
	}
	public void setTglbergabung(String tglbergabung) {
		this.tglbergabung = tglbergabung;
	}
	public String getKelamin() {
		return kelamin;
	}
	public void setKelamin(String kelamin) {
		this.kelamin = kelamin;
	}
	
}
