package com.rsmurniteguh.webservice.dep.all.model.hris;

import java.util.List;

public class CreateCutiView {
	private List<SimpleKaryawan> karyawan;
	private List<SimpleKaryawan> karyawanpenting;
	private SaldoCuti saldocuti;
	
	public List<SimpleKaryawan> getKaryawan() {
		return karyawan;
	}
	public void setKaryawan(List<SimpleKaryawan> karyawan) {
		this.karyawan = karyawan;
	}
	public List<SimpleKaryawan> getKaryawanpenting() {
		return karyawanpenting;
	}
	public void setKaryawanpenting(List<SimpleKaryawan> karyawanpenting) {
		this.karyawanpenting = karyawanpenting;
	}
	public SaldoCuti getSaldocuti() {
		return saldocuti;
	}
	public void setSaldocuti(SaldoCuti saldocuti) {
		this.saldocuti = saldocuti;
	}
}
