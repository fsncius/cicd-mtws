package com.rsmurniteguh.webservice.dep.all.model.hris;

public class PermohonanIzin {
	private String kodeizin;
	private String tglpermohonan;
	private String tglizinkeluar;
	private String tglizinmasuk;
	private String jamizinmasuk;
	private String jamizinkeluar;
	private String alasan;
	private String idkaryawan;
	private String jlhpax;
	private String progressing;
	private String approved;
	private String rejected;
	private String statusall;
	private String dlt;
	private String kendaraan;
	private String supir;
	private String dokumen1;
	private String dokumen2;
	private String dokumen3;
	private String kodeapprove;
	private String lamaexpired;
	private String namakaryawan;
	private String keterangan;
	private String jenispotong;
	private String approvalby;
	private String lokasi;
	private String kodemesin;
	private String tipeizin;
	
	public String getKodeizin() {
		return kodeizin;
	}
	public void setKodeizin(String kodeizin) {
		this.kodeizin = kodeizin;
	}
	public String getTglpermohonan() {
		return tglpermohonan;
	}
	public void setTglpermohonan(String tglpermohonan) {
		this.tglpermohonan = tglpermohonan;
	}
	public String getJamizinmasuk() {
		return jamizinmasuk;
	}
	public void setJamizinmasuk(String jamizinmasuk) {
		this.jamizinmasuk = jamizinmasuk;
	}
	public String getJamizinkeluar() {
		return jamizinkeluar;
	}
	public void setJamizinkeluar(String jamizinkeluar) {
		this.jamizinkeluar = jamizinkeluar;
	}
	public String getAlasan() {
		return alasan;
	}
	public void setAlasan(String alasan) {
		this.alasan = alasan;
	}
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getJlhpax() {
		return jlhpax;
	}
	public void setJlhpax(String jlhpax) {
		this.jlhpax = jlhpax;
	}
	public String getProgressing() {
		return progressing;
	}
	public void setProgressing(String progressing) {
		this.progressing = progressing;
	}
	public String getApproved() {
		return approved;
	}
	public void setApproved(String approved) {
		this.approved = approved;
	}
	public String getRejected() {
		return rejected;
	}
	public void setRejected(String rejected) {
		this.rejected = rejected;
	}
	public String getStatusall() {
		return statusall;
	}
	public void setStatusall(String statusall) {
		this.statusall = statusall;
	}
	public String getDlt() {
		return dlt;
	}
	public void setDlt(String dlt) {
		this.dlt = dlt;
	}
	public String getKendaraan() {
		return kendaraan;
	}
	public void setKendaraan(String kendaraan) {
		this.kendaraan = kendaraan;
	}
	public String getSupir() {
		return supir;
	}
	public void setSupir(String supir) {
		this.supir = supir;
	}
	public String getDokumen1() {
		return dokumen1;
	}
	public void setDokumen1(String dokumen1) {
		this.dokumen1 = dokumen1;
	}
	public String getDokumen2() {
		return dokumen2;
	}
	public void setDokumen2(String dokumen2) {
		this.dokumen2 = dokumen2;
	}
	public String getDokumen3() {
		return dokumen3;
	}
	public void setDokumen3(String dokumen3) {
		this.dokumen3 = dokumen3;
	}
	public String getKodeapprove() {
		return kodeapprove;
	}
	public void setKodeapprove(String kodeapprove) {
		this.kodeapprove = kodeapprove;
	}
	public String getLamaexpired() {
		return lamaexpired;
	}
	public void setLamaexpired(String lamaexpired) {
		this.lamaexpired = lamaexpired;
	}
	public String getNamakaryawan() {
		return namakaryawan;
	}
	public void setNamakaryawan(String namakaryawan) {
		this.namakaryawan = namakaryawan;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getJenispotong() {
		return jenispotong;
	}
	public void setJenispotong(String jenispotong) {
		this.jenispotong = jenispotong;
	}
	public String getApprovalby() {
		return approvalby;
	}
	public void setApprovalby(String approvalby) {
		this.approvalby = approvalby;
	}
	public String getLokasi() {
		return lokasi;
	}
	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}
	public String getKodemesin() {
		return kodemesin;
	}
	public void setKodemesin(String kodemesin) {
		this.kodemesin = kodemesin;
	}
	public String getTipeizin() {
		return tipeizin;
	}
	public void setTipeizin(String tipeizin) {
		this.tipeizin = tipeizin;
	}
	public String getTglizinkeluar() {
		return tglizinkeluar;
	}
	public void setTglizinkeluar(String tglizinkeluar) {
		this.tglizinkeluar = tglizinkeluar;
	}
	public String getTglizinmasuk() {
		return tglizinmasuk;
	}
	public void setTglizinmasuk(String tglizinmasuk) {
		this.tglizinmasuk = tglizinmasuk;
	}
}
