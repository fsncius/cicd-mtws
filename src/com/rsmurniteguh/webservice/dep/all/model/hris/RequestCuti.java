package com.rsmurniteguh.webservice.dep.all.model.hris;

public class RequestCuti {

	private String kodecuti;
	private String idkaryawan;
	private String jeniscuti;
	private String tglmulai;
	private String tglakhir;
	private String infotambahan;
	private String keterangan;
	private String lamacuti;
	private String bonuscuti;
	private String kodeapprove;
	private String tglinput;
	private String tgledit;
	private String createby;
	private String dlt;
	private String fingerprintid;
	private String nama;
	private String lamaexpired;
	private String statusall;
	private String kodemesin;
	private String foto;
	private String kodecabang;
	
	public String getKodecuti() {
		return kodecuti;
	}
	public void setKodecuti(String kodecuti) {
		this.kodecuti = kodecuti;
	}
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getJeniscuti() {
		return jeniscuti;
	}
	public void setJeniscuti(String jeniscuti) {
		this.jeniscuti = jeniscuti;
	}
	public String getTglmulai() {
		return tglmulai;
	}
	public void setTglmulai(String tglmulai) {
		this.tglmulai = tglmulai;
	}
	public String getTglakhir() {
		return tglakhir;
	}
	public void setTglakhir(String tglakhir) {
		this.tglakhir = tglakhir;
	}
	public String getInfotambahan() {
		return infotambahan;
	}
	public void setInfotambahan(String infotambahan) {
		this.infotambahan = infotambahan;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getLamacuti() {
		return lamacuti;
	}
	public void setLamacuti(String lamacuti) {
		this.lamacuti = lamacuti;
	}
	public String getBonuscuti() {
		return bonuscuti;
	}
	public void setBonuscuti(String bonuscuti) {
		this.bonuscuti = bonuscuti;
	}
	public String getKodeapprove() {
		return kodeapprove;
	}
	public void setKodeapprove(String kodeapprove) {
		this.kodeapprove = kodeapprove;
	}
	public String getTglinput() {
		return tglinput;
	}
	public void setTglinput(String tglinput) {
		this.tglinput = tglinput;
	}
	public String getTgledit() {
		return tgledit;
	}
	public void setTgledit(String tgledit) {
		this.tgledit = tgledit;
	}
	public String getCreateby() {
		return createby;
	}
	public void setCreateby(String createby) {
		this.createby = createby;
	}
	public String getDlt() {
		return dlt;
	}
	public void setDlt(String dlt) {
		this.dlt = dlt;
	}
	public String getFingerprintid() {
		return fingerprintid;
	}
	public void setFingerprintid(String fingerprintid) {
		this.fingerprintid = fingerprintid;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getLamaexpired() {
		return lamaexpired;
	}
	public void setLamaexpired(String lamaexpired) {
		this.lamaexpired = lamaexpired;
	}
	public String getStatusall() {
		return statusall;
	}
	public void setStatusall(String statusall) {
		this.statusall = statusall;
	}
	public String getKodemesin() {
		return kodemesin;
	}
	public void setKodemesin(String kodemesin) {
		this.kodemesin = kodemesin;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getKodecabang() {
		return kodecabang;
	}
	public void setKodecabang(String kodecabang) {
		this.kodecabang = kodecabang;
	}
	
}
