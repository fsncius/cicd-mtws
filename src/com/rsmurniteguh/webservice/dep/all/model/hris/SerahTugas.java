package com.rsmurniteguh.webservice.dep.all.model.hris;

public class SerahTugas {
	private String idkaryawan;
	private String tanggal;
	private String tugas;
	private String lapmasalahpenting;
	public String getLapmasalahpenting() {
		return lapmasalahpenting;
	}
	public void setLapmasalahpenting(String lapmasalahpenting) {
		this.lapmasalahpenting = lapmasalahpenting;
	}
	public String getTugas() {
		return tugas;
	}
	public void setTugas(String tugas) {
		this.tugas = tugas;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
}
