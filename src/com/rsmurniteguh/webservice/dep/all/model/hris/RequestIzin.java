package com.rsmurniteguh.webservice.dep.all.model.hris;

public class RequestIzin {
	
	private String tipeizin;
	private String idrequester;
	private String tglizinkeluar;
	private String tglizinmasuk;
	private String jamizinkeluar;
	private String jamizinmasuk;
	private String kendaraan;
	private String supir;
	private String dokumen1;
	private String dokumen2;
	private String dokumen3;
	private String kodeapprove;
	private String tglpermohonan;
	private String dlt;
	private String alasanrequester;
	private String namarequester;
	private String lamaexpired;
	private String fingerprintid;
	private String statusall;
	private String kodemesin;
	private String foto;
	private String kodecabang;
	private String jenispotong;
	private String bonuscuti;
	private String lokasi;
	private String kodeizin;
	
	
	public String getKodeizin() {
		return kodeizin;
	}
	public void setKodeizin(String kodeizin) {
		this.kodeizin = kodeizin;
	}
	public String getTipeizin() {
		return tipeizin;
	}
	public void setTipeizin(String tipeizin) {
		this.tipeizin = tipeizin;
	}
	public String getIdrequester() {
		return idrequester;
	}
	public void setIdrequester(String idrequester) {
		this.idrequester = idrequester;
	}
	public String getTglizinkeluar() {
		return tglizinkeluar;
	}
	public void setTglizinkeluar(String tglizinkeluar) {
		this.tglizinkeluar = tglizinkeluar;
	}
	public String getTglizinmasuk() {
		return tglizinmasuk;
	}
	public void setTglizinmasuk(String tglizinmasuk) {
		this.tglizinmasuk = tglizinmasuk;
	}
	public String getJamizinkeluar() {
		return jamizinkeluar;
	}
	public void setJamizinkeluar(String jamizinkeluar) {
		this.jamizinkeluar = jamizinkeluar;
	}
	public String getJamizinmasuk() {
		return jamizinmasuk;
	}
	public void setJamizinmasuk(String jamizinmasuk) {
		this.jamizinmasuk = jamizinmasuk;
	}
	public String getKendaraan() {
		return kendaraan;
	}
	public void setKendaraan(String kendaraan) {
		this.kendaraan = kendaraan;
	}
	public String getSupir() {
		return supir;
	}
	public void setSupir(String supir) {
		this.supir = supir;
	}
	public String getDokumen1() {
		return dokumen1;
	}
	public void setDokumen1(String dokumen1) {
		this.dokumen1 = dokumen1;
	}
	public String getDokumen2() {
		return dokumen2;
	}
	public void setDokumen2(String dokumen2) {
		this.dokumen2 = dokumen2;
	}
	public String getDokumen3() {
		return dokumen3;
	}
	public void setDokumen3(String dokumen3) {
		this.dokumen3 = dokumen3;
	}
	public String getKodeapprove() {
		return kodeapprove;
	}
	public void setKodeapprove(String kodeapprove) {
		this.kodeapprove = kodeapprove;
	}
	public String getTglpermohonan() {
		return tglpermohonan;
	}
	public void setTglpermohonan(String tglpermohonan) {
		this.tglpermohonan = tglpermohonan;
	}
	public String getDlt() {
		return dlt;
	}
	public void setDlt(String dlt) {
		this.dlt = dlt;
	}
	public String getAlasanrequester() {
		return alasanrequester;
	}
	public void setAlasanrequester(String alasanrequester) {
		this.alasanrequester = alasanrequester;
	}
	public String getNamarequester() {
		return namarequester;
	}
	public void setNamarequester(String namarequester) {
		this.namarequester = namarequester;
	}
	public String getLamaexpired() {
		return lamaexpired;
	}
	public void setLamaexpired(String lamaexpired) {
		this.lamaexpired = lamaexpired;
	}
	public String getFingerprintid() {
		return fingerprintid;
	}
	public void setFingerprintid(String fingerprintid) {
		this.fingerprintid = fingerprintid;
	}
	public String getStatusall() {
		return statusall;
	}
	public void setStatusall(String statusall) {
		this.statusall = statusall;
	}
	public String getKodemesin() {
		return kodemesin;
	}
	public void setKodemesin(String kodemesin) {
		this.kodemesin = kodemesin;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getKodecabang() {
		return kodecabang;
	}
	public void setKodecabang(String kodecabang) {
		this.kodecabang = kodecabang;
	}
	public String getJenispotong() {
		return jenispotong;
	}
	public void setJenispotong(String jenispotong) {
		this.jenispotong = jenispotong;
	}
	public String getBonuscuti() {
		return bonuscuti;
	}
	public void setBonuscuti(String bonuscuti) {
		this.bonuscuti = bonuscuti;
	}
	public String getLokasi() {
		return lokasi;
	}
	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}
	
	
}
