package com.rsmurniteguh.webservice.dep.all.model.hris;

public class RequestCreateIzin {
	private long kodeizin;
	private String idkaryawan;
	private String idkatasan;
	private String tglpermohonan;
	private String tglend;
	private String tipe;
	private String rbizin;
	private String jamEnd;
	private String jamStart;
	private String kendaraan;
	private String supir;
	private String alasan;
	private String kodemesin;
	private int jlh;
	
	public long getKodeizin() {
		return kodeizin;
	}
	public void setKodeizin(long kodeizin) {
		this.kodeizin = kodeizin;
	}
	
	public String getKodemesin() {
		return kodemesin;
	}
	public void setKodemesin(String kodemesin) {
		this.kodemesin = kodemesin;
	}
	public String getRbizin() {
		return rbizin;
	}
	public void setRbizin(String rbizin) {
		this.rbizin = rbizin;
	}
	public String getKendaraan() {
		return kendaraan;
	}
	public void setKendaraan(String kendaraan) {
		this.kendaraan = kendaraan;
	}
	public String getSupir() {
		return supir;
	}
	public void setSupir(String supir) {
		this.supir = supir;
	}
	public String getAlasan() {
		return alasan;
	}
	public void setAlasan(String alasan) {
		this.alasan = alasan;
	}
	
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getIdkatasan() {
		return idkatasan;
	}
	public void setIdkatasan(String idkatasan) {
		this.idkatasan = idkatasan;
	}
	public String getTglpermohonan() {
		return tglpermohonan;
	}
	public void setTglpermohonan(String tglpermohonan) {
		this.tglpermohonan = tglpermohonan;
	}
	public String getTglend() {
		return tglend;
	}
	public void setTglend(String tglend) {
		this.tglend = tglend;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public int getJlh() {
		return jlh;
	}
	public void setJlh(int jlh) {
		this.jlh = jlh;
	}
	public String getJamEnd() {
		return jamEnd;
	}
	public void setJamEnd(String jamEnd) {
		this.jamEnd = jamEnd;
	}
	public String getJamStart() {
		return jamStart;
	}
	public void setJamStart(String jamStart) {
		this.jamStart = jamStart;
	}
	
}
