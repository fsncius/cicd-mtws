package com.rsmurniteguh.webservice.dep.all.model.hris;

public class SimpleKaryawan {
	private String idkaryawan;
	private String nama;
	private String kodehuruf;
	private String department;
	private String jabatan;
	private String kodejabatan;
	

	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getKodehuruf() {
		return kodehuruf;
	}
	public void setKodehuruf(String kodehuruf) {
		this.kodehuruf = kodehuruf;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getJabatan() {
		return jabatan;
	}
	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}
	public String getKodejabatan() {
		return kodejabatan;
	}
	public void setKodejabatan(String kodejabatan) {
		this.kodejabatan = kodejabatan;
	}
	
}
