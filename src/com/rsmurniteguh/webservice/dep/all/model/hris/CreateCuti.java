package com.rsmurniteguh.webservice.dep.all.model.hris;

import java.util.List;

public class CreateCuti {
	private String kodecuti;
	private String idkaryawan;
	private String tglawalcuti;
	private String tglakhircuti;
	private int jlhreqcuti;
	private String tipecuti;
	private int jlhatasan;
	private String infotambahan;
	private String lamacuti;
	private String prov;
//	private List<?> task;
	private String bonuscuti;
	private int jlh_serahterimatugas;
	private List<SerahTugas> list_serahtugas;
	private List<CutiAtasan> list_cutiatasan;

	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getTglawalcuti() {
		return tglawalcuti;
	}
	public void setTglawalcuti(String tglawalcuti) {
		this.tglawalcuti = tglawalcuti;
	}
	public String getTglakhircuti() {
		return tglakhircuti;
	}
	public void setTglakhircuti(String tglakhircuti) {
		this.tglakhircuti = tglakhircuti;
	}
	public int getJlhreqcuti() {
		return jlhreqcuti;
	}
	public void setJlhreqcuti(int jlhreqcuti) {
		this.jlhreqcuti = jlhreqcuti;
	}
	public String getTipecuti() {
		return tipecuti;
	}
	public void setTipecuti(String tipecuti) {
		this.tipecuti = tipecuti;
	}
	public String getInfotambahan() {
		return infotambahan;
	}
	public void setInfotambahan(String infotambahan) {
		this.infotambahan = infotambahan;
	}
	public String getLamacuti() {
		return lamacuti;
	}
	public void setLamacuti(String lamacuti) {
		this.lamacuti = lamacuti;
	}
	public String getProv() {
		return prov;
	}
	public void setProv(String prov) {
		this.prov = prov;
	}
//	public List getTask() {
//		return task;
//	}
//	public void setTask(List task) {
//		this.task = task;
//	}
	public String getBonuscuti() {
		return bonuscuti;
	}
	public void setBonuscuti(String bonuscuti) {
		this.bonuscuti = bonuscuti;
	}
	public int getJlh_serahterimatugas() {
		return jlh_serahterimatugas;
	}
	public void setJlh_serahterimatugas(int jlh_serahterimatugas) {
		this.jlh_serahterimatugas = jlh_serahterimatugas;
	}
	public int getJlhatasan() {
		return jlhatasan;
	}
	public void setJlhatasan(int jlhatasan) {
		this.jlhatasan = jlhatasan;
	}
	public List<SerahTugas> getList_serahtugas() {
		return list_serahtugas;
	}
	public void setList_serahtugas(List<SerahTugas> list_serahtugas) {
		this.list_serahtugas = list_serahtugas;
	}
	public List<CutiAtasan> getList_cutiatasan() {
		return list_cutiatasan;
	}
	public void setList_cutiatasan(List<CutiAtasan> list_cutiatasan) {
		this.list_cutiatasan = list_cutiatasan;
	}
	public String getKodecuti() {
		return kodecuti;
	}
	public void setKodecuti(String kodecuti) {
		this.kodecuti = kodecuti;
	}
}
