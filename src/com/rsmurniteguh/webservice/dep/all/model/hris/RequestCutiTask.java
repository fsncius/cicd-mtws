package com.rsmurniteguh.webservice.dep.all.model.hris;

public class RequestCutiTask {
	private String kodeserahtugas;
	private String kodecuti;
	private String idkaryawan;
	private String tanggal;
	private String tugas;
	private String lapmasalahpenting;
	private String status;
	private String tglinput;
	private String tglubah;
	private String dlt;
	private String idrequester;
	private String namarequester;
	private String jeniscuti;
	private String lamaexpired;
	private String fingerprintid;
	private String notifikasi;
	private String hasil;
	private String foto;
	
	
	public String getKodeserahtugas() {
		return kodeserahtugas;
	}
	public void setKodeserahtugas(String kodeserahtugas) {
		this.kodeserahtugas = kodeserahtugas;
	}
	public String getKodecuti() {
		return kodecuti;
	}
	public void setKodecuti(String kodecuti) {
		this.kodecuti = kodecuti;
	}
	public String getIdkaryawan() {
		return idkaryawan;
	}
	public void setIdkaryawan(String idkaryawan) {
		this.idkaryawan = idkaryawan;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getTugas() {
		return tugas;
	}
	public void setTugas(String tugas) {
		this.tugas = tugas;
	}
	public String getLapmasalahpenting() {
		return lapmasalahpenting;
	}
	public void setLapmasalahpenting(String lapmasalahpenting) {
		this.lapmasalahpenting = lapmasalahpenting;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTglinput() {
		return tglinput;
	}
	public void setTglinput(String tglinput) {
		this.tglinput = tglinput;
	}
	public String getTglubah() {
		return tglubah;
	}
	public void setTglubah(String tglubah) {
		this.tglubah = tglubah;
	}
	public String getDlt() {
		return dlt;
	}
	public void setDlt(String dlt) {
		this.dlt = dlt;
	}
	public String getIdrequester() {
		return idrequester;
	}
	public void setIdrequester(String idrequester) {
		this.idrequester = idrequester;
	}
	public String getNamarequester() {
		return namarequester;
	}
	public void setNamarequester(String namarequester) {
		this.namarequester = namarequester;
	}
	public String getJeniscuti() {
		return jeniscuti;
	}
	public void setJeniscuti(String jeniscuti) {
		this.jeniscuti = jeniscuti;
	}
	public String getLamaexpired() {
		return lamaexpired;
	}
	public void setLamaexpired(String lamaexpired) {
		this.lamaexpired = lamaexpired;
	}
	public String getFingerprintid() {
		return fingerprintid;
	}
	public void setFingerprintid(String fingerprintid) {
		this.fingerprintid = fingerprintid;
	}
	public String getNotifikasi() {
		return notifikasi;
	}
	public void setNotifikasi(String notifikasi) {
		this.notifikasi = notifikasi;
	}
	public String getHasil() {
		return hasil;
	}
	public void setHasil(String hasil) {
		this.hasil = hasil;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
}
