package com.rsmurniteguh.webservice.dep.tasks;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.rsmurniteguh.webservice.dep.base.ISysConstant;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;

public abstract class EmailScheduledPurchaseTask implements Job {
	private Properties properties;
	private String from;
	private String host;
	private String password;
	protected String receipentTo = "victor@rsmurniteguh.com, fransiska@rsmurniteguh.com";
	protected String receipentCC = "";
	protected String receipentBCC = "victor@rsmurniteguh.com, yuyanto.yang@rsmurniteguh.com, fransiska@rsmurniteguh.com, jimmylay@rsmurniteguh.com";

	protected String reportName;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		CommonService cs = new CommonService();
		String tipe = cs.getProgramEnvironment();
		
		from = "reporting@rsmurniteguh.com";
		password = "12345678";
		host = tipe.equals(ISysConstant.ENV_PRODUCTION) ? "11.0.200.2" : "rsmurniteguh.com"; // "11.0.200.2";
		
		properties = System.getProperties();
	    properties.setProperty("mail.smtp.host", host);
	    properties.put("mail.smtp.auth", "true");
	    
        reportName = "Report";
	    this.run();
	}
	
	protected void sendMessage(String body) {
		Transport transport = null;
		try {
			Session session = Session.getDefaultInstance(properties);
		    
	    	session.setDebug(true);
	    	
	    	transport = session.getTransport();
	    	transport.connect(from, password);
	    	
	    	BodyPart messageBodyPart = new MimeBodyPart();
	    	if (body == null) body = "";
	    	messageBodyPart.setContent(body, "text/html");
	    	
	    	Multipart multipart = new MimeMultipart();
	    	multipart.addBodyPart(messageBodyPart);
	    	
//	    	messageBodyPart = new MimeBodyPart();
//	    	messageBodyPart.setDataHandler(new DataHandler(new FileDataSource(dir.concat("/" + reportName + ".pdf"))));
//	    	messageBodyPart.setFileName(reportName + ".pdf");
//	    	multipart.addBodyPart(messageBodyPart);
	    	
	    	Message message = new MimeMessage(session);
	        message.setFrom(new InternetAddress(from, "Automatic Reporting"));
	        if (receipentTo != "")
	        	message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receipentTo));
	        if (receipentCC != "")
	        	message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(receipentCC));
	        if (receipentBCC != "")
	        	message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(receipentBCC));	
	        message.setSubject(reportName);
	        message.setContent(multipart);
	
	        transport.sendMessage(message, message.getAllRecipients());
	        System.out.println("Send Auto Email Purchasing Notification");
		}
		catch(MessagingException e) {
			throw new RuntimeException(e);
		}
		catch (Exception e) {
	    	e.printStackTrace();
	    }
		finally {
			if (transport != null) try { transport.close(); } catch(Exception e) {}
		}
	}
	
	public abstract void run();
}
