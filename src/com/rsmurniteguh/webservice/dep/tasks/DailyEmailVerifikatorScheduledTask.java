package com.rsmurniteguh.webservice.dep.tasks;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.pdfbox.util.PDFMergerUtility;

import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class DailyEmailVerifikatorScheduledTask extends EmailScheduledTask {
	
	public void run() {
		CommonService cs = new CommonService();
		if(!cs.getParameterValue(IParameterConstant.DAILY_EMAIL_EVENING).equals(IConstant.IND_YES)) return;
		this.reportName = "Daily Report";
		
		Connection verificatorConnection = null;
		try {
	    	verificatorConnection = DbConnection.getPooledConnection();
	    	
	        Calendar cal = Calendar.getInstance();
	        HashMap map = new HashMap<>();

	        if (verificatorConnection != null) {
		        Date currTime = cal.getTime();
		        map.put("CurrTime", currTime);
		    	JasperReport jasperReport = JasperCompileManager.compileReport(dir.concat("/VerifikatorReport.jrxml"));
		    	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, verificatorConnection);
		    	JasperExportManager.exportReportToPdfFile(jasperPrint, dir.concat("/Report.pdf"));
	        }
	    	
	        if (verificatorConnection != null) {
		    	PDFMergerUtility ut = new PDFMergerUtility();
		    	if (verificatorConnection != null) {
		    		ut.addSource(dir.concat("/Report.pdf"));
		    	}
		    	ut.setDestinationFileName(dir.concat("/" + this.reportName + ".pdf"));
		    	ut.mergeDocuments();
	        
		    	String body = "<p>Dear All, </p>";
		    	body += "<p>Berikut terlampir : ";
		    	body += "<ol>";
			    if (verificatorConnection != null) {
			    	body += "<li>Laporan Rencana Pasien Pulang / Verifikator</li>";
			    }
		    	body += "</ol></p></br>";
		    	body += "<p>Regards, </p><br><p>IT Dept.</p>";
		    	
		    	this.sendMessage(body);
	        }
	    } catch (Exception e) {
			e.printStackTrace();
		} finally {
	    	if (verificatorConnection != null) try { verificatorConnection.close(); } catch (SQLException e) {}
	    }
	}

}
