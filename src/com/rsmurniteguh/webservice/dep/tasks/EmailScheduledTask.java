package com.rsmurniteguh.webservice.dep.tasks;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.base.ISysConstant;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;

public abstract class EmailScheduledTask implements Job {
	private Properties properties;
	private String from;
	private String host;
	private String password;

	protected String dir;
	protected String reportName;
	protected List<String> listReport;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		CommonService cs = new CommonService();
		String tipe = cs.getProgramEnvironment();

		from = "reporting@rsmurniteguh.com";
		password = "12345678";
		host = cs.getParameterValue(IParameterConstant.HOST_EMAIL); // tipe.equals(ISysConstant.ENV_PRODUCTION) ? "11.0.200.2" : "rsmurniteguh.com"; // "11.0.200.2"; 
		
		properties = System.getProperties();
	    properties.setProperty("mail.smtp.host", host);
	    properties.put("mail.smtp.auth", "true");
	    
	    ClassLoader classLoader = new DailyEmailScheduledTask().getClass().getClassLoader();
        File file = new File(classLoader.getResource("report/DailyReport.jrxml").getFile());
        dir = file.getParent();
	    
        reportName = "Report";
	    this.run();
	}
	
	protected void sendMessage(String body) {
		Transport transport = null;
		try {
			CommonService cs = new CommonService();
			String emailTo = cs.getParameterValue(IParameterConstant.REPORT_EMAIL_TO);
			String emailBCC = cs.getParameterValue(IParameterConstant.REPORT_EMAIL_BCC);
			
			Session session = Session.getDefaultInstance(properties);
		    
	    	session.setDebug(true);
	    	
	    	transport = session.getTransport();
	    	transport.connect(from, password);
	    	
	    	BodyPart messageBodyPart = new MimeBodyPart();
	    	if (body == null) body = "";
	    	messageBodyPart.setContent(body, "text/html");
	    	
	    	Multipart multipart = new MimeMultipart();
	    	multipart.addBodyPart(messageBodyPart);
	    		    	
	    	if (listReport != null && listReport.size() > 0){
	    		for (int a=0;a<listReport.size();a++){	    			
	    			BodyPart mb2 = new MimeBodyPart();
	    			mb2.setDataHandler(new DataHandler(new FileDataSource(dir.concat("/listReport" + (a+1) + ".pdf"))));
	    			mb2.setFileName(listReport.get(a) + ".pdf");
			    	multipart.addBodyPart(mb2);
	    		}
	    	}else {
	    		messageBodyPart = new MimeBodyPart();
	    		messageBodyPart.setDataHandler(new DataHandler(new FileDataSource(dir.concat("/" + reportName + ".pdf"))));
		    	messageBodyPart.setFileName(reportName + ".pdf");
		    	multipart.addBodyPart(messageBodyPart);
	    	}
	    	
	    	Message message = new MimeMessage(session);
	        message.setFrom(new InternetAddress(from, "Automatic Reporting"));
	        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo));
//	        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("victor@rsmurniteguh.com"));
//	        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("theankhor@rsmurniteguh.com, johnnychandra@rsmurniteguh.com, jongkhai@rsmurniteguh.com, dr.bangbang_buhari@rsmurniteguh.com, lilymutiara@rsmurniteguh.com, drlimenta@rsmurniteguh.com, dr.merry@rsmurniteguh.com, hennysutanto@rsmurniteguh.com"));
//	        message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse("victor@rsmurniteguh.com, yuyanto.yang@rsmurniteguh.com, fransiska@rsmurniteguh.com, jimmylay@rsmurniteguh.com"));
	        if (emailBCC != null && !emailBCC.equals("")) message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(emailBCC));
	        message.setSubject(reportName);
	        message.setContent(multipart);
	
	        transport.sendMessage(message, message.getAllRecipients());
		}
		catch(MessagingException e) {
			throw new RuntimeException(e);
		}
		catch (Exception e) {
	    	e.printStackTrace();
	    }
		finally {
			if (transport != null) try { transport.close(); } catch(Exception e) {}
		}
	}
	
	public abstract void run();
}
