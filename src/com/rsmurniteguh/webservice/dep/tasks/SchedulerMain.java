package com.rsmurniteguh.webservice.dep.tasks;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.rsmurniteguh.webservice.dep.tasks.mobile.MobileRegnNotif;

public abstract class SchedulerMain {
	
	public static void run() throws InterruptedException, SchedulerException {
//		Timer time = new Timer(); // Instantiate Timer Object
//		ScheduledTask st = new ScheduledTask(); // Instantiate SheduledTask class
//		time.schedule(st, 0, 1000); // Create Repetitively task for every 1 secs

//		Timer mobileDoctorRegnNotif = new Timer(); // Instantiate Timer Object
//		MobileRegnNotif mobileRegnNotif = new MobileRegnNotif(); // Instantiate SheduledTask class
//		mobileDoctorRegnNotif.schedule(mobileRegnNotif, 0, 1000); // Create Repetitively task for every 1 secs
		
//		JobDetail dailyJob = (JobDetail) JobBuilder.newJob(DailyEmailScheduledTask.class)
//				.withIdentity("dailyJob", "emailGroup").build();
//		Trigger dailyTrigger = TriggerBuilder.newTrigger()
//				.withIdentity("dailyTrigger", "emailGroup")
//				.withSchedule(CronScheduleBuilder.cronSchedule("0 0 11 * * ?")).build();
////		.withSchedule(CronScheduleBuilder.cronSchedule("0 0/1 * * * ?")).build();
//		
//		JobDetail dailyJob2 = (JobDetail) JobBuilder.newJob(DailyEmailVerifikatorScheduledTask.class)
//				.withIdentity("dailyJob2", "emailGroup").build();
//		Trigger dailyTrigger2 = TriggerBuilder.newTrigger()
//				.withIdentity("dailyTrigger2", "emailGroup")
//				.withSchedule(CronScheduleBuilder.cronSchedule("0 0 17 * * ?")).build();
//		
//		JobDetail dailyJob3 = (JobDetail) JobBuilder.newJob(DailyScheduleKHTISPostJournal.class)
//				.withIdentity("dailyJob3", "postGroup").build();
//		Trigger dailyTrigger3 = TriggerBuilder.newTrigger()
//				.withIdentity("dailyTrigger3", "postGroup")
//				.withSchedule(CronScheduleBuilder.cronSchedule("0 0 1 * * ?")).build();
//				
//		JobDetail weeklyJob = (JobDetail) JobBuilder.newJob(WeeklyEmailScheduledTask.class)
//				.withIdentity("weeklyJob", "emailGroup").build();
//		Trigger weeklyTrigger = TriggerBuilder.newTrigger()
//				.withIdentity("weeklyTrigger", "emailGroup")
//				.withSchedule(CronScheduleBuilder.cronSchedule("0 0 11 ? * MON")).build();
////				.withSchedule(CronScheduleBuilder.cronSchedule("0 0/2 * * * ?")).build();		
//		
//		JobDetail monthlyJob = (JobDetail) JobBuilder.newJob(MonthlyEmailScheduledTask.class)
//				.withIdentity("monthlyJob", "emailGroup").build();
//		Trigger monthlyTrigger = TriggerBuilder.newTrigger()
//				.withIdentity("monthlyTrigger", "emailGroup")
//				.withSchedule(CronScheduleBuilder.cronSchedule("0 0 11 1 * ?")).build();
////				.withSchedule(CronScheduleBuilder.cronSchedule("0 0/3 * * * ?")).build();
//		
//		JobDetail timelyPurchasingJob = (JobDetail) JobBuilder.newJob(TimelyEmailPurchaseScheduledTask.class)
//				.withIdentity("timelyPurchaseJob", "emailGroup").build();
//		Trigger timelyPrTrigger = TriggerBuilder.newTrigger()
//				.withIdentity("timelyPrTrigger","emailGroup")
//				.withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 * * * ?")).build();
		
		Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		scheduler.start();
//		scheduler.scheduleJob(dailyJob, dailyTrigger);
//		scheduler.scheduleJob(dailyJob2, dailyTrigger2);
//		scheduler.scheduleJob(dailyJob3, dailyTrigger3);
//		scheduler.scheduleJob(weeklyJob, weeklyTrigger);
//		scheduler.scheduleJob(monthlyJob, monthlyTrigger);
//		scheduler.scheduleJob(timelyPurchasingJob, timelyPrTrigger);
	}
	
}
