package com.rsmurniteguh.webservice.dep.tasks;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.pdfbox.util.PDFMergerUtility;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class MonthlyEmailScheduledTask extends EmailScheduledTask {

	public void run() {
		CommonService cs = new CommonService();
		if(!cs.getParameterValue(IParameterConstant.MONTHLY_EMAIL).equals(IConstant.IND_YES)) return;
		
		this.reportName = "Monthly Report";
		
		Connection pooledConnection = null;
		try {
	    	pooledConnection = DbConnection.getPooledConnection();
	    	
	        Calendar cal = Calendar.getInstance();
	        HashMap map = new HashMap<>();

	        if (pooledConnection != null) {
	        	cal.add(Calendar.MONTH, -1);
	        	cal.set(Calendar.DAY_OF_MONTH, 1);
		        Date startTime = cal.getTime();
		        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		        Date endTime = cal.getTime();
		        
	        	map.put("START_TIME", startTime);
	        	map.put("END_TIME", endTime);
	        	map.put("SUBREPORT_DIRECTORY", dir);
		    	JasperReport jasperReport = JasperCompileManager.compileReport(dir.concat("/MonthlyReport.jrxml"));
		    	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, pooledConnection);
		    	JasperExportManager.exportReportToPdfFile(jasperPrint, dir.concat("/" + this.reportName + ".pdf"));
	        
		    	String body = "<p>Dear All, </p>";
		    	body += "<p>Berikut terlampir : ";
		    	body += "<ol>";
		    	if (pooledConnection != null) {
			    	body += "<li>Laporan Utilisasi Ruang Operasi Murni Teguh Memorial Hospital (Bulanan)</li>";
		    	}
		    	body += "</ol></p></br>";
		    	body += "<p>Regards, </p><br><p>IT Dept.</p>";
		    	
		    	this.sendMessage(body);
	        }
	    } catch (Exception e) {
			e.printStackTrace();
		} finally {
	    	if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
	    }
	}

}
