package com.rsmurniteguh.webservice.dep.tasks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.pdfbox.util.PDFMergerUtility;

import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.kthis.model.Commonsearch;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class DailyEmailScheduledTask extends EmailScheduledTask {
	
	public void run() {
		CommonService cs = new CommonService();
		if(!cs.getParameterValue(IParameterConstant.DAILY_EMAIL_MORNING).equals(IConstant.IND_YES)) return;
		
		this.reportName = "Daily Report";
		this.listReport = new ArrayList<String>();
		
		Connection pooledConnection = null;
		Connection verificatorConnection = null;
		try {
	    	pooledConnection = DbConnection.getPooledConnection();
	    	verificatorConnection = DbConnection.getMysqlOnlineQueueInstance2();
	    	
	        Calendar cal = Calendar.getInstance();
	        HashMap map = new HashMap<>();

	        if (verificatorConnection != null) {
	        	String file1 = "Laporan Rencana Pasien Pulang";
		        Date currTime = cal.getTime();
		        map.put("CurrTime", currTime);
		    	JasperReport jasperReport = JasperCompileManager.compileReport(dir.concat("/VerifikatorReport.jrxml"));
		    	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, verificatorConnection);
		    	JasperExportManager.exportReportToPdfFile(jasperPrint, dir.concat("/listReport1.pdf"));
		    	listReport.add(file1);
	        }
	    	
	        if (pooledConnection != null) {
	        	Date endTime1 = cal.getTime();
	        	String file1 = "Laporan Utilisasi Ruang Operasi";
	        	file1 += new SimpleDateFormat("(dd-MM-yyyy)").format(cal.getTime());
	        	
	        	map.put("OT_DATE", endTime1);
	        	map.put("SUBREPORT_DIRECTORY", dir);
		    	JasperReport jasperReport = JasperCompileManager.compileReport(dir.concat("/DailyReport.jrxml"));
		    	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, pooledConnection);
		    	JasperExportManager.exportReportToPdfFile(jasperPrint, dir.concat("/listReport2.pdf"));
	        	
		    	listReport.add(file1);
		    	
		    	cal.add(Calendar.DATE, -1);
		        Date endTime = cal.getTime();
		        
		        String file2 = "Laporan Utilisasi Ruang Operasi";
		        file2 += new SimpleDateFormat("(dd-MM-yyyy)").format(cal.getTime());
		        
	        	map.put("OT_DATE", endTime);
	        	map.put("SUBREPORT_DIRECTORY", dir);
		    	jasperReport = JasperCompileManager.compileReport(dir.concat("/DailyReport.jrxml"));
		    	jasperPrint = JasperFillManager.fillReport(jasperReport, map, pooledConnection);
		    	JasperExportManager.exportReportToPdfFile(jasperPrint, dir.concat("/listReport3.pdf"));
		    	
		    	listReport.add(file2);
		        
		        cal.add(Calendar.DATE, -6);
		        Date startTime = cal.getTime();
		        
		        String file3 = "Laporan Total Waktu Utilisasi Ruang Operasi";
		        
		    	map.put("StartTime", startTime);
		    	map.put("EndTime", endTime);
		    	map.put("Efektif", 8);
		    	jasperReport = JasperCompileManager.compileReport(dir.concat("/OT_Time_Report.jrxml"));
		    	jasperPrint = JasperFillManager.fillReport(jasperReport, map, pooledConnection);
		    	JasperExportManager.exportReportToPdfFile(jasperPrint, dir.concat("/listReport4.pdf"));
		    	
		    	listReport.add(file3);
	        }
	    	
	        if (verificatorConnection != null || pooledConnection != null) {
//		    	PDFMergerUtility ut = new PDFMergerUtility();
//		    	if (pooledConnection != null) {
//			    	ut.addSource(dir.concat("/Report1.pdf"));
//			    	ut.addSource(dir.concat("/Report3.pdf"));
//			    	ut.addSource(dir.concat("/Report2.pdf"));
//		    	}
//		    	if (verificatorConnection != null) {
//		    		ut.addSource(dir.concat("/Report.pdf"));
//		    	}
//		    	ut.setDestinationFileName(dir.concat("/" + this.reportName + ".pdf"));
//		    	ut.mergeDocuments();
	        
		    	String body = "<p>Dear All, </p>";
		    	body += "<p>Berikut terlampir : ";
		    	body += "<ol>";
		    	if (pooledConnection != null) {
			    	body += "<li>Laporan Total Waktu Utilisasi Ruang Operasi Murni Teguh Memorial Hospital</li>";
			    	body += "<li>Laporan Utilisasi Ruang Operasi Murni Teguh Memorial Hospital (Harian)</li>";
		    	}
			    if (verificatorConnection != null) {
			    	body += "<li>Laporan Rencana Pasien Pulang / Verifikator</li>";
			    }
		    	body += "</ol></p></br>";
		    	body += "<p>Regards, </p><br><p>IT Dept.</p>";
		    	
		    	this.sendMessage(body);
	        }
	    } catch (Exception e) {
			e.printStackTrace();
		} finally {
	    	if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
	    	if (verificatorConnection != null) try { verificatorConnection.close(); } catch (SQLException e) {}
	    }
	}

}
