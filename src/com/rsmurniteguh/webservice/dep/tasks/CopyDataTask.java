package com.rsmurniteguh.webservice.dep.tasks;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.rsmurniteguh.webservice.dep.kthis.model.ProtokolHD;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class CopyDataTask {
	public static void copyData(){
		Connection connectionHD = DbConnection.getMysqlOnlineQueueInstance2();
		Connection connectionKTHIS = DbConnection.getPooledConnection();
		if(connectionHD == null || connectionKTHIS == null)return;
		ResultSet rsHD=null;
		ResultSet rsHDCheck=null;
		ResultSet rsKthisCheck=null;
		PreparedStatement psInsertKTHIS=null;
		PreparedStatement psHD=null;
		PreparedStatement psCheckHD=null;
		PreparedStatement psCheckKTHIS=null;
		List<ProtokolHD> listAll = new ArrayList<ProtokolHD>();

		String datadiri = "SELECT dd.`nokartu`, dd.`haritgjam`, dd.`diagnosamedis`, dd.`riwayatkesehatan`, dd.`nomesin`, dd.`hemodialiske`, dd.`tipedialis`, dd.`carabayar`,dd.`tanggal` "
				+ "FROM tb_datadiri dd WHERE LENGTH(DD.`haritgjam`) = 17 AND dd.`haritgjam` IS NOT NULL AND dd.`haritgjam` != '' AND DD.`tanggal` != '0000-00-00'";
		try {
			psHD = connectionHD.prepareStatement(datadiri);
			rsHD = psHD.executeQuery();
			while(rsHD.next())
			{
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
			    Date parsedDate = dateFormat.parse(rsHD.getString("haritgjam"));
			   
			    Timestamp tanggalHd = new java.sql.Timestamp(parsedDate.getTime());
			    
			    SimpleDateFormat dateFormatTanggal = new SimpleDateFormat("yyyy-MM-dd");
			    Date parsedDateTanggal = dateFormatTanggal.parse(rsHD.getString("tanggal"));
			    Timestamp tanggal = new java.sql.Timestamp(parsedDateTanggal.getTime());
			    
				ProtokolHD ph = new ProtokolHD();
				ph.setCardNo(rsHD.getBigDecimal("nokartu"));
				ph.setTanggalHd(tanggalHd);
				ph.setDiagnosaMedis(rsHD.getString("diagnosamedis"));
				ph.setRiwayatKesehatan(rsHD.getString("riwayatkesehatan"));
				ph.setNomorMesin(rsHD.getString("nomesin"));
				ph.setHemodialisisKe(rsHD.getString("hemodialiske"));
				ph.setTipeDialiser(rsHD.getString("tipedialis"));
				ph.setCaraBayar(rsHD.getString("carabayar"));
				ph.setTanggal(tanggal);
				ph.setTanggalString(rsHD.getString("tanggal"));
				listAll.add(ph);
			}
			if (psHD != null) try { psHD.close(); } catch (SQLException e) {}
			if (rsHD != null) try { rsHD.close(); } catch (SQLException e) {}
			
			for (int a=0;a<listAll.size();a++){
				ProtokolHD phd = listAll.get(a);
				BigDecimal noKartu = phd.getCardNo();
				Timestamp tglT = phd.getTanggal();


				Date date = new Date();
				date.setTime(tglT.getTime());
				String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
				
				String checkKthis = "SELECT * FROM PROTOKOLHD PH WHERE TRUNC(PH.CREATED_DATETIME) = '"+formattedDate+"' AND PH.CARD_NO = '"+noKartu+"'";
				if (psCheckKTHIS != null) try { psCheckKTHIS.close(); } catch (SQLException e) {}
				if (rsKthisCheck != null) try { rsKthisCheck.close(); } catch (SQLException e) {}
				psCheckKTHIS = connectionKTHIS.prepareStatement(checkKthis);
				rsKthisCheck = psCheckKTHIS.executeQuery();
				if(rsKthisCheck.next())
				{
					continue;
				}
				else
				{
					
					String status = "SELECT CONCAT('{\"merokok\":',IF(st.`rokok` = 'Merokok', 'true,', 'false,'),'\"konsumsiAlkohol\":',IF(st.`alkohol` = 'Konsumsi Alkohol', 'true,', 'false,'),'\"seksBebas\":',IF(st.`seks` = 'Seks Bebas', 'true,', 'false,'),"
							+ "'\"penggunaObatTerlarang\":',IF(st.`obat` = 'Pengguna Obat Terlarang', 'true,', 'false,'),'\"dll\":\"',st.`lainpsiko`,'\"}') AS STATUS_PSIKOSOSIAL,"
							+ "CONCAT('{\"kecemasanBerlebih\":',IF(st.`cemas` = 'Kecemasan berlebih', 'true,', 'false,'),'\"prilakuTidakNormal\":',IF(st.`perilaku` = 'Perilaku Tidak Normal', 'true,', 'false,'),'\"sifatKasar\":',IF(st.`kasar` = 'Sifat Kasar / Berbahaya', 'true,', 'false,'),"
							+ "'\"tidakBisaMengurusDiri\":',IF(st.`tdkbsurusdiri` = 'Tidak Bisa Mengurus Diri', 'true,', 'false,'),'\"dll\":\"',st.`lainstatusfungsi`,'\"}') AS STATUS_FUNGSIONAL,"
							+ "CONCAT('{\"tidakBerisiko\":',IF(st.`tidakrisiko` = 'Tidak Berisiko', 'true,', 'false,'),'\"risikoRendah\":',IF(st.`risikorendah` = 'Risiko Rendah', 'true,', 'false,'),'\"risikoSedang\":',IF(st.`risikosedang` = 'Risiko Sedang', 'true,', 'false,'),"
							+ "'\"risikoTinggi\":',IF(st.`risikotinggi` = 'Risiko Tinggi', 'true,', 'false,'),'\"catatan\":\"',st.`catatanrisiko`,'\"}') AS RISIKO_JATUH,"
							+ "CONCAT('{\"alergiYa\":false,\"alergiTidak\":true,\"dll\":\"\"}') AS ALERGI_OBAT"
							+ " FROM tb_status st WHERE st.nokartu = '"+noKartu+"' AND st.tanggal = '"+tglT+"'ORDER BY st.idstatus DESC LIMIT 1";
					
					psCheckHD = connectionHD.prepareStatement(status);
					rsHDCheck = psCheckHD.executeQuery();
					while(rsHDCheck.next())
					{
						phd.setStatusPsikososial(rsHDCheck.getString("STATUS_PSIKOSOSIAL"));
						phd.setStatusFungsional(rsHDCheck.getString("STATUS_FUNGSIONAL"));
						//phd.setRisikoJatuh(rsHDCheck.getString("RISIKO_JATUH"));
						phd.setAlergiObat(rsHDCheck.getString("ALERGI_OBAT"));
					}
					
					if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
					if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}

					String pre_hd = "SELECT ph.`sensorium`, ph.`td3`, ph.`nadi3`, ph.`nafas`, ph.`suhu3`, ph.`bbkering`, ph.`bbdatang`, ph.`bbsebelumnya`, ph.`kenaikanbb`, ph.`aksesvaskular`, ph.`sens`, ph.`td2`, ph.`nadi2`, ph.`nafas2`, ph.`suhu2`, ph.`bbselesai`, ph.`bedabb`, ph.`catatan`"
							+ "FROM tb_pre_hd ph WHERE ph.`nokartu` = '"+noKartu+"' AND ph.`tanggal` = '"+tglT+"' ORDER BY ph.idprehd DESC LIMIT 1";
					psCheckHD = connectionHD.prepareStatement(pre_hd);
					rsHDCheck = psCheckHD.executeQuery();
					while(rsHDCheck.next())
					{
						phd.setSensoriumPreHd(rsHDCheck.getString("sensorium"));
						phd.setTdPreHd(rsHDCheck.getString("td3"));
						phd.setNadiPreHd(rsHDCheck.getString("nadi3"));
						phd.setNafasPreHd(rsHDCheck.getString("nafas"));
						phd.setSuhuPreHd(rsHDCheck.getString("suhu3"));
						phd.setBbKeringPreHd(rsHDCheck.getString("bbkering"));
						phd.setBbDatangPreHd(rsHDCheck.getString("bbdatang"));
						phd.setBbSebelumnyaPreHd(rsHDCheck.getString("bbsebelumnya"));
						phd.setBbKenaikanPreHd(rsHDCheck.getString("kenaikanbb"));
						phd.setAksesVaskularPreHd(rsHDCheck.getString("aksesvaskular"));
						phd.setSensoriumPostHd(rsHDCheck.getString("sens"));
						phd.setTdPostHd(rsHDCheck.getString("td2"));
						phd.setNadiPostHd(rsHDCheck.getString("nadi2"));
						phd.setNafasPostHd(rsHDCheck.getString("nafas2"));
						phd.setSuhuPostHd(rsHDCheck.getString("suhu2"));
						phd.setBbSelesaiPostHd(rsHDCheck.getString("bbselesai"));
						phd.setBedaBbPostHd(rsHDCheck.getString("bedabb"));
						phd.setCatatanPostHd(rsHDCheck.getString("catatan"));
					}
					/*else
					{
						phd.setSensoriumPreHd("0");
						phd.setTdPreHd("0");
						phd.setNadiPreHd("0");
						phd.setNafasPreHd("0");
						phd.setSuhuPreHd("0");
						phd.setBbKeringPreHd("0");
						phd.setBbDatangPreHd("0");
						phd.setBbSebelumnyaPreHd("0");
						phd.setBbKenaikanPreHd("0");
						phd.setAksesVaskularPreHd("0");
						phd.setSensoriumPostHd("0");
						phd.setTdPostHd("0");
						phd.setNadiPostHd("0");
						phd.setNafasPostHd("0");
						phd.setSuhuPostHd("0");
						phd.setBbSelesaiPostHd("0");
						phd.setBedaBbPostHd("0");
						phd.setCatatanPostHd(null);
					}*/
					
					if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
					if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}

					String instruksi_medik = "SELECT im.`waktumulai`, im.`lamahd`, im.`waktuselesai`, im.`primingmasuk`, im.`primingkeluar`, im.`antikoagulasi`, im.`awal`, im.`maintenance`, CONCAT('{\"tanpaHeprain\":',IF(im.`tpheparin` = 'Tanpa Heparin', 'true', 'false'),'}') AS TANPA_HEPARIN,"
							+ "im.`ufg`, im.`resephd`, im.`durasi`, im.`qb`, im.`qd`, im.`td`, im.`ufg2`, im.`ufr`, im.`profiling`, im.`antikoagulasi2`, im.`namadokter`, im.`periksa_fisik`"
							+ "FROM tb_instruksi_medik im WHERE im.`nokartu` = '"+noKartu+"' AND im.`tanggal` = '"+tglT+"' ORDER BY im.idinstruksi DESC LIMIT 1";
					psCheckHD = connectionHD.prepareStatement(instruksi_medik);
					rsHDCheck = psCheckHD.executeQuery();
					while(rsHDCheck.next())
					{
						if(rsHDCheck.getString("waktumulai").equals("") || rsHDCheck.getString("waktuselesai").equals(""))
						{
							phd.setWaktuMulai(null);
							phd.setWaktuSelesai(null);
						}
						else
						{	
							SimpleDateFormat dateFormatTanggal = new SimpleDateFormat("yyyy-MM-dd hh:mm");
						    Date parsedDateTanggal = dateFormatTanggal.parse(phd.getTanggalString() + " "+rsHDCheck.getString("waktumulai"));
						    Timestamp waktumulai = new java.sql.Timestamp(parsedDateTanggal.getTime());
						    
						    Date parsedDateTanggal1 = dateFormatTanggal.parse(phd.getTanggalString() + " "+rsHDCheck.getString("waktuselesai"));
						    Timestamp waktuselesai = new java.sql.Timestamp(parsedDateTanggal1.getTime());
			    
							phd.setWaktuMulai(waktumulai);
							phd.setWaktuSelesai(waktuselesai);
						}				
						phd.setLamaHd(rsHDCheck.getBigDecimal("lamahd"));
						phd.setPrimingMasuk(rsHDCheck.getString("primingmasuk"));
						phd.setPrimingKeluar(rsHDCheck.getString("primingkeluar"));
						phd.setAntiKoagulasi(rsHDCheck.getString("antikoagulasi"));
						phd.setAwalHd(rsHDCheck.getString("awal"));
						phd.setMaintenance(rsHDCheck.getString("maintenance"));
						phd.setTanpaHeparin(rsHDCheck.getString("TANPA_HEPARIN"));
						phd.setUfgHd(rsHDCheck.getString("ufg"));
						phd.setResepHd(rsHDCheck.getString("resephd"));
						phd.setDurasiHd(rsHDCheck.getString("durasi"));
						phd.setQbInstruksiMedik(rsHDCheck.getString("qb"));
						phd.setQdInstruksiMedik(rsHDCheck.getString("qd"));
						phd.setTdInstruksiMedik(rsHDCheck.getString("td"));
						phd.setUfgInstruksiMedik(rsHDCheck.getString("ufg2"));
						phd.setUfrInstruksiMedik(rsHDCheck.getString("ufr"));
						phd.setProfillingInstruksiMedik(rsHDCheck.getString("profiling"));
						phd.setAntikoagulasiInstruksiMedik(rsHDCheck.getString("antikoagulasi2"));
						phd.setNamaDokter(rsHDCheck.getString("namadokter"));
						phd.setPemeriksaanFisik(rsHDCheck.getString("periksa_fisik"));
					}

					
					if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
					if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
					
					String indikator_sakit = "SELECT CONCAT('{\"anemis\":',IF(idn.`anemis` = 'Anemis', 'true,', 'false,'),'\"ikterus\":',IF(idn.`ikterus` = 'Ikterus', 'true,', 'false,'),'\"sianosis\":',IF(idn.`sianosis` = 'Sianosis', 'true,', 'false,'),"
							+ "'\"sesak\":',IF(idn.`sesak` = 'Sesak', 'true,', 'false,'),'\"edema\":',IF(idn.`edema` = 'Edema', 'true,', 'false,'),'\"asites\":',IF(idn.`asites` = 'Asites', 'true', 'false'),'}') AS KEADAAN_UMUM,"
							+ "CONCAT('{\"tenang\":',IF(idn.`tenang` = 'Tenang', 'true,', 'false,'),'\"gelisah\":',IF(idn.`gelisah` = 'Gelisah', 'true,', 'false,'),'\"takut\":',IF(idn.`takut` = 'Takut', 'true,', 'false,'),"
							+ "'\"marah\":',IF(idn.`marah` = 'Marah', 'true,', 'false,'),'\"mudahTersinggung\":',IF(idn.`singgung` = 'Mudah Tersinggung', 'true', 'false'),'}') AS KONDISI_SAATINI,"
							+ "CONCAT('{\"nyeri\":',IF(idn.`nyeri` = 'Nyeri', 'true,', 'false,'),'\"akut\":',IF(idn.`akut` = 'Akut', 'true,', 'false,'),'\"kronik\":',IF(idn.`kronik` = 'Kronik', 'true', 'false'),'}') AS KONDISI_SAKIT,"
							+ "idn.`nilaiindikator` "
							+ "FROM tb_indikator_nyeri idn WHERE idn.`nokartu` = '"+noKartu+"' AND idn.`tanggal` = '"+tglT+"' ORDER BY idn.idindikatornyeri DESC LIMIT 1";
					psCheckHD = connectionHD.prepareStatement(indikator_sakit);
					rsHDCheck = psCheckHD.executeQuery();
					while(rsHDCheck.next())
					{
						phd.setKeadaanUmum(rsHDCheck.getString("KEADAAN_UMUM"));
						phd.setKondisiSaatIni(rsHDCheck.getString("KONDISI_SAATINI"));
						phd.setKondisiSakit(rsHDCheck.getString("KONDISI_SAKIT"));
						phd.setIndikatorSakit(rsHDCheck.getBigDecimal("nilaiindikator"));
					}
					if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
					if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}

					String diagnosa_intervensi = "SELECT CONCAT('{\"kelebihanVolumeCairan\":',IF(di.`lebihvolume` = 'Kelebihan Volume Cairan', 'true,', 'false,'),'\"gangguanPertukaranGas\":',IF(di.`gangguantukargas` = 'Gangguan Pertukaran Gas', 'true,', 'false,'),'\"gangguanElektrolit\":',IF(di.`gangguanelektrolit` = 'Takut', 'true,', 'false,'),"
							+ "'\"penurunanCurahJantung\":',IF(di.`penurunancurah` = 'Penurunan Curah Jantung', 'true,', 'false,'),'\"kurangNutrisi\":',IF(di.`kurangnutrisi` = 'Kurang Nutrisi', 'true,', 'false,'),'\"tidakPatuhTerhadapDiet\":',IF(di.`tidakpatuhdiet` = 'Tidak Patuh Terhadap Diet', 'true,', 'false,'),"
							+ "'\"gangguanKeseimbanganAsamBasa\":',IF(di.`gangguanasambasa` = 'Gangguan Keseimbangan Asam Basa', 'true,', 'false,'),'\"gangguanRasaNyaman\":',IF(di.`gangguanrasanyaman` = 'Gangguan Rasa Nyaman / Nyeri', 'true,', 'false,'),'\"dll\":\"',di.`laindiagnosarawat`,'\"}') AS DIAGNOSA_KEPERAWATAN,"
							+ "CONCAT('{\"monitorBB\":',IF(di.`monitorbbasup` = 'Monitor BB, Asupan, Keluaran', 'true,', 'false,'),'\"aturPosisi\":',IF(di.`aturposisipasien` = 'Atur Posisi Pasien Agar Ventilasi Adekuat', 'true,', 'false,'),'\"memberikanOksigen\":',IF(di.`memberikanoksigen` = 'Memberikan Oksigen Sesuai Kebutuhan', 'true,', 'false,'),"
							+ "'\"observasiPasien\":',IF(di.`observasipasien` = 'Observasi Pasien dan Tindakan', 'true,', 'false,'),'\"menanganiHipotensi\":',IF(di.`tanganihipotensi` = 'Menangani Hipotensi Sesuai SOP', 'true,', 'false,'),'\"kajiKemampuan\":',IF(di.`kajikemampuan` = 'Kaji Kemampuan Mendapatkan Nutrisi Yang Sesuai', 'true,', 'false,'),"
							+ "'\"gejalaInfeksi\":',IF(di.`monitortandainfeksi` = 'Monitor Tanda dan Gejala Infeksi', 'true,', 'false,'),'\"gejalaHipoglikemi\":',IF(di.`monitortandahipo` = 'Monitor Tanda dan Gejala Hipoglikemi', 'true,', 'false,'),'\"edukasiPasien\":',IF(di.`edukasipasien` = 'Mengedukasi Pasien', 'true,', 'false,'),"
							+ "'\"dll\":\"',di.`lainintervensirawat`,'\"}') AS INTERVENSI_KEPERAWATAN,"
							+ "CONCAT('{\"programHd\":',IF(di.`programhd` = 'Program HD', 'true,', 'false,'),'\"transfusiDarah\":',IF(di.`transfusidarah` = 'Transfusi Darah', 'true,', 'false,'),'\"kolaborasiDiet\":',IF(di.`kolaborasidiet` = 'Kolaborasi Diet', 'true,', 'false,'),"
							+ "'\"programPreparatBesi\":',IF(di.`programprebesi` = 'Program Preparat Besi', 'true,', 'false,'),'\"pemberianEpo\":',IF(di.`beriepo` = 'Pemberian EPO', 'true,', 'false,'),'\"pemberianAntipiretik\":',IF(di.`beriantipire` = 'Pemberian Antipiretik', 'true,', 'false,'),"
							+ "'\"pemberianCaGluconas\":',IF(di.`bericagluco` = 'Pemberian Ca Gluconas', 'true,', 'false,'),'\"analgetik\":',IF(di.`analgetik` = 'Analgetik', 'true,', 'false,'),'\"pemberianAntibiotik\":',IF(di.`beriantibio` = 'Pemberian Antibiotik', 'true,', 'false,'),"
							+ "'\"obatEmergensi\":',IF(di.`obatemergensi` = 'Obat-Obat Emergensi', 'true,', 'false,'),'\"dll\":\"',di.`lainintervensikorab`,'\"}') AS INTERVENSI_KOLABORASI "
							+ "FROM tb_diagnosa_intervensi di WHERE di.`nokartu` = '"+noKartu+"' AND di.`tanggal` = '"+tglT+"' ORDER BY di.iddiagnosa DESC LIMIT 1";
					psCheckHD = connectionHD.prepareStatement(diagnosa_intervensi);
					rsHDCheck = psCheckHD.executeQuery();
					while(rsHDCheck.next())
					{
						phd.setDiagnosaKeperawatan(rsHDCheck.getString("DIAGNOSA_KEPERAWATAN"));
						phd.setIntervensiKeperawatan(rsHDCheck.getString("INTERVENSI_KEPERAWATAN"));
						phd.setIntervensiKolaborasi(rsHDCheck.getString("INTERVENSI_KOLABORASI"));
					}
					if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
					if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
					
					String catat_rawat = "SELECT cr.`catatrawat`, cr.`namarawat` FROM tb_catat_rawat cr WHERE cr.`nokartu` = '"+noKartu+"' AND cr.`tanggal` = '"+tglT+"' ORDER BY cr.idcatatrawat DESC LIMIT 1";
					psCheckHD = connectionHD.prepareStatement(catat_rawat);
					rsHDCheck = psCheckHD.executeQuery();
					while(rsHDCheck.next())
					{
						phd.setCatatanKeperawatan(rsHDCheck.getString("catatrawat"));
						phd.setNamaPerawat(rsHDCheck.getString("namarawat"));
					}
					if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
					if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
					
					String catatan_perkembangan = "SELECT cp.`jam2`, cp.`tepidokter`, cp.`tepiklinis`, cp.`tepittd`, cp.`asupangizi`, cp.`kalori`, cp.`protein`, cp.`pengkajian`, cp.`materi`, CONCAT('{\"leafletBrosur\":',IF(cp.`leaflet` = 'leaflet', 'true', 'false'),'}') AS LEAFLET_BROSUR,cp.`rencanakedepan` "
							+ "FROM tb_catatan_perkembangan cp WHERE cp.`nokartu` = '"+noKartu+"' AND cp.`tanggal` = '"+tglT+"' ORDER BY cp.idcatatan DESC LIMIT 1";
					psCheckHD = connectionHD.prepareStatement(catatan_perkembangan);
					rsHDCheck = psCheckHD.executeQuery();
					while(rsHDCheck.next())
					{
						if(rsHDCheck.getString("jam2").equals(null) || rsHDCheck.getString("jam2").equals(""))
						{
							phd.setWaktuCatatanPerkembangan(null);						
						}
						else
						{						
							SimpleDateFormat dateFormatTanggal = new SimpleDateFormat("hh:mm");
						    Date parsedDateTanggal = dateFormatTanggal.parse(rsHDCheck.getString("jam2"));
						    Timestamp waktucatatan = new java.sql.Timestamp(parsedDateTanggal.getTime());
							phd.setWaktuCatatanPerkembangan(waktucatatan);
						}

						phd.setUtkDokter(rsHDCheck.getString("tepidokter"));
						phd.setUtkStaff(rsHDCheck.getString("tepiklinis"));
						phd.setTtdPerawat(rsHDCheck.getString("tepittd"));
						phd.setRekomendasiGizi(rsHDCheck.getString("asupangizi"));
						phd.setKaloriGizi(rsHDCheck.getString("kalori"));
						phd.setProteinGizi(rsHDCheck.getString("protein"));
						phd.setPengkajianGizi(rsHDCheck.getString("pengkajian"));
						phd.setEdukasiMateri(rsHDCheck.getString("materi"));
						phd.setLeafletBrosur(rsHDCheck.getString("LEAFLET_BROSUR"));
						phd.setRencanaKedepan(rsHDCheck.getString("rencanakedepan"));
					}					
					if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
					if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
					
					String akhir = "SELECT ak.`mesinhemo`, ak.`dialyzer`, ak.`desiheat`, ak.`rinse`, ak.`jenis`, ak.`komplikasi`, ak.`conductivity`, ak.`temperatur`, ak.`dialisat`, ak.`sisapriming`, ak.`drip`, ak.`darah`, ak.`washout`, ak.`minumsonde`,"
							+ "ak.`jumlah`, ak.`penekanan`, ak.`desiheater`, ak.`rinse2` FROM "
							+ "tb_akhir ak WHERE ak.`nokartu` = '"+noKartu+"' AND ak.`tanggal` = '"+tglT+"' ORDER BY ak.idakhir DESC LIMIT 1";
					psCheckHD = connectionHD.prepareStatement(akhir);
					rsHDCheck = psCheckHD.executeQuery();
					while(rsHDCheck.next())
					{
						phd.setNamaMesinHd(rsHDCheck.getString("mesinhemo"));
						phd.setDialyzerHd(rsHDCheck.getString("dialyzer"));
						phd.setDesinfectansSebelumHd(rsHDCheck.getBigDecimal("desiheat"));
						if(rsHDCheck.getString("rinse").equals("-"))
						{
							phd.setRinserSebelumHD(null);
						}
						else
						{
							phd.setRinserSebelumHD(rsHDCheck.getBigDecimal("rinse"));
						}
						phd.setJenisHd(rsHDCheck.getString("jenis"));
						phd.setKomplikasiHd(rsHDCheck.getString("komplikasi"));
						phd.setConductivitySebelumHd(rsHDCheck.getString("conductivity"));
						phd.setTemperaturSebelumHd(rsHDCheck.getString("temperatur"));
						phd.setDialisatSebelumHd(rsHDCheck.getString("dialisat"));
						phd.setSisaPriming(rsHDCheck.getString("sisapriming"));
						phd.setDripMasuk(rsHDCheck.getString("drip"));
						phd.setDarahMasuk(rsHDCheck.getString("darah"));
						phd.setWashOut(rsHDCheck.getString("washout"));
						phd.setMinumMasuk(rsHDCheck.getString("minumsonde"));
						phd.setJumlahMasuk(rsHDCheck.getString("jumlah"));
						
						if(rsHDCheck.getString("penekanan").equals("5 - 10"))
						{
							phd.setPenekananMasuk(null);
						}
						else
						{
							phd.setPenekananMasuk(rsHDCheck.getBigDecimal("penekanan"));
						}
						phd.setDesinfectantsSesudahHd(rsHDCheck.getBigDecimal("desiheater"));
						if(rsHDCheck.getString("rinse2").equals("-"))
						{
							phd.setRinseSesudahHd(null);
						}
						else
						{
							phd.setRinseSesudahHd(rsHDCheck.getBigDecimal("rinse2"));
						}
					}	
					if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
					if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
				
					
					String visit = "SELECT V.VISIT_ID FROM VISIT V INNER JOIN PATIENT PA "
							+ "ON PA.PATIENT_ID = V.PATIENT_ID INNER JOIN PERSON PE "
							+ "ON PE.PERSON_ID = PA.PERSON_ID INNER JOIN CARD CA "
							+ "ON CA.PERSON_ID = PE.PERSON_ID "
							+ "WHERE CA.CARD_NO = '"+noKartu+"' AND TRUNC(V.ADMISSION_DATETIME) = '"+formattedDate+"'"
							+ " AND ROWNUM = 1 ORDER BY ROWNUM";
					psCheckKTHIS = connectionKTHIS.prepareStatement(visit);
					rsKthisCheck = psCheckKTHIS.executeQuery();
					while(rsKthisCheck.next())
					{
						phd.setVisitId(rsKthisCheck.getBigDecimal("VISIT_ID"));
					}	
					if (psCheckKTHIS != null) try { psCheckKTHIS.close(); } catch (SQLException e) {}
					if (rsKthisCheck != null) try { rsKthisCheck.close(); } catch (SQLException e) {}
		
					String insertQuery = "INSERT INTO PROTOKOLHD (PROTOKOLHD_ID ,VISIT_ID ,TANGGAL_HD, DIAGNOSA_MEDIS, RIWAYAT_KESEHATAN, NOMOR_MESIN ,HEMODIALIS_KE ,TIPE_DIALISER ,CARA_BAYAR ,STATUS_PSIKOSOSIAL, "
							+ "STATUS_FUNGSIONAL ,RISIKO_JATUH ,ALERGI_OBAT ,SENSORIUM_PREHD ,TD_PREHD ,NADI_PREHD ,NAFAS_PREHD ,SUHU_PREHD ,BB_KERING_PREHD ,BB_DATANG_PREHD, BB_SEBELUMNYA_PREHD,"
							+ "BB_KENAIKAN_PREHD ,AKSES_VASKULAR_PREHD ,SENSORIUM_POSTHD ,TD_POSTHD ,NADI_POSTHD ,NAFAS_POSTHD ,SUHU_POSTHD ,BB_SELESAI_POSTHD , BEDA_BB_POSTHD, CATATAN_POSTHD ,WAKTU_MULAI ,LAMA_HD,"
							+ "WAKTU_SELESAI ,PRIMING_MASUK ,PRIMING_KELUAR ,ANTIKOAGULASI ,AWAL_HD ,MAINTENANCE ,TANPA_HEPARIN ,UFG_HD ,RESEP_HD ,DURASI_HD ,QB_INSTRUKSIMEDIK ,QD_INSTRUKSIMEDIK,"
							+ "TD_INSTRUKSIMEDIK, UFG_INSTRUKSIMEDIK, UFR_INSTRUKSIMEDIK, PROFILLING_INSTRUKSIMEDIK, ANTIKOAGULASI_INSTRUKSIMEDIK, NAMA_DOKTER, PEMERIKSAAN_FISIK, KEADAAN_UMUM,"
							+ "KONDISI_SAATINI, KONDISI_SAKIT, INDIKATOR_SAKIT, DIAGNOSA_KEPERAWATAN, INTERVENSI_KEPERAWATAN, INTERVENSI_KOLABORASI, CATATAN_KEPERAWATAN, NAMA_PERAWAT, WAKTU_CATATAN_PERKEMBANGAN,"
							+ "UTK_DOKTER, UTK_STAFF, TTD_PERAWAT, REKOMENDASI_GIZI, KALORI_GIZI, PROTEIN_GIZI, PENGKAJIAN_GIZI, EDUKASI_MATERI, LEAFLET_BROSUR, RENCANA_KEDEPAN, NAMA_MESINHD, DIALYZER_HD,"
							+ "DESINFECTANS_SEBELUMHD, RINSE_SEBELUMHD, JENIS_HD, KOMPLIKASI_HD, CONDUCTIVITY_SEBELUMHD, TEMPERATUR_SEBELUMHD, DIALISAT_SEBELUMHD, SISA_PRIMING, DRIP_MASUK, DARAH_MASUK,"
							+ "WASH_OUT, MINUM_MASUK, JUMLAH_MASUK, PENEKANAN_MASUK, DESINFECTANTS_SESUDAHHD, RINSE_SESUDAHHD, CREATED_BY, CREATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME, DEFUNCT_IND, CARD_NO)"
							+ "VALUES (pgsysfunc.fxGenPrimaryKey, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
								   + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
					psInsertKTHIS = connectionKTHIS.prepareStatement(insertQuery);
					psInsertKTHIS.setBigDecimal(1, phd.getVisitId());
					psInsertKTHIS.setTimestamp(2, phd.getTanggalHd());
					psInsertKTHIS.setString(3, phd.getDiagnosaMedis());
					psInsertKTHIS.setString(4, phd.getRiwayatKesehatan());
					psInsertKTHIS.setString(5, phd.getNomorMesin());
					psInsertKTHIS.setString(6, phd.getHemodialisisKe());
					psInsertKTHIS.setString(7, phd.getTipeDialiser());
					psInsertKTHIS.setString(8, phd.getCaraBayar());
					psInsertKTHIS.setString(9, phd.getStatusPsikososial());
					psInsertKTHIS.setString(10, phd.getStatusFungsional());
//					/psInsertKTHIS.setString(11, phd.getRisikoJatuh());
					psInsertKTHIS.setString(12, phd.getAlergiObat());
					psInsertKTHIS.setString(13, phd.getSensoriumPreHd());
					psInsertKTHIS.setString(14, phd.getTdPreHd());
					psInsertKTHIS.setString(15, phd.getNadiPreHd());
					psInsertKTHIS.setString(16, phd.getNafasPreHd());
					psInsertKTHIS.setString(17, phd.getSuhuPreHd());
					psInsertKTHIS.setString(18, phd.getBbKeringPreHd());
					psInsertKTHIS.setString(19, phd.getBbDatangPreHd());
					psInsertKTHIS.setString(20, phd.getBbSebelumnyaPreHd());
					psInsertKTHIS.setString(21, phd.getBbKenaikanPreHd());
					psInsertKTHIS.setString(22, phd.getAksesVaskularPreHd());
					psInsertKTHIS.setString(23, phd.getSensoriumPostHd());
					psInsertKTHIS.setString(24, phd.getTdPostHd());
					psInsertKTHIS.setString(25, phd.getNadiPostHd());
					psInsertKTHIS.setString(26, phd.getNafasPostHd());
					psInsertKTHIS.setString(27, phd.getSuhuPostHd());
					psInsertKTHIS.setString(28, phd.getBbSelesaiPostHd());
					psInsertKTHIS.setString(29, phd.getBedaBbPostHd());
					psInsertKTHIS.setString(30, phd.getCatatanPostHd());
					psInsertKTHIS.setTimestamp(31, phd.getWaktuMulai());
					psInsertKTHIS.setBigDecimal(32, phd.getLamaHd());
					psInsertKTHIS.setTimestamp(33, phd.getWaktuSelesai());
					psInsertKTHIS.setString(34, phd.getPrimingMasuk());
					psInsertKTHIS.setString(35, phd.getPrimingKeluar());
					psInsertKTHIS.setString(36, phd.getAntiKoagulasi());
					psInsertKTHIS.setString(37, phd.getAwalHd());
					psInsertKTHIS.setString(38, phd.getMaintenance());
					psInsertKTHIS.setString(39, phd.getTanpaHeparin());
					psInsertKTHIS.setString(40, phd.getUfgHd());
					psInsertKTHIS.setString(41, phd.getResepHd());
					psInsertKTHIS.setString(42, phd.getDurasiHd());
					psInsertKTHIS.setString(43, phd.getQbInstruksiMedik());
					psInsertKTHIS.setString(44, phd.getQdInstruksiMedik());
					psInsertKTHIS.setString(45, phd.getTdInstruksiMedik());
					psInsertKTHIS.setString(46, phd.getUfgInstruksiMedik());
					psInsertKTHIS.setString(47, phd.getUfrInstruksiMedik());
					psInsertKTHIS.setString(48, phd.getProfillingInstruksiMedik());
					psInsertKTHIS.setString(49, phd.getAntikoagulasiInstruksiMedik());
					psInsertKTHIS.setString(50, phd.getNamaDokter());
					psInsertKTHIS.setString(51, phd.getPemeriksaanFisik());
					psInsertKTHIS.setString(52, phd.getKeadaanUmum());
					psInsertKTHIS.setString(53, phd.getKondisiSaatIni());
					psInsertKTHIS.setString(54, phd.getKondisiSakit());
					psInsertKTHIS.setBigDecimal(55, phd.getIndikatorSakit());
					psInsertKTHIS.setString(56, phd.getDiagnosaKeperawatan());
					psInsertKTHIS.setString(57, phd.getIntervensiKeperawatan());
					psInsertKTHIS.setString(58, phd.getIntervensiKolaborasi());
					psInsertKTHIS.setString(59, phd.getCatatanKeperawatan());
					psInsertKTHIS.setString(60, phd.getNamaPerawat());
					psInsertKTHIS.setTimestamp(61, phd.getWaktuCatatanPerkembangan());
					psInsertKTHIS.setString(62, phd.getUtkDokter());
					psInsertKTHIS.setString(63, phd.getUtkStaff());
					psInsertKTHIS.setString(64, phd.getTtdPerawat());
					psInsertKTHIS.setString(65, phd.getRekomendasiGizi());
					psInsertKTHIS.setString(66, phd.getKaloriGizi());
					psInsertKTHIS.setString(67, phd.getProteinGizi());
					psInsertKTHIS.setString(68, phd.getPengkajianGizi());
					psInsertKTHIS.setString(69, phd.getEdukasiMateri());
					psInsertKTHIS.setString(70, phd.getLeafletBrosur());
					psInsertKTHIS.setString(71, phd.getRencanaKedepan());
					psInsertKTHIS.setString(72, phd.getNamaMesinHd());
					psInsertKTHIS.setString(73, phd.getDialyzerHd());
					psInsertKTHIS.setBigDecimal(74, phd.getDesinfectansSebelumHd());
					psInsertKTHIS.setBigDecimal(75, phd.getRinserSebelumHD());
					psInsertKTHIS.setString(76, phd.getJenisHd());
					psInsertKTHIS.setString(77, phd.getKomplikasiHd());
					psInsertKTHIS.setString(78, phd.getConductivitySebelumHd());
					psInsertKTHIS.setString(79, phd.getTemperaturSebelumHd());
					psInsertKTHIS.setString(80, phd.getDialisatSebelumHd());
					psInsertKTHIS.setString(81, phd.getSisaPriming());
					psInsertKTHIS.setString(82, phd.getDripMasuk());
					psInsertKTHIS.setString(83, phd.getDarahMasuk());
					psInsertKTHIS.setString(84, phd.getWashOut());
					psInsertKTHIS.setString(85, phd.getMinumMasuk());
					psInsertKTHIS.setString(86, phd.getJumlahMasuk());
					psInsertKTHIS.setBigDecimal(87, phd.getPenekananMasuk());
					psInsertKTHIS.setBigDecimal(88, phd.getDesinfectantsSesudahHd());
					psInsertKTHIS.setBigDecimal(89, phd.getRinseSesudahHd());
					psInsertKTHIS.setString(90, null);
					psInsertKTHIS.setTimestamp(91, phd.getTanggal());
					psInsertKTHIS.setString(92, null);
					psInsertKTHIS.setTimestamp(93, phd.getTanggal());
					psInsertKTHIS.setString(94, "N");
					psInsertKTHIS.setString(95, phd.getCardNo().toString());					
					psInsertKTHIS.executeQuery();	
					if (psInsertKTHIS != null) try { psInsertKTHIS.close(); } catch (SQLException e) {}
				}
			}			
			connectionHD.close(); 
			connectionKTHIS.close();
		}catch(Exception e){
			e.printStackTrace();
			if (connectionHD != null) try { connectionHD.rollback();} catch (Exception ex){}
			if (connectionKTHIS != null) try { connectionKTHIS.rollback();} catch (Exception ex){}
		}finally {
			if (connectionHD != null) try { connectionHD.close();} catch (Exception ex){}
			if (connectionKTHIS != null) try { connectionKTHIS.close();} catch (Exception ex){}
			if (psInsertKTHIS != null) try {psInsertKTHIS.close(); } catch (SQLException e) {}
			if (psCheckHD != null) try {psCheckHD.close(); } catch (SQLException e) {}
			if (psCheckKTHIS != null) try {psCheckKTHIS.close(); } catch (SQLException e) {}
			if (psHD != null) try { psHD.close(); } catch (SQLException e) {}
			if (rsHD != null) try { rsHD.close(); } catch (SQLException e) {}
			if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
			if (rsKthisCheck != null) try { rsKthisCheck.close(); } catch (SQLException e) {}
		}		 
	}
	
	
}
