package com.rsmurniteguh.webservice.dep.tasks;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.rsmurniteguh.webservice.dep.kthis.model.ProtokolHD;
import com.rsmurniteguh.webservice.dep.kthis.model.ProtokolHdPart;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class CopyDataTaskPart {
	/*public static void copyData(){
		Connection connectionHD = DbConnection.getMysqlOnlineQueueInstance2();
		Connection connectionKTHIS = DbConnection.getPooledConnection();
		if(connectionHD == null || connectionKTHIS == null)return;
		ResultSet rsHD=null;
		ResultSet rsHDCheck=null;
		ResultSet rsKthisCheck=null;
		PreparedStatement psInsertKTHIS=null;
		PreparedStatement psHD=null;
		PreparedStatement psCheckHD=null;
		PreparedStatement psCheckKTHIS=null;
		List<ProtokolHdPart> listAll = new ArrayList<ProtokolHdPart>();

		String waktu_instruksi = "SELECT wi.nokartu, wi.totwakmul, wi.tanggal FROM tb_waktu_instruksi wi where wi.`tanggal` != '0000-00-00'";
		try {
			psHD = connectionHD.prepareStatement(waktu_instruksi);
			rsHD = psHD.executeQuery();
			while(rsHD.next())
			{
			    
			    SimpleDateFormat dateFormatTanggal = new SimpleDateFormat("yyyy-MM-dd");
			    Date parsedDateTanggal = dateFormatTanggal.parse(rsHD.getString("tanggal"));
			    Timestamp tanggal = new java.sql.Timestamp(parsedDateTanggal.getTime());
			    
			    String[] parts = rsHD.getString("totwakmul").split("\\+");			    
			    String rightside = parts[0];
			    rightside = rightside.replace(".", ":").replace(";", ":").replace(" ", "").replace("O", "0").replace("`", "");
			    int counter = 0;
			    for( int i=0; i<rightside.length(); i++ ) {
			        if( rightside.charAt(i) == ':' ) {
			            counter++;
			        } 
			    }
			    if(counter == 1 && rightside.length() >= 4)
			    {
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			    	Date parsedDate = dateFormat.parse(rsHD.getString("tanggal") + " " +rightside);
				    Timestamp waktu_pemeriksaan = new java.sql.Timestamp(parsedDate.getTime());
				    
				    ProtokolHdPart ph = new ProtokolHdPart();
				    ph.setNokartu(rsHD.getBigDecimal("nokartu"));
					ph.setWaktuPemeriksaan(waktu_pemeriksaan);
					ph.setQbPemeriksaan(parts[1]);
					ph.setQdPemeriksaan(parts[2]);
					ph.setUfrPemeriksaan(parts[3]);
					ph.setTdPemeriksaan(parts[4]);
					ph.setNadiPemeriksaan(parts[5]);
					ph.setNrsPemeriksaan(parts[6]);
					ph.setSuhuPemeriksaan(parts[7]);
					ph.setRrPemeriksaan(parts[8]);
					ph.setVpPemeriksaan(parts[9]);
					ph.setApPemeriksaan(parts[10]);
					ph.setTmpPemeriksaan(parts[11]);
					ph.setKeteranganPemeriksaan(parts[12]);
					ph.setParafPemeriksa(parts[13]);
					ph.setCreatedDatetime(tanggal);
					listAll.add(ph);
			    }
			    else
			    {
			    	continue;
			    }
			    
			}
			if (psHD != null) try { psHD.close(); } catch (SQLException e) {}
			if (rsHD != null) try { rsHD.close(); } catch (SQLException e) {}
			
			for (int a=0;a<listAll.size();a++){
				ProtokolHdPart phd = listAll.get(a);
				BigDecimal noKartu = phd.getNokartu();
				Timestamp tglT = phd.getCreatedDatetime();
				
				Date date = new Date();
				date.setTime(tglT.getTime());
				String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
				

				String visit = "SELECT V.VISIT_ID FROM VISIT V INNER JOIN PATIENT PA "
						+ "ON PA.PATIENT_ID = V.PATIENT_ID INNER JOIN PERSON PE "
						+ "ON PE.PERSON_ID = PA.PERSON_ID INNER JOIN CARD CA "
						+ "ON CA.PERSON_ID = PE.PERSON_ID "
						+ "WHERE CA.CARD_NO = '"+noKartu+"' AND TRUNC(V.ADMISSION_DATETIME) = '"+formattedDate+"'"
						+ " AND ROWNUM = 1 ORDER BY ROWNUM";
				psCheckKTHIS = connectionKTHIS.prepareStatement(visit);
				rsKthisCheck = psCheckKTHIS.executeQuery();
				while(rsKthisCheck.next())
				{
					phd.setVisitId(rsKthisCheck.getBigDecimal("VISIT_ID"));
				}	
				if (psCheckKTHIS != null) try { psCheckKTHIS.close(); } catch (SQLException e) {}
				if (rsKthisCheck != null) try { rsKthisCheck.close(); } catch (SQLException e) {}
				String insertQuery = "INSERT INTO PROTOKOLHD_PART (PROTOKOLHD_PART_ID ,VISIT_ID ,WAKTU_PEMERIKSAAN ,QB_PEMERIKSAAN ,QD_PEMERIKSAAN ,UFR_PEMERIKSAAN ,TD_PEMERIKSAAN ,NADI_PEMERIKSAAN ,NRS_PEMERIKSAAN,"
						+ "SUHU_PEMERIKSAAN ,RR_PEMERIKSAAN ,VP_PEMERIKSAAN ,AP_PEMERIKSAAN ,TMP_PEMERIKSAAN ,KETERANGAN_PEMERIKSAAN ,PARAF_PEMERIKSA,"
						+ "CREATED_BY, CREATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME, DEFUNCT_IND)"
						+ "VALUES (pgsysfunc.fxGenPrimaryKey, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				psInsertKTHIS = connectionKTHIS.prepareStatement(insertQuery);
				psInsertKTHIS.setBigDecimal(1, phd.getVisitId());
				psInsertKTHIS.setTimestamp(2, phd.getWaktuPemeriksaan());
				psInsertKTHIS.setString(3, phd.getQbPemeriksaan());
				psInsertKTHIS.setString(4, phd.getQdPemeriksaan());
				psInsertKTHIS.setString(5, phd.getUfrPemeriksaan());
				psInsertKTHIS.setString(6, phd.getTdPemeriksaan());
				psInsertKTHIS.setString(7, phd.getNadiPemeriksaan());
				psInsertKTHIS.setString(8, phd.getNrsPemeriksaan());
				psInsertKTHIS.setString(9, phd.getSuhuPemeriksaan());
				psInsertKTHIS.setString(10, phd.getRrPemeriksaan());
				psInsertKTHIS.setString(11, phd.getVpPemeriksaan());
				psInsertKTHIS.setString(12, phd.getApPemeriksaan());
				psInsertKTHIS.setString(13, phd.getTmpPemeriksaan());
				psInsertKTHIS.setString(14, phd.getKeteranganPemeriksaan());
				psInsertKTHIS.setString(15, phd.getParafPemeriksa());
				psInsertKTHIS.setString(16, null);
				psInsertKTHIS.setTimestamp(17, phd.getCreatedDatetime());
				psInsertKTHIS.setString(18, null);
				psInsertKTHIS.setTimestamp(19, phd.getCreatedDatetime());
				psInsertKTHIS.setString(20, "N");					
				psInsertKTHIS.executeQuery();	
				if (psInsertKTHIS != null) try { psInsertKTHIS.close(); } catch (SQLException e) {}
			}
		connectionHD.close(); 
		connectionKTHIS.close();
		}catch(Exception e){
			e.printStackTrace();
			if (connectionHD != null) try { connectionHD.rollback();} catch (Exception ex){}
			if (connectionKTHIS != null) try { connectionKTHIS.rollback();} catch (Exception ex){}
		}finally {
			if (psInsertKTHIS != null) try {psInsertKTHIS.close(); } catch (SQLException e) {}
			if (psCheckHD != null) try {psCheckHD.close(); } catch (SQLException e) {}
			if (psCheckKTHIS != null) try {psCheckKTHIS.close(); } catch (SQLException e) {}
			if (psHD != null) try { psHD.close(); } catch (SQLException e) {}
			if (rsHD != null) try { rsHD.close(); } catch (SQLException e) {}
			if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
			if (rsKthisCheck != null) try { rsKthisCheck.close(); } catch (SQLException e) {}
		}		 
	}*/
	
	
}
