package com.rsmurniteguh.webservice.dep.tasks;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.rsmurniteguh.webservice.dep.all.model.viewJournal;
import com.rsmurniteguh.webservice.dep.all.model.viewJournal2;
import com.rsmurniteguh.webservice.dep.kthis.services.StoreDispanceSer;

public class DailyScheduleMTPosPostJournal implements Job {
	private String SALE_TYPE = "SLT";
	private String SALE_TYPE_RETAIL = SALE_TYPE + "RET";
	private String SALE_TYPE_TENDER = SALE_TYPE + "TEN";
	private String SALE_TYPE_CAPTAIN_ORDER = SALE_TYPE + "CTO";
	
	private String SALE_PAYMENT_METHOD = "SPM";
	private String SALE_PAYMENT_METHOD_RETAIL = SALE_PAYMENT_METHOD + "1";
	private String SALE_PAYMENT_METHOD_EMONEY = SALE_PAYMENT_METHOD + "2";
	private String SALE_PAYMENT_METHOD_DEBIT = SALE_PAYMENT_METHOD + "3";
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
	}
	
	private void postSalesRetail(){
		Connection connectionApotik = null;
		Connection connectionGLApotik = null;
		if(connectionApotik == null || connectionGLApotik == null)return;
		ResultSet rsApotik=null;
		PreparedStatement psApotik=null;
		PreparedStatement psGLApotik=null;
//		String query = "SELECT * FROM SaleDetails sd "
//				+ " INNER JOIN  Sales s ON s.Sale_ID = sd.SaleDetail_ID "
//				+ " INNER JOIN CounterCollections cc ON cc.CounterCollection_ID = s.CounterCollection_ID "
//				+ " INNER JOIN stores st on st.LoginStore_ID = cc.LoginStore_ID "
//				+ " INNER JOIN Storematerial sm ON sm.store_id = st.Store_Id and sd.Material_ID = sm.material_id "
//				+ " INNER JOIN CodeMstrs cm1 ON cm1.Code_Cat + cm1.Code_Abbr = s.Sale_Type "
//				+ " INNER JOIN CodeMstrs cm2 ON cm2.Code_Cat + cm2.Code_Abbr = s.Payment_Method "
//				+ " INNER JOIN Vendors ven ON ven.Vendor_ID = s.Vendor_ID "
//				+ " WHERE cc.Sale_Journal_Ind IS NULL "
//				+ " AND cc.Counter_Status = 'CCS2' ";
		String query = "select * from ("
						+ "  select cc.CounterCollection_ID, s.Sale_Type, s.Payment_Method,'' as category_id,sum(sale_detail_total_price - (sale_detail_total_price * s.Disocunt_Rate / 100)) as 'amount' from SaleDetails sd"
						+ "   inner join Sales s on s.Sale_ID = sd.Sale_ID "
						+ "   inner join CounterCollections cc on cc.CounterCollection_ID = s.CounterCollection_ID "
						+ "   where cc.Sale_Journal_Ind is null "
//						+ "   --AND cc.CounterCollection_ID = 138 "
						+ "   group by cc.CounterCollection_ID, s.Sale_Type, s.Payment_Method "
						+ "  union "
						+ "  select cc.CounterCollection_ID, s.Sale_Type, s.payment_method,c.Category_Name, sum(sale_detail_total_price - (sale_detail_total_price * s.Disocunt_Rate / 100)) from SaleDetails sd "
						+ "   inner join Materials m on m.Material_ID = sd.Material_ID "
						+ "   inner join Category c on c.Category_ID = m.Category_ID "
						+ "   inner join Sales s on s.Sale_ID = sd.Sale_ID "
						+ "   inner join CounterCollections cc on cc.CounterCollection_ID = s.CounterCollection_ID "
						+ "   inner join stores str on str.store_id = "
						+ "   where cc.Sale_Journal_Ind is null "
//						+ "   --AND cc.CounterCollection_ID = 138 "
						+ "   and c.Defunct_Ind = 0 "
						+ "  group by  cc.CounterCollection_ID, s.Sale_Type, s.payment_method,c.Category_Name ) t"
						+ " order by t.CounterCollection_ID, t.Sale_Type, t.Payment_method";
		String oldIdt = "";
		try {
			psApotik = connectionApotik.prepareStatement(query);
			rsApotik = psApotik.executeQuery();
			while (rsApotik.next()){
				String saleType = rsApotik.getString("Sale_Type");

				String cashierName = rsApotik.getString("FIRST_NAME") + " " + rsApotik.getString("LAST_NAME");
				BigDecimal tipe = hardCodeCompanyId(rsApotik.getBigDecimal("Company_id")); 
				String sandi = rsApotik.getString("sandi");
				String dis = rsApotik.getString("disc");
				Timestamp tglT = rsApotik.getTimestamp("");
				String grandTotal = rsApotik.getBigDecimal("SALE_GRAND_TOTAL").toString();
				String nasup = rsApotik.getString("tipePayment");
				String total = rsApotik.getBigDecimal("amount").toString();
				
				String nsp = nasup;
				String tot = total;
				String kat = rsApotik.getString("category_name");
				
				if (saleType.equals(SALE_TYPE_RETAIL)){
									
					String idt = builtRetailTransactionId(rsApotik.getBigDecimal("counterCollectionId") 
							, rsApotik.getString("Payment_Method")
							, cashierName// cashierName
							, tipe
							, tglT);
					
					if (!oldIdt.equals(idt)){
						StoreDispanceSer.addJpj( idt
								, grandTotal // total
								, tipe.toString() //tipe
								, cashierName);
						oldIdt = idt;
					}					
					
					List<viewJournal2> stats = StoreDispanceSer.addJpjDetail(idt
							, new SimpleDateFormat("MM/d/yyyy").format(tglT)
							, dis
							, sandi
							, nasup
							, total
							, tipe.toString());
					
					List<viewJournal> statss = StoreDispanceSer.addJpjNilai(sandi
							, new SimpleDateFormat("MM/d/yyyy").format(tglT) //tglT
							, nsp
							, idt
							, tot
							, kat
							, tipe.toString());
				}else if(saleType.equals(SALE_TYPE_TENDER)){
					String idt = rsApotik.getBigDecimal("counterCollectionId").toString() + "-TENDER-" + rsApotik.getString("FIRST_NAME");
					
				}
				else if(saleType.equals(SALE_TYPE_CAPTAIN_ORDER)){
					
				}
			}
			connectionApotik.commit();
			connectionGLApotik.commit();
		}catch(Exception e){
			e.printStackTrace();
			if (connectionApotik != null) try { connectionApotik.rollback();} catch (Exception ex){}
			if (connectionGLApotik != null) try { connectionGLApotik.rollback();} catch (Exception ex){}
		}finally {
			if (connectionApotik != null) try {connectionApotik.setAutoCommit(true);connectionApotik.close();} catch (Exception e){}
			if (connectionGLApotik != null) try {connectionGLApotik.setAutoCommit(true);connectionGLApotik.close();} catch (Exception e){}
			if (psApotik != null) try { psApotik.close(); } catch (SQLException e) {}
			if (psGLApotik != null) try { psGLApotik.close(); } catch (SQLException e) {}
			if (rsApotik != null) try { rsApotik.close(); } catch (SQLException e) {}
		}		 
	}
	
    private static String builtRetailTransactionId(BigDecimal counterCollectionId, String paymentMethod, String cashierName, BigDecimal companyId, Timestamp transactionDate)
    {
        String result = "";
        StringBuilder sb = new StringBuilder();
        sb.append(counterCollectionId);
        sb.append("-").append(new SimpleDateFormat("ddMM").format(transactionDate));
        if (companyId.equals(new BigDecimal(5))) sb.append("-HF");
        sb.append("-").append(paymentMethod);
        sb.append("-").append(cashierName);
        result = sb.toString();
        return result;
    }
    
    private static BigDecimal hardCodeCompanyId(BigDecimal companyId)
    {
        return companyId.equals(new BigDecimal(5)) ? new BigDecimal(2) : companyId;
    }
    
    
	
}
