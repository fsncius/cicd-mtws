package com.rsmurniteguh.webservice.dep.tasks.mobile;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.stream.Collectors;

import com.rsmurniteguh.webservice.dep.all.model.doctorsMobile.DoctorVisitRegnList;
import com.rsmurniteguh.webservice.dep.kthis.services.mobile.DoctorMobileService;

public class MobileRegnNotif extends TimerTask{
	public void run() {
		List<DoctorVisitRegnList> allList = new ArrayList<DoctorVisitRegnList>();
		allList = new DoctorMobileService().GetDoctorsAvailableRegistration();
		
		List<String> tempDoctors = allList.stream().map(DoctorVisitRegnList::getDokter).collect(Collectors.toList());
		List<String> doctors = tempDoctors.stream().distinct().collect(Collectors.toList());
		Map<String, List<DoctorVisitRegnList>> map = new HashMap<String, List<DoctorVisitRegnList>>();
		
		for(String doctor : doctors)
		{
			List<DoctorVisitRegnList> currentDoctorList = allList.stream().filter(p-> p.dokter.equals(doctor)).collect(Collectors.toList());
			map.put(doctor, currentDoctorList);
			String patient = "";
			for(DoctorVisitRegnList visit : currentDoctorList)
			{
				patient.concat(visit.getPasien()).concat(", ");
			}
		}
	}
}