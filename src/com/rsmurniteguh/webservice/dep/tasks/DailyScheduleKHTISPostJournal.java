package com.rsmurniteguh.webservice.dep.tasks;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.rsmurniteguh.webservice.dep.kthis.services.PatientTransactionService;

public class DailyScheduleKHTISPostJournal implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		PatientTransactionService.processAllBill(new Timestamp(cal.getTimeInMillis()));
	}
	
}
