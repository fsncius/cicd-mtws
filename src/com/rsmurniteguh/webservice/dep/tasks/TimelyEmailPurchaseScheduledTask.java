package com.rsmurniteguh.webservice.dep.tasks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class TimelyEmailPurchaseScheduledTask extends EmailScheduledPurchaseTask  {
	final private String storeMstrIDStorage = "311102896";
	final private String storeMstrIDLaboratory = "329035678";
	final private String storeMstrIDLabPatologiAnatomik = "311530486";
	final private String storeMstrIDLabMikrobiology = "311530516";
	
	public void run() {	
		CommonService cs = new CommonService();
		if(!cs.getParameterValue(IParameterConstant.TIMELY_EMAIL_PURCHASING).equals(IConstant.IND_YES)) return;
		
		this.reportName = "Purchasing Report";
		
		Connection pooledConnection = null;
		PreparedStatement pstmtPRConfirm = null;
		PreparedStatement pstmtPRApprove = null;
		PreparedStatement pstmtUpdatePRConfirmEmail = null;
		PreparedStatement pstmtUpdatePRApproveEmail = null;
		ResultSet rsPRConfirm = null;
		ResultSet rsPRApprove = null;
		try {
	    	pooledConnection = DbConnection.getPooledConnection();
	    	if (pooledConnection == null) throw new Exception("Database failed to connect !");
	    	pooledConnection.setAutoCommit(false);	    	
	    	
	        if ( pooledConnection != null) {
	        	String selectPRConfirm = "SELECT STOCKPURCHASEREQUISITION_ID, PURCHASE_REQUISITION_NO, CONFIRMED_DATETIME, STOREMSTR_ID FROM STOCKPURCHASEREQUISITION "
	        			+ " WHERE CONFIRM_EMAIL_IND='N' "
	        			+ " AND PURCHASE_STATUS IN ('SPS10','SPS11','SPS2') ";
	        	pstmtPRConfirm = pooledConnection.prepareStatement(selectPRConfirm);
	        	rsPRConfirm = pstmtPRConfirm.executeQuery();
	        	String prConfirmStorage = "";
	        	String prConfirmLaboratory  = "";
	        	String prConfirmLabPatologiAnatomik  = "";
	        	String prConfirmLabMikrobiology = "";
	        	while (rsPRConfirm.next()){
	        		Boolean isEmailed = false;
	        		if (rsPRConfirm.getBigDecimal("STOREMSTR_ID").toString().equals(storeMstrIDStorage)){
	        			prConfirmStorage += "<li>" + rsPRConfirm.getString("PURCHASE_REQUISITION_NO") + " " + new SimpleDateFormat("dd/MMM/YYYY").format(rsPRConfirm.getTimestamp("CONFIRMED_DATETIME")) + "</li>";
	        			isEmailed = true;
	        		}
	        		else if (rsPRConfirm.getBigDecimal("STOREMSTR_ID").toString().equals(storeMstrIDLaboratory)){
	        			prConfirmLaboratory += "<li>" + rsPRConfirm.getString("PURCHASE_REQUISITION_NO") + " " + new SimpleDateFormat("dd/MMM/YYYY").format(rsPRConfirm.getTimestamp("CONFIRMED_DATETIME")) + "</li>";
	        			isEmailed = true;
	        		}
	        		else if (rsPRConfirm.getBigDecimal("STOREMSTR_ID").toString().equals(storeMstrIDLabPatologiAnatomik)){
	        			prConfirmLabPatologiAnatomik += "<li>" + rsPRConfirm.getString("PURCHASE_REQUISITION_NO") + " " + new SimpleDateFormat("dd/MMM/YYYY").format(rsPRConfirm.getTimestamp("CONFIRMED_DATETIME")) + "</li>";
	        			isEmailed = true;
	        		}
	        		else if (rsPRConfirm.getBigDecimal("STOREMSTR_ID").toString().equals(storeMstrIDLabMikrobiology)){
	        			prConfirmLabMikrobiology += "<li>" + rsPRConfirm.getString("PURCHASE_REQUISITION_NO") + " " + new SimpleDateFormat("dd/MMM/YYYY").format(rsPRConfirm.getTimestamp("CONFIRMED_DATETIME")) + "</li>";
	        			isEmailed = true;
	        		}
	        		if (isEmailed){
	        			String updatePRConfirmEmail = "UPDATE STOCKPURCHASEREQUISITION "
	        					+ " SET CONFIRM_EMAIL_IND='Y' "
	        					+ " WHERE STOCKPURCHASEREQUISITION_ID = ?";
	        			pstmtUpdatePRConfirmEmail = pooledConnection.prepareStatement(updatePRConfirmEmail);
	        			pstmtUpdatePRConfirmEmail.setBigDecimal(1, rsPRConfirm.getBigDecimal("STOCKPURCHASEREQUISITION_ID"));
	        			pstmtUpdatePRConfirmEmail.executeUpdate();
	        		}
	        	}	        	
	        	String headerTo = "dr. Hendric dan dr. Limenta";
	        	String emailTo = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_CONFIRM_PR_EMAIL_TO); //"dr.hendric@rsmurniteguh.com, drlimenta@rsmurniteguh.com";
	        	String emailcc = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_CONFIRM_PR_STORAGE_EMAIL_CC); //"suvinafonger@rsmurniteguh.com";
	        	sendEmail(headerTo, prConfirmStorage, "confirm", "Storage", emailTo, emailcc, "");
	        	emailcc = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_CONFIRM_PR_LABORATORY_EMAIL_CC);// "jeniver@rsmurniteguh.com";
	        	sendEmail(headerTo, prConfirmLaboratory, "confirm", "Laboratory",  emailTo, emailcc, "");
	        	emailcc = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_CONFIRM_PR_LABPATOLOGIANATOMI_EMAIL_CC); // "drsufida@rsmurniteguh.com";
	        	sendEmail(headerTo, prConfirmLabPatologiAnatomik, "confirm", "Lab Patologi Anatomik",  emailTo, emailcc, "");
	        	emailcc = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_CONFIRM_PR_LABMIKROBIOLOGY_EMAIL_CC);// "dr.rosnawaty@rsmurniteguh.com, labmikro@rsmurniteguh.com";
	        	sendEmail(headerTo, prConfirmLabMikrobiology, "confirm", "Lab Mikrobiology",  emailTo, emailcc, "");
	        	
	        	String selectPRApprove = "SELECT STOCKPURCHASEREQUISITION_ID, PURCHASE_REQUISITION_NO, CONFIRMED_DATETIME, STOREMSTR_ID FROM STOCKPURCHASEREQUISITION "
	        			+ " WHERE APPROVE_EMAIL_IND='N' "
	        			+ " AND PURCHASE_STATUS IN ('SPS3','SPS8','SPS2') ";
	        	pstmtPRApprove = pooledConnection.prepareStatement(selectPRApprove);
	        	rsPRApprove = pstmtPRApprove.executeQuery();
	        	String prApproveStorage = "";
	        	String prApproveLaboratory  = "";
	        	String prApproveLabPatologiAnatomik  = "";
	        	String prApproveLabMikrobiology = "";
	        	while (rsPRApprove.next()){
	        		Boolean isEmailed = false;
	        		if (rsPRApprove.getBigDecimal("STOREMSTR_ID").toString().equals(storeMstrIDStorage)){
	        			prApproveStorage += "<li>" + rsPRApprove.getString("PURCHASE_REQUISITION_NO") + " " + new SimpleDateFormat("dd/MMM/YYYY").format(rsPRApprove.getTimestamp("CONFIRMED_DATETIME")) + "</li>";
	        			isEmailed = true;
	        		}
	        		else if (rsPRApprove.getBigDecimal("STOREMSTR_ID").toString().equals(storeMstrIDLaboratory)){
	        			prApproveLaboratory += "<li>" + rsPRApprove.getString("PURCHASE_REQUISITION_NO") + " " + new SimpleDateFormat("dd/MMM/YYYY").format(rsPRApprove.getTimestamp("CONFIRMED_DATETIME")) + "</li>";
	        			isEmailed = true;
	        		}
	        		else if (rsPRApprove.getBigDecimal("STOREMSTR_ID").toString().equals(storeMstrIDLabPatologiAnatomik)){
	        			prApproveLabPatologiAnatomik += "<li>" + rsPRApprove.getString("PURCHASE_REQUISITION_NO") + " " + new SimpleDateFormat("dd/MMM/YYYY").format(rsPRApprove.getTimestamp("CONFIRMED_DATETIME")) + "</li>";
	        			isEmailed = true;
	        		}
	        		else if (rsPRApprove.getBigDecimal("STOREMSTR_ID").toString().equals(storeMstrIDLabMikrobiology)){
	        			prApproveLabMikrobiology += "<li>" + rsPRApprove.getString("PURCHASE_REQUISITION_NO") + " " + new SimpleDateFormat("dd/MMM/YYYY").format(rsPRApprove.getTimestamp("CONFIRMED_DATETIME")) + "</li>";
	        			isEmailed = true;
	        		}
	        		if (isEmailed){
	        			String updatePRApproveEmail = "UPDATE STOCKPURCHASEREQUISITION "
	        					+ " SET APPROVE_EMAIL_IND='Y' "
	        					+ " WHERE STOCKPURCHASEREQUISITION_ID = ?";
	        			pstmtUpdatePRApproveEmail = pooledConnection.prepareStatement(updatePRApproveEmail);
	        			pstmtUpdatePRApproveEmail.setBigDecimal(1, rsPRApprove.getBigDecimal("STOCKPURCHASEREQUISITION_ID"));
	        			pstmtUpdatePRApproveEmail.executeUpdate();
	        		}
	        	}
	        	headerTo = "Purchasing";
		        emailTo = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_APPROVE_PR_EMAIL_TO);// "mariani@rsmurniteguh.com, endi@rsmurniteguh.com, evelyn@rsmurniteguh.com, dewi2@rsmurniteguh.com";
		        emailcc = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_APPROVE_PR_STORAGE_EMAIL_CC); // "dr.hendric@rsmurniteguh.com, drlimenta@rsmurniteguh.com, suvinafonger@rsmurniteguh.com";
	        	sendEmail(headerTo, prApproveStorage, "approve", "Storage", emailTo, emailcc, "");
	        	emailcc = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_APPROVE_PR_LABORATORY_EMAIL_CC);//"dr.hendric@rsmurniteguh.com, drlimenta@rsmurniteguh.com, jeniver@rsmurniteguh.com";
	        	sendEmail(headerTo, prApproveLaboratory, "approve", "Laboratory", emailTo, emailcc, "");
	        	emailcc = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_APPROVE_PR_LABPATOLOGIANATOMI_EMAIL_CC);//"dr.hendric@rsmurniteguh.com, drlimenta@rsmurniteguh.com, drsufida@rsmurniteguh.com";
	        	sendEmail(headerTo, prApproveLabPatologiAnatomik, "approve", "Lab Patologi Anatomik", emailTo, emailcc, "");
	        	emailcc = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_APPROVE_PR_LABMIKROBIOLOGY_EMAIL_CC);// "dr.hendric@rsmurniteguh.com, drlimenta@rsmurniteguh.com, dr.rosnawaty@rsmurniteguh.com, labmikro@rsmurniteguh.com";
	        	sendEmail(headerTo, prApproveLabMikrobiology, "approve", "Lab Mikrobiology", emailTo, emailcc, "");
	        	
	        	pooledConnection.commit();
	        	
	        	pooledConnection.close();
	        }
		} catch (SQLException e){
			e.printStackTrace();
		    if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
	    } catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		} finally {
	    	if (pooledConnection != null) try {pooledConnection.setAutoCommit(true); pooledConnection.close(); } catch (SQLException e) {}
	    	if (pstmtPRConfirm != null) try { pstmtPRConfirm.close(); } catch (SQLException e) {}
	    	if (pstmtPRApprove != null) try { pstmtPRApprove.close(); } catch (SQLException e) {}
	    	if (pstmtUpdatePRConfirmEmail != null) try { pstmtUpdatePRConfirmEmail.close(); } catch (SQLException e) {}
	    	if (pstmtUpdatePRApproveEmail != null) try { pstmtUpdatePRApproveEmail.close(); } catch (SQLException e) {}
	    	if (rsPRConfirm != null) try { rsPRConfirm.close(); } catch (SQLException e) {}
	    	if (rsPRApprove != null) try { rsPRApprove.close(); } catch (SQLException e) {}
	    }
	}
	
	private void sendEmail(String headerTo, String centerBody, String status, String location, String emailTo, String emailCC, String emailBCC){
		if (centerBody.equals("")) return;
		String body = "<p>Dear  " + headerTo + ", </p>";
    	body += "<p>Berikut PR yang sudah di-" + status + " dari " + location + ": ";
    	body += "<ul>";
    	body += centerBody;
    	body += "</ul></p></br>";
    	body += "<p>Regards, </p><br><p>IT Dept.</p>";
    	
    	if (!emailTo.equals("")) receipentTo = emailTo;
    	if (!emailCC.equals("")) receipentCC = emailCC;
    	if (!emailBCC.equals("")) receipentBCC = emailBCC;
    	
    	this.sendMessage(body);
	}
}