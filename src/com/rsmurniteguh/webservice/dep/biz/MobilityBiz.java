package com.rsmurniteguh.webservice.dep.biz;

import com.rsmurniteguh.webservice.dep.all.model.AntrianDispance;
import com.rsmurniteguh.webservice.dep.all.model.CurrentDatetim;
import com.rsmurniteguh.webservice.dep.all.model.Dispensedetailinfo;
import com.rsmurniteguh.webservice.dep.all.model.InfoCekRem;
import com.rsmurniteguh.webservice.dep.all.model.InfoRekonObat;
import com.rsmurniteguh.webservice.dep.all.model.InfoRekonObatForCat;
import com.rsmurniteguh.webservice.dep.all.model.Infostockreceipt;
import com.rsmurniteguh.webservice.dep.all.model.ListDispanceCount;
import com.rsmurniteguh.webservice.dep.all.model.ListGetHasBPJS;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoBalance;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoCek;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoKaryawan;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoTransaksi;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoViewTrx;
import com.rsmurniteguh.webservice.dep.all.model.ListgetPharPulvis;
import com.rsmurniteguh.webservice.dep.all.model.PrepareDatetime;
import com.rsmurniteguh.webservice.dep.all.model.StoreDispance;
import com.rsmurniteguh.webservice.dep.all.model.StoreDispance2;
import com.rsmurniteguh.webservice.dep.all.model.mobile.transaction_emoney;
import com.rsmurniteguh.webservice.dep.kthis.services.AuthMobileFunctions;
import com.rsmurniteguh.webservice.dep.kthis.services.StoreDispanceSer;
import java.util.List;

public class MobilityBiz {

	public List<StoreDispance> getdispancebatch(String dispancebano) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getDispanceBatch(dispancebano);
	}

	public List<StoreDispance> getdispancebatch2(String dispancebano2, String patienttype, String patientclass) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getDispanceBatch2(dispancebano2, patienttype, patientclass);
	}

	public List<StoreDispance> getdrugstore(String drugreturnno, String patienttype, String patientclass) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getdrugstore(drugreturnno, patienttype, patientclass);
	}

	public List<StoreDispance2> getdispensebatchno(String dispensebatchno) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getdispensebatchno(dispensebatchno);
	}

	public List<AntrianDispance> getstoremstrid(String storemstrid) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getstoremstrid(storemstrid);
	}

	public List<Dispensedetailinfo> getdrugdispensedetailinfo(String dispensebatchno) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getdrugdispensedetailinfo(dispensebatchno);
	}

	public List<CurrentDatetim> getCurrentDatetime() {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getCurrentDatetime();
	}

	public List<PrepareDatetime> getprepareddatetime(String firstdate, String lastdate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getprepareddatetime(firstdate,lastdate);
	}

	public Object getTxnDateTime(String first, String last, String codedesc, String codedesc2) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getTxnDateTime(first,last, codedesc, codedesc2);
	}
	
	public Object getEnterDateTime(String first, String last, String codedesc, String codedesc2) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getEnterDateTime(first,last, codedesc, codedesc2);
	}

	public List<InfoRekonObat> getrekonobat(String visitid) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getrekonobat(visitid);
	}

	public List<InfoCekRem> getcekrem(String fromdate, String todate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getcekrem(fromdate, todate);
	}

	public List<Infostockreceipt> getstockreceipt(String fromdate, String todate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getstockreceipt(fromdate, todate);
	}

	public List<ListDispanceCount> getDispanceCount(String fromdate, String todate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getDispanceCount(fromdate, todate);
	}

	public List<ListgetPharPulvis> getPharPulvis(String fromdate, String todate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getPharPulvis(fromdate,todate);
	}

	public List<InfoRekonObatForCat> getrekonobatforcat(String visitid) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getrekonobatforcat(visitid);
	}
//	public List<ListgetDataSms> getDataSms(String nohp, String pesan, String keterangan, String status) {
//		// TODO Auto-generated method stub
//		return StoreDispanceSer.getDataSms(nohp,pesan,keterangan,status);
//	}

	public List<ListInfoViewTrx> getInfoViewTrx(String fromdate, String todate, String idnik) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getInfoViewTrx(fromdate,todate, idnik);
	}

	public List<ListInfoCek> getInfoCekSts(String cardno) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getInfoCekSts(cardno);
	}
	
	public List<ListInfoKaryawan>getInfoKaryawan(String idnik)
	{
		return StoreDispanceSer.getInfoKaryawan(idnik);
	}

	public List<ListGetHasBPJS> getHasBPJS(String patientid) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getHasBPJS(patientid);
	}

	public List<ListInfoViewTrx> getAllViewTrx(String fromdate, String todate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getAllViewTrx(fromdate,todate);
	}

	public  List<ListInfoKaryawan> getAllKaryawan() {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getAllKaryawan();
	}

	public List<ListInfoBalance> getCekBalanceNik(String nik) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getCekBalanceNik(nik);
	}

	public List<ListInfoTransaksi> getCekTransaksiNik(String accountBlcId) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getCekTransaksiNik(accountBlcId);
	}
	
	

}
