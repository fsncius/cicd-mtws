package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.PatientTransactionStatus;
import com.rsmurniteguh.webservice.dep.kthis.services.PatientTransactionService;

public class PatientTransactionServiceBiz  {
	
	public List<PatientTransactionStatus> addPatientTransaction(String visitId, String journalMonth, String journalYear, String postedBy)
	{
		return PatientTransactionService.addPatientTransaction(visitId, journalMonth, journalYear, postedBy);
	}
	
	public List<PatientTransactionStatus> addPatientBill(String billId)
	{
		return PatientTransactionService.addPatientBill(billId);
	}
	
	public List<PatientTransactionStatus> addPatientCancelBill(String billId)
	{
		return PatientTransactionService.addPatientCancelBill(billId);
	}
//	
//	public List<PurchaseStatus> AddConsignment(String purchaseNo) {
//		return PurchaseService.AddConsignment(purchaseNo);
//	}
}
