package com.rsmurniteguh.webservice.dep.biz;

import java.io.IOException;
import java.math.BigDecimal;

import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.kthis.services.InacbgServices;

public class InacbgBiz {
	public String cetakKlaim(String noSep) {
		// TODO Auto-generated method stub
		return InacbgServices.cetakKlaim(noSep);
	}
	
	
	public String tambahKlaim(String noKa, String noSep, String mrn, String nama, String tglLahir, String gender) {
		// TODO Auto-generated method stub
		return InacbgServices.tambahKlaim(noKa,noSep,mrn,nama,tglLahir,gender);
	}
	
	public String updateKlaim(String noSep, String noKa, String tglMasuk, String tglPulang, String jnsRawat,String klsRawat,
			String icuInd, String icuLos, String ventHour, String upInd, String upCls, String upLos, String addPay,
			String brWgt, String disSts, String diag, String pro, String trfPnb, String trfBdh, String trfKon,
			String trfTa, String trfKep, String trfPnj, String trfRad, String trfLab, String trfPd, String trfReh,
			String trfKmr, String trfRi, String trfOb, String trfAl, String trfBm, String trfSa, String naDok) {
		// TODO Auto-generated method stub
		return InacbgServices.updateKlaim(noSep,noKa,tglMasuk,tglPulang,jnsRawat,klsRawat,icuInd,icuLos,ventHour,upInd,upCls,
				upLos,addPay,brWgt,disSts,diag,pro,trfPnb,trfBdh,trfKon,trfTa,trfKep,trfPnj,trfRad,trfLab,trfPd,trfReh,
				trfKmr, trfRi, trfOb, trfAl, trfBm, trfSa, naDok);
	}
	
	public String groupingStage1(String noSep) {
		// TODO Auto-generated method stub
		return InacbgServices.groupingStage1(noSep);
	}
	
	public String groupingStage2(String noSep, String cmg) {
		// TODO Auto-generated method stub
		return InacbgServices.groupingStage2(noSep,cmg);
	}
	
	public String finalKlaim(String noSep) {
		// TODO Auto-generated method stub
		return InacbgServices.finalKlaim(noSep);
	}
	
	public String updatePasien(String mrn,String bpjs,String nama,String tgllahir,String gender) {
		// TODO Auto-generated method stub
		return InacbgServices.updatePasien(mrn,bpjs,nama,tgllahir,gender);
	}
	
	public String hapusPasien(String mrn) {
		// TODO Auto-generated method stub
		return InacbgServices.hapusPasien(mrn);
	}
	
	public String editUlangKlaim(String sep) {
		// TODO Auto-generated method stub
		return InacbgServices.editUlangKlaim(sep);
	}

	public String sendKlaim(String startdate, String enddate, String jenis, String tipe) {
		// TODO Auto-generated method stub
		return InacbgServices.sendKlaim(startdate,enddate,jenis,tipe);
	}

	public String sendKlaimIndividual(String sep) {
		// TODO Auto-generated method stub
		return InacbgServices.sendKlaimIndividual(sep);
	}

	public String getKlaim(String startdate, String enddate, String jenis) {
		// TODO Auto-generated method stub
		return InacbgServices.getKlaim(startdate,enddate,jenis);
	}
	
	public String getDetailKlaimbySep(String sep) {
		// TODO Auto-generated method stub
		return InacbgServices.getDetailKlaimbySep(sep);
	}


	public String getStatusKlaimbySep(String sep) {
		// TODO Auto-generated method stub
		return InacbgServices.getStatusKlaimbySep(sep);
	}


	public String deleteKlaimbySep(String sep) {
		// TODO Auto-generated method stub
		return InacbgServices.deleteKlaimbySep(sep);
	}
	
	public String searchDiagnosa(String keyword)  {
		// TODO Auto-generated method stub
		return InacbgServices.searchDiagnosa(keyword);
	}
	
	public String searchProcedure(String keyword)  {
		// TODO Auto-generated method stub
		return InacbgServices.searchProcedure(keyword);
	}
	
	public String integrasiKthis(BigDecimal visitId)  {
		// TODO Auto-generated method stub
		return InacbgServices.integrasiKthis(visitId);
	}
}
