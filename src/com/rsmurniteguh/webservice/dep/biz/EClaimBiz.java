package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.MedicalResumeSignature;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.BillReport;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.LabReport;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.PoctReport;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.RadiologyReport;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.ResumeMedis;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.SepReport;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.SepReportDetail;
import com.rsmurniteguh.webservice.dep.kthis.services.EClaimService;
import com.rsmurniteguh.webservice.dep.kthis.services.dataBpjsSer;

public class EClaimBiz {
	
	public List<ResumeMedis> getReportSep(String EClaimBPJS, String sepNo)
	{
		return EClaimService.getReportSep(EClaimBPJS, sepNo);
	}
	
	public List<ResumeMedis> getResumeMedis(String EClaimBPJS, String sepNo)
	{
		return EClaimService.getResumeMedis(EClaimBPJS, sepNo);
	}
	
	public MedicalResumeSignature GetSignatureReport(String EClaimBPJS, String sepNo) {
		return EClaimService.GetSignatureReport(EClaimBPJS, sepNo);
	}
	
	
	
	
	public List<LabReport> getLabReport(String EClaimBPJS, String sepNo)
	{
		return EClaimService.getLabReport(EClaimBPJS, sepNo);
	}
	
	public List<String> getPendingLabReport(String fromdate, String todate)
	{
		return EClaimService.getPendingLabReport(fromdate, todate);
	}
	
	public List<PoctReport> getPoctReport(String EClaimBPJS, String sepNo)
	{
		return EClaimService.getPoctReport(EClaimBPJS, sepNo);
	}
	
	public List<RadiologyReport> getRadiologyReport(String EClaimBPJS, String sepNo)
	{
		return EClaimService.getRadiologyReport(EClaimBPJS, sepNo);
	}
	
	public BaseResult getCheckSep(String EClaimBPJS, String sepNo)
	{
		return EClaimService.getCheckSep(EClaimBPJS, sepNo);
	}
	
	public List<SepReportDetail> getNoBa(String EClaimBPJS, String noBa)
	{
		return EClaimService.getNoBa(EClaimBPJS, noBa);
	}
	
	public List<ResumeMedis> getReportDll(String EClaimBPJS, String sepNo)
	{
		return EClaimService.getReportDll(EClaimBPJS, sepNo);
	}
	
	public List<SepReport> getSepReport(String EClaimBPJS, String fromDate, String toDate, String tipe, String noBa)
	{
		return EClaimService.getSepReport(EClaimBPJS, fromDate, toDate, tipe, noBa);
	}
	
	public List<SepReport> getSepReport(String EClaimBPJS, String fromDate, String toDate, String tipe)
	{
		return EClaimService.getSepReport(EClaimBPJS, fromDate, toDate, tipe, null);
	}
	
	public List<SepReport> getSepReport(String EClaimBPJS, String noBa)
	{
		return EClaimService.getSepReport(EClaimBPJS, null, null, null, noBa);
	}
	
	public List<BillReport> getBilling(String EClaimBPJS, String sepNo)
	{
		return EClaimService.getBilling(EClaimBPJS, sepNo);
	}
	
	public BaseResult getCheckTxnStatus(String EClaimBPJS, String sepNo)
	{
		return EClaimService.getCheckTxnStatus(EClaimBPJS, sepNo);
	}
}

