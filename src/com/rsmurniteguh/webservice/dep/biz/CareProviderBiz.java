package com.rsmurniteguh.webservice.dep.biz;

import java.math.BigDecimal;
import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.CareProvider;
import com.rsmurniteguh.webservice.dep.all.model.ListDoctorUser;
import com.rsmurniteguh.webservice.dep.all.model.ListQueNum;
import com.rsmurniteguh.webservice.dep.all.model.ListSpecialQueNum;
import com.rsmurniteguh.webservice.dep.kthis.services.CareProviderSer;

public class CareProviderBiz {

	public List<CareProvider> getCareProvider(String caretype) {
		return CareProviderSer.getCareProvider(caretype);
	}

	public List<ListDoctorUser> getdoctoruser() {
		return CareProviderSer.getdoctoruser();
	}

	public Object getcekdispas(String visitid) {
		return CareProviderSer.getcekdispas(visitid);
	}

	public List<ListQueNum> getquenum(String queueno) {
		return CareProviderSer.getquenum(queueno);
	}	
	
	public List<ListSpecialQueNum> getspecialquenum(String queueno) {
		return CareProviderSer.getspecialquenum(queueno);
	}
	
	public List<ListSpecialQueNum> getspecialquenum2(String queueno, String subspeciality) {
		return CareProviderSer.getspecialquenum2(queueno,subspeciality);
	}
	
	public List<ListQueNum> getDoctorQueueList(String careproviderId, String subspecialtymstr_id) {
		return CareProviderSer.getDoctorQueueList(careproviderId, subspecialtymstr_id);
	}

	public String getCareprovidersByUsermstr(String users) {
		return CareProviderSer.getCareprovidersByUsermstr(users);
	}

	public List<ListQueNum> getDoctorQueueListByUsermstrId(String careproviderId, String subspecialtymstr_id) {
		return CareProviderSer.getDoctorQueueListByUsermstrId(careproviderId, subspecialtymstr_id);
	}
	
	public BaseResult getIsDoctorByUserMstrId(String userMstrId) {
		return CareProviderSer.getIsDoctorByUserMstrId(userMstrId);
	}
	
	public BaseResult syncSIPDoctor(String careProviderId) {
		return CareProviderSer.syncSIPDoctor(careProviderId);
	}
	
	public BaseResult syncSIPDoctorAll() {
		return CareProviderSer.syncSIPDoctorAll();
	}
}
