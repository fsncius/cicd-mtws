package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import javax.ws.rs.core.Response.ResponseBuilder;

import com.rsmurniteguh.webservice.dep.all.model.NamaLabUser;
import com.rsmurniteguh.webservice.dep.all.model.NamaOrder;
import com.rsmurniteguh.webservice.dep.all.model.NamaRadiologi;
import com.rsmurniteguh.webservice.dep.kthis.services.LabUserSer;

public class labUserBiz {

	public List<NamaLabUser> getlabuser(String submstrid) {
		// TODO Auto-generated method stub
		return LabUserSer.getlabuser(submstrid);
	}

	public List<NamaOrder> getorder() {
		// TODO Auto-generated method stub
		return LabUserSer.getorder();
	}

	public List<NamaRadiologi> getradiologi() {
		// TODO Auto-generated method stub
		return LabUserSer.getradiologi();
	}

}
