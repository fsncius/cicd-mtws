package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.kthis.model.Storemstr;
import com.rsmurniteguh.webservice.dep.kthis.services.StoreService;

public class StorageBiz {
	
	public List<Storemstr> getStoreByLocation(String location)
	{
		return StoreService.getStoreByLocation(location);
	}

}
