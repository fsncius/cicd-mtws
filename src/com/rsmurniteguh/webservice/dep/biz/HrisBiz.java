package com.rsmurniteguh.webservice.dep.biz;

import com.rsmurniteguh.webservice.dep.all.model.hris.CreateCuti;
import com.rsmurniteguh.webservice.dep.all.model.hris.EmployeAttendance;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.hris.RequestCreateIzin;
import com.rsmurniteguh.webservice.dep.kthis.services.HrisService;

public class HrisBiz {
	public Object get_employe_detail(String id_karyawan) {
		return HrisService.get_employe_detail(id_karyawan);
	}

	public List<EmployeAttendance> get_employe_attendance(String finger_id, String kodemesin, String startdate, String enddate) {
		return HrisService.get_employe_attendance(finger_id,kodemesin,startdate,enddate);
	}

	public Object get_izin_data(String id_karyawan, String startdate, String enddate) {
		return HrisService.get_izin_data(id_karyawan,startdate,enddate);
	}

	public Object get_cuti_data(String id_karyawan, String startdate, String enddate) {
		return HrisService.get_cuti_data(id_karyawan,startdate,enddate);
	}

	public Object get_data_request_task_cuti(String id_karyawan) {
		return HrisService.get_data_request_task_cuti(id_karyawan);
	}
		
	public ResponseStatus create_izin_hris(RequestCreateIzin model) {
		return HrisService.create_izin_hris(model);
	}
	
	public ResponseStatus update_izin_hris(RequestCreateIzin model) {
		return HrisService.update_izin_hris(model);
	}
	
	public ResponseStatus delete_izin_hris(RequestCreateIzin model) {
		return HrisService.delete_izin_hris(model);
	}

	public Object view_create_cuti(String id_karyawan) {
		return HrisService.view_create_cuti(id_karyawan);
	}

	public Object create_cuti(CreateCuti createCuti) {
		return HrisService.create_cuti(createCuti);
	}

	public Object update_cuti(CreateCuti createCuti) {
		return HrisService.update_cuti(createCuti);
	}

	public Object delete_cuti(String kodecuti) {
		return HrisService.delete_cuti(kodecuti);
	}

	public Object get_cuti_info(String kodecuti) {
		return HrisService.get_cuti_info(kodecuti);
	}
	
	public Object get_atasan(String id_karyawan) {
		return HrisService.get_atasan(id_karyawan);
	}

	public Object get_bawahan(String kode_jabatan) {
		return HrisService.get_bawahan(kode_jabatan);
	}

	public Object konfirmasi_izin_cuti(String id, String alasan, String status, String tipe, String idkaryawan,
			String kodepermohonan) {
		return HrisService.konfirmasi_izin_cuti(id, alasan, status, tipe, idkaryawan, kodepermohonan);
	}

	public Object get_request_approval(String id_karyawan, String status) {
		return HrisService.get_request_approval(id_karyawan,status);
	}

	public Object get_berita(String reader_role) {
		return HrisService.get_berita(reader_role);
	}
	
	public Object get_login_hris(String username, String password) {
		return HrisService.get_login_hris(username,password);
	}

	public Object GetEmoney(String id_karyawan) {
		return HrisService.GetEmoney(id_karyawan);
	}

	public Object HitungHariCuti(String id_karyawan, String startdate, String enddate) {
		return HrisService.HitungHariCuti(id_karyawan,startdate,enddate);
	}

	public Object add_kotak_saran(String user_id, String user_name, String project_code, String version, String saran, String kontak) {
		return HrisService.add_kotak_saran(user_id,user_name,project_code,version, saran, kontak);
	}
	
	public Object authPass() {
		return HrisService.authPass();
	}
}
