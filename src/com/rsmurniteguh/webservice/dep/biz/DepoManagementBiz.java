package com.rsmurniteguh.webservice.dep.biz;


import com.rsmurniteguh.webservice.dep.all.model.PaymentModel;
import com.rsmurniteguh.webservice.dep.all.model.ReturnResult;
import com.rsmurniteguh.webservice.dep.all.model.UpdateCard;
import com.rsmurniteguh.webservice.dep.kthis.services.DepoManagementService;

public class DepoManagementBiz {

	public ReturnResult DoPayment(PaymentModel model) {
		return DepoManagementService.postPayment(model);
	}
	
	public ReturnResult DoPaymentModified(PaymentModel model) {
		return DepoManagementService.postPaymentModified(model);
	}
	
	public ReturnResult RegisterAccount(PaymentModel model) {
		return DepoManagementService.RegisterAccount(model);
	}
	
	public Object UpdateAccount(PaymentModel model) {
		return DepoManagementService.UpdateAccount(model);
	}
	
	public ReturnResult GenerateInvoice() {
		return DepoManagementService.getGeneratedDepositInvoice();
	}
	
	public ReturnResult GenerateInvoiceCan()
	{
		return DepoManagementService.getGeneratedDepositInvoiceCan();
	}
	
	public ReturnResult GetBalance(String cardNo) {
		return DepoManagementService.GetBalance(cardNo);
	}
	
	public ReturnResult PrepareGetCardDataFromMachine(String machineNo) {
		return DepoManagementService.PrepareGetCardDataFromMachine(machineNo);
	}
	
	public ReturnResult GetCardDataByMachine(String machineNo) {
		return DepoManagementService.GetCardDataByMachine(machineNo);
	}

	public Object GetValidCardDataByMachine(String machineNo) {
		return DepoManagementService.GetValidCardDataByMachine(machineNo);
	}

	public Object ChangeCardNumber(UpdateCard data) {
		return DepoManagementService.ChangeCardNumber(data);
	}
	
	public Object GetCardDetailInfo(String cardNo, String cardType){
		return DepoManagementService.GetCardDetailInfo(cardNo, cardType);
	}

}
