package com.rsmurniteguh.webservice.dep.biz;


import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.ResponseString;
import com.rsmurniteguh.webservice.dep.kthis.services.AuthMobileFunctions;
import com.rsmurniteguh.webservice.dep.kthis.services.UserService;
import com.rsmurniteguh.webservice.dep.kthis.view.UserLoginView;
import com.rsmurniteguh.webservice.dep.util.EncryptUtil;

public class UserBiz {

	public UserLoginView checklogin(String user, String pass)
	{
		return UserService.getLoginDetails(user, EncryptUtil.getInstance().nonReversibleEncode(pass)); 
	}
	
	public ResponseStatus change_password_kthis(String usermstrId, String password, String new_password)
	{
		return UserService.getChangePasswordKthis(usermstrId, password, new_password); 
	}
	
	public ResponseString change_password_patient(String usermstrId, String password, String new_password)
	{
		return UserService.getChangePasswordPatient(usermstrId, password, new_password); 
	}
	
	public ResponseStatus check_login_hris(String username, String password)
	{
		return AuthMobileFunctions.getCheckLoginHris(username, password); 
	}
	
	public Object change_password_hris(String idkaryawan, String password, String new_password) {
		return UserService.getChangePasswordHris(idkaryawan, password, new_password);
	}
}
