package com.rsmurniteguh.webservice.dep.biz;

import java.math.BigDecimal;
import java.sql.Blob;
import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.BaseInfo;
import com.rsmurniteguh.webservice.dep.all.model.BpjsQueue;
import com.rsmurniteguh.webservice.dep.all.model.DetailDokter;
import com.rsmurniteguh.webservice.dep.all.model.DoctorSchedule;
import com.rsmurniteguh.webservice.dep.all.model.IntegratedKthis;
import com.rsmurniteguh.webservice.dep.all.model.KamarPasien;
import com.rsmurniteguh.webservice.dep.all.model.KelasPasien;
import com.rsmurniteguh.webservice.dep.all.model.LicstCekValid;
import com.rsmurniteguh.webservice.dep.all.model.ListCanReg;
import com.rsmurniteguh.webservice.dep.all.model.ListCekTxn;
import com.rsmurniteguh.webservice.dep.all.model.ListCodeCar;
import com.rsmurniteguh.webservice.dep.all.model.ListDietPasien;
import com.rsmurniteguh.webservice.dep.all.model.ListGetQueue;
import com.rsmurniteguh.webservice.dep.all.model.ListInsertQueue;
import com.rsmurniteguh.webservice.dep.all.model.ListKamarHiv;
import com.rsmurniteguh.webservice.dep.all.model.ListPatientId;
import com.rsmurniteguh.webservice.dep.all.model.ListPoliCode;
import com.rsmurniteguh.webservice.dep.all.model.ListQueueBpjs;
import com.rsmurniteguh.webservice.dep.all.model.ListQueueUser;
import com.rsmurniteguh.webservice.dep.all.model.ListReceipt;
import com.rsmurniteguh.webservice.dep.all.model.ListPasien;
import com.rsmurniteguh.webservice.dep.all.model.ListResDokter;
import com.rsmurniteguh.webservice.dep.all.model.ListRuangan;
import com.rsmurniteguh.webservice.dep.all.model.ListResourceMstr;
import com.rsmurniteguh.webservice.dep.all.model.ListResourceMstr2;
import com.rsmurniteguh.webservice.dep.all.model.ListResourceMstrPoly;
import com.rsmurniteguh.webservice.dep.all.model.ListResourceMstrPolyName;
import com.rsmurniteguh.webservice.dep.all.model.ListResponseTime;
import com.rsmurniteguh.webservice.dep.all.model.ListSelfReg;
import com.rsmurniteguh.webservice.dep.all.model.ListTindakan;
import com.rsmurniteguh.webservice.dep.all.model.ListTotal;
import com.rsmurniteguh.webservice.dep.all.model.ListUserNameKT;
import com.rsmurniteguh.webservice.dep.all.model.ListValidAntrian;
import com.rsmurniteguh.webservice.dep.all.model.Listqueueno;
import com.rsmurniteguh.webservice.dep.all.model.Listtxn;
import com.rsmurniteguh.webservice.dep.all.model.MedicalResume;
import com.rsmurniteguh.webservice.dep.all.model.MedicalResumeSignature;
import com.rsmurniteguh.webservice.dep.all.model.ObatBpjs;
import com.rsmurniteguh.webservice.dep.all.model.RegData;
import com.rsmurniteguh.webservice.dep.all.model.ResourceList;
import com.rsmurniteguh.webservice.dep.all.model.RujukanBpjs;
import com.rsmurniteguh.webservice.dep.all.model.SelfRegistration;
import com.rsmurniteguh.webservice.dep.all.model.SignatureMedicalResume;
import com.rsmurniteguh.webservice.dep.all.model.StatusLab;
import com.rsmurniteguh.webservice.dep.all.model.UploadResume;
import com.rsmurniteguh.webservice.dep.all.model.VisitId;
import com.rsmurniteguh.webservice.dep.all.model.VisitSignatureMedicalResume;
import com.rsmurniteguh.webservice.dep.all.model.listCardNoBpjs;
import com.rsmurniteguh.webservice.dep.all.model.listHasBpjsNo;
import com.rsmurniteguh.webservice.dep.all.model.listpolyres;
import com.rsmurniteguh.webservice.dep.kthis.services.dataBpjsSer;
import com.rsmurniteguh.webservice.dep.all.model.ListOnlineReg;
public class dataBpjsBiz {

	public List<listHasBpjsNo> getDataBpjsNo(String nobpjs) {
		return dataBpjsSer.getDataBpjsNo(nobpjs);
	}

	public List<listCardNoBpjs> getCardNo(String cardno) {
		return dataBpjsSer.getCardNo(cardno);
	}
	public BaseInfo getBaseInfo(String mrn) {
		return dataBpjsSer.getBaseInfo(mrn);
	}
	public List<listpolyres> getPolyResourceMstrByUser(String usermasterid) {
		return dataBpjsSer.getPolyResourceMstrByUser(usermasterid);
	}

	public List<Listqueueno> getPatientByQueueNo(String queueno) {
		return dataBpjsSer.getPatientByQueueNo(queueno);
	}
	
	public List<Listqueueno> getPatientByQueueNoUpdate(String queueno,String subspeciality) {
		return dataBpjsSer.getPatientByQueueNoUpdate(queueno,subspeciality);
	}
	
	public List<ListResponseTime> getPharmacyResponseTime(String tglAwal, String tglAkhir) {
		return dataBpjsSer.getPharmacyResponseTime(tglAwal,tglAkhir);
	}

	public List<Listqueueno> getPatientByQueueNo2(String queueno2) {
		return dataBpjsSer.getPatientByQueueNo2(queueno2);
	}

	public List<Listqueueno> getPatientByQueueNo2Update(String queueno2,String subspeciality) {
		return dataBpjsSer.getPatientByQueueNo2Update(queueno2,subspeciality);
	}

	public List<ListSelfReg> getSelfRegistration(String cardno, String careproviderid, String noantri, String userid) {
		return dataBpjsSer.getSelfRegistration(cardno,careproviderid,noantri,userid);
	}
	
	public IntegratedKthis getInsBrid(String cardno,String bpjsno,String sepno,String vid,String careproviderid, String noantri,String userid) {
		return dataBpjsSer.getInsBrid(cardno,bpjsno,sepno,vid,careproviderid,noantri,userid);
	}
	
	public IntegratedKthis getInsSign(String mrid,String vid,String sign,String userid){
		return dataBpjsSer.getInsSign(mrid,vid,sign,userid);
	}
	
	public IntegratedKthis sendFileResume(UploadResume resumeform){
		return dataBpjsSer.sendFileResume(resumeform);
	}
	
	
	public List<ListSelfReg> getSelfRegistration2(String cardno, String careproviderid, String noantri, String userid) {
		return dataBpjsSer.getSelfRegistration2(cardno,careproviderid,noantri,userid);
	}

	public SelfRegistration registrationInfo (String regno)
	{
		return dataBpjsSer.registrationInfo(regno);
	}
	
	
	
	public BpjsQueue addBpjsQueue (String queue_no)
	{
		return dataBpjsSer.addBpjsQueue(queue_no);
	}
	
	
	public RegData GetRegData (String regno)
	{
		return dataBpjsSer.GetRegData(regno);
	}
	
	public List<ListCekTxn> getCekTxn(String visitId) {
		return dataBpjsSer.getCekTxn(visitId);
	}

	
	public List<ResourceList> SearchDoctor(String searchtext,String lokasi) {
		return dataBpjsSer.SearchDoctor(searchtext,lokasi);
	}

	public List<Listtxn> getlisttxn(String visitid) {
		return dataBpjsSer.getlisttxn(visitid);
	}

	public List<ListCanReg> getcancelreg(String userid, String visitid, String remarks,  String cancelreason) {
		return dataBpjsSer.getcancelreg(userid,visitid,remarks,cancelreason);
	}
	
	public ListInsertQueue getcancelQueue(String cardno, String tgl_berobat) {
		return dataBpjsSer.getcancelQueue(cardno,tgl_berobat);
	}

	public List<ListCodeCar> getcodecar() {
		return dataBpjsSer.getcodecar();
	}
	
	public List<ListResourceMstr2> getpolydoc(String idhari) {
		return dataBpjsSer.getpolydoc(idhari);
	}
	
	public List<ListResourceMstr2> getpolybpjsdoc(String idhari) {
		return dataBpjsSer.getpolybpjsdoc(idhari);
	}
	
	
	public List<ListResourceMstrPoly> getpolydoc3(String idhari) {
		return dataBpjsSer.getpolydoc3(idhari);
	}
	
	public List<ListResourceMstr2> getpolydoc2(String idhari,String poli) {
		return dataBpjsSer.getpolydoc2(idhari,poli);
	}
	
	public List<ListResourceMstrPolyName> getpolyname(String idhari) {
		return dataBpjsSer.getpolyname(idhari);
	}

	public List<LicstCekValid> getcekvalidpas(String bpjsno, String namapas, String tglahir) {
		return dataBpjsSer.getcekvalidpas(bpjsno,namapas,tglahir);
	}

	public List<ListValidAntrian> getcekvalidantrian(String cardNo, String idNo, String bpjsNo) {
		return dataBpjsSer.getcekvalidantrian(cardNo, idNo, bpjsNo);
	}

	public List<ListResourceMstr> getResourceMaster(String ResourceName) {
		return dataBpjsSer.getResourceMaster(ResourceName);
	}
	
	public List<ListResourceMstr> getResourceMaster2(String ResourceName) {
		return dataBpjsSer.getResourceMaster2(ResourceName);
	}
	
	public ListResourceMstr2 getResourceMaster3(String ResourceId) {
		return dataBpjsSer.getResourceMaster3(ResourceId);
	}
	
	
	public ListTotal gettotal(String idhari) {
		return dataBpjsSer.gettotal(idhari);
	}
	
	public List<ListResourceMstr> getResourceMasterjaddok() {
		return dataBpjsSer.getResourceMasterjaddok();
	}

	public List<ListResourceScheme> getResourceScheme(String resourceMstrId, String regnDate) {
		return dataBpjsSer.getResourceScheme(resourceMstrId,regnDate);
	}

	public List<ListPatientId> getPatientId(String cardNo) {
		return dataBpjsSer.getPatiendId(cardNo);
	}


	public List<ListResDokter> GetResDokter(String careProviderId) {
		return dataBpjsSer.GetResDokter(careProviderId);
	}
	
	public DetailDokter GetDetailDokter(String dokterId) {
		return dataBpjsSer.GetDetailDokter(dokterId);
	}
	
	public Object GetQueueBpjs(String counterId) {
		
		return dataBpjsSer.GetQueueBpjs(counterId);
	}

	public Object GetQueueUser(String userMstrId) {
		
		return dataBpjsSer.GetQueueUser(userMstrId);
	}

	public Object GetOnlineReg(String barcode) {
		
		return dataBpjsSer.GetOnlineReg(barcode);
	}

//	public Object GetOnlineReg2(String barcode) {
////		return dataBpjsSer.GetOnlineReg2(barcode);
//	}
	
	public ListDietPasien GetDietPasien(String Mrn) {
		return dataBpjsSer.GetDietPasien(Mrn);
	}
	
	public KelasPasien GetKelasPasien(String Pc) {
		return dataBpjsSer.GetKelasPasien(Pc);
	}
	
	public KamarPasien GetKamarPasien(String Vid) {
		return dataBpjsSer.GetKamarPasien(Vid);
	}

	public VisitId GetVisitId(String mrn) {
		return dataBpjsSer.GetVisitId(mrn);
	}
	
	public List<ListPoliCode> GetPoliCode() {
		return dataBpjsSer.GetPoliCode();
	}

	public List<ListUserNameKT> GetUserName(String UserCode) {
		return dataBpjsSer.GetUserName(UserCode);
	}
	
	public List<ListTindakan> GetTindakan(String VisitId) {
		return dataBpjsSer.GetTindakan(VisitId);
	}
	
	public List<DoctorSchedule> getDailyDoctorSchedule(String RegnDate) {
		return dataBpjsSer.getDailyDoctorSchedule(RegnDate);
	}

	public List<ListReceipt> GetReceipt(String sysReceptNo) {
		return dataBpjsSer.GetReceipt(sysReceptNo);
	}
	
	public List<ListKamarHiv> ListKamar() {
		return dataBpjsSer.ListKamar();
	}
	
	public List<ListRuangan> ListRuangan() {
		return dataBpjsSer.ListRuangan();
	}
	
	public List<ListPasien> ListPasien(String Ruangan,String Tipe) {
		return dataBpjsSer.ListPasien(Ruangan,Tipe);
	}
	
	public MedicalResumeSignature GetSignatureReport(String mrid) {
		return dataBpjsSer.GetSignatureReport(mrid);
	}
	
	public MedicalResume GetMedicalResume(String MedicalResumeId, String Ruangan,String Tipe) {
		return dataBpjsSer.GetMedicalResume(MedicalResumeId,Ruangan,Tipe);
	}
	
	public MedicalResume getMedicalResumeByMrnNo(String mrnNo) {
		return dataBpjsSer.getMedicalResumeByMrnNo(mrnNo);
	}
	
	public MedicalResume GetMedicalResumeById(String resumeId) {
		return dataBpjsSer.GetMedicalResumeById(resumeId);
	}
	
	
	public List<VisitSignatureMedicalResume> getListMedicalVisitByMRN(String mrn){
		return dataBpjsSer.getListMedicalVisitByMRN(mrn);
	}
	
	public List<SignatureMedicalResume> getListPatientMedicalResumeByUserMstrId(String userMstrId){
		return dataBpjsSer.getListPatientMedicalResumeByUserMstrId(userMstrId);
	}
	
	public RujukanBpjs GetDataKartu(String bpjsNo){
		return dataBpjsSer.GetDataKartu(bpjsNo);
	}
	
	public List<ObatBpjs> GetBpjsMedicine(String vid) {
		return dataBpjsSer.GetBpjsMedicine(vid);
	}
	
	
	public StatusLab GetLabReport(String mrn) {
		return dataBpjsSer.GetLabReport(mrn);
	}

	public Object GetPatientHiv(String startdate, String enddate) {
		return dataBpjsSer.GetPatientHiv(startdate,enddate);
	}

	public Object getDoctorScheduleDay(String idhari, String subspecialty_id) {
		return dataBpjsSer.getDoctorScheduleDay(idhari,subspecialty_id);
	}
	
	public Object checkResourceStop(BigDecimal resourceMstrId, String regDate) {
		return dataBpjsSer.checkStopResource(resourceMstrId, regDate);
	}

}
