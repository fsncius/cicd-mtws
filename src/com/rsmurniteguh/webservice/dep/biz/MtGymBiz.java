package com.rsmurniteguh.webservice.dep.biz;

import com.rsmurniteguh.webservice.dep.all.model.gym.AccountBalance;
import com.rsmurniteguh.webservice.dep.all.model.gym.AccountBalanceParam;
import com.rsmurniteguh.webservice.dep.all.model.gym.CreateMember;
import com.rsmurniteguh.webservice.dep.all.model.gym.ResponseString;
import com.rsmurniteguh.webservice.dep.kthis.services.AuthMobileFunctions;
import com.rsmurniteguh.webservice.dep.kthis.services.MtGymSer;

public class MtGymBiz {

	public ResponseString CheckLogin(String user, String pass) {
		return AuthMobileFunctions.CheckLogin(user, pass);
	}
	
	public ResponseString SaveRegistrationToken(String user, String token)
	{
		return AuthMobileFunctions.SaveRegistrationToken(user, token);
	}
	
	public Object info_account_from_cardno(String cardno) {
		// TODO Auto-generated method stub
		return MtGymSer.info_account_from_cardno(cardno);
	}	

	public Object create_member(CreateMember create_member) {
		return MtGymSer.create_member(create_member);
	}

	public Object topup_account(String total, String nfcno, String created_by) {
		return MtGymSer.topup_account(total,nfcno,created_by);
	}

	public Object info_member(String nfcno) {
		return MtGymSer.info_member(nfcno);
	}

	public Object update_account(AccountBalance account_balance) {
		return MtGymSer.update_account(account_balance);
	}

	public Object member() {
		return MtGymSer.member();
	}

	public Object cancel_topup_account(String invoice_no, String created_by) {
		return MtGymSer.cancel_topup_account(invoice_no,created_by);
	}

	public Object trx(String date, String type) {
		return MtGymSer.trx(date,type);
	}

	public Object get_rfid(String machineNo) {
		return MtGymSer.get_rfid(machineNo);
	}

	public Object absensi(String accountBalanceId, String memberShipId, String machineCode) {
		return MtGymSer.absensi(accountBalanceId, memberShipId, machineCode);
	}

	public Object info_account_from_old_card(String machineNo) {
		return MtGymSer.info_account_from_old_card(machineNo);
	}

	public Object create_account(AccountBalanceParam accountBalanceParam) {
		return MtGymSer.create_account(accountBalanceParam);
	}

}
