package com.rsmurniteguh.webservice.dep.biz;


import java.math.BigDecimal;

import com.rsmurniteguh.webservice.dep.kthis.services.BpjsService;

public class BpjsServiceBiz {

	public String getBpjsByCardNo(String cardNo) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberByIdService(cardNo);
	}
	
	public String getBpjsOnlineCardNo(String cardNo) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberOnlineIdService(cardNo);
	}
	
	public String getBpjsByNik(String Nik) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberByNik(Nik);
	}
	
	public String getBpjsSep(String cardNo) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberSep(cardNo);
	}
	
	public String getBpjsRujukanData(String noRujuk) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberDataRujukan(noRujuk);
	}
	
	public String getBpjsRujukanTanggal(String Tanggal) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberTanggalRujukan(Tanggal);
	}
	
	public String getBpjsRujukanNomorKartu(String cardNo) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberRujukanNomorKartu(cardNo);
	}
	
	public String getBpjsRujukanFktlNomorKartu(String cardNo) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberRujukanFktlNomorKartu(cardNo);
	}
	
	public String getBpjsRujukanFktlTanggal(String Tanggal) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberRujukanFktlTanggal(Tanggal);
	}
	
	public String getBpjsRujukanFktlNoRujuk(String noRujuk) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberRujukanFktlNoRujuk(noRujuk);
	}
	
	public String getBpjsDetailNoSep(String noSep) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsMemberDetailNoSep(noSep);
	}
	
	public String getBpjsDiagnosaKey(String Keyword) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsCbgDiagnosa(Keyword);
	}
	
	public String getBpjsProsedurCbgKey(String Keyword) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsCbgProsedur(Keyword);
	}
	
	public String getBpjsDataCmgKey(String Keyword) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsDetailCmg(Keyword);
	}
	
	public String getBpjsDataVerifikasi(String TglMasuk, String TglKeluar, String KlsRawat, String Kasus, String Cari , String Status) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsDetailVerifikasi(TglMasuk,TglKeluar,KlsRawat,Kasus,Cari,Status);
	}
	
	public String getBpjsLaporanNoSep(String noSep) {
		// TODO Auto-generated method stub
		return BpjsService.GetBpjsLaporanSepNo(noSep);
	}
	
	public String getBpjsDetailRujukanKartu(String noKartu)
	{
		return BpjsService.GetRujukanKartu(noKartu);	
		
	}
	
	public String getBpjsTarifCbg(String noSep)
	{
		return BpjsService.GetTarifCbg(noSep);	
		
	}
	
	public String getBpjsDetailRujukanRs(String noRujuk)
	{
		return BpjsService.GetRujukanRs(noRujuk);	
		
	}
	
	public String CreateLaporanSep(
			String noKartu,
			String tglSep,
			String tglRujukan,
			String noRujukan,
			String ppkRujukan,
			String ppkPelayanan,
			String jnsPelayanan,
			String catatan,
			String diagAwal,
			String poliTujuan,
			String klsRawat,
			String lakaLantas,
			String noMr) {
		// TODO Auto-generated method stub
		return BpjsService.CreateBpjsLaporanSep(noKartu, tglSep,tglRujukan,noRujukan,ppkRujukan,ppkPelayanan,jnsPelayanan,catatan,diagAwal,poliTujuan,klsRawat,lakaLantas,noMr);
	}
	
	public String CreateRoom(String kodeKelas,
			String kodeRuang,
			String namaRuang,
			String kapasitas,
			String tersedia){
		return BpjsService.CreateRoom(kodeKelas,kodeRuang,namaRuang,kapasitas,tersedia);
	}
	
	public String UpdateRoom(String kodeKelas,
			String kodeRuang,
			String namaRuang,
			String kapasitas,
			String tersedia){
		return BpjsService.UpdateRoom(kodeKelas,kodeRuang,namaRuang,kapasitas,tersedia);
	}
	
	public String UpdateTglPulangSep(
			String noSep,
			String tglPlg) {
		// TODO Auto-generated method stub
		return BpjsService.UpdateBpjsTglpulangSep(noSep,tglPlg);
	}
	
	public String UpdateSepBpjs(
			String noSep,
			String noKartu,
			String tglSep,
			String tglRujukan,
			String noRujukan,
			String ppkRujukan,
			String ppkPelayanan,
			String jnsPelayanan,
			String catatan,
			String diagAwal,
			String poliTujuan,
			String klsRawat,
			String noMr){
		return BpjsService.UpdateBpjsSep(noSep,noKartu,tglSep,tglRujukan,noRujukan,ppkRujukan,ppkPelayanan,jnsPelayanan,catatan,diagAwal,poliTujuan,klsRawat,noMr);
	}
	
	public String DeleteSep(String noSep) {
		// TODO Auto-generated method stub
		return BpjsService.DeleteBpjsSep(noSep);
	}
	
	public String KodeKamar(){
		return BpjsService.KodeKamar();
	}
	
	public String ListKamar(){
		return BpjsService.ListKamar();
	}
	
	public String MappingTransaksiSep(String noSep,String noTrans) {
		// TODO Auto-generated method stub
		return BpjsService.MappingBpjsSep(noSep,noTrans);
	}
	
	public String GrouperDataInacbg() {
		// TODO Auto-generated method stub
		return BpjsService.GrouperBpjsInacbg();
	}
	
	public String FinalisasiGrouperInacbg() {
		// TODO Auto-generated method stub
		return BpjsService.FinalisasiGrouperBpjsInacbg();
	}
	
	public String GetEligibilitasPeserta(String nokainhealth, String tglpelayanan, String jenispelayanan,
			String poli) {
		// TODO Auto-generated method stub
		return BpjsService.GetEligibilitasPeserta(nokainhealth,tglpelayanan,jenispelayanan,poli);
	}
	
	public String CekSJP(String nokainhealth, String tanggalsjp, String poli, String tkp) {
		// TODO Auto-generated method stub
		return BpjsService.CekSJP(nokainhealth,tanggalsjp,poli,tkp);
	}
	
	public String ConfirmAKTFirstPayor(String nosjp, String userid) {
		// TODO Auto-generated method stub
		return BpjsService.ConfirmAKTFirstPayor(nosjp,userid);
	}
	
	public String HapusDetailSJP(String nosjp,String notes, String userid) {
		// TODO Auto-generated method stub
		return BpjsService.HapusDetailSJP(nosjp,notes,userid);
	}
	
	public String HapusSJP(String nosjp,String alasanhapus, String userid) {
		// TODO Auto-generated method stub
		return BpjsService.HapusSJP(nosjp,alasanhapus,userid);
	}

	public String SimpanBiayaINACBGS(String nosjp, String kodeinacbg, BigDecimal biayainacbgs, String nosep,
			String notes, String userid) {
		// TODO Auto-generated method stub
		return BpjsService.SimpanBiayaINACBGS(nosjp,kodeinacbg,biayainacbgs,nosep,notes,userid);
	}


	public String HapusTindakan(String nosjp, String kodetindakan, String tgltindakan, String notes, String userid) {
		// TODO Auto-generated method stub
		return BpjsService.HapusTindakan(nosjp,kodetindakan,tgltindakan,notes,userid);
	}

	public String UpdateTanggalPulang(BigDecimal id, String nosjp, String tglmasuk, String tglkeluar) {
		// TODO Auto-generated method stub
		return BpjsService.UpdateTanggalPulang(id,nosjp,tglmasuk,tglkeluar);
	}

	public String InfoSJP(String nosjp) {
		// TODO Auto-generated method stub
		return BpjsService.InfoSJP(nosjp);
	}

	public String Poli(String keyword) {
		// TODO Auto-generated method stub
		return BpjsService.Poli(keyword);
	}

	public String ProviderRujukan(String keyword) {
		// TODO Auto-generated method stub
		return BpjsService.ProviderRujukan(keyword);
	}

	public String RekapHasilVerifikasi(String nofpk) {
		// TODO Auto-generated method stub
		return BpjsService.RekapHasilVerifikasi(nofpk);
	}

	public String SimpanRuangRawat(String nosjp, String tglmasuk, String kelasrawat, String kodejenispelayanan,
			BigDecimal byharirawat) {
		// TODO Auto-generated method stub
		return BpjsService.SimpanRuangRawat(nosjp,tglmasuk,kelasrawat,kodejenispelayanan,byharirawat);
	}

	public String SimpanSJP(String tanggalpelayanan, String jenispelayanan, String nokainhealth,
			String nomormedicalreport, String nomorasalrujukan, String kodeproviderasalrujukan,
			String tanggalasalrujukan, String kodediagnosautama, String poli, String username, String informasitambahan,
			String kodediagnosatambahan, BigDecimal kecelakaankerja, String kelasrawat, String kodejenpelruangrawat) {
		// TODO Auto-generated method stub
		return BpjsService.SimpanSJP(tanggalpelayanan,jenispelayanan,nokainhealth,nomormedicalreport,nomorasalrujukan,kodeproviderasalrujukan,tanggalasalrujukan,kodediagnosautama,poli,username,informasitambahan,kodediagnosatambahan,kecelakaankerja,kelasrawat,kodejenpelruangrawat);
	}

	public String SimpanTindakan(String jenispelayanan, String nosjp, String tglmasukrawat, String tanggalpelayanan,
			String kodetindakan, String poli, String kodedokter, String biayaaju) {
		// TODO Auto-generated method stub
		return BpjsService.SimpanTindakan(jenispelayanan,nosjp,tglmasukrawat,tanggalpelayanan,kodetindakan,poli,kodedokter,biayaaju);
	}

	public String SimpanTindakanRITL(String jenispelayanan, String nosjp, String idakomodasi, String tglmasukrawat,
			String tanggalpelayanan, String kodetindakan, String poli, String kodedokter, BigDecimal biayaaju) {
		// TODO Auto-generated method stub
		return BpjsService.SimpanTindakanRITL(jenispelayanan,nosjp,idakomodasi,tglmasukrawat,tanggalpelayanan,kodetindakan,poli,kodedokter,biayaaju);
	}
	
}
