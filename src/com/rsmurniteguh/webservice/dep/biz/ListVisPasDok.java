package com.rsmurniteguh.webservice.dep.biz;

import java.sql.Date;

public class ListVisPasDok {
	private String CardNo;
	private String PersonName;
	private String Sex;
	private Date BirthDate;
	private Long VisitId;
	private Date AdmisDate;
	private String PatientType;
	private String PersonNameDokter;
	private String PatientWard;
	
	
	public String getPatientWard() {
		return PatientWard;
	}
	public void setPatientWard(String patientWard) {
		PatientWard = patientWard;
	}
	public String getCardNo() {
		return CardNo;
	}
	public void setCardNo(String cardNo) {
		CardNo = cardNo;
	}
	public String getPersonName() {
		return PersonName;
	}
	public void setPersonName(String personName) {
		PersonName = personName;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public Date getBirthDate() {
		return BirthDate;
	}
	public void setBirthDate(Date birthDate) {
		BirthDate = birthDate;
	}
	public Long getVisitId() {
		return VisitId;
	}
	public void setVisitId(Long visitId) {
		VisitId = visitId;
	}
	public Date getAdmisDate() {
		return AdmisDate;
	}
	public void setAdmisDate(Date admisDate) {
		AdmisDate = admisDate;
	}
	public String getPatientType() {
		return PatientType;
	}
	public void setPatientType(String patientType) {
		PatientType = patientType;
	}
	public String getPersonNameDokter() {
		return PersonNameDokter;
	}
	public void setPersonNameDokter(String personNameDokter) {
		PersonNameDokter = personNameDokter;
	}
	
	
}
