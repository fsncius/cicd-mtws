package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.ListGetQueue;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.queue.QueueConfig;
import com.rsmurniteguh.webservice.dep.all.model.queue.RequestTicket;
import com.rsmurniteguh.webservice.dep.kthis.services.QueueService;
import com.rsmurniteguh.webservice.dep.util.EncryptUtil;
import com.rsmurniteguh.webservice.dep.all.model.queue.RequestTicket;

public class QueueServiceBiz {
	public static final String QUE_REG_IDLE = "SQUE0";
	public static final String QUE_REG_CALL = "SQUE1";
	public static final String QUE_REG_START = "SQUE2";
	public static final String QUE_REG_FINISH = "SQUE3";
	public static final String QUE_REG_SKIP = "SQUE9";

	public static final String STATUS_TIME_CSTOCALL = "STM1";
	public static final String STATUS_TIME_STARTREGTOFINISH = "STM2";
	
	public static final String VOICE_ON = "1";
	public static final String VOICE_OFF = "0";
	
	public static final String REGISTRASI_BPJS = "RBS1";
	public static final String POLIKLINIK_BPJS = "PBS1";
	public static final	String KASIR_BPJS = "KBS1";
	public static final String REGISTRASI_GENERAL = "RGL1";
	public static final	String POLIKLINIK_GENERAL = "PGL1";
	public static final String KASIR_GENERAL = "KGL1";

	
	public static final String COUNTER_ACTIVE = "CTR1";
	public static final String COUNTER_NONACTIVE = "CTR0";
	
	public static final String LOCATION_REGISTRASI_ANTRIAN_SELF_REGISTRASI = "QUE3";
	public static final String LOCATION_REGISTRASI_ANTRIAN_ONLINE_WEB = "QUE1";
	public static final String LOCATION_REGISTRASI_ANTRIAN_ONLINE_MOBILE = "QUE2";
	
	public static final String SOURCE_CAT = "QUE";
	public static final String SOURCE_DEFAULT = "00";
	public static final String SOURCE_WEB = "01";
	public static final String SOURCE_MOBILE = "02";
	public static final String SOURCE_DESKTOP = "03";
	
	public static final String QUEUE_TYPE_REGULER = "TRR1";
	public static final Object LOCATION_GROUP_CASHIER = "KSRGRP";
	
	public Boolean postDoctorWorkstationCall(String queueno, String subspeciality, String usermstr){
		return QueueService.postDoctorWorkstationCall(queueno, subspeciality, usermstr);
	}
	
	public void postPharmacyComplete(String queueno, String subspeciality, String usermstr) {
	}
	
	public List<ListGetQueue> InsertQueue(String careProviderId, String patient_name, String card_no, String tgl_berobat, String PatientId, String location, String group_id) {
		return QueueService.InsertQueue(careProviderId, patient_name, card_no,tgl_berobat,PatientId, location, group_id,null);
	}
	
	public Object RequestIDTicket(String resourcemstr, String location, String tanggal, String source, String nama_patient, String no_mrn, String queue_type) {
		return QueueService.RequestIDTicket(resourcemstr, location, tanggal, source, nama_patient, no_mrn, queue_type);
	}

	public ResponseStatus RequestTicket(String location, String no_subspecialis, String no_doctor,String queue_id, String tanggal){
		return QueueService.RequestTicket(location, no_subspecialis, no_doctor, queue_id, tanggal);
	}
	
	public ResponseStatus ChangeQueueStatus(String queue_id, String userMstrID, String status, String counterId, String statusQueueTime, String queue_no){
		return QueueService.ChangeQueueStatus(queue_id, userMstrID, status, counterId, statusQueueTime, queue_no);
	}
	
	public Object ListviewReg(String counters) {
		return QueueService.ListviewReg(counters);
	}
	public Object ListQueue(String lokasi, String counter_id, String tanggal, String regType) {
		return QueueService.ListQueue(lokasi,counter_id,tanggal,regType);
	}
	
	public Object ListQueue(String lokasi, String counter_id, String tanggal, String regType, String status) {
		return QueueService.ListQueue(lokasi,counter_id,tanggal,regType, status);
	}
	public Object SoundQueue(String counters) {
		return QueueService.SoundQueue(counters);
	}
	public Object delQueueCall(String queue_id) {
		return QueueService.delQueueCall(queue_id);
	}

	public Object getConfigData() {
		return QueueService.getConfigData();
	}
	public Object getActiveCounter(String counters) {
		return QueueService.getActiveCounter(counters);
	}
	
	public Object CallSpecialQueueByNumber(String location, String counterid, String queueNo, String userMstrId) {
		return QueueService.CallSpecialQueueByNumber(location, counterid, queueNo, userMstrId);
	}
	public Object SkipSpecialQueueByNumber(String location, String counterid, String queueNo, String userMstrId) {
		return QueueService.SkipSpecialQueueByNumber(location, counterid, queueNo, userMstrId);
	}
	public Object FinishSpecialQueueByNumber(String location, String counterid, String queueNo, String userMstrId) {
		return QueueService.FinishSpecialQueueByNumber(location, counterid, queueNo, userMstrId);
	}
	public Object ListViewPolyclinicBPJS(String location, String counters) {
		return QueueService.ListViewPolyclinicBPJS(location, counters);
	}
	public Object LoginCaller(String username, String password) {
		return QueueService.LoginCaller(username, EncryptUtil.getInstance().nonReversibleEncode(password));
	}
	public Object ChangeCountertoAktif(String counterid, String usermstrid, String resourcemstrId) {
		return QueueService.ChangeCountertoAktif(counterid, usermstrid,resourcemstrId);
	}
	public Object ChangeCountertoNonAktif(String counterid) {
		return QueueService.ChangeCountertoNonAktif(counterid);
	}

	public Object QueueDetails(String queue_no, String location, String tanggal) {
		return QueueService.QueueDetails(queue_no, location, tanggal);
	}
	
	public Object ListQueueKasir(String location) {
		return QueueService.ListQueueKasir(location);
	}
	
	public Object InsertQueueKasir(String queue_no,String location, String source,String tanggal, String queue_type) {
		return QueueService.InsertQueueKasir(queue_no,location,source,tanggal,queue_type);
	}

	public Object QueueCallKasir(String location, String counter_id) {
		return QueueService.QueueCallKasir(location,counter_id);
	}
	
	public Object QueueNoByMrn(String noMrn) {
		return QueueService.QueueNoByMrn(noMrn);
	}

	public Object getConfigDataByAddress(String mac_address) {
		return QueueService.getConfigDataByAddress(mac_address);
	}

	public Object setConfigDataByAddress(QueueConfig queueConfig) {
		return QueueService.setConfigDataByAddress(queueConfig);
	}

	public Object retConfigDataByAddress(QueueConfig queueConfig) {
		return QueueService.retConfigDataByAddress(queueConfig);
	}
	
	public Object QueueCallSkipKasir(String queue_no,String lokasi) {
		return QueueService.QueueCallSkipKasir(queue_no,lokasi);
	}

	public Object getServingTime(Long queueId) {
		return QueueService.getServingTime(queueId);
	}


}
