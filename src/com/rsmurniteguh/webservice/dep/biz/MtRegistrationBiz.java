package com.rsmurniteguh.webservice.dep.biz;

import com.rsmurniteguh.webservice.dep.all.model.BaseInfo;
import com.rsmurniteguh.webservice.dep.all.model.IntegratedKthis;
import com.rsmurniteguh.webservice.dep.all.model.PDFUpload;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.CancelReg;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.DetailSep;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegBpjs;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegGeneral;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Metadata;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.RegKthis;
import com.rsmurniteguh.webservice.dep.kthis.services.MtRegistrationService;;

public class MtRegistrationBiz {
	public BaseInfo getBaseInfo(String baseno)
	{
		return MtRegistrationService.getBaseInfo(baseno);
	}
	
	public RegKthis insertRegKthisBpjs(InsertRegBpjs insertRegBpjs)
	{
		return MtRegistrationService.insertRegKthisBpjs(insertRegBpjs);
	}
	
	public RegKthis insertRegKthisGeneral(InsertRegGeneral insertRegGeneral)
	{
		return MtRegistrationService.insertRegKthisGeneral(insertRegGeneral);
	}
	
	public RegKthis insertRegKthisBpjs2(InsertRegBpjs insertRegBpjs)
	{
		return MtRegistrationService.insertRegKthisBpjs2(insertRegBpjs);
	}
	
	public RegKthis insertRegKthisGeneral2(InsertRegGeneral insertRegGeneral)
	{
		return MtRegistrationService.insertRegKthisGeneral2(insertRegGeneral);
	}
	
	public String cancelReg( CancelReg cancelReg)
	{
		return MtRegistrationService.cancelReg(cancelReg);
	}
	
	public DetailSep getSep(String baseno)
	{
		return MtRegistrationService.getSep(baseno);
	}

	public IntegratedKthis sendSep(PDFUpload sep) {
		return MtRegistrationService.sendSep(sep);
	}
}
