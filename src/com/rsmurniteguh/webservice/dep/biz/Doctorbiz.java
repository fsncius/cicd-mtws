package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.NamaMapBpjs;
import com.rsmurniteguh.webservice.dep.kthis.services.DocService;
import com.rsmurniteguh.webservice.dep.kthis.view.CodeDescView;

public class Doctorbiz {

	public List<CodeDescView> getDataDoctor(String idDok) {
		return DocService.getDataDoctor(idDok);
	}
	
	public List<NamaMapBpjs> getDataPoly() {
		return DocService.getDataPoly();
	}

}
