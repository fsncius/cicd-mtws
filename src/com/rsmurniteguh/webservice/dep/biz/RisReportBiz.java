package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.RisAdmissionList;
import com.rsmurniteguh.webservice.dep.all.model.RisReportList;
import com.rsmurniteguh.webservice.dep.kthis.services.RisReportSer;

public class RisReportBiz {

	public List<RisReportList> getrisreport(String perstartdate) {
		return RisReportSer.getrisreport(perstartdate);
	}

	public List<RisAdmissionList> getrisadmission(String admissionid) {
		return RisReportSer.getrisadmission(admissionid);
	}
	public List<RisReportList> getrisreportByPatient(String patientName, String patientId) {
		return RisReportSer.getRisReportBypatient(patientName, patientId);
	}
	public List<RisReportList> getxrayrisreport(String perstartdate) {
		return RisReportSer.getxrayrisreport(perstartdate);
	}
	public List<RisAdmissionList> getxrayrisreportadmission(String admissionid) {
		return RisReportSer.getxrayrisreportadmission(admissionid);
	}
	
	public List<RisAdmissionList> getrisadmissionlist(String admissionidlist) {
		return RisReportSer.getrisadmissionlist(admissionidlist);
	}
	
	public List<RisAdmissionList> getrisreportadmission(String admissionid) {
		return RisReportSer.getrisreportadmission(admissionid);
	}
}
