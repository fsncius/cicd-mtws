package com.rsmurniteguh.webservice.dep.biz;

import java.util.ArrayList;

import com.rsmurniteguh.webservice.dep.kthis.services.HrisService;
import com.rsmurniteguh.webservice.dep.kthis.services.ServerPrintSer;

public class ServerPrintBiz {

	public static Object getPrintList(String listString) {
		return ServerPrintSer.getPrintList(listString);
	}

	public static Object addPrintCount(Long serverPrintId) {
		return ServerPrintSer.addPrintCount(serverPrintId);
	}

}
