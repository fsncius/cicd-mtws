package com.rsmurniteguh.webservice.dep.biz;


import com.rsmurniteguh.webservice.dep.all.model.ReturnResult;
import com.rsmurniteguh.webservice.dep.kthis.services.AuthenticatorService;
import com.rsmurniteguh.webservice.dep.kthis.services.mobile.DoctorMobileService;

public class AuthenticatorBiz {

	public ReturnResult Authenticate(String user, String token) {
		return AuthenticatorService.Authenticate(user, token);
	}
	public Object get_trx_emoney(String nik, String startdate, String enddate) {
		// TODO Auto-generated method stub
		return AuthenticatorService.get_trx_emoney(nik, startdate, enddate);
	}
	public Object getMainDataDoctor(String usermstr_id) {
		return DoctorMobileService.getMainDataDoctor(usermstr_id);
	}
}
