package com.rsmurniteguh.webservice.dep.biz;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import com.rsmurniteguh.webservice.dep.all.model.mobile.appointmentHistory;
import com.rsmurniteguh.webservice.dep.all.model.AndroidUpdate;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.mobile.AppointmentSource;
import com.rsmurniteguh.webservice.dep.all.model.mobile.AppointmentStatus;
import com.rsmurniteguh.webservice.dep.all.model.mobile.CodeDescView;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataAppointment;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataRujukan;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataUser;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DoctorVisitRegnList;
import com.rsmurniteguh.webservice.dep.all.model.mobile.InfoPatient;
import com.rsmurniteguh.webservice.dep.all.model.mobile.IpDoctorVisitList;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ListQueNum;
import com.rsmurniteguh.webservice.dep.all.model.mobile.LoginExternal;
import com.rsmurniteguh.webservice.dep.all.model.mobile.LoginForm;
import com.rsmurniteguh.webservice.dep.all.model.mobile.PurchaseRequisition;
import com.rsmurniteguh.webservice.dep.all.model.mobile.RadiologyScheduleViewModel;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResourceList;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResourceScheme;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.mobile.RisReportTime;
import com.rsmurniteguh.webservice.dep.all.model.mobile.UserList;
import com.rsmurniteguh.webservice.dep.all.model.mobile.WeeklySchedule;
import com.rsmurniteguh.webservice.dep.all.model.mobile.WhatsNewView;
import com.rsmurniteguh.webservice.dep.kthis.services.HrisService;
import com.rsmurniteguh.webservice.dep.kthis.services.MobileAppointment;
import com.rsmurniteguh.webservice.dep.kthis.services.MobileAuth;
import com.rsmurniteguh.webservice.dep.kthis.services.MobileCommon;
import com.rsmurniteguh.webservice.dep.kthis.services.MobileDoctor;
import com.rsmurniteguh.webservice.dep.kthis.services.MobileServices;

public class MobileBiz {
	public static final String UPLOAD_FILE_SERVER = "D:\\upload\\";
	public final static String FMCurl = "https://fcm.googleapis.com/fcm/send";
	public final static String FMKey = "AAAAfK_57xU:APA91bHhBr-5QVytkPOL6f_knTW-5I4L0g36MCFqAsclNC3c-T4cBt4FRGbac8XjBezYsKwUFab8lfLzN8T7DkUHN_HvH0PjB5PM5gA369f9N82KZesbC8NPw7h7IOidSD1PVweuMluc";
	public final static String APPOINTMENT_STATUS_CONFIRM = "Confirm";
	public final static String APPOINTMENT_STATUS_REGISTERED = "Registered";
	public final static String APPOINTMENT_STATUS_CANCEL = "Cancel";
	public final static String APPOINTMENT_STATUS_WAIT = "Wait";
	public final static String APPOINTMENT_STATUS_DONE = "Done";
	public final static String APPOINTMENT_STATUS_DELETE = "Delete";
	public final static String APPOINTMENT_STATUS_REJECT = "Rejected";
	public final static String APPOINTMENT_KET_WAIT = "Menunggu Konfirmasi dari Pihak Rumah Sakit";
	public final static String APPOINTMENT_KET_CONFIRM = "Silahkan datang ke RS Murni Teguh sesuai dengan tanggal berobat anda";
	
	public final static BigDecimal BPJS = new BigDecimal(336581902);
	public final static BigDecimal POLI = new BigDecimal(311187819);
	public static final String BPJS_TEXT = "BPJS";
	public static final String MRN_TEXT = "MRN";
	public static final String GENERAL_TEXT = "GENERAL";
	public static final String STAFF_TEXT = "STAFF";
	public static final String DEPARTMENT_BPJS = "01";
	public static final String DEPARTMENT_BPJSEX = "01";
	public static final String DEPARTMENT_POLI = "02";
	public static final String DEPARTMENT_COORPORATE = "03";
	public static final String DEPARTMENT_INSURANCE = "04";
	public static final String MOBILE_SOURCE = "02";
	public static final String DESKTOP_SOURCE = "03";
	public static final String WEB_SOURCE = "01";
	public static final String mRujukan = "Rujukan";
	public static final String mRujukanTambahan = "Rujukantambahan";
	
	public static final String IND_Y = "Y";
	public static final String IND_N = "N";
	public static final String WEB_ALL = "ALL";
	public static final String WEB_BIG = "BIG";
	public static final String WEB_SMALL = "SMALL";
	
	public static final String mBerita = "berita";
	public static final String mView = "Viewer";
	// -- fungsi hardcodeF
	public static final String CUSTOM_CODE_ABR = "'HDL'";
	public static final String CUSTOM_RESOURCEMSTR_ID_GENERAL = "";
	public static final String CUSTOM_RESOURCEMSTR_ID_BPJS = "";
	public static final String CUSTOM_SUBSPECIALTYMSTR_ID_GENERAL = "(311187819)"; 
	public static final String REMOTE_IP_WHITELIST = "rsmurniteguh.com 11.0.200.3 127.0.0.1 192.168.222.114 192.168.110.211 192.168.110.212 192.168.222.234 192.168.222.233"; 
	
	
	public static final String CUSTOM_SUBSPECIALTYMSTR_ID_BPJS = "(311099877)";

	public List<CodeDescView> GetPoliclynicLists() {
		return MobileDoctor.GetPoliclynicLists();
	}

	public ResponseString change_password_patient(String usermstrId, String password, String new_password) {
		return MobileAuth.getChangePasswordPatient(usermstrId, password, new_password);
	}

	public List<CodeDescView> GetPoliclynicListsByLocation(String location) {
		return MobileDoctor.GetPoliclynicListsByLocation(location);
	}

	public List<ResourceList> GetDoctorsList(String poli, String location) {
		return MobileDoctor.GetDoctorsList(poli, location);
	}

	public WeeklySchedule GetDoctorSchedule(String resourcemstr) {
		return MobileDoctor.GetDoctorSchedule(resourcemstr);
	}

	public List<ResourceScheme> getResourceScheme(String resourcemstr, String tanggal) {
		return MobileDoctor.getResourceScheme(resourcemstr, tanggal);
	}

	public Object get_doctorlist_schedule(String poli) {
		return MobileDoctor.get_doctorlist_schedule(poli);
	}

	public Object getDoctorListScheduleByName(String name) {
		return MobileDoctor.getDoctorListScheduleByName(name);
	}

	public ResponseString CheckLogin(String user, String pass) {
		return MobileAuth.CheckLogin(user, pass);
	}

	public ResponseString CheckLoginPatient(String user, String pass) {
		return MobileAuth.CheckLoginPatient(user, pass);
	}

	public ResponseString CheckLoginStaff(String user, String pass) {
		return MobileAuth.CheckLoginStaff(user, pass);
	}

	public LoginExternal login_external(LoginForm loginform) {
		return MobileAuth.login_external(loginform);
	}

	public List<RisReportTime> getRisReportTime(String tanggal, String jenis) {
		return MobileDoctor.getRisReportTime(tanggal, jenis);
	}

	public List<PurchaseRequisition> getPurchaseRequisition() {
		return MobileDoctor.getPurchaseRequisition();
	}

	public ResponseString setPurchaseRequisition(String stockpurchase_requisitionid, String isconfirm, String userMstrId) {
		return MobileDoctor.setPurchaseRequisition(stockpurchase_requisitionid, isconfirm, userMstrId);
	}

	public ResponseString SaveRegistrationToken(String user, String token) {
		return MobileAuth.SaveRegistrationToken(user, token);
	}

	public List<DoctorVisitRegnList> GetDoctorRegistrationList(String usermstrId) {
		return MobileDoctor.GetDoctorsAvailableRegistration(new BigDecimal(usermstrId));
	}

	public List<IpDoctorVisitList> GetDoctorIPVisitList(String usermstrId) {
		return MobileDoctor.GetDoctorIPVisitList(new BigDecimal(usermstrId));
	}

	public List<RadiologyScheduleViewModel> GetRadiologySchedule(String date, String type) {
		return MobileDoctor.GetRadiologySchedule(date, type);
	}

	public ResponseString UserCreate(String mrn, String username, String password, String tgl_lahir) {
		return MobileServices.UserCreate(mrn, username, password, tgl_lahir);
	}

	public List<UserList> UserGet(String username) {
		return MobileServices.UserGet(username);
	}

	public ResponseString UserUpdate(String mrn, String username, String password, String tgl_lahir,
			String token) {
		return MobileServices.UserUpdate(mrn, username, password, tgl_lahir, token);
	}

	public ResponseString UserDelete(String username) {
		return MobileServices.UserDelete(username);
	}

	public List<ResourceList> SearchDoctor(String searchtext, String lokasi) {
		return MobileServices.SearchDoctor(searchtext, lokasi);
	}

	public InfoPatient getInfoPatient(String mrn, String tgl_lahir, String username) {
		return MobileServices.getInfoPatient(mrn, tgl_lahir, username);
	}

	public Object confirmPatient(String mrn, String tgl_lahir, String pass) {
		return MobileServices.confirmPatient(mrn, tgl_lahir, pass);
	}

	public Object notificationPublic(String tanggal) {
		return MobileServices.notificationAppointment(tanggal);
	}

	public List<appointmentHistory> appointmentHistory(String user_id, String mrn, String lokasi) {
		return MobileAppointment.appointmentHistory(user_id, mrn, lokasi);
	}
	
	public List<appointmentHistory> appointmentList(String user_id, String mrn) {
		return MobileAppointment.appointmentList(user_id, mrn);
	}

	public ResponseString UserCreateApointment(String mrn, String resourcemstr, String user_id, String tanggal,
			String lokasi, String careprovider_id, String doctor_name, String no_subspecialis,String no_doctor, String patient_name, String no_mrn) {
		return MobileAppointment.UserCreateApointment(mrn, resourcemstr, user_id, tanggal, lokasi,
				careprovider_id, doctor_name, no_subspecialis, no_doctor, patient_name, no_mrn);
	}

	public Object cancelAppointment(String reg_id) {
		return MobileAppointment.cancelAppointment(reg_id);
	}

	public Object checkAvailable(String tanggal, String user_id, String careprovider_id, String resourcemstr) {
		return MobileAppointment.checkAvailable(tanggal, user_id, careprovider_id, resourcemstr);
	}

	public ResponseString notificationAppointment(String tanggal) {
		return MobileAppointment.notificationAppointment(tanggal);
	}

	public Object notificationDirect(String message, String user_id) {
		return MobileAppointment.notificationDirect(message, user_id);
	}

	public appointmentHistory InfoRegistration(String registrasi_id) {
		return MobileAppointment.InfoRegistration(registrasi_id);
	}

	public List<ListQueNum> getDoctorQueueList(String careproviderId, String subspecialtymstr) {
		return MobileAppointment.getDoctorQueueList(careproviderId, subspecialtymstr);
	}

	public List<AppointmentSource> getAllAppointmentByStatus(String tanggal, String status) {
		return MobileAppointment.getAllAppointmentByStatus(tanggal, status);
	}

	public List<AppointmentStatus> topRegisteredAppointment(String tanggal) {
		return MobileAppointment.topRegisteredAppointment(tanggal);
	}

	public List<DataRujukan> getDataRujukan(String mrn) {
		return MobileAppointment.getDataRujukan(mrn);
	}

	public ResponseString addRujukan(String mrn, String userid, String no_rujukan, String tanggal_rujukan,
			String gambar) {
		return MobileAppointment.addRujukan(mrn, userid, no_rujukan, tanggal_rujukan, gambar);
	}

	public ResponseString uploadRujukan(List<Attachment> attachments, HttpServletRequest request) {
		return MobileAppointment.uploadRujukan(attachments, request);
	}

	public Response downloadRujukan(String rujukan_id, String jenis, HttpServletRequest request) {
		return MobileAppointment.downloadRujukan(rujukan_id, jenis, request);
	}

	public List<WhatsNewView> getWhatsNewList(String projectCode, String version) {
		return MobileCommon.getWhatsNewList(projectCode, version);
	}
	
	public AndroidUpdate getAndroidVersion(String projectCode) {
		return MobileCommon.getAndroidVersion(projectCode);
	}

	public Object simple_get_poly() {
		return MobileCommon.simple_get_poly();
	}

	public List<ResourceList> GetDoctorsPolyName(String poli, String location) {
		return MobileDoctor.GetDoctorsPolyName(poli, location);
	}

	public Object get_berita(String reader_role) {
		return MobileCommon.get_berita(reader_role);
	}
	
	public Object add_kotak_saran(String user_id, String user_name, String project_code, String version, String saran, String kontak) {
		return MobileCommon.add_kotak_saran(user_id,user_name,project_code,version, saran, kontak);
	} 
	
	public Object getBpjsData(String noKartuBpjs) {
		return MobileCommon.getBpjsData(noKartuBpjs);
	} 
	
	
	/* --------------- */
	public ResponseStatus updateDataUser(String fullname,String idcard, String bpjs, String contact,  String email, String userid) {
		return MobileAppointment.updateDataUser(fullname,idcard, bpjs, contact, email, userid);
	}
	
	public ResponseStatus checkAvalibaleRujukan(String mrn) {
		return MobileAppointment.checkAvalibaleRujukan(mrn);
	}
	
	public ResponseStatus checkAvalibaleAppointment(String nomrn, String tglappointment, String resourcemstrid) {
		return MobileAppointment.checkAvalibaleAppointment(nomrn,tglappointment,resourcemstrid);
	}
	
	public List<DataRujukan> getRujukan(String mrn) {
		return MobileAppointment.getRujukan(mrn);
	}
	
	public List<DataRujukan> getRujukanTambahan(String mrn) {
		return MobileAppointment.getRujukanTambahan(mrn);
	}
	
	
	public ResponseStatus createAppointment(DataAppointment dataform){
		return MobileAppointment.createAppointment(dataform);
	}

	public ResponseStatus createRujukan(DataRujukan dataform){
		return MobileAppointment.createRujukan(dataform);
	}
	
	public ResponseStatus createRujukanTambahan(DataRujukan dataform){
		return MobileAppointment.createRujukanTambahan(dataform);
	}
	
	
	public ResponseStatus getInfoReject(String ids){
		return MobileAppointment.getInfoReject(ids);
	}
	
	public ResponseStatus validNomorRujukan(String ids, String mrn){
		return MobileAppointment.validNomorRujukan(ids,mrn);
	}
	
	public DataAppointment getAppointmentbyId(String ids){
		return MobileAppointment.getAppointmentbyId(ids);
	}
	
	public List<DataAppointment> getAppointment(String mrn,String departement, String source, String tipe){
		return MobileAppointment.getAppointment(mrn,departement,source,tipe);
	}
}
