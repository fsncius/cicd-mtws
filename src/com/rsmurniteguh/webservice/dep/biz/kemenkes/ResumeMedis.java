package com.rsmurniteguh.webservice.dep.biz.kemenkes;

import org.apache.http.util.TextUtils;

import com.rsmurniteguh.webservice.dep.all.model.kemenkes.MetaData;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.ResponseData;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis.GetParam;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis.GetResponse;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis.InsertParam;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis.InsertResponse;

public class ResumeMedis {
	private String mNik;

	public ResumeMedis(String nik) {
		this.mNik = nik;
	}
	
	public ResponseData insert(InsertParam param) {
		ResponseData result = ResponseData.init();
		try {
			String test = Utils.toJsonString(param);
			String response = Service.ResumeMedis.insert(param);
			if(TextUtils.isEmpty(response)) result = ResponseData.fail("Gagal request kemenkes.[null]");
			InsertResponse data = Utils.toObjectFromJson(response, InsertResponse.class);
			data.setMessage(TextUtils.isEmpty(data.getMessage()) ? data.getMsg() : data.getMessage());
			String message = TextUtils.isEmpty(data.getMessage()) ? "Gagal." : data.getMessage();
			if(data.getSuccess())result = ResponseData.success(MetaData.success(TextUtils.isEmpty(data.getMessage()) ? "Berhasil" : message), data);
			else result = ResponseData.fail(MetaData.fail("201", message), data);
			
		}catch(Exception ex) {
			result = ResponseData.error(ex);
		}
		
		return result;
	}

	public String getmNik() {
		return mNik;
	}

	public void setmNik(String mNik) {
		this.mNik = mNik;
	}

	public ResponseData get(String tanggalAwal, String tanggalAkhir) {
		ResponseData result = ResponseData.init();
		try {
			GetParam param = new GetParam(getmNik(), tanggalAwal, tanggalAkhir);
			String response = Service.ResumeMedis.get(param);
			GetResponse data = Utils.toObjectFromJson(response, GetResponse.class);
			String message = TextUtils.isEmpty(data.getMessage()) ? "Berhasil request get." : data.getMessage();
			result = ResponseData.success(MetaData.success(message), data.getData());
		}catch(Exception ex) {
			result = ResponseData.error(ex);
		}
		
		return result;
	}
}
