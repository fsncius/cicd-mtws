package com.rsmurniteguh.webservice.dep.biz.kemenkes;

import org.apache.http.util.TextUtils;
import org.json.JSONObject;
import org.json.XML;

import com.rsmurniteguh.webservice.dep.all.model.kemenkes.MetaData;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.ResponseData;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.referensi.DataUmumResponse;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.Service.ReferensiProvider.Path;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.IResponseClass;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.IResponseTarget;

public class Referensi {
	
	public ResponseData DataUmum(DataUmum dataUmum) {
		ResponseData result = ResponseData.init();
		try {
			String response = Service.Referensi.DataUmum(dataUmum.getPath());
			if(TextUtils.isEmpty(response)) result = ResponseData.fail("Gagal request.[null]");
			JSONObject jsonObject = XML.toJSONObject(response);
			result = processResponse(jsonObject, ResponseData.class, DataUmumResponse.class);
		}catch(Exception ex) {
			result = ResponseData.error(ex);
		}
		return result;
	}
	
	public static enum DataUmum{
		JenisPelayanan(Service.ReferensiProvider.Path.JENIS_PELAYANAN),
		KeadaanKeluar(Service.ReferensiProvider.Path.KEADAAN_KELUAR),
		Faskes(Service.ReferensiProvider.Path.FASKES),
		CaraKeluar(Service.ReferensiProvider.Path.CARA_KELUAR),
		Instalasi(Service.ReferensiProvider.Path.INSTALASI);
		private Path path;

		DataUmum(Service.ReferensiProvider.Path path){
			this.path = path;
		}

		public Path getPath() {
			return path;
		}

		public void setPath(Path path) {
			this.path = path;
		}
	}
	
	
	private <T> T processResponse(JSONObject jsonObject, Class<T> baseClass, Class clazz) throws Exception {
		T result = baseClass.newInstance();
		Object objTarget = Utils.toObjectFromJson(jsonObject, clazz);
		IResponseClass responseClass = (IResponseClass)result;
		IResponseTarget target = objTarget instanceof IResponseTarget ? (IResponseTarget)objTarget : null;
		if(target !=null) responseClass.processResponseTarget(target);
		return result;
	}
	
}
