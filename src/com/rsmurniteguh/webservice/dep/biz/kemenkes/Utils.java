package com.rsmurniteguh.webservice.dep.biz.kemenkes;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;

import org.apache.http.util.TextUtils;
import org.json.JSONObject;
import org.json.XML;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


public class Utils {
	private static Gson gson;
	public static String toJsonString(Object target) {
		String result = "{}";
        if(target == null)return result;
        gson = new Gson();
        result = gson.toJson(target);
        return result;
	}
	
	public static byte[] toBytes_UTF8(Object target) throws UnsupportedEncodingException {
		String convertTarget = toJsonString(target);
		return convertTarget.getBytes("UTF-8");
	}

	public static <T> T toObjectFromJson(String target, Class<T> clazz)throws Exception {
		T result = null;
		if(TextUtils.isEmpty(target)) return (T)result;
        Object convert = toObject(target, clazz);
        result = (T)convert;
        return result;
	}
	
	public static <T> T toObjectFromJson(JSONObject jsonObject, Class<T> clazz)throws Exception {
		return toObjectFromJson(jsonObject.toString(), clazz);
	}
	
	public static Object toObject(String target, Type type){
        Object result = null;
        if(TextUtils.isEmpty(target) || type == null)return result;
        gson = new Gson();
        result = gson.fromJson(target, type);        
        return result;
    }
	public static <T> T getMappedClass(HashMap rowInfo, Class<T> clazz, boolean caseInsensitive) throws JsonProcessingException, IOException
	{
		T result = null;
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		if(caseInsensitive) mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(rowInfo);
		result = mapClass(mapper, json, clazz);
		return result;
	}
	
	public static <T> T getMappedClass(HashMap rowInfo, Class<T> clazz) throws JsonProcessingException, IOException{
		return getMappedClass(rowInfo, clazz, false);
	}
	
	private static <T> T mapClass(ObjectMapper mapper, String json, Class<T> clazz) throws JsonProcessingException, IOException
	{
		T result = null;
		if(mapper == null)return result;
		result = mapper.readValue(json, clazz);
		return result;
	}
	
	
}
