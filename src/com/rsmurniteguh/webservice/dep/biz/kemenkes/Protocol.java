package com.rsmurniteguh.webservice.dep.biz.kemenkes;

import org.apache.http.util.TextUtils;

public class Protocol {
	public enum ProtocolType{
		HTTP("http"),
		HTTPS("https"),
		FTP("ftp"), 
		FILE("file"),
		JAR("jar");
		private String name;
		ProtocolType(String name) {
			this.setName(name);
		}
		
		@Override
		public String toString() {
			return this.name();
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
	}
	
	public static Boolean isProtocol(ProtocolType protocol, String input) {
		return TextUtils.isEmpty(input) ? false : protocol.toString().toLowerCase().equals(input.toLowerCase());
	}
}
