package com.rsmurniteguh.webservice.dep.biz.kemenkes;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpException;

import com.rsmurniteguh.webservice.dep.biz.kemenkes.NetworkConnection.RequestProvider.Builder;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.NetworkConnection.RequestProvider.RequestException;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.Protocol.ProtocolType;
import com.rsmurniteguh.webservice.dep.kthis.services.TrustModifier;

public class NetworkConnection {
	public interface IConnectionType {
		public abstract HttpURLConnection DefaultConnection(URL url)throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException;
		public abstract HttpsURLConnection SecureConnection(URL url) throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException;
		

	}
	private static abstract class ConnectionProvider implements IConnectionType{
		private static HttpURLConnection BasicHTTPConnection(URL url) throws IOException{
			HttpURLConnection result = (HttpURLConnection) url.openConnection();
			return result;
		}
		
		private static HttpURLConnection BasicHTTPSConnection(URL url) throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException{
			HttpURLConnection result = (HttpURLConnection) url.openConnection();
			TrustModifier.relaxHostChecking(result);
			return result;
		}
		
		
	}
	
	public interface IRequestProvider{
		public String Response() throws IOException, HttpException, RequestException;
	}
	
	public static abstract class RequestProvider implements IRequestProvider{
		
		public static class Builder{
			private byte[] body;
			private String method;
			private String contentType;
			private byte[] content;
			private String acceptType;
			public static enum RequestMethod{
				GET("GET"),
				POST("POST");
				private String value;

				RequestMethod(String value){
					this.value = value;
				}

				public String getValue() {
					return value;
				}

				public void setValue(String value) {
					this.value = value;
				}
				
				public String toString() {
					return this.value;
				}
			}
			
			public static class RequestContent{
				public static RequestContent getNewInstance(PropertyValue acceptType, PropertyValue contentType, byte[] content) 
				{
					return new RequestContent(acceptType, contentType, content); 
				} 
				private PropertyValue contentType;
				private byte[] content;
				private PropertyValue acceptType;
				public PropertyValue getAcceptType() {
					return acceptType;
				}
				public void setAcceptType(PropertyValue acceptType) {
					this.acceptType = acceptType;
				}
				RequestContent(PropertyValue acceptType, PropertyValue contentType, byte[] content){
					this.contentType = contentType;
					this.acceptType = acceptType;
					this.content = content;
				}
				public PropertyValue getContentType() {
					return contentType;
				}
				public void setContentType(PropertyValue contentType) {
					this.contentType = contentType;
				}
				public byte[] getContent() {
					return content;
				}
				public void setContent(byte[] content) {
					this.content = content;
				}
			}
			
			public static enum PropertyValue{
				X_WWW_FORM_URLENCODED("application/x-www-form-urlencoded"),
				APPLICATION_JSON("application/json"),
				NONE("");
				private String value;

				PropertyValue(String value){
					this.value = value;
				}

				public String getValue() {
					return value;
				}

				public void setValue(String value) {
					this.value = value;
				}
				
				public String toString() {
					return this.value;
				}
			}
			
			public static Builder Basic(RequestMethod method, RequestContent content) {
				Builder result = new Builder();
				result.body = null;
				result.method = method.toString();
				result.contentType = content == null ? PropertyValue.NONE.toString() : content.getContentType().toString();
				result.acceptType = content ==null ? PropertyValue.NONE.toString() : content.getAcceptType().toString();
				result.content = content == null ? null : content.content;
				return result;
			}
			
			public static Builder Basic(RequestMethod method) {
				return Basic(method, null);
			}

			public byte[] getBody() {
				return body;
			}

			public void setBody(byte[] body) {
				this.body = body;
			}

			public String getMethod() {
				return method;
			}

			public void setMethod(String method) {
				this.method = method;
			}

			public String getContentType() {
				return contentType;
			}

			public void setContentType(String content) {
				this.contentType = content;
			}
			
			public String getAcceptType() {
				return acceptType;
			}

			public void setAcceptType(String acceptType) {
				this.acceptType = acceptType;
			}


			public byte[] getContent() {
				return content;
			}

			public void setContent(byte[] content) {
				this.content = content;
			}
		}
		
		public RequestProvider(Builder builder) {
			
		}
		

		
		public static class RequestException extends Exception{
			private int code;
			RequestException(String message, int code){
				super(message);
				this.code = code;
			}
			public int getCode() {
				return code;
			}
			public void setCode(int code) {
				this.code = code;
			}
		}
	}

	public static IConnectionType Build = Build();
	private static ConnectionProvider Build() {
		return new ConnectionProvider() {
			@Override
			public HttpURLConnection DefaultConnection(URL url) throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException{
				if(Protocol.isProtocol(ProtocolType.HTTP, url.getProtocol()))return ConnectionProvider.BasicHTTPConnection(url);
				if(Protocol.isProtocol(ProtocolType.HTTPS, url.getProtocol()))return ConnectionProvider.BasicHTTPSConnection(url);
				else return (HttpURLConnection) url.openConnection();
			}
			
			@Override
			public HttpsURLConnection SecureConnection(URL url) throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException{
				HttpsURLConnection result = (HttpsURLConnection) url.openConnection();
				TrustModifier.relaxHostChecking(result);
				return result;
			}
			
			
		};
	}
	public static IRequestProvider Request(HttpURLConnection con, Builder builder) {
		return new RequestProvider(builder) 
		{
			@Override
			public String Response() throws IOException, HttpException, RequestException  {
				String result = null;
				con.setInstanceFollowRedirects(false);
				con.setRequestMethod(builder.getMethod());
				con.setRequestProperty("Accept", builder.getAcceptType().toString());
				con.setRequestProperty("Content-Type", builder.getContentType().toString());
				con.setDoOutput(true);
				byte[] content = builder.getContent();
				System.out.println(content);
				BufferedReader in = null;
				String output;
				StringBuffer response = new StringBuffer();
				if(content != null) {

					DataOutputStream wr = new DataOutputStream(con.getOutputStream());
					wr.write(content);
					wr.flush();
					wr.close();
					response = getStringBuffer(con);
					
				}else {
					response = getStringBuffer(con);
				}
				
				result = response.toString();
				System.out.println(result);
				return result;
			}
			
			private StringBuffer getStringBuffer(HttpURLConnection con) throws RequestException, IOException {
				StringBuffer result = new StringBuffer();
				String output;
				BufferedReader in = null;
				int code = con.getResponseCode();
				in = new BufferedReader(new InputStreamReader(code < HttpURLConnection.HTTP_BAD_REQUEST ? con.getInputStream() :  con.getErrorStream()));
				while ((output = in.readLine()) != null) {
					result.append(output);
				}
				if(code != HttpURLConnection.HTTP_OK) throw new RequestException(result.toString(), code);
				return result;
			}
		};
	}
}
