package com.rsmurniteguh.webservice.dep.biz.kemenkes;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.util.TextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.Diagnosa;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.Diagnosa.DiagnosaType;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.Medis;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.MetaData;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.Pasien;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.ResponseData;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.SendResumeMedisParam;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis.InsertParam;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class SimRsIntegration {

	public static ResponseData sendResumeMedis(SendResumeMedisParam inputParam) {
		ResponseData result = ResponseData.fail("Gagal inisialisasi data");
		try {
			String query = "SELECT V.VISIT_ID, " + 
					"       PS.PERSON_ID, " + 
					"       CD.CARD_NO NO_RM, " + 
					"       PS.PERSON_NAME NAMA, " + 
					"       CASE " + 
					"         WHEN PS.SEX = 'SEXM' THEN " + 
					"          'L' " + 
					"         WHEN PS.SEX = 'SEXF' THEN " + 
					"          'P' " + 
					"         ELSE " + 
					"          'O' " + 
					"       END AS JENIS_KELAMIN, " + 
					"       CASE " + 
					"         WHEN SH.STAKEHOLDER_ID IS NOT NULL AND " + 
					"              (SH.CITY IS NOT NULL OR SH.ADDRESS_1 IS NOT NULL) THEN " + 
					"          CASE " + 
					"            WHEN SH.CITY IS NOT NULL THEN " + 
					"             (SELECT CITY_DESC FROM CITYMSTR WHERE CITY_CODE = SH.CITY) " + 
					"            ELSE " + 
					"             SH.ADDRESS_1 " + 
					"          END " + 
					"         ELSE " + 
					"          '' " + 
					"       END AS TEMPAT_LAHIR, " + 
					"       TO_CHAR(PS.BIRTH_DATE, 'YYYY-MM-DD') AS TANGGAL_LAHIR, " + 
					"       PS.MOBILE_PHONE_NO NO_TELP, " + 
					"       CASE " + 
					"         WHEN PS.ID_TYPE = 'IIT1' THEN " + 
					"          PS.ID_NO " + 
					"         ELSE " + 
					"          '' " + 
					"       END AS NIK, " + 
					"       CASE " + 
					"         WHEN V.PATIENT_TYPE = 'PTY1' THEN " + 
					"          '02' " + 
					"         ELSE " + 
					"          '-' " + 
					"       END AS KODE_PELAYANAN, " + 
					"       TO_CHAR(V.ADMISSION_DATETIME, 'YYYY-MM-DD') AS TANGGAL_REGISTRASI, " + 
					"       TO_CHAR(V.DISCHARGE_DATETIME, 'YYYY-MM-DD') AS TANGGAL_KELUAR, " + 
					"       MR.MEDICAL_RESUME_ID, " + 
					"       MR.IN_DIAGNOSISMSTR_ID AS DIAGNOSA_MASUK, " + 
					"       MR.MAIN_DIAGNOSISMSTR_ID AS DIAGNOSA_UTAMA, " + 
					"       MR.SEC_DIAGNOSISMSTR_ID AS DIAGNOSA_SEKUNDER, " + 
					"       'ANAMNESE' || CHR(13) || MR.ANAMNESE_DESC || CHR(13) || CHR(13) || " + 
					"       'PEMERIKSAAN FISIK' || CHR(13) || MR.PHYSIC_CHECK_DESC AS PEMERIKSAAN_FISIK, " + 
					"       CASE " + 
					"         WHEN MR.STATUS_KESADARAN IS NULL THEN " + 
					"          '' " + 
					"         WHEN MR.STATUS_KESADARAN = 'Sadar' THEN " + 
					"          '1' " + 
					"         ELSE " + 
					"          '2' " + 
					"       END AS KESADARAN, " + 
					"       MR.STATUS_KESADARAN AS KESADARAN, " + 
					"       MR.BLOOD_PLEASURE AS TEKANAN_DARAH, " + 
					"       MR.PULSE AS NADI, " + 
					"       MR.TEMPERATURE AS SUHU, " + 
					"       MR.BREATHING AS FREKUENSI_NAFAS, " + 
					"       MR.THERAPY_DESC AS TERAPI, " + 
					"       MR.ANAMNESE_DESC AS KELUHAN_UTAMA, " + 
					"       MR.SUPPORT_CHECK_DESC AS PEMERIKSAAN_PENUNJANG " + 
					"  FROM VISIT V " + 
					"  JOIN PATIENT P " + 
					"    ON P.PATIENT_ID = V.PATIENT_ID " + 
					"   AND P.DEFUNCT_IND = 'N' " + 
					"   AND P.LAST_CONDITION = 'PLC4' " + 
					"  JOIN PERSON PS " + 
					"    ON PS.PERSON_ID = P.PERSON_ID " + 
					"   AND PS.DEFUNCT_IND = 'N' " + 
					"  JOIN CARD CD " + 
					"    ON CD.PERSON_ID = PS.PERSON_ID " + 
					"   AND CD.DEFUNCT_IND = 'N' " + 
					"  LEFT JOIN (SELECT * " + 
					"               FROM (SELECT STAKEHOLDER_ID, " + 
					"                            CITY, " + 
					"                            ADDRESS_1, " + 
					"                            ROW_NUMBER() OVER(PARTITION BY STAKEHOLDER_ID ORDER BY ADDRESS_ID DESC) RN " + 
					"                       FROM ADDRESS " + 
					"                      WHERE ADDRESS_TYPE = 'ADRNPA' " + 
					"                        AND DEFUNCT_IND = 'N') " + 
					"              WHERE RN = 1) SH " + 
					"    ON SH.STAKEHOLDER_ID = PS.STAKEHOLDER_ID " + 
					"  LEFT JOIN (SELECT * " + 
					"               FROM (SELECT MEDICAL_RESUME_ID, " + 
					"                            VISIT_ID, " + 
					"                            DISCHARGED_DATETIME, " + 
					"                            IN_DIAGNOSISMSTR_ID, " + 
					"                            MAIN_DIAGNOSISMSTR_ID, " + 
					"                            NULL AS SEC_DIAGNOSISMSTR_ID, " + 
					"                            ANAMNESE_DESC, " + 
					"                            SUPPORT_CHECK_DESC, " + 
					"                            PHYSIC_CHECK_DESC, " + 
					"                            STATUS_KESADARAN, " + 
					"                            BLOOD_PLEASURE, " + 
					"                            PULSE, " + 
					"                            TEMPERATURE, " + 
					"                            BREATHING, " + 
					"                            THERAPY_DESC, " + 
					"                            ROW_NUMBER() OVER(PARTITION BY VISIT_ID ORDER BY MEDICAL_RESUME_ID DESC) AS RN " + 
					"                       FROM MEDICAL_RESUME " + 
					"                      WHERE DEFUNCT_IND = 'N') " + 
					"              WHERE RN = 1) MR " + 
					"    ON MR.VISIT_ID = V.VISIT_ID " + 
					" WHERE V.VISIT_ID = ? " + 
					"   AND V.PATIENT_TYPE = 'PTY1'"; //-- PATIENT TYPE HANYA RAWAT INAP
			BigDecimal visitId = inputParam.getVisitId();
			Object[] parameters = new Object[] {visitId};
			List rawData = DbConnection.executeReader(DbConnection.KTHIS, query, parameters);
			if(rawData ==null)return result;
			if(rawData.size() > 1)return fail("Data resume lebih dari satu. query gagal!", visitId, true);
			if(rawData.size() <= 0)return fail("Data kosong/tidak sesuai kualifikasi.", visitId, true);
			HashMap data = (HashMap)rawData.get(0);
			Pasien pasien = Utils.getMappedClass(data, Pasien.class, true);
			pasien.setTempat_lahir(StringUtils.isEmpty(pasien.getTempat_lahir()) ? inputParam.getTempatLahir() : pasien.getTempat_lahir());
			ResumeMedis resumeMedis = new ResumeMedis(pasien.getNik());
			Medis medis = Utils.getMappedClass((HashMap)rawData.get(0), Medis.class, true);		
			DiagnosaType diagnosaMasuk = DiagnosaType.DIAGNOSA_MASUK.set(data);
			DiagnosaType diagnosaUtama = DiagnosaType.DIAGNOSA_UTAMA.set(data);
			DiagnosaType diagnosaSekunderI = DiagnosaType.DIAGNOSA_SEKUNDER_I.set(data);
			DiagnosaType[] listDiagnosa = new DiagnosaType[] {diagnosaMasuk, diagnosaUtama, diagnosaSekunderI};
			ArrayList<Diagnosa> buildDiagnosa = generateResumeMedisDiagnosa(listDiagnosa);
			Diagnosa[] diagnosas = buildDiagnosa == null ? null : buildDiagnosa.toArray(new Diagnosa[buildDiagnosa.size()]);
			medis.setKode_rs_rujukan(inputParam.getKodeRs());
			medis.setNama_rs_rujukan(inputParam.getNamaRs());
			medis.setJenis_rujukan(inputParam.getJenisRujukan());
//			medis.setKode_cara_keluar("07");
//			medis.setKode_keadaan_keluar("02");
			InsertParam sendParam = new InsertParam(pasien, medis, diagnosas);
			ResponseData responseData = resumeMedis.insert(sendParam);
			if(responseData == null)return fail("Request kemenkes gagal.", visitId, true);
			if(!responseData.getMetaData().getCode().equals("200"))return fail(responseData.getMetaData(), visitId, true);
			result = responseData;
			markVisitResumeMedisSended(visitId, true);
		}catch(Exception ex) {
			ex.fillInStackTrace();
			System.out.println(ex.getMessage());
			result = ResponseData.fail(ex.getMessage());
		}
		return result;
	}
	
	private static ResponseData fail(String message, BigDecimal visitId, boolean entryResumeMedis) throws SQLException, Exception{
		if(entryResumeMedis)markVisitResumeMedisSended(visitId, false);
		return ResponseData.fail(message);
	}
	private static ResponseData fail(MetaData metaData, BigDecimal visitId, boolean entryResumeMedis) throws SQLException, Exception{
		if(entryResumeMedis)markVisitResumeMedisSended(visitId, false);
		return ResponseData.fail(metaData);
	}
	
	private static void markVisitResumeMedisSended(BigDecimal visitId, boolean condition) throws SQLException, Exception {
			if(visitId == null)return;
			String status = condition ? "MRKSEND" : "MRKENTRY";
			String query = "UPDATE VISIT SET MEDICAL_RESUME_STATUS = ? WHERE VISIT_ID = ? "; 
			Object[] parameters = new Object[] {status, visitId};
			DbConnection.executeQuery(DbConnection.KTHIS, query, parameters);
	}
	
	private static ArrayList<Diagnosa> generateResumeMedisDiagnosa(DiagnosaType[] targetDiagnosa) {
		ArrayList<Diagnosa> result = new ArrayList<Diagnosa>();
		try {
			StringBuilder sb = new StringBuilder();
			int index = 0;
			for(DiagnosaType target : targetDiagnosa) {
				if(target == null)continue;
				if(target.getId() ==null)continue;
				if(index <=0)sb.append(" AND DIAGNOSISMSTR_ID IN (");
				else sb.append(", ");
				sb.append(target.getId());
				index++;
			}
			String condition = sb.toString();
			if(TextUtils.isEmpty(condition))return result;
			condition += ")";
			String query = "SELECT DIAGNOSISMSTR_ID ID, DIAGNOSIS_CODE AS KODE_ICD10, DIAGNOSIS_DESC AS KETERANGAN FROM DIAGNOSISMSTR WHERE DEFUNCT_IND = 'N' " + condition;
			Object[] parameters = new Object[] {};
			List rawData = DbConnection.executeReader(DbConnection.KTHIS, query, parameters);
			if(rawData == null)return result;
			if(rawData.size() <=0)return result;
			ArrayList<Diagnosa> listDataMapped = getMappedListDiagnosa(rawData);
			for(DiagnosaType target : targetDiagnosa) {
				if(target.getId() == null) continue;
				Diagnosa diagnosa = Diagnosa.from(target, listDataMapped);
				result.add(diagnosa);
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		return result;
	}
	
	private static ArrayList<Diagnosa> getMappedListDiagnosa(List rawData) throws JsonProcessingException, IOException{
		if(rawData == null)return null;
		ArrayList<Diagnosa> result = new ArrayList();
		for(Object data : rawData){
			HashMap target = (HashMap)data;
			Diagnosa diagnosa = Utils.getMappedClass(target, Diagnosa.class, true);
			result.add(diagnosa);
		}
		return result;
	}

}
