package com.rsmurniteguh.webservice.dep.biz.kemenkes;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringUtils;

import com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis.GetParam;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis.InsertParam;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.NetworkConnection;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.NetworkConnection.IRequestProvider;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.NetworkConnection.RequestProvider.Builder;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.NetworkConnection.RequestProvider.Builder.PropertyValue;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.NetworkConnection.RequestProvider.Builder.RequestContent;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.NetworkConnection.RequestProvider.Builder.RequestMethod;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;

public class Service {
	private static final String XCONSID =  getBaseConfig(IParameterConstant.KEMENKES_XCONSID);
//	"1275889";
	private static final String SECRETKEY = getBaseConfig(IParameterConstant.KEMENKES_SECRETKEY);
//	"rujukan2016";
	private static final String BASEURL = getBaseConfig(IParameterConstant.KEMENKES_BASE_URL);
//	"http://103.74.143.35/apps/sisrute/index_ci.php/services/";
	
	private static String getBaseConfig(String parameter){
		CommonService cs = new CommonService();
		return cs.getParameterValue(parameter);
	}
	
	private static String generateHmacSHA256Signature(String data, String key) throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] hmacData;
        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(secretKey);
        hmacData = mac.doFinal(data.getBytes("UTF-8"));
        return Base64.getEncoder().encodeToString(hmacData);
    }
	
	private static void setHeader(HttpURLConnection con) throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String strTimeStamp = Long.toString(timestamp.getTime()).substring(0, 10);
		con.setRequestProperty("X-Cons-Id", XCONSID);
		con.setRequestProperty("X-Timestamp", strTimeStamp);
		con.addRequestProperty("X-Signature", generateHmacSHA256Signature(XCONSID + "&" + strTimeStamp, SECRETKEY));
	}
	
	private static String Request(String urlParam, byte[] param) throws Exception 
	{
		URL url = new URL(urlParam);
		HttpURLConnection con = NetworkConnection.Build.DefaultConnection(url);
		setHeader(con);
		IRequestProvider requestProvider = NetworkConnection.Request(con, Builder.Basic(RequestMethod.POST
				, RequestContent.getNewInstance(PropertyValue.APPLICATION_JSON, PropertyValue.APPLICATION_JSON, param)));
		return requestProvider.Response();
	}
	
	private static String Request(String urlParam) throws Exception 
	{
		return Request(urlParam, RequestMethod.GET);
	}
	
	private static String Request(String urlParam, RequestMethod requestMethod) throws Exception 
	{
		URL url = new URL(urlParam);
		HttpURLConnection con = NetworkConnection.Build.DefaultConnection(url);
		setHeader(con);
		IRequestProvider requestProvider = NetworkConnection.Request(con, Builder.Basic(requestMethod));
		return requestProvider.Response();
	}
	
	private static abstract class ResumeMedisProvider implements IResumeMedis{
		private final String BaseURL = "";
		public final String InsertURL = BaseURL + "";
		public abstract String getBaseURL();
		public String getUrl(Path path) {
			String result = null;
			result = getBaseURL() + path.getPath();
			return result;
		}
		
		public static enum Path{
			Insert("/resume/save_resume"),
			Get("/resume/load_resume");
			private String path;

			Path(String path){
				this.path = path;
			}

			public String getPath() {
				return path;
			}

			public void setPath(String path) {
				this.path = path;
			}
		}
		
		class QueryParam {
			private String mResult;
			private StringBuilder sb;
			private Integer index = 0;
			QueryParam(){
				this.sb = new StringBuilder();
				this.sb.append("?");
			}
			
			public void add(String key, String value) {
				if(index >1)this.sb.append("&");
				this.sb.append(key);
				this.sb.append("=");
				this.sb.append(value);
				this.index++;
			}
			
			public String getResult() {
				return index <=0 ? mResult : sb.toString();
			}

			public StringBuilder getSb() {
				return sb;
			}
		}
	}
	

	public interface IResumeMedis{
		public String insert(InsertParam param) throws Exception;
		public String get(GetParam param) throws Exception;
	}
	
	public static abstract class ReferensiProvider implements IReferensi{
		public abstract String getBaseURL();
		public String getUrl(Path path) {
			String result = null;
			result = getBaseURL() + path.getPath();
			return result;
		}
		
		public enum Path{
			JENIS_PELAYANAN("/resume/load_ref_jenis_pelayanan"),
			KEADAAN_KELUAR("/resume/load_ref_cara_keluar"),
			FASKES("/resume/load_ref_rs"),
			CARA_KELUAR("/resume/load_ref_cara_keluar"),
			INSTALASI("/resume/load_ref_instalasi");
			private String path;

			Path(String path){
				this.path = path;
			}

			public String getPath() {
				return path;
			}

			public void setPath(String path) {
				this.path = path;
			}
		}
	}
	
	public interface IReferensi{
		public String DataUmum(ReferensiProvider.Path path)throws Exception;
	}
	
	private static ResumeMedisProvider resumeMedisInstance = new com.rsmurniteguh.webservice.dep.biz.kemenkes.Service.ResumeMedisProvider() 
	{

		@Override
		public String insert(InsertParam param) throws Exception {
			String url = getUrl(Path.Insert);
			return Request(url, Utils.toBytes_UTF8(param));
		}

		@Override
		public String get(GetParam param) throws Exception {
			
			String url = getUrl(Path.Get);
			return Request(url, Utils.toBytes_UTF8(param));
		}

		@Override
		public String getBaseURL() {
			return BASEURL;
		}
		
	};
	
	private static ReferensiProvider referensiInstance = new ReferensiProvider() {

		@Override
		public String getBaseURL() {
			return BASEURL;
		}

		@Override
		public String DataUmum(Path path) throws Exception {
			String url = getUrl(path);
			return Request(url, RequestMethod.POST);
		}
		
	};
	
	public static IResumeMedis ResumeMedis = (IResumeMedis) resumeMedisInstance;
	
	public static IReferensi Referensi = (IReferensi) referensiInstance;
}
