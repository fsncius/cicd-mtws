package com.rsmurniteguh.webservice.dep.biz;

import com.rsmurniteguh.webservice.dep.all.model.AntrianDispance;
import com.rsmurniteguh.webservice.dep.all.model.CurrentDatetim;
import com.rsmurniteguh.webservice.dep.all.model.Dispensedetailinfo;
import com.rsmurniteguh.webservice.dep.all.model.DrugDispense;
import com.rsmurniteguh.webservice.dep.all.model.InfoCekRem;
import com.rsmurniteguh.webservice.dep.all.model.InfoRekonObat;
import com.rsmurniteguh.webservice.dep.all.model.InfoRekonObatForCat;
import com.rsmurniteguh.webservice.dep.all.model.Infostockreceipt;
import com.rsmurniteguh.webservice.dep.all.model.ListDispanceCount;
import com.rsmurniteguh.webservice.dep.all.model.ListGetHasBPJS;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoBalance;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoCek;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoKaryawan;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoTransaksi;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoViewTrx;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoViewTrxNew;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoViewTrxNewDetail;
import com.rsmurniteguh.webservice.dep.all.model.ListKamar;
import com.rsmurniteguh.webservice.dep.all.model.ListgetPharPulvis;
import com.rsmurniteguh.webservice.dep.all.model.PrepareDatetime;
import com.rsmurniteguh.webservice.dep.all.model.ReturnResult;
import com.rsmurniteguh.webservice.dep.all.model.StoreDispance;
import com.rsmurniteguh.webservice.dep.all.model.StoreDispance2;
import com.rsmurniteguh.webservice.dep.all.model.VisitNoIp;
import com.rsmurniteguh.webservice.dep.all.model.viewJournal;
import com.rsmurniteguh.webservice.dep.all.model.viewJournal2;
import com.rsmurniteguh.webservice.dep.kthis.services.CareProviderSer;
import com.rsmurniteguh.webservice.dep.kthis.services.StoreDispanceSer;
import com.rsmurniteguh.webservice.dep.all.model.QueuePhar;

import java.math.BigDecimal;
import java.util.List;

public class DispanceBatchBiz{

	public List<DrugDispense> getdrugdispense(String drugdispense_id) {
		return StoreDispanceSer.getdrugdispense(drugdispense_id);
	}
	
	public List<DrugDispense> getdrugdispensevisit(String visit_id, String prepared_datetime) {
		return StoreDispanceSer.getdrugdispensevisit(visit_id, prepared_datetime);
	}
	
	public List<DrugDispense> updatedrugdispense(String drugdispense_id) {
		return StoreDispanceSer.updatedrugdispense(drugdispense_id);
	}
	
	public List<StoreDispance> getdispancebatch(String dispancebano) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getDispanceBatch(dispancebano);
	}

	public List<StoreDispance> getdispancebatch2(String dispancebano2, String patienttype, String patientclass) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getDispanceBatch2(dispancebano2, patienttype, patientclass);
	}

	public List<StoreDispance> getdrugstore(String drugreturnno, String patienttype, String patientclass) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getdrugstore(drugreturnno, patienttype, patientclass);
	}

	public List<StoreDispance2> getdispensebatchno(String dispensebatchno) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getdispensebatchno(dispensebatchno);
	}

	public List<AntrianDispance> getstoremstrid(String storemstrid) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getstoremstrid(storemstrid);
	}

	public List<Dispensedetailinfo> getdrugdispensedetailinfo(String dispensebatchno) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getdrugdispensedetailinfo(dispensebatchno);
	}
	
	public List<Dispensedetailinfo> getdrugdispensedetailinfo2(String dispensebatchno) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getdrugdispensedetailinfo2(dispensebatchno);
	}

	public List<CurrentDatetim> getCurrentDatetime() {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getCurrentDatetime();
	}

	public List<PrepareDatetime> getprepareddatetime(String firstdate, String lastdate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getprepareddatetime(firstdate,lastdate);
	}

	public Object getTxnDateTime(String first, String last, String codedesc, String codedesc2) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getTxnDateTime(first,last, codedesc, codedesc2);
	}
	
	public Object getEnterDateTime(String first, String last, String codedesc, String codedesc2) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getEnterDateTime(first,last, codedesc, codedesc2);
	}

	public List<InfoRekonObat> getrekonobat(String visitid) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getrekonobat(visitid);
	}

	public VisitNoIp getVisitNo(String mrn)
	{
		return StoreDispanceSer.getVisitNo(mrn);	
	}
	
	public List<InfoCekRem> getcekrem(String fromdate, String todate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getcekrem(fromdate, todate);
	}

	public List<Infostockreceipt> getstockreceipt(String fromdate, String todate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getstockreceipt(fromdate, todate);
	}

	public List<ListDispanceCount> getDispanceCount(String fromdate, String todate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getDispanceCount(fromdate, todate);
	}

	public List<ListgetPharPulvis> getPharPulvis(String fromdate, String todate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getPharPulvis(fromdate,todate);
	}

	public List<InfoRekonObatForCat> getrekonobatforcat(String visitid) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getrekonobatforcat(visitid);
	}
//	public List<ListgetDataSms> getDataSms(String nohp, String pesan, String keterangan, String status) {
//		// TODO Auto-generated method stub
//		return StoreDispanceSer.getDataSms(nohp,pesan,keterangan,status);
//	}

	public List<ListInfoViewTrx> getInfoViewTrx(String fromdate, String todate, String idnik) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getInfoViewTrx(fromdate,todate, idnik);
	}

	public List<ListInfoCek> getInfoCekSts(String cardno) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getInfoCekSts(cardno);
	}
	
	public List<ListInfoKaryawan>getInfoKaryawan(String idnik)
	{
		return StoreDispanceSer.getInfoKaryawan(idnik);
	}

	public List<ListGetHasBPJS> getHasBPJS(String patientid) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getHasBPJS(patientid);
	}

	public List<ListInfoViewTrx> getAllViewTrx(String fromdate, String todate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getAllViewTrx(fromdate,todate);
	}

	public  List<ListInfoKaryawan> getAllKaryawan() {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getAllKaryawan();
	}
	
	public List<ListKamar> getAllRoom(){
		return StoreDispanceSer.getAllRoom();		
	}
	
	public List<viewJournal> addApotekGl(String nopo,String total,String tipe){
		return StoreDispanceSer.addApotekGl(nopo,total,tipe);	
	}
	
	public List<viewJournal2> addApotekGlDetail(String nopo,String tglt,String nourt,String sandi,String kat,String nasup,String total,String ido,String tipe){
		return StoreDispanceSer.addApotekGlDetail(nopo,tglt,nourt,sandi,kat,nasup,total,ido,tipe);	
	}
	
	public List<viewJournal> addNilai(String sandi,String tgl,String nsp,String nopo,String ppn,String nppn,String dis,String tot,String nopr,String noref, String tipe){
		return StoreDispanceSer.addNilai(sandi,tgl,nsp,nopo,ppn,nppn,dis,tot,nopr,noref,tipe);	
	}
	
	public List<viewJournal> addHutang(String nobukti,String ids,String tgl,String tgljt,String user,String total, String tipe){
		return StoreDispanceSer.addHutang(nobukti,ids,tgl,tgljt,user,total,tipe);	
	}
	
	public List<viewJournal> addPiutang(String nobukti,String ids,String tgl,String user,String total, String tipe){
		return StoreDispanceSer.addPiutang(nobukti,ids,tgl,user,total,tipe);	
	}
	
	public List<viewJournal> addHutangDetail(String nobukti,String kategori,String sandi,BigDecimal qty,BigDecimal biaya, String tipe){
		return StoreDispanceSer.addHutangDetail(nobukti,kategori,sandi,qty,biaya,tipe);	
	}
	
	public List<viewJournal> addPiutangDetail(String nobukti,String kategori,String sandi,BigDecimal qty,BigDecimal biaya, String tipe){
		return StoreDispanceSer.addPiutangDetail(nobukti,kategori,sandi,qty,biaya,tipe);	
	}
	
	public List<viewJournal> addRtb(String nopo,String total,String tipe){
		return StoreDispanceSer.addRtb(nopo,total,tipe);	
	}
	public List<viewJournal> nilaiRtb(String nopo,String nsp,String tgl,String totalret,String ppn,String total,String kategori,String sandi,String tipe){
		return StoreDispanceSer.nilaiRtb(nopo,nsp,tgl,totalret,ppn,total,kategori,sandi,tipe);	
	}
	public List<viewJournal> addJpj(String idt,String total,String tipe,String user){
		return StoreDispanceSer.addJpj(idt,total,tipe,user);	
	}
	public List<viewJournal2> addJpjDetail(String idt,String tglt,String dis,String sandi,String nasup,String total,String tipe){
		return StoreDispanceSer.addJpjDetail(idt,tglt,dis,sandi,nasup,total,tipe);	
	}
	public List<viewJournal> addJpjNilai(String sandi,String tgl,String nsp,String idt,String tot,String kat, String tipe){
		return StoreDispanceSer.addJpjNilai(sandi,tgl,nsp,idt,tot,kat,tipe);	
	}
	
	
	public List<ListInfoBalance> getCekBalanceNik(String nik) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getCekBalanceNik(nik);
	}

	public List<ListInfoTransaksi> getCekTransaksiNik(String accountBlcId) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getCekTransaksiNik(accountBlcId);
	}

	public Object getDataPasienSms() {
		return StoreDispanceSer.getDataPasienSms();	
	}

	public List<ListInfoViewTrxNew> getInfoViewTrxNew(String IdCompany) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getInfoViewTrxNew(IdCompany);
	}

	public List<ListInfoViewTrxNewDetail> getInfoViewTrxNewDetail(String fromDate, String toDate) {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getInfoViewTrxNewDetail(fromDate, toDate);
	}

	public List<ListInfoViewTrxNew> getInfoViewTrxNewNoCompa() {
		// TODO Auto-generated method stub
		return StoreDispanceSer.getInfoViewTrxNewNoCompa();
	}
	
	public ReturnResult GetDelQueuePhar(QueuePhar DataPhar)
	{
		return StoreDispanceSer.GetDelQueuePhar(DataPhar);
	}

}
