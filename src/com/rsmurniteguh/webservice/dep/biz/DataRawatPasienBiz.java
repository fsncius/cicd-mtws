package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.infodatapasien;
import com.rsmurniteguh.webservice.dep.all.model.infopasien;
import com.rsmurniteguh.webservice.dep.all.model.infopasiendata;
import com.rsmurniteguh.webservice.dep.kthis.model.ProtokolHD;
import com.rsmurniteguh.webservice.dep.kthis.services.DataRawatPasienSer;
import com.rsmurniteguh.webservice.dep.kthis.view.ProtokolHdPartView;
import com.rsmurniteguh.webservice.dep.kthis.view.ProtokolHdView;

public class DataRawatPasienBiz {

	public List<infopasien> getDataRawatPasien(String cardno) {
		// TODO Auto-generated method stub
		return DataRawatPasienSer.getDataRawatPasien(cardno);
	}

	public List<infopasiendata> getinfopasien(String cardNo) {
		// TODO Auto-generated method stub
		return DataRawatPasienSer.getinfopasien(cardNo);
	}

	public List<infodatapasien> getdatapasien(String cardNo) {
		// TODO Auto-generated method stub
		return DataRawatPasienSer.getdatapasien(cardNo);
	}

	public ProtokolHdView getvisitprotokolhd(String visitid) {
		// TODO Auto-generated method stub
		return DataRawatPasienSer.getvisitprotokolhd(visitid);
	}

}
