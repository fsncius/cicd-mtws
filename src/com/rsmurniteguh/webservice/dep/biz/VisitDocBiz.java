package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.ListBedHistory;
import com.rsmurniteguh.webservice.dep.all.model.ListConDoc;
import com.rsmurniteguh.webservice.dep.all.model.ListDataFamily;
import com.rsmurniteguh.webservice.dep.all.model.ListDataRgPasien;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoData;
import com.rsmurniteguh.webservice.dep.all.model.PansienInfo;
import com.rsmurniteguh.webservice.dep.all.model.PasienDok;
import com.rsmurniteguh.webservice.dep.kthis.services.VisitDocService;

public class VisitDocBiz {

	public List<PasienDok> getVisitDoctor(String dadok) {
		// TODO Auto-generated method stub
		return VisitDocService.getVisitDoctor(dadok);
	}

	public List<PansienInfo> getpasieninfo(String mrn) {
		// TODO Auto-generated method stub
		return VisitDocService.getpasieninfo(mrn);
	}

	public List <ListVisPasDok> getinvispasdok(String mrnaja) {
		// TODO Auto-generated method stub
		return VisitDocService.getinvispasdok(mrnaja);
	}

	public List<ListConDoc> getConDoc(String tglcek) {
		// TODO Auto-generated method stub
		return VisitDocService.getConDoc(tglcek);
	}
	
	public List<ListInfoData> getInfoData(String mrn) {
		// TODO Auto-generated method stub
		return VisitDocService.getInfoData(mrn);
	}

	public List<ListBedHistory> getBedHistory(String visitid) {
		// TODO Auto-generated method stub
		return VisitDocService.getBedHistory(visitid);
	}

	public List<ListDataFamily> getListFamily(String personid) {
		// TODO Auto-generated method stub
		return VisitDocService.getListFamily(personid);
	}

	public List<ListDataRgPasien> getDataRgPasien(String mrn) {
		// TODO Auto-generated method stub
		return VisitDocService.getDataRgPasien(mrn);
	}

}
