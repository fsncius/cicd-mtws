package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.BaseInfo;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.CathlabAct;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.Diagnosis;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.DischargePlanning;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.EndoskopiAct;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.LocationMstr;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.MaterialReport;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.OutpatientCity;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.PatientVisit;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.Surgeryroom;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.SurgeryroomTime;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.UtilizationRadiology;
import com.rsmurniteguh.webservice.dep.kthis.services.DashboardSer;
import com.rsmurniteguh.webservice.dep.kthis.services.MtRegistrationService;

public class DashboardBiz {
	
    public static final String DEPARTMENT_BPJS = "01";
    public static final String DEPARTMENT_POLI = "02";	
    public final static String APPOINTMENT_STATUS_CONFIRM = "Confirm";
	public final static String APPOINTMENT_STATUS_REGISTERED = "Registered";
	public final static String APPOINTMENT_STATUS_CANCEL = "Cancel";
	public final static String APPOINTMENT_STATUS_WAIT = "Wait";
	public final static String APPOINTMENT_STATUS_DONE = "Done";
    public static final String MOBILE_SOURCE = "02";
    public static final String DESKTOP_SOURCE = "03";
    public static final String WEB_SOURCE = "01";
	
	public List<PatientVisit> patient_visit_report(String startdate, String enddate, String tipe)
	{
		return DashboardSer.patient_visit_report(startdate,enddate,tipe);
	}
	public List<PatientVisit> patient_visit_los(String startdate, String enddate, String tipe, String start, String end)
	{
		return DashboardSer.patient_visit_los(startdate,enddate,tipe,start,end);
	}
	public List<PatientVisit> patient_visit_over(String startdate, String enddate, String tipe)
	{
		return DashboardSer.patient_visit_over(startdate,enddate,tipe);
	}
	public List<PatientVisit> patient_visit_more(String startdate, String enddate, String tipe)
	{
		return DashboardSer.patient_visit_more(startdate,enddate,tipe);
	}
	
	public List<Diagnosis> diagnosa_report(String startdate, String enddate)
	{
		return DashboardSer.diagnosa_report(startdate,enddate);
	}
	
	public List<UtilizationRadiology> utilization_radiology_report(String startdate, String enddate)
	{
		return DashboardSer.utilization_radiology_report(startdate,enddate);
	}

	public List<Surgeryroom> utilization_surgeryroom_report(String ot_code, String ot_date) {
		return DashboardSer.utilization_surgeryroom_report(ot_code,ot_date);
	}

	public List<SurgeryroomTime> utilization_surgeryroom_time_report(String efektif, String startdate, String enddate) {
		return DashboardSer.utilization_surgeryroom_time_report(efektif,startdate,enddate);
	}

	public List<DischargePlanning> discharge_planning_report(String tanggal) {
		return DashboardSer.discharge_planning_report(tanggal);
	}
	
	public List<CathlabAct> cathlab_act_report(String startdate, String enddate) {
		return DashboardSer.cathlab_act_report(startdate,enddate);
	}
	
	public List<EndoskopiAct> endoskopi_act_report(String startdate, String enddate) {
		return DashboardSer.endoskopi_act_report(startdate,enddate);
	}
	
	public List<OutpatientCity> outpatient_city_report(String startdate, String enddate) {
		return DashboardSer.outpatient_city_report(startdate,enddate);
	}
	
	public List<MaterialReport> material_report(String type, String report) {
		return DashboardSer.material_report(type, report);
	}
	
	public List<LocationMstr> location() {
		return DashboardSer.location();
	}

	public Object radiology_schedule_report(String date, String type) {
		return DashboardSer.radiology_schedule_report(date, type);
	}

	public Object radiology_responsetime_report(String startdate, String enddate, String type) {
		return DashboardSer.radiology_responsetime_report(startdate, enddate, type);
	}

	public Object top_registered_appointment_report(String date) {
		return DashboardSer.top_registered_appointment_report(date);
	}

	public Object appointment_report(String date, String department) {
		return DashboardSer.appointment_report(date, department);
	}

	public Object inpatient_billing_report(String billing_id) {
		return DashboardSer.inpatient_billing_report(billing_id);
	}

	public Object top_operation_doctors(String startdate, String enddate) {
		return DashboardSer.top_operation_doctors(startdate,enddate);
	}
}

