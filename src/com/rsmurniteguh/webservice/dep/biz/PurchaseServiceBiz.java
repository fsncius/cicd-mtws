package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.PurchaseStatus;
import com.rsmurniteguh.webservice.dep.kthis.services.PurchaseService;


public class PurchaseServiceBiz  {
	
	public List<PurchaseStatus> AddPurchase(String purchaseNo)
	{
		return PurchaseService.AddPurchase(purchaseNo);
	}
	
	public List<PurchaseStatus> AddReceive(String receiptNo)
	{
		return PurchaseService.AddReceive(receiptNo);
	}
	
	public List<PurchaseStatus> AddReturn(String receiptNo)
	{
		return PurchaseService.AddReturn(receiptNo);
	}
	
	public List<PurchaseStatus> AddConsignment(String purchaseNo) {
		return PurchaseService.AddConsignment(purchaseNo);
	}
	
	public List<PurchaseStatus> UnpostPurchase(String purchaseNo) {
		return PurchaseService.UnpostPurchase(purchaseNo);
	}
	
	public List<PurchaseStatus> UnpostReceive(String receiptNo) {
		return PurchaseService.UnpostReceive(receiptNo);
	}
}
