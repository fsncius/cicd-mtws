package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import com.rsmurniteguh.webservice.dep.all.model.BaseInfo;
import com.rsmurniteguh.webservice.dep.all.model.DataPasien;
import com.rsmurniteguh.webservice.dep.all.model.JobsRecruitmentRegistration;
import com.rsmurniteguh.webservice.dep.all.model.RegistrasiNewPatient;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteBerita;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteGallery;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteMegazine;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteTahunGallery;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteWhatNew;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataUser;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.mobile.appointmentHistory;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegBpjs;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.RegKthis;
import com.rsmurniteguh.webservice.dep.kthis.services.MTWebsiteSer;
import com.rsmurniteguh.webservice.dep.kthis.services.MobileAppointment;
import com.rsmurniteguh.webservice.dep.kthis.services.MtRegistrationService;

public class MTWebsiteBiz {
	
	
	public ResponseString upload(String type, String name, List<Attachment> attachment, HttpServletRequest request) {
		return MTWebsiteSer.upload(type, name, attachment, request);
	}

	public Response download(String gambar_id, String type, HttpServletRequest request) {
		return MTWebsiteSer.download(gambar_id, type, request);
	}
	
	public List<WebsiteBerita> getBerita(String kategori, String type, String tags, String status) {
		return MTWebsiteSer.getBerita(kategori, type, tags,status);
	}
	
	public List<WebsiteBerita> getJudulBerita(String judul, String kategori) {
		return MTWebsiteSer.getJudulBerita(judul, kategori);
	}
	
	public ResponseStatus getSaveViewerBerita(String ids, String jumlah) {
		return MTWebsiteSer.getSaveViewerBerita(ids, jumlah); 
	}
	
	public List<WebsiteMegazine> getMegazine() {
		return MTWebsiteSer.getMegazine();
	}
	
	public ResponseStatus SaveReadOnline(String firstname, String lastname, String email, String phone){
		return MTWebsiteSer.SaveReadOnline(firstname,lastname,email,phone);
	}
	
	public ResponseStatus SaveKonsultasi(String fullname,String email, String phone, String gender, String blood, String height, String weight, String age, String jobs, String complaint, String spesialis){
		return MTWebsiteSer.SaveKonsultasi(fullname,email,phone,gender,blood,height,weight,age,jobs,complaint,spesialis);
	}
	
	public List<WebsiteWhatNew> getWhatNew(String code, String version, String type){
		return MTWebsiteSer.getWhatNew(code, version, type);
	}
	
	public List<WebsiteTahunGallery>getTahunGallery() {
		return MTWebsiteSer.getTahunGallery();
	}
	public List<WebsiteTahunGallery>getJenisGallery() {
		return MTWebsiteSer.getJenisGallery();
	}
	
	public List<WebsiteGallery>getGallerybyTahun(String tahun){
		return MTWebsiteSer.getGallerybyTahun(tahun);
	}

	public List<WebsiteGallery>getGallerybyJudul(String judul){
		return MTWebsiteSer.getGallerybyJudul(judul);
	}
	
	public ResponseString uploadRujukanPasien(List<Attachment> attachments, HttpServletRequest request) {
		return MobileAppointment.uploadRujukan(attachments, request);
	}
	
	public ResponseStatus addPasienBaru(RegistrasiNewPatient registrasiNewPatient) {
		return MTWebsiteSer.addPasienBaru(registrasiNewPatient);
	}
	
	public ResponseStatus addJobsRekruitment(JobsRecruitmentRegistration jobsRecruitmentRegistration) {
		return MTWebsiteSer.addJobsRekruitment(jobsRecruitmentRegistration);
	}
	
	public ResponseStatus addKodeKonfirmRekruitment(String idRegist, String kodeKonfirm, String param){
		return MTWebsiteSer.addKodeKonfirmRekruitment(idRegist,kodeKonfirm,param);
	}
	public ResponseStatus addKonfirmasiByEmail(String idRegist, String kodeKonfirm){
		return MTWebsiteSer.addKonfirmasiByEmail(idRegist,kodeKonfirm);
	}
	
	public ResponseStatus getKonfirmasiByIdRegist(String idRegist){
		return MTWebsiteSer.getKonfirmasiByIdRegist(idRegist);
	}
	public RegistrasiNewPatient getPasienBaru(String barcode){
		return MTWebsiteSer.getPasienBaru(barcode);
	}
	
	public BaseInfo getCheckPasienBpjsbyNoBpjs(String nobpjs)
	{
		return MTWebsiteSer.getCheckPasienBpjsbyNoBpjs(nobpjs);
	}
	
	public DataPasien getInfoPasienByNIKBPJS(String nomor, String tipe)
	{
		return MTWebsiteSer.getInfoPasienByNIKBPJS(nomor, tipe);
	}
	
	public ResponseStatus sendEmailPasienBaru(String regId){
		return MTWebsiteSer.sendEmailPasienBaru(regId);
	}
	
	public DataUser checkLoginPatientAntrian(String user, String pass) {
		return MTWebsiteSer.checkLoginPatientAntrian(user, pass);
	}

	public ResponseStatus checkUserPatientAntrian(String mrn, String ktp, String bpjs) {
		// TODO Auto-generated method stub
		return MTWebsiteSer.checkUserPatientAntrian(mrn, ktp, bpjs);
	}

	public ResponseStatus saveUserPatientAntrian(String mrn, String ktp, String bpjs, String username, String fullname, String contact, String email, String pass) {
		// TODO Auto-generated method stub
		return MTWebsiteSer.saveUserPatientAntrian(mrn, ktp, bpjs, username, fullname, contact, email, pass);
	}
	
	public DataUser getUserbyCard(String mrn, String ktp, String bpjs) {
		// TODO Auto-generated method stub
		return MTWebsiteSer.getUserbyCard(mrn, ktp, bpjs);
	}

	public ResponseStatus resetPasswordUser(String mrn, String userid, String pass, String username, String tipe) {
		// TODO Auto-generated method stub
		return MTWebsiteSer.resetPasswordUser(mrn, userid, pass, username,tipe);
	}
}
