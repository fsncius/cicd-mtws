package com.rsmurniteguh.webservice.dep.biz;

import java.text.ParseException;

import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.BpjsSepUpdateParam;
import com.rsmurniteguh.webservice.dep.all.model.PengajuanKlaim;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsService2;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsService2.BpjsInfoType;

public class BpjsServiceBiz2 {
	
	
	public String getBpjsInfobyNoka(String noKa) {
		// TODO Auto-generated method stub
		return BpjsService2.GetBpjsInfobyNoka(noKa);
	}
	

	public String GetBpjsInfobyNik(String nik) {
		// TODO Auto-generated method stub
		return BpjsService2.GetBpjsInfobyNik(nik);
	}
	
	public String insertSep(String noKa,String jnsPelayanan,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String poli,String noHp,String user) {
		// TODO Auto-generated method stub
		return BpjsService2.insertSep(noKa,jnsPelayanan,klsRawat,mrn,asalRujuk,tglRujuk,noRujuk,catatan,diagAwal,poli,noHp,user);
	}
	
	public String insertSep4(String noKa,String tglSep,String jnsPelayanan,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String poli,String noHp,String user) {
		// TODO Auto-generated method stub
		return BpjsService2.insertSep4(noKa,tglSep,jnsPelayanan,klsRawat,mrn,asalRujuk,tglRujuk,noRujuk,catatan,diagAwal,poli,noHp,user);
	}
	
	public String insertSep2(String noKa,String jnsPelayanan,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String poli,String noHp,String user) {
		// TODO Auto-generated method stub
		return BpjsService2.insertSep2(noKa,jnsPelayanan,klsRawat,mrn,asalRujuk,tglRujuk,noRujuk,catatan,diagAwal,poli,noHp,user);
	}
	
	public String detailSep(String noSep){
		// TODO Auto-generated method stub
		return BpjsService2.detailSep(noSep);
	}
	
	public String hapusSep(String noSep,String user){
		// TODO Auto-generated method stub
		return BpjsService2.hapusSep(noSep,user);
	}
	
	public String updateSep(String noSep,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String noHp,String user) {
		// TODO Auto-generated method stub
		return BpjsService2.updateSep(noSep,klsRawat,mrn,asalRujuk,tglRujuk,noRujuk,catatan,diagAwal,noHp,user);
	}
	
	public String pengajuanSep(String noKa,String tglPelayanan,String jnsPelayanan,String keterangan,String user){
		// TODO Auto-generated method stub
		return BpjsService2.pengajuanSep(noKa,tglPelayanan,jnsPelayanan,keterangan,user);
	}
	
	public String approvalSep(String noKa,String tglPelayanan,String jnsPelayanan,String keterangan,String user){
		// TODO Auto-generated method stub
		return BpjsService2.approvalSep(noKa,tglPelayanan,jnsPelayanan,keterangan,user);
	}
	
	public String updateTglPlg(String noSep,String tglPulang,String user){
		// TODO Auto-generated method stub
		return BpjsService2.updateTglPlg(noSep,tglPulang,user);
	}
	
	public String integrasiInacbg(String noSep){
		// TODO Auto-generated method stub
		return BpjsService2.integrasiInacbg(noSep);
	}
	
	public String insertRujukan(String noSep,String ppkDirujuk,String jnsPelayanan,String catatan,String diagRujukan,String tipeRujukan,String poliRujukan,String user){
		// TODO Auto-generated method stub
		return BpjsService2.insertRujukan(noSep,ppkDirujuk,jnsPelayanan,catatan,diagRujukan,tipeRujukan,poliRujukan,user);
	}
	
	public String updateRujukan(String noRujukan,String ppkDirujuk,String tipe,String jnsPelayanan,String catatan,String diagRujukan,String tipeRujukan,String poliRujukan,String user){
		// TODO Auto-generated method stub
		return BpjsService2.updateRujukan(noRujukan,ppkDirujuk,tipe,jnsPelayanan,catatan,diagRujukan,tipeRujukan,poliRujukan,user);
	}
	
	public String deleteRujukan(String noRujukan,String user){
		// TODO Auto-generated method stub
		return BpjsService2.deleteRujukan(noRujukan,user);
	}
	
	public String dataKunjungan(String tglSep,String jnsPelayanan){
		// TODO Auto-generated method stub
		return BpjsService2.dataKunjungan(tglSep,jnsPelayanan);
	}
	
	public String dataFaskes(String tipe){
		// TODO Auto-generated method stub
		return BpjsService2.dataFaskes(tipe);
	}
	
	public String dataFaskes(String kodeRS, String tipe){
		return BpjsService2.dataFaskes(kodeRS, tipe);
	}
	
	public String diagnosa(String diag){
		// TODO Auto-generated method stub
		return BpjsService2.diagnosa(diag);
	}
	
	public String dataKlaim(String tglPulang,String jnsPelayanan,String status) {
		// TODO Auto-generated method stub
		return BpjsService2.dataKlaim(tglPulang,jnsPelayanan,status);
	}
	
	public String insertLpk(PengajuanKlaim pengajuanKlaim) {
		// TODO Auto-generated method stub
		return BpjsService2.insertLpk(pengajuanKlaim);
	}
	
	public String insertSep3(
			String noKa,
			String jnsPelayanan,
			String klsRawat,
			String mrn,
			String asalRujukan,
			String tglRujukan,
			String noRujukan,
			String asalPerujuk,
			String catatan,
			String diagAwal,
			String poli,
			String Eks,
			String lakaLantas,
			String penjamin,
			String tglKejadian,
			String ket,
			String suplesi,
			String noSuplesi,
			String kdPro,
			String kdKab,
			String kdKec,
			String noSurat,
			String dpjp,
			String noHp,
			String user) {
		// TODO Auto-generated method stub
		return BpjsService2.insertSep3(noKa,jnsPelayanan,klsRawat,mrn,asalRujukan,tglRujukan,noRujukan,asalPerujuk,catatan,diagAwal,poli,Eks,lakaLantas,penjamin,tglKejadian,ket,suplesi,noSuplesi,kdPro,kdKab,kdKec,noSurat,dpjp,noHp,user);
	}
	
	public String dpjp(
			String jnsPelayanan,
			String spesialis,
			String tanggal) {
		// TODO Auto-generated method stub
		return BpjsService2.dpjp(jnsPelayanan,spesialis, tanggal);
	}
	
	
	/* ---------- Andy Three S ----*/

	public String getRujukanbyNmrRujukan(String nmrRujukan) {
		// TODO Auto-generated method stub
		return BpjsService2.getRujukanbyNmrRujukan(nmrRujukan);
	}
	
	public String getRujukanRSbyNmrRujukan(String nmrRujukan) {
		// TODO Auto-generated method stub
		return BpjsService2.getRujukanRSbyNmrRujukan(nmrRujukan);
	}
	
	public String getRujukanbyNoka(String noKa) {
		// TODO Auto-generated method stub
		return BpjsService2.getRujukanbyNoka(noKa);
	}
	
	public String getRujukanRSbyNoka(String noKa) {
		// TODO Auto-generated method stub
		return BpjsService2.getRujukanRSbyNoka(noKa);
	}
	
	public String getRujukanPcareorRSbyNoka(String noKa) {
		return BpjsService2.getRujukanPcareorRSbyNoka(noKa);
	}
	
	public String getRujukanListbyNoka(String noKa) {
		// TODO Auto-generated method stub
		return BpjsService2.getRujukanListbyNoka(noKa);
	}
	
	public String getRujukanRSListbyNoka(String noKa) {
		// TODO Auto-generated method stub
		return BpjsService2.getRujukanRSListbyNoka(noKa);
	}
		
	public String getRujukanbyTglRujukan(String tglRujukan) throws ParseException {
		// TODO Auto-generated method stub
		return BpjsService2.getRujukanbyTglRujukan(tglRujukan);
	}
	
	public String getRujukanRSbyTglRujukan(String tglRujukan) throws ParseException {
		// TODO Auto-generated method stub
		return BpjsService2.getRujukanRSbyTglRujukan(tglRujukan);
	}

	public String getReferensiProcedure(String kode, String nama){
		// TODO Auto-generated method stub
		return BpjsService2.getReferensiProcedure(kode, nama);
	}
	
	public String getReferensiKelasRawat(){
		// TODO Auto-generated method stub
		return BpjsService2.getReferensiKelasRawat();
	}
	
	public String getReferensiDokter(String namaDokter){
		// TODO Auto-generated method stub
		return BpjsService2.getReferensiDokter(namaDokter);
	}
	
	public String getReferensiSpesialistik() {
		return BpjsService2.getReferensiSpesialistik();
	}
	public String getReferensiRuangRawat() {
		return BpjsService2.getReferensiRuangRawat();
	}
	public String getReferensiCaraKeluar() {
		return BpjsService2.getReferensiCaraKeluar();
	}
	public String getReferensiPascaPulang() {
		return BpjsService2.getReferensiPascaPulang();
	}
	public String getReferensiPropinsi() {
		return BpjsService2.getReferensiPropinsi();
	}
	public String getReferensiKabupaten(String kodePropinsi) {
		return BpjsService2.getReferensiKabupaten(kodePropinsi);
	}
	public String getReferensiKecamatan(String kodeKabupaten) {
		return BpjsService2.getReferensiKecamatan(kodeKabupaten);
	}
	public String getSuplesiJasaRaharja(String noBPJS,String tglSEP) throws ParseException {
		return BpjsService2.getSuplesiJasaRaharja(noBPJS,tglSEP);
	}


	public Object getBpjsInfobyType(String type, String target) {
		return BpjsService2.GetBpjsInfobyType(BpjsInfoType.from(type), target);
	}


	public Object updateSep2(BpjsSepUpdateParam param) {
		return BpjsService2.updateSep2(param);
	}


	public Object getReferensiPoli(String codeOrName) {
		return BpjsService2.getReferensiPoli(codeOrName);
	}
}
