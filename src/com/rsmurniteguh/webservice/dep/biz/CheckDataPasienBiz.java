package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.DataPasien;
import com.rsmurniteguh.webservice.dep.all.model.InfoUserNameKthis;
import com.rsmurniteguh.webservice.dep.all.model.InfoVisitPasien;
import com.rsmurniteguh.webservice.dep.all.model.Listcekdiagnos;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.NewPatient;
import com.rsmurniteguh.webservice.dep.kthis.model.DropDownBo;
import com.rsmurniteguh.webservice.dep.kthis.services.CheckDataPasienSer;
import com.rsmurniteguh.webservice.dep.kthis.view.RegisterOption;

public class CheckDataPasienBiz {

	public List<DataPasien> getCheckDataPasien(String ipmrn) {
		return CheckDataPasienSer.getCheckDataPasien(ipmrn);
	}

	
	public List<DataPasien> getCheckDataPasienByBpjs(String bpjs) {
		return CheckDataPasienSer.getCheckDataPasienByBpjs(bpjs);
	}
	
	public Object getVisitDiagnosisDetailId(String visitdiagnosadetailid) {
		return CheckDataPasienSer.getVisitDiagnosisDetailId(visitdiagnosadetailid);
	}

	public List<InfoVisitPasien> getInfoVisitPasien(String VisitId) {
		return CheckDataPasienSer.getInfoVisitPasien(VisitId);
	}

	public List<InfoUserNameKthis> getInfoUserKthis(String userMstrId) {
		// TODO Auto-generated method stub
		return CheckDataPasienSer.getInfoUserKthis(userMstrId);
	}


	public List<Listcekdiagnos> getcekdiagnos(String ipmrn) {
		return CheckDataPasienSer.getcekdiagnos(ipmrn);
	}

	public static RegisterOption getRegistrationOption(){
		return CheckDataPasienSer.getRegistrationOption();
	}
	
	public static List<DropDownBo> getListDropDownCityMstr(String stateMstrId){
		return CheckDataPasienSer.getListDropDownCityMstr(stateMstrId);
	}
	
	public static List<DataPasien> getListPasienByNameAndDate(String patientName, String birthDate) {
		return CheckDataPasienSer.getListPasienByNameAndDate(patientName, birthDate); 
	}
	
	public List<DataPasien> getDataPasienByBpjs(String bpjs) {
		return CheckDataPasienSer.getDataPasienByBpjs(bpjs);
	}
	
	public static BaseResult registerNewPatient(NewPatient newPatient){
		return CheckDataPasienSer.registerNewPatient(newPatient);
	}
}
