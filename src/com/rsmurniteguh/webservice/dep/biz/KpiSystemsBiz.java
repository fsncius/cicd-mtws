package com.rsmurniteguh.webservice.dep.biz;

import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.DataPasien;
import com.rsmurniteguh.webservice.dep.all.model.ListKpiQuer1;
import com.rsmurniteguh.webservice.dep.all.model.ListKpiQuer2;
import com.rsmurniteguh.webservice.dep.all.model.ListViewJenisKamar;
import com.rsmurniteguh.webservice.dep.all.model.ListViewKunjungan;
import com.rsmurniteguh.webservice.dep.all.model.ListViewPembiayaan;
import com.rsmurniteguh.webservice.dep.all.model.ListViewWaktuLayanIGD;
import com.rsmurniteguh.webservice.dep.kthis.services.CheckDataPasienSer;
import com.rsmurniteguh.webservice.dep.kthis.services.KpiSystemSer;

public class KpiSystemsBiz {

	public List<ListKpiQuer1> getKpiQuer1() {
		// TODO Auto-generated method stub
		return KpiSystemSer.getKpiQuer1();
	}
	
	public List<ListKpiQuer2> getKpiQuer2() {
		// TODO Auto-generated method stub
		return KpiSystemSer.getKpiQuer2();
	}
	
	
	public List<ListViewJenisKamar> getViewJenisKamar(){
		return KpiSystemSer.getViewJenisKamar();		
	}
	
	public List<ListViewKunjungan> getViewKunjungan(){
		return KpiSystemSer.getViewKunjungan();		
	}
	
	public List<ListViewPembiayaan> getViewPembiayaan(){
		return KpiSystemSer.getViewPembiayaan();		
	}
	
	public List<ListViewWaktuLayanIGD> getWaktuLayanIGD(){
		return KpiSystemSer.getWaktuLayanIGD();		
	}
}
