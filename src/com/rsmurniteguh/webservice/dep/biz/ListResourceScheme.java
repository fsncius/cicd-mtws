package com.rsmurniteguh.webservice.dep.biz;

public class ListResourceScheme {
	private String RESOURCESCHEME_ID;
	private String RESOURCEMSTR_ID;
	private String DATE_SCHEDULE;
	private String SESSION_DESC;
	public String getRESOURCESCHEME_ID() {
		return RESOURCESCHEME_ID;
	}
	public void setRESOURCESCHEME_ID(String rESOURCESCHEME_ID) {
		RESOURCESCHEME_ID = rESOURCESCHEME_ID;
	}
	public String getRESOURCEMSTR_ID() {
		return RESOURCEMSTR_ID;
	}
	public void setRESOURCEMSTR_ID(String rESOURCEMSTR_ID) {
		RESOURCEMSTR_ID = rESOURCEMSTR_ID;
	}
	public String getDATE_SCHEDULE() {
		return DATE_SCHEDULE;
	}
	public void setDATE_SCHEDULE(String dATE_SCHEDULE) {
		DATE_SCHEDULE = dATE_SCHEDULE;
	}
	public String getSESSION_DESC() {
		return SESSION_DESC;
	}
	public void setSESSION_DESC(String sESSION_DESC) {
		SESSION_DESC = sESSION_DESC;
	}
	
}
