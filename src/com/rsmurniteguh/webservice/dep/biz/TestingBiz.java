package com.rsmurniteguh.webservice.dep.biz;


import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.ReturnResult;
import com.rsmurniteguh.webservice.dep.all.model.TestModel;
import com.rsmurniteguh.webservice.dep.kthis.services.AuthenticatorService;
import com.rsmurniteguh.webservice.dep.kthis.services.TestingService;
import com.rsmurniteguh.webservice.dep.kthis.services.email.TestingEmail;;

public class TestingBiz {

	public ReturnResult Authenticate(TestModel test) {
		return TestingService.save(test);
	}
	
	public BaseResult testEmail(){
		return TestingService.testEmail();
	}
}
