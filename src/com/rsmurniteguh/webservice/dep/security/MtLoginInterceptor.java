package com.rsmurniteguh.webservice.dep.security;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.login.Configuration;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.cxf.common.logging.LogUtils;
import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.security.AuthenticationException;
import org.apache.cxf.interceptor.security.DefaultSecurityContext;
import org.apache.cxf.interceptor.security.NamePasswordCallbackHandler;
import org.apache.cxf.interceptor.security.RolePrefixSecurityContextImpl;
import org.apache.cxf.interceptor.security.callback.CallbackHandlerProvider;
import org.apache.cxf.interceptor.security.callback.CallbackHandlerProviderAuthPol;
import org.apache.cxf.interceptor.security.callback.CallbackHandlerProviderUsernameToken;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.security.SecurityContext;

import com.rsmurniteguh.webservice.dep.util.DataUtils;

public class MtLoginInterceptor extends AbstractPhaseInterceptor<Message>  {
	public static final String ROLE_CLASSIFIER_PREFIX = "prefix";
	public static final String ROLE_CLASSIFIER_CLASS_NAME = "classname";

	private static final Logger LOG = LogUtils.getL7dLogger(StaffLoginInterceptor.class);

	private String contextName = "";
	private Configuration loginConfig;
	private String roleClassifier;
	private String roleClassifierType = ROLE_CLASSIFIER_PREFIX;
	private boolean reportFault;
	private boolean useDoAs = true;
	private List<CallbackHandlerProvider> callbackHandlerProviders;
	private boolean allowAnonymous = true;
	private String ServiceName = "";

	public MtLoginInterceptor() {
		this(Phase.UNMARSHAL);
	}

	public MtLoginInterceptor(String phase) {
		super(phase);
		this.callbackHandlerProviders = new ArrayList<CallbackHandlerProvider>();
		this.callbackHandlerProviders.add(new CallbackHandlerProviderAuthPol());
		this.callbackHandlerProviders.add(new CallbackHandlerProviderUsernameToken());
	}

	public void setContextName(String name) {
		contextName = name;
	}

	public String getContextName() {
		return contextName;
	}

	@Deprecated
	public void setRolePrefix(String name) {
		setRoleClassifier(name);
	}

	public void setRoleClassifier(String value) {
		roleClassifier = value;
	}

	public String getRoleClassifier() {
		return roleClassifier;
	}

	public void setRoleClassifierType(String value) {
		if (!ROLE_CLASSIFIER_PREFIX.equals(value) && !ROLE_CLASSIFIER_CLASS_NAME.equals(value)) {
			throw new IllegalArgumentException("Unsupported role classifier");
		}
		roleClassifierType = value;
	}

	public String getRoleClassifierType() {
		return roleClassifierType;
	}

	public void setReportFault(boolean reportFault) {
		this.reportFault = reportFault;
	}

	public void setUseDoAs(boolean useDoAs) {
		this.useDoAs = useDoAs;
	}

	private CallbackHandler getFirstCallbackHandler(Message message) {
		for (CallbackHandlerProvider cbp : callbackHandlerProviders) {
			CallbackHandler cbh = cbp.create(message);
			if (cbh != null) {
				return cbh;
			}
		}
		return null;
	}

	@Override
	public void handleMessage(final Message message) throws Fault {
		CallbackHandler handler = getFirstCallbackHandler(message);
		
		if (handler == null && !allowAnonymous) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}

		try {
			AuthorizationPolicy policy = message.get(AuthorizationPolicy.class);
			String username = policy.getUserName();
			String password = policy.getPassword();
			String service = this.getServiceName();
			
			if (!DataUtils.MurniteguhInterceptor(username, password)) {
				throw new WebApplicationException(Response.Status.UNAUTHORIZED);
			}
		} catch (NullPointerException ex) {
				throw new WebApplicationException("Not Logged",418);
		} catch (WebApplicationException e) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
	}

	public String getServiceName() {
		return ServiceName;
	}

	public void setServiceName(String serviceName) {
		ServiceName = serviceName;
	}

	private String getUsername(CallbackHandler handler) {
		if (handler == null) {
			return null;
		}
		try {
			NameCallback usernameCallBack = new NameCallback("user");
			handler.handle(new Callback[] { usernameCallBack });
			return usernameCallBack.getName();
		} catch (Exception e) {
			return null;
		}
	}

	protected CallbackHandler getCallbackHandler(String name, String password) {
		return new NamePasswordCallbackHandler(name, password);
	}

	protected SecurityContext createSecurityContext(String name, Subject subject) {
		if (getRoleClassifier() != null) {
			return new RolePrefixSecurityContextImpl(subject, getRoleClassifier(), getRoleClassifierType());
		} else {
			return new DefaultSecurityContext(name, subject);
		}
	}

	public Configuration getLoginConfig() {
		return loginConfig;
	}

	public void setLoginConfig(Configuration loginConfig) {
		this.loginConfig = loginConfig;
	}

	public List<CallbackHandlerProvider> getCallbackHandlerProviders() {
		return callbackHandlerProviders;
	}

	public void setCallbackHandlerProviders(List<CallbackHandlerProvider> callbackHandlerProviders) {
		this.callbackHandlerProviders.clear();
		this.callbackHandlerProviders.addAll(callbackHandlerProviders);
	}

	public void addCallbackHandlerProviders(List<CallbackHandlerProvider> callbackHandlerProviders2) {
		this.callbackHandlerProviders.addAll(callbackHandlerProviders2);
	}

	public void setAllowAnonymous(boolean allowAnonymous) {
		this.allowAnonymous = allowAnonymous;
	}
}
