package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.apache.http.util.TextUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.gym.AccountBalance;
import com.rsmurniteguh.webservice.dep.all.model.gym.AccountBalanceParam;
import com.rsmurniteguh.webservice.dep.all.model.gym.CreateMember;
import com.rsmurniteguh.webservice.dep.biz.MtGymBiz;
import com.rsmurniteguh.webservice.dep.services.IMtGym;

public class MtGymService implements IMtGym {
	@Autowired
	private MtGymBiz mtGymBiz;

	@Override
	public Response CheckLogin(String user, String pass) {
		return Response.ok(mtGymBiz.CheckLogin(user,pass)).build();
	}
	
	@Override
	public Response SaveRegistrationToken(String usermstrId, String token) {
		return Response.ok(mtGymBiz.SaveRegistrationToken(usermstrId, token)).build();
	}
	
	@Override
	public Response info_account_from_cardno(String cardno) {
		return Response.ok(mtGymBiz.info_account_from_cardno(cardno)).build();
	}

	@Override
	public Response update_account(AccountBalance account_balance) {
		return Response.ok(mtGymBiz.update_account(account_balance)).build();
	}
	
	@Override
	public Response info_member(String nfcno) {
		return Response.ok(mtGymBiz.info_member(nfcno)).build();
	}

	@Override
	public Response create_member(CreateMember create_member) {
		return Response.ok(mtGymBiz.create_member(create_member)).build();
	}

	@Override
	public Response topup_account(String total, String nfcno, String created_by) {
		return Response.ok(mtGymBiz.topup_account(total,nfcno,created_by)).build();
	}
	
	@Override
	public Response member() {
		return Response.ok(mtGymBiz.member()).build();
	}
	
	@Override
	public Response cancel_topup_account(String invoice_no, String created_by) {
		return Response.ok(mtGymBiz.cancel_topup_account(invoice_no,created_by)).build();
	}	@Override
	
	public Response trx(String date, String type) {
		return Response.ok(mtGymBiz.trx(date,type)).build();
	}
	
	public Response get_rfid(String machineNo) {
		return Response.ok(mtGymBiz.get_rfid(machineNo)).build();
	}

	@Override
	public Response absensi(String accountBalanceId, String memberShipId, String machineCode) {
		return Response.ok(mtGymBiz.absensi(accountBalanceId, memberShipId, machineCode)).build();
	}

	@Override
	public Response info_account_from_old_card(String machineNo) {
		return Response.ok(mtGymBiz.info_account_from_old_card(machineNo)).build();
	}

	@Override
	public Response create_account(AccountBalanceParam accountBalanceParam) {
		if(accountBalanceParam == null){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mtGymBiz.create_account(accountBalanceParam)).build();
	}

}
