package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.apache.http.util.TextUtils;

import com.rsmurniteguh.webservice.dep.all.model.bpjs.RegisterSepParam;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsIntegration;
import com.rsmurniteguh.webservice.dep.services.IBpjsIntegrationService;

public class BpjsIntegrationService implements IBpjsIntegrationService{

	@Override
	public Response RegisterSep(RegisterSepParam param) {
		if(param == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(BpjsIntegration.SEP.Register(param)).build(); 
	}

	@Override
	public Response deleteLocalRujukanTerbaru(String mrn, String noKa) {
		if(TextUtils.isEmpty(mrn) || TextUtils.isEmpty(noKa)) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(BpjsIntegration.Rujukan.deleteLocalRujukanTerbaru(mrn, noKa)).build();
	}

}
