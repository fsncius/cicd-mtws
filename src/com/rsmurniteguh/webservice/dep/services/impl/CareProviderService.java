package com.rsmurniteguh.webservice.dep.services.impl;


import java.math.BigDecimal;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.CareProvider;
import com.rsmurniteguh.webservice.dep.biz.CareProviderBiz;
import com.rsmurniteguh.webservice.dep.services.ICareProvider;

public class CareProviderService implements ICareProvider {
	@Autowired
	private CareProviderBiz CareProviderBiz;
	
	@Override
	public Response getcareprovider(String caretype)
	{
		if(caretype==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CareProviderBiz.getCareProvider(caretype)).build();
	}

	@Override
	public Response sendcareprovider(CareProvider b1) {
		// TODO Auto-generated method stub
		return Response.ok(b1).build();
	}


	@Override
	public Response getdoctoruser()
	{
		return Response.ok(CareProviderBiz.getdoctoruser()).build();
	}
	
	@Override
	public Response getcekdispas(String visitid)
	{
		if(visitid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CareProviderBiz.getcekdispas(visitid)).build();
	}
	
	@Override
	public Response getquenum(String queueno)
	{
		return Response.ok(CareProviderBiz.getquenum(queueno)).build();
	}
	
	@Override
	public Response getspecialquenum(String queueno)
	{
		return Response.ok(CareProviderBiz.getspecialquenum(queueno)).build();
	}
	
	public Response getspecialquenum2(String queueno, String subspeciality)
	{
		return Response.ok(CareProviderBiz.getspecialquenum2(queueno,subspeciality)).build();
	}

	@Override
	public Response getDoctorQueueList(String careproviderId, String subspecialtymstr_id) {
		if(careproviderId==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CareProviderBiz.getDoctorQueueList(careproviderId, subspecialtymstr_id)).build();
	}

	@Override
	public Response getCareprovidersByUsermstr(String users) {
		if(users==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CareProviderBiz.getCareprovidersByUsermstr(users)).build();
	}

	@Override
	public Response getDoctorQueueListByUsermstrId(String usermstrId, String subspecialtymstr_id) {
		if(usermstrId==null || subspecialtymstr_id == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CareProviderBiz.getDoctorQueueListByUsermstrId(usermstrId, subspecialtymstr_id)).build();
	}

	@Override
	public Response getIsDoctorByUserMstrId(String userMstrId) {
		if(userMstrId==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CareProviderBiz.getIsDoctorByUserMstrId(userMstrId)).build();
	}

	@Override
	public Response syncSIPDoctor(String careProviderId) {
		if(careProviderId==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CareProviderBiz.syncSIPDoctor(careProviderId)).build();
	}
	
	@Override
	public Response syncSIPDoctorAll() {
		return Response.ok(CareProviderBiz.syncSIPDoctorAll()).build();
	}
}
