package com.rsmurniteguh.webservice.dep.services.impl;

import java.math.BigDecimal;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.BpjsServiceBiz;
import com.rsmurniteguh.webservice.dep.services.IBpjsService;

public class BpjsService implements IBpjsService{
	@Autowired
	private BpjsServiceBiz bpjsSeriveBiz;
	@Override
	public Response GetBpjsMemberInfo(String cardNo) {
		return Response.ok(bpjsSeriveBiz.getBpjsByCardNo(cardNo)).build();
	}
	
	public Response GetBpjsMemberOnline(String cardNo) {
		return Response.ok(bpjsSeriveBiz.getBpjsOnlineCardNo(cardNo)).build();
	}
	public Response GetBpjsNikInfo(String Nik) {
		return Response.ok(bpjsSeriveBiz.getBpjsByNik(Nik)).build();
	}
	
	public Response GetBpjsSepInfo(String cardNo) {
		return Response.ok(bpjsSeriveBiz.getBpjsSep(cardNo)).build();
	}
	public Response GetBpjsDataRujukan(String noRujuk) {
		return Response.ok(bpjsSeriveBiz.getBpjsRujukanData(noRujuk)).build();
	}
	
	public Response GetBpjsTanggalRujukan(String Tanggal) {
		return Response.ok(bpjsSeriveBiz.getBpjsRujukanTanggal(Tanggal)).build();
	}
	
	public Response GetBpjsDataRujukanNomorKartu(String cardNo) {
		return Response.ok(bpjsSeriveBiz.getBpjsRujukanNomorKartu(cardNo)).build();
	}
	
	public Response GetBpjsRujukanFktl(String cardNo) {
		return Response.ok(bpjsSeriveBiz.getBpjsRujukanFktlNomorKartu(cardNo)).build();
	}
	
	public Response GetBpjsTanggalRujukanFktl(String Tanggal) {
		return Response.ok(bpjsSeriveBiz.getBpjsRujukanFktlTanggal(Tanggal)).build();
	}
	
	public Response GetBpjsNomorRujukanFktl(String noRujuk) {
		return Response.ok(bpjsSeriveBiz.getBpjsRujukanFktlNoRujuk(noRujuk)).build();
	}
	
	public Response GetBpjsDetailSep(String noSep){
		return Response.ok(bpjsSeriveBiz.getBpjsDetailNoSep(noSep)).build();
	}
	
	public Response GetRujukanRs(String noRujuk){
		return Response.ok(bpjsSeriveBiz.getBpjsDetailRujukanRs(noRujuk)).build();
	}
	
	public Response GetRujukanKartu(String noKartu){
		return Response.ok(bpjsSeriveBiz.getBpjsDetailRujukanKartu(noKartu)).build();
	}
	
	public Response GetCbgTarif(String noSep){
		return Response.ok(bpjsSeriveBiz.getBpjsTarifCbg(noSep)).build();
	}
	
	
	
	public Response GetBpjsDiagnosa(String Keyword) {
		return Response.ok(bpjsSeriveBiz.getBpjsDiagnosaKey(Keyword)).build();
	}
	
	public Response GetBpjsProsedurCbg(String Keyword) {
		return Response.ok(bpjsSeriveBiz.getBpjsProsedurCbgKey(Keyword)).build();
	}
	
	public Response GetBpjsDataCmg(String Keyword) {
		return Response.ok(bpjsSeriveBiz.getBpjsDataCmgKey(Keyword)).build();
	}
	
	public Response GetBpjsVerifikasiKlaim(
			String TglMasuk, 
			String TglKeluar, 
			String KlsRawat, 
			String Kasus, 
			String Cari , 
			String Status ) {
		return Response.ok(bpjsSeriveBiz.getBpjsDataVerifikasi(TglMasuk,TglKeluar,KlsRawat,Kasus,Cari,Status)).build();
	}
	
	public Response GetBpjsLaporanSep(String noSep) {
		return Response.ok(bpjsSeriveBiz.getBpjsLaporanNoSep(noSep)).build();
	}
	
	public Response CreateSep(
			String noKartu,
			String tglSep,
			String tglRujukan,
			String noRujukan,
			String ppkRujukan,
			String ppkPelayanan,
			String jnsPelayanan,
			String catatan,
			String diagAwal,
			String poliTujuan,
			String klsRawat,
			String lakaLantas,
			String noMr){
		return Response.ok(bpjsSeriveBiz.CreateLaporanSep(noKartu, tglSep,tglRujukan,noRujukan,ppkRujukan,ppkPelayanan,jnsPelayanan,catatan,diagAwal,poliTujuan,klsRawat,lakaLantas,noMr)).build();
	}
	
	public Response CreateRoom(String kodeKelas,
			String kodeRuang,
			String namaRuang,
			String kapasitas,
			String tersedia){
		return Response.ok(bpjsSeriveBiz.CreateRoom(kodeKelas,kodeRuang,namaRuang,kapasitas,tersedia)).build();
		
	}
	
	public Response UpdateRoom(String kodeKelas,
			String kodeRuang,
			String namaRuang,
			String kapasitas,
			String tersedia){
		return Response.ok(bpjsSeriveBiz.UpdateRoom(kodeKelas,kodeRuang,namaRuang,kapasitas,tersedia)).build();
		
	}
	
	public Response UpdateTglPulang(
			String noSep,
			String tglPlg){
		return Response.ok(bpjsSeriveBiz.UpdateTglPulangSep(noSep,tglPlg)).build();
	}
	
	public Response UpdateSep(
			String noSep,
			String noKartu,
			String tglSep,
			String tglRujukan,
			String noRujukan,
			String ppkRujukan,
			String ppkPelayanan,
			String jnsPelayanan,
			String catatan,
			String diagAwal,
			String poliTujuan,
			String klsRawat,
			String noMr){
		return Response.ok(bpjsSeriveBiz.UpdateSepBpjs(noSep,noKartu,tglSep,tglRujukan,noRujukan,ppkRujukan,ppkPelayanan,jnsPelayanan,catatan,diagAwal,poliTujuan,klsRawat,noMr)).build();
	}
	
	public Response HapusSep(String noSep){
		return Response.ok(bpjsSeriveBiz.DeleteSep(noSep)).build();
	}
	
	public Response KodeKamar(){
		return Response.ok(bpjsSeriveBiz.KodeKamar()).build();	
	}
	
	public Response ListKamar(){
		return Response.ok(bpjsSeriveBiz.ListKamar()).build();
	}
	
	public Response TransaksiSep(String noSep,String noTrans){
		return Response.ok(bpjsSeriveBiz.MappingTransaksiSep(noSep,noTrans)).build();
	}
	
	public Response GrouperInacbg(){
		return Response.ok(bpjsSeriveBiz.GrouperDataInacbg()).build();
	}
	
	public Response FinalisasiGrouper(){
		return Response.ok(bpjsSeriveBiz.FinalisasiGrouperInacbg()).build();
	}

	//WEBSERVICE INHEALTH
	public Response GetEligibilitasPeserta(String nokainhealth, String tglpelayanan, String jenispelayanan,
			String poli) {
		return Response.ok(bpjsSeriveBiz.GetEligibilitasPeserta(nokainhealth,tglpelayanan,jenispelayanan,poli)).build();
	}

	public Response CekSJP(String nokainhealth, String tanggalsjp, String poli, String tkp) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.CekSJP(nokainhealth,tanggalsjp,poli,tkp)).build();
	}
	
	public Response ConfirmAKTFirstPayor(String nosjp, String userid) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.ConfirmAKTFirstPayor(nosjp,userid)).build();
	}
	
	public Response HapusDetailSJP(String nosjp, String notes,String userid) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.HapusDetailSJP(nosjp,notes,userid)).build();
	}
	
	public Response HapusSJP(String nosjp, String alasanhapus,String userid) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.HapusSJP(nosjp,alasanhapus,userid)).build();
	}

	@Override
	public Response HapusTindakan(String nosjp, String kodetindakan, String tgltindakan, String notes, String userid) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.HapusTindakan(nosjp,kodetindakan,tgltindakan,notes,userid)).build();
	}

	@Override
	public Response InfoSJP(String nosjp) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.InfoSJP(nosjp)).build();
	}

	@Override
	public Response Poli(String keyword) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.Poli(keyword)).build();
	}

	@Override
	public Response ProviderRujukan(String keyword) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.ProviderRujukan(keyword)).build();
	}

	@Override
	public Response RekapHasilVerifikasi(String nofpk) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.RekapHasilVerifikasi(nofpk)).build();
	}

	@Override
	public Response SimpanBiayaINACBGS(String nosjp, String kodeinacbg, BigDecimal biayainacbgs, String nosep,
			String notes, String userid) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.SimpanBiayaINACBGS(nosjp,kodeinacbg,biayainacbgs,nosep,notes,userid)).build();
	}

	@Override
	public Response SimpanRuangRawat(String nosjp, String tglmasuk, String kelasrawat, String kodejenispelayanan,
			BigDecimal byharirawat) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.SimpanRuangRawat(nosjp,tglmasuk,kelasrawat,kodejenispelayanan,byharirawat)).build();
	}

	@Override
	public Response SimpanSJP(String tanggalpelayanan, String jenispelayanan, String nokainhealth,
			String nomormedicalreport, String nomorasalrujukan, String kodeproviderasalrujukan,
			String tanggalasalrujukan, String kodediagnosautama, String poli, String username, String informasitambahan,
			String kodediagnosatambahan, BigDecimal kecelakaankerja, String kelasrawat, String kodejenpelruangrawat) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.SimpanSJP(tanggalpelayanan,jenispelayanan,nokainhealth,nomormedicalreport,nomorasalrujukan,kodeproviderasalrujukan,tanggalasalrujukan,kodediagnosautama,poli,username,informasitambahan,kodediagnosatambahan,kecelakaankerja,kelasrawat,kodejenpelruangrawat)).build();
	}

	@Override
	public Response SimpanTindakan(String jenispelayanan, String nosjp, String tglmasukrawat, String tanggalpelayanan,
			String kodetindakan, String poli, String kodedokter, String biayaaju) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.SimpanTindakan(jenispelayanan,nosjp,tglmasukrawat,tanggalpelayanan,kodetindakan,poli,kodedokter,biayaaju)).build();
	}

	@Override
	public Response SimpanTindakanRITL(String jenispelayanan, String nosjp, String idakomodasi, String tglmasukrawat,
			String tanggalpelayanan, String kodetindakan, String poli, String kodedokter, BigDecimal biayaaju) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.SimpanTindakanRITL(jenispelayanan,nosjp,idakomodasi,tglmasukrawat,tanggalpelayanan,kodetindakan,poli,kodedokter,biayaaju)).build();
	}

	@Override
	public Response UpdateTanggalPulang(BigDecimal id, String nosjp, String tglmasuk, String tglkeluar) {
		// TODO Auto-generated method stub
		return Response.ok(bpjsSeriveBiz.UpdateTanggalPulang(id,nosjp,tglmasuk,tglkeluar)).build();
	}
	

}
