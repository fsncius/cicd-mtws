package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.biz.kemenkes.Referensi;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.Referensi.DataUmum;
import com.rsmurniteguh.webservice.dep.services.IKemenkesReferensi;

public class KemenkesReferensi implements IKemenkesReferensi{

	@Override
	public Response jenisPelayanan() {
		Referensi referensi = new Referensi();
		return Response.ok(referensi.DataUmum(DataUmum.JenisPelayanan)).build();
	}

	@Override
	public Response keadaanKeluar() {
		Referensi referensi = new Referensi();
		return Response.ok(referensi.DataUmum(DataUmum.KeadaanKeluar)).build();
	}
	
	@Override
	public Response faskes() {
		Referensi referensi = new Referensi();
		return Response.ok(referensi.DataUmum(DataUmum.Faskes)).build();
	}

	@Override
	public Response caraKeluar() {

		Referensi referensi = new Referensi();
		return Response.ok(referensi.DataUmum(DataUmum.CaraKeluar)).build();
	}

	@Override
	public Response instalasi() {

		Referensi referensi = new Referensi();
		return Response.ok(referensi.DataUmum(DataUmum.Instalasi)).build();
	}

}
