package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.DashboardBiz;
import com.rsmurniteguh.webservice.dep.biz.MtRegistrationBiz;
import com.rsmurniteguh.webservice.dep.services.IDashboard;

public class DashboardService implements IDashboard {
	@Autowired
	private DashboardBiz dashboardBiz;
	
	@Override
	public Response patient_visit_report(String startdate, String enddate, String tipe) {
		if(startdate == null || enddate == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.patient_visit_report(startdate,enddate,tipe)).build();
	}
	
	@Override
	public Response patient_visit_los(String startdate, String enddate, String tipe, String start, String end) {
		if(startdate == null || enddate == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.patient_visit_los(startdate,enddate,tipe,start,end)).build();
	}
	@Override
	public Response patient_visit_over(String startdate, String enddate, String tipe) {
		if(startdate == null || enddate == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.patient_visit_over(startdate,enddate,tipe)).build();
	}
	@Override
	public Response patient_visit_more(String startdate, String enddate, String tipe) {
		if(startdate == null || enddate == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.patient_visit_more(startdate,enddate,tipe)).build();
	}
	@Override
	public Response diagnosa_report(String startdate, String enddate) {
		if(startdate == null || enddate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.diagnosa_report(startdate,enddate)).build();
	}
	
	@Override
	public Response utilization_radiology_report(String startdate, String enddate) {
		if(startdate == null || enddate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.utilization_radiology_report(startdate,enddate)).build();
	}
	
	@Override
	public Response utilization_surgeryroom_report(String ot_code, String ot_date) {
		if(ot_code == null || ot_date == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.utilization_surgeryroom_report(ot_code,ot_date)).build();
	}
	
	@Override
	public Response utilization_surgeryroom_time_report(String efektif, String startdate, String enddate) {
		if(efektif == null || startdate == null || enddate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.utilization_surgeryroom_time_report(efektif, startdate,enddate)).build();
	}
	
	@Override
	public Response discharge_planning_report(String tanggal) {
		if(tanggal == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.discharge_planning_report(tanggal)).build();
	}
	
	@Override
	public Response cathlab_act_report(String startdate, String enddate) {
		if(startdate == null || enddate == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.cathlab_act_report(startdate,enddate)).build();
	}
	
	@Override
	public Response endoskopi_act_report(String startdate, String enddate) {
		if(startdate == null || enddate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.endoskopi_act_report(startdate, enddate)).build();
	}
	
	@Override
	public Response outpatient_city_report(String startdate, String enddate) {
		if(startdate == null || enddate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.outpatient_city_report(startdate, enddate)).build();
	}
	
	@Override
	public Response material_report(String type, String report) {
		if(type == null || report == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.material_report(type, report)).build();
	}
	
	@Override
	public Response location() {
		return Response.ok(dashboardBiz.location()).build();
	}
	
	@Override
	public Response radiology_schedule_report(String date, String type) {
		if(type == null || date == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.radiology_schedule_report(date, type)).build();
	}
	
	@Override
	public Response radiology_responsetime_report(String startdate, String enddate, String type) {
		if(type == null || startdate == null || enddate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.radiology_responsetime_report(startdate, enddate, type)).build();
	}
	
	@Override
	public Response top_registered_appointment_report(String date) {
		if(date == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.top_registered_appointment_report(date)).build();
	}
	
	@Override
	public Response appointment_report(String date, String department) {
		if(date == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.appointment_report(date,department)).build();
	}
	
	

	@Override
	public Response inpatient_billing_report(String billing_id) {
		if(billing_id == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.inpatient_billing_report(billing_id)).build();
	}
	
	@Override
	public Response top_operation_doctors(String startdate, String enddate) {
		if(startdate == null || enddate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dashboardBiz.top_operation_doctors(startdate, enddate)).build();
	}
	
}
