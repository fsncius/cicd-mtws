package com.rsmurniteguh.webservice.dep.services.impl;

import java.util.ArrayList;

import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.biz.ServerPrintBiz;
import com.rsmurniteguh.webservice.dep.services.IServerPrint;

public class ServerPrintService implements IServerPrint{

	@Override
	public Response getPrintList( String listString) {
		return Response.ok(ServerPrintBiz.getPrintList(listString)).build();
	}

	@Override
	public Response addPrintCount(Long serverPrintId) {
		return Response.ok(ServerPrintBiz.addPrintCount(serverPrintId)).build();
	}
	
}
