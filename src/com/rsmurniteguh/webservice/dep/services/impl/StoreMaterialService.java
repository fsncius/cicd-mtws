package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.StoreMaterialBiz;
import com.rsmurniteguh.webservice.dep.services.IStoreMaterial;

public class StoreMaterialService implements IStoreMaterial{
	@Autowired
	private StoreMaterialBiz StoreMaterialBiz;
	
	@Override
	public Response getstorematerial(String storemtId)
	{
		if(storemtId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(StoreMaterialBiz.getStoreMaterial(storemtId)).build();
	}
}
