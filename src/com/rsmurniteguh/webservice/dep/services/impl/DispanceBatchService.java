package com.rsmurniteguh.webservice.dep.services.impl;

import java.math.BigDecimal;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.DispanceBatchBiz;
import com.rsmurniteguh.webservice.dep.services.IDispanceBatch;
import com.rsmurniteguh.webservice.dep.all.model.QueuePhar;
import com.rsmurniteguh.webservice.dep.all.model.DrugDispense;

public class DispanceBatchService implements IDispanceBatch {
	@Autowired
	private DispanceBatchBiz DispanceBatchBiz;
	@Override
	public Response getdispancebatch (String dispancebano)
	{
		if(dispancebano == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getdispancebatch(dispancebano)).build();
	}
	
	
	@Override
	public Response getdispancebatch2 (String dispancebano2, String patienttype, String patientclass)
	{
		if(dispancebano2 == null || patienttype == null ||  patientclass == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getdispancebatch2(dispancebano2, patienttype, patientclass)).build();
	}
	
	@Override
	public Response getdrugstore (String drugreturnno, String patienttype, String patientclass)
	{
		if(drugreturnno == null || patienttype == null ||  patientclass == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getdrugstore(drugreturnno, patienttype, patientclass)).build();
	}
	
	@Override
	public Response getdispensebatchno(String dispensebatchno)
	{
		if(dispensebatchno == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getdispensebatchno(dispensebatchno)).build();
	}
	
	@Override
	public Response getstoremstrid(String storemstrid)
	{
		if(storemstrid == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getstoremstrid(storemstrid)).build();
	}
	
	@Override
	public Response getdrugdispensedetailinfo(String dispensebatchno)
	{
		if(dispensebatchno ==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getdrugdispensedetailinfo(dispensebatchno)).build();
	}
	
	@Override
	public Response getdrugdispensedetailinfo2(String dispensebatchno)
	{
		if(dispensebatchno ==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getdrugdispensedetailinfo2(dispensebatchno)).build();
	}
	
	@Override
	public Response getCurrentDatetime()
	{
		return Response.ok(DispanceBatchBiz.getCurrentDatetime()).build();
	}
	
	@Override
	public Response getPreparedDatetime(String firstdate, String lastdate)
	{
		if(firstdate== null || lastdate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getprepareddatetime(firstdate,lastdate)).build();
	}
	
	@Override
	public Response getTxnDateTime(String first, String last, String codedesc, String codedesc2)
	{
		if(first== null || last == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getTxnDateTime(first,last,codedesc,codedesc2)).build();
	}
	
	public Response getEnterDateTime(String first, String last, String codedesc, String codedesc2)
	{
		if(first== null || last == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getEnterDateTime(first,last,codedesc,codedesc2)).build();
	}
	
	public Response getVisitNo(String mrn)
	{
		if(mrn == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();	
		}
		return Response.ok(DispanceBatchBiz.getVisitNo(mrn)).build();
	}
	
	
	
	@Override
	public Response getrekonobat(String visitid)
	{
		if(visitid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getrekonobat(visitid)).build();
	}
	
	@Override
	public Response getcekrem(String fromdate, String todate)
	{
		if(fromdate == null || todate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getcekrem(fromdate,todate)).build();
	}
	
	@Override
	public Response getstockreceipt(String fromdate, String todate)
	{
		if(fromdate == null || todate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getstockreceipt(fromdate,todate)).build();
	}
	
	@Override
	public Response getDispanceCount(String fromdate, String todate)
	{
		if(fromdate == null || todate==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getDispanceCount(fromdate, todate)).build();
	}
	
	@Override
	public Response getPharPulvis(String fromdate, String todate)
	{
		if(fromdate==null|| todate==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getPharPulvis(fromdate,todate)).build();
	}
	
	@Override
	public Response getrekonobatforcat(String visitid)
	{
		if(visitid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getrekonobatforcat(visitid)).build();
	}
	
	@Override
	public Response getInfoViewTrx(String fromdate, String todate, String idnik)
	{
		if(fromdate == null || todate == null || idnik == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getInfoViewTrx(fromdate,todate, idnik)).build();
	}
	
	
	@Override
	public Response getInfoViewTrxNew(String IdCompany)
	{
		if(IdCompany == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getInfoViewTrxNew(IdCompany)).build();
	}
	
	@Override
	public Response getInfoViewTrxNewNoCompa()
	{
		return Response.ok(DispanceBatchBiz.getInfoViewTrxNewNoCompa()).build();
	}
	
	@Override
	public Response getInfoViewTrxNewDetail(String FromDate, String ToDate)
	{
		if(FromDate == null || ToDate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getInfoViewTrxNewDetail(FromDate, ToDate)).build();
	}
	
	
	@Override
	public Response getInfoCekSts(String cardno)
	{
		if(cardno == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getInfoCekSts(cardno)).build();
	}
	
	@Override
	public Response getInfoKaryawan(String idnik)
	{
		return Response.ok(DispanceBatchBiz.getInfoKaryawan(idnik)).build();
	}
	
	@Override
	public Response getHasBPJS(String patientid)
	{
		if(patientid == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getHasBPJS(patientid)).build();
	}
	
	
	@Override
	public Response getAllViewTrx(String fromdate, String todate)
	{
		if(fromdate == null || todate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getAllViewTrx(fromdate,todate)).build();
	}
	
	@Override
	public Response getAllKaryawan()
	{
		return Response.ok(DispanceBatchBiz.getAllKaryawan()).build();
	}
	
	public Response getAllRoom()
	{
		return Response.ok(DispanceBatchBiz.getAllRoom()).build();
	}
	@Override
	public Response addApotekGl(String nopo,String total,String tipe)
	{
		if( nopo  ==null || total  ==null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addApotekGl(nopo,total,tipe)).build();
	}
	
	public Response addApotekGlDetail(String nopo,String tglt,String nourt,String sandi,String kat,String nasup,String total,String ido,String tipe)
	{
		if( nopo  ==null || tglt  ==null || nourt  ==null || sandi == null || kat == null || nasup == null || total == null || ido ==null || tipe == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addApotekGlDetail(nopo,tglt,nourt,sandi,kat,nasup,total,ido,tipe)).build();
	}
	
	public Response addNilai(String sandi,String tgl,String nsp,String nopo,String ppn,String nppn,String dis,String tot,String nopr, String noref, String tipe)
	{
		if( sandi  ==null || tgl  ==null || nsp  ==null || nopo == null || ppn == null || nppn==null || dis == null || tot == null || nopr ==null || noref == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addNilai(sandi,tgl,nsp,nopo,ppn,nppn,dis,tot,nopr,noref,tipe)).build();
	}
	
	public Response addHutang(String nobukti,String ids,String tgl,String tgljt,String user,String total, String tipe)
	{
		if( nobukti  ==null || ids  ==null || tgl  ==null || tgljt  ==null || user == null || total == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addHutang(nobukti,ids,tgl,tgljt,user,total,tipe)).build();
	}
	
	public Response addPiutang(String nobukti,String ids,String tgl,String user,String total, String tipe)
	{
		if( nobukti  ==null || ids  ==null || tgl  ==null || user == null || total == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addPiutang(nobukti,ids,tgl,user,total,tipe)).build();
	}
	
	public Response addHutangDetail(String nobukti,String kategori,String sandi,BigDecimal qty,BigDecimal biaya, String tipe)
	{
		if( nobukti  ==null || kategori  ==null || sandi  ==null || qty == null || biaya == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addHutangDetail(nobukti,kategori,sandi,qty,biaya,tipe)).build();
	}
	
	public Response addPiutangDetail(String nobukti,String kategori,String sandi,BigDecimal qty,BigDecimal biaya, String tipe)
	{
		if( nobukti  ==null || kategori  ==null || sandi  ==null || qty == null || biaya == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addPiutangDetail(nobukti,kategori,sandi,qty,biaya,tipe)).build();
	}
	
	
	public Response addRtb(String nopo,String total,String tipe)
	{
		if( nopo  ==null || total  ==null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addRtb(nopo,total,tipe)).build();
	}

	public Response nilaiRtb(String nopo,String nsp,String tgl,String totalret,String ppn,String total,String kategori,String sandi,String tipe)
	{
		if( nopo  ==null ||nsp==null||tgl==null || totalret  ==null || ppn == null || total==null || kategori ==null || sandi==null || tipe ==null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.nilaiRtb(nopo,nsp,tgl,totalret,ppn,total,kategori,sandi,tipe)).build();
	}
	
	public Response addJpj(String idt,String total,String tipe,String user)
	{
		if( idt  ==null || total  ==null || tipe == null || user == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addJpj(idt,total,tipe,user)).build();
	}
	public Response addJpjDetail(String idt,String tglt,String dis,String sandi,String nasup,String total,String tipe)
	{
		if( idt  ==null || tglt  ==null || dis  ==null || sandi == null  || nasup == null || total == null  || tipe == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addJpjDetail(idt,tglt,dis,sandi,nasup,total,tipe)).build();
	}
	public Response addJpjNilai(String sandi,String tgl,String nsp,String idt,String tot,String kat, String tipe)
	{
		if( sandi  ==null || tgl  ==null || nsp  ==null || idt == null || tot == null || kat==null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(DispanceBatchBiz.addJpjNilai(sandi,tgl,nsp,idt,tot,kat,tipe)).build();
	}
	
	
	
	
	
	@Override
	public Response getCekBalanceNik(String Nik)
	{
		if(Nik == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getCekBalanceNik(Nik)).build();
	}
	
	@Override
	public Response getCekTransaksiNik(String AccountBlcId)
	{
		if(AccountBlcId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.getCekTransaksiNik(AccountBlcId)).build();
	}
	
	@Override
	public Response getDataPasienSms()
	{
		return Response.ok(DispanceBatchBiz.getDataPasienSms()).build();
	}
	
	@Override
	public Response GetDelQueuePhar(QueuePhar DataPhar)
	{
		if(DataPhar == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DispanceBatchBiz.GetDelQueuePhar(DataPhar)).build();
	}


	@Override
	public Response getdrugdispense(String drugdispense_id) {
		// TODO Auto-generated method stub
		return Response.ok(DispanceBatchBiz.getdrugdispense(drugdispense_id)).build();
	}


	@Override
	public Response getdrugdispensevisit(String visit_id, String prepared_datetime) {
		// TODO Auto-generated method stub
		return Response.ok(DispanceBatchBiz.getdrugdispensevisit(visit_id, prepared_datetime)).build();
	}
	
	@Override
	public Response updatedrugdispense(String drugdispense_id) {
		// TODO Auto-generated method stub
		return Response.ok(DispanceBatchBiz.updatedrugdispense(drugdispense_id)).build();
	}

	
//	@Override
//	public Response getDataSms(String nohp, String pesan, String keterangan, String status)
//	{
//		if(nohp==null || pesan==null || keterangan==null || status==null)
//		{
//			return Response.status(Response.Status.BAD_REQUEST).build();
//		}
//		return Response.ok(DispanceBatchBiz.getDataSms(nohp, pesan, keterangan, status)).build();
//	}
}