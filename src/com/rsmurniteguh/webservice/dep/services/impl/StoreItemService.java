package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.StoreItemBiz;
import com.rsmurniteguh.webservice.dep.services.IStoreItem;

public class StoreItemService implements IStoreItem {
	@Autowired
	private StoreItemBiz StoreItemBiz;
	
	@Override
	public Response getstoreitem(String StoreMstrId, String MaterialItemMstrId)
	{
		if(StoreMstrId == null || MaterialItemMstrId== null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(StoreItemBiz.getStoreItem(StoreMstrId, MaterialItemMstrId)).build();
			
	}

}
