package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.TestModel;
import com.rsmurniteguh.webservice.dep.biz.TestingBiz;
import com.rsmurniteguh.webservice.dep.services.ITesting;
import com.rsmurniteguh.webservice.dep.util.CommonUtil;
import com.securecontext.secure.EncryptorConnection;

public class Testing implements ITesting{
	
	@Context 
	HttpHeaders headers;
	
	@Autowired
	private TestingBiz testingBiz;
	
	@Override
	public Response TestSave(TestModel model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response encrypt() {
		String passHeaderAuth = CommonUtil.getInstance().getPasswordFromAuth(headers);
		
		String enkrip = EncryptorConnection.getInstance().encrypt(passHeaderAuth);
		ResponseString ret = new ResponseString();
		ret.setResponse(enkrip);
		
		return Response.ok(ret).build();
	}
	@Override
	public Response dekripsi() {
		String passHeaderAuth = CommonUtil.getInstance().getPasswordFromAuth(headers);
		String dekrip = EncryptorConnection.getInstance().decrypt(passHeaderAuth);
		ResponseString ret = new ResponseString();
		ret.setResponse(dekrip);
		
		return Response.ok(ret).build();
	}

	@Override
	public Response testEmail() {
		return Response.ok(testingBiz.testEmail()).build();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             