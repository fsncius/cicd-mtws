package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.apache.http.util.TextUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.queue.QueueConfig;
import com.rsmurniteguh.webservice.dep.all.model.queue.RequestTicket;
import com.rsmurniteguh.webservice.dep.biz.QueueServiceBiz;
import com.rsmurniteguh.webservice.dep.services.IQueueService;
import com.rsmurniteguh.webservice.dep.all.model.queue.RequestTicket;

public class QueueService implements IQueueService{
	@Autowired
	private QueueServiceBiz queueServiceBiz;

	@Override
	public Response postDoctorWorkstationCall(String queueno, String subspeciality, String usermstr) {
		if(queueno == null || subspeciality == null || usermstr == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.postDoctorWorkstationCall(queueno, subspeciality, usermstr)).build();
	}

	@Override
	public Response postPharmacyComplete(String queueno, String subspeciality, String usermstr) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Response InsertQueue(String CareProviderId, String patient_name, String card_no, String tgl_berobat, String PatientId, String location, String group_id) {
		if(CareProviderId == null || patient_name == null || card_no == null || tgl_berobat == null || PatientId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(queueServiceBiz.InsertQueue(CareProviderId, patient_name, card_no, tgl_berobat,PatientId, location, group_id)).build();
		}
	}
	
	@Override
	public Response RequestIDTicket(String resourcemstr, String location, String tanggal, String source, String nama_patient, String no_mrn, String queue_type) {
		if(resourcemstr == null || location == null || tanggal == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.RequestIDTicket(resourcemstr, location, tanggal, source, nama_patient,  no_mrn, queue_type)).build();
	}
	
	@Override
	public Response RequestTicket(String location, String queue_id, String no_doctor, String no_subspecialis,String tanggal){
		if(location == null || location.isEmpty() || location == null || location.isEmpty() || tanggal == null || no_doctor == null 
				|| no_doctor.isEmpty() || no_subspecialis == null || no_subspecialis.isEmpty()){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.RequestTicket(location, no_subspecialis, no_doctor, queue_id, tanggal)).build();
	}
	
	@Override 
	public Response ChangeQueueStatus(String queue_id, String userMstrID, String status, String counterId, String statusQueueTime, String queue_no){
		if(queue_id == null || userMstrID == null){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else{
			return Response.ok(queueServiceBiz.ChangeQueueStatus(queue_id, userMstrID, status, counterId, statusQueueTime, queue_no)).build();
		}
	}
	
	@Override 
	public Response ListviewReg(String counters){
		if(counters == null || counters.isEmpty() ){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.ListviewReg(counters)).build();
	}

	@Override
	public Response ListQueue(String lokasi, String counter_id, String tanggal, String regType, String status) {
		if(lokasi == null || lokasi.isEmpty() || counter_id == null || counter_id.isEmpty() || tanggal == null || tanggal.isEmpty() || regType == null || regType.isEmpty() ){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return TextUtils.isEmpty(status) ? Response.ok(queueServiceBiz.ListQueue(lokasi,counter_id,tanggal,regType)).build() 
				: Response.ok(queueServiceBiz.ListQueue(lokasi,counter_id,tanggal,regType, status)).build();
	}

	@Override
	public Response SoundQueue(String counters) {
		if(counters == null || counters.isEmpty() ){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.SoundQueue(counters)).build();
	}

	@Override
	public Response delQueueCall(String queue_id) {
		if(queue_id == null || queue_id.isEmpty() ){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.delQueueCall(queue_id)).build();
	}

	@Override
	public Response getConfigData() {
		return Response.ok(queueServiceBiz.getConfigData()).build();
	}

	@Override
	public Response getActiveCounter(String counters) {
		return Response.ok(queueServiceBiz.getActiveCounter(counters)).build();
	}

	public Response CallSpecialQueueByNumber(String location, String counterid, String queueNo, String userMstrId) {
		if(location == null || location.isEmpty() || counterid == null || counterid.isEmpty() || queueNo == null || queueNo.isEmpty() || userMstrId == null || userMstrId.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.CallSpecialQueueByNumber(location, counterid, queueNo, userMstrId)).build();
	}

	@Override
	public Response SkipSpecialQueueByNumber(String location, String counterid, String queueNo, String userMstrId) {
		if(location == null || location.isEmpty() || counterid == null || counterid.isEmpty() || queueNo == null || queueNo.isEmpty() || userMstrId == null || userMstrId.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.SkipSpecialQueueByNumber(location, counterid, queueNo, userMstrId)).build();
	}

	@Override
	public Response FinishSpecialQueueByNumber(String location, String counterid, String queueNo, String userMstrId) {
		if(location == null || location.isEmpty() || counterid == null || counterid.isEmpty() || queueNo == null || queueNo.isEmpty() || userMstrId == null || userMstrId.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.FinishSpecialQueueByNumber(location, counterid, queueNo, userMstrId)).build();
	}

	@Override
	public Response ListViewPolyclinicBPJS(String location, String counters) {
		if(location == null || location.isEmpty() || counters == null || counters.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.ListViewPolyclinicBPJS(location, counters)).build();
	}
	
	@Override
	public Response LoginCaller(String username, String password) {
		if(username == null || username.isEmpty() || password == null || password.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.LoginCaller(username, password)).build();
	}
	
	public Response ChangeCountertoAktif(String counterid, String usermstrid, String resourcemstrId) {
		if(counterid == null || counterid.isEmpty()|| usermstrid == null || usermstrid.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.ChangeCountertoAktif(counterid,usermstrid,resourcemstrId)).build();
	}
	
	public Response ChangeCountertoNonAktif(String counterid) {
		if(counterid == null || counterid.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.ChangeCountertoNonAktif(counterid)).build();
	}

	@Override
	public Response QueueDetails(String queue_no, String location, String tanggal) {
		if(location == null || location.isEmpty() || queue_no == null || queue_no.isEmpty() || tanggal == null || tanggal.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.QueueDetails(queue_no, location, tanggal)).build();
	}
	
	@Override
	public Response ListQueueKasir(String location) {
		if(location == null || location.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.ListQueueKasir(location)).build();
	}
	
	@Override
	public Response InsertQueueKasir(String queue_no, String location, String source,String tanggal,String queue_type) {
		if(queue_no == null || queue_no.isEmpty() || location == null || location.isEmpty() ) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.InsertQueueKasir(queue_no,location,source,tanggal,queue_type)).build();
	}
	
	@Override
	public Response QueueCallKasir(String location,String counter_id) {
		if(location == null || location.isEmpty() || counter_id == null || counter_id.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.QueueCallKasir(location,counter_id)).build();
	}
	
	@Override
	public Response QueueNoByMrn(String noMrn) {
		if(noMrn == null || noMrn.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.QueueNoByMrn(noMrn)).build();
	}

	@Override
	public Response getConfigDataByAddress(String mac_address) {
		if(mac_address == null || mac_address.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.getConfigDataByAddress(mac_address)).build();
	}
	
	@Override
	public Response retConfigDataByAddress(QueueConfig queueConfig) {
		if(queueConfig == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.retConfigDataByAddress(queueConfig)).build();
	}
	
	@Override
	public Response setConfigDataByAddress(QueueConfig queueConfig) {
		if(queueConfig == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.setConfigDataByAddress(queueConfig)).build();
	}
	
	public Response QueueCallSkipKasir(String queue_no,String lokasi) {
		if(queue_no == null || queue_no.isEmpty() || lokasi == null || lokasi.isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.QueueCallSkipKasir(queue_no,lokasi)).build();
	}

	@Override
	public Response getServingTime(Long queueId) {
		if(queueId == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(queueServiceBiz.getServingTime(queueId)).build();
	}

}
