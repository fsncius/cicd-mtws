package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.WardRoomBiz;
import com.rsmurniteguh.webservice.dep.services.IWardRoom;

public class WardRoomService implements IWardRoom {
	@Autowired
	private WardRoomBiz WardRoomBiz;
	
	@Override
	public Response getWardRoom()
	{
		return Response.ok(WardRoomBiz.getWardRoom()).build();
	}
}
