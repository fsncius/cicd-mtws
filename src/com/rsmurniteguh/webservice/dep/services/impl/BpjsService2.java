package com.rsmurniteguh.webservice.dep.services.impl;

import java.text.ParseException;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.http.util.TextUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.BpjsSepUpdateParam;
import com.rsmurniteguh.webservice.dep.all.model.PengajuanKlaim;
import com.rsmurniteguh.webservice.dep.biz.BpjsServiceBiz;
import com.rsmurniteguh.webservice.dep.biz.BpjsServiceBiz2;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsService;
import com.rsmurniteguh.webservice.dep.services.IBpjsService2;

public class BpjsService2 implements IBpjsService2 {
	@Autowired
	private BpjsServiceBiz2 bpjsServiceBiz2;
	
	@Override
	public Response GetBpjsInfobyNoka(String noKa) {
		return Response.ok(bpjsServiceBiz2.getBpjsInfobyNoka(noKa)).build();
	}
	
	public Response GetBpjsInfobyNik(String nik) {
		return Response.ok(bpjsServiceBiz2.GetBpjsInfobyNik(nik)).build();
	}
		
	public Response insertSep(String noKa,String jnsPelayanan,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String poli,String noHp,String user) {
		return Response.ok(bpjsServiceBiz2.insertSep(noKa,jnsPelayanan,klsRawat,mrn,asalRujuk,tglRujuk,noRujuk,catatan,diagAwal,poli,noHp,user)).build();
	}
	
	public Response insertSep4(String noKa,String tglSep,String jnsPelayanan,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String poli,String noHp,String user) {
		return Response.ok(bpjsServiceBiz2.insertSep4(noKa,tglSep,jnsPelayanan,klsRawat,mrn,asalRujuk,tglRujuk,noRujuk,catatan,diagAwal,poli,noHp,user)).build();
	}
	
	public Response insertSep2(String noKa,String jnsPelayanan,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String poli,String noHp,String user) {
		return Response.ok(bpjsServiceBiz2.insertSep2(noKa,jnsPelayanan,klsRawat,mrn,asalRujuk,tglRujuk,noRujuk,catatan,diagAwal,poli,noHp,user)).build();
	}
	
	public Response detailSep(String noSep) {
		return Response.ok(bpjsServiceBiz2.detailSep(noSep)).build();
	}
	
	public Response hapusSep(String noSep,String user) {
		return Response.ok(bpjsServiceBiz2.hapusSep(noSep,user)).build();
	}
	
	public Response updateSep(String noSep,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String noHp,String user) {
		return Response.ok(bpjsServiceBiz2.updateSep(noSep,klsRawat,mrn,asalRujuk,tglRujuk,noRujuk,catatan,diagAwal,noHp,user)).build();
	}
	public Response pengajuanSep(String noKa,String tglPelayanan,String jnsPelayanan,String keterangan,String user) {
		return Response.ok(bpjsServiceBiz2.pengajuanSep(noKa,tglPelayanan,jnsPelayanan,keterangan,user)).build();
	}
	
	public Response approvalSep(String noKa,String tglPelayanan,String jnsPelayanan,String keterangan,String user) {
		return Response.ok(bpjsServiceBiz2.approvalSep(noKa,tglPelayanan,jnsPelayanan,keterangan,user)).build();
	}
	
	public Response updateTglPlg(String noSep,String tglPulang,String user) {
		return Response.ok(bpjsServiceBiz2.updateTglPlg(noSep,tglPulang,user)).build();
	}
	
	public Response integrasiInacbg(String noSep) {
		return Response.ok(bpjsServiceBiz2.integrasiInacbg(noSep)).build();
	}
	
	public Response insertRujukan(String noSep,String ppkDirujuk,String jnsPelayanan,String catatan,String diagRujukan,String tipeRujukan,String poliRujukan,String user) {
		return Response.ok(bpjsServiceBiz2.insertRujukan(noSep,ppkDirujuk,jnsPelayanan,catatan,diagRujukan,tipeRujukan,poliRujukan,user)).build();
	}
	
	public Response updateRujukan(String noRujukan,String ppkDirujuk,String tipe,String jnsPelayanan,String catatan,String diagRujukan,String tipeRujukan,String poliRujukan,String user) {
		return Response.ok(bpjsServiceBiz2.updateRujukan(noRujukan,ppkDirujuk,tipe,jnsPelayanan,catatan,diagRujukan,tipeRujukan,poliRujukan,user)).build();
	}
	
	public Response deleteRujukan(String noRujukan,String user) {
		return Response.ok(bpjsServiceBiz2.deleteRujukan(noRujukan,user)).build();
	}

	public Response dataKunjungan(String tglSep,String jnsPelayanan) {
		return Response.ok(bpjsServiceBiz2.dataKunjungan(tglSep,jnsPelayanan)).build();
	}
	
	public Response dataFaskes(String kodeRs, String tipe) {
		if(kodeRs == null)return Response.ok(bpjsServiceBiz2.dataFaskes(tipe)).build();
		else
		return Response.ok(bpjsServiceBiz2.dataFaskes(kodeRs, tipe)).build();
	}
	
	public Response diagnosa(String diag) {
		return Response.ok(bpjsServiceBiz2.diagnosa(diag)).build();
	}
	
	public Response dataKlaim(String tglPulang,String jnsPelayanan,String status) {
		return Response.ok(bpjsServiceBiz2.dataKlaim(tglPulang,jnsPelayanan,status)).build();
	}
	
	public Response insertLpk(PengajuanKlaim pengajuanKlaim) {
		return Response.ok(bpjsServiceBiz2.insertLpk(pengajuanKlaim)).build();
	}
	public Response insertSep3(
			String noKa,
			String jnsPelayanan,
			String klsRawat,
			String mrn,
			String asalRujukan,
			String tglRujukan,
			String noRujukan,
			String asalPerujuk,
			String catatan,
			String diagAwal,
			String poli,
			String Eks,
			String lakaLantas,
			String penjamin,
			String tglKejadian,
			String ket,
			String suplesi,
			String noSuplesi,
			String kdPro,
			String kdKab,
			String kdKec,
			String noSurat,
			String dpjp,
			String noHp,
			String user) {
		return Response.ok(bpjsServiceBiz2.insertSep3(noKa,jnsPelayanan,klsRawat,mrn,asalRujukan,tglRujukan,noRujukan,asalPerujuk,catatan,diagAwal,poli,Eks,lakaLantas,penjamin,tglKejadian,ket,suplesi,noSuplesi,kdPro,kdKab,kdKec,noSurat,dpjp,noHp,user)).build();
	}

	public Response dpjp(
			String jnsPelayanan,
			String spesialis,
			String tanggal) {
		if(jnsPelayanan == null || tanggal == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.dpjp(jnsPelayanan,spesialis, tanggal)).build();
	}
	
	
	/* ---------- Andy Three S ----*/
	
	public Response getRujukanbyNmrRujukan(String nmrRujukan) {
		if(nmrRujukan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getRujukanbyNmrRujukan(nmrRujukan)).build();
	}
	
	public Response getRujukanRSbyNmrRujukan(String nmrRujukan) {
		if(nmrRujukan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getRujukanRSbyNmrRujukan(nmrRujukan)).build();
	}
	
	public Response getRujukanbyNoka(String noKa) {
		if(noKa == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getRujukanbyNoka(noKa)).build();
	}
	
	public Response getRujukanRSbyNoka(String noKa) {
		if(noKa == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getRujukanRSbyNoka(noKa)).build();
	}

	@Override
	public Response getRujukanPcareorRSbyNoka(String noKa) {
		if(noKa == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getRujukanPcareorRSbyNoka(noKa)).build();
	}
	
	public Response getRujukanListbyNoka(String noKa) {
		if(noKa == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getRujukanListbyNoka(noKa)).build();
	}
	
	public Response getRujukanRSListbyNoka(String noKa) {
		if(noKa == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getRujukanRSListbyNoka(noKa)).build();
	}

	public Response getRujukanbyTglRujukan(String tglRujukan) throws ParseException {
		if(tglRujukan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getRujukanbyTglRujukan(tglRujukan)).build();
	}
	
	public Response getRujukanRSbyTglRujukan(String tglRujukan) throws ParseException {
		if(tglRujukan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getRujukanRSbyTglRujukan(tglRujukan)).build();
	}

	public Response getReferensiProcedure(String kode, String nama) {
		if(kode == null && nama == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getReferensiProcedure(kode, nama)).build();
	}
	
	public Response getReferensiKelasRawat() {
		return Response.ok(bpjsServiceBiz2.getReferensiKelasRawat()).build();
	}
	
	public Response getReferensiDokter(String namaDokter) {
		if(namaDokter == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getReferensiDokter(namaDokter)).build();
	}
	
	public Response getReferensiSpesialistik() {
		return Response.ok(bpjsServiceBiz2.getReferensiSpesialistik()).build();
	}
	
	public Response getReferensiRuangRawat() {
		return Response.ok(bpjsServiceBiz2.getReferensiRuangRawat()).build();
	}
	
	public Response getReferensiCaraKeluar() {
		return Response.ok(bpjsServiceBiz2.getReferensiCaraKeluar()).build();
	}
	
	public Response getReferensiPascaPulang() {
		return Response.ok(bpjsServiceBiz2.getReferensiPascaPulang()).build();
	}
	
	public Response getReferensiPropinsi() {
		return Response.ok(bpjsServiceBiz2.getReferensiPropinsi()).build();
	}
	public Response getReferensiKabupaten(String kodePropinsi) {
		if(kodePropinsi == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getReferensiKabupaten(kodePropinsi)).build();
	}
	public Response getReferensiKecamatan(String kodeKabupaten) {
		if(kodeKabupaten == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getReferensiKecamatan(kodeKabupaten)).build();
	}
	
	public Response getSuplesiJasaRaharja(String noBPJS,String tglSEP) throws ParseException {
		if(noBPJS == null || tglSEP == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getSuplesiJasaRaharja(noBPJS,tglSEP)).build();
	}

	@Override
	public Response GetBpjsInfobyType(String type, String target) {
		if(type == null || target == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		return Response.ok(bpjsServiceBiz2.getBpjsInfobyType(type, target)).build();
	}

	@Override
	public Response updateSep2(BpjsSepUpdateParam param) {
		if(param == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		return Response.ok(bpjsServiceBiz2.updateSep2(param)).build();
	}

	@Override
	public Response getReferensiPoli(String codeOrName) {
		if(codeOrName == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(bpjsServiceBiz2.getReferensiPoli(codeOrName)).build();
	}
	
}
