package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.CheckDataPasienBiz;
import com.rsmurniteguh.webservice.dep.biz.KpiSystemsBiz;
import com.rsmurniteguh.webservice.dep.services.ICheckDataPasien;
import com.rsmurniteguh.webservice.dep.services.IKpiSystems;

public class KpiSystemsImpService implements IKpiSystems{
	@Autowired
	private KpiSystemsBiz KpiSystemsBiz;
	
	@Override
	public Response getKpiQuer1()
	{
		return Response.ok(KpiSystemsBiz.getKpiQuer1()).build();
	}

	@Override
	public Response getKpiQuer2()
	{
		return Response.ok(KpiSystemsBiz.getKpiQuer2()).build();
	}
	
	@Override
	public Response ViewJenisKamar() {
		// TODO Auto-generated method stub
		return Response.ok(KpiSystemsBiz.getViewJenisKamar()).build();
	}
	@Override
	public Response getViewKunjungan() {
		// TODO Auto-generated method stub
		return Response.ok(KpiSystemsBiz.getViewKunjungan()).build();
	}

	@Override
	public Response getViewPembiayaan() {
		// TODO Auto-generated method stub
		return Response.ok(KpiSystemsBiz.getViewPembiayaan()).build();
	}

	@Override
	public Response getWaktuLayanIGD() {
		// TODO Auto-generated method stub
		return Response.ok(KpiSystemsBiz.getWaktuLayanIGD()).build();
	}

	
}