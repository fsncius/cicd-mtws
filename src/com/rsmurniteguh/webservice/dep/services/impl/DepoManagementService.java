package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.PaymentModel;
import com.rsmurniteguh.webservice.dep.all.model.UpdateCard;
import com.rsmurniteguh.webservice.dep.biz.BpjsServiceBiz;
import com.rsmurniteguh.webservice.dep.biz.DepoManagementBiz;
import com.rsmurniteguh.webservice.dep.services.IDepoManagement;

public class DepoManagementService implements IDepoManagement{
	@Autowired
	private DepoManagementBiz depoManagementBiz;

	@Override
	public Response PaymentTransaction(PaymentModel pay) {
		if(pay == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(depoManagementBiz.DoPayment(pay)).build();
	}
	
	
	@Override
	public Response PaymentTransactionModified(PaymentModel pay) {
		if(pay == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(depoManagementBiz.DoPaymentModified(pay)).build();
	}

	@Override
	public Response RegisterAccount(PaymentModel pay) {
		if(pay == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(depoManagementBiz.RegisterAccount(pay)).build();
	}
	
	@Override
	public Response UpdateAccount(PaymentModel pay) {
		if(pay == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(depoManagementBiz.UpdateAccount(pay)).build();
	}

	@Override
	public Response GetInvoice() {
		return Response.ok(depoManagementBiz.GenerateInvoice()).build();
	}

	@Override
	public Response GetInvoiceCan()
	{
		return Response.ok(depoManagementBiz.GenerateInvoiceCan()).build();
	}
	
	@Override
	public Response GetBalance(String cardNo) {
		if(cardNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(depoManagementBiz.GetBalance(cardNo)).build();
	}

	@Override
	public Response PrepareGetCardDataFromMachine(String machineNo) {
		if(machineNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(depoManagementBiz.PrepareGetCardDataFromMachine(machineNo)).build();
	}

	@Override
	public Response GetCardDataByMachine(String machineNo) {
		if(machineNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(depoManagementBiz.GetCardDataByMachine(machineNo)).build();
	}

	@Override
	public Response GetValidCardDataByMachine(String machineNo) {
		if(machineNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(depoManagementBiz.GetValidCardDataByMachine(machineNo)).build();
	}
	
	
	
	@Override
	public Response ChangeCardNumber(UpdateCard data) {
		if(data == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(depoManagementBiz.ChangeCardNumber(data)).build();
	}
	
	@Override
	public Response GetCardDetailInfo(String cardNo, String cardType){
		if(cardNo == null || cardType == null)return Response.status(Response.Status.BAD_REQUEST).build();
		else return Response.ok(depoManagementBiz.GetCardDetailInfo(cardNo, cardType)).build();
	}
	
}
