package com.rsmurniteguh.webservice.dep.services.impl;

import java.io.IOException;
import java.math.BigDecimal;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.BpjsServiceBiz2;
import com.rsmurniteguh.webservice.dep.biz.InacbgBiz;
import com.rsmurniteguh.webservice.dep.services.IInacbg;

public class InacbgService implements IInacbg  {
	@Autowired
	private InacbgBiz inacbgBiz;
	
	@Override
	public Response cetakKlaim(String noSep) {
		return Response.ok(inacbgBiz.cetakKlaim(noSep)).build();
	}

	@Override
	public Response tambahKlaim(String noKa, String noSep, String mrn, String nama, String tglLahir, String gender) {
		return Response.ok(inacbgBiz.tambahKlaim(noKa,noSep,mrn,nama,tglLahir,gender)).build();
	}

	@Override
	public Response updateKlaim(String noSep, String noKa, String tglMasuk, String tglPulang, String jnsRawat,String klsRawat,
			String icuInd, String icuLos, String ventHour, String upInd, String upCls, String upLos, String addPay,
			String brWgt, String disSts, String diag, String pro, String trfPnb, String trfBdh, String trfKon,
			String trfTa, String trfKep, String trfPnj, String trfRad, String trfLab, String trfPd, String trfReh,
			String trfKmr, String trfRi, String trfOb, String trfAl, String trfBm, String trfSa, String naDok) {
		return Response.ok(inacbgBiz.updateKlaim(noSep,noKa,tglMasuk,tglPulang,jnsRawat,klsRawat,icuInd,icuLos,ventHour,upInd,upCls,
				upLos,addPay,brWgt,disSts,diag,pro,trfPnb,trfBdh,trfKon,trfTa,trfKep,trfPnj,trfRad,trfLab,trfPd,trfReh,
				trfKmr, trfRi, trfOb, trfAl, trfBm, trfSa, naDok)).build();
	}

	@Override
	public Response groupingStage1(String noSep) {
		return Response.ok(inacbgBiz.groupingStage1(noSep)).build();
	}

	@Override
	public Response groupingStage2(String noSep, String cmg) {
		return Response.ok(inacbgBiz.groupingStage2(noSep,cmg)).build();
	}

	@Override
	public Response finalKlaim(String noSep) {
		return Response.ok(inacbgBiz.finalKlaim(noSep)).build();
	}
	
	@Override
	public Response updatePasien(String mrn,String bpjs,String nama,String tgllahir,String gender) {
		return Response.ok(inacbgBiz.updatePasien(mrn,bpjs,nama,tgllahir,gender)).build();
	}
	
	@Override
	public Response hapusPasien(String mrn) {
		return Response.ok(inacbgBiz.hapusPasien(mrn)).build();
	}
	
	@Override
	public Response editUlangKlaim(String sep) {
		return Response.ok(inacbgBiz.editUlangKlaim(sep)).build();
	}
	
	@Override
	public Response sendKlaim(String startdate, String enddate, String jenis, String tipe) {
		return Response.ok(inacbgBiz.sendKlaim(startdate,enddate,jenis,tipe)).build();
	}
	
	@Override
	public Response sendKlaimIndividual(String sep) {
		return Response.ok(inacbgBiz.sendKlaimIndividual(sep)).build();
	}
	
	@Override
	public Response getKlaim(String startdate, String enddate, String jenis) {
		return Response.ok(inacbgBiz.getKlaim(startdate,enddate,jenis)).build();
	}
	
	@Override
	public Response getDetailKlaimbySep(String sep) {
		return Response.ok(inacbgBiz.getDetailKlaimbySep(sep)).build();
	}
	
	@Override
	public Response getStatusKlaimbySep(String sep) {
		return Response.ok(inacbgBiz.getStatusKlaimbySep(sep)).build();
	}
		
	@Override
	public Response deleteKlaimbySep(String sep) {
		return Response.ok(inacbgBiz.deleteKlaimbySep(sep)).build();
	}
	
	@Override
	public Response searchDiagnosa(String keyword) {
		return Response.ok(inacbgBiz.searchDiagnosa(keyword)).build();
	}
	
	@Override
	public Response searchProcedure(String keyword) {
		return Response.ok(inacbgBiz.searchProcedure(keyword)).build();
	}
	
	@Override
	public Response integrasiKthis(BigDecimal visitId) {
		return Response.ok(inacbgBiz.integrasiKthis(visitId)).build();
	}
	
	
	
}
