package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.StorageBiz;
import com.rsmurniteguh.webservice.dep.services.IStorageService;

public class StorageService implements IStorageService{

	@Autowired
	private StorageBiz storageBiz;
	
	@Override
	public Response getStorageByLocation(String location) {
		if(location == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}		
		return Response.ok(storageBiz.getStoreByLocation(location)).build();
	}

}
