package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.labUserBiz;
import com.rsmurniteguh.webservice.dep.services.LabUser;

public class LabUserService implements LabUser {
	@Autowired
	private labUserBiz LabUserBiz;
	
	@Override
	public Response getlabuser (String submstrid)
	{
		if(submstrid == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(LabUserBiz.getlabuser(submstrid)).build();
	}
	
	public Response getorder()
	{
		return Response.ok(LabUserBiz.getorder()).build();
	}
	
	public Response getradiologi()
	{
		return Response.ok(LabUserBiz.getradiologi()).build();	
	}

}