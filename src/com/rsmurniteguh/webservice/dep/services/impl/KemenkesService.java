package com.rsmurniteguh.webservice.dep.services.impl;

import java.math.BigDecimal;

import javax.ws.rs.core.Response;

import org.apache.http.util.TextUtils;

import com.rsmurniteguh.webservice.dep.all.model.kemenkes.SendResumeMedisParam;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.SimRsIntegration;
import com.rsmurniteguh.webservice.dep.services.IKemenkes;
import com.rsmurniteguh.webservice.dep.services.IKemenkesReferensi;
import com.rsmurniteguh.webservice.dep.services.IKemenkesResumeMedis;

public class KemenkesService extends KemenkesResumeMedis implements IKemenkes{
	private  String mNik = null;
	public KemenkesService() {
		
	}
	public KemenkesService(String nik) {
		this.setNik(nik);
	}
	
	private static IKemenkesResumeMedis instance(String nik) {
		return new KemenkesService(nik);
	}
	
	@Override
	public IKemenkesResumeMedis ResumeMedis(String nik) {
		return instance(nik);
	}

	@Override
	public String getNik() {
		return mNik;
	}
	
	@Override
	public void setNik(String nik) {
		mNik = nik;
	}
	
	@Override
	public Response sendResumeMedis(SendResumeMedisParam param) {
		if(param == null)return Response.status(Response.Status.BAD_REQUEST).build();
		return Response.ok(SimRsIntegration.sendResumeMedis(param)).build();
	}
	
	@Override
	public IKemenkesReferensi Referensi() {
		return new KemenkesReferensi();
	}
	


}
