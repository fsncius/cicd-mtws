package com.rsmurniteguh.webservice.dep.services.impl;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.DataPasien;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.NewPatient;
import com.rsmurniteguh.webservice.dep.biz.CheckDataPasienBiz;
import com.rsmurniteguh.webservice.dep.kthis.view.RegisterOption;
import com.rsmurniteguh.webservice.dep.services.ICheckDataPasien;

public class CheckDataPasienService implements ICheckDataPasien{
	@Autowired
	private CheckDataPasienBiz CheckDataPasienBiz;
	
	@Override
	public Response getcheckdatapasien(String ipmrn)
	{
		if(ipmrn == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CheckDataPasienBiz.getCheckDataPasien(ipmrn)).build();
	}
	
	@Override
	public Response getcekdiagnos(String ipmrn)
	{
		if(ipmrn == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CheckDataPasienBiz.getcekdiagnos(ipmrn)).build();
	}
	
	@Override
	public Response getVisitDiagnosisDetailId (String visitdiagnosadetailid)
	{
		if(visitdiagnosadetailid == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CheckDataPasienBiz.getVisitDiagnosisDetailId(visitdiagnosadetailid)).build();
	}
	
	@Override
	public Response getInfoVisitPasien(String VisitId)
	{
		if(VisitId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CheckDataPasienBiz.getInfoVisitPasien(VisitId)).build();
	}
	
	@Override
	public Response getInfoUserKthis(String userMstrId)
	{
		if(userMstrId==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CheckDataPasienBiz.getInfoUserKthis(userMstrId)).build();
	}

	@Override
	public Response getcheckdatapasienByBpjsNo(String bpjs) {
		if(bpjs == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CheckDataPasienBiz.getCheckDataPasienByBpjs(bpjs)).build();
	}
	
	@Override
	public Response getRegistrationOption() {
		return Response.ok(CheckDataPasienBiz.getRegistrationOption()).build();
	}
	
	@Override
	public Response getListDropDownCityMstr(String stateMstrId) {
		return Response.ok(CheckDataPasienBiz.getListDropDownCityMstr(stateMstrId)).build();
	}
	
	@Override
	public Response getListPasienByNameAndDate(String patientName, String birthDate){
		if(patientName == null || birthDate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CheckDataPasienBiz.getListPasienByNameAndDate(patientName, birthDate)).build();
	}
	
	@Override
	public Response getdatapasienByBpjsNo(String bpjs) {
		if(bpjs == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CheckDataPasienBiz.getDataPasienByBpjs(bpjs)).build();
	}
	
	@Override
	public Response registerNewPatient(NewPatient newPatient) {
		if(newPatient == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CheckDataPasienBiz.registerNewPatient(newPatient)).build();
	}
}