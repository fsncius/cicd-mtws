package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.PurchaseServiceBiz;
import com.rsmurniteguh.webservice.dep.services.IPurchaseService;

public class PurchaseService implements IPurchaseService {
	
	@Autowired
	private PurchaseServiceBiz PurchaseServiceBiz;

	@Override
	public Response AddPurchase(String purchaseNo) {
		// TODO Auto-generated method stub
		if(purchaseNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();		
		}
		return Response.ok(PurchaseServiceBiz.AddPurchase(purchaseNo)).build();
	}

	@Override
	public Response AddReceive(String receiptNo) {
		// TODO Auto-generated method stub
		if(receiptNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();		
		}
		return Response.ok(PurchaseServiceBiz.AddReceive(receiptNo)).build();
	}
	
	public Response AddReturn(String receiptNo) {
		// TODO Auto-generated method stub
		if(receiptNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();		
		}
		return Response.ok(PurchaseServiceBiz.AddReturn(receiptNo)).build();
	}

	public Response AddConsignment(String purchaseNo) {
		if (purchaseNo == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(PurchaseServiceBiz.AddConsignment(purchaseNo)).build();
	}
	
	public Response UnpostPurchase(String purchaseNo) {
		if (purchaseNo == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(PurchaseServiceBiz.UnpostPurchase(purchaseNo)).build();
	}
	
	public Response UnpostReceive(String receiptNo) {
		if (receiptNo == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(PurchaseServiceBiz.UnpostReceive(receiptNo)).build();
	}
}
