package com.rsmurniteguh.webservice.dep.services.impl;


import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.PDFUpload;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.CancelReg;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegBpjs;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegGeneral;
import com.rsmurniteguh.webservice.dep.biz.MtRegistrationBiz;
import com.rsmurniteguh.webservice.dep.services.IMtRegistration;
public class MtRegistrationService implements IMtRegistration{
	@Autowired
	private MtRegistrationBiz mtRegistrationBiz;

	@Override
	public Response getBaseInfo(String baseno) {
		if(baseno == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mtRegistrationBiz.getBaseInfo(baseno)).build();
	}
	
	@Override
	public Response insertRegKthisBpjs(InsertRegBpjs insertRegBpjs) {
		if(insertRegBpjs.getNoBpjs() == null || insertRegBpjs.getTglSep() == null || insertRegBpjs.getTglRujukan() == null || insertRegBpjs.getNoRujukan() == null || 
				insertRegBpjs.getPpkRujukan() == null || insertRegBpjs.getCatatan() == null || insertRegBpjs.getPoliTujuan() == null || insertRegBpjs.getKlsRawat() == null || 
				insertRegBpjs.getMrn() == null || insertRegBpjs.getCareProviderId() == null || insertRegBpjs.getPatientName() == null || insertRegBpjs.getTglBerobat() == null || 
				insertRegBpjs.getPatientId() == null || insertRegBpjs.getLocation() == null || insertRegBpjs.getGroupId() == null || insertRegBpjs.getUserid() == null || 
				insertRegBpjs.getNoBpjs() == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mtRegistrationBiz.insertRegKthisBpjs(insertRegBpjs)).build();
	}
	
	@Override
	public Response insertRegKthisGeneral(InsertRegGeneral insertRegGeneral) {
		if(insertRegGeneral.getCareProviderId() == null || insertRegGeneral.getPatient_name() == null || insertRegGeneral.getTgl_berobat() == null || insertRegGeneral.getPatientId() == null || 
				insertRegGeneral.getLocation() == null || insertRegGeneral.getGroup_id() == null || insertRegGeneral.getMrn() == null || insertRegGeneral.getUserid() == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mtRegistrationBiz.insertRegKthisGeneral(insertRegGeneral)).build();
	}
	
	@Override
	public Response insertRegKthisBpjs2(InsertRegBpjs insertRegBpjs) {
		if(insertRegBpjs.getNoBpjs() == null || insertRegBpjs.getTglSep() == null || insertRegBpjs.getTglRujukan() == null || insertRegBpjs.getNoRujukan() == null || 
				insertRegBpjs.getPpkRujukan() == null || insertRegBpjs.getCatatan() == null || insertRegBpjs.getPoliTujuan() == null || insertRegBpjs.getKlsRawat() == null || 
				insertRegBpjs.getMrn() == null || insertRegBpjs.getCareProviderId() == null || insertRegBpjs.getPatientName() == null || insertRegBpjs.getTglBerobat() == null || 
				insertRegBpjs.getPatientId() == null || insertRegBpjs.getLocation() == null || insertRegBpjs.getGroupId() == null || insertRegBpjs.getUserid() == null || 
				insertRegBpjs.getNoBpjs() == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mtRegistrationBiz.insertRegKthisBpjs2(insertRegBpjs)).build();
	}
	
	@Override
	public Response insertRegKthisGeneral2(InsertRegGeneral insertRegGeneral) {
		if(insertRegGeneral.getCareProviderId() == null || insertRegGeneral.getPatient_name() == null || insertRegGeneral.getTgl_berobat() == null || insertRegGeneral.getPatientId() == null || 
				insertRegGeneral.getLocation() == null || insertRegGeneral.getGroup_id() == null || insertRegGeneral.getMrn() == null || insertRegGeneral.getUserid() == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mtRegistrationBiz.insertRegKthisGeneral2(insertRegGeneral)).build();
	}
	
	@Override
	public Response cancelReg(CancelReg cancelReg) {
		if(cancelReg == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mtRegistrationBiz.cancelReg(cancelReg)).build();
	}
	
	@Override
	public Response getSep(String baseno) {
		if(baseno == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mtRegistrationBiz.getSep(baseno)).build();
	}
	
	@Override
	public Response sendSep(PDFUpload sep){
		if(sep == null){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mtRegistrationBiz.sendSep(sep)).build();
	}
	
}
