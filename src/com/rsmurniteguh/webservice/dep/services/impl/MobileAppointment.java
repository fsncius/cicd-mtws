package com.rsmurniteguh.webservice.dep.services.impl;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.services.IMobileAppointment;

public class MobileAppointment implements IMobileAppointment {
	@Autowired
	private MobileBiz mobileBiz;
	
	@Context
	HttpServletRequest request;

	@Override
	public Response appointmentHistory(String user_id, String mrn, String lokasi) {
		return Response.ok(mobileBiz.appointmentHistory(user_id,mrn,lokasi)).build();
	}
	
	@Override
	public Response appointmentList(String user_id, String mrn) {
		return Response.ok(mobileBiz.appointmentList(user_id,mrn)).build();
	}
	
	@Override
	public Response UserCreateApointment(String mrn, String resourcemstr, String user_id, String tanggal,
			String lokasi, String careprovider_id, String doctor_name, String no_subspecialis,String no_doctor, String patient_name, String no_mrn) {
		return Response.ok(mobileBiz.UserCreateApointment(mrn, resourcemstr, user_id, tanggal,
				lokasi, careprovider_id, doctor_name, no_subspecialis,no_doctor, patient_name, no_mrn)).build();
	}
	
	@Override
	public Response notificationAppointment(String tanggal) {
		return Response.ok(mobileBiz.notificationAppointment( tanggal)).build();
	}
	
	@Override
	public Response notificationDirect(String message, String user_id) {
		return Response.ok(mobileBiz.notificationDirect( message, user_id)).build();
	}
	
	@Override
	public Response cancelAppointment(String reg_id) {
		return Response.ok(mobileBiz.cancelAppointment(reg_id)).build();
	}
	
	@Override
	public Response checkAvailable(String tanggal, String user_id, String careprovider_id, String resourcemstr) {
		return Response.ok(mobileBiz.checkAvailable( tanggal,  user_id,  careprovider_id,  resourcemstr)).build();
	}

	@Override
	public Response InfoRegistration(String registrasi_id) {
		return Response.ok(mobileBiz.InfoRegistration(registrasi_id)).build();
	}
	
	@Override
	public Response getDoctorQueueList(String careproviderId, String subspecialtymstr) {
		return Response.ok(mobileBiz.getDoctorQueueList(careproviderId,subspecialtymstr)).build();
	}
	
	@Override
	public Response getAllAppointmentByStatus(String tanggal, String status) {
		return Response.ok(mobileBiz.getAllAppointmentByStatus(tanggal,status)).build();
	}
	
	@Override
	public Response topRegisteredAppointment(String tanggal) {
		return Response.ok(mobileBiz.topRegisteredAppointment(tanggal)).build();
	}
	
	@Override
	public Response getDataRujukan(String mrn) {
		return Response.ok(mobileBiz.getDataRujukan(mrn)).build();
	}
	
	@Override
	public Response addRujukan(String mrn, String userid, String no_rujukan, String tanggal_rujukan, String gambar) {
		return Response.ok(mobileBiz.addRujukan(mrn, userid, no_rujukan, tanggal_rujukan, gambar)).build();
	}
	
	@Override
	public Response uploadRujukan(List<Attachment> attachments, HttpServletRequest request) {
		return Response.ok(mobileBiz.uploadRujukan(attachments, request)).build();
	}
	
	@Override
	public Response downloadRujukan(String rujukan_id , String jenis) {
		return mobileBiz.downloadRujukan(rujukan_id, jenis, request);
	}
	
	

	
	
	
	
}
