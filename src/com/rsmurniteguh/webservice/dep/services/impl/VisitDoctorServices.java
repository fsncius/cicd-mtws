package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.VisitDocBiz;
import com.rsmurniteguh.webservice.dep.services.IVisitDoctor;

public class VisitDoctorServices implements IVisitDoctor{
	@Autowired
	private VisitDocBiz VisitDocBiz;
	
	@Override
	public Response getpaspul(String dadok) {
		if(dadok == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(VisitDocBiz.getVisitDoctor(dadok)).build();
	}
	
	@Override
	public Response getpasieninfo (String mrn)
	{
		if(mrn==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(VisitDocBiz.getpasieninfo(mrn)).build();
	}
	
	@Override
	public Response getinvispasdok(String mrnaja)
	{
		if(mrnaja==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(VisitDocBiz.getinvispasdok(mrnaja)).build();
	}
	
	@Override
	public Response getConDoc(String tglcek)
	{
		if(tglcek == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(VisitDocBiz.getConDoc(tglcek)).build();
		
	}
	
	@Override
	public Response getInfoData(String mrn)
	{
		if(mrn == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(VisitDocBiz.getInfoData(mrn)).build();
		
	}
	
	@Override
	public Response getBedHistory(String visitid)
	{
		if(visitid == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(VisitDocBiz.getBedHistory(visitid)).build();
		
	}
	
	@Override
	public Response getListFamily(String personid)
	{
		if(personid == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(VisitDocBiz.getListFamily(personid)).build();
		
	}
	
	@Override
	public Response getDataRgPasien(String mrn)
	{
		if(mrn == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(VisitDocBiz.getDataRgPasien(mrn)).build();
		
	}	

}
