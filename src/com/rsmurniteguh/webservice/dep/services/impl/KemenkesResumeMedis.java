package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.apache.http.util.TextUtils;

import com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis.InsertParam;
import com.rsmurniteguh.webservice.dep.biz.kemenkes.ResumeMedis;
import com.rsmurniteguh.webservice.dep.services.IKemenkesResumeMedis;

public class KemenkesResumeMedis implements IKemenkesResumeMedis{
	public String getNik() {
		return null;
	}
	public void setNik(String nik) {
		
	}
	
	@Override
	public Response insert(InsertParam param) {
		if(TextUtils.isEmpty(getNik()))return Response.status(Response.Status.BAD_REQUEST).build();
		ResumeMedis resumeMedis = new ResumeMedis(getNik());
		return Response.ok(resumeMedis.insert(param)).build();
	}

	@Override
	public Response get(String tanggalAwal, String tanggalAkhir) {
		if(TextUtils.isEmpty(getNik()))return Response.status(Response.Status.BAD_REQUEST).build();
		ResumeMedis resumeMedis = new ResumeMedis(getNik());
		return Response.ok(resumeMedis.get(tanggalAwal, tanggalAkhir)).build();
	}

}
