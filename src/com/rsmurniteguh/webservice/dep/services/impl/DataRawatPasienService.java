package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.DataRawatPasienBiz;
import com.rsmurniteguh.webservice.dep.services.IDataRawatPasien;

public class DataRawatPasienService implements IDataRawatPasien{
	@Autowired
	private DataRawatPasienBiz DataRawatPasienBiz;
	
	@Override
	public Response getdatarawatpasien(String cardno)
	{
		if(cardno == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DataRawatPasienBiz.getDataRawatPasien(cardno)).build();
	}
	
	public Response getinfopasien(String CardNo)
	{
		if(CardNo==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DataRawatPasienBiz.getinfopasien(CardNo)).build();
	}
	
	public Response getdatapasien(String CardNo)
	{
		if(CardNo==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DataRawatPasienBiz.getdatapasien(CardNo)).build();
	}
	
	public Response getvisitprotokolhd(String visitid)
	{
		if(visitid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(DataRawatPasienBiz.getvisitprotokolhd(visitid)).build();
	}
}
