package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.Doctorbiz;
import com.rsmurniteguh.webservice.dep.services.IDoctorService;

public class DoctorService implements IDoctorService {
	@Autowired
	private Doctorbiz doctorBiz;
	
	@Override
	public Response getDataDoctor  (String idDok)
	{
		if(idDok == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(doctorBiz.getDataDoctor(idDok)).build();
	}
	
	@Override
	public Response GetMapBpjs  ()
	{
		return Response.ok(doctorBiz.getDataPoly()).build();
	}
	

}
