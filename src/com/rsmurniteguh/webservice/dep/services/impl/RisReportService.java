package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.RisReportBiz;
import com.rsmurniteguh.webservice.dep.services.IRisReportService;

public class RisReportService implements IRisReportService {
	
	@Autowired
	private RisReportBiz RisReportBiz;
	
	@Override
	public Response getrisreport(String perstartdate)
	{
		if(perstartdate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(RisReportBiz.getrisreport(perstartdate)).build();
	}
	
	@Override
	public Response getrisadmission(String admissionid)
	{
		if(admissionid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(RisReportBiz.getrisadmission(admissionid)).build();
	}

	@Override
	public Response getRisByPatient(String patientName, String patientId) {
		if(patientName==null && patientId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(RisReportBiz.getrisreportByPatient(patientName, patientId)).build();
	}

	@Override
	public Response getxrayrisreport(String perstartdate) {
		if(perstartdate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(RisReportBiz.getxrayrisreport(perstartdate)).build();
	}

	@Override
	public Response getxrayrisreportadmission(String admissionid) {
		if(admissionid == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(RisReportBiz.getxrayrisreportadmission(admissionid)).build();
	
	}

	@Override
	public Response getrisadmissionlist(String admissionidlist)
	{
		if(admissionidlist==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(RisReportBiz.getrisadmissionlist(admissionidlist)).build();
	}

	@Override
	public Response getrisreportadmission(String admissionid) {
		if(admissionid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(RisReportBiz.getrisreportadmission(admissionid)).build();
	}
	
}
