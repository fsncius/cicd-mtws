package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.UserBiz;
import com.rsmurniteguh.webservice.dep.services.IUserService;

public class UserService implements IUserService{

	@Autowired
	private UserBiz userBiz;
	
	@Override
	public Response checklogin(String user, String pass) {
		if(user == null || pass == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}		
		return Response.ok(userBiz.checklogin(user, pass)).build();
	}
	
	@Override
	public Response change_password_kthis(String usermstrId, String password, String new_password) {
		if(usermstrId == null || password == null || new_password == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}		
		return Response.ok(userBiz.change_password_kthis(usermstrId, password, new_password)).build();
	}
	
	@Override
	public Response change_password_patient(String usermstrId, String password, String new_password){
		if(usermstrId == null || password == null || new_password == null){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(userBiz.change_password_patient(usermstrId, password, new_password)).build();
	}
	
	@Override
	public Response change_password_hris(String idkaryawan, String password, String new_password) {
		if(idkaryawan == null || password == null || new_password == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}		
		return Response.ok(userBiz.change_password_hris(idkaryawan, password, new_password)).build();
	}
	
	@Override
	public Response check_login_hris(String username, String password) {
		if(username == null || password == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}		
		return Response.ok(userBiz.check_login_hris(username, password)).build();
	}
}
