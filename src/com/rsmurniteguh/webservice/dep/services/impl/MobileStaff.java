package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.services.IMobileStaff;
import com.rsmurniteguh.webservice.dep.util.CommonUtil;

public class MobileStaff implements IMobileStaff{

	@Context 
	HttpHeaders headers;
	
	@Autowired
	private MobileBiz mobilityBiz;
	
	@Override
	public Response GetDoctorRegistrationList(String usermstrId) {
		return Response.ok(mobilityBiz.GetDoctorRegistrationList(usermstrId)).build();
	}

	@Override
	public Response GetDoctorIPVisitList(String usermstrId) {
		return Response.ok(mobilityBiz.GetDoctorIPVisitList(usermstrId)).build();
	}

	@Override
	public Response GetRadiologySchedule(String date, String type) {
		return Response.ok(mobilityBiz.GetRadiologySchedule(date, type)).build();
	}
	
	@Override
	public Response getPurchaseRequisition() {
		return Response.ok(mobilityBiz.getPurchaseRequisition()).build();
	}
	
	@Override
	public Response setPurchaseRequisition(String stockpurchase_requisitionid,String isconfirm) {
		String userMstrId = CommonUtil.getInstance().getUserMstrIdFromAuth(headers);
		return Response.ok(mobilityBiz.setPurchaseRequisition(stockpurchase_requisitionid, isconfirm, userMstrId)).build();
	}
	
	@Override
	public Response getRisReportTime(String tanggal, String jenis) {
		return Response.ok(mobilityBiz.getRisReportTime(tanggal,jenis)).build();
	}
}