package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.hris.CreateCuti;
import com.rsmurniteguh.webservice.dep.all.model.hris.RequestCreateIzin;
import com.rsmurniteguh.webservice.dep.biz.HrisBiz;
import com.rsmurniteguh.webservice.dep.services.IHris;


public class HrisService implements IHris{
	@Autowired
	private HrisBiz hrisBiz;
	
	@Override
	public Response get_employe_detail(String id_karyawan) {
		if(id_karyawan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_employe_detail(id_karyawan)).build();
	}
	
	@Override
	public Response get_employe_attendance(String finger_id,String kodemesin, String startdate, String enddate) {
		if(finger_id == null || startdate == null || enddate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_employe_attendance(finger_id,kodemesin,startdate,enddate)).build();
	}
	
	@Override
	public Response get_izin_data(String id_karyawan,String startdate, String enddate) {
		if(id_karyawan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_izin_data(id_karyawan, startdate, enddate)).build();
	}
	
	@Override
	public Response get_cuti_data(String id_karyawan,String startdate, String enddate) {
		if(id_karyawan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_cuti_data(id_karyawan, startdate, enddate)).build();
	}
	
	@Override
	public Response get_data_request_task_cuti(String id_karyawan) {
		if(id_karyawan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_data_request_task_cuti(id_karyawan)).build();
	}
	
	@Override
	public Response view_create_cuti(String id_karyawan) {
		if(id_karyawan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.view_create_cuti(id_karyawan)).build();
	}
	
	@Override
	public Response create_cuti(CreateCuti createCuti) {
		if(createCuti == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.create_cuti(createCuti)).build();
	}
	
	@Override
	public Response update_cuti(CreateCuti createCuti) {
		if(createCuti == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.update_cuti(createCuti)).build();
	}
	
	@Override
	public Response delete_cuti(String kodecuti) {
		if(kodecuti == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.delete_cuti(kodecuti)).build();
	}
	
	@Override
	public Response get_cuti_info(String kodecuti) {
		if(kodecuti == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_cuti_info(kodecuti)).build();
	}
	
	@Override
	public Response get_atasan(String id_karyawan) {
		if(id_karyawan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_atasan(id_karyawan)).build();
	}
	
	@Override
	public Response get_bawahan(String kode_jabatan) {
		if(kode_jabatan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_bawahan(kode_jabatan)).build();
	}
	
	@Override
	public Response konfirmasi_izin_cuti(String id, String alasan, String status, String tipe, String idkaryawan, String kodepermohonan) {
		if( id == null ||   alasan == null || status == null || tipe == null || idkaryawan == null || kodepermohonan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.konfirmasi_izin_cuti(id, alasan, status, tipe, idkaryawan, kodepermohonan)).build();
	}
	
	public Response create_izin_hris(RequestCreateIzin izin) {
		return Response.ok(hrisBiz.create_izin_hris(izin)).build();
	}
	
	@Override
	public Response update_izin_hris(RequestCreateIzin izin) {
		return Response.ok(hrisBiz.update_izin_hris(izin)).build();
	}
	
	@Override
	public Response delete_izin_hris(RequestCreateIzin izin) {
		return Response.ok(hrisBiz.delete_izin_hris(izin)).build();
	}
	
	@Override
	public Response get_request_approval(String id_karyawan,String status) {
		if(id_karyawan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_request_approval(id_karyawan, status)).build();
	}
	
	@Override
	public Response get_berita(String reader_role) {
		if(reader_role == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_berita(reader_role)).build();
	}
	
	@Override
	public Response get_login_hris(String username, String password) {
		if(username == null || password == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.get_login_hris(username,password)).build();
	}
		
	@Override
	public Response GetEmoney(String id_karyawan) {
		if(id_karyawan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.GetEmoney(id_karyawan)).build();
	}
	
	@Override
	public Response HitungHariCuti(String id_karyawan,String startdate, String enddate) {
		if(id_karyawan == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.HitungHariCuti(id_karyawan, startdate, enddate)).build();
	}
	
	@Override
	public Response add_kotak_saran(String user_id,String user_name, String project_code, String version, String saran, String kontak) {
		if(user_id == null || project_code == null || version == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(hrisBiz.add_kotak_saran(user_id,user_name,project_code,version, saran, kontak)).build();
	}

	public Response authPass(){
		return Response.ok(hrisBiz.authPass()).build();
	}
}
