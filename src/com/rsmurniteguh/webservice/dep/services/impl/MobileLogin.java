package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.services.IMobileLogin;

public class MobileLogin implements IMobileLogin {
	@Autowired
	private MobileBiz mobilityBiz;

	@Override
	public Response CheckLogin(String user, String pass) {
		return Response.ok(mobilityBiz.CheckLogin(user, pass)).build();
	}

	@Override
	public Response change_password_patient(String usermstrId, String password, String new_password) {
		if (usermstrId == null || password == null || new_password == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mobilityBiz.change_password_patient(usermstrId, password, new_password)).build();
	}

	@Override
	public Response CheckLoginStaff(String user, String pass) {
		return Response.ok(mobilityBiz.CheckLoginStaff(user, pass)).build();
	}

	@Override
	public Response CheckLoginPatient(String user, String pass) {
		return Response.ok(mobilityBiz.CheckLoginPatient(user, pass)).build();
	}

	@Override
	public Response UserCreate(String mrn, String username, String password, String tgl_lahir) {
		return Response.ok(mobilityBiz.UserCreate(mrn, username, password, tgl_lahir)).build();
	}

	@Override
	public Response UserGet(String username) {
		return Response.ok(mobilityBiz.UserGet(username)).build();
	}

	@Override
	public Response UserUpdate(String mrn, String username, String password, String tgl_lahir,
			String token) {
		return Response.ok(mobilityBiz.UserUpdate(mrn, username, password, tgl_lahir, token)).build();
	}

	@Override
	public Response UserDelete(String username) {
		return Response.ok(mobilityBiz.UserDelete(username)).build();
	}

	@Override
	public Response SearchDoctor(String searchtext, String lokasi) {
		return Response.ok(mobilityBiz.SearchDoctor(searchtext, lokasi)).build();
	}

	@Override
	public Response getInfoPatient(String mrn, String tgl_lahir, String username) {
		return Response.ok(mobilityBiz.getInfoPatient(mrn, tgl_lahir, username)).build();
	}

	@Override
	public Response confirmPatient(String mrn, String tgl_lahir, String pass) {
		return Response.ok(mobilityBiz.confirmPatient(mrn, tgl_lahir, pass)).build();
	}

	@Override
	public Response notificationPublic(String tanggal) {
		return Response.ok(mobilityBiz.notificationPublic(tanggal)).build();
	}

	@Override
	public Response simple_get_poly() {
		return Response.ok(mobilityBiz.simple_get_poly()).build();
	}
}
