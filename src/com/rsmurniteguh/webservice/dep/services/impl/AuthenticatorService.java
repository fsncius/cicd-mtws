package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.AuthenticatorBiz;
import com.rsmurniteguh.webservice.dep.services.IAuthenticatorService;

public class AuthenticatorService implements IAuthenticatorService{
	@Autowired
	private AuthenticatorBiz authenticatorBiz;

	@Override
	public Response PaymentTransaction(String user, String token) {
		return Response.ok(authenticatorBiz.Authenticate(user, token)).build();
	}
}