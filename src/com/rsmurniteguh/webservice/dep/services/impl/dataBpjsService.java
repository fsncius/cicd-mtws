package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.sql.Blob;

import com.rsmurniteguh.webservice.dep.all.model.UploadResume;
import com.rsmurniteguh.webservice.dep.biz.dataBpjsBiz;
import com.rsmurniteguh.webservice.dep.services.IdataBpjs;

public class dataBpjsService implements IdataBpjs {
	@Autowired
	private dataBpjsBiz dataBpjsBiz;
	
	@Override
	public Response getDataBpjsNo(String nobpjs)
	{
		if(nobpjs==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getDataBpjsNo(nobpjs)).build();
	}
	
	@Override
	public Response getCardNo(String cardno)
	{
		if(cardno==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getCardNo(cardno)).build();
	}
	
	public Response getBaseInfo(String mrn)
	{
		if(mrn==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getBaseInfo(mrn)).build();
	}
	
	@Override
	public Response getPolyResourceMstrByUser(String usermasterid)
	{
		if(usermasterid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getPolyResourceMstrByUser(usermasterid)).build();
	}
	
	@Override
	public Response getPatientByQueueNo(String queueno)
	{
		if(queueno == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getPatientByQueueNo(queueno)).build();
	}
	
	public Response getPatientByQueueNoUpdate(String queueno, String subspeciality)
	{
		if(queueno == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getPatientByQueueNoUpdate(queueno,subspeciality)).build();
	}
	
	@Override
	public Response getPharmacyResponseTime(String tglAwal,String tglAkhir)
	{
		if(tglAwal == null || tglAkhir==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getPharmacyResponseTime(tglAwal,tglAkhir)).build();
	}
	
	@Override
	public Response getPatientByQueueNo2(String queueno2)
	{
		if(queueno2==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getPatientByQueueNo2(queueno2)).build();
	}
	
	@Override
	public Response getPatientByQueueNo2Update(String queueno2, String subspeciality)
	{
		if(queueno2==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getPatientByQueueNo2Update(queueno2,subspeciality)).build();
	}
	
	@Override
	public Response getSelfRegistration(String cardno, String careproviderid, String noantri, String userid)
	{
		if(cardno==null || careproviderid==null || noantri==null || userid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getSelfRegistration(cardno,careproviderid,noantri,userid)).build();
	}
	
	public Response getInsBrid(String cardno,String bpjsno,String sepno,String vid,String careproviderid, String noantri,String userid)
	{
		if(cardno==null || bpjsno==null || sepno==null || vid==null || careproviderid==null || noantri==null || userid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getInsBrid(cardno,bpjsno,sepno,vid,careproviderid,noantri,userid)).build();
	}
	
	public Response getInsSign(String mrid,String vid,String sign,String userid)
	{
		if(mrid==null || vid==null || sign==null || userid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getInsSign(mrid,vid,sign,userid)).build();
	}
	
	public Response sendFileResume(UploadResume resumeform)
	{
		return Response.ok(dataBpjsBiz.sendFileResume(resumeform)).build();
	}
	
	public Response getSelfRegistration2(String cardno, String careproviderid, String noantri, String userid)
	{
		if(cardno==null || careproviderid==null || noantri==null || userid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getSelfRegistration2(cardno,careproviderid,noantri,userid)).build();
	}
	
	public Response registrationInfo(String regno)
	{
		if(regno==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.registrationInfo(regno)).build();
	}
	
	@Override
	public Response addBpjsQueue(String queue_no)
	{
		if(queue_no==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(dataBpjsBiz.addBpjsQueue(queue_no)).build();
	}
	
	public Response GetRegData(String regno)
	{
		if(regno==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(dataBpjsBiz.GetRegData(regno)).build();
	}
	
	@Override
	public Response SearchDoctor(String searchtext,String lokasi)
	{
		if(searchtext==null || lokasi ==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();			
		}
		return Response.ok(dataBpjsBiz.SearchDoctor(searchtext,lokasi)).build();
	}
	
	
	
	@Override
	public Response getCekTxn(String VisitId)
	{
		if(VisitId==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}		
		return Response.ok(dataBpjsBiz.getCekTxn(VisitId)).build();
	}
	
	@Override
	public Response getlisttxn(String visitid)
	{
		if(visitid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getlisttxn(visitid)).build();
			
		}
	}

	@Override
	public Response getcekvalidpas(String bpjsno, String namapas, String tglahir) {
		// TODO Auto-generated method stub
		if(bpjsno == null || namapas == null || tglahir ==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getcekvalidpas(bpjsno,namapas,tglahir)).build();
		}
	}
	
	@Override
	public Response getcancelreg(String userid, String visitid, String remarks,  String cancelreason)
	{
		if(userid == null || visitid == null || remarks == null || cancelreason==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getcancelreg(userid, visitid, remarks, cancelreason)).build();
		}
	}
	
	@Override
	public Response getcancelQueue(String cardno, String tgl_berobat)
	{
		if(cardno == null || tgl_berobat == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getcancelQueue(cardno, tgl_berobat)).build();
		}
	}
	
	@Override
	public Response getcodecar()
	{
		return Response.ok(dataBpjsBiz.getcodecar()).build();
	}
	
	public Response getpolydoc(String idhari)
	{
		return Response.ok(dataBpjsBiz.getpolydoc(idhari)).build();
	}
	
	public Response getpolybpjsdoc(String idhari)
	{
		return Response.ok(dataBpjsBiz.getpolybpjsdoc(idhari)).build();
	}
	
	public Response getpolydoc3(String idhari)
	{
		return Response.ok(dataBpjsBiz.getpolydoc3(idhari)).build();
	}
	
	public Response getpolydoc2(String idhari,String poli)
	{
		return Response.ok(dataBpjsBiz.getpolydoc2(idhari,poli)).build();
	}
	
	public Response getpolyname(String idhari)
	{
		return Response.ok(dataBpjsBiz.getpolyname(idhari)).build();
	}
	
	public Response gettotal(String idhari)
	{
		if(idhari == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();		
		}
		else
		{
			return Response.ok(dataBpjsBiz.gettotal(idhari)).build();
		}
	}
	
	@Override
	public Response getcekvalidantrian(String CardNo, String IdNo, String BpjsNo)
	{
		if(CardNo== null || IdNo == null || BpjsNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getcekvalidantrian(CardNo, IdNo, BpjsNo)).build();
		}
	}
	
	@Override
	public Response getResourceMaster(String ResourceName)
	{
		if(ResourceName == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getResourceMaster(ResourceName)).build();
		}
	}
	
	@Override
	public Response getResourceMaster2(String ResourceName)
	{
		if(ResourceName == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getResourceMaster2(ResourceName)).build();
		}
	}
	
	@Override
	public Response getResourceMaster3(String ResourceId)
	{
		if(ResourceId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getResourceMaster3(ResourceId)).build();
		}
	}
	
	@Override
	public Response getResourceMasterjaddok()
	{
		return Response.ok(dataBpjsBiz.getResourceMasterjaddok()).build();
	}
	
	@Override
	public Response getResourceScheme(String ResourceMstrId, String RegnDate)
	{
		if(ResourceMstrId == null || RegnDate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getResourceScheme(ResourceMstrId,RegnDate)).build();
		}
	}
	
	@Override
	public Response getPatientId(String CardNo)
	{
		if(CardNo == null || CardNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getPatientId(CardNo)).build();
		}
	}
	

	@Override
	public Response GetResDokter(String CareProviderId)
	{
		if(CareProviderId==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetResDokter(CareProviderId)).build();
		}
	}
	
	public Response GetDetailDokter(String dokterId)
	{
		if(dokterId==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetDetailDokter(dokterId)).build();
		}
	}
	
	@Override
	public Response GetQueueBpjs(String CounterId)
	{
		if(CounterId==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetQueueBpjs(CounterId)).build();
		}
	}
	
	@Override
	public Response GetQueueUser(String UserMstrId)
	{
		if(UserMstrId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetQueueUser(UserMstrId)).build();
		}
	}
	
	@Override
	public Response GetOnlineReg(String Barcode)
	{
		if(Barcode == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetOnlineReg(Barcode)).build();
		}
	}
	
//	@Override
//	public Response GetOnlineReg2(String Barcode)
//	{
//		if(Barcode == null)
//		{
//			return Response.status(Response.Status.BAD_REQUEST).build();
//		}
//		else
//		{
////			return Response.ok(dataBpjsBiz.GetOnlineReg2(Barcode)).build();
//		}
//	}
	
	@Override
	public Response GetDietPasien(String Mrn)
	{
		if(Mrn == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetDietPasien(Mrn)).build();
		}
	}
	
	@Override
	public Response GetKelasPasien(String Pc)
	{
		if(Pc == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetKelasPasien(Pc)).build();
		}
	}
	
	@Override
	public Response GetKamarPasien(String Vid)
	{
		if(Vid == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetKamarPasien(Vid)).build();
		}
	}
	
	public Response GetVisitId(String mrn)
	{
		if(mrn == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetVisitId(mrn)).build();
		}
	}
	
	@Override
	public Response GetPoliCode()
	{
		return Response.ok(dataBpjsBiz.GetPoliCode()).build();
	}
	
	@Override
	public Response GetUserName(String UserCode)
	{
		if(UserCode== null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetUserName(UserCode)).build();
		}
	}
	
	@Override
	public Response GetTindakan(String VisitId)
	{
		if(VisitId== null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetTindakan(VisitId)).build();
		}
	}

	@Override
	public Response getDailyDoctorSchedule(String RegnDate) {
		if(RegnDate== null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getDailyDoctorSchedule(RegnDate)).build();
		}
	}
	
	@Override
	public Response GetReceipt(String SysReceptNo){
		if(SysReceptNo==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetReceipt(SysReceptNo)).build();
		}
	}
	@Override
	public Response ListKamar(){
			return Response.ok(dataBpjsBiz.ListKamar()).build();
	}
	
	@Override
	public Response ListRuangan(){
			return Response.ok(dataBpjsBiz.ListRuangan()).build();
	}
	
	public Response ListPasien(String Ruangan,String Tipe){
		if(Ruangan==null || Tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.ListPasien(Ruangan,Tipe)).build();
		}
	}
	
	public Response GetSignatureReport(String mrid){
		if(mrid==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetSignatureReport(mrid)).build();
		}
	}
	
	public Response GetMedicalResume(String MedicalResumeId, String Ruangan,String Tipe){
		if(MedicalResumeId ==null  || Ruangan==null || Tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetMedicalResume(MedicalResumeId,Ruangan,Tipe)).build();
		}
	}
	
	public Response getMedicalResumeByMrnNo(String mrnNo) {
		if(mrnNo ==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getMedicalResumeByMrnNo(mrnNo)).build();
		}		
	}
	
	public Response GetMedicalResumeById(String resumeId) {
		if(resumeId ==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetMedicalResumeById(resumeId)).build();
		}		
	}
	
	public Response getListMedicalVisitByMRN(String mrn) {
		if(mrn ==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getListMedicalVisitByMRN(mrn)).build();
		}		
	}
	
	public Response getListPatientMedicalResumeByUserMstrId(String userMstrId) {
		if(userMstrId ==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.getListPatientMedicalResumeByUserMstrId(userMstrId)).build();
		}		
	}
	
	public Response GetDataKartu(String bpjsNo){
		if(bpjsNo ==null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetDataKartu(bpjsNo)).build();
		}
	}
	
	public Response GetBpjsMedicine(String vid){
		if(vid ==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetBpjsMedicine(vid)).build();
		}
	}
	
	@Override
	public Response GetLabReport(String mrn){
			return Response.ok(dataBpjsBiz.GetLabReport(mrn)).build();
	}
	
	@Override
	public Response GetPatientHiv(String startdate, String enddate){
		if(startdate==null || enddate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(dataBpjsBiz.GetPatientHiv(startdate,enddate)).build();
		}
	}

	@Override
	public Response getDoctorScheduleDay(String idhari, String subspecialty_id) {
		if(idhari==null || subspecialty_id == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.getDoctorScheduleDay(idhari,subspecialty_id)).build();
	}
	
	@Override
	public Response checkResourceStop(BigDecimal resourceMstrId, String regDate) {
		if(resourceMstrId==null || regDate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(dataBpjsBiz.checkResourceStop(resourceMstrId, regDate)).build();
	}
	
}
