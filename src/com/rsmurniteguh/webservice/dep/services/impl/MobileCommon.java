package com.rsmurniteguh.webservice.dep.services.impl;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.services.IMobileCommon;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

public class MobileCommon implements IMobileCommon{

	@Autowired
	private MobileBiz mobilityBiz;
	
	@Override
	public Response getWhatsNew(String projectCode, String version){
		return Response.ok(mobilityBiz.getWhatsNewList(projectCode, version)).build();
	}
	
	@Override
	public Response getAndroidVersion(String projectCode){
		return Response.ok(mobilityBiz.getAndroidVersion(projectCode)).build();
	}

	@Override
	public Response get_berita(String reader_role) {
		return Response.ok(mobilityBiz.get_berita(reader_role)).build();
	}
	
	@Override
	public Response add_kotak_saran(String user_id,String user_name, String project_code, String version, String saran, String kontak) {
		if(user_id == null || project_code == null || version == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mobilityBiz.add_kotak_saran(user_id,user_name,project_code,version, saran, kontak)).build();
	}
	
	@Override
	public Response getBpjsData(String noKartuBpjs) {
		if(noKartuBpjs == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mobilityBiz.getBpjsData(noKartuBpjs)).build();
	}
}