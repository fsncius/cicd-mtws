package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.AuthenticatorBiz;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.services.IMobileDoctor;

public class MobileDoctor implements IMobileDoctor {
	
	@Autowired
	private MobileBiz mobilityBiz;
	
	@Autowired
	private AuthenticatorBiz authenticatorBiz;
	
	// old mobility
	@Override
	public Response get_trx_emoney(String nik, String startdate, String enddate) {
		if(nik == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}		
		return Response.ok(authenticatorBiz.get_trx_emoney(nik,startdate,enddate)).build();
	}
	
	@Override
	public Response getMainDataDoctor(String usermstr_id) {
		if(usermstr_id == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}		
		return Response.ok(authenticatorBiz.getMainDataDoctor(usermstr_id)).build();
	}

	// end of old mobility
	@Override
	public Response GetPoliclynicLists() {
		return Response.ok(mobilityBiz.GetPoliclynicLists()).build();
	}

	@Override
	public Response GetPoliclynicListsByLocation(String location) {
		return Response.ok(mobilityBiz.GetPoliclynicListsByLocation(location)).build();
	}

	@Override
	public Response GetDoctorsList(String poli, String location) {
		return Response.ok(mobilityBiz.GetDoctorsList(poli, location)).build();
	}

	@Override
	public Response GetDoctorSchedule(String resourcemstr) {
		return Response.ok(mobilityBiz.GetDoctorSchedule(resourcemstr)).build();
	}

	@Override
	public Response getResourceScheme(String resourcemstr, String tanggal) {
		return Response.ok(mobilityBiz.getResourceScheme(resourcemstr, tanggal)).build();
	}

	@Override
	public Response get_doctorlist_schedule(String poli) {
		return Response.ok(mobilityBiz.get_doctorlist_schedule(poli)).build();
	}

	@Override
	public Response getDoctorListScheduleByName(String name) {
		return Response.ok(mobilityBiz.getDoctorListScheduleByName(name)).build();
	}

	@Override
	public Response GetDoctorsPolyName(String poli, String location) {
		return Response.ok(mobilityBiz.GetDoctorsPolyName(poli, location)).build();
	}
}
