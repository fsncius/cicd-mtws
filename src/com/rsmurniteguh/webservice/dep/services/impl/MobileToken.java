package com.rsmurniteguh.webservice.dep.services.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.UploadResume;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataAppointment;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataRujukan;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.services.IMobileToken;


public class MobileToken implements IMobileToken{
	@Autowired
	private MobileBiz mobilityBiz;
	
	@Context
	HttpServletRequest request;
	
	@Override
	public Response SaveRegistrationToken(String usermstrId, String token) {
		return Response.ok(mobilityBiz.SaveRegistrationToken(usermstrId, token)).build();
	}
	
	@Override
	public Response uploadRujukan(List<Attachment> attachments, HttpServletRequest request) {
		return Response.ok(mobilityBiz.uploadRujukan(attachments, request)).build();
	}
	
	@Override
	public Response downloadRujukan(String rujukan_id , String jenis) {
		return mobilityBiz.downloadRujukan(rujukan_id, jenis, request);
	}


	
	
	@Override
	public Response updateDataUser(String fullname,String idcard, String bpjs,  String contact,  String email, String userid) {
		if(userid == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mobilityBiz.updateDataUser(fullname,idcard, bpjs, contact, email, userid)).build();
	}
	
	@Override
	public Response checkAvalibaleRujukan(String mrn) {
		if(mrn == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mobilityBiz.checkAvalibaleRujukan(mrn)).build();
	}
	
	@Override
	public Response checkAvalibaleAppointment(String nomrn, String tglappointment, String resourcemstrid) {
		return Response.ok(mobilityBiz.checkAvalibaleAppointment(nomrn,tglappointment,resourcemstrid)).build();
	}
	
	@Override
	public Response getRujukan(String mrn) {
		return Response.ok(mobilityBiz.getRujukan(mrn)).build();
	}
	
	@Override
	public Response getRujukanTambahan(String mrn) {
		return Response.ok(mobilityBiz.getRujukanTambahan(mrn)).build();
	}
	
	@Override
	public Response createAppointment(DataAppointment dataform)
	{
		return Response.ok(mobilityBiz.createAppointment(dataform)).build();
	}
	
	@Override
	public Response createRujukan(DataRujukan dataform)
	{
		return Response.ok(mobilityBiz.createRujukan(dataform)).build();
	}
	
	@Override
	public Response createRujukanTambahan(DataRujukan dataform)
	{
		return Response.ok(mobilityBiz.createRujukanTambahan(dataform)).build();
	}
	
	
	@Override
	public Response cancelAppointment(String ids){
		return Response.ok(mobilityBiz.cancelAppointment(ids)).build();
	}
	
	
	@Override
	public Response getInfoReject(String ids){
		return Response.ok(mobilityBiz.getInfoReject(ids)).build();
	}
	
	@Override
	public Response validNomorRujukan(String ids, String mrn){
		return Response.ok(mobilityBiz.validNomorRujukan(ids,mrn)).build();
	}
	
	
	@Override
	public Response getAppointmentbyId(String ids){
		return Response.ok(mobilityBiz.getAppointmentbyId(ids)).build();
	}
	
	@Override
	public Response getAppointment(String mrn,String departement, String source,String tipe){
		return Response.ok(mobilityBiz.getAppointment(mrn,departement,source,tipe)).build();
	}
}
