package com.rsmurniteguh.webservice.dep.services.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.all.model.JobsRecruitmentRegistration;
import com.rsmurniteguh.webservice.dep.all.model.RegistrasiNewPatient;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegBpjs;
import com.rsmurniteguh.webservice.dep.biz.CheckDataPasienBiz;
import com.rsmurniteguh.webservice.dep.biz.MTWebsiteBiz;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.services.IMTWebsite;

public class MTWebsiteService implements IMTWebsite {
	
	
	@Autowired
	private MTWebsiteBiz mTWebsiteBiz;
	
	@Context 
	UriInfo uriInfo;
	
	@Context 
	HttpHeaders headers;
	
	@Context
	HttpServletRequest request;
	
	@Override
	public Response upload(String type, String name, List<Attachment> attachment, HttpServletRequest request ) {
		/*
		String urls = uriInfo.getRequestUri().toString();
		String[] urlarr = urls.split("&name=");
		String urld = urls.replace(urlarr[1], "");
		String[] urlarry = urld.split("&type=");
		String type = urlarry[1];
		String name = urlarr[1];
		*/
		if(type == null && name == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.upload(type, name, attachment, request)).build();
	}

	@Override
	public Response download(String gambar_id, String type) {
		if(gambar_id == null || type == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return mTWebsiteBiz.download(gambar_id, type, request);
	}
	
	@Override
	public Response getBerita(String kategori, String type, String tags, String status) {
		if(kategori == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getBerita(kategori, type, tags, status)).build();
	}
	
	@Override
	public Response getJudulBerita(String judul, String kategori) {
		if(judul == null || kategori == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getJudulBerita(judul, kategori)).build();
	}
	
	@Override
	public Response getSaveViewerBerita(String ids, String jumlah) {
		if(ids == null || jumlah == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getSaveViewerBerita(ids, jumlah)).build();
	}
	
	@Override
	public Response getMegazine() {
		return Response.ok(mTWebsiteBiz.getMegazine()).build();
	}
	
	@Override
	public Response SaveReadOnline(String firstname, String lastname, String email, String phone){
		if(firstname == null && lastname == null && email == null  && phone == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.SaveReadOnline(firstname,lastname,email,phone)).build();
	}
	
	@Override
	public Response SaveKonsultasi(String fullname,String email, String phone, String gender, String blood, String height, String weight, String age, String jobs, String complaint, String spesialis){
		if(fullname == null && gender == null && email == null  && phone == null&& age == null&& spesialis== null && complaint == null && jobs == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.SaveKonsultasi(fullname,email,phone,gender,blood,height,weight,age,jobs,complaint,spesialis)).build();
	}
	
	@Override
	public Response getWhatNew(String code, String version, String type){
		if(code == null && version == null && type == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getWhatNew(code, version, type)).build();
	}

	
	@Override
	public Response getTahunGallery() {
		return Response.ok(mTWebsiteBiz.getTahunGallery()).build();
	}
	
	@Override
	public Response getJenisGallery() {
		return Response.ok(mTWebsiteBiz.getJenisGallery()).build();
	}
	
	@Override
	public Response getGallerybyTahun(String tahun){
		if(tahun == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getGallerybyTahun(tahun)).build();
	}

	@Override
	public Response getGallerybyJudul(String judul){
		if(judul == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getGallerybyJudul(judul)).build();
	}
	
	@Override
	public Response uploadRujukanPasien(List<Attachment> attachments, HttpServletRequest request) {
		return Response.ok(mTWebsiteBiz.uploadRujukanPasien(attachments, request)).build();
	}
	
	@Override
	public Response addPasienBaru(RegistrasiNewPatient registrasiNewPatient) {
		if(registrasiNewPatient.getFullName() == null || registrasiNewPatient.getBirth() == null || registrasiNewPatient.getEmail() == null || registrasiNewPatient.getPhone() == null || 
				registrasiNewPatient.getAgama() == null || registrasiNewPatient.getJenisKelamin() == null || registrasiNewPatient.getEtnis() == null || 
				registrasiNewPatient.getPendidikan() == null || registrasiNewPatient.getAlamat() == null || registrasiNewPatient.getKota() == null || registrasiNewPatient.getProvinsi() == null || 
				registrasiNewPatient.getPasienType() == null || registrasiNewPatient.getPasienJenis() == null ||registrasiNewPatient.getResourceMstrId() == null ||  registrasiNewPatient.getCareProviderId() == null || 
				registrasiNewPatient.getDoctorName() == null || registrasiNewPatient.getTglAppointment() == null  )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		if((registrasiNewPatient.getPasienType().equals(MobileBiz.DEPARTMENT_BPJS) || registrasiNewPatient.getPasienType().equals(MobileBiz.DEPARTMENT_BPJSEX)) && (registrasiNewPatient.getNoBpjs() == null ||
			registrasiNewPatient.getTipecard() == null || registrasiNewPatient.getIdcard() == null || registrasiNewPatient.getTglSurat() == null || registrasiNewPatient.getNoSurat() == null))
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		if(registrasiNewPatient.getPasienJenis().equals(MobileBiz.IND_Y) && registrasiNewPatient.getNomrn() == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}	
		return Response.ok(mTWebsiteBiz.addPasienBaru(registrasiNewPatient)).build();
	}
	
	@Override
	public Response addJobsRekruitment(JobsRecruitmentRegistration jobsRecruitmentRegistration) {
		if(jobsRecruitmentRegistration.getEmail()== null || jobsRecruitmentRegistration.getPassword() == null || jobsRecruitmentRegistration.getAgama() == null || 
				jobsRecruitmentRegistration.getNamalengkap() == null ||jobsRecruitmentRegistration.getJeniskelamin() == null || jobsRecruitmentRegistration.getTempatlahir()== null || jobsRecruitmentRegistration.getTgllahir() == null || jobsRecruitmentRegistration.getStatusperkawinan() == null || 
				jobsRecruitmentRegistration.getSuku() == null || jobsRecruitmentRegistration.getTelepontetap() == null || jobsRecruitmentRegistration.getAlamattetap() == null || jobsRecruitmentRegistration.getNoktp() == null || 
				jobsRecruitmentRegistration.getNilai() == null || jobsRecruitmentRegistration.getPendidikanterakhir()== null || jobsRecruitmentRegistration.getInstansipendidikan() == null ||  jobsRecruitmentRegistration.getJurusan() == null || 
				jobsRecruitmentRegistration.getGaji() == null || jobsRecruitmentRegistration.getTglkerja() == null  )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.addJobsRekruitment(jobsRecruitmentRegistration)).build();
	}
	
	@Override
	public Response addKodeKonfirmRekruitment(String idRegist, String kodeKonfirm, String param){
		if(idRegist == null || kodeKonfirm == null || param == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.addKodeKonfirmRekruitment(idRegist,kodeKonfirm,param)).build();
	}
	
	@Override
	public Response addKonfirmasiByEmail(String idRegist, String kodeKonfirm){
		if(idRegist == null || kodeKonfirm == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.addKonfirmasiByEmail(idRegist,kodeKonfirm)).build();
	}
	@Override
	public Response getKonfirmasiByIdRegist(String idRegist){
		if(idRegist == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getKonfirmasiByIdRegist(idRegist)).build();
	}
	
	@Override
	public Response getPasienBaru(String barcode){
		if(barcode == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getPasienBaru(barcode)).build();
	}
	
	@Override
	public Response getCheckPasienBpjsbyNoBpjs(String nobpjs) {
		if(nobpjs == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getCheckPasienBpjsbyNoBpjs(nobpjs)).build();
	}
	
	@Override
	public Response getInfoPasienByNIKBPJS(String nomor, String tipe) {
		if(nomor == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getInfoPasienByNIKBPJS(nomor, tipe)).build();
	}
	
	@Override
	public Response getRegistrationOption() {
		return Response.ok(CheckDataPasienBiz.getRegistrationOption()).build();
	}
	
	@Override
	public Response getListDropDownCityMstr(String stateMstrId) {
		if(stateMstrId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(CheckDataPasienBiz.getListDropDownCityMstr(stateMstrId)).build();
	}
	
	@Override
	public Response sendEmailPasienBaru(String regId){
		if(regId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.sendEmailPasienBaru(regId)).build();
	}
	
	
	
	@Override
	public Response checkLoginPatientAntrian(String user, String pass) {
		if(user == null || pass == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.checkLoginPatientAntrian(user, pass)).build();
	}
	
	@Override
	public Response checkUserPatientAntrian(String mrn, String ktp, String bpjs) {
		if(mrn == null && ktp == null && bpjs == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.checkUserPatientAntrian(mrn, ktp, bpjs)).build();
	}
	
	@Override
	public Response saveUserPatientAntrian(String mrn, String ktp, String bpjs,String username, String fullname, String contact, String email, String pass) {
		if(username == null || mrn == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		} else if(bpjs != null && ktp == null){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.saveUserPatientAntrian(mrn, ktp, bpjs, username, fullname, contact, email, pass)).build();
	}
	
	@Override
	public Response getUserbyCard(String mrn, String ktp, String bpjs) {
		if(mrn == null && ktp == null && bpjs == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.getUserbyCard(mrn, ktp, bpjs)).build();
	}
	
	@Override
	public Response resetPasswordUser(String mrn, String userid, String pass, String username, String tipe) {
		if(mrn == null || userid == null || pass == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(mTWebsiteBiz.resetPasswordUser(mrn, userid, pass, username, tipe)).build();
	}
	
	/*
	@Override
	public Response getTahunGallery(String type){
		return Response.ok(mTWebsiteBiz.getJudulBerita(type)).build();
	}
	
	
	@Override
	public Response getGallery(String tahun, String type){
		return Response.ok(mTWebsiteBiz.getJudulBerita(type)).build();
	}
*/
}
	