package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.PatientTransactionServiceBiz;
import com.rsmurniteguh.webservice.dep.services.IPatientTransactionService;

public class PatientTransactionService implements IPatientTransactionService {
	
	@Autowired
	private PatientTransactionServiceBiz PatientTransactionServiceBiz;

	@Override
	public Response addPatientTransaction(String visitId, String journalMonth, String journalYear, String postedBy) {
		if(visitId == null || journalMonth == null || journalYear == null || postedBy == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();		
		}
		return Response.ok(PatientTransactionServiceBiz.addPatientTransaction(visitId, journalMonth,journalYear, postedBy)).build();
	}

//	@Override
//	public Response addPatientTransactionReversePost(String receiptNo) {
//		// TODO Auto-generated method stub
//		if(receiptNo == null)
//		{
//			return Response.status(Response.Status.BAD_REQUEST).build();		
//		}
//		return Response.ok(PatientTransactionServiceBiz.addPatientTransactionReversePost(receiptNo)).build();
//	}
//	
	@Override
	public Response addPatientBill(String billId) {
		// TODO Auto-generated method stub
		if(billId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();		
		}
		return Response.ok(PatientTransactionServiceBiz.addPatientBill(billId)).build();
	}

	@Override
	public Response addPatientCancelBill(String billId) {
		// TODO Auto-generated method stub
		if(billId == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();		
		}
		return Response.ok(PatientTransactionServiceBiz.addPatientCancelBill(billId)).build();
	}
}
