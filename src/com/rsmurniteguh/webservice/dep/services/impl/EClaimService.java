package com.rsmurniteguh.webservice.dep.services.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.rsmurniteguh.webservice.dep.biz.EClaimBiz;
import com.rsmurniteguh.webservice.dep.services.IEClaim;

public class EClaimService implements IEClaim {
	@Autowired
	private EClaimBiz eClaimBiz;
	
	@Override
	public Response getReportSep(String EClaimBPJS, String sepNo) {
		if(sepNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getReportSep(EClaimBPJS, sepNo)).build();
	}
	
	@Override
	public Response getResumeMedis(String EClaimBPJS, String sepNo) {
		if(sepNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getResumeMedis(EClaimBPJS, sepNo)).build();
	}
	
	public Response GetSignatureReport(String EClaimBPJS, String sepNo){
		if(sepNo==null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else
		{
			return Response.ok(eClaimBiz.GetSignatureReport(EClaimBPJS, sepNo)).build();
		}
	}
	
	@Override
	public Response getLabReport(String EClaimBPJS, String sepNo) {
		if(sepNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getLabReport(EClaimBPJS, sepNo)).build();
	}
	
	@Override
	public Response getPoctReport(String EClaimBPJS, String sepNo) {
		if(sepNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getPoctReport(EClaimBPJS, sepNo)).build();
	}

	@Override
	public Response getRadiologyReport(String EClaimBPJS, String sepNo) {
		if(sepNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getRadiologyReport(EClaimBPJS, sepNo)).build();
	}
	
	@Override
	public Response getCheckSep(String EClaimBPJS, String sepNo) {
		if(sepNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getCheckSep(EClaimBPJS, sepNo)).build();
	}
	
	@Override
	public Response getReportDll(String EClaimBPJS, String sepNo) {
		if(sepNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getReportDll(EClaimBPJS, sepNo)).build();
	}
	
	@Override
	public Response getSepReport(String EClaimBPJS, String fromDate, String toDate, String tipe, String noBa) {
		if(fromDate == null || toDate == null || tipe == null )
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getSepReport(EClaimBPJS, fromDate, toDate, tipe, noBa)).build();
	}
	
	@Override
	public Response getSepReport(String EClaimBPJS, String fromDate, String toDate, String tipe) {
		if(fromDate == null || toDate == null || tipe == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getSepReport(EClaimBPJS, fromDate, toDate, tipe)).build();
	}
	
	@Override
	public Response getSepReportBa(String EClaimBPJS, String noBa) {
		if(noBa == null || EClaimBPJS == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getSepReport(EClaimBPJS, noBa)).build();
	}
	
	@Override
	public Response getNoBa(String EClaimBPJS, String baNo) {
		if(baNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getNoBa(EClaimBPJS, baNo)).build();
	}
	
	@Override
	public Response getBilling(String EClaimBPJS, String sepNo) {
		if(sepNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getBilling(EClaimBPJS, sepNo)).build();
	}

	@Override
	public Response getCheckTxnStatus(String EClaimBPJS, String sepNo) {
		if(sepNo == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getCheckTxnStatus(EClaimBPJS, sepNo)).build();
	}

	@Override
	public Response getPendingLabReport(String fromdate, String todate) {
		if(fromdate == null || todate == null)
		{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(eClaimBiz.getPendingLabReport(fromdate, todate)).build();
	}
	
	
}
