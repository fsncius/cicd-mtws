package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.bpjs.RegisterSepParam;

@Path("/")
@WebService(name="BpjsIntegration")
public interface IBpjsIntegrationService {
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/RegisterSep")
	public Response RegisterSep(RegisterSepParam param);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteLocalRujukanTerbaru")
	public Response deleteLocalRujukanTerbaru(@QueryParam("mrn") String mrn, @QueryParam("noKa") String noKa);
}
