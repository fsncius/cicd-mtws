package com.rsmurniteguh.webservice.dep.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface IKemenkesReferensi {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/JenisPelayanan")
	public Response jenisPelayanan();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/KeadaanKeluar")
	public Response keadaanKeluar();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/CaraKeluar")
	Response caraKeluar();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Instalasi")
	Response instalasi();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Faskes")
	public Response faskes();
}
