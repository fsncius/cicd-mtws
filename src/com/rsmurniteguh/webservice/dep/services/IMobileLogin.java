package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name = "MobileLogin")
public interface IMobileLogin {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/CheckLogin")
	public Response CheckLogin(@FormParam("username") String user, @FormParam("password") String pass);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/CheckLoginStaff")
	public Response CheckLoginStaff(@FormParam("username") String user, @FormParam("password") String pass);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/CheckLoginPatient")
	public Response CheckLoginPatient(@FormParam("username") String user, @FormParam("password") String pass);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UserCreate")
	public Response UserCreate(@FormParam("mrn") String mrn,
			@FormParam("username") String username, @FormParam("password") String passwod,
			@FormParam("tgl_lahir") String tgl_lahir);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UserGet")
	public Response UserGet(@QueryParam("username") String username);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UserUpdate")
	public Response UserUpdate(@FormParam("mrn") String mrn,
			@FormParam("username") String username, @FormParam("password") String passwod,
			@FormParam("tgl_lahir") String tgl_lahir, @FormParam("token") String token);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UserDelete")
	public Response UserDelete(@QueryParam("username") String username);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/change_password_patient")
	public Response change_password_patient(@FormParam("user_id") String user_id,
			@FormParam("password") String password, @FormParam("new_password") String new_password);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/SearchDoctor")
	public Response SearchDoctor(@QueryParam("searchtext") String searchtext, @QueryParam("lokasi") String lokasi);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getInfoPatient")
	public Response getInfoPatient(@QueryParam("mrn") String mrn, @QueryParam("tgl_lahir") String tgl_lahir,
			@QueryParam("username") String username);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/confirmPatient")
	public Response confirmPatient(@QueryParam("mrn") String mrn, @QueryParam("tgl_lahir") String tgl_lahir, @QueryParam("password") String password);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/notificationPublic")
	public Response notificationPublic(@QueryParam("tanggal") String tanggal);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/simple_get_poly")
	public Response simple_get_poly();
}