package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/")
@WebService(name="PatientTransactionService")
public interface IPatientTransactionService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addPatientTransaction")
	public Response addPatientTransaction(@QueryParam("visitId")String visitId, @QueryParam("journalMonth")String journalMonth, @QueryParam("journalYear")String journalYear, @QueryParam("postedBy")String postedBy);
	
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/addPatientTransactionReversePost")
//	public Response addPatientTransactionReversePost(@QueryParam("patientAccountTxnId")String patientAccountTxnId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addPatientBill")
	public Response addPatientBill(@QueryParam("billId")String billId);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addPatientCancelBill")
	public Response addPatientCancelBill(@QueryParam("billId")String billId);
}
