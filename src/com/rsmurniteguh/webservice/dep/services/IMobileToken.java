package com.rsmurniteguh.webservice.dep.services;

import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import com.rsmurniteguh.webservice.dep.all.model.mobile.DataAppointment;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataRujukan;

@Path("/")
@WebService(name = "MobileToken")
public interface IMobileToken {
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/SaveRegistrationToken")
	public Response SaveRegistrationToken(@FormParam("usermstrId") String usermstrId, @FormParam("token") String token);
	
	@POST
	@Path("/uploadRujukan")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadRujukan(List<Attachment> attachment, @Context HttpServletRequest request);

	@GET
	@Path("/downloadRujukan")
	@Produces({ "image/jpg", "image/gif" })
	public Response downloadRujukan(@QueryParam("rujukan_id") String rujukan_id, @QueryParam("jenis") String jenis);
	
	

	/*-- Tambahan Website Antrian Online : Andy Three Saputra --*/
	@GET
	@Path("/updateDataUser")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateDataUser(@FormParam("fullname") String fullname,@FormParam("idcard") String idcard, @FormParam("bpjs") String bpjs,@FormParam("contact") String contact,@FormParam("email") String email,@FormParam("userid") String userid);
	
	@GET
	@Path("/checkAvalibaleRujukan")	
	public Response checkAvalibaleRujukan(@QueryParam("mrn") String mrn);
		
	@GET
	@Path("/checkAvalibaleAppointment")	
	public Response checkAvalibaleAppointment(@QueryParam("nomrn") String nomrn, @QueryParam("tglappointment") String tglappointment, @QueryParam("resourcemstrid") String resourcemstrid);
	
	@GET
	@Path("/getRujukan")	
	public Response getRujukan(@QueryParam("mrn") String mrn);
	
	@GET
	@Path("/getRujukanTambahan")	
	public Response getRujukanTambahan(@QueryParam("mrn") String mrn);
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/createAppointment")
	public Response createAppointment(DataAppointment dataform);
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/createRujukan")
	public Response createRujukan(DataRujukan dataform);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/createRujukanTambahan")
	public Response createRujukanTambahan(DataRujukan dataform);
	
	@GET
	@Path("/cancelAppointment")	
	public Response cancelAppointment(@QueryParam("ids") String ids);
	
	
	@GET
	@Path("/getInfoReject")	
	public Response getInfoReject(@QueryParam("ids") String ids);


	@GET
	@Path("/validNomorRujukan")	
	public Response validNomorRujukan(@QueryParam("ids") String ids,@QueryParam("mrn") String mrn);
	
	@GET
	@Path("/getAppointmentbyId")	
	public Response getAppointmentbyId(@QueryParam("ids") String ids);
	
	@GET
	@Path("/getAppointment")	
	public Response getAppointment(@QueryParam("mrn") String mrn,@QueryParam("departement") String departement,@QueryParam("source") String source,@QueryParam("tipe") String tipe);
	
}
