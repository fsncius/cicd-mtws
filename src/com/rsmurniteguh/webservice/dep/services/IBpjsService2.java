package com.rsmurniteguh.webservice.dep.services;

import java.text.ParseException;

import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.BpjsSepUpdateParam;
import com.rsmurniteguh.webservice.dep.all.model.PengajuanKlaim;

@Path("/")
@WebService(name="BpjsService2")
public interface IBpjsService2 {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsInfobyNoka")
	public Response GetBpjsInfobyNoka(@QueryParam("noKa") String noKa);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsInfobyNik")
	public Response GetBpjsInfobyNik(@QueryParam("nik") String nik);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsInfobyType")
	public Response GetBpjsInfobyType(@QueryParam("type") String type, @QueryParam("target") String target);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/insertSep")
	public Response insertSep(
			@QueryParam("noKa") String noKa,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("klsRawat") String klsRawat,
			@QueryParam("mrn") String mrn,
			@QueryParam("asalRujuk") String asalRujuk,
			@QueryParam("tglRujuk") String tglRujuk,
			@QueryParam("noRujuk") String noRujuk,
			@QueryParam("catatan") String catatan,
			@QueryParam("diagAwal") String diagAwal,
			@QueryParam("poli") String poli,
			@QueryParam("noHp") String noHp,
			@QueryParam("user") String user);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/insertSep4")
	public Response insertSep4(
			@QueryParam("noKa") String noKa,
			@QueryParam("tglSep") String tglSep,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("klsRawat") String klsRawat,
			@QueryParam("mrn") String mrn,
			@QueryParam("asalRujuk") String asalRujuk,
			@QueryParam("tglRujuk") String tglRujuk,
			@QueryParam("noRujuk") String noRujuk,
			@QueryParam("catatan") String catatan,
			@QueryParam("diagAwal") String diagAwal,
			@QueryParam("poli") String poli,
			@QueryParam("noHp") String noHp,
			@QueryParam("user") String user);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/insertSep2")
	public Response insertSep2(
			@QueryParam("noKa") String noKa,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("klsRawat") String klsRawat,
			@QueryParam("mrn") String mrn,
			@QueryParam("asalRujuk") String asalRujuk,
			@QueryParam("tglRujuk") String tglRujuk,
			@QueryParam("noRujuk") String noRujuk,
			@QueryParam("catatan") String catatan,
			@QueryParam("diagAwal") String diagAwal,
			@QueryParam("poli") String poli,
			@QueryParam("noHp") String noHp,
			@QueryParam("user") String user);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/detailSep")
	public Response detailSep(
			@QueryParam("noSep") String noSep);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/hapusSep")
	public Response hapusSep(
			@QueryParam("noSep") String noSep,
			@QueryParam("user") String user);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateSep")
	public Response updateSep(
			@QueryParam("noSep") String noSep,
			@QueryParam("klsRawat") String klsRawat,
			@QueryParam("mrn") String mrn,
			@QueryParam("asalRujuk") String asalRujuk,
			@QueryParam("tglRujuk") String tglRujuk,
			@QueryParam("noRujuk") String noRujuk,
			@QueryParam("catatan") String catatan,
			@QueryParam("diagAwal") String diagAwal,
			@QueryParam("noHp") String noHp,
			@QueryParam("user") String user);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateSep2")
	public Response updateSep2(BpjsSepUpdateParam param);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/pengajuanSep")
	public Response pengajuanSep(
			@QueryParam("noKa") String noKa,
			@QueryParam("tglPelayanan") String tglPelayanan,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("keterangan") String keterangan,
			@QueryParam("user") String user);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/approvalSep")
	public Response approvalSep(
			@QueryParam("noKa") String noKa,
			@QueryParam("tglPelayanan") String tglPelayanan,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("keterangan") String keterangan,
			@QueryParam("user") String user);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateTglPlg")
	public Response updateTglPlg(
			@QueryParam("noSep") String noSep,
			@QueryParam("tglPulang") String tglPulang,
			@QueryParam("user") String user);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/integrasiInacbg")
	public Response integrasiInacbg(
			@QueryParam("noSep") String noSep);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/insertRujukan")
	public Response insertRujukan(
			@QueryParam("noSep") String noSep,
			@QueryParam("ppkDirujuk") String ppkDirujuk,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("catatan") String catatan,
			@QueryParam("diagRujukan") String diagRujukan,
			@QueryParam("tipeRujukan") String tipeRujukan,
			@QueryParam("poliRujukan") String poliRujukan,
			@QueryParam("user") String user);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateRujukan")
	public Response updateRujukan(
			@QueryParam("noRujukan") String noRujukan,
			@QueryParam("ppkDirujuk") String ppkDirujuk,
			@QueryParam("tipe") String tipe,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("catatan") String catatan,
			@QueryParam("diagRujukan") String diagRujukan,
			@QueryParam("tipeRujukan") String tipeRujukan,
			@QueryParam("poliRujukan") String poliRujukan,
			@QueryParam("user") String user);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteRujukan")
	public Response deleteRujukan(
			@QueryParam("noRujukan") String noRujukan,
			@QueryParam("user") String user);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/dataKunjungan")
	public Response dataKunjungan(
			@QueryParam("tglSep") String tglSep,
			@QueryParam("jnsPelayanan") String jnsPelayanan);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/dataFaskes")
	public Response dataFaskes(@QueryParam("kodeRs") String kodeRs, @QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/diagnosa")
	public Response diagnosa(@QueryParam("diag") String diag);
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/dataKlaim")
	public Response dataKlaim(
			@QueryParam("tglPulang") String tglPulang,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("status") String status);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/insertLpk")
	public Response insertLpk(PengajuanKlaim pengajuanKlaim);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/insertSep3")
	public Response insertSep3(
			@QueryParam("noKa") String noKa,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("klsRawat") String klsRawat,
			@QueryParam("mrn") String mrn,
			@QueryParam("asalRujukan") String asalRujukan,
			@QueryParam("tglRujukan") String tglRujukan,
			@QueryParam("noRujukan") String noRujukan,
			@QueryParam("asalPerujuk") String asalPerujuk,
			@QueryParam("catatan") String catatan,
			@QueryParam("diagAwal") String diagAwal,
			@QueryParam("poli") String poli,
			@QueryParam("Eks") String Eks,
			@QueryParam("lakaLantas") String lakaLantas,
			@QueryParam("penjamin") String penjamin,
			@QueryParam("tglKejadian") String tglKejadian,
			@QueryParam("ket") String ket,
			@QueryParam("suplesi") String suplesi,
			@QueryParam("noSuplesi") String noSuplesi,
			@QueryParam("kdPro") String kdPro,
			@QueryParam("kdKab") String kdKab,
			@QueryParam("kdKec") String kdKec,
			@QueryParam("noSurat") String noSurat,
			@QueryParam("dpjp") String dpjp,
			@QueryParam("noHp") String noHp,
			@QueryParam("user") String user);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/dpjp")
	public Response dpjp(
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("spesialis") String spesialis,
			@QueryParam("tanggal") String tanggal);
	
	
	
	/* ---------- Andy Three S ----*/

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanbyNmrRujukan")
	public Response getRujukanbyNmrRujukan(@QueryParam("nmrRujukan") String nmrRujukan);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanRSbyNmrRujukan")
	public Response getRujukanRSbyNmrRujukan(@QueryParam("nmrRujukan") String nmrRujukan);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanbyNoka")
	public Response getRujukanbyNoka(@QueryParam("noKa") String noKa);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanRSbyNoka")
	public Response getRujukanRSbyNoka(@QueryParam("noKa") String noKa);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanPcareorRSbyNoka")
	public Response getRujukanPcareorRSbyNoka(@QueryParam("noKa") String noKa);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanListbyNoka")
	public Response getRujukanListbyNoka(@QueryParam("noKa") String noKa);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanRSListbyNoka")
	public Response getRujukanRSListbyNoka(@QueryParam("noKa") String noKa);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanbyTglRujukan")
	public Response getRujukanbyTglRujukan(@QueryParam("tglRujukan") String tglRujukan) throws ParseException;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanRSbyTglRujukan")
	public Response getRujukanRSbyTglRujukan(@QueryParam("tglRujukan") String tglRujukan) throws ParseException;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiProcedure")
	public Response getReferensiProcedure(@QueryParam("kode") String kode, @QueryParam("nama") String nama);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiKelasRawat")
	public Response getReferensiKelasRawat();
		
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiDokter")
	public Response getReferensiDokter(@QueryParam("namaDokter") String namaDokter);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiSpesialistik")
	public Response getReferensiSpesialistik();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiPoli")
	public Response getReferensiPoli(@QueryParam("kodeAtauNama")String codeOrName);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiRuangRawat")
	public Response getReferensiRuangRawat();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiCaraKeluar")
	public Response getReferensiCaraKeluar();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiPascaPulang")
	public Response getReferensiPascaPulang();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiPropinsi")
	public Response getReferensiPropinsi();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiKabupaten")
	public Response getReferensiKabupaten(@QueryParam("kodePropinsi") String kodePropinsi);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReferensiKecamatan")
	public Response getReferensiKecamatan(@QueryParam("kodeKabupaten") String kodeKabupaten);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSuplesiJasaRaharja")
	public Response getSuplesiJasaRaharja(@QueryParam("noBPJS") String noBPJS,@QueryParam("tglSEP") String tglSEP) throws ParseException;
	
}
