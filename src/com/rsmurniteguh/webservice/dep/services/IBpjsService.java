package com.rsmurniteguh.webservice.dep.services;

import java.math.BigDecimal;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="BpjsService")
public interface IBpjsService {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsMemberInfo")
	public Response GetBpjsMemberInfo(@QueryParam("CardNo") String cardNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsMemberOnline")
	public Response GetBpjsMemberOnline(@QueryParam("CardNo") String cardNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsNikInfo")
	public Response GetBpjsNikInfo(@QueryParam("Nik") String Nik);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsSepInfo")
	public Response GetBpjsSepInfo(@QueryParam("CardNo") String cardNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsDataRujukan")
	public Response GetBpjsDataRujukan(@QueryParam("NoRujuk") String noRujuk);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsTanggalRujukan")
	public Response GetBpjsTanggalRujukan(@QueryParam("Tanggal") String Tanggal);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsDataRujukanNomorKartu")
	public Response GetBpjsDataRujukanNomorKartu(@QueryParam("CardNo") String cardNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsRujukanFktl")
	public Response GetBpjsRujukanFktl(@QueryParam("CardNo") String cardNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsTanggalRujukanFktl")
	public Response GetBpjsTanggalRujukanFktl(@QueryParam("Tanggal") String Tanggal);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsNomorRujukanFktl")
	public Response GetBpjsNomorRujukanFktl(@QueryParam("NoRujuk") String noRujuk);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsDetailSep")
	public Response GetBpjsDetailSep(@QueryParam("NoSep") String noSep);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanRs")
	public Response GetRujukanRs(@QueryParam("NoRujuk") String noRujuk);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRujukanKartu")
	public Response GetRujukanKartu(@QueryParam("NoKartu") String noKartu);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCbgTarif")
	public Response GetCbgTarif(@QueryParam("NoSep") String noSep);
	
	
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsDiagnosa")
	public Response GetBpjsDiagnosa(@QueryParam("Keyword") String Keyword);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsProsedurCbg")
	public Response GetBpjsProsedurCbg(@QueryParam("Keyword") String Keyword);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsDataCmg")
	public Response GetBpjsDataCmg(@QueryParam("Keyword") String Keyword);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsVerifikasiKlaim")
	public Response GetBpjsVerifikasiKlaim(
			@QueryParam("TglMasuk") String TglMasuk,
			@QueryParam("TglKeluar") String TglKeluar, 
			@QueryParam("KlsRawat") String KlsRawat,
			@QueryParam("Kasus") String Kasus, 
			@QueryParam("Cari") String Cari,
			@QueryParam("Status") String Status);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsLaporanSep")
	public Response GetBpjsLaporanSep(@QueryParam("NoSep") String noSep);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/createSep")
	public Response CreateSep(
			@QueryParam("noKartu") String noKartu,
			@QueryParam("tglSep") String tglSep,
			@QueryParam("tglRujukan") String tglRujukan,
			@QueryParam("noRujukan") String noRujukan,
			@QueryParam("ppkRujukan") String ppkRujukan,
			@QueryParam("ppkPelayanan") String ppkPelayanan,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("catatan") String catatan,
			@QueryParam("diagAwal") String diagAwal,
			@QueryParam("poliTujuan") String poliTujuan,
			@QueryParam("klsRawat") String klsRawat,
			@QueryParam("lakaLantas") String lakaLantas,
			@QueryParam("noMr") String noMr);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/CreateRoom")
	public Response CreateRoom(
		@QueryParam("kodeKelas") String kodeKelas,
		@QueryParam("kodeRuang") String kodeRuang,
		@QueryParam("namaRuang") String namaRuang,
		@QueryParam("kapasitas") String kapasitas,
		@QueryParam("tersedia") String tersedia);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UpdateRoom")
	public Response UpdateRoom(
		@QueryParam("kodeKelas") String kodeKelas,
		@QueryParam("kodeRuang") String kodeRuang,
		@QueryParam("namaRuang") String namaRuang,
		@QueryParam("kapasitas") String kapasitas,
		@QueryParam("tersedia") String tersedia);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateTglPulang")
	public Response UpdateTglPulang(
			@QueryParam("noSep") String noSep,
			@QueryParam("tglPlg") String tglPlg);
	
	
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateSep")
	public Response UpdateSep(
			@QueryParam("noSep") String noSep,
			@QueryParam("noKartu") String noKartu,
			@QueryParam("tglSep") String tglSep,
			@QueryParam("tglRujukan") String tglRujukan,
			@QueryParam("noRujukan") String noRujukan,
			@QueryParam("ppkRujukan") String ppkRujukan,
			@QueryParam("ppkPelayanan") String ppkPelayanan,
			@QueryParam("jnsPelayanan") String jnsPelayanan,
			@QueryParam("catatan") String catatan,
			@QueryParam("diagAwal") String diagAwal,
			@QueryParam("poliTujuan") String poliTujuan,
			@QueryParam("klsRawat") String klsRawat,
			@QueryParam("noMr") String noMr);
	
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/hapusSep")
	public Response HapusSep(@QueryParam("noSep") String noSep);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getKodeKamar")
	public Response KodeKamar();
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listKamar")
	public Response ListKamar();
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/transaksiSep")
	public Response TransaksiSep(
	@QueryParam("noSep") String noSep,
	@QueryParam("noTrans") String noTrans);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/grouperInacbg")
	public Response GrouperInacbg();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/finalisasiGrouper")
	public Response FinalisasiGrouper();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/eligibilitasPeserta")
	public Response GetEligibilitasPeserta(
			@QueryParam("nokainhealth") String nokainhealth,
			@QueryParam("tglpelayanan") String tglpelayanan,
			@QueryParam("jenispelayanan") String jenispelayanan,
			@QueryParam("poli") String poli);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cekSJP")
	public Response CekSJP(
			@QueryParam("nokainhealth") String nokainhealth,
			@QueryParam("tanggalsjp") String tanggalsjp,
			@QueryParam("poli") String poli,
			@QueryParam("tkp") String tkp);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/confirmAKTFirstPayor")
	public Response ConfirmAKTFirstPayor(
			@QueryParam("nosjp") String nosjp,
			@QueryParam("userid") String userid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/hapusDetailSJP")
	public Response HapusDetailSJP(
			@QueryParam("nosjp") String nosjp,
			@QueryParam("notes") String notes,
			@QueryParam("userid") String userid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/hapusSJP")
	public Response HapusSJP(
			@QueryParam("nosjp") String nosjp,
			@QueryParam("alasanhapus") String alasanhapus,
			@QueryParam("userid") String userid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/hapusTindakan")
	public Response HapusTindakan(
			@QueryParam("nosjp") String nosjp,
			@QueryParam("kodetindakan") String kodetindakan,
			@QueryParam("tgltindakan") String tgltindakan,
			@QueryParam("notes") String notes,
			@QueryParam("userid") String userid);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/infoSJP")
	public Response InfoSJP(
			@QueryParam("nosjp") String nosjp);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/poli")
	public Response Poli(
			@QueryParam("keyword") String keyword);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/providerRujukan")
	public Response ProviderRujukan(
			@QueryParam("keyword") String keyword);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/rekapHasilVerifikasi")
	public Response RekapHasilVerifikasi(
			@QueryParam("nofpk") String nofpk);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/simpanBiayaINACBGS")
	public Response SimpanBiayaINACBGS(
			@QueryParam("nosjp") String nosjp,
			@QueryParam("kodeinacbg") String kodeinacbg,
			@QueryParam("biayainacbgs") BigDecimal biayainacbgs,
			@QueryParam("nosep") String nosep,
			@QueryParam("notes") String notes,
			@QueryParam("userid") String userid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/simpanRuangRawat")
	public Response SimpanRuangRawat(
			@QueryParam("nosjp") String nosjp,
			@QueryParam("tglmasuk") String tglmasuk,
			@QueryParam("kelasrawat") String kelasrawat,
			@QueryParam("kodejenispelayanan") String kodejenispelayanan,
			@QueryParam("byharirawat") BigDecimal byharirawat);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/simpanSJP")
	public Response SimpanSJP(
			@QueryParam("tanggalpelayanan") String tanggalpelayanan,
			@QueryParam("jenispelayanan") String jenispelayanan,
			@QueryParam("nokainhealth") String nokainhealth,
			@QueryParam("nomormedicalreport") String nomormedicalreport,
			@QueryParam("nomorasalrujukan") String nomorasalrujukan,
			@QueryParam("kodeproviderasalrujukan") String kodeproviderasalrujukan,
			@QueryParam("tanggalasalrujukan") String tanggalasalrujukan,
			@QueryParam("kodediagnosautama") String kodediagnosautama,
			@QueryParam("poli") String poli,
			@QueryParam("username") String username,
			@QueryParam("informasitambahan") String informasitambahan,
			@QueryParam("kodediagnosatambahan") String kodediagnosatambahan,
			@QueryParam("kecelakaankerja") BigDecimal kecelakaankerja,
			@QueryParam("kelasrawat") String kelasrawat,
			@QueryParam("kodejenpelruangrawat") String kodejenpelruangrawat);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/simpanTindakan")
	public Response SimpanTindakan(
			@QueryParam("jenispelayanan") String jenispelayanan,
			@QueryParam("nosjp") String nosjp,
			@QueryParam("tglmasukrawat") String tglmasukrawat,
			@QueryParam("tanggalpelayanan") String tanggalpelayanan,
			@QueryParam("kodetindakan") String kodetindakan,
			@QueryParam("poli") String poli,
			@QueryParam("kodedokter") String kodedokter,
			@QueryParam("biayaaju") String biayaaju);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/simpanTindakanRITL")
	public Response SimpanTindakanRITL(
			@QueryParam("jenispelayanan") String jenispelayanan,
			@QueryParam("nosjp") String nosjp,
			@QueryParam("idakomodasi") String idakomodasi,
			@QueryParam("tglmasukrawat") String tglmasukrawat,
			@QueryParam("tanggalpelayanan") String tanggalpelayanan,
			@QueryParam("kodetindakan") String kodetindakan,
			@QueryParam("poli") String poli,
			@QueryParam("kodedokter") String kodedokter,
			@QueryParam("biayaaju") BigDecimal biayaaju);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateTanggalPulang")
	public Response UpdateTanggalPulang(
			@QueryParam("id") BigDecimal id,
			@QueryParam("nosjp") String nosjp,
			@QueryParam("tglmasuk") String tglmasuk,
			@QueryParam("tglkeluar") String tglkeluar);
	
	
}
