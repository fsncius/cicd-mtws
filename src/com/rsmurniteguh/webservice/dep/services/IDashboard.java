package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="Dashboard")
public interface IDashboard {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/patient_visit_report")
	public Response patient_visit_report(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate,
			@QueryParam("tipe") String tipe
			);
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/patient_visit_los")
	public Response patient_visit_los(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate,
			@QueryParam("tipe") String tipe,
			@QueryParam("start") String start,
			@QueryParam("end") String end
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/patient_visit_over")
	public Response patient_visit_over(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate,
			@QueryParam("tipe") String tipe
			);
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/patient_visit_more")
	public Response patient_visit_more(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate,
			@QueryParam("tipe") String tipe
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/diagnosa_report")
	public Response diagnosa_report(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/utilization_radiology_report")
	public Response utilization_radiology_report(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/utilization_surgeryroom_report")
	public Response utilization_surgeryroom_report(
			@QueryParam("ot_code") String ot_code,
			@QueryParam("ot_date") String ot_date
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/utilization_surgeryroom_time_report")
	public Response utilization_surgeryroom_time_report(
			@QueryParam("efektif") String efektif,
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/discharge_planning_report")
	public Response discharge_planning_report(
			@QueryParam("tanggal") String tanggal
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cathlab_act_report")
	public Response cathlab_act_report(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/endoskopi_act_report")
	public Response endoskopi_act_report(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/outpatient_city_report")
	public Response outpatient_city_report(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/material_report")
	public Response material_report(
			@QueryParam("type") String type,
			@QueryParam("report") String report
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/location")
	public Response location();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/radiology_schedule_report")
	public Response radiology_schedule_report(
			@QueryParam("date") String date,
			@QueryParam("type") String type
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/radiology_responsetime_report")
	public Response radiology_responsetime_report(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate,
			@QueryParam("type") String type
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/top_registered_appointment_report")
	public Response top_registered_appointment_report(
			@QueryParam("date") String date
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/appointment_report")
	public Response appointment_report(
			@QueryParam("date") String date,
			@QueryParam("department") String department
			);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/inpatient_billing_report")
	public Response inpatient_billing_report(
			@QueryParam("billing_id") String billing_id
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/top_operation_doctors")
	public Response top_operation_doctors(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
}
