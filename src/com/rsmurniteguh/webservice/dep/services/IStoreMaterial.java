package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="StoreMaterialService")
public interface IStoreMaterial {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getstorematerial")
	public Response getstorematerial(@QueryParam("stormtId") String storemtId);
}
