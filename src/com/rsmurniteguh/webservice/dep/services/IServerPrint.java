package com.rsmurniteguh.webservice.dep.services;

import java.util.ArrayList;

import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="ServerPrint")
public interface IServerPrint {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPrintList")
	public Response getPrintList(@QueryParam("reportCodes") String listString);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addPrintCount")
	public Response addPrintCount(@FormParam("serverPrintId") Long serverPrintId);
}
