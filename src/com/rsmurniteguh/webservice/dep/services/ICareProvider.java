package com.rsmurniteguh.webservice.dep.services;

import java.math.BigDecimal;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.CareProvider;


@Path("/")
@WebService(name="CareProviderService")
public interface ICareProvider {
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/sendcareprovider")
	public Response sendcareprovider(CareProvider b1);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcareprovider")
	public Response getcareprovider(@QueryParam("CareType") String caretype);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getdoctoruser")
	public Response getdoctoruser(); 
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcekdispas")
	public Response getcekdispas(@QueryParam("VisitId")String visitid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getquenum")
	public Response getquenum(@QueryParam("Queueno") String queueno);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getspecialquenum")
	public Response getspecialquenum(@QueryParam("queueno") String queueno);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getspecialquenum2")
	public Response getspecialquenum2(@QueryParam("queueno") String queueno,@QueryParam("subspeciality") String subspeciality);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getDoctorQueueList")
	public Response getDoctorQueueList(@QueryParam("careproviderId") String careproviderId,@QueryParam("subspecialtymstr") String subspecialtymstr_id);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCareprovidersByUsermstr")
	public Response getCareprovidersByUsermstr(@QueryParam("users") String users);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getDoctorQueueListByUsermstrId")
	public Response getDoctorQueueListByUsermstrId(@QueryParam("usermstrId") String usermstrId,@QueryParam("subspecialtymstr") String subspecialtymstr_id);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getIsDoctorByUserMstrId")
	public Response getIsDoctorByUserMstrId(@QueryParam("userMstrId") String userMstrId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/syncSIPDoctor")
	public Response syncSIPDoctor(@QueryParam("careProviderId") String careProviderId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/syncSIPDoctorAll")
	public Response syncSIPDoctorAll();
}
