package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.queue.QueueConfig;
import com.rsmurniteguh.webservice.dep.all.model.queue.RequestTicket;

@Path("/")
@WebService(name="queue")
public interface IQueueService {
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/postDoctorWorkstationCall")
	public Response postDoctorWorkstationCall(
			@QueryParam("queueno") String queueno, 
			@QueryParam("subspeciality") String subspeciality, 
			@QueryParam("usermstrid") String usermstr);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/postPharmacyComplete")
	public Response postPharmacyComplete(
			@QueryParam("queueno") String queueno, 
			@QueryParam("subspeciality") String subspeciality, 
			@QueryParam("usermstrid") String usermstr);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InsertQueue")
	public Response InsertQueue(
			@QueryParam("CareProviderId") String CareProviderId, 
			@QueryParam("patient_name") String patient_name, 
			@QueryParam("card_no") String card_no, 
			@QueryParam("tgl_berobat") String tgl_berobat, 
			@QueryParam("PatientId") String PatientId, 
			@QueryParam("location") String location, 
			@QueryParam("groupId") String group_id);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/RequestIDTicket")
	public Response RequestIDTicket(
			@QueryParam("resourcemstr") String resourcemstr, 
			@QueryParam("location") String location, 
			@QueryParam("tanggal") String tanggal,
			@QueryParam("source") String source,
			@QueryParam("nama_patient")String nama_patient,
			@QueryParam("no_mrn")String no_mrn,
			@QueryParam("queue_type") String queue_type);

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/RequestTicket")
	public Response RequestTicket(
			@QueryParam("location")String location,
			@QueryParam("queue_id")String queue_id,
			@QueryParam("no_doctor")String no_doctor,
			@QueryParam("no_subspecialis")String no_subspecialis,
			@QueryParam("tanggal")String tanggal);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ChangeQueueStatus")
	public Response ChangeQueueStatus(
			@FormParam("queue_id")String queue_id,
			@FormParam("userMstrID")String userMstrID,
			@FormParam("status")String status,
			@FormParam("counterId")String counterId,
			@FormParam("statusQueueTime")String statusQueueTime,
			@FormParam("queue_no")String queue_no);
	
/*	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ChangeQueueStatus")
	public Response ChangeQueueStatus(
			@QueryParam("queue_id")String queue_id,
			@QueryParam("userMstrID")String userMstrID,
			@QueryParam("status")String status,
			@QueryParam("counterId")String counterId,
			@QueryParam("statusQueueTime")String statusQueueTime,
			@QueryParam("queue_no")String queue_no);*/
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ListviewReg")
	public Response ListviewReg(
			@QueryParam("counters") String counters);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ListQueue")
	public Response ListQueue(
			@QueryParam("lokasi") String lokasi,
			@QueryParam("counter_id") String counter_id,
			@QueryParam("tanggal") String tanggal,
			@QueryParam("regType") String regType,
			@QueryParam("status") String status);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/SoundQueue")
	public Response SoundQueue(
			@QueryParam("counters") String counters);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/delQueueCall")
	public Response delQueueCall(
			@QueryParam("queue_id") String queue_id);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getConfigData")
	public Response getConfigData();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getConfigDataByAddress")
	public Response getConfigDataByAddress(@QueryParam("mac_address") String mac_address);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getServingTime")
	public Response getServingTime(@QueryParam("queueId") Long queueId);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/retConfigDataByAddress")
	public Response retConfigDataByAddress(QueueConfig queueConfig);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/setConfigDataByAddress")
	public Response setConfigDataByAddress(QueueConfig queueConfig);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getActiveCounter")
	public Response getActiveCounter(
			@QueryParam("counters") String counters); // with delimiter e.g : 1,2,3,4
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/CallSpecialQueueByNumber")
	public Response CallSpecialQueueByNumber(
			@QueryParam("location") String location
			, @QueryParam("counterid") String counterid
			, @QueryParam("queueNo") String queueNo
			, @QueryParam("userMstrId") String userMstrId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/SkipSpecialQueueByNumber")
	public Response SkipSpecialQueueByNumber(
			@QueryParam("location") String location
			, @QueryParam("counterid") String counterid
			, @QueryParam("queueNo") String queueNo
			, @QueryParam("userMstrId") String userMstrId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/FinishSpecialQueueByNumber")
	public Response FinishSpecialQueueByNumber(
			@QueryParam("location") String location
			, @QueryParam("counterid") String counterid
			, @QueryParam("queueNo") String queueNo
			, @QueryParam("userMstrId") String userMstrId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ListViewPolyclinicBPJS")
	public Response ListViewPolyclinicBPJS(
			@QueryParam("location") String location
			, @QueryParam("counters") String counters);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/LoginCaller")
	public Response LoginCaller(
			@QueryParam("username") String username
			, @QueryParam("password") String password);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ChangeCountertoAktif")
	public Response ChangeCountertoAktif(
			@QueryParam("counterid") String counterid
			, @QueryParam("usermstrid") String usermstrid
			, @QueryParam("resourcemstrId") String resourcemstrId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ChangeCountertoNonAktif")
	public Response ChangeCountertoNonAktif(
			@QueryParam("counterid") String counterid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/QueueDetails")
	public Response QueueDetails(
			@QueryParam("queue_no") String queue_no
			, @QueryParam("location") String location
			, @QueryParam("tanggal") String tanggal);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ListQueueKasir")
	public Response ListQueueKasir(
			@QueryParam("lokasi") String lokasi);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InsertQueueKasir")
	public Response InsertQueueKasir(
			@QueryParam("queue_no") String queue_no, 
			@QueryParam("lokasi") String lokasi,
			@QueryParam("source") String source,
			@QueryParam("tanggal") String tanggal,
			@QueryParam("queue_type") String queue_type);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/QueueCallKasir")
	public Response QueueCallKasir(
			@QueryParam("lokasi") String lokasi,
			@QueryParam("counter_id") String counter_id);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/QueueNoByMrn")
	public Response QueueNoByMrn(
			@QueryParam("noMrn") String noMrn);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/QueueCallSkipKasir")
	public Response QueueCallSkipKasir(
			@QueryParam("queue_no") String queue_no,
			@QueryParam("lokasi") String lokasi);

}
