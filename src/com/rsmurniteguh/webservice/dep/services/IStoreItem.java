package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="StoreItemService")
public interface IStoreItem {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getstoreitem")
	public Response getstoreitem(@QueryParam("StoreMstrId") String StoreMstrId, @QueryParam("MaterialItemMstrId") String MaterialItemMstrId);
}
