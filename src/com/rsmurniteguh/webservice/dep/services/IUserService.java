package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="UserService")
public interface IUserService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/checklogin")
	public Response checklogin(@QueryParam("user") String user, @QueryParam("pass") String pass);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/change_password_patient")
	public Response change_password_patient(
			@FormParam("user_id") String user_id, 
			@FormParam("password") String password, 
			@FormParam("new_password") String new_password);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/change_password_kthis")
	public Response change_password_kthis(
			@FormParam("usermstrId") String usermstrId, 
			@FormParam("password") String password, 
			@FormParam("new_password") String new_password);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/change_password_hris")
	public Response change_password_hris(
			@FormParam("idkaryawan") String idkaryawan, 
			@FormParam("password") String password, 
			@FormParam("new_password") String new_password);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/check_login_hris")
	public Response check_login_hris(
			@FormParam("username") String username, 
			@FormParam("password") String password);
}
