package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/")
@WebService(name="PurchaseService")
public interface IPurchaseService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddPurchase")
	public Response AddPurchase(@QueryParam("purchaseNo")String purchaseNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddReceive")
	public Response AddReceive(@QueryParam("receiptNo")String receiptNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddReturn")
	public Response AddReturn(@QueryParam("receiptNo")String receiptNo);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddConsignment")
	public Response AddConsignment(@QueryParam("purchaseNo")String purchaseNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UnpostPurchase")
	public Response UnpostPurchase(@QueryParam("purchaseNo")String purchaseNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UnpostReceive")
	public Response UnpostReceive(@QueryParam("receiptNo")String receiptNo);
}
