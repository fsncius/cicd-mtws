package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

@Path("/")
@WebService(name="LabUserService")
public interface LabUser {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getlabuser")
	public Response getlabuser(@QueryParam("submstrid") String submstrid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/orderlab")
	public Response getorder();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/orderradiologi")
	public Response getradiologi();
}
