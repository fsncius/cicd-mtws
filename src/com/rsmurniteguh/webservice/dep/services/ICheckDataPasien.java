package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.NewPatient;

@Path("/")
@WebService(name="CheckDataPasienService")
public interface ICheckDataPasien {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcheckdatapasien")
	public Response getcheckdatapasien(@QueryParam("IpMrn") String ipmrn);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcekdiagnos")
	public Response getcekdiagnos(@QueryParam("IpMrn") String ipmrn);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcheckdatapasienByBpjsNo")
	public Response getcheckdatapasienByBpjsNo(@QueryParam("bpjs") String bpjs);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/VisitDiagDetailId")
	public Response getVisitDiagnosisDetailId(@QueryParam("VisitDiagnosaDetailId") String visitdiagnosadetailid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InfoVisitPasien")
	public Response getInfoVisitPasien(@QueryParam("VisitId") String VisitId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UserKthis")
	public Response getInfoUserKthis(@QueryParam("userMstrId")String userMstrId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRegistrationOption")
	public Response getRegistrationOption();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getListDropDownCityMstr")
	public Response getListDropDownCityMstr(@QueryParam("stateMstrId")String stateMstrId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getListPasienByNameAndDate")
	public Response getListPasienByNameAndDate(@QueryParam("patientName")String patientName, @QueryParam("birthDate")String birthDate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getdatapasienByBpjsNo")
	public Response getdatapasienByBpjsNo(@QueryParam("bpjs") String bpjs);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/registerNewPatient")
	public Response registerNewPatient(NewPatient newPatient);
}
