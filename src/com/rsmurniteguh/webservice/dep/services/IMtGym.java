package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.gym.AccountBalance;
import com.rsmurniteguh.webservice.dep.all.model.gym.AccountBalanceParam;
import com.rsmurniteguh.webservice.dep.all.model.gym.CreateMember;

@Path("/")
@WebService(name="Gym")
public interface IMtGym {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/CheckLogin")
	public Response CheckLogin(
			@FormParam("username")String user,
			@FormParam("password")String pass);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/SaveRegistrationToken")
	public Response SaveRegistrationToken(@QueryParam("usermstrId")String usermstrId, @QueryParam("token")String token);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/info_account_from_cardno")
	public Response info_account_from_cardno(
			@QueryParam("cardno") String cardno
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/info_account_from_old_card")
	public Response info_account_from_old_card(
			@QueryParam("machine_no") String machineNo);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/update_account")
	public Response update_account(
			AccountBalance account_balance
			);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/create_account")
	public Response create_account(
			AccountBalanceParam accountBalanceParam
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/info_member")
	public Response info_member(
			@QueryParam("nfcno") String nfcno
			);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/create_member")
	public Response create_member(
			CreateMember create_member
			);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/topup_account")
	public Response topup_account(
			@FormParam("total") String total,
			@FormParam("nfcno") String nfcno,
			@FormParam("created_by") String created_by
			);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/member")
	public Response member();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cancel_topup_account")
	public Response cancel_topup_account(
			@QueryParam("invoice_no") String invoice_no,
			@QueryParam("created_by") String created_by
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/trx")
	public Response trx(
			@QueryParam("date") String date,
			@QueryParam("type") String type
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_rfid")
	public Response get_rfid(
			@QueryParam("machine_no") String machineNo
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/absensi")
	public Response absensi(
			@QueryParam("accountBalanceId") String accountBalanceId,
			@QueryParam("memberShipId") String memberShipId,
			@QueryParam("machineCode") String machineCode
			);
	
}
