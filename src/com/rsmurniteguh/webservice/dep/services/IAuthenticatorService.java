package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/")
@WebService(name="Authenticator")
public interface IAuthenticatorService {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Authenticate")
	public Response PaymentTransaction(@QueryParam("user")String user, @QueryParam("token")String token);
	
	
	
}