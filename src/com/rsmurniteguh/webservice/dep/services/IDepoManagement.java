package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.PaymentModel;
import com.rsmurniteguh.webservice.dep.all.model.UpdateCard;

@Path("/")
@WebService(name="DepoManagement")
public interface IDepoManagement {
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/PaymentTransaction")
	public Response PaymentTransaction(PaymentModel pay);
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/PaymentTransactionModified")
	public Response PaymentTransactionModified(PaymentModel pay);
	
	
	
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/RegisterAccount")
	public Response RegisterAccount(PaymentModel pay);
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UpdateAccount")
	public Response UpdateAccount(PaymentModel pay);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetInvoice")
	public Response GetInvoice();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetInvoiceCan")
	public Response GetInvoiceCan();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetBalance")
	public Response GetBalance(@QueryParam("cardNo")String cardNo);
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/PrepareGetCardDataFromMachine")
	public Response PrepareGetCardDataFromMachine(@QueryParam("machineNo")String machineNo);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetCardDataByMachine")
	public Response GetCardDataByMachine(@QueryParam("machineNo")String machineNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetValidCardDataByMachine")
	public Response GetValidCardDataByMachine(@QueryParam("machineNo")String machineNo);
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ChangeCardNumber")
	public Response ChangeCardNumber(UpdateCard data);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetCardDetailInfo")
	public Response GetCardDetailInfo(
			@QueryParam("cardNo") String cardNo,
			@QueryParam("cardType") String cardType 
			);
	
}