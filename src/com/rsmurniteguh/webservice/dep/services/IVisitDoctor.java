package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="VisitDoctorService")
public interface IVisitDoctor {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getpasdok")
	public Response getpaspul(@QueryParam("dadok") String dadok);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getinfopasien")
	public Response getpasieninfo(@QueryParam("mrn") String mrn);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getinvispasdok")
	public Response getinvispasdok(@QueryParam("mrnaja")String mrnaja);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getConDoc")
	public Response getConDoc(@QueryParam("tglcek")String tglcek);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getInfoData")
	public Response getInfoData(@QueryParam("mrn")String mrn);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getBedHistory")
	public Response getBedHistory(@QueryParam("visitid")String visitid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getListFamily")
	public Response getListFamily(@QueryParam("personid")String personid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/DataRgPasien")
	public Response getDataRgPasien(@QueryParam("mrn") String mrn);
	
	
}
