package com.rsmurniteguh.webservice.dep.services;

import java.math.BigDecimal;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.kemenkes.SendResumeMedisParam;

@WebService(name="Kemenkes")
public interface IKemenkes{
	 @Path("{id}/ResumeMedis")
	 @Produces(MediaType.APPLICATION_JSON)
	 public IKemenkesResumeMedis ResumeMedis(@PathParam("id") String nik);
	 
	 @Path("/Referensi")
	 @Produces(MediaType.APPLICATION_JSON)
	 public IKemenkesReferensi Referensi();
	 
	 @POST
	 @Path("/sendResumeMedis")
	 @Produces(MediaType.APPLICATION_JSON)
	 public  Response sendResumeMedis(SendResumeMedisParam param);
	 
}
