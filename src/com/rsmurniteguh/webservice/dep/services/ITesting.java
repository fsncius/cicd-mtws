package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.TestModel;


@Path("/")
@WebService(name="Testing")
public interface ITesting {
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/TestSave")
	public Response TestSave(TestModel model);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/enkripsi")
	public Response encrypt();
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/dekripsi")
	public Response dekripsi();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/testEmail")
	public Response testEmail();
	
	
}