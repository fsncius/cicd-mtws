package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.rsmurniteguh.webservice.dep.all.model.UploadResume;

import java.math.BigDecimal;
import java.sql.Blob;

@Path("/")
@WebService(name="dataBpjsService")
public interface IdataBpjs {
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getDataBpjsNo")
	public Response getDataBpjsNo(@QueryParam("NoBpjs")String nobpjs);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCardNo")
	public Response getCardNo(@QueryParam("CardNo")String cardno);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBaseInfo")
	public Response getBaseInfo(@QueryParam("mrn")String mrn);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPolyResourceMstrByUser")
	public Response getPolyResourceMstrByUser(@QueryParam("usermasterid")String usermasterid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPatientByQueueNo")
	public Response getPatientByQueueNo(@QueryParam("queueno")String queueno);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPatientByQueueNoUpdate")
	public Response getPatientByQueueNoUpdate(@QueryParam("queueno")String queueno, @QueryParam("subspeciality")String subspeciality);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPharmacyResponseTime")
	public Response getPharmacyResponseTime(@QueryParam("tglAwal")String tglAwal,@QueryParam("tglAkhir")String tglAkhir);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPatientByQueueNo2")
	public Response getPatientByQueueNo2(@QueryParam("queueno2")String queueno2);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPatientByQueueNo2Update")
	public Response getPatientByQueueNo2Update(@QueryParam("queueno2")String queueno2,@QueryParam("subspeciality")String subspeciality);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSelfRegistration")
	public Response getSelfRegistration(@QueryParam("cardno")String cardno, @QueryParam("careproviderid") String careproviderid, @QueryParam("noantri") String noantri, @QueryParam("userid") String userid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getInsBrid")
	public Response getInsBrid(
			@QueryParam("cardno")String cardno,
			@QueryParam("bpjsno")String bpjsno,
			@QueryParam("sepno")String sepno,
			@QueryParam("vid")String vid,
			@QueryParam("careproviderid") String careproviderid, 
			@QueryParam("noantri") String noantri, 
			@QueryParam("userid") String userid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getInsSign")
	public Response getInsSign(
			@QueryParam("mrid")String mrid,
			@QueryParam("vid")String vid,
			@QueryParam("sign")String sign,
			@QueryParam("userid") String userid);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sendFileResume")
	public Response sendFileResume(UploadResume resumeform);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSelfRegistration2")
	public Response getSelfRegistration2(@QueryParam("cardno")String cardno, @QueryParam("careproviderid") String careproviderid, @QueryParam("noantri") String noantri, @QueryParam("userid") String userid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/RegistrationInfo")
	public Response registrationInfo(@QueryParam("regno")String regno);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addQueue")
	public Response addBpjsQueue(@QueryParam("queue_no") String queue_no);
		
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRegData")
	public Response GetRegData(@QueryParam("regno") String regno);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/SearchDoctor")
	public Response SearchDoctor(
			@QueryParam("searchtext") String searchtext,
			@QueryParam("lokasi") String lokasi);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCekTxn")
	public Response getCekTxn(@QueryParam("VisitId") String VisitId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getlisttxn")
	public Response getlisttxn(@QueryParam("Visitid") String visitid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcekvalidpas")
	public Response getcekvalidpas(@QueryParam ("bpjsNo") String bpjsno, @QueryParam("namaPas") String namapas, @QueryParam("tgLahir") String tglahir);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcancelreg")
	public Response getcancelreg (@QueryParam("Userid") String userid, @QueryParam("Visitid") String visitid, @QueryParam("Remarks") String remarks, @QueryParam("CancelReason") String cancelreason);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcancelQueue")
	public Response getcancelQueue (@QueryParam("cardno") String cardno, @QueryParam("tgl_berobat") String tgl_berobat);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcodecar")
	public Response getcodecar();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getpolydoc")
	public Response getpolydoc(@QueryParam("idhari") String idhari);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getpolybpjsdoc")
	public Response getpolybpjsdoc(@QueryParam("idhari") String idhari);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getpolydoc3")
	public Response getpolydoc3(@QueryParam("idhari") String idhari);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getpolydoc2")
	public Response getpolydoc2(@QueryParam("idhari") String idhari,@QueryParam("poli") String poli);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getpolyname")
	public Response getpolyname(@QueryParam("idhari") String idhari);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcekvalidantrian")
	public Response getcekvalidantrian (@QueryParam("CardNo") String CardNo, @QueryParam("IdNo") String IdNo, @QueryParam("BpjsNo") String BpjsNo);	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getResourceMaster")
	public Response getResourceMaster(@QueryParam("ResourceName") String ResourceName);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getResourceMaster2")
	public Response getResourceMaster2(@QueryParam("ResourceName") String ResourceName);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getResourceMaster3")
	public Response getResourceMaster3(@QueryParam("ResourceId") String ResourceId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/gettotal")
	public Response gettotal(@QueryParam("idhari") String idhari);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getResourceMasterjaddok")
	public Response getResourceMasterjaddok();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getResourceScheme")
	public Response getResourceScheme(@QueryParam("ResourceMstrId") String ResourceMstrId, @QueryParam("RegnDate") String RegnDate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPatientId")
	public Response getPatientId(@QueryParam("CardNo")String CardNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetResDokter")
	public Response GetResDokter(@QueryParam("CareProviderId") String CareProviderId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetDetailDokter")
	public Response GetDetailDokter(@QueryParam("dokterId") String dokterId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetQueueBpjs")
	public Response GetQueueBpjs(@QueryParam("CounterId") String CounterId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetQueueUser")
	public Response GetQueueUser(@QueryParam("UserMstrId") String UserMstrId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetOnlineReg")
	public Response GetOnlineReg(@QueryParam("Barcode") String Barcode);
	
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/GetOnlineReg2")
//	public Response GetOnlineReg2(@QueryParam("Barcode") String Barcode);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetDietPasien")
	public Response GetDietPasien(@QueryParam("Mrn") String Mrn);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetKelasPasien")
	public Response GetKelasPasien(@QueryParam("Pc") String Pc);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetKamarPasien")
	public Response GetKamarPasien(@QueryParam("Vid") String Vid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetVisitId")
	public Response GetVisitId(@QueryParam("mrn") String mrn);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetPoliCode")
	public Response GetPoliCode();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetUserName")
	public Response GetUserName(@QueryParam("UserCode") String UserCode);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetTindakan")
	public Response GetTindakan(@QueryParam("VisitId") String VisitId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getDailyDoctorSchedule")
	public Response getDailyDoctorSchedule(@QueryParam("RegnDate") String RegnDate);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetReceipt")
	public Response GetReceipt(@QueryParam("SysReceptNo") String SysReceptNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ListKamar")
	public Response ListKamar();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ListRuangan")
	public Response ListRuangan();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ListPasien")
	public Response ListPasien(@QueryParam("Ruangan") String Ruangan,@QueryParam("Tipe") String Tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetSignatureReport")
	public Response GetSignatureReport(@QueryParam("mrid") String mrid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetMedicalResume")
	public Response GetMedicalResume(
			@QueryParam("MedicalResumeId") String MedicalResumeId,
			@QueryParam("Ruangan") String Ruangan,
			@QueryParam("Tipe") String Tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetMedicalResumeByMrnNo")
	public Response getMedicalResumeByMrnNo(@QueryParam("mrnNo") String mrnNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetMedicalResumeById")
	public Response GetMedicalResumeById(@QueryParam("resumeId") String resumeId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getListMedicalVisitByMRN")
	public Response getListMedicalVisitByMRN(@QueryParam("mrn") String mrn);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getListPatientMedicalResumeByUserMstrId")
	public Response getListPatientMedicalResumeByUserMstrId(@QueryParam("userMstrId") String userMstrId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetDataKartu")
	public Response GetDataKartu(
			@QueryParam("bpjsNo") String bpjsNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetBpjsMedicine")
	public Response GetBpjsMedicine(
			@QueryParam("vid") String vid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetLabReport")
	public Response GetLabReport(@QueryParam("mrn") String mrn);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetPatientHiv")
	public Response GetPatientHiv(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getDoctorScheduleDay")
	public Response getDoctorScheduleDay(
			@QueryParam("idhari") String idhari,
			@QueryParam("subspecialty_id") String subspecialty_id
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/checkResourceStop")
	public Response checkResourceStop(
			@QueryParam("resourceMstrId") BigDecimal resourceMstrId,
			@QueryParam("regDate") String regDate
			);
}
