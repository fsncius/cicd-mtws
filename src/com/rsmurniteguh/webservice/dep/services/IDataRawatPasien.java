package com.rsmurniteguh.webservice.dep.services;

import java.math.BigDecimal;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.http.ResponseEntity;

@Path("/")
@WebService(name="DataRawatPasienService")
public interface IDataRawatPasien {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getdatarawatpasien")
	public Response getdatarawatpasien(@QueryParam("cardNo") String cardno);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getinfopasien")
	public Response getinfopasien (@QueryParam("CardNo") String CardNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getdatapasien")
	public Response getdatapasien(@QueryParam("CardNo") String CardNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getvisitprotokolhd")
	public Response getvisitprotokolhd(@QueryParam("visitid") String visitid);
}
