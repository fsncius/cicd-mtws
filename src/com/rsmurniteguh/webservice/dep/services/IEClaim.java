package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="EClaim")
public interface IEClaim {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReportSep")
	public Response getReportSep(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("sepNo") String sepNo
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getResumeMedis")
	public Response getResumeMedis(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("sepNo") String sepNo
			);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetSignatureReport")
	public Response GetSignatureReport(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("sepNo") String sepNo);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getLabReport")
	public Response getLabReport(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("sepNo") String sepNo
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPendingLabReport")
	public Response getPendingLabReport(
			@QueryParam("fromdate") String fromdate
			,@QueryParam("todate") String todate
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPoctReport")
	public Response getPoctReport(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("sepNo") String sepNo
			);
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRadiologyReport")
	public Response getRadiologyReport(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("sepNo") String sepNo
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCheckSep")
	public Response getCheckSep(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("sepNo") String sepNo
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSepReport")
	public Response getSepReport(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("fromDate") String fromDate
			,@QueryParam("toDate") String toDate
			,@QueryParam("tipe") String tipe
			,@QueryParam("noBa") String noBa
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSepReport")
	public Response getSepReport(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("fromDate") String fromDate
			,@QueryParam("toDate") String toDate
			,@QueryParam("tipe") String tipe
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSepReportBa")
	public Response getSepReportBa(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("noBa") String noBa
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getNoBa")
	public Response getNoBa(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("noBa") String noBa
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getReportDll")
	public Response getReportDll(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("sepNo") String sepNo
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBilling")
	public Response getBilling(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("sepNo") String sepNo
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCheckTxnStatus")
	public Response getCheckTxnStatus(
			@QueryParam("EClaimBPJS") String EClaimBPJS
			,@QueryParam("sepNo") String sepNo
			);
}
