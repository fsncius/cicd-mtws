package com.rsmurniteguh.webservice.dep.services;


import java.math.BigDecimal;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.QueuePhar;

@Path("/")
@WebService(name="DispanceBatchService")
public interface IDispanceBatch {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/DispanceBatch")
	public Response getdispancebatch(@QueryParam("DispanceBatchNo") String dispancebano );
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Getdrugdispensevisit")
	public Response getdrugdispensevisit(@QueryParam("visit_id") String visit_id, @QueryParam("prepared_datetime")String prepared_datetime);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Getdrugdispense")
	public Response getdrugdispense(@QueryParam("drugdispense_id") String drugdispense_id);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/DispanceBatch2")
	public Response getdispancebatch2(@QueryParam("DispanceBatchNo2") String dispancebano2, @QueryParam("PatientType") String patienttype, @QueryParam("PatientClass") String patientclass);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/DrugStore")
	public Response getdrugstore(@QueryParam("DrugReturnNo") String drugreturnno, @QueryParam("PatientType") String patienttype, @QueryParam("PatientClass") String patientclass);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/FrmDispance")
	public Response getdispensebatchno(@QueryParam("DispenseBatchNo") String dispensebatchno);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AntrianDis")
	public Response getstoremstrid(@QueryParam("StoreMstrId")String storemstrid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/DrugDispenseDetailInfo")
	public Response getdrugdispensedetailinfo(@QueryParam("DispenseBatchNo")String dispensebatchno);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/DrugDispenseDetailInfo2")
	public Response getdrugdispensedetailinfo2(@QueryParam("DispenseBatchNo")String dispensebatchno);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/CurrentDateTime")
	public Response getCurrentDatetime();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/PreparedDatetime")
	public Response getPreparedDatetime(@QueryParam("FirstDate")String firstdate, @QueryParam("LastDate")String lastdate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/TxnDateTime")
	public Response getTxnDateTime(@QueryParam("first")String first, @QueryParam("last")String last, @QueryParam("codedesc") String codedesc, @QueryParam("codedesc2") String codedesc2);	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/EnterDateTime")
	public Response getEnterDateTime(@QueryParam("first")String first, @QueryParam("last")String last, @QueryParam("codedesc") String codedesc, @QueryParam("codedesc2") String codedesc2);	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetVisitIp")
	public Response getVisitNo(@QueryParam("mrn") String mrn);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InfoRekonObat")
	public Response getrekonobat(@QueryParam ("visitid") String visitid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cekrem")
	public Response getcekrem(@QueryParam("fromdate") String fromdate, @QueryParam("todate") String todate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/stockreceipt")
	public Response getstockreceipt(@QueryParam("fromdate") String fromdate, @QueryParam("todate") String todate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/DispanceCount")
	public Response getDispanceCount(@QueryParam("fromdate") String fromdate, @QueryParam("todate") String todate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/PharPulvis")
	public Response getPharPulvis(@QueryParam("fromdate") String fromdate, @QueryParam("todate") String todate);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InfoRekonObatforcat")
	public Response getrekonobatforcat(@QueryParam ("visitid") String visitid);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InfoViewTrx")
	public Response getInfoViewTrx(@QueryParam("fromdate") String fromdate, @QueryParam("todate") String todate, @QueryParam("IdNik")String idnik);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InfoViewTrxNew")
	public Response getInfoViewTrxNew(@QueryParam("IdCompany")String IdCompany);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InfoViewTrxNewNoCompa")
	public Response getInfoViewTrxNewNoCompa();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InfoViewTrxNewDetail")
	public Response getInfoViewTrxNewDetail(@QueryParam("FromDate") String FromDate, @QueryParam("ToDate") String ToDate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InfoCekSts")
	public Response getInfoCekSts(@QueryParam("CardNo") String cardno);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InfoKaryawan")
	public Response getInfoKaryawan(@QueryParam("IdNik")String idnik);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/HasBPJS")
	public Response getHasBPJS(@QueryParam("PatientID") String patientid);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AllViewTrx")
	public Response getAllViewTrx(@QueryParam("fromdate") String fromdate, @QueryParam("todate") String todate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Allkaryawan")
	public Response getAllKaryawan();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ListKamar")
	public Response getAllRoom();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddJournal")
	public Response addApotekGl(
		@QueryParam("nopo") String nopo,
		@QueryParam("total") String total,
		@QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddJournalDetail")
	public Response addApotekGlDetail(
		@QueryParam("nopo") String nopo,
		@QueryParam("tglt") String tglt,
		@QueryParam("nourt") String nourt,
		@QueryParam("sandi") String sandi,
		@QueryParam("kat")	String kat,
		@QueryParam("nasup") String nasup,
		@QueryParam("total") String total,
		@QueryParam("ido") String ido,
		@QueryParam("tipe") String tipe);
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddNilaiJournal")
	public Response addNilai(
		@QueryParam("sandi") String sandi,
		@QueryParam("tgl") String tgl,
		@QueryParam("nsp") String nsp,
		@QueryParam("nopo") String nopo,
		@QueryParam("ppn") String ppn,
		@QueryParam("nppn") String nppn,
		@QueryParam("dis") String dis,
		@QueryParam("tot") String tot,
		@QueryParam("nopr") String nopr,
		@QueryParam("noref") String noref,
		@QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddHutang")
	public Response addHutang(
		@QueryParam("nobukti") String nobukti,
		@QueryParam("ids") String ids,
		@QueryParam("tgl") String tgl,
		@QueryParam("tgljt") String tgljt,
		@QueryParam("user") String user,
		@QueryParam("total") String total,
		@QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddHutangDetail")
	public Response addHutangDetail(
		@QueryParam("nobukti") String nobukti,
		@QueryParam("kategori") String kategori,
		@QueryParam("sandi") String sandi,
		@QueryParam("qty") BigDecimal qty,
		@QueryParam("biaya") BigDecimal biaya,
		@QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddPiutang")
	public Response addPiutang(
		@QueryParam("nobukti") String nobukti,
		@QueryParam("ids") String ids,
		@QueryParam("tgl") String tgl,
		@QueryParam("user") String user,
		@QueryParam("total") String total,
		@QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddPiutangDetail")
	public Response addPiutangDetail(
		@QueryParam("nobukti") String nobukti,
		@QueryParam("kategori") String kategori,
		@QueryParam("sandi") String sandi,
		@QueryParam("qty") BigDecimal qty,
		@QueryParam("biaya") BigDecimal biaya,
		@QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddRtb")
	public Response addRtb(
		@QueryParam("nopo") String nopo,
		@QueryParam("total") String total,
		@QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/NilaiRtb")
	public Response nilaiRtb(
		@QueryParam("nopo") String nopo,
		@QueryParam("nsp") String nsp,
		@QueryParam("tgl") String tgl,
		@QueryParam("totalret") String totalret,
		@QueryParam("ppn") String ppn,
		@QueryParam("total") String total,
		@QueryParam("kategori") String kategori,
		@QueryParam("sandi") String sandi,
		@QueryParam("tipe") String tipe);
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddJpj")
	public Response addJpj(
		@QueryParam("idt") String idt,
		@QueryParam("total") String total,
		@QueryParam("tipe") String tipe,
		@QueryParam("user") String user);
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddJpjDetail")
	public Response addJpjDetail(
		@QueryParam("idt") String idt,
		@QueryParam("tglt") String tglt,
		@QueryParam("dis") String dis,
		@QueryParam("sandi") String sandi,
		@QueryParam("nasup") String nasup,
		@QueryParam("total") String total,
		@QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/AddJpjNilai")
	public Response addJpjNilai(
		@QueryParam("sandi") String sandi,
		@QueryParam("tgl") String tgl,
		@QueryParam("nsp") String nsp,
		@QueryParam("idt") String idt,
		@QueryParam("tot") String tot,
		@QueryParam("kat") String kat,
		@QueryParam("tipe") String tipe);
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/CekBalanceNik")
	public Response getCekBalanceNik(@QueryParam("Nik") String Nik);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/CekTransaksiNik")
	public Response getCekTransaksiNik(@QueryParam("AccountBlcId") String AccountBlcId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/DataPasienSms")
	public Response getDataPasienSms();
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/DelQueuePhar")
	public Response GetDelQueuePhar(QueuePhar DataPhar);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Updatedrugdispense")
	public Response updatedrugdispense(@QueryParam("drugdispense_id") String drugdispense_id);

//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/DataSms")
//	public Response getDataSms(@QueryParam("nohp") String nohp, @QueryParam("pesan") String pesan, @QueryParam("keterangan") String keterangan, @QueryParam("status") String status);
	
}
