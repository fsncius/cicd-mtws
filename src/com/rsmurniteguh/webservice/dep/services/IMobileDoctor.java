package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name = "MobileDoctor")
public interface IMobileDoctor {
	
	//old mobility	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_trx_emoney")
	public Response get_trx_emoney(@QueryParam("nik") String nik, @QueryParam("startdate") String startdate, @QueryParam("enddate") String enddate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getMainDataDoctor")
	public Response getMainDataDoctor(@QueryParam("usermstr_id") String usermstr_id);
	// end of old mobility
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetPoliclynicLists")
	public Response GetPoliclynicLists();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetPoliclynicListsByLocation")
	public Response GetPoliclynicListsByLocation(@QueryParam("location") String location);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetDoctorsList")
	public Response GetDoctorsList(@QueryParam("poli") String poli, @QueryParam("location") String location);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetDoctorSchedule")
	public Response GetDoctorSchedule(@QueryParam("resourcemstr") String resourcemstr);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getResourceScheme")
	public Response getResourceScheme(@QueryParam("resourcemstr") String resourcemstr,
			@QueryParam("tanggal") String tanggal);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_doctorlist_schedule")
	public Response get_doctorlist_schedule(@QueryParam("poli") String poli);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_doctorlist_schedule_by_name")
	public Response getDoctorListScheduleByName(@QueryParam("name") String name);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetDoctorsPolyName")
	public Response GetDoctorsPolyName(@QueryParam("poli") String poli, @QueryParam("location") String location);
	
}
