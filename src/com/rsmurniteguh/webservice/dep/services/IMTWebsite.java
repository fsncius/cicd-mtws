package com.rsmurniteguh.webservice.dep.services;

import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import com.rsmurniteguh.webservice.dep.all.model.JobsRecruitmentRegistration;
import com.rsmurniteguh.webservice.dep.all.model.RegistrasiNewPatient;

@Path("/")
@WebService(name="MTWebsite")
public interface IMTWebsite {

	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response upload(@QueryParam("type") String type,@QueryParam("name")  String name, List<Attachment> attachment, @Context HttpServletRequest request);

	@GET
	@Path("/download")
	@Produces({ "image/jpg", "image/gif" })
	public Response download(@QueryParam("gambar_id") String gambar_id, @QueryParam("type") String type);

	@GET
	@Path("/getBerita")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBerita(@QueryParam("kategori") String kategori, @QueryParam("type") String type, @QueryParam("tags") String tags,@QueryParam("status") String status);
	
		
	@GET
	@Path("/getJudulBerita")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJudulBerita(@QueryParam("judul") String judul, @QueryParam("kategori") String kategori);
	
	@GET
	@Path("/getSaveViewerBerita")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSaveViewerBerita(@QueryParam("ids") String ids, @QueryParam("jumlah") String jumlah);
	
	@GET
	@Path("/getMegazine")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMegazine();
	
	@GET
	@Path("/SaveReadOnline")
	@Produces(MediaType.APPLICATION_JSON)
	public Response SaveReadOnline(@QueryParam("firstname") String firstname, @QueryParam("lastname") String lastname, @QueryParam("email") String email, @QueryParam("phone") String phone);
	
	@GET
	@Path("/SaveKonsultasi")
	@Produces(MediaType.APPLICATION_JSON)
	public Response SaveKonsultasi(@QueryParam("fullname") String fullname,@QueryParam("email") String email, @QueryParam("phone") String phone, @QueryParam("gender") String gender, @QueryParam("blood") String blood, @QueryParam("height") String height, @QueryParam("weight") String weight, @QueryParam("age") String age, @QueryParam("jobs") String jobs, @QueryParam("complaint") String complaint, @QueryParam("spesialis") String spesialis);
	
	@GET
	@Path("/getWhatNew")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWhatNew(@QueryParam("code") String code, @QueryParam("version") String version, @QueryParam("type") String type);
	
	@GET
	@Path("/getTahunGallery")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTahunGallery();
	
	@GET
	@Path("/getJenisGallery")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJenisGallery();
	
	@GET
	@Path("/getGallerybyTahun")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGallerybyTahun(@QueryParam("tahun") String tahun);
	
	@GET
	@Path("/getGallerybyJudul")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGallerybyJudul(@QueryParam("judul") String judul);
	
	
	@POST
	@Path("/uploadRujukanPasien")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadRujukanPasien(List<Attachment> attachment, @Context HttpServletRequest request);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addPasienBaru")
	public Response addPasienBaru(RegistrasiNewPatient registrasiNewPatient);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addJobsRekruitment")
	public Response addJobsRekruitment(JobsRecruitmentRegistration jobsRecruitmentRegistration);
	
	@GET
	@Path("/addKodeKonfirmRekruitment")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addKodeKonfirmRekruitment(@QueryParam("idRegist") String idRegist,@QueryParam("kodeKonfirm") String kodeKonfirm,@QueryParam("param") String param);
	
	
	@GET
	@Path("/addKonfirmasiByEmail")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addKonfirmasiByEmail(@QueryParam("idRegist") String idRegist,@QueryParam("kodeKonfirm") String kodeKonfirm);
	
	@GET
	@Path("/getKonfirmasiByIdRegist")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKonfirmasiByIdRegist(@QueryParam("idRegist") String idRegist);
	
	@GET
	@Path("/getPasienBaru")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPasienBaru(@QueryParam("barcode") String barcode);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCheckPasienBpjsbyNoBpjs")
	public Response getCheckPasienBpjsbyNoBpjs(@QueryParam("nobpjs") String nobpjs);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getInfoPasienByNIKBPJS")
	public Response getInfoPasienByNIKBPJS(@QueryParam("nomor") String nomor,@QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRegistrationOption")
	public Response getRegistrationOption();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getListDropDownCityMstr")
	public Response getListDropDownCityMstr(@QueryParam("stateMstrId")String stateMstrId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sendEmailPasienBaru")
	public Response sendEmailPasienBaru(@QueryParam("regId")String regId);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/checkLoginPatientAntrian")
	public Response checkLoginPatientAntrian(@FormParam("user") String user, @FormParam("pass") String pass);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/checkUserPatientAntrian")
	public Response checkUserPatientAntrian(@FormParam("mrn") String mrn, @FormParam("ktp") String ktp, @FormParam("bpjs") String bpjs);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/saveUserPatientAntrian")
	public Response saveUserPatientAntrian(@FormParam("mrn") String mrn, @FormParam("ktp") String ktp, @FormParam("bpjs") String bpjs, @FormParam("username") String username, @FormParam("fullname") String fullname, @FormParam("contact") String contact, @FormParam("email") String email, @FormParam("pass") String pass);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getUserbyCard")
	public Response getUserbyCard(@FormParam("mrn") String mrn, @FormParam("ktp") String ktp, @FormParam("bpjs") String bpjs);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/resetPasswordUser")
	public Response resetPasswordUser(@FormParam("mrn") String mrn, @FormParam("userid") String userid, @FormParam("pass") String pass, @FormParam("username") String username, @FormParam("tipe") String tipe);
	
/*
	@GET
	@Path("/getTahunGallery")
	public Response getTahunGallery(@QueryParam("type") String type);
	
	
	@GET
	@Path("/getGallery")
	public Response getGallery(@QueryParam("tahun") String tahun, @QueryParam("type") String type);
*/	
	
	
}
