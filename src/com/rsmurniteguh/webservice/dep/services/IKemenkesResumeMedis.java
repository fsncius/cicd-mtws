package com.rsmurniteguh.webservice.dep.services;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;
import com.rsmurniteguh.webservice.dep.all.model.kemenkes.resumemedis.InsertParam;

@Component
public interface IKemenkesResumeMedis {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Insert")
	public Response insert(InsertParam param);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Get")
	public Response get(@QueryParam("tanggalAwal") String tanggalAwal, @QueryParam("tanggalAkhir") String tanggalAkhir);
}
