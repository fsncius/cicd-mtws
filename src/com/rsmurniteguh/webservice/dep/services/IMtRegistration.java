package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.PDFUpload;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.CancelReg;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegBpjs;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegGeneral;

@Path("/")
@WebService(name="MTREGISTRATION")
public interface IMtRegistration {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBaseInfo")
	public Response getBaseInfo(@QueryParam("baseno") String baseno);
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/insertRegKthisBpjs")
	public Response insertRegKthisBpjs(InsertRegBpjs insertRegBpjs);
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/insertRegKthisGeneral")
	public Response insertRegKthisGeneral(InsertRegGeneral insertRegGeneral);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/insertRegKthisBpjs2")
	public Response insertRegKthisBpjs2(InsertRegBpjs insertRegBpjs);
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/insertRegKthisGeneral2")
	public Response insertRegKthisGeneral2(InsertRegGeneral insertRegGeneral);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cancelReg")
	public Response cancelReg(CancelReg cancelReg);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSep")
	public Response getSep(@QueryParam("baseno") String baseno);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sendSEP")
	public Response sendSep(PDFUpload sep);
	
	
}