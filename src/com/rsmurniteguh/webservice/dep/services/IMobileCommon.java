package com.rsmurniteguh.webservice.dep.services;

import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;


@Path("/")
@WebService(name = "MobileCommon")
public interface IMobileCommon {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getWhatsNew")
	public Response getWhatsNew(@QueryParam("projectCode") String projectCode, @QueryParam("version") String version);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAndroidVersion")
	public Response getAndroidVersion(@QueryParam("projectCode") String projectCode);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_berita")
	public Response get_berita(@QueryParam("reader_role") String reader_role);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/add_kotak_saran")
	public Response add_kotak_saran(
			@FormParam("user_id") String user_id, 
			@FormParam("user_name") String user_name, 
			@FormParam("project_code") String project_code, 
			@FormParam("version") String version, 
			@FormParam("saran") String saran,
			@FormParam("kontak") String kontak
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBpjsData")
	public Response getBpjsData(@QueryParam("noKartuBpjs") String noKartuBpjs);
}
