package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rsmurniteguh.webservice.dep.all.model.hris.CreateCuti;
import com.rsmurniteguh.webservice.dep.all.model.hris.RequestCreateIzin;

@Path("/")
@WebService(name="HRIS")
public interface IHris {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_employe_detail")
	public Response get_employe_detail(
			@QueryParam("id_karyawan") String id_karyawan
			);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_employe_attendance")
	public Response get_employe_attendance(
			@QueryParam("finger_id") String finger_id,
			@QueryParam("kodemesin") String kodemesin,
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_izin_data")
	public Response get_izin_data(
			@QueryParam("id_karyawan") String id_karyawan,
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_cuti_data")
	public Response get_cuti_data(
			@QueryParam("id_karyawan") String id_karyawan,
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_data_request_task_cuti")
	public Response get_data_request_task_cuti(
			@QueryParam("id_karyawan") String id_karyawan
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/view_create_cuti")
	public Response view_create_cuti(
			@QueryParam("id_karyawan") String id_karyawan
			);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/create_cuti")
	public Response create_cuti(
			CreateCuti createCuti
			);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/update_cuti")
	public Response update_cuti(CreateCuti createCuti);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/delete_cuti")
	public Response delete_cuti(
			@QueryParam("kodecuti") String kodecuti
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_cuti_info")
	public Response get_cuti_info(
			@QueryParam("kodecuti") String kodecuti
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_atasan")
	public Response get_atasan(
			@QueryParam("id_karyawan") String id_karyawan
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_bawahan")
	public Response get_bawahan(
			@QueryParam("kode_jabatan") String kode_jabatan
			);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/konfirmasi_izin_cuti")
	public Response konfirmasi_izin_cuti(
			@FormParam("id") String id, 
			@FormParam("alasan") String alasan, 
			@FormParam("status") String status, 
			@FormParam("tipe") String tipe, 
			@FormParam("idkaryawan") String idkaryawan, 
			@FormParam("kodepermohonan") String kodepermohonan
			);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/create_izin_hris")
	public Response create_izin_hris(RequestCreateIzin izin);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/update_izin_hris")
	public Response update_izin_hris(RequestCreateIzin izin);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/delete_izin_hris")
	public Response delete_izin_hris(RequestCreateIzin izin);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_request_approval")
	public Response get_request_approval(
			@QueryParam("id_karyawan") String id_karyawan,
			@QueryParam("status") String status
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_berita")
	public Response get_berita(
			@QueryParam("reader_role") String reader_role
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get_login_hris")
	public Response get_login_hris(
			@QueryParam("username") String username, 
			@QueryParam("password") String password
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetEmoney")
	public Response GetEmoney(
			@QueryParam("id_karyawan") String id_karyawan
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/HitungHariCuti")
	public Response HitungHariCuti(
			@QueryParam("id_karyawan") String id_karyawan,
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate
			);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/add_kotak_saran")
	public Response add_kotak_saran(
			@FormParam("user_id") String user_id, 
			@FormParam("user_name") String user_name, 
			@FormParam("project_code") String project_code, 
			@FormParam("version") String version, 
			@FormParam("saran") String saran,
			@FormParam("kontak") String kontak
			);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/authPass")
	public Response authPass();
}
