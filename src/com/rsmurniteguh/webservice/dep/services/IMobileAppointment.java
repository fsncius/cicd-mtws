package com.rsmurniteguh.webservice.dep.services;

import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

@Path("/")
@WebService(name = "MobileAppointment")
public interface IMobileAppointment {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/appointmentHistory")
	public Response appointmentHistory(@QueryParam("user_id") String user_id, @QueryParam("mrn") String mrn,
			@QueryParam("lokasi") String lokasi);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/appointmentList")
	public Response appointmentList(@QueryParam("user_id") String user_id, @QueryParam("mrn") String mrn);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UserCreateApointment")
	public Response UserCreateApointment(@FormParam("mrn") String mrn, @FormParam("resourcemstr") String resourcemstr,
			@FormParam("user_id") String user_id, @FormParam("tanggal") String tanggal,
			@FormParam("lokasi") String lokasi, @FormParam("careprovider_id") String careprovider_id,
			@FormParam("doctor_name") String doctor_name, @FormParam("no_subspecialis") String no_subspecialis,
			@FormParam("no_doctor")String no_doctor, @FormParam("patient_name")String patient_name, @FormParam("no_mrn")String no_mrn);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cancelAppointment")
	public Response cancelAppointment(@QueryParam("reg_id") String reg_id);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/checkAvailable")
	public Response checkAvailable(@FormParam("tanggal") String tanggal, @FormParam("user_id") String user_id,
			@FormParam("careprovider_id") String careprovider_id, @FormParam("resourcemstr") String resourcemstr);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/notificationAppointment")
	public Response notificationAppointment(@QueryParam("tanggal") String tanggal);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/notificationDirect")
	public Response notificationDirect(@FormParam("message") String tanggal, @FormParam("user_id") String user_id);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/InfoRegistration")
	public Response InfoRegistration(@FormParam("registrasi_id") String tanggal);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getDoctorQueueList")
	public Response getDoctorQueueList(@FormParam("careproviderId") String careproviderId,
			@FormParam("subspecialtymstr") String subspecialtymstr);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAllAppointmentByStatus")
	public Response getAllAppointmentByStatus(@FormParam("tanggal") String tanggal, @FormParam("status") String status);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/topRegisteredAppointment")
	public Response topRegisteredAppointment(@FormParam("tanggal") String tanggal);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getDataRujukan")
	public Response getDataRujukan(@QueryParam("mrn") String mrn);

	@POST
	@Path("/addRujukan")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addRujukan(@FormParam("mrn") String mrn, @FormParam("userid") String userid,
			@FormParam("no_rujukan") String no_rujukan, @FormParam("tanggal_rujukan") String tanggal_rujukan,
			@FormParam("gambar") String gambar);

	@POST
	@Path("/uploadRujukan")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadRujukan(List<Attachment> attachment, @Context HttpServletRequest request);

	@GET
	@Path("/downloadRujukan")
	@Produces({ "image/jpg", "image/gif" })
	public Response downloadRujukan(@QueryParam("rujukan_id") String rujukan_id, @QueryParam("jenis") String jenis);

	
	
	
	
}

