package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="WardRoomService")

public interface IWardRoom {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getWardRoom")
	public Response getWardRoom();
}
