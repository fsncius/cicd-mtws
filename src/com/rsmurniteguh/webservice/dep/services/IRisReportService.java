package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="RisReportService")
public interface IRisReportService {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getrisreport")
	public Response getrisreport(@QueryParam("PerStartDate") String perstartdate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getrisadmission")
	public Response getrisadmission(@QueryParam("AdmissionId")String admissionid);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getrisreportbypatient")
	public Response getRisByPatient(@QueryParam("patientName")String patientName, @QueryParam("patientId")String patientId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getxrayrisreport")
	public Response getxrayrisreport(@QueryParam("PerStartDate") String perstartdate);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getxrayrisreportadmission")
	public Response getxrayrisreportadmission(@QueryParam("AdmissionId")String admissionid);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getrisadmissionlist")
	public Response getrisadmissionlist(@QueryParam("AdmissionIdList")String admissionidlist);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getrisreportadmission")
	public Response getrisreportadmission(@QueryParam("AdmissionId")String admissionid);
}
