package com.rsmurniteguh.webservice.dep.services;

import java.math.BigDecimal;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="InacbgService")
public interface IInacbg {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cetakKlaim")
	public Response cetakKlaim(@QueryParam("noSep") String noSep);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/tambahKlaim")
	public Response tambahKlaim(
			@QueryParam("noKa") String noKa,
			@QueryParam("noSep") String noSep,
			@QueryParam("mrn") String mrn,
			@QueryParam("nama") String nama,
			@QueryParam("tglLahir") String tglLahir,
			@QueryParam("gender") String gender);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateKlaim")
	public Response updateKlaim(
			@QueryParam("noSep") String noSep,
			@QueryParam("noKa") String noKa,
			@QueryParam("tglMasuk") String tglMasuk,
			@QueryParam("tglPulang") String tglPulang,
			@QueryParam("jnsRawat") String jnsRawat,
			@QueryParam("klsRawat") String klsRawat,
			@QueryParam("icuInd") String icuInd,
			@QueryParam("icuLos") String icuLos,
			@QueryParam("ventHour") String ventHour,
			@QueryParam("upInd") String upInd,
			@QueryParam("upCls") String upCls,
			@QueryParam("upLos") String upLos,
			@QueryParam("addPay") String addPay,
			@QueryParam("brWgt") String brWgt,
			@QueryParam("disSts") String disSts,
			@QueryParam("diag") String diag,
			@QueryParam("pro") String pro,
			@QueryParam("trfPnb") String trfPnb,
			@QueryParam("trfBdh") String trfBdh,
			@QueryParam("trfKon") String trfKon,
			@QueryParam("trfTa") String trfTa,
			@QueryParam("trfKep") String trfKep,
			@QueryParam("trfPnj") String trfPnj,
			@QueryParam("trfRad") String trfRad,
			@QueryParam("trfLab") String trfLab,
			@QueryParam("trfPd") String trfPd,
			@QueryParam("trfReh") String trfReh,
			@QueryParam("trfKmr") String trfKmr,
			@QueryParam("trfRi") String trfRi,
			@QueryParam("trfOb") String trfOb,
			@QueryParam("trfAl") String trfAl,
			@QueryParam("trfBm") String trfBm,
			@QueryParam("trfSa") String trfSa,
			@QueryParam("naDok") String naDok);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/groupingStage1")
	public Response groupingStage1(
			@QueryParam("noSep") String noSep);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/groupingStage2")
	public Response groupingStage2(
			@QueryParam("noSep") String noSep,
			@QueryParam("cmg") String cmg);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/finalKlaim")
	public Response finalKlaim(
			@QueryParam("noSep") String noSep);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updatePasien")
	public Response updatePasien(
			@QueryParam("mrn") String mrn,
			@QueryParam("bpjs") String bpjs,
			@QueryParam("nama") String nama,
			@QueryParam("tgllahir") String tgllahir,
			@QueryParam("gender") String gender);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/hapusPasien")
	public Response hapusPasien(
			@QueryParam("mrn") String mrn);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/editUlangKlaim")
	public Response editUlangKlaim(
			@QueryParam("sep") String sep);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sendKlaim")
	public Response sendKlaim(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate,
			@QueryParam("jenis") String jenis,
			@QueryParam("tipe") String tipe);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sendKlaimIndividual")
	public Response sendKlaimIndividual(
			@QueryParam("sep") String sep);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getKlaim")
	public Response getKlaim(
			@QueryParam("startdate") String startdate,
			@QueryParam("enddate") String enddate,
			@QueryParam("jenis") String jenis);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getDetailKlaimbySep")
	public Response getDetailKlaimbySep(
			@QueryParam("sep") String sep);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getStatusKlaimbySep")
	public Response getStatusKlaimbySep(
			@QueryParam("sep") String sep);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteKlaimbySep")
	public Response deleteKlaimbySep(
			@QueryParam("sep") String sep);


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchDiagnosa")
	public Response searchDiagnosa(
			@QueryParam("keyword") String keyword);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchProcedure")
	public Response searchProcedure(
			@QueryParam("keyword") String keyword);

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/integrasiKthis")
	public Response integrasiKthis(
			@QueryParam("visitId") BigDecimal visitId);
	
	
	
	
}
