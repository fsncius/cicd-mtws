package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="KpiSystemsImpService")
public interface IKpiSystems {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ViewDemography")
	public Response getKpiQuer1();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ViewDisease")
	public Response getKpiQuer2();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ViewJenisKamar")
	public Response ViewJenisKamar();
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ViewKunjungan")
	public Response getViewKunjungan();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ViewPembiayaan")
	public Response getViewPembiayaan();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ViewWaktuLayanIGD")
	public Response getWaktuLayanIGD();
	
}
