package com.rsmurniteguh.webservice.dep.services;

import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="MobileStaff")
public interface IMobileStaff {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetDoctorRegistrationList")
	public Response GetDoctorRegistrationList(@QueryParam("usermstrId")String usermstrId);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetDoctorIPVisitList")
	public Response GetDoctorIPVisitList(@QueryParam("usermstrId")String usermstrId);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/GetRadiologySchedule")
	public Response GetRadiologySchedule(@QueryParam("date")String date, @QueryParam("type")String type);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getPurchaseRequisition")
	public Response getPurchaseRequisition();
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/setPurchaseRequisition")
	public Response setPurchaseRequisition(
			@FormParam("stockpurchase_requisitionid")String stockpurchase_requisitionid,
			@FormParam("isconfirm")String isconfirm);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRisReportTime")
	public Response getRisReportTime(
			@QueryParam("tanggal")String tanggal,
			@QueryParam("jenis") String jenis);
}
