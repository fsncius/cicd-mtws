package com.rsmurniteguh.webservice.dep.util;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.rsmurniteguh.webservice.dep.base.ISysConstant;

public class LanguageUtil {
	private static LanguageUtil languageUtil = new LanguageUtil();
	
	private String mainLanguage;
	
	/**
	 * <p>Description: 从外部指定系统主语言，此修改为全局修改，通常只需要在服务启动时，以及客户端登录时调用</p>
	 * @param defaultLanguage
	 */
	public void setMainLanguage(String mainLanguage) {
		this.mainLanguage = mainLanguage;
		this.setDefaultLanguage(mainLanguage);
	}
	
	public String getMainLanguage(){
		return mainLanguage;
	}

	private LanguageUtil() {}
	
	/**
	 * <p>Description: 获取LanguageUtil实例</p>
	 * @return
	 */
	public static LanguageUtil getInstance() {
		return languageUtil;
	}
	
	/**
	 * <p>Description: 判断传入的语言是否为系统主语言，主语言可以从外部指定</p>
	 * @param language
	 * @return
	 */
	public boolean isMainLanguage(String language) {
		if (language != null && !"".equals(language)) {
			if (mainLanguage != null) {
				return language.equals(mainLanguage);
			} else {
				return language.startsWith("zh");
			}
		} else {
			return false;
		}
	}
	
	/**
	 * <p>Description: 根据传入的语言选择主描述或附加描述</p>
	 * @param language
	 * @param desc
	 * @param descLang1
	 * @return
	 */
	public String getNormalDesc(String language, String desc, String descLang1) {
		return isMainLanguage(language) ? desc : (descLang1 != null ? descLang1 : desc);
	}
	
	/**
     * <p>Description: 得到存储过程抛出的异常的主语言描述 </p>
     * @param cn
     * @param en
     * @return
     */
	public String getProcedureError(Map<String, Object> map) {
	    if (map.get("poErrorCode") == null) {
	        return null;
	    }
	    
	    return this.getProcedureError((String) map.get("poErrorCnMsg"), (String) map.get("poErrorEnMsg"));
	}
	
	/**
	 * <p>Description: 得到存储过程抛出的异常的主语言描述 </p>
	 * @param cn
	 * @param en
	 * @return
	 */
	public String getProcedureError(String cn, String en) {
	    return isMainLanguage(ISysConstant.LANGUAGE_CN) ? (cn != null ? cn : en) : (en != null ? en : cn);
	}
	
	/**
	 * <p>Description: 设置Locale里面的默认语言，操作系统的语言将会影响ResourceBundle对于默认值的读取
	 *   （会使用操作系统的语言，而不是用系统参数定义的主语言）。 </p>
	 * @param language 前台：登录时选择的系统语言；后台：系统参数中定义的主语言
	 */
	private void setDefaultLanguage(String language) {
		if (ISysConstant.LANGUAGE_CN.equals(language)) { //$NON-NLS-1$
			Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
		}
		if (ISysConstant.LANGUAGE_TW.equals(language)) { //$NON-NLS-1$
			Locale.setDefault(Locale.TRADITIONAL_CHINESE);
		}
		if (ISysConstant.LANGUAGE_EN.equals(language)) { //$NON-NLS-1$
			Locale.setDefault(Locale.US);
		}		
	}

	private Map<String, String> priceUnitDescMap = new HashMap<String, String>();
	
	public String getPriceUnitDesc(String language) {
		if (language == null) {
			language = mainLanguage;
		}
		return priceUnitDescMap.get(language) == null ? "" : priceUnitDescMap.get(language);
	}

	private Map<String, String> priceUnitMarkMap = new HashMap<String, String>();
	
	public String getPriceUnitMark(String language) {
		if (language == null) {
			language = mainLanguage;
		}
		return priceUnitMarkMap.get(language) == null ? "" : priceUnitMarkMap.get(language);
	}
}
