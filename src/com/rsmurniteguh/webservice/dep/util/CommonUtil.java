package com.rsmurniteguh.webservice.dep.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Base64;

import javax.ws.rs.core.HttpHeaders;

public class CommonUtil {
	
	
	
	private static CommonUtil commonUtils = new CommonUtil();
	private CommonUtil() {
	}

	public static CommonUtil getInstance() {
		return commonUtils;
	}
	
	public boolean isNotEmpty(Object object) {
		return (object != null && !("".equals(object)));
	}
	
	public String convertToReadableName(String personName){
		if (personName == null) return null;
		String result = "";
		String regex = "[/\\\\]";
		try {
//			String fullName = personName.toUpperCase().replace("DR.", "");
			String[] parts = personName.trim().split(regex);
			String[] frontParts = parts[0].trim().split("[(]");
//			String[] bachParts = frontParts[0].trim().split("[,]");
			result = frontParts[0];
//			String[] nameParts = bachParts[0].trim().split("[\\s]");
//			for (int a = nameParts.length - 1;a>=0;a--){
//				if (result.equals("")) {
//					result += nameParts[a].trim();
//				} else {
//					result += "^" + nameParts[a].trim();
//				}	
//			}
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
		return result;
	}
	
	public static PreparedStatement initialStatement(PreparedStatement ps){
		try {
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60);
		} catch (SQLException e){
			e.printStackTrace();
		}
		return ps;
	}
	
	public String getUserMstrIdFromAuth(HttpHeaders headers){
		String auth = headers.getHeaderString("Authorization");
		String authorization = auth.substring("Basic ".length());
        byte[] decoded = Base64.getDecoder().decode(authorization);
        String decodedString = new String(decoded);
        
        String[] actualCredentials = decodedString.split(":");
        String ID = actualCredentials[0];
        String Password = actualCredentials[1];
        
		return ID;
	}
	
	public String getPasswordFromAuth(HttpHeaders headers){
		String auth = headers.getHeaderString("Authorization");
		String authorization = auth.substring("Basic ".length());
        byte[] decoded = Base64.getDecoder().decode(authorization);
        String decodedString = new String(decoded);
        decodedString = decodedString.substring(decodedString.indexOf(":")+1);

		return decodedString;
	}
}
