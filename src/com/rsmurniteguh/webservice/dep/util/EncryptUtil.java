package com.rsmurniteguh.webservice.dep.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptUtil {
	public static final String ALGORITHM_MD5 = "MD5";
	public static final String ALGORITHM_SHA1 = "SHA-1";
	public static final String ALGORITHM_RSA = "RSA";
	
	private String[] hexDigits = { "0", "1", "2", "3", "4", "5", "6", "7", "8",
			"9", "a", "b", "c", "d", "e", "f" };
	
	private static final EncryptUtil encryptUtil = new EncryptUtil();
	
	private EncryptUtil(){
		
	}
	
	public static EncryptUtil getInstance(){
		return encryptUtil;
	}
	
	public String nonReversibleEncode(String originStr){
		String resultStr = null;
		if (originStr != null) {
			try {
				MessageDigest sha1MD = MessageDigest.getInstance(ALGORITHM_SHA1);
				resultStr = byteArrayToHexString(sha1MD.digest(originStr.getBytes()));
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
		return resultStr;
	}
	
	public String byteArrayToHexString(byte[] b) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			buffer.append(byteToHexString(b[i]));
		}
		return buffer.toString();
	}

	private String byteToHexString(byte b) {
		int n = b;
		if (n < 0)
			n = 256 + n;
		int d1 = n / 16;
		int d2 = n % 16;
		return hexDigits[d1] + hexDigits[d2];
	}
	
	public String StringtoMD5(String md5){
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.reset();
			m.update(md5.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String encrypt_md5 = bigInt.toString(16);
			// Now we need to zero pad it if you actually want the full 32 chars.
			while(encrypt_md5.length() < 32 ){
				encrypt_md5 = "0"+encrypt_md5;
			}
			
			return encrypt_md5;
			
	    } catch (java.security.NoSuchAlgorithmException e) {
	    	System.out.println(e.getMessage());
	    }
	    return null;
	}
}
