package com.rsmurniteguh.webservice.dep.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptPatient {
	public static final String ALGORITHM_MD5 = "MD5";
	public static final String ALGORITHM_SHA1 = "SHA-1";
	public static final String ALGORITHM_RSA = "RSA";

	private String[] hexDigits = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
	private static final char[] HEX_DIGITS = "0123456789ABCDEF".toCharArray();
	private static final EncryptPatient encryptUtil = new EncryptPatient();

	private EncryptPatient() {

	}

	public static EncryptPatient getInstance() {
		return encryptUtil;
	}
	
	
	public static String encryptOnlineAppointment(String pass)
    {
    	String sha1 = convertSha1forOnlineAppoinment(pass);
    	String md5 = convertMd5forOnlineAppoinment(sha1);
    	String reverse = new StringBuilder(md5).reverse().toString();
    	return reverse;
    }

	private static String convertMd5forOnlineAppoinment(String pass) {
		try {
			String result = pass;
			if (pass != null) {
				MessageDigest md = MessageDigest.getInstance("MD5"); // or
																		// "SHA-1"
				md.update(pass.getBytes());
				BigInteger hash = new BigInteger(1, md.digest());
				result = hash.toString(16);
				while (result.length() < 32) { // 40 for SHA-1
					result = "0" + result;
				}
			}
			return result;
		} catch (Exception e) {

		}
		return null;
	}

	private static String convertSha1forOnlineAppoinment(String pass) {
		String ret = "";
		byte[] digest = null, theTextToDigestAsBytes = null;
		try {
			theTextToDigestAsBytes = pass.getBytes();
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(theTextToDigestAsBytes);
			digest = md.digest();

			char[] chars = new char[digest.length * 2];
			for (int i = 0; i < digest.length; i++) {
				chars[i * 2] = HEX_DIGITS[(digest[i] >> 4) & 0xf];
				chars[i * 2 + 1] = HEX_DIGITS[digest[i] & 0xf];
			}
			// return new String(chars);

			ret = new String(chars).toLowerCase();

			// ret = new java.math.BigInteger(1, digest).toString(16);
			return (ret);
		} catch (NoSuchAlgorithmException g) {
			return ("");
		}
	}


	public String byteArrayToHexString(byte[] b) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			buffer.append(byteToHexString(b[i]));
		}
		return buffer.toString();
	}

	private String byteToHexString(byte b) {
		int n = b;
		if (n < 0)
			n = 256 + n;
		int d1 = n / 16;
		int d2 = n % 16;
		return hexDigits[d1] + hexDigits[d2];
	}
	
	public String StringtoMD5(String md5){
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.reset();
			m.update(md5.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String encrypt_md5 = bigInt.toString(16);
			// Now we need to zero pad it if you actually want the full 32 chars.
			while(encrypt_md5.length() < 32 ){
				encrypt_md5 = "0"+encrypt_md5;
			}
			
			return encrypt_md5;
			
	    } catch (java.security.NoSuchAlgorithmException e) {
	    	System.out.println(e.getMessage());
	    }
	    return null;
	}
}
