package com.rsmurniteguh.webservice.dep.util;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;
import com.rsmurniteguh.webservice.dep.tasks.DailyEmailScheduledTask;

public abstract class EmailUtil {
	private Properties properties;
	
	private String host;
	protected String from;
	protected String password;

	protected String receipentTo = "victor@rsmurniteguh.com";
	protected String receipentCC = "";
	protected String receipentBCC = "victor@rsmurniteguh.com";
	
	protected String dir;
	protected String reportName; //Name of the report (only one file)
	protected String emailSubject; //The subject of the email
	protected List<String> listReport; //list of attachment name - fill empty if not needed 
	
	public EmailUtil() {
		CommonService cs = new CommonService();
		host = cs.getParameterValue(IParameterConstant.HOST_EMAIL);
		
		from = "info.mtmh@rsmurniteguh.com";
		password = "murniteguh";
		
		properties = System.getProperties();
	    properties.setProperty("mail.smtp.host", host);
	    properties.put("mail.smtp.auth", "true");
	    
	    ClassLoader classLoader = new DailyEmailScheduledTask().getClass().getClassLoader();
        File file = new File(classLoader.getResource("report/DailyReport.jrxml").getFile());
        dir = file.getParent();
	    
        emailSubject = "Report";
        reportName = "";
	}
	
	protected void sendMessage(String body) {
		Transport transport = null;
		try {
			CommonService cs = new CommonService();
			Session session = Session.getDefaultInstance(properties);
		    
	    	session.setDebug(true);
	    	
	    	transport = session.getTransport();
	    	transport.connect(from, password);
	    	
	    	BodyPart messageBodyPart = new MimeBodyPart();
	    	if (body == null) body = "";
	    	
	    	messageBodyPart.setContent(body, "text/html");
	    	Multipart multipart = new MimeMultipart();
	    	multipart.addBodyPart(messageBodyPart);
	    		    	
	    	if (listReport != null && listReport.size() > 0){
	    		for (int a=0;a<listReport.size();a++){	    			
	    			BodyPart mb2 = new MimeBodyPart();
	    			mb2.setDataHandler(new DataHandler(new FileDataSource(dir.concat("/listReport" + (a+1) + ".pdf"))));
	    			mb2.setFileName(listReport.get(a) + ".pdf");
			    	multipart.addBodyPart(mb2);
	    		}
	    	}else if (!reportName.equals("")) {
	    		messageBodyPart = new MimeBodyPart();
	    		messageBodyPart.setDataHandler(new DataHandler(new FileDataSource(dir.concat("/" + reportName + ".pdf"))));
		    	messageBodyPart.setFileName(reportName + ".pdf");
		    	multipart.addBodyPart(messageBodyPart);
	    	}
	    	
	    	Message message = new MimeMessage(session);
	        message.setFrom(new InternetAddress(from, "Automatic Reporting"));
	        if (receipentTo != "")
	        	message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receipentTo));
	        if (receipentCC != "")
	        	message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(receipentCC));
	        if (receipentBCC != "")
	        	message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(receipentBCC));
	        message.setSubject(emailSubject);
	        message.setContent(multipart);
	
	        transport.sendMessage(message, message.getAllRecipients());
	        System.out.println("Sending Email : " + emailSubject);
		}
		catch(MessagingException e) {
			throw new RuntimeException(e);
		}
		catch (Exception e) {
	    	e.printStackTrace();
	    }
		finally {
			if (transport != null) try { transport.close(); } catch(Exception e) {}
		}
	}
}
