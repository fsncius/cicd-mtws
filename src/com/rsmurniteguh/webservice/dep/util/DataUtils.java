package com.rsmurniteguh.webservice.dep.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.Connection;

import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

import serp.bytecode.NewArrayInstruction;

public abstract class DataUtils {
	public static final SimpleDateFormat DATE_FORMAT_DB = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	public static final SimpleDateFormat DATE_FORMAT_FILE = new SimpleDateFormat("yyyy\\MM\\dd", Locale.US);
    private static final String mountDir = "/mnt/";
	
	public static Date StringToDate(String tgl)
	{
		
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date date = new Date();
        try
        {
            date = simpleDateFormat.parse(tgl);

//            System.out.println("date : "+simpleDateFormat.format(date));
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }
		
		return date;
	}
	
	public static Calendar StringToCalendar(String tgl)
	{
		
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date date = new Date();
        try
        {
            date = simpleDateFormat.parse(tgl);

//            System.out.println("date : "+simpleDateFormat.format(date));
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
		return cal;
	}
	
	/*
	 * iterate over class members
	 * @autor : Benyamin Ginting
	 */
	public static Map<String, String> getObject(Object obj) throws IllegalArgumentException, IllegalAccessException{
		
		Map<String, String> res = new HashMap<String, String>();
		for (Field field : obj.getClass().getDeclaredFields()) {
	        //field.setAccessible(true); // if you want to modify private fields
			res.put(field.getName(), field.get(obj).toString());
	    }
		return res;
	}
	
	@SuppressWarnings("finally")
	public static Boolean MurniteguhInterceptor(String username, String password)
	{
		Boolean result = false;
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null) return null;
		String SPsql = "select 1 from usertoken where usermstr_id = ? and salt like ? and deleted = 0";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, username);
			ps.setString(2, password);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = true;
			}
			else
			{
				result = false;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (rs != null) try  { rs.close(); } catch (final Exception ignore){}
		    if (ps != null) try  { ps.close(); } catch (final Exception ignore){}
		    if (connection != null) try { connection.close(); } catch (final Exception ignore){}
		    return result;
		}
	}
	
	@SuppressWarnings("finally")
	public static Boolean checkStaffLogin(String username, String password)
	{
		String reEncryptKthis = EncryptUtil.getInstance().nonReversibleEncode(Encryptor.getInstance().decrypt(password));
		String reEncryptHris = EncryptUtil.getInstance().StringtoMD5(Encryptor.getInstance().decrypt(password));
		
		Connection connection = DbConnection.getPooledConnection();
		Connection connection_new=DbConnection.getHrisInstance();
		if (connection == null) return null;
		Boolean result = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps_new = null;
		ResultSet rs_new = null;
		
		try {

			String SPsql = "select 1 from usermstr where user_code = ? and password = ? and defunct_ind = 'N'";

			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, username);
			ps.setString(2, reEncryptKthis);
			
			rs = ps.executeQuery();
			
			
			if (rs.next()) {
				result = true;
			}
			else
			{
				SPsql = " SELECT usr.Username, usr.Password, kar.Nama, usr.IDKaryawan , kar.KodeJabatan, kar.KodeBagian, jab.Jabatan, bag.bagian "
						+ "FROM TblUsers usr  " + "left join TblKaryawan kar ON usr.IDKaryawan = kar.IDKaryawan "
						+ "left join TblJabatan jab ON kar.KodeJabatan = jab.KodeJabatan "
						+ "left join TblBagian bag ON kar.KodeBagian = bag.Kodebagian "
						+ "where usr.Username = ?  and usr.Password = ?  and usr.Aktif = 1 ";
				
				ps_new = connection_new.prepareStatement(SPsql);
				ps_new.setEscapeProcessing(true);
				ps_new.setQueryTimeout(60000);
				ps_new.setString(1, username);
				ps_new.setString(2, reEncryptHris);
				rs_new=ps_new.executeQuery();
				
				if (rs_new.next()) {
					result = true;
				}
				else
				{
					result = false;
				}
			}
			

		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (rs != null) try  { rs.close(); } catch (final Exception ignore){}
		    if (ps != null) try  { ps.close(); } catch (final Exception ignore){}
			if (rs_new != null) try  { rs_new.close(); } catch (final Exception ignore){}
		    if (ps_new != null) try  { ps_new.close(); } catch (final Exception ignore){}
		    if (connection != null) try { connection.close(); } catch (final Exception ignore){}
		    if (connection_new != null) try { connection_new.close(); } catch (final Exception ignore){}
		}
		return result;
	}
	
	public static Boolean checkPatientLogin(String username, String password)
	{
		String reEncryptPatient = EncryptPatient.getInstance().encryptOnlineAppointment(Encryptor.getInstance().decrypt(password));
		
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null) return null;
		Boolean result = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = "select 1 from tb_user where username = ? and password = ?";
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, username);
			ps.setString(2, reEncryptPatient);
			rs = ps.executeQuery();
			
			
			if (rs.next()) {
				result = true;
			}
			else
			{
				result = false;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (rs != null) try  { rs.close(); } catch (final Exception ignore){}
		    if (ps != null) try  { ps.close(); } catch (final Exception ignore){}
		    if (connection != null) try { connection.close(); } catch (final Exception ignore){}
		}
	    return result;
	}
	
	public static String getCurrentTimeLog(){
		String currentTimeString = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString();
//		String currentTimeString = "<" + new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + ">"; 
		return currentTimeString;
	}
	
	public static void printCurrentTimeLog(){
		ColorUtil.print(getCurrentTimeLog(), ColorUtil.WHITE, ColorUtil.MAGENTA);
		System.out.print(" ");
	}
	
	public static String ifUnixDir(String dir){
		if (CommonService.isUnix()) {
			if (dir.indexOf("\\\\\\\\") == 0) {
				dir = dir.replace("\\\\\\\\", mountDir).replace("\\\\", "/").replace("\\", "/").trim();
			}else {
				dir = dir.replace("\\\\", mountDir).replace("\\", "/").trim();
			}
		}
		return dir;
	}
	
}
