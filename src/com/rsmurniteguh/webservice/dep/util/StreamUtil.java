package com.rsmurniteguh.webservice.dep.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Project: com.kompakar.ehealth.frame</p>
 * <p>Description: </p>
 * <p>Copyright (c) 2005-2007 Kompakar Technology (Shanghai) Co., Ltd.</p>
 * <p>All Rights Reserved.</p>
 * @author <a href="mailto:gaoyuxiang@kompakar.com.cn">Gao Yuxiang</a>
 */
public class StreamUtil {

	private static final StreamUtil streamUtil = new StreamUtil();

	private StreamUtil() {
	}

	public static StreamUtil getInstance() {
		return streamUtil;
	}

	public <T> T clone(T object) throws IOException, ClassNotFoundException {
		byte[] bytes = this.object2Bytes(object);
		Object result = this.bytes2Object(bytes);
		return (T) result;
	}

	public byte[] object2Bytes(Object object) throws IOException {
		byte[] bytes = null;
		if (object != null) {
			ByteArrayOutputStream baos = null;
			ObjectOutputStream oos = null;
			try {
				baos = new ByteArrayOutputStream();
				oos = new ObjectOutputStream(baos);
				oos.writeObject(object);
				oos.flush();
				baos.flush();
				bytes = baos.toByteArray();
			} catch (IOException e) {
				System.err.println(getClass() + " IOException occured in performing object2Bytes(Object object)");
				e.printStackTrace();
				throw e;
			} finally {
				if (oos != null) {
					oos.close();
				}
				if (baos != null) {
					baos.close();
				}
			}
		}
		return bytes;
	}

	public Object bytes2Object(byte[] bytes) throws IOException, ClassNotFoundException {
		Object object = null;
		if (bytes != null && bytes.length > 0) {
			ByteArrayInputStream bais = null;
			ObjectInputStream ois = null;
			try {
				bais = new ByteArrayInputStream(bytes);
				ois = new ObjectInputStream(bais);
				object = ois.readObject();
			} catch (ClassNotFoundException e) {
				System.err.println(getClass() + " ClassNotFoundException occured in performing bytes2Object(byte[] bytes)");
				e.printStackTrace();
				throw e;
			} catch (IOException e) {
				System.err.println(getClass() + " IOException occured in performing bytes2Object(byte[] bytes)");
				e.printStackTrace();
				throw e;
			} catch (Exception e) {
				System.err.println(getClass() + " (Unknown)Exception occured in performing bytes2Object(byte[] bytes)");
				e.printStackTrace();
			} finally {
				if (ois != null) {
					ois.close();
				}
				if (bais != null) {
					bais.close();
				}
			}
		}
		return object;
	}

	public byte[] readBytes(InputStream is, int bufferSize) throws IOException {
		byte[] bytes = null;
		if (is != null) {
			int readNumbers = 0;
			int totalSize = 0;
			boolean errorOccured = false;
			List<byte[]> byteArrayList = new ArrayList<byte[]>();
			BufferedInputStream gis = new BufferedInputStream(is);
			byte[] buffer = new byte[bufferSize];
			int count = 0;
			try {
				do {
					try {
						while ((readNumbers = gis.read(buffer)) != -1) {
							byte[] element = new byte[readNumbers];
							System.arraycopy(buffer, 0, element, 0, readNumbers);
							byteArrayList.add(element);
							totalSize += readNumbers;
						}
						errorOccured = false;
					} catch (SocketTimeoutException e) {
						System.err.println(getClass()
								+ " SocketTimeoutException occured in performing readBytes(InputStream is,int byteOfReadEveryTime)");
						e.printStackTrace();
						errorOccured = true;
						count++;
						if (count == 2) {
							throw e;
						}
					}
				} while (errorOccured);
			} catch (IOException e) {
				System.err.println(getClass() + " IOException occured in performing readBytes(InputStream is,int byteOfReadEveryTime)");
				e.printStackTrace();
				throw e;
			} finally {
				if (gis != null) {
					gis.close();
				}
				if (is != null) {
					is.close();
				}
			}
			int byteArraySize = byteArrayList.size();
			bytes = new byte[totalSize];
			int indexFlag = 0;
			for (int i = 0; i < byteArraySize; i++) {
				byte[] byteArray = byteArrayList.get(i);
				int byteArrayLength = byteArray.length;
				for (int j = 0; j < byteArrayLength; j++) {
					bytes[indexFlag++] = byteArray[j];
				}
			}
		}
		return bytes;
	}

	public void writeBytes(OutputStream os, byte[] bytes, int bufferSize) throws IOException {
		if (os != null && bytes != null) {
			BufferedOutputStream gos = new BufferedOutputStream(os);
			int totalBytes = bytes.length;
			int entireCircleTimes = totalBytes / bufferSize;
			int remainBytes = totalBytes % bufferSize;
			int indexFlag = 0;

			if (entireCircleTimes > 0) {
				byte[] buffer = new byte[bufferSize];
				for (int i = 0; i < entireCircleTimes; i++) {
					System.arraycopy(bytes, indexFlag, buffer, 0, bufferSize);
					try {
						gos.write(buffer);
						gos.flush();
						os.flush();
					} catch (IOException e) {
						System.err.println(getClass() + " IOException occured in performing writeBytes(OutputStream os,byte[] bytes)");
						e.printStackTrace();
						throw e;
					}
					indexFlag += bufferSize;
				}
			}
			if (remainBytes > 0) {
				byte[] buffer = new byte[remainBytes];
				System.arraycopy(bytes, indexFlag, buffer, 0, remainBytes);
				try {
					gos.write(buffer);
					gos.flush();
					os.flush();
				} catch (IOException e) {
					System.err.println(getClass() + " IOException occured in performing writeBytes(OutputStream os,byte[] bytes)");
					e.printStackTrace();
					throw e;
				}
			}
			if (gos != null) {
				gos.close();
			}
			if (os != null) {
				os.close();
			}
		}
	}

	/**
	 * <p>Description: close {@link InputStream} </p>
	 * @param stream
	 */
	public void close(InputStream stream) {
		try {
			if (stream != null) {
				stream.close();
			}
		} catch (Exception e) {
		} finally {
			stream = null;
		}
	}

	/**
	 * <p>Description: close {@link OutputStream} </p>
	 * @param stream
	 */
	public void close(OutputStream stream) {
		try {
			if (stream != null) {
				stream.flush();
				stream.close();
			}
		} catch (Exception e) {
		} finally {
			stream = null;
		}
	}
}