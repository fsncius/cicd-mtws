package com.rsmurniteguh.webservice.dep.util;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionUtil {
	private static CollectionUtil instance = new CollectionUtil();
	public static CollectionUtil getInstance(){
		return instance;
	}
	
	public interface IPredicate<T> { boolean apply(T type); }
	
	public <T> Collection<T> filter(Collection<T> target, IPredicate<T> predicate){
		Collection<T> result = new ArrayList<T>();
	    for (T element: target) {
	        if (predicate.apply(element)) {
	            result.add(element);
	        }
	    }
	    return result;
	}
}
