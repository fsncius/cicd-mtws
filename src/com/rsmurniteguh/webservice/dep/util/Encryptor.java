package com.rsmurniteguh.webservice.dep.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class Encryptor {
	protected static final String key = "Bar12345Bar12345"; // 128 bit key
	protected static final String initVector = "RandomInitVector"; // 16 bytes IV
	
	
	private static final Encryptor encryptor = new  Encryptor();
	private Encryptor()
	{
		
	}
	
	public static Encryptor getInstance()
	{
		return encryptor;
	}
		
	public String encrypt(String key, String initVector, String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            byte[] encrypted = cipher.doFinal(value.getBytes("UTF-8"));
            
            Base64 ed=new Base64();

            String encoded=new String(ed.encode(encrypted));
            return encoded;
//            return Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String decrypt(String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(Encryptor.initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(Encryptor.key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            
            Base64 ed=new Base64();

//            String decoded=new String(ed.decode(encrypted.getBytes()));
            
            
            //byte[] original = cipher.doFinal(ed.decode(encrypted.getBytes()));
            byte[] original = cipher.doFinal(ed.decode(encrypted));
            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

//    public static void main(String[] args) {
//        String key = "Bar12345Bar12345"; // 128 bit key
//        String initVector = "RandomInitVector"; // 16 bytes IV
//
//        System.out.println(decrypt(key, initVector,
//                encrypt(key, initVector, "Hello World")));
//    }
}
