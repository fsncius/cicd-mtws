package com.rsmurniteguh.webservice.dep.util;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Color;
import org.fusesource.jansi.AnsiConsole;

import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class ColorUtil {
	public static Color ERROR_COLOR = Ansi.Color.RED;
	public static Color BLACK = Color.BLACK;
	public static Color RED = Color.RED;
	public static Color GREEN = Color.GREEN;
	public static Color YELLOW = Color.YELLOW;
	public static Color BLUE = Color.BLUE;
	public static Color MAGENTA = Color.MAGENTA;
	public static Color CYAN = Color.CYAN;
	public static Color WHITE = Color.WHITE;
	
	public static Color TRACE_GROUP_1_FOREGROUND = Color.BLACK;
	public static Color TRACE_GROUP_1_BACKGROUND = Color.WHITE;
	
	public static Color IPFOREGROUND = Color.WHITE;
	public static Color IPBACKGROUND = Color.GREEN;
	
	public static Color STATUSFOREGROUND = Color.BLUE;
	public static Color STATUSBACKGROUND = Color.BLACK;
	public static Color STATUSWARNINGFOREGROUND = Color.WHITE;
	public static Color STATUSWARNINGBACKGROUND = Color.YELLOW;
	public static Color STATUSDANGERBACKGROUND = Ansi.Color.RED;
	
	public static void print(String message){
		print(message, WHITE);
	}
	
	public static void print(String message, Color fontColor) {
		print(message, fontColor, BLACK);
	}
	
	public static void print(String message, Color fontColor, Color bgColor){
		AnsiConsole.systemInstall();
		try {
			System.out.print(Ansi.ansi().bg(bgColor).fg(fontColor).a(message).reset());	
		} catch (Exception e){
			System.out.print(message);
		}
		AnsiConsole.systemUninstall();
	}
	
	public static void println(String message){
		println(message, WHITE);
	}
	
	public static void println(String message, Color fontColor){
		println(message, fontColor, BLACK);
	}
	
	public static void println(String message, Color fontColor, Color bgColor){
		AnsiConsole.systemInstall();
		try {
			System.out.println(Ansi.ansi().bg(bgColor).fg(fontColor).a(message).reset());	
		} catch (Exception e){
			System.out.println(message);
		}
		
		AnsiConsole.systemUninstall();
	}
	
	public static void printConnection(String connectionName){
		String message = "Open " + connectionName + " Service Connection";
		println(message, getColorBasedOnConnection(connectionName), getBGColorBasedOnConnection(connectionName));
	}

	public static void printIP(String ip){
		print(ip, IPFOREGROUND, IPBACKGROUND);
	}
	
	private static Color getColorBasedOnConnection(String connectionName){
		switch (connectionName) {
		case DbConnection.KTHIS:
		case DbConnection.WEB_SERVICE:
			return GREEN;
		case DbConnection.QUEUESYSTEM:
		case DbConnection.ONLINEQUEUESYSTEM2:
			return YELLOW;
		case DbConnection.RSPAJAK:
		case DbConnection.GLCONNECTION:
		case DbConnection.GLCONNECTION2:
		case DbConnection.GLCONNECTION3:
			return CYAN;
		case DbConnection.GYM:
		case DbConnection.HRIS:
		case DbConnection.BPJSONLINE:
			return BLUE;
		case DbConnection.PROAPOTIK:
		case DbConnection.RIS:
		case DbConnection.RIS_REPORT:
		case DbConnection.TRX:
		case DbConnection.ROOMCONNECTION:
			return MAGENTA;
		case DbConnection.ANTRIAN:
			return TRACE_GROUP_1_FOREGROUND;
		default:
			return WHITE;
		}		
	}
	
	private static Color getBGColorBasedOnConnection(String connectionName){
		switch (connectionName) {
		case DbConnection.WEB_SERVICE:
			return MAGENTA;
		case DbConnection.ANTRIAN:
			return TRACE_GROUP_1_BACKGROUND;
		default:
			return BLACK;
		}
	}
}
