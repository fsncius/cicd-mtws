package com.rsmurniteguh.webservice.dep.util;

import java.math.BigDecimal;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.rsmurniteguh.webservice.dep.all.model.gym.Pair;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class AES {
	private static final int KEY_SIZE = 128;
	private static final int PSWD_ITERATIONS = 10000;
	private static final String AES_TYPE = "AES/CBC/PKCS5Padding";
	
	public static AES getInstance() { 
		return new AES();
	}
	
	private SecretKeySpec getSecretKeySpec(String pass, String salt) throws Exception {
		// get salt
		byte[] saltBytes = salt.getBytes("UTF-8");
		
		// Derive the key
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		PBEKeySpec spec = new PBEKeySpec(pass.toCharArray(), saltBytes, PSWD_ITERATIONS, KEY_SIZE);
		SecretKey secretKey = factory.generateSecret(spec);
		return new SecretKeySpec(secretKey.getEncoded(), "AES");
	}
	
	public String generateSalt() {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		return new String(bytes);
	}
	
	@SuppressWarnings("finally")
	public Pair<String, String> encrypt(String plainText, String usermstrId) {
		Connection connection = DbConnection.getGymInstance();
		if (connection == null) return null;
		
		String SPsql = "select salt from usertoken where usermstr_id = ? and deleted = 0";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		Pair<String, String> pair = null;
		
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, usermstrId);
			
			rs = ps.executeQuery();
			
			String salt = null;
			String pass = usermstrId;
			if (rs.next()) {
				salt = rs.getString("salt");
			}
			
			if (salt == null || pass == null) throw new Exception("File cannot be encrypt, user key not valid"); 
			
			SecretKeySpec spec = getSecretKeySpec(pass, salt);
			
			Cipher cipher = Cipher.getInstance(AES_TYPE);
			cipher.init(Cipher.ENCRYPT_MODE, spec);
			
			AlgorithmParameters params = cipher.getParameters();
			byte[] ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
			byte[] encrypedTypeBytes = cipher.doFinal(plainText.getBytes("UTF-8"));
			pair = new Pair<String, String>(new Base64().encodeAsString(ivBytes), new Base64().encodeAsString(encrypedTypeBytes));
			System.out.println(pair.getKey());
			System.out.println(pair.getValue());
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (rs != null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps != null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection != null) try { connection.close(); } catch (Exception ignore){}
		    return pair;
		}
	}
	
	@SuppressWarnings("finally")
	public String decrypt(Pair<String, String> encryptedText, String usermstrId, String salt) throws Exception {
		String string = null;
		try {
			String pass = usermstrId;
			if (salt == null || pass == null) throw new Exception("File cannot be decrypt, user key not valid"); 
			
			byte[] ivBytes = new Base64().decodeBase64(encryptedText.getKey());
			byte[] encryptedTextBytes = new Base64().decodeBase64(encryptedText.getValue());
		
			SecretKeySpec spec = getSecretKeySpec(pass, salt);
			
			Cipher cipher = Cipher.getInstance(AES_TYPE);
			cipher.init(Cipher.DECRYPT_MODE, spec, new IvParameterSpec(ivBytes));
			
			byte[] decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
			string = new String(decryptedTextBytes);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
		    return string;
		}
	}
}
