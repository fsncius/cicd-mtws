package com.rsmurniteguh.webservice.dep.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base32;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorConfig;
import com.warrenstrange.googleauth.GoogleAuthenticatorException;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import com.warrenstrange.googleauth.IGoogleAuthenticator;

public class GoogleAuthenticatorCustom implements IGoogleAuthenticator{

	  public static final String RNG_ALGORITHM = "com.warrenstrange.googleauth.rng.algorithm";

	    /**
	     * The system property to specify the random number generator provider to use.
	     *
	     * @since 0.5.0
	     */
	    public static final String RNG_ALGORITHM_PROVIDER = "com.warrenstrange.googleauth.rng.algorithmProvider";

	    /**
	     * The logger for this class.
	     */
	    private static final Logger LOGGER = Logger.getLogger(GoogleAuthenticatorCustom.class.getName());

	    /**
	     * The number of bits of a secret key in binary form. Since the Base32
	     * encoding with 8 bit characters introduces an 160% overhead, we just need
	     * 80 bits (10 bytes) to generate a 16 bytes Base32-encoded secret key.
	     */
	    private static final int SECRET_BITS = 80;

	    /**
	     * Number of scratch codes to generate during the key generation.
	     * We are using Google's default of providing 5 scratch codes.
	     */
	    private static final int SCRATCH_CODES = 5;

	    /**
	     * Number of digits of a scratch code represented as a decimal integer.
	     */
	    private static final int SCRATCH_CODE_LENGTH = 8;

	    /**
	     * Modulus used to truncate the scratch code.
	     */
	    public static final int SCRATCH_CODE_MODULUS = (int) Math.pow(10, SCRATCH_CODE_LENGTH);

	    /**
	     * Magic number representing an invalid scratch code.
	     */
	    private static final int SCRATCH_CODE_INVALID = -1;

	    /**
	     * Length in bytes of each scratch code. We're using Google's default of
	     * using 4 bytes per scratch code.
	     */
	    private static final int BYTES_PER_SCRATCH_CODE = 4;

	    /**
	     * The default SecureRandom algorithm to use if none is specified.
	     *
	     * @see java.security.SecureRandom#getInstance(String)
	     * @since 0.5.0
	     */
	    @SuppressWarnings("SpellCheckingInspection")
	    private static final String DEFAULT_RANDOM_NUMBER_ALGORITHM = "SHA1PRNG";

	    /**
	     * The default random number algorithm provider to use if none is specified.
	     *
	     * @see java.security.SecureRandom#getInstance(String)
	     * @since 0.5.0
	     */
	    private static final String DEFAULT_RANDOM_NUMBER_ALGORITHM_PROVIDER = "SUN";

	    /**
	     * Cryptographic hash function used to calculate the HMAC (Hash-based
	     * Message Authentication Code). This implementation uses the SHA1 hash
	     * function.
	     */
	    private static final String HMAC_HASH_FUNCTION = "HmacSHA1";

	    /**
	     * The configuration used by the current instance.
	     */
	    private final GoogleAuthenticatorConfig config;
	    
	    public GoogleAuthenticatorCustom()
	    {
	        config = new GoogleAuthenticatorConfig();
	    }
	@Override
	public GoogleAuthenticatorKey createCredentials() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GoogleAuthenticatorKey createCredentials(String userName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean authorize(String secret, int verificationCode) throws GoogleAuthenticatorException {
		return authorize(secret, verificationCode, new Date().getTime());
	}

	@Override
	public boolean authorizeUser(String userName, int verificationCode) throws GoogleAuthenticatorException {
		// TODO Auto-generated method stub
		return false;
	}
	public GoogleAuthenticatorCustom(GoogleAuthenticatorConfig config)
    {
        if (config == null)
        {
            throw new IllegalArgumentException("Configuration cannot be null.");
        }

        this.config = config;
    }

	public boolean authorize(String secret, int verificationCode, long time)
            throws GoogleAuthenticatorException
    {
        // Checking user input and failing if the secret key was not provided.
        if (secret == null)
        {
            throw new IllegalArgumentException("Secret cannot be null.");
        }

        // Checking if the verification code is between the legal bounds.
        if (verificationCode <= 0 || verificationCode >= this.config.getKeyModulus())
        {
            return false;
        }

        // Checking the validation code using the current UNIX time.
        return checkCode(
                secret,
                verificationCode,
                time,
                this.config.getWindowSize());
    }
	private long getTimeWindowFromTime(long time)
    {
        return time / this.config.getTimeStepSizeInMillis();
    }
	 int calculateCode(byte[] key, long tm)
	    {
	        // Allocating an array of bytes to represent the specified instant
	        // of time.
	        byte[] data = new byte[8];
	        long value = tm;

	        // Converting the instant of time from the long representation to a
	        // big-endian array of bytes (RFC4226, 5.2. Description).
	        for (int i = 8; i-- > 0; value >>>= 8)
	        {
	            data[i] = (byte) value;
	        }

	        // Building the secret key specification for the HmacSHA1 algorithm.
	        SecretKeySpec signKey = new SecretKeySpec(key, HMAC_HASH_FUNCTION);

	        try
	        {
	            // Getting an HmacSHA1 algorithm implementation from the JCE.
	            Mac mac = Mac.getInstance(HMAC_HASH_FUNCTION);

	            // Initializing the MAC algorithm.
	            mac.init(signKey);

	            // Processing the instant of time and getting the encrypted data.
	            byte[] hash = mac.doFinal(data);

	            // Building the validation code performing dynamic truncation
	            // (RFC4226, 5.3. Generating an HOTP value)
	            int offset = hash[hash.length - 1] & 0xF;

	            // We are using a long because Java hasn't got an unsigned integer type
	            // and we need 32 unsigned bits).
	            long truncatedHash = 0;

	            for (int i = 0; i < 4; ++i)
	            {
	                truncatedHash <<= 8;

	                // Java bytes are signed but we need an unsigned integer:
	                // cleaning off all but the LSB.
	                truncatedHash |= (hash[offset + i] & 0xFF);
	            }

	            // Clean bits higher than the 32nd (inclusive) and calculate the
	            // module with the maximum validation code value.
	            truncatedHash &= 0x7FFFFFFF;
	            truncatedHash %= config.getKeyModulus();

	            // Returning the validation code to the caller.
	            return (int) truncatedHash;
	        }
	        catch (NoSuchAlgorithmException | InvalidKeyException ex)
	        {
	            // Logging the exception.

	            // We're not disclosing internal error details to our clients.
	            throw new GoogleAuthenticatorException("The operation cannot be "
	                    + "performed now.");
	        }
	    }
	 private boolean checkCode(
	            String secret,
	            long code,
	            long timestamp,
	            int window)
	    {
	        byte[] decodedKey = decodeSecret(secret);
String s = new String(decodedKey);
	        // convert unix time into a 30 second "window" as specified by the
	        // TOTP specification. Using Google's default interval of 30 seconds.
	        final long timeWindow = getTimeWindowFromTime(timestamp);

	        // Calculating the verification code of the given key in each of the
	        // time intervals and returning true if the provided code is equal to
	        // one of them.
	        for (int i = -((window - 1) / 2); i <= window / 2; ++i)
	        {
	            // Calculating the verification code for the current time interval.
	            long hash = calculateCode(decodedKey, timeWindow + i);

	            // Checking if the provided code is equal to the calculated one.
	            if (hash == code)
	            {
	                // The verification code is valid.
	                return true;
	            }
	        }

	        // The verification code is invalid.
	        return false;
	    }

	    private byte[] decodeSecret(String secret)
	    {
	        // Decoding the secret key to get its raw byte representation.
	        switch (config.getKeyRepresentation())
	        {
	            case BASE32:
	                Base32 codec32 = new Base32();
	                return codec32.decode(secret);
	            case BASE64:
	                return Base64.getDecoder().decode(secret);
	            default:
	                throw new IllegalArgumentException("Unknown key representation type.");
	        }
	    }
}
