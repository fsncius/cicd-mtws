package com.rsmurniteguh.webservice.dep.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.StringRefAddr;
import javax.naming.spi.ObjectFactory;
import org.apache.tomcat.jdbc.pool.DataSourceFactory;
import org.fusesource.jansi.Ansi.Color;

import com.rsmurniteguh.webservice.dep.base.ISysConstant;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;
import com.securecontext.secure.InitConnection;


public class WebServiceDataSourceFactory extends DataSourceFactory implements ObjectFactory {
	private static final String COLUMN_DEVELOPMENT = "DEVELOPMENT";
	private static final String COLUMN_PRODUCTION = "PRODUCTION";
	
	private static final Pattern _propRefPattern = Pattern.compile("\\$\\{.*?\\}");

	@Override
	public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable environment) throws Exception {
		if (obj instanceof Reference) {
			Reference ref = (Reference) obj;
			CommonService cs = new CommonService();
			String type = cs.getProgramEnvironment();
			//System.out.println("Resolving " + name.toString()); //biar cantik sekali aja system out print nya
			
			String connectionString = getConnectionString(name.toString(),type);
			InitConnection newIc = new InitConnection();
			newIc.SetConnection(connectionString);
			
			for (int i = 0; i < ref.size(); i++) {
				RefAddr addr = ref.get(i);
				String tag = addr.getType();
				String value = (String) addr.getContent();

				Matcher matcher = _propRefPattern.matcher(value);
				if (matcher.find()) {
					String resolvedValue = newIc.getRequest(value);
					//System.out.println(resolvedValue);
					ref.remove(i);
					ref.add(i, new StringRefAddr(tag, resolvedValue));
				}
			}
			ColorUtil.print("Resolved " + name.toString());
			ColorUtil.println("[" + (type.equals(ISysConstant.ENV_PRODUCTION) ? COLUMN_PRODUCTION : COLUMN_DEVELOPMENT) + "]", ColorUtil.WHITE, ColorUtil.GREEN);;
		}
		return super.getObjectInstance(obj, name, nameCtx, environment);
	}
	
	private static String getConnectionString(String value, String type){
		Connection wsConnection = getWebServicePooledConnection();
		if (wsConnection == null){
			System.out.println("Unable to connect to Web Service database");
			return null;
		}
		
		String result = null;
		
		PreparedStatement pstmt = null;
		ResultSet rsWS = null;
		try {
			String query = "SELECT " + (type.equals(ISysConstant.ENV_PRODUCTION) ? COLUMN_PRODUCTION : COLUMN_DEVELOPMENT) 
					+ " CONSTRING "
					+ " FROM CONNECTION "
					+ " WHERE CONNECTION_NAME = ? "
					+ " AND DEFUNCT_IND = 'N'";
			pstmt = wsConnection.prepareStatement(query);
			pstmt.setString(1, value);
			rsWS = pstmt.executeQuery();
			if (rsWS.next()){
				result = rsWS.getString("CONSTRING");
			}			
			pstmt.close();
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			if (wsConnection != null) try { wsConnection.close(); } catch (Exception e){}
			if (pstmt != null) try {pstmt.close();}catch (Exception e) {}
			if (rsWS != null) try {rsWS.close();} catch (Exception e){}
		}
		return result;
	}

	private static Connection getWebServicePooledConnection() {
		InitConnection newIc = new InitConnection();
		Connection wsConnection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			wsConnection = DriverManager.getConnection(
					newIc.getRequest("url"));

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return wsConnection;
	}
}