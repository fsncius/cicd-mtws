package com.rsmurniteguh.webservice.dep.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;

public class FileUploadUtil {
    private static final FileUploadUtil fileUploadUtil = new FileUploadUtil();
	
    private FileUploadUtil() {}
	
    public static FileUploadUtil getInstance() {
        return fileUploadUtil;
    }
    
    public byte[] getByteFromFtpFile(String fullPath) throws Exception {
        URL url;
 
        try {
            url = new URL(fullPath);

            URLConnection con = url.openConnection();
//            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            BufferedInputStream input = new BufferedInputStream(con.getInputStream());
            return StreamUtil.getInstance().readBytes(input, 1024);
        } catch (Exception e){
        	throw new Exception(e.getMessage());
        }
    }
	
    public byte[] convertFile2Bytes(String fullFileName) throws IOException {
    	fullFileName = DataUtils.ifUnixDir(fullFileName);
    	File file = new File(fullFileName);

        if (file.exists()) {        	
            FileInputStream fis = new FileInputStream(file);

            return StreamUtil.getInstance().readBytes(fis, 1024);
        }
        return null;
    }
    
    public List convertFile2Bytes(String[] fullFileNames) throws IOException {
        List list = new ArrayList();

        if (fullFileNames != null) {
            for (int i = 0; i < fullFileNames.length; i++) {
            	
            	fullFileNames[i] = DataUtils.ifUnixDir(fullFileNames[i]);
            	
                String fullFileName = fullFileNames[i];
                byte[] bytes = convertFile2Bytes(fullFileName);

                list.add(fullFileName);
                list.add(bytes);
            }
        }
        return list;
    }
    
public byte[] getByteFromServer(String path){
    	
    	path = DataUtils.ifUnixDir(path);
    	
    	File file = new File(path);
    	byte[] imgByte = null;
    	FileInputStream fis = null;
		try {
			if (file.exists()){
				fis = new FileInputStream(file);
				imgByte = StreamUtil.getInstance().readBytes(fis, 1024);
			} 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) try { fis.close();	} catch (IOException e) { e.printStackTrace();	}
		}
		return imgByte;
    }

	public File getFileFromServer(String path){
		
		path = DataUtils.ifUnixDir(path);
		File ret = null;
		
		try {
			File file = new File(path);
			if (file.exists()){
				ret = file;
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

    public void copyFileUsingStream(File source, File dest) throws Exception {
        InputStream is = null;
        OutputStream os = null;
        try {
        	if (source.exists()){
        		File parent = dest.getParentFile();
               	if (!parent.exists() && !parent.mkdirs()) {
               	    throw new Exception("Couldn't create dir: " + parent);
               	}
               	if (dest.exists()){
               		throw new Exception("File is exists: " + dest.getPath());
               	}
        		
        		is = new FileInputStream(source);
                os = new FileOutputStream(dest);
                
                byte[] buffer = new byte[1024];
                int length;
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
                System.out.println("File copy at " + dest.getAbsolutePath());
        	} else {
        		throw new Exception("File doesn't exits: " + source.getAbsolutePath());
        	}
        } finally {
            is.close();
            os.close();
        }
    }
}