package com.rsmurniteguh.webservice.dep.kthis.view;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class CardManageView {	
	private BigDecimal /*Integ2BigDe*/ cardId; //卡ID
	private BigDecimal /*Integ2BigDe*/ personId; //
	private String cardNo;	//卡号
	private String cardType;	//卡类型
	private String cardTypeNote; //卡类型描述
	private String cardStatus;	//卡状态
	private String preCardStatus;//卡前一态
	private String cardStatusNote; //卡状态描述
	private String cardStatusDesc;  //卡状态中文描述
	private String cardStatusDescLang; //卡状态英文描述 
	private String remark; //备注 
	private BigDecimal /*Integ2BigDe*/ stakeHolderAccountTXNID;
	private String defunct;  //删除标记
	private BigDecimal /*Integ2BigDe*/ stakeholderId;//门诊交费
	private Timestamp lastUpdateTime; //最后更新时间
	
	private Timestamp effectiveDate;	//有效时间
	private Timestamp expiryDate;	//失效时间
	private String cardPassword;	//卡密码
	private String oldCardStatus;	//老状态
	private String oldCardStatusNote;	//老状态描述
	private String newCardStatus;	//新状态
	private String newCardStatusNote;	//新状态描述
	private Timestamp historyEffectiveDate;	//历史有效时间
	private Timestamp historyExpiryDate;	//历史失效时间
	private Timestamp updateDate;	//更新时间
	private String updateBy;	//更新人
	private String updateByName;
	private Timestamp cardHistoryPrevUpdateTime;//卡状态产生时间
	private Timestamp txnHoldDate; //交易产生时间
	
	private BigDecimal /*Integ2BigDe*/ txnCodeMstrId;
	private String txnCode;
	private String txnDesc;
	private String txmDescLang1;
	private String txnNote;
	
	private BigDecimal /*Integ2BigDe*/ txnAcount;//交易费用
	private BigDecimal /*Integ2BigDe*/ txnHistoryAcount;
	
	private BigDecimal cardValue; 	//卡费
	private String parameterName;
	private String parameterValue;
	private String cardDepositeIncome; //收取押金
	private String cardDepositeOut;  //退还押金 
	private boolean isMedicalCard;	//标识是否是医保卡。医保卡对应的是MedicalRecordNo。
	
	

	public Timestamp getHistoryEffectiveDate() {
		return historyEffectiveDate;
	}
	public void setHistoryEffectiveDate(Timestamp historyEffectiveDate) {
		this.historyEffectiveDate = historyEffectiveDate;
	}
	public Timestamp getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Timestamp lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public Timestamp getHistoryExpiryDate() {
		return historyExpiryDate;
	}
	public void setHistoryExpiryDate(Timestamp historyExpiryDate) {
		this.historyExpiryDate = historyExpiryDate;
	}
	public String getParameterValue() {
		return parameterValue;
	}
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}
	public BigDecimal getCardValue() {
		return cardValue;
	}
	public void setCardValue(BigDecimal cardValue) {
		this.cardValue = cardValue;
	}
	public String getParameterName() {
		return parameterName;
	}
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getCardPassword() {
		return cardPassword;
	}
	public void setCardPassword(String cardPassword) {
		this.cardPassword = cardPassword;
	}
	public String getCardStatus() {
		return cardStatus;
	}
	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public Timestamp getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Timestamp effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public Timestamp getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Timestamp expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getNewCardStatus() {
		return newCardStatus;
	}
	public void setNewCardStatus(String newCardStatus) {
		this.newCardStatus = newCardStatus;
	}
	public String getOldCardStatus() {
		return oldCardStatus;
	}
	public void setOldCardStatus(String oldCardStatus) {
		this.oldCardStatus = oldCardStatus;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public BigDecimal /*Integ2BigDe*/ getCardId() {
		return cardId;
	}
	public void setCardId(BigDecimal /*Integ2BigDe*/ cardId) {
		this.cardId = cardId;
	}
	public String getTxmDescLang1() {
		return txmDescLang1;
	}
	public void setTxmDescLang1(String txmDescLang1) {
		this.txmDescLang1 = txmDescLang1;
	}
	public BigDecimal /*Integ2BigDe*/ getTxnCodeMstrId() {
		return txnCodeMstrId;
	}
	public void setTxnCodeMstrId(BigDecimal /*Integ2BigDe*/ txnCodeMstrId) {
		this.txnCodeMstrId = txnCodeMstrId;
	}
	public String getTxnDesc() {
		return txnDesc;
	}
	public void setTxnDesc(String txnDesc) {
		this.txnDesc = txnDesc;
	}
	public String getTxnNote() {
		return txnNote;
	}
	public void setTxnNote(String txnNote) {
		this.txnNote = txnNote;
	}
	public BigDecimal /*Integ2BigDe*/ getPersonId() {
		return personId;
	}
	public void setPersonId(BigDecimal /*Integ2BigDe*/ personId) {
		this.personId = personId;
	}
	public String getTxnCode() {
		return txnCode;
	}
	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public String getCardDepositeIncome() {
		return cardDepositeIncome;
	}
	public void setCardDepositeIncome(String cardDepositeIncome) {
		this.cardDepositeIncome = cardDepositeIncome;
	}
	public String getCardDepositeOut() {
		return cardDepositeOut;
	}
	public void setCardDepositeOut(String cardDepositeOut) {
		this.cardDepositeOut = cardDepositeOut;
	}
	public String getCardStatusDesc() {
		return cardStatusDesc;
	}
	public void setCardStatusDesc(String cardStatusDesc) {
		this.cardStatusDesc = cardStatusDesc;
	}
	public String getCardStatusDescLang() {
		return cardStatusDescLang;
	}
	public void setCardStatusDescLang(String cardStatusDescLang) {
		this.cardStatusDescLang = cardStatusDescLang;
	}
	public String getCardStatusNote() {
		return cardStatusNote;
	}
	public void setCardStatusNote(String cardStatusNote) {
		this.cardStatusNote = cardStatusNote;
	}
	public String getCardTypeNote() {
		return cardTypeNote;
	}
	public void setCardTypeNote(String cardTypeNote) {
		this.cardTypeNote = cardTypeNote;
	}
	public String getNewCardStatusNote() {
		return newCardStatusNote;
	}
	public void setNewCardStatusNote(String newCardStatusNote) {
		this.newCardStatusNote = newCardStatusNote;
	}
	public String getOldCardStatusNote() {
		return oldCardStatusNote;
	}
	public void setOldCardStatusNote(String oldCardStatusNote) {
		this.oldCardStatusNote = oldCardStatusNote;
	}
	public String getDefunct() {
		return defunct;
	}
	public void setDefunct(String defunct) {
		this.defunct = defunct;
	}
	public String getPreCardStatus() {
		return preCardStatus;
	}
	public void setPreCardStatus(String preCardStatus) {
		this.preCardStatus = preCardStatus;
	}
	public BigDecimal /*Integ2BigDe*/ getStakeHolderAccountTXNID() {
		return stakeHolderAccountTXNID;
	}
	public void setStakeHolderAccountTXNID(BigDecimal /*Integ2BigDe*/ stakeHolderAccountTXNID) {
		this.stakeHolderAccountTXNID = stakeHolderAccountTXNID;
	}
	public BigDecimal /*Integ2BigDe*/ getStakeholderId() {
		return stakeholderId;
	}
	public void setStakeholderId(BigDecimal /*Integ2BigDe*/ stakeholderId) {
		this.stakeholderId = stakeholderId;
	}
	public BigDecimal /*Integ2BigDe*/ getTxnAcount() {
		return txnAcount;
	}
	public void setTxnAcount(BigDecimal /*Integ2BigDe*/ txnAcount) {
		this.txnAcount = txnAcount;
	}
	public String getUpdateByName() {
		return updateByName;
	}
	public void setUpdateByName(String updateByName) {
		this.updateByName = updateByName;
	}
	public Timestamp getTxnHoldDate() {
		return txnHoldDate;
	}
	public void setTxnHoldDate(Timestamp txnHoldDate) {
		this.txnHoldDate = txnHoldDate;
	}
	public Timestamp getCardHistoryPrevUpdateTime() {
		return cardHistoryPrevUpdateTime;
	}
	public void setCardHistoryPrevUpdateTime(Timestamp cardHistoryPrevUpdateTime) {
		this.cardHistoryPrevUpdateTime = cardHistoryPrevUpdateTime;
	}
	public BigDecimal /*Integ2BigDe*/ getTxnHistoryAcount() {
		return txnHistoryAcount;
	}
	public void setTxnHistoryAcount(BigDecimal /*Integ2BigDe*/ txnHistoryAcount) {
		this.txnHistoryAcount = txnHistoryAcount;
	}

	public boolean getIsMedicalCard() {
		return isMedicalCard;
	}
	public void setIsMedicalCard(boolean isMedicalCard) {
		this.isMedicalCard = isMedicalCard;
	}
	
}