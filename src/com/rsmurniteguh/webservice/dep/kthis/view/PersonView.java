package com.rsmurniteguh.webservice.dep.kthis.view;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.rsmurniteguh.webservice.dep.kthis.model.Company;
import com.rsmurniteguh.webservice.dep.kthis.model.CustomerClassHistory;
import com.rsmurniteguh.webservice.dep.kthis.model.Patientaccount;
import com.rsmurniteguh.webservice.dep.kthis.model.Person;
import com.rsmurniteguh.webservice.dep.kthis.model.StakeHolder;
import com.rsmurniteguh.webservice.dep.kthis.model.Patient;

public class PersonView {
	private Person person = new Person();
	private Patient patient = new Patient();
	private Company company = new Company();
	private StakeHolder stakeHolder;
	private String occupationGroupDesc;
	private String idTypeDesc;
	private String sexDesc;
	private String nationalityDesc;
	private String maritalStatusDesc;
	private String medicalRecordNo;
	
	//following statedesc and citydesc added by zy
	//注意ADDRESS#ADDRESS_TYPE
	private BigDecimal/*Integ2BigDe*/ birthAddressId;
	private String birthState;//出生地（省/市）,ADDRESS#STATE
	private String birthStateNote;
	private String birthStateDesc;//出生地（省/市）,ADDRESS#STATE
	private String birthCity;//市/区,ADDRESS#City
	private String birthCityNote;
	private String birthCityDesc;//市/区,ADDRESS#City
	private String birthAddress1;//详细地点,ADDRESS#ADDRESS_1
	
	private BigDecimal/*Integ2BigDe*/ officerAddressId;
	private String officerAddress;//工作单位及地址,ADDRESS#ADDRESS_1
	private String officerTelephone;//公司电话,ADDRESS#PHONE_NO
	private String officerPostalCode;//邮政编码,ADDRESS#POSTAL_CODE
	
	private BigDecimal/*Integ2BigDe*/ rprAddressId;
	private String rprState;//户口地址,ADDRESS#STATE
	private String rprStateDesc;//户口地址,ADDRESS#STATE
	private String rprCity;//市/区,ADDRESS#City
	private String rprCityDesc;//市/区,ADDRESS#City
	private String rprDistrictCode;//街道,ADDRESS#POSTAL_CODE
	private String rprDistrictDesc;//街道,ADDRESS#POSTAL_CODE
	private String rprPostalCode;//邮政编码,ADDRESS#POSTAL_CODE
	private String rprAddress1;//详细地点,ADDRESS#ADDRESS_1
	
	
	private BigDecimal/*Integ2BigDe*/ currentAddressId;
	private String currentState;//现地址,ADDRESS#STATE
	private String currentStateDesc;//现地址,ADDRESS#STATE
	private String currentCity;//市/区,ADDRESS#City
	private String currentCityDesc;//市/区,ADDRESS#City
	private String currentDistrictCode;//街道,ADDRESS#POSTAL_CODE
	private String currentDistrictDesc;//街道,ADDRESS#POSTAL_CODE
	private String currentPostalCode;//邮政编码,ADDRESS#POSTAL_CODE
	private String currentAddress1;//详细地点,ADDRESS#ADDRESS_1
	
	
	private String contactPersonName;//联系人姓名,new Person
	
	private BigDecimal/*Integ2BigDe*/ contactAddressId;
	private String contactAddress;//地址,ADDRESS#ADDRESS_1,地址类型为户口
	private String contactEmail;//联系人邮件
	private String contactShortCode;//联系人的拼音码
	private String contactTelephone;//电话,ADDRESS#PHONE_NO
    
	private BigDecimal/*Integ2BigDe*/ nokId;
	private String relationShipCode;//与病人关系,CAT=RRL ,NOK#RELATIONSHIP
	//added by zy for patient exsit checking
	private String mrnNo; //门诊/住院病案号
	private Timestamp firstVisitDate;
	private String checkPatientExistLogic; //计算病人是否存在逻辑
	private String mrnType;
	private BigDecimal maxAdmissionTimes;// 最大入院时间
	
	// added by Chen Benjun 20090706
	private String mioNo;			//医保手册号
	private String micNo;			//医保卡号
	private String mrtPeNo;		//体检档案号
	private String emplyeeNo;	//工号
	private Patientaccount patientAccount; 
	//会员身份变更历史表
	private CustomerClassHistory customerClassHistory;
	
	private String companyName;
	private BigDecimal companyId;
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public String getOccupationGroupDesc() {
		return occupationGroupDesc;
	}
	public void setOccupationGroupDesc(String occupationGroupDesc) {
		this.occupationGroupDesc = occupationGroupDesc;
	}
	public String getIdTypeDesc() {
		return idTypeDesc;
	}
	public void setIdTypeDesc(String idTypeDesc) {
		this.idTypeDesc = idTypeDesc;
	}
	public String getSexDesc() {
		return sexDesc;
	}
	public void setSexDesc(String sexDesc) {
		this.sexDesc = sexDesc;
	}
	public String getNationalityDesc() {
		return nationalityDesc;
	}
	public void setNationalityDesc(String nationalityDesc) {
		this.nationalityDesc = nationalityDesc;
	}
	public String getMaritalStatusDesc() {
		return maritalStatusDesc;
	}
	public void setMaritalStatusDesc(String maritalStatusDesc) {
		this.maritalStatusDesc = maritalStatusDesc;
	}
	public String getMedicalRecordNo() {
		return medicalRecordNo;
	}
	public void setMedicalRecordNo(String medicalRecordNo) {
		this.medicalRecordNo = medicalRecordNo;
	}
	public BigDecimal getBirthAddressId() {
		return birthAddressId;
	}
	public void setBirthAddressId(BigDecimal birthAddressId) {
		this.birthAddressId = birthAddressId;
	}
	public String getBirthState() {
		return birthState;
	}
	public void setBirthState(String birthState) {
		this.birthState = birthState;
	}
	public String getBirthStateNote() {
		return birthStateNote;
	}
	public void setBirthStateNote(String birthStateNote) {
		this.birthStateNote = birthStateNote;
	}
	public String getBirthStateDesc() {
		return birthStateDesc;
	}
	public void setBirthStateDesc(String birthStateDesc) {
		this.birthStateDesc = birthStateDesc;
	}
	public String getBirthCity() {
		return birthCity;
	}
	public void setBirthCity(String birthCity) {
		this.birthCity = birthCity;
	}
	public String getBirthCityNote() {
		return birthCityNote;
	}
	public void setBirthCityNote(String birthCityNote) {
		this.birthCityNote = birthCityNote;
	}
	public String getBirthCityDesc() {
		return birthCityDesc;
	}
	public void setBirthCityDesc(String birthCityDesc) {
		this.birthCityDesc = birthCityDesc;
	}
	public String getBirthAddress1() {
		return birthAddress1;
	}
	public void setBirthAddress1(String birthAddress1) {
		this.birthAddress1 = birthAddress1;
	}
	public BigDecimal getOfficerAddressId() {
		return officerAddressId;
	}
	public void setOfficerAddressId(BigDecimal officerAddressId) {
		this.officerAddressId = officerAddressId;
	}
	public String getOfficerAddress() {
		return officerAddress;
	}
	public void setOfficerAddress(String officerAddress) {
		this.officerAddress = officerAddress;
	}
	public String getOfficerTelephone() {
		return officerTelephone;
	}
	public void setOfficerTelephone(String officerTelephone) {
		this.officerTelephone = officerTelephone;
	}
	public String getOfficerPostalCode() {
		return officerPostalCode;
	}
	public void setOfficerPostalCode(String officerPostalCode) {
		this.officerPostalCode = officerPostalCode;
	}
	public BigDecimal getRprAddressId() {
		return rprAddressId;
	}
	public void setRprAddressId(BigDecimal rprAddressId) {
		this.rprAddressId = rprAddressId;
	}
	public String getRprState() {
		return rprState;
	}
	public void setRprState(String rprState) {
		this.rprState = rprState;
	}
	public String getRprStateDesc() {
		return rprStateDesc;
	}
	public void setRprStateDesc(String rprStateDesc) {
		this.rprStateDesc = rprStateDesc;
	}
	public String getRprCity() {
		return rprCity;
	}
	public void setRprCity(String rprCity) {
		this.rprCity = rprCity;
	}
	public String getRprCityDesc() {
		return rprCityDesc;
	}
	public void setRprCityDesc(String rprCityDesc) {
		this.rprCityDesc = rprCityDesc;
	}
	public String getRprDistrictCode() {
		return rprDistrictCode;
	}
	public void setRprDistrictCode(String rprDistrictCode) {
		this.rprDistrictCode = rprDistrictCode;
	}
	public String getRprDistrictDesc() {
		return rprDistrictDesc;
	}
	public void setRprDistrictDesc(String rprDistrictDesc) {
		this.rprDistrictDesc = rprDistrictDesc;
	}
	public String getRprPostalCode() {
		return rprPostalCode;
	}
	public void setRprPostalCode(String rprPostalCode) {
		this.rprPostalCode = rprPostalCode;
	}
	public String getRprAddress1() {
		return rprAddress1;
	}
	public void setRprAddress1(String rprAddress1) {
		this.rprAddress1 = rprAddress1;
	}
	public BigDecimal getCurrentAddressId() {
		return currentAddressId;
	}
	public void setCurrentAddressId(BigDecimal currentAddressId) {
		this.currentAddressId = currentAddressId;
	}
	public String getCurrentState() {
		return currentState;
	}
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}
	public String getCurrentStateDesc() {
		return currentStateDesc;
	}
	public void setCurrentStateDesc(String currentStateDesc) {
		this.currentStateDesc = currentStateDesc;
	}
	public String getCurrentCity() {
		return currentCity;
	}
	public void setCurrentCity(String currentCity) {
		this.currentCity = currentCity;
	}
	public String getCurrentCityDesc() {
		return currentCityDesc;
	}
	public void setCurrentCityDesc(String currentCityDesc) {
		this.currentCityDesc = currentCityDesc;
	}
	public String getCurrentDistrictCode() {
		return currentDistrictCode;
	}
	public void setCurrentDistrictCode(String currentDistrictCode) {
		this.currentDistrictCode = currentDistrictCode;
	}
	public String getCurrentDistrictDesc() {
		return currentDistrictDesc;
	}
	public void setCurrentDistrictDesc(String currentDistrictDesc) {
		this.currentDistrictDesc = currentDistrictDesc;
	}
	public String getCurrentPostalCode() {
		return currentPostalCode;
	}
	public void setCurrentPostalCode(String currentPostalCode) {
		this.currentPostalCode = currentPostalCode;
	}
	public String getCurrentAddress1() {
		return currentAddress1;
	}
	public void setCurrentAddress1(String currentAddress1) {
		this.currentAddress1 = currentAddress1;
	}
	public String getContactPersonName() {
		return contactPersonName;
	}
	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}
	public BigDecimal getContactAddressId() {
		return contactAddressId;
	}
	public void setContactAddressId(BigDecimal contactAddressId) {
		this.contactAddressId = contactAddressId;
	}
	public String getContactAddress() {
		return contactAddress;
	}
	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getContactShortCode() {
		return contactShortCode;
	}
	public void setContactShortCode(String contactShortCode) {
		this.contactShortCode = contactShortCode;
	}
	public String getContactTelephone() {
		return contactTelephone;
	}
	public void setContactTelephone(String contactTelephone) {
		this.contactTelephone = contactTelephone;
	}
	public BigDecimal getNokId() {
		return nokId;
	}
	public void setNokId(BigDecimal nokId) {
		this.nokId = nokId;
	}
	public String getRelationShipCode() {
		return relationShipCode;
	}
	public void setRelationShipCode(String relationShipCode) {
		this.relationShipCode = relationShipCode;
	}
	public String getMrnNo() {
		return mrnNo;
	}
	public void setMrnNo(String mrnNo) {
		this.mrnNo = mrnNo;
	}
	public Timestamp getFirstVisitDate() {
		return firstVisitDate;
	}
	public void setFirstVisitDate(Timestamp firstVisitDate) {
		this.firstVisitDate = firstVisitDate;
	}
	public String getCheckPatientExistLogic() {
		return checkPatientExistLogic;
	}
	public void setCheckPatientExistLogic(String checkPatientExistLogic) {
		this.checkPatientExistLogic = checkPatientExistLogic;
	}
	public String getMrnType() {
		return mrnType;
	}
	public void setMrnType(String mrnType) {
		this.mrnType = mrnType;
	}
	public BigDecimal getMaxAdmissionTimes() {
		return maxAdmissionTimes;
	}
	public void setMaxAdmissionTimes(BigDecimal maxAdmissionTimes) {
		this.maxAdmissionTimes = maxAdmissionTimes;
	}
	public String getMioNo() {
		return mioNo;
	}
	public void setMioNo(String mioNo) {
		this.mioNo = mioNo;
	}
	public String getMicNo() {
		return micNo;
	}
	public void setMicNo(String micNo) {
		this.micNo = micNo;
	}
	public String getMrtPeNo() {
		return mrtPeNo;
	}
	public void setMrtPeNo(String mrtPeNo) {
		this.mrtPeNo = mrtPeNo;
	}
	public String getEmplyeeNo() {
		return emplyeeNo;
	}
	public void setEmplyeeNo(String emplyeeNo) {
		this.emplyeeNo = emplyeeNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public BigDecimal getCompanyId() {
		return companyId;
	}
	public void setCompanyId(BigDecimal companyId) {
		this.companyId = companyId;
	}
	public StakeHolder getStakeHolder() {
		return stakeHolder;
	}
	public void setStakeHolder(StakeHolder stakeHolder) {
		this.stakeHolder = stakeHolder;
	}
	public Patientaccount getPatientAccount() {
		return patientAccount;
	}
	public void setPatientAccount(Patientaccount patientAccount) {
		this.patientAccount = patientAccount;
	}
	public CustomerClassHistory getCustomerClassHistory() {
		return customerClassHistory;
	}
	public void setCustomerClassHistory(CustomerClassHistory customerClassHistory) {
		this.customerClassHistory = customerClassHistory;
	}
}
