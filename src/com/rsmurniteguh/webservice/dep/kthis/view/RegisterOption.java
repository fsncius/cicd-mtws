package com.rsmurniteguh.webservice.dep.kthis.view;

import java.util.List;

import com.rsmurniteguh.webservice.dep.kthis.model.DropDownBo;

public class RegisterOption {
	private List<DropDownBo> idType;
	private List<DropDownBo> religion;
	private List<DropDownBo> ethnic;
	private List<DropDownBo> education;
	private List<DropDownBo> province;
	public List<DropDownBo> getReligion() {
		return religion;
	}
	public void setReligion(List<DropDownBo> religion) {
		this.religion = religion;
	}
	public List<DropDownBo> getEthnic() {
		return ethnic;
	}
	public void setEthnic(List<DropDownBo> ethnic) {
		this.ethnic = ethnic;
	}
	public List<DropDownBo> getEducation() {
		return education;
	}
	public void setEducation(List<DropDownBo> education) {
		this.education = education;
	}
	public List<DropDownBo> getProvince() {
		return province;
	}
	public void setProvince(List<DropDownBo> province) {
		this.province = province;
	}
	public List<DropDownBo> getIdType() {
		return idType;
	}
	public void setIdType(List<DropDownBo> idType) {
		this.idType = idType;
	}
}
