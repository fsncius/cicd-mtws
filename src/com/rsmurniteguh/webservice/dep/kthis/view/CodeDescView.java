package com.rsmurniteguh.webservice.dep.kthis.view;

import java.math.BigDecimal;

public class CodeDescView {

	private BigDecimal id;
	private String code;
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}	
	
}
