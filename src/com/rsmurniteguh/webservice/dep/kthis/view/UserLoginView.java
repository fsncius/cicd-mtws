package com.rsmurniteguh.webservice.dep.kthis.view;

import java.util.Date;
import java.util.List;


public class UserLoginView {

	private long usermstrId;
	private String userCode;

	private String userName;
	private Date passwordExpiryDate;
	
	private List<CodeDescView> locations;
	private List<CodeDescView> roles;

	
	
	
	
	public long getUsermstrId() {
		return usermstrId;
	}
	public void setUsermstrId(long usermstrId) {
		this.usermstrId = usermstrId;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getPasswordExpiryDate() {
		return passwordExpiryDate;
	}
	public void setPasswordExpiryDate(Date passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}
	public List<CodeDescView> getLocations() {
		return locations;
	}
	public void setLocations(List<CodeDescView> locations) {
		this.locations = locations;
	}
	public List<CodeDescView> getRoles() {
		return roles;
	}
	public void setRoles(List<CodeDescView> roles) {
		this.roles = roles;
	}
	
	
	
}
