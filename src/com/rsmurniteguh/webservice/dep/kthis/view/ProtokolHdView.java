package com.rsmurniteguh.webservice.dep.kthis.view;

import java.sql.Timestamp;
import java.util.List;

import com.rsmurniteguh.webservice.dep.kthis.model.ProtokolHD;
import com.rsmurniteguh.webservice.dep.kthis.model.ProtokolHdPart;

public class ProtokolHdView {
	private ProtokolHD protokolHd;

	private List<ProtokolHdPartView> protokolHdPartView;
	
	public ProtokolHdView() {
		protokolHd = new ProtokolHD();
	}
	
	public ProtokolHD getProtokolHd() {
		return protokolHd;
	}
	
	public void setProtokolHd(ProtokolHD protokolHd) {
		this.protokolHd = protokolHd;
	}

	public List<ProtokolHdPartView> getProtokolHdPartView() {
		return protokolHdPartView;
	}

	public void setProtokolHdPartView(List<ProtokolHdPartView> protokolHdPartView) {
		this.protokolHdPartView = protokolHdPartView;
	}
	
}
