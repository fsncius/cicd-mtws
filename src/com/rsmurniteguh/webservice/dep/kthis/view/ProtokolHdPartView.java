package com.rsmurniteguh.webservice.dep.kthis.view;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.rsmurniteguh.webservice.dep.kthis.model.ProtokolHD;
import com.rsmurniteguh.webservice.dep.kthis.model.ProtokolHdPart;

public class ProtokolHdPartView {
	private ProtokolHdPart protokolHdPart;
	private BigDecimal nokartu;
	
	public ProtokolHdPartView() {
		protokolHdPart = new ProtokolHdPart();
	}
	public ProtokolHdPart getProtokolHdPart() {
		return protokolHdPart;
	}

	public void setProtokolHdPart(ProtokolHdPart protokolHdPart) {
		this.protokolHdPart = protokolHdPart;
	}
	public BigDecimal getNokartu() {
		return nokartu;
	}
	public void setNokartu(BigDecimal nokartu) {
		this.nokartu = nokartu;
	}
	
	
}
