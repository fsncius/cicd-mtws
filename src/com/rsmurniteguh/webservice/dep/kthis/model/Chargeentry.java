package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CHARGEENTRY database table.
 * 
 */
@Entity
@NamedQuery(name="Chargeentry.findAll", query="SELECT c FROM Chargeentry c")
public class Chargeentry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGEENTRY_ID")
	private long chargeentryId;

	@Column(name="ACTUAL_UNIT_PRICE")
	private BigDecimal actualUnitPrice;

	@Column(name="BASE_UNIT_PRICE")
	private BigDecimal baseUnitPrice;

	@Column(name="CHARGE_REVENUECENTREMSTR_ID")
	private BigDecimal chargeRevenuecentremstrId;

	@Column(name="CHARGING_SOURCE")
	private String chargingSource;

	@Column(name="CLAIM_AMOUNT")
	private BigDecimal claimAmount;

	@Column(name="CLAIM_CLASS")
	private String claimClass;

	@Column(name="CLAIM_OVERWRITTEN_IND")
	private String claimOverwrittenInd;

	@Column(name="CLAIM_PERCENT")
	private BigDecimal claimPercent;

	@Column(name="COST_PRICE")
	private BigDecimal costPrice;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DEPOSIT_TXN_AMOUNT")
	private BigDecimal depositTxnAmount;

	@Column(name="DIAGNOSISMSTR_ID")
	private BigDecimal diagnosismstrId;

	@Column(name="DISCOUNT_AMOUNT")
	private BigDecimal discountAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DATETIME")
	private Date endDatetime;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATETIME")
	private Date enteredDatetime;

	@Column(name="ENTERED_LOCATIONMSTR_ID")
	private BigDecimal enteredLocationmstrId;

	@Column(name="FORM_NO")
	private String formNo;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MIO_INDICATION")
	private String mioIndication;

	@Column(name="MIO_REMARKS")
	private String mioRemarks;

	@Column(name="ORDERED_CAREPROVIDER_ID")
	private BigDecimal orderedCareproviderId;

	@Column(name="ORDERENTRY_ID")
	private BigDecimal orderentryId;

	@Column(name="ORDERENTRYITEM_ID")
	private BigDecimal orderentryitemId;

	@Column(name="PERFORMED_CAREPROVIDER_ID")
	private BigDecimal performedCareproviderId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PRICE_REMARKS")
	private String priceRemarks;

	private BigDecimal qty;

	@Column(name="QTY_UOM")
	private String qtyUom;

	@Column(name="REFERENCE_NO")
	private String referenceNo;

	private String remarks;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DATETIME")
	private Date startDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="TXN_DATETIME")
	private Date txnDatetime;

	@Column(name="TXN_DESC")
	private String txnDesc;

	@Column(name="TXNCODEMSTR_ID")
	private BigDecimal txncodemstrId;

	@Column(name="VISIT_SUBSPECIALTYMSTR_ID")
	private BigDecimal visitSubspecialtymstrId;

	@Column(name="VISIT_WARDMSTR_ID")
	private BigDecimal visitWardmstrId;

	//bi-directional many-to-one association to Patientaccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNT_ID")
	private Patientaccount patientaccount;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Chargeentrydetail
	@OneToMany(mappedBy="chargeentry")
	private List<Chargeentrydetail> chargeentrydetails;

	public Chargeentry() {
	}

	public long getChargeentryId() {
		return this.chargeentryId;
	}

	public void setChargeentryId(long chargeentryId) {
		this.chargeentryId = chargeentryId;
	}

	public BigDecimal getActualUnitPrice() {
		return this.actualUnitPrice;
	}

	public void setActualUnitPrice(BigDecimal actualUnitPrice) {
		this.actualUnitPrice = actualUnitPrice;
	}

	public BigDecimal getBaseUnitPrice() {
		return this.baseUnitPrice;
	}

	public void setBaseUnitPrice(BigDecimal baseUnitPrice) {
		this.baseUnitPrice = baseUnitPrice;
	}

	public BigDecimal getChargeRevenuecentremstrId() {
		return this.chargeRevenuecentremstrId;
	}

	public void setChargeRevenuecentremstrId(BigDecimal chargeRevenuecentremstrId) {
		this.chargeRevenuecentremstrId = chargeRevenuecentremstrId;
	}

	public String getChargingSource() {
		return this.chargingSource;
	}

	public void setChargingSource(String chargingSource) {
		this.chargingSource = chargingSource;
	}

	public BigDecimal getClaimAmount() {
		return this.claimAmount;
	}

	public void setClaimAmount(BigDecimal claimAmount) {
		this.claimAmount = claimAmount;
	}

	public String getClaimClass() {
		return this.claimClass;
	}

	public void setClaimClass(String claimClass) {
		this.claimClass = claimClass;
	}

	public String getClaimOverwrittenInd() {
		return this.claimOverwrittenInd;
	}

	public void setClaimOverwrittenInd(String claimOverwrittenInd) {
		this.claimOverwrittenInd = claimOverwrittenInd;
	}

	public BigDecimal getClaimPercent() {
		return this.claimPercent;
	}

	public void setClaimPercent(BigDecimal claimPercent) {
		this.claimPercent = claimPercent;
	}

	public BigDecimal getCostPrice() {
		return this.costPrice;
	}

	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getDepositTxnAmount() {
		return this.depositTxnAmount;
	}

	public void setDepositTxnAmount(BigDecimal depositTxnAmount) {
		this.depositTxnAmount = depositTxnAmount;
	}

	public BigDecimal getDiagnosismstrId() {
		return this.diagnosismstrId;
	}

	public void setDiagnosismstrId(BigDecimal diagnosismstrId) {
		this.diagnosismstrId = diagnosismstrId;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Date getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredDatetime() {
		return this.enteredDatetime;
	}

	public void setEnteredDatetime(Date enteredDatetime) {
		this.enteredDatetime = enteredDatetime;
	}

	public BigDecimal getEnteredLocationmstrId() {
		return this.enteredLocationmstrId;
	}

	public void setEnteredLocationmstrId(BigDecimal enteredLocationmstrId) {
		this.enteredLocationmstrId = enteredLocationmstrId;
	}

	public String getFormNo() {
		return this.formNo;
	}

	public void setFormNo(String formNo) {
		this.formNo = formNo;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMioIndication() {
		return this.mioIndication;
	}

	public void setMioIndication(String mioIndication) {
		this.mioIndication = mioIndication;
	}

	public String getMioRemarks() {
		return this.mioRemarks;
	}

	public void setMioRemarks(String mioRemarks) {
		this.mioRemarks = mioRemarks;
	}

	public BigDecimal getOrderedCareproviderId() {
		return this.orderedCareproviderId;
	}

	public void setOrderedCareproviderId(BigDecimal orderedCareproviderId) {
		this.orderedCareproviderId = orderedCareproviderId;
	}

	public BigDecimal getOrderentryId() {
		return this.orderentryId;
	}

	public void setOrderentryId(BigDecimal orderentryId) {
		this.orderentryId = orderentryId;
	}

	public BigDecimal getOrderentryitemId() {
		return this.orderentryitemId;
	}

	public void setOrderentryitemId(BigDecimal orderentryitemId) {
		this.orderentryitemId = orderentryitemId;
	}

	public BigDecimal getPerformedCareproviderId() {
		return this.performedCareproviderId;
	}

	public void setPerformedCareproviderId(BigDecimal performedCareproviderId) {
		this.performedCareproviderId = performedCareproviderId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPriceRemarks() {
		return this.priceRemarks;
	}

	public void setPriceRemarks(String priceRemarks) {
		this.priceRemarks = priceRemarks;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getQtyUom() {
		return this.qtyUom;
	}

	public void setQtyUom(String qtyUom) {
		this.qtyUom = qtyUom;
	}

	public String getReferenceNo() {
		return this.referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getStartDatetime() {
		return this.startDatetime;
	}

	public void setStartDatetime(Date startDatetime) {
		this.startDatetime = startDatetime;
	}

	public Date getTxnDatetime() {
		return this.txnDatetime;
	}

	public void setTxnDatetime(Date txnDatetime) {
		this.txnDatetime = txnDatetime;
	}

	public String getTxnDesc() {
		return this.txnDesc;
	}

	public void setTxnDesc(String txnDesc) {
		this.txnDesc = txnDesc;
	}

	public BigDecimal getTxncodemstrId() {
		return this.txncodemstrId;
	}

	public void setTxncodemstrId(BigDecimal txncodemstrId) {
		this.txncodemstrId = txncodemstrId;
	}

	public BigDecimal getVisitSubspecialtymstrId() {
		return this.visitSubspecialtymstrId;
	}

	public void setVisitSubspecialtymstrId(BigDecimal visitSubspecialtymstrId) {
		this.visitSubspecialtymstrId = visitSubspecialtymstrId;
	}

	public BigDecimal getVisitWardmstrId() {
		return this.visitWardmstrId;
	}

	public void setVisitWardmstrId(BigDecimal visitWardmstrId) {
		this.visitWardmstrId = visitWardmstrId;
	}

	public Patientaccount getPatientaccount() {
		return this.patientaccount;
	}

	public void setPatientaccount(Patientaccount patientaccount) {
		this.patientaccount = patientaccount;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public List<Chargeentrydetail> getChargeentrydetails() {
		return this.chargeentrydetails;
	}

	public void setChargeentrydetails(List<Chargeentrydetail> chargeentrydetails) {
		this.chargeentrydetails = chargeentrydetails;
	}

	public Chargeentrydetail addChargeentrydetail(Chargeentrydetail chargeentrydetail) {
		getChargeentrydetails().add(chargeentrydetail);
		chargeentrydetail.setChargeentry(this);

		return chargeentrydetail;
	}

	public Chargeentrydetail removeChargeentrydetail(Chargeentrydetail chargeentrydetail) {
		getChargeentrydetails().remove(chargeentrydetail);
		chargeentrydetail.setChargeentry(null);

		return chargeentrydetail;
	}

}