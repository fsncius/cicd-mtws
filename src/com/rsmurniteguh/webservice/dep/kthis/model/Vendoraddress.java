package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the VENDORADDRESS database table.
 * 
 */
@Entity
@NamedQuery(name="Vendoraddress.findAll", query="SELECT v FROM Vendoraddress v")
public class Vendoraddress implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VENDORADDRESS_ID")
	private long vendoraddressId;

	@Column(name="ADDRESS_1")
	private String address1;

	@Column(name="ADDRESS_2")
	private String address2;

	@Column(name="ADDRESS_3")
	private String address3;

	@Column(name="ADDRESS_TYPE")
	private String addressType;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="POSTAL_CODE")
	private String postalCode;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="VENDORMSTR_ID")
	private BigDecimal vendormstrId;

	//bi-directional many-to-one association to Citymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CITYMSTR_ID")
	private Citymstr citymstr;

	//bi-directional many-to-one association to Countrymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COUNTRYMSTR_ID")
	private Countrymstr countrymstr;

	//bi-directional many-to-one association to Statemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATEMSTR_ID")
	private Statemstr statemstr;

	//bi-directional many-to-one association to Vendoraddresscontact
	@OneToMany(mappedBy="vendoraddress")
	private List<Vendoraddresscontact> vendoraddresscontacts;

	public Vendoraddress() {
	}

	public long getVendoraddressId() {
		return this.vendoraddressId;
	}

	public void setVendoraddressId(long vendoraddressId) {
		this.vendoraddressId = vendoraddressId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddressType() {
		return this.addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getVendormstrId() {
		return this.vendormstrId;
	}

	public void setVendormstrId(BigDecimal vendormstrId) {
		this.vendormstrId = vendormstrId;
	}

	public Citymstr getCitymstr() {
		return this.citymstr;
	}

	public void setCitymstr(Citymstr citymstr) {
		this.citymstr = citymstr;
	}

	public Countrymstr getCountrymstr() {
		return this.countrymstr;
	}

	public void setCountrymstr(Countrymstr countrymstr) {
		this.countrymstr = countrymstr;
	}

	public Statemstr getStatemstr() {
		return this.statemstr;
	}

	public void setStatemstr(Statemstr statemstr) {
		this.statemstr = statemstr;
	}

	public List<Vendoraddresscontact> getVendoraddresscontacts() {
		return this.vendoraddresscontacts;
	}

	public void setVendoraddresscontacts(List<Vendoraddresscontact> vendoraddresscontacts) {
		this.vendoraddresscontacts = vendoraddresscontacts;
	}

	public Vendoraddresscontact addVendoraddresscontact(Vendoraddresscontact vendoraddresscontact) {
		getVendoraddresscontacts().add(vendoraddresscontact);
		vendoraddresscontact.setVendoraddress(this);

		return vendoraddresscontact;
	}

	public Vendoraddresscontact removeVendoraddresscontact(Vendoraddresscontact vendoraddresscontact) {
		getVendoraddresscontacts().remove(vendoraddresscontact);
		vendoraddresscontact.setVendoraddress(null);

		return vendoraddresscontact;
	}

}