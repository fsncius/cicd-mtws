package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the COUNTERCOLLECTION database table.
 * 
 */
@Entity
@NamedQuery(name="Countercollection.findAll", query="SELECT c FROM Countercollection c")
public class Countercollection implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTERCOLLECTION_ID")
	private BigDecimal countercollectionId;

	@Column(name="AUTHORISATION_CODE")
	private String authorisationCode;

	@Column(name="BANK_ACCOUNT_NO")
	private String bankAccountNo;

	@Column(name="BANK_CODE")
	private String bankCode;

	@Temporal(TemporalType.DATE)
	@Column(name="CARD_EXPIRY_DATE")
	private Date cardExpiryDate;

	@Column(name="COLLECTED_AMOUNT")
	private BigDecimal collectedAmount;

	@Column(name="COLLECTION_MODE")
	private String collectionMode;

	@Column(name="COLLECTION_STATUS")
	private String collectionStatus;

	@Column(name="COLLECTION_TYPE")
	private String collectionType;

	@Column(name="CURRENCY_CODE")
	private String currencyCode;

	@Column(name="EXCHANGE_RATE")
	private BigDecimal exchangeRate;

	@Column(name="FOREIGN_AMOUNT")
	private BigDecimal foreignAmount;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="REFERENCE_DATE")
	private Date referenceDate;

	@Column(name="REFERENCE_NO")
	private String referenceNo;

	@Column(name="TRANSFER_IND")
	private String transferInd;

	@Temporal(TemporalType.DATE)
	@Column(name="TXN_DATETIME")
	private Date txnDatetime;

	@Column(name="TXNCODEMSTR_ID")
	private BigDecimal txncodemstrId;

	@Column(name="USERMSTR_ID")
	private BigDecimal usermstrId;

	//bi-directional many-to-one association to Counter
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COUNTER_ID")
	private Counter counter;

	//bi-directional many-to-one association to Countercollection
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CANCEL_COUNTERCOLLECTION_ID")
	private Countercollection countercollection;

	//bi-directional many-to-one association to Countercollection
	@OneToMany(mappedBy="countercollection")
	private List<Countercollection> countercollections;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOCATIONMSTR_ID")
	private Locationmstr locationmstr;
	private BigDecimal locationMstrId;

	//bi-directional many-to-one association to Receipt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="RECEIPT_ID")
	private Receipt receipt;

	//bi-directional many-to-one association to Receipthistory
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="RECEIPTHISTORY_ID")
	private Receipthistory receipthistory;

	private BigDecimal receiptHistoryId;
	private BigDecimal receiptId;
	
	public Countercollection() {
	}

	public BigDecimal getCountercollectionId() {
		return this.countercollectionId;
	}

	public void setCountercollectionId(BigDecimal countercollectionId) {
		this.countercollectionId = countercollectionId;
	}

	public String getAuthorisationCode() {
		return this.authorisationCode;
	}

	public void setAuthorisationCode(String authorisationCode) {
		this.authorisationCode = authorisationCode;
	}

	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public Date getCardExpiryDate() {
		return this.cardExpiryDate;
	}

	public void setCardExpiryDate(Date cardExpiryDate) {
		this.cardExpiryDate = cardExpiryDate;
	}

	public BigDecimal getCollectedAmount() {
		return this.collectedAmount;
	}

	public void setCollectedAmount(BigDecimal collectedAmount) {
		this.collectedAmount = collectedAmount;
	}

	public String getCollectionMode() {
		return this.collectionMode;
	}

	public void setCollectionMode(String collectionMode) {
		this.collectionMode = collectionMode;
	}

	public String getCollectionStatus() {
		return this.collectionStatus;
	}

	public void setCollectionStatus(String collectionStatus) {
		this.collectionStatus = collectionStatus;
	}

	public String getCollectionType() {
		return this.collectionType;
	}

	public void setCollectionType(String collectionType) {
		this.collectionType = collectionType;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public BigDecimal getForeignAmount() {
		return this.foreignAmount;
	}

	public void setForeignAmount(BigDecimal foreignAmount) {
		this.foreignAmount = foreignAmount;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Date getReferenceDate() {
		return this.referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	public String getReferenceNo() {
		return this.referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getTransferInd() {
		return this.transferInd;
	}

	public void setTransferInd(String transferInd) {
		this.transferInd = transferInd;
	}

	public Date getTxnDatetime() {
		return this.txnDatetime;
	}

	public void setTxnDatetime(Date txnDatetime) {
		this.txnDatetime = txnDatetime;
	}

	public BigDecimal getTxncodemstrId() {
		return this.txncodemstrId;
	}

	public void setTxncodemstrId(BigDecimal txncodemstrId) {
		this.txncodemstrId = txncodemstrId;
	}

	public BigDecimal getUsermstrId() {
		return this.usermstrId;
	}

	public void setUsermstrId(BigDecimal usermstrId) {
		this.usermstrId = usermstrId;
	}

	public Counter getCounter() {
		return this.counter;
	}

	public void setCounter(Counter counter) {
		this.counter = counter;
	}

	public Countercollection getCountercollection() {
		return this.countercollection;
	}

	public void setCountercollection(Countercollection countercollection) {
		this.countercollection = countercollection;
	}

	public List<Countercollection> getCountercollections() {
		return this.countercollections;
	}

	public void setCountercollections(List<Countercollection> countercollections) {
		this.countercollections = countercollections;
	}

	public Countercollection addCountercollection(Countercollection countercollection) {
		getCountercollections().add(countercollection);
		countercollection.setCountercollection(this);

		return countercollection;
	}

	public Countercollection removeCountercollection(Countercollection countercollection) {
		getCountercollections().remove(countercollection);
		countercollection.setCountercollection(null);

		return countercollection;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public Receipt getReceipt() {
		return this.receipt;
	}

	public void setReceipt(Receipt receipt) {
		this.receipt = receipt;
	}

	public Receipthistory getReceipthistory() {
		return this.receipthistory;
	}

	public void setReceipthistory(Receipthistory receipthistory) {
		this.receipthistory = receipthistory;
	}

	public BigDecimal getReceiptHistoryId() {
		return receiptHistoryId;
	}

	public void setReceiptHistoryId(BigDecimal receiptHistoryId) {
		this.receiptHistoryId = receiptHistoryId;
	}

	public BigDecimal getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(BigDecimal receiptId) {
		this.receiptId = receiptId;
	}

	public BigDecimal getLocationMstrId() {
		return locationMstrId;
	}

	public void setLocationMstrId(BigDecimal locationMstrId) {
		this.locationMstrId = locationMstrId;
	}

}