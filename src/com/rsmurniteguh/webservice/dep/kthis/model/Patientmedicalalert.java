package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PATIENTMEDICALALERT database table.
 * 
 */
@Entity
@NamedQuery(name="Patientmedicalalert.findAll", query="SELECT p FROM Patientmedicalalert p")
public class Patientmedicalalert implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTMEDICALALERT_ID")
	private long patientmedicalalertId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MEDICAL_ALERT")
	private String medicalAlert;

	@Column(name="MEDICAL_ALERT_STATUS")
	private String medicalAlertStatus;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	//bi-directional many-to-one association to Person
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PERSON_ID")
	private Person person;

	public Patientmedicalalert() {
	}

	public long getPatientmedicalalertId() {
		return this.patientmedicalalertId;
	}

	public void setPatientmedicalalertId(long patientmedicalalertId) {
		this.patientmedicalalertId = patientmedicalalertId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMedicalAlert() {
		return this.medicalAlert;
	}

	public void setMedicalAlert(String medicalAlert) {
		this.medicalAlert = medicalAlert;
	}

	public String getMedicalAlertStatus() {
		return this.medicalAlertStatus;
	}

	public void setMedicalAlertStatus(String medicalAlertStatus) {
		this.medicalAlertStatus = medicalAlertStatus;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}