package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PATIENTACCOUNT database table.
 * 
 */
@Entity
@NamedQuery(name="Patientaccount.findAll", query="SELECT p FROM Patientaccount p")
public class Patientaccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTACCOUNT_ID")
	private BigDecimal patientaccountId;

	@Column(name="ACCOUNT_BALANCE")
	private BigDecimal accountBalance;

	@Column(name="ACCOUNT_CHANGED_IND")
	private String accountChangedInd;

	@Temporal(TemporalType.DATE)
	@Column(name="ACCOUNT_POSTED_DATE")
	private Date accountPostedDate;

	@Column(name="ACCOUNT_STATUS")
	private String accountStatus;

	@Column(name="BILL_SIZE")
	private BigDecimal billSize;

	@Column(name="DEPOSIT_BALANCE")
	private BigDecimal depositBalance;

	@Column(name="FROZEN_ACCOUNT_BALANCE")
	private BigDecimal frozenAccountBalance;

	@Column(name="FROZEN_BILL_SIZE")
	private BigDecimal frozenBillSize;

	@Temporal(TemporalType.DATE)
	@Column(name="FROZEN_DATETIME")
	private Date frozenDatetime;

	@Column(name="INSURANCE_COVERAGE_AMOUNT")
	private BigDecimal insuranceCoverageAmount;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="OPEN_CARD_IND")
	private String openCardInd;

	@Column(name="PATIENT_ACCOUNT_NO")
	private String patientAccountNo;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PRICING_POLICY_CODE")
	private String pricingPolicyCode;

	@Column(name="STAKEHOLDER_ID")
	private BigDecimal stakeholderId;

	//bi-directional many-to-one association to Accountdebtor
	@OneToMany(mappedBy="patientaccount")
	private List<Accountdebtor> accountdebtors;

	//bi-directional many-to-one association to Bill
	@OneToMany(mappedBy="patientaccount")
	private List<Bill> bills;

	//bi-directional many-to-one association to Chargeentry
	@OneToMany(mappedBy="patientaccount")
	private List<Chargeentry> chargeentries;

	//bi-directional many-to-one association to Chargerollingplan
	@OneToMany(mappedBy="patientaccount")
	private List<Chargerollingplan> chargerollingplans;

	//bi-directional many-to-one association to Depositoverdraft
	@OneToMany(mappedBy="patientaccount")
	private List<Depositoverdraft> depositoverdrafts;

	//bi-directional many-to-one association to Deposittopup
	@OneToMany(mappedBy="patientaccount")
	private List<Deposittopup> deposittopups;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Patientaccounttxn
	@OneToMany(mappedBy="patientaccount")
	private List<Patientaccounttxn> patientaccounttxns;

	public Patientaccount() {
	}

	public BigDecimal getPatientaccountId() {
		return this.patientaccountId;
	}

	public void setPatientaccountId(BigDecimal patientaccountId) {
		this.patientaccountId = patientaccountId;
	}

	public BigDecimal getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getAccountChangedInd() {
		return this.accountChangedInd;
	}

	public void setAccountChangedInd(String accountChangedInd) {
		this.accountChangedInd = accountChangedInd;
	}

	public Date getAccountPostedDate() {
		return this.accountPostedDate;
	}

	public void setAccountPostedDate(Date accountPostedDate) {
		this.accountPostedDate = accountPostedDate;
	}

	public String getAccountStatus() {
		return this.accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public BigDecimal getBillSize() {
		return this.billSize;
	}

	public void setBillSize(BigDecimal billSize) {
		this.billSize = billSize;
	}

	public BigDecimal getDepositBalance() {
		return this.depositBalance;
	}

	public void setDepositBalance(BigDecimal depositBalance) {
		this.depositBalance = depositBalance;
	}

	public BigDecimal getFrozenAccountBalance() {
		return this.frozenAccountBalance;
	}

	public void setFrozenAccountBalance(BigDecimal frozenAccountBalance) {
		this.frozenAccountBalance = frozenAccountBalance;
	}

	public BigDecimal getFrozenBillSize() {
		return this.frozenBillSize;
	}

	public void setFrozenBillSize(BigDecimal frozenBillSize) {
		this.frozenBillSize = frozenBillSize;
	}

	public Date getFrozenDatetime() {
		return this.frozenDatetime;
	}

	public void setFrozenDatetime(Date frozenDatetime) {
		this.frozenDatetime = frozenDatetime;
	}

	public BigDecimal getInsuranceCoverageAmount() {
		return this.insuranceCoverageAmount;
	}

	public void setInsuranceCoverageAmount(BigDecimal insuranceCoverageAmount) {
		this.insuranceCoverageAmount = insuranceCoverageAmount;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getOpenCardInd() {
		return this.openCardInd;
	}

	public void setOpenCardInd(String openCardInd) {
		this.openCardInd = openCardInd;
	}

	public String getPatientAccountNo() {
		return this.patientAccountNo;
	}

	public void setPatientAccountNo(String patientAccountNo) {
		this.patientAccountNo = patientAccountNo;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPricingPolicyCode() {
		return this.pricingPolicyCode;
	}

	public void setPricingPolicyCode(String pricingPolicyCode) {
		this.pricingPolicyCode = pricingPolicyCode;
	}

	public BigDecimal getStakeholderId() {
		return this.stakeholderId;
	}

	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
	}

	public List<Accountdebtor> getAccountdebtors() {
		return this.accountdebtors;
	}

	public void setAccountdebtors(List<Accountdebtor> accountdebtors) {
		this.accountdebtors = accountdebtors;
	}

	public Accountdebtor addAccountdebtor(Accountdebtor accountdebtor) {
		getAccountdebtors().add(accountdebtor);
		accountdebtor.setPatientaccount(this);

		return accountdebtor;
	}

	public Accountdebtor removeAccountdebtor(Accountdebtor accountdebtor) {
		getAccountdebtors().remove(accountdebtor);
		accountdebtor.setPatientaccount(null);

		return accountdebtor;
	}

	public List<Bill> getBills() {
		return this.bills;
	}

	public void setBills(List<Bill> bills) {
		this.bills = bills;
	}

	public Bill addBill(Bill bill) {
		getBills().add(bill);
		bill.setPatientaccount(this);

		return bill;
	}

	public Bill removeBill(Bill bill) {
		getBills().remove(bill);
		bill.setPatientaccount(null);

		return bill;
	}

	public List<Chargeentry> getChargeentries() {
		return this.chargeentries;
	}

	public void setChargeentries(List<Chargeentry> chargeentries) {
		this.chargeentries = chargeentries;
	}

	public Chargeentry addChargeentry(Chargeentry chargeentry) {
		getChargeentries().add(chargeentry);
		chargeentry.setPatientaccount(this);

		return chargeentry;
	}

	public Chargeentry removeChargeentry(Chargeentry chargeentry) {
		getChargeentries().remove(chargeentry);
		chargeentry.setPatientaccount(null);

		return chargeentry;
	}

	public List<Chargerollingplan> getChargerollingplans() {
		return this.chargerollingplans;
	}

	public void setChargerollingplans(List<Chargerollingplan> chargerollingplans) {
		this.chargerollingplans = chargerollingplans;
	}

	public Chargerollingplan addChargerollingplan(Chargerollingplan chargerollingplan) {
		getChargerollingplans().add(chargerollingplan);
		chargerollingplan.setPatientaccount(this);

		return chargerollingplan;
	}

	public Chargerollingplan removeChargerollingplan(Chargerollingplan chargerollingplan) {
		getChargerollingplans().remove(chargerollingplan);
		chargerollingplan.setPatientaccount(null);

		return chargerollingplan;
	}

	public List<Depositoverdraft> getDepositoverdrafts() {
		return this.depositoverdrafts;
	}

	public void setDepositoverdrafts(List<Depositoverdraft> depositoverdrafts) {
		this.depositoverdrafts = depositoverdrafts;
	}

	public Depositoverdraft addDepositoverdraft(Depositoverdraft depositoverdraft) {
		getDepositoverdrafts().add(depositoverdraft);
		depositoverdraft.setPatientaccount(this);

		return depositoverdraft;
	}

	public Depositoverdraft removeDepositoverdraft(Depositoverdraft depositoverdraft) {
		getDepositoverdrafts().remove(depositoverdraft);
		depositoverdraft.setPatientaccount(null);

		return depositoverdraft;
	}

	public List<Deposittopup> getDeposittopups() {
		return this.deposittopups;
	}

	public void setDeposittopups(List<Deposittopup> deposittopups) {
		this.deposittopups = deposittopups;
	}

	public Deposittopup addDeposittopup(Deposittopup deposittopup) {
		getDeposittopups().add(deposittopup);
		deposittopup.setPatientaccount(this);

		return deposittopup;
	}

	public Deposittopup removeDeposittopup(Deposittopup deposittopup) {
		getDeposittopups().remove(deposittopup);
		deposittopup.setPatientaccount(null);

		return deposittopup;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public List<Patientaccounttxn> getPatientaccounttxns() {
		return this.patientaccounttxns;
	}

	public void setPatientaccounttxns(List<Patientaccounttxn> patientaccounttxns) {
		this.patientaccounttxns = patientaccounttxns;
	}

	public Patientaccounttxn addPatientaccounttxn(Patientaccounttxn patientaccounttxn) {
		getPatientaccounttxns().add(patientaccounttxn);
		patientaccounttxn.setPatientaccount(this);

		return patientaccounttxn;
	}

	public Patientaccounttxn removePatientaccounttxn(Patientaccounttxn patientaccounttxn) {
		getPatientaccounttxns().remove(patientaccounttxn);
		patientaccounttxn.setPatientaccount(null);

		return patientaccounttxn;
	}

}