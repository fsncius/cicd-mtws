package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PATIENTTRACKINGINFOTEMPLATE database table.
 * 
 */
@Entity
@NamedQuery(name="Patienttrackinginfotemplate.findAll", query="SELECT p FROM Patienttrackinginfotemplate p")
public class Patienttrackinginfotemplate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTTRACKINGINFOTEMPLATE_ID")
	private long patienttrackinginfotemplateId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="INFO_TEMPLATE")
	private String infoTemplate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	//bi-directional many-to-one association to Patienttrackinginfotypemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTTRACKINGINFOTYPEMSTR_ID")
	private Patienttrackinginfotypemstr patienttrackinginfotypemstr;

	public Patienttrackinginfotemplate() {
	}

	public long getPatienttrackinginfotemplateId() {
		return this.patienttrackinginfotemplateId;
	}

	public void setPatienttrackinginfotemplateId(long patienttrackinginfotemplateId) {
		this.patienttrackinginfotemplateId = patienttrackinginfotemplateId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getInfoTemplate() {
		return this.infoTemplate;
	}

	public void setInfoTemplate(String infoTemplate) {
		this.infoTemplate = infoTemplate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public Patienttrackinginfotypemstr getPatienttrackinginfotypemstr() {
		return this.patienttrackinginfotypemstr;
	}

	public void setPatienttrackinginfotypemstr(Patienttrackinginfotypemstr patienttrackinginfotypemstr) {
		this.patienttrackinginfotypemstr = patienttrackinginfotypemstr;
	}

}