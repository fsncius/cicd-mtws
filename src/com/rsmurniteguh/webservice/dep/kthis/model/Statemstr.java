package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STATEMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Statemstr.findAll", query="SELECT s FROM Statemstr s")
public class Statemstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STATEMSTR_ID")
	private long statemstrId;

	@Column(name="CODE_SEQ")
	private BigDecimal codeSeq;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="STATE_CODE")
	private String stateCode;

	@Column(name="STATE_DESC")
	private String stateDesc;

	@Column(name="STATE_DESC_LANG1")
	private String stateDescLang1;

	@Column(name="STATE_DESC_LANG2")
	private String stateDescLang2;

	@Column(name="STATE_DESC_LANG3")
	private String stateDescLang3;

	//bi-directional many-to-one association to Citymstr
	@OneToMany(mappedBy="statemstr")
	private List<Citymstr> citymstrs;

	//bi-directional many-to-one association to Districtmstr
	@OneToMany(mappedBy="statemstr")
	private List<Districtmstr> districtmstrs;

	//bi-directional many-to-one association to Countrymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COUNTRYMSTR_ID")
	private Countrymstr countrymstr;

	//bi-directional many-to-one association to Vendoraddress
	@OneToMany(mappedBy="statemstr")
	private List<Vendoraddress> vendoraddresses;

	public Statemstr() {
	}

	public long getStatemstrId() {
		return this.statemstrId;
	}

	public void setStatemstrId(long statemstrId) {
		this.statemstrId = statemstrId;
	}

	public BigDecimal getCodeSeq() {
		return this.codeSeq;
	}

	public void setCodeSeq(BigDecimal codeSeq) {
		this.codeSeq = codeSeq;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getStateCode() {
		return this.stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateDesc() {
		return this.stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public String getStateDescLang1() {
		return this.stateDescLang1;
	}

	public void setStateDescLang1(String stateDescLang1) {
		this.stateDescLang1 = stateDescLang1;
	}

	public String getStateDescLang2() {
		return this.stateDescLang2;
	}

	public void setStateDescLang2(String stateDescLang2) {
		this.stateDescLang2 = stateDescLang2;
	}

	public String getStateDescLang3() {
		return this.stateDescLang3;
	}

	public void setStateDescLang3(String stateDescLang3) {
		this.stateDescLang3 = stateDescLang3;
	}

	public List<Citymstr> getCitymstrs() {
		return this.citymstrs;
	}

	public void setCitymstrs(List<Citymstr> citymstrs) {
		this.citymstrs = citymstrs;
	}

	public Citymstr addCitymstr(Citymstr citymstr) {
		getCitymstrs().add(citymstr);
		citymstr.setStatemstr(this);

		return citymstr;
	}

	public Citymstr removeCitymstr(Citymstr citymstr) {
		getCitymstrs().remove(citymstr);
		citymstr.setStatemstr(null);

		return citymstr;
	}

	public List<Districtmstr> getDistrictmstrs() {
		return this.districtmstrs;
	}

	public void setDistrictmstrs(List<Districtmstr> districtmstrs) {
		this.districtmstrs = districtmstrs;
	}

	public Districtmstr addDistrictmstr(Districtmstr districtmstr) {
		getDistrictmstrs().add(districtmstr);
		districtmstr.setStatemstr(this);

		return districtmstr;
	}

	public Districtmstr removeDistrictmstr(Districtmstr districtmstr) {
		getDistrictmstrs().remove(districtmstr);
		districtmstr.setStatemstr(null);

		return districtmstr;
	}

	public Countrymstr getCountrymstr() {
		return this.countrymstr;
	}

	public void setCountrymstr(Countrymstr countrymstr) {
		this.countrymstr = countrymstr;
	}

	public List<Vendoraddress> getVendoraddresses() {
		return this.vendoraddresses;
	}

	public void setVendoraddresses(List<Vendoraddress> vendoraddresses) {
		this.vendoraddresses = vendoraddresses;
	}

	public Vendoraddress addVendoraddress(Vendoraddress vendoraddress) {
		getVendoraddresses().add(vendoraddress);
		vendoraddress.setStatemstr(this);

		return vendoraddress;
	}

	public Vendoraddress removeVendoraddress(Vendoraddress vendoraddress) {
		getVendoraddresses().remove(vendoraddress);
		vendoraddress.setStatemstr(null);

		return vendoraddress;
	}

}