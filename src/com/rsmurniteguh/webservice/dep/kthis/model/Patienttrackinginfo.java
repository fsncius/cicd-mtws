package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PATIENTTRACKINGINFO database table.
 * 
 */
@Entity
@NamedQuery(name="Patienttrackinginfo.findAll", query="SELECT p FROM Patienttrackinginfo p")
public class Patienttrackinginfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTTRACKINGINFO_ID")
	private long patienttrackinginfoId;

	@Column(name="COMPLETED_BY")
	private BigDecimal completedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="COMPLETED_DATETIME")
	private Date completedDatetime;

	@Column(name="COMPLETED_INFO")
	private String completedInfo;

	@Column(name="COMPLETED_LOCATIONMSTR_ID")
	private BigDecimal completedLocationmstrId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="CREATED_LOCATIONMSTR_ID")
	private BigDecimal createdLocationmstrId;

	@Column(name="DEFUNCT_BY")
	private BigDecimal defunctBy;

	@Temporal(TemporalType.DATE)
	@Column(name="DEFUNCT_DATETIME")
	private Date defunctDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="INFO_CONTENT")
	private String infoContent;

	@Column(name="INFO_STATUS")
	private String infoStatus;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENTTRACKINGINFO_LEVEL")
	private String patienttrackinginfoLevel;

	@Column(name="PATIENTTRACKINGINFO_TYPE")
	private String patienttrackinginfoType;

	//bi-directional many-to-one association to Patient
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENT_ID")
	private Patient patient;

	//bi-directional many-to-one association to Patienttrackinginfotypemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTTRACKINGINFOTYPEMSTR_ID")
	private Patienttrackinginfotypemstr patienttrackinginfotypemstr;

	//bi-directional many-to-one association to Patienttrackinginforole
	@OneToMany(mappedBy="patienttrackinginfo")
	private List<Patienttrackinginforole> patienttrackinginforoles;

	public Patienttrackinginfo() {
	}

	public long getPatienttrackinginfoId() {
		return this.patienttrackinginfoId;
	}

	public void setPatienttrackinginfoId(long patienttrackinginfoId) {
		this.patienttrackinginfoId = patienttrackinginfoId;
	}

	public BigDecimal getCompletedBy() {
		return this.completedBy;
	}

	public void setCompletedBy(BigDecimal completedBy) {
		this.completedBy = completedBy;
	}

	public Date getCompletedDatetime() {
		return this.completedDatetime;
	}

	public void setCompletedDatetime(Date completedDatetime) {
		this.completedDatetime = completedDatetime;
	}

	public String getCompletedInfo() {
		return this.completedInfo;
	}

	public void setCompletedInfo(String completedInfo) {
		this.completedInfo = completedInfo;
	}

	public BigDecimal getCompletedLocationmstrId() {
		return this.completedLocationmstrId;
	}

	public void setCompletedLocationmstrId(BigDecimal completedLocationmstrId) {
		this.completedLocationmstrId = completedLocationmstrId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getCreatedLocationmstrId() {
		return this.createdLocationmstrId;
	}

	public void setCreatedLocationmstrId(BigDecimal createdLocationmstrId) {
		this.createdLocationmstrId = createdLocationmstrId;
	}

	public BigDecimal getDefunctBy() {
		return this.defunctBy;
	}

	public void setDefunctBy(BigDecimal defunctBy) {
		this.defunctBy = defunctBy;
	}

	public Date getDefunctDatetime() {
		return this.defunctDatetime;
	}

	public void setDefunctDatetime(Date defunctDatetime) {
		this.defunctDatetime = defunctDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getInfoContent() {
		return this.infoContent;
	}

	public void setInfoContent(String infoContent) {
		this.infoContent = infoContent;
	}

	public String getInfoStatus() {
		return this.infoStatus;
	}

	public void setInfoStatus(String infoStatus) {
		this.infoStatus = infoStatus;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPatienttrackinginfoLevel() {
		return this.patienttrackinginfoLevel;
	}

	public void setPatienttrackinginfoLevel(String patienttrackinginfoLevel) {
		this.patienttrackinginfoLevel = patienttrackinginfoLevel;
	}

	public String getPatienttrackinginfoType() {
		return this.patienttrackinginfoType;
	}

	public void setPatienttrackinginfoType(String patienttrackinginfoType) {
		this.patienttrackinginfoType = patienttrackinginfoType;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Patienttrackinginfotypemstr getPatienttrackinginfotypemstr() {
		return this.patienttrackinginfotypemstr;
	}

	public void setPatienttrackinginfotypemstr(Patienttrackinginfotypemstr patienttrackinginfotypemstr) {
		this.patienttrackinginfotypemstr = patienttrackinginfotypemstr;
	}

	public List<Patienttrackinginforole> getPatienttrackinginforoles() {
		return this.patienttrackinginforoles;
	}

	public void setPatienttrackinginforoles(List<Patienttrackinginforole> patienttrackinginforoles) {
		this.patienttrackinginforoles = patienttrackinginforoles;
	}

	public Patienttrackinginforole addPatienttrackinginforole(Patienttrackinginforole patienttrackinginforole) {
		getPatienttrackinginforoles().add(patienttrackinginforole);
		patienttrackinginforole.setPatienttrackinginfo(this);

		return patienttrackinginforole;
	}

	public Patienttrackinginforole removePatienttrackinginforole(Patienttrackinginforole patienttrackinginforole) {
		getPatienttrackinginforoles().remove(patienttrackinginforole);
		patienttrackinginforole.setPatienttrackinginfo(null);

		return patienttrackinginforole;
	}

}