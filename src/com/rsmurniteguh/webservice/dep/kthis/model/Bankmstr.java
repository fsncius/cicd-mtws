package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BANKMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Bankmstr.findAll", query="SELECT b FROM Bankmstr b")
public class Bankmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BANKMSTR_ID")
	private long bankmstrId;

	@Column(name="BANK_CODE")
	private String bankCode;

	@Column(name="BANK_DESC")
	private String bankDesc;

	@Column(name="BANK_DESC_LANG1")
	private String bankDescLang1;

	@Column(name="BANK_DESC_LANG2")
	private String bankDescLang2;

	@Column(name="BANK_DESC_LANG3")
	private String bankDescLang3;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SG_GIRO_BANK_CODE")
	private String sgGiroBankCode;

	//bi-directional many-to-one association to Vendormstr
	@OneToMany(mappedBy="bankmstr")
	private List<Vendormstr> vendormstrs;

	public Bankmstr() {
	}

	public long getBankmstrId() {
		return this.bankmstrId;
	}

	public void setBankmstrId(long bankmstrId) {
		this.bankmstrId = bankmstrId;
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankDesc() {
		return this.bankDesc;
	}

	public void setBankDesc(String bankDesc) {
		this.bankDesc = bankDesc;
	}

	public String getBankDescLang1() {
		return this.bankDescLang1;
	}

	public void setBankDescLang1(String bankDescLang1) {
		this.bankDescLang1 = bankDescLang1;
	}

	public String getBankDescLang2() {
		return this.bankDescLang2;
	}

	public void setBankDescLang2(String bankDescLang2) {
		this.bankDescLang2 = bankDescLang2;
	}

	public String getBankDescLang3() {
		return this.bankDescLang3;
	}

	public void setBankDescLang3(String bankDescLang3) {
		this.bankDescLang3 = bankDescLang3;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getSgGiroBankCode() {
		return this.sgGiroBankCode;
	}

	public void setSgGiroBankCode(String sgGiroBankCode) {
		this.sgGiroBankCode = sgGiroBankCode;
	}

	public List<Vendormstr> getVendormstrs() {
		return this.vendormstrs;
	}

	public void setVendormstrs(List<Vendormstr> vendormstrs) {
		this.vendormstrs = vendormstrs;
	}

	public Vendormstr addVendormstr(Vendormstr vendormstr) {
		getVendormstrs().add(vendormstr);
		vendormstr.setBankmstr(this);

		return vendormstr;
	}

	public Vendormstr removeVendormstr(Vendormstr vendormstr) {
		getVendormstrs().remove(vendormstr);
		vendormstr.setBankmstr(null);

		return vendormstr;
	}

}