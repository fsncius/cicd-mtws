package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the VITALSIGNITEMMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Vitalsignitemmstr.findAll", query="SELECT v FROM Vitalsignitemmstr v")
public class Vitalsignitemmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VITALSIGNITEMMSTR_ID")
	private long vitalsignitemmstrId;

	@Column(name="DATA_ENTRY_FREQUENCY")
	private String dataEntryFrequency;

	@Column(name="DATA_TYPE")
	private String dataType;

	@Column(name="DEFAULT_USE_IND")
	private String defaultUseInd;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="ITEM_CODE")
	private String itemCode;

	@Column(name="ITEM_DESC")
	private String itemDesc;

	@Column(name="ITEM_UOM")
	private String itemUom;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MANDATORY_USE_IND")
	private String mandatoryUseInd;

	@Column(name="MAX_VALUE")
	private BigDecimal maxValue;

	@Column(name="MIN_VALUE")
	private BigDecimal minValue;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	//bi-directional many-to-one association to Visitvitalsignitem
	@OneToMany(mappedBy="vitalsignitemmstr")
	private List<Visitvitalsignitem> visitvitalsignitems;

	//bi-directional many-to-one association to Visitvitalsignrecord
	@OneToMany(mappedBy="vitalsignitemmstr")
	private List<Visitvitalsignrecord> visitvitalsignrecords;

	//bi-directional many-to-one association to Vitalsignitemvaluemstr
	@OneToMany(mappedBy="vitalsignitemmstr")
	private List<Vitalsignitemvaluemstr> vitalsignitemvaluemstrs;

	public Vitalsignitemmstr() {
	}

	public long getVitalsignitemmstrId() {
		return this.vitalsignitemmstrId;
	}

	public void setVitalsignitemmstrId(long vitalsignitemmstrId) {
		this.vitalsignitemmstrId = vitalsignitemmstrId;
	}

	public String getDataEntryFrequency() {
		return this.dataEntryFrequency;
	}

	public void setDataEntryFrequency(String dataEntryFrequency) {
		this.dataEntryFrequency = dataEntryFrequency;
	}

	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getDefaultUseInd() {
		return this.defaultUseInd;
	}

	public void setDefaultUseInd(String defaultUseInd) {
		this.defaultUseInd = defaultUseInd;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDesc() {
		return this.itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getItemUom() {
		return this.itemUom;
	}

	public void setItemUom(String itemUom) {
		this.itemUom = itemUom;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMandatoryUseInd() {
		return this.mandatoryUseInd;
	}

	public void setMandatoryUseInd(String mandatoryUseInd) {
		this.mandatoryUseInd = mandatoryUseInd;
	}

	public BigDecimal getMaxValue() {
		return this.maxValue;
	}

	public void setMaxValue(BigDecimal maxValue) {
		this.maxValue = maxValue;
	}

	public BigDecimal getMinValue() {
		return this.minValue;
	}

	public void setMinValue(BigDecimal minValue) {
		this.minValue = minValue;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public List<Visitvitalsignitem> getVisitvitalsignitems() {
		return this.visitvitalsignitems;
	}

	public void setVisitvitalsignitems(List<Visitvitalsignitem> visitvitalsignitems) {
		this.visitvitalsignitems = visitvitalsignitems;
	}

	public Visitvitalsignitem addVisitvitalsignitem(Visitvitalsignitem visitvitalsignitem) {
		getVisitvitalsignitems().add(visitvitalsignitem);
		visitvitalsignitem.setVitalsignitemmstr(this);

		return visitvitalsignitem;
	}

	public Visitvitalsignitem removeVisitvitalsignitem(Visitvitalsignitem visitvitalsignitem) {
		getVisitvitalsignitems().remove(visitvitalsignitem);
		visitvitalsignitem.setVitalsignitemmstr(null);

		return visitvitalsignitem;
	}

	public List<Visitvitalsignrecord> getVisitvitalsignrecords() {
		return this.visitvitalsignrecords;
	}

	public void setVisitvitalsignrecords(List<Visitvitalsignrecord> visitvitalsignrecords) {
		this.visitvitalsignrecords = visitvitalsignrecords;
	}

	public Visitvitalsignrecord addVisitvitalsignrecord(Visitvitalsignrecord visitvitalsignrecord) {
		getVisitvitalsignrecords().add(visitvitalsignrecord);
		visitvitalsignrecord.setVitalsignitemmstr(this);

		return visitvitalsignrecord;
	}

	public Visitvitalsignrecord removeVisitvitalsignrecord(Visitvitalsignrecord visitvitalsignrecord) {
		getVisitvitalsignrecords().remove(visitvitalsignrecord);
		visitvitalsignrecord.setVitalsignitemmstr(null);

		return visitvitalsignrecord;
	}

	public List<Vitalsignitemvaluemstr> getVitalsignitemvaluemstrs() {
		return this.vitalsignitemvaluemstrs;
	}

	public void setVitalsignitemvaluemstrs(List<Vitalsignitemvaluemstr> vitalsignitemvaluemstrs) {
		this.vitalsignitemvaluemstrs = vitalsignitemvaluemstrs;
	}

	public Vitalsignitemvaluemstr addVitalsignitemvaluemstr(Vitalsignitemvaluemstr vitalsignitemvaluemstr) {
		getVitalsignitemvaluemstrs().add(vitalsignitemvaluemstr);
		vitalsignitemvaluemstr.setVitalsignitemmstr(this);

		return vitalsignitemvaluemstr;
	}

	public Vitalsignitemvaluemstr removeVitalsignitemvaluemstr(Vitalsignitemvaluemstr vitalsignitemvaluemstr) {
		getVitalsignitemvaluemstrs().remove(vitalsignitemvaluemstr);
		vitalsignitemvaluemstr.setVitalsignitemmstr(null);

		return vitalsignitemvaluemstr;
	}

}