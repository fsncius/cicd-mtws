package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CONSULTATIONREQUISITION database table.
 * 
 */
@Entity
@NamedQuery(name="Consultationrequisition.findAll", query="SELECT c FROM Consultationrequisition c")
public class Consultationrequisition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CONSULTATIONREQUISITION_ID")
	private long consultationrequisitionId;

	@Temporal(TemporalType.DATE)
	@Column(name="AGREE_BEGIN_DATE")
	private Date agreeBeginDate;

	@Temporal(TemporalType.DATE)
	@Column(name="AGREE_END_DATE")
	private Date agreeEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name="CONSULTATION_DATETIME")
	private Date consultationDatetime;

	@Column(name="CONSULTATION_PURPOSE")
	private String consultationPurpose;

	@Column(name="CONSULTATION_STATUS")
	private String consultationStatus;

	@Column(name="CONSULTATION_SUGGESTION")
	private String consultationSuggestion;

	@Column(name="CONSULTATION_TYPE")
	private String consultationType;

	@Column(name="CONSULTATIONREQUISITION_NO")
	private String consultationrequisitionNo;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MEDICAL_DESC")
	private String medicalDesc;

	@Column(name="ORDERENTRY_ID")
	private BigDecimal orderentryId;

	@Temporal(TemporalType.DATE)
	@Column(name="REQUISITION_DATETIME")
	private Date requisitionDatetime;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONSULTATION_CP_ID")
	private Careprovider careprovider1;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REQUISITION_CP_ID")
	private Careprovider careprovider2;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr1;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONSULTATION_SSPM_ID")
	private Subspecialtymstr subspecialtymstr2;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REQUISITION_SSPM_ID")
	private Subspecialtymstr subspecialtymstr3;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public Consultationrequisition() {
	}

	public long getConsultationrequisitionId() {
		return this.consultationrequisitionId;
	}

	public void setConsultationrequisitionId(long consultationrequisitionId) {
		this.consultationrequisitionId = consultationrequisitionId;
	}

	public Date getAgreeBeginDate() {
		return this.agreeBeginDate;
	}

	public void setAgreeBeginDate(Date agreeBeginDate) {
		this.agreeBeginDate = agreeBeginDate;
	}

	public Date getAgreeEndDate() {
		return this.agreeEndDate;
	}

	public void setAgreeEndDate(Date agreeEndDate) {
		this.agreeEndDate = agreeEndDate;
	}

	public Date getConsultationDatetime() {
		return this.consultationDatetime;
	}

	public void setConsultationDatetime(Date consultationDatetime) {
		this.consultationDatetime = consultationDatetime;
	}

	public String getConsultationPurpose() {
		return this.consultationPurpose;
	}

	public void setConsultationPurpose(String consultationPurpose) {
		this.consultationPurpose = consultationPurpose;
	}

	public String getConsultationStatus() {
		return this.consultationStatus;
	}

	public void setConsultationStatus(String consultationStatus) {
		this.consultationStatus = consultationStatus;
	}

	public String getConsultationSuggestion() {
		return this.consultationSuggestion;
	}

	public void setConsultationSuggestion(String consultationSuggestion) {
		this.consultationSuggestion = consultationSuggestion;
	}

	public String getConsultationType() {
		return this.consultationType;
	}

	public void setConsultationType(String consultationType) {
		this.consultationType = consultationType;
	}

	public String getConsultationrequisitionNo() {
		return this.consultationrequisitionNo;
	}

	public void setConsultationrequisitionNo(String consultationrequisitionNo) {
		this.consultationrequisitionNo = consultationrequisitionNo;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMedicalDesc() {
		return this.medicalDesc;
	}

	public void setMedicalDesc(String medicalDesc) {
		this.medicalDesc = medicalDesc;
	}

	public BigDecimal getOrderentryId() {
		return this.orderentryId;
	}

	public void setOrderentryId(BigDecimal orderentryId) {
		this.orderentryId = orderentryId;
	}

	public Date getRequisitionDatetime() {
		return this.requisitionDatetime;
	}

	public void setRequisitionDatetime(Date requisitionDatetime) {
		this.requisitionDatetime = requisitionDatetime;
	}

	public Careprovider getCareprovider1() {
		return this.careprovider1;
	}

	public void setCareprovider1(Careprovider careprovider1) {
		this.careprovider1 = careprovider1;
	}

	public Careprovider getCareprovider2() {
		return this.careprovider2;
	}

	public void setCareprovider2(Careprovider careprovider2) {
		this.careprovider2 = careprovider2;
	}

	public Subspecialtymstr getSubspecialtymstr1() {
		return this.subspecialtymstr1;
	}

	public void setSubspecialtymstr1(Subspecialtymstr subspecialtymstr1) {
		this.subspecialtymstr1 = subspecialtymstr1;
	}

	public Subspecialtymstr getSubspecialtymstr2() {
		return this.subspecialtymstr2;
	}

	public void setSubspecialtymstr2(Subspecialtymstr subspecialtymstr2) {
		this.subspecialtymstr2 = subspecialtymstr2;
	}

	public Subspecialtymstr getSubspecialtymstr3() {
		return this.subspecialtymstr3;
	}

	public void setSubspecialtymstr3(Subspecialtymstr subspecialtymstr3) {
		this.subspecialtymstr3 = subspecialtymstr3;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}