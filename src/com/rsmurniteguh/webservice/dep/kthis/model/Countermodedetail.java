package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the COUNTERMODEDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Countermodedetail.findAll", query="SELECT c FROM Countermodedetail c")
public class Countermodedetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTERMODEDETAIL_ID")
	private long countermodedetailId;

	@Column(name="COLLECTED_AMOUNT")
	private BigDecimal collectedAmount;

	@Column(name="COLLECTION_MODE")
	private String collectionMode;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="TXNCODEMSTR_ID")
	private BigDecimal txncodemstrId;

	//bi-directional many-to-one association to Counter
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COUNTER_ID")
	private Counter counter;

	public Countermodedetail() {
	}

	public long getCountermodedetailId() {
		return this.countermodedetailId;
	}

	public void setCountermodedetailId(long countermodedetailId) {
		this.countermodedetailId = countermodedetailId;
	}

	public BigDecimal getCollectedAmount() {
		return this.collectedAmount;
	}

	public void setCollectedAmount(BigDecimal collectedAmount) {
		this.collectedAmount = collectedAmount;
	}

	public String getCollectionMode() {
		return this.collectionMode;
	}

	public void setCollectionMode(String collectionMode) {
		this.collectionMode = collectionMode;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getTxncodemstrId() {
		return this.txncodemstrId;
	}

	public void setTxncodemstrId(BigDecimal txncodemstrId) {
		this.txncodemstrId = txncodemstrId;
	}

	public Counter getCounter() {
		return this.counter;
	}

	public void setCounter(Counter counter) {
		this.counter = counter;
	}

}