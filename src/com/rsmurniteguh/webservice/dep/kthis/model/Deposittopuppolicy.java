package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DEPOSITTOPUPPOLICY database table.
 * 
 */
@Entity
@NamedQuery(name="Deposittopuppolicy.findAll", query="SELECT d FROM Deposittopuppolicy d")
public class Deposittopuppolicy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DEPOSITTOPUPPOLICY_ID")
	private long deposittopuppolicyId;

	@Column(name="ALERT_PERCENT")
	private BigDecimal alertPercent;

	private BigDecimal coefficient;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Column(name="ENTITYMSTR_ID")
	private BigDecimal entitymstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="FINANCIAL_CLASS")
	private String financialClass;

	@Column(name="FROZEN_PERCENT")
	private BigDecimal frozenPercent;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="POLICY_CODE")
	private String policyCode;

	@Column(name="POLICY_DESC")
	private String policyDesc;

	@Column(name="POLICY_DESC_LANG1")
	private String policyDescLang1;

	@Column(name="POLICY_DESC_LANG2")
	private String policyDescLang2;

	@Column(name="POLICY_DESC_LANG3")
	private String policyDescLang3;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SELFPAY_PERCENT")
	private BigDecimal selfpayPercent;

	public Deposittopuppolicy() {
	}

	public long getDeposittopuppolicyId() {
		return this.deposittopuppolicyId;
	}

	public void setDeposittopuppolicyId(long deposittopuppolicyId) {
		this.deposittopuppolicyId = deposittopuppolicyId;
	}

	public BigDecimal getAlertPercent() {
		return this.alertPercent;
	}

	public void setAlertPercent(BigDecimal alertPercent) {
		this.alertPercent = alertPercent;
	}

	public BigDecimal getCoefficient() {
		return this.coefficient;
	}

	public void setCoefficient(BigDecimal coefficient) {
		this.coefficient = coefficient;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public BigDecimal getEntitymstrId() {
		return this.entitymstrId;
	}

	public void setEntitymstrId(BigDecimal entitymstrId) {
		this.entitymstrId = entitymstrId;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getFinancialClass() {
		return this.financialClass;
	}

	public void setFinancialClass(String financialClass) {
		this.financialClass = financialClass;
	}

	public BigDecimal getFrozenPercent() {
		return this.frozenPercent;
	}

	public void setFrozenPercent(BigDecimal frozenPercent) {
		this.frozenPercent = frozenPercent;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPolicyCode() {
		return this.policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getPolicyDesc() {
		return this.policyDesc;
	}

	public void setPolicyDesc(String policyDesc) {
		this.policyDesc = policyDesc;
	}

	public String getPolicyDescLang1() {
		return this.policyDescLang1;
	}

	public void setPolicyDescLang1(String policyDescLang1) {
		this.policyDescLang1 = policyDescLang1;
	}

	public String getPolicyDescLang2() {
		return this.policyDescLang2;
	}

	public void setPolicyDescLang2(String policyDescLang2) {
		this.policyDescLang2 = policyDescLang2;
	}

	public String getPolicyDescLang3() {
		return this.policyDescLang3;
	}

	public void setPolicyDescLang3(String policyDescLang3) {
		this.policyDescLang3 = policyDescLang3;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getSelfpayPercent() {
		return this.selfpayPercent;
	}

	public void setSelfpayPercent(BigDecimal selfpayPercent) {
		this.selfpayPercent = selfpayPercent;
	}

}