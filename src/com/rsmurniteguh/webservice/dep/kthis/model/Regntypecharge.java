package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the REGNTYPECHARGE database table.
 * 
 */
@Entity
@NamedQuery(name="Regntypecharge.findAll", query="SELECT r FROM Regntypecharge r")
public class Regntypecharge implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REGNTYPECHARGE_ID")
	private long regntypechargeId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATETIME")
	private Date effectiveDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATETIME")
	private Date expiryDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REGISTRATION_TYPE")
	private String registrationType;

	@Column(name="RESOURCEMSTR_ID")
	private BigDecimal resourcemstrId;

	@Column(name="VISIT_TYPE")
	private String visitType;

	//bi-directional many-to-one association to Chargeitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEITEMMSTR_ID")
	private Chargeitemmstr chargeitemmstr;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	public Regntypecharge() {
	}

	public long getRegntypechargeId() {
		return this.regntypechargeId;
	}

	public void setRegntypechargeId(long regntypechargeId) {
		this.regntypechargeId = regntypechargeId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffectiveDatetime() {
		return this.effectiveDatetime;
	}

	public void setEffectiveDatetime(Date effectiveDatetime) {
		this.effectiveDatetime = effectiveDatetime;
	}

	public Date getExpiryDatetime() {
		return this.expiryDatetime;
	}

	public void setExpiryDatetime(Date expiryDatetime) {
		this.expiryDatetime = expiryDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRegistrationType() {
		return this.registrationType;
	}

	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}

	public BigDecimal getResourcemstrId() {
		return this.resourcemstrId;
	}

	public void setResourcemstrId(BigDecimal resourcemstrId) {
		this.resourcemstrId = resourcemstrId;
	}

	public String getVisitType() {
		return this.visitType;
	}

	public void setVisitType(String visitType) {
		this.visitType = visitType;
	}

	public Chargeitemmstr getChargeitemmstr() {
		return this.chargeitemmstr;
	}

	public void setChargeitemmstr(Chargeitemmstr chargeitemmstr) {
		this.chargeitemmstr = chargeitemmstr;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

}