package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the REGNPATIENTCLASSLIMIT database table.
 * 
 */
@Entity
@NamedQuery(name="Regnpatientclasslimit.findAll", query="SELECT r FROM Regnpatientclasslimit r")
public class Regnpatientclasslimit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REGNPATIENTCLASSLIMIT_ID")
	private long regnpatientclasslimitId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="REGN_TYPE")
	private String regnType;

	public Regnpatientclasslimit() {
	}

	public long getRegnpatientclasslimitId() {
		return this.regnpatientclasslimitId;
	}

	public void setRegnpatientclasslimitId(long regnpatientclasslimitId) {
		this.regnpatientclasslimitId = regnpatientclasslimitId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getRegnType() {
		return this.regnType;
	}

	public void setRegnType(String regnType) {
		this.regnType = regnType;
	}

}