package com.rsmurniteguh.webservice.dep.kthis.model;
import java.math.BigDecimal;
import java.sql.Timestamp;

public final class ProtokolHD {

	public ProtokolHD() {}

	private BigDecimal cardNo;
	private Timestamp tanggal;
	private BigDecimal ProtokolHdId;
	private BigDecimal visitId;
	private Timestamp tanggalHd;
	private String diagnosaMedis;
	private String riwayatKesehatan;
	private String nomorMesin;
	private String hemodialisisKe;
	private String tipeDialiser;
	private String caraBayar;
	private String statusPsikososial;
	private String statusFungsional;
	private String alergiObat;
	private String sensoriumPreHd;
	private String tdPreHd;
	private String nadiPreHd;
	private String nafasPreHd;
	private String suhuPreHd;
	private String bbKeringPreHd;
	private String bbDatangPreHd;
	private String bbSebelumnyaPreHd;
	private String bbKenaikanPreHd;
	private String aksesVaskularPreHd;
	private String sensoriumPostHd;
	private String tdPostHd;
	private String nadiPostHd;
	private String nafasPostHd;
	private String suhuPostHd;
	private String bbSelesaiPostHd;
	private String bedaBbPostHd;
	private String catatanPostHd;
	private Timestamp waktuMulai;
	private BigDecimal lamaHd;
	private Timestamp waktuSelesai;
	private String primingMasuk;
	private String primingKeluar;
	private String antiKoagulasi;
	private String awalHd;
	private String maintenance;
	private String tanpaHeparin;
	private String ufgHd;
	private String resepHd;
	private String durasiHd;
	private String qbInstruksiMedik;
	private String qdInstruksiMedik;
	private String tdInstruksiMedik;
	private String ufgInstruksiMedik;
	private String ufrInstruksiMedik;
	private String profillingInstruksiMedik;
	private String antikoagulasiInstruksiMedik;
	private String namaDokter;
	private String pemeriksaanFisik;
	private String keadaanUmum;
	private String kondisiSaatIni;
	private String kondisiSakit;
	private BigDecimal indikatorSakit;
	private String diagnosaKeperawatan;
	private String intervensiKeperawatan;
	private String intervensiKolaborasi;
	private String catatanKeperawatan;
	private String namaPerawat;
	private Timestamp waktuCatatanPerkembangan;
	private String utkDokter;
	private String utkStaff;
	private String ttdPerawat;
	private String rekomendasiGizi;
	private String kaloriGizi;
	private String proteinGizi;
	private String pengkajianGizi;
	private String edukasiMateri;
	private String leafletBrosur;
	private String rencanaKedepan;
	private String namaMesinHd;
	private String dialyzerHd;
	private BigDecimal desinfectansSebelumHd;
	private BigDecimal rinserSebelumHD;
	private String jenisHd;
	private String komplikasiHd;
	private String conductivitySebelumHd;
	private String temperaturSebelumHd;
	private String dialisatSebelumHd;
	private String sisaPriming;
	private String dripMasuk;
	private String darahMasuk;
	private String washOut;
	private String minumMasuk;
	private String jumlahMasuk;
	private BigDecimal penekananMasuk;
	private BigDecimal desinfectantsSesudahHd;
	private BigDecimal rinseSesudahHd;
	private BigDecimal createdBy;
	private Timestamp createdDateTime;
	private BigDecimal lastUpdatedBy;
	private Timestamp lastUpdatedDateTime;
	private String defunctInd;
	private String tanggalString;
	
	public BigDecimal getProtokolHdId() {
		return ProtokolHdId;
	}
	public void setProtokolHdId(BigDecimal protokolHdId) {
		ProtokolHdId = protokolHdId;
	}
	public BigDecimal getVisitId() {
		return visitId;
	}
	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}
	public Timestamp getTanggalHd() {
		return tanggalHd;
	}
	public void setTanggalHd(Timestamp tanggalHd) {
		this.tanggalHd = tanggalHd;
	}
	public String getDiagnosaMedis() {
		return diagnosaMedis;
	}
	public void setDiagnosaMedis(String diagnosaMedis) {
		this.diagnosaMedis = diagnosaMedis;
	}
	public String getRiwayatKesehatan() {
		return riwayatKesehatan;
	}
	public void setRiwayatKesehatan(String riwayatKesehatan) {
		this.riwayatKesehatan = riwayatKesehatan;
	}
	public String getNomorMesin() {
		return nomorMesin;
	}
	public void setNomorMesin(String nomorMesin) {
		this.nomorMesin = nomorMesin;
	}
	public String getHemodialisisKe() {
		return hemodialisisKe;
	}
	public void setHemodialisisKe(String hemodialisisKe) {
		this.hemodialisisKe = hemodialisisKe;
	}
	public String getTipeDialiser() {
		return tipeDialiser;
	}
	public void setTipeDialiser(String tipeDialiser) {
		this.tipeDialiser = tipeDialiser;
	}
	public String getCaraBayar() {
		return caraBayar;
	}
	public void setCaraBayar(String caraBayar) {
		this.caraBayar = caraBayar;
	}
	public String getStatusPsikososial() {
		return statusPsikososial;
	}
	public void setStatusPsikososial(String statusPsikososial) {
		this.statusPsikososial = statusPsikososial;
	}
	public String getStatusFungsional() {
		return statusFungsional;
	}
	public void setStatusFungsional(String statusFungsional) {
		this.statusFungsional = statusFungsional;
	}
	public String getAlergiObat() {
		return alergiObat;
	}
	public void setAlergiObat(String alergiObat) {
		this.alergiObat = alergiObat;
	}
	public String getSensoriumPreHd() {
		return sensoriumPreHd;
	}
	public void setSensoriumPreHd(String sensoriumPreHd) {
		this.sensoriumPreHd = sensoriumPreHd;
	}
	public String getTdPreHd() {
		return tdPreHd;
	}
	public void setTdPreHd(String tdPreHd) {
		this.tdPreHd = tdPreHd;
	}
	public String getNadiPreHd() {
		return nadiPreHd;
	}
	public void setNadiPreHd(String nadiPreHd) {
		this.nadiPreHd = nadiPreHd;
	}
	public String getNafasPreHd() {
		return nafasPreHd;
	}
	public void setNafasPreHd(String nafasPreHd) {
		this.nafasPreHd = nafasPreHd;
	}
	public String getSuhuPreHd() {
		return suhuPreHd;
	}
	public void setSuhuPreHd(String suhuPreHd) {
		this.suhuPreHd = suhuPreHd;
	}
	public String getBbKeringPreHd() {
		return bbKeringPreHd;
	}
	public void setBbKeringPreHd(String bbKeringPreHd) {
		this.bbKeringPreHd = bbKeringPreHd;
	}
	public String getBbDatangPreHd() {
		return bbDatangPreHd;
	}
	public void setBbDatangPreHd(String bbDatangPreHd) {
		this.bbDatangPreHd = bbDatangPreHd;
	}
	public String getBbSebelumnyaPreHd() {
		return bbSebelumnyaPreHd;
	}
	public void setBbSebelumnyaPreHd(String bbSebelumnyaPreHd) {
		this.bbSebelumnyaPreHd = bbSebelumnyaPreHd;
	}
	public String getBbKenaikanPreHd() {
		return bbKenaikanPreHd;
	}
	public void setBbKenaikanPreHd(String bbKenaikanPreHd) {
		this.bbKenaikanPreHd = bbKenaikanPreHd;
	}
	public String getAksesVaskularPreHd() {
		return aksesVaskularPreHd;
	}
	public void setAksesVaskularPreHd(String aksesVaskularPreHd) {
		this.aksesVaskularPreHd = aksesVaskularPreHd;
	}
	public String getSensoriumPostHd() {
		return sensoriumPostHd;
	}
	public void setSensoriumPostHd(String sensoriumPostHd) {
		this.sensoriumPostHd = sensoriumPostHd;
	}
	public String getTdPostHd() {
		return tdPostHd;
	}
	public void setTdPostHd(String tdPostHd) {
		this.tdPostHd = tdPostHd;
	}
	public String getNadiPostHd() {
		return nadiPostHd;
	}
	public void setNadiPostHd(String nadiPostHd) {
		this.nadiPostHd = nadiPostHd;
	}
	public String getNafasPostHd() {
		return nafasPostHd;
	}
	public void setNafasPostHd(String nafasPostHd) {
		this.nafasPostHd = nafasPostHd;
	}
	public String getSuhuPostHd() {
		return suhuPostHd;
	}
	public void setSuhuPostHd(String suhuPostHd) {
		this.suhuPostHd = suhuPostHd;
	}
	public String getBbSelesaiPostHd() {
		return bbSelesaiPostHd;
	}
	public void setBbSelesaiPostHd(String bbSelesaiPostHd) {
		this.bbSelesaiPostHd = bbSelesaiPostHd;
	}
	public String getBedaBbPostHd() {
		return bedaBbPostHd;
	}
	public void setBedaBbPostHd(String bedaBbPostHd) {
		this.bedaBbPostHd = bedaBbPostHd;
	}
	public String getCatatanPostHd() {
		return catatanPostHd;
	}
	public void setCatatanPostHd(String catatanPostHd) {
		this.catatanPostHd = catatanPostHd;
	}
	public Timestamp getWaktuMulai() {
		return waktuMulai;
	}
	public void setWaktuMulai(Timestamp waktuMulai) {
		this.waktuMulai = waktuMulai;
	}
	public BigDecimal getLamaHd() {
		return lamaHd;
	}
	public void setLamaHd(BigDecimal lamaHd) {
		this.lamaHd = lamaHd;
	}
	public Timestamp getWaktuSelesai() {
		return waktuSelesai;
	}
	public void setWaktuSelesai(Timestamp waktuSelesai) {
		this.waktuSelesai = waktuSelesai;
	}
	public String getPrimingMasuk() {
		return primingMasuk;
	}
	public void setPrimingMasuk(String primingMasuk) {
		this.primingMasuk = primingMasuk;
	}
	public String getPrimingKeluar() {
		return primingKeluar;
	}
	public void setPrimingKeluar(String primingKeluar) {
		this.primingKeluar = primingKeluar;
	}
	public String getAntiKoagulasi() {
		return antiKoagulasi;
	}
	public void setAntiKoagulasi(String antiKoagulasi) {
		this.antiKoagulasi = antiKoagulasi;
	}
	public String getAwalHd() {
		return awalHd;
	}
	public void setAwalHd(String awalHd) {
		this.awalHd = awalHd;
	}
	public String getMaintenance() {
		return maintenance;
	}
	public void setMaintenance(String maintenance) {
		this.maintenance = maintenance;
	}
	public String getTanpaHeparin() {
		return tanpaHeparin;
	}
	public void setTanpaHeparin(String tanpaHeparin) {
		this.tanpaHeparin = tanpaHeparin;
	}
	public String getUfgHd() {
		return ufgHd;
	}
	public void setUfgHd(String ufgHd) {
		this.ufgHd = ufgHd;
	}
	public String getResepHd() {
		return resepHd;
	}
	public void setResepHd(String resepHd) {
		this.resepHd = resepHd;
	}
	public String getDurasiHd() {
		return durasiHd;
	}
	public void setDurasiHd(String durasiHd) {
		this.durasiHd = durasiHd;
	}
	public String getQbInstruksiMedik() {
		return qbInstruksiMedik;
	}
	public void setQbInstruksiMedik(String qbInstruksiMedik) {
		this.qbInstruksiMedik = qbInstruksiMedik;
	}
	public String getQdInstruksiMedik() {
		return qdInstruksiMedik;
	}
	public void setQdInstruksiMedik(String qdInstruksiMedik) {
		this.qdInstruksiMedik = qdInstruksiMedik;
	}
	public String getTdInstruksiMedik() {
		return tdInstruksiMedik;
	}
	public void setTdInstruksiMedik(String tdInstruksiMedik) {
		this.tdInstruksiMedik = tdInstruksiMedik;
	}
	public String getUfgInstruksiMedik() {
		return ufgInstruksiMedik;
	}
	public void setUfgInstruksiMedik(String ufgInstruksiMedik) {
		this.ufgInstruksiMedik = ufgInstruksiMedik;
	}
	public String getUfrInstruksiMedik() {
		return ufrInstruksiMedik;
	}
	public void setUfrInstruksiMedik(String ufrInstruksiMedik) {
		this.ufrInstruksiMedik = ufrInstruksiMedik;
	}
	public String getProfillingInstruksiMedik() {
		return profillingInstruksiMedik;
	}
	public void setProfillingInstruksiMedik(String profillingInstruksiMedik) {
		this.profillingInstruksiMedik = profillingInstruksiMedik;
	}
	public String getAntikoagulasiInstruksiMedik() {
		return antikoagulasiInstruksiMedik;
	}
	public void setAntikoagulasiInstruksiMedik(String antikoagulasiInstruksiMedik) {
		this.antikoagulasiInstruksiMedik = antikoagulasiInstruksiMedik;
	}
	public String getNamaDokter() {
		return namaDokter;
	}
	public void setNamaDokter(String namaDokter) {
		this.namaDokter = namaDokter;
	}
	public String getPemeriksaanFisik() {
		return pemeriksaanFisik;
	}
	public void setPemeriksaanFisik(String pemeriksaanFisik) {
		this.pemeriksaanFisik = pemeriksaanFisik;
	}
	public String getKeadaanUmum() {
		return keadaanUmum;
	}
	public void setKeadaanUmum(String keadaanUmum) {
		this.keadaanUmum = keadaanUmum;
	}
	public String getKondisiSaatIni() {
		return kondisiSaatIni;
	}
	public void setKondisiSaatIni(String kondisiSaatIni) {
		this.kondisiSaatIni = kondisiSaatIni;
	}
	public String getKondisiSakit() {
		return kondisiSakit;
	}
	public void setKondisiSakit(String kondisiSakit) {
		this.kondisiSakit = kondisiSakit;
	}
	public BigDecimal getIndikatorSakit() {
		return indikatorSakit;
	}
	public void setIndikatorSakit(BigDecimal indikatorSakit) {
		this.indikatorSakit = indikatorSakit;
	}
	public String getDiagnosaKeperawatan() {
		return diagnosaKeperawatan;
	}
	public void setDiagnosaKeperawatan(String diagnosaKeperawatan) {
		this.diagnosaKeperawatan = diagnosaKeperawatan;
	}
	public String getIntervensiKeperawatan() {
		return intervensiKeperawatan;
	}
	public void setIntervensiKeperawatan(String intervensiKeperawatan) {
		this.intervensiKeperawatan = intervensiKeperawatan;
	}
	public String getIntervensiKolaborasi() {
		return intervensiKolaborasi;
	}
	public void setIntervensiKolaborasi(String intervensiKolaborasi) {
		this.intervensiKolaborasi = intervensiKolaborasi;
	}
	public String getCatatanKeperawatan() {
		return catatanKeperawatan;
	}
	public void setCatatanKeperawatan(String catatanKeperawatan) {
		this.catatanKeperawatan = catatanKeperawatan;
	}
	public String getNamaPerawat() {
		return namaPerawat;
	}
	public void setNamaPerawat(String namaPerawat) {
		this.namaPerawat = namaPerawat;
	}
	public Timestamp getWaktuCatatanPerkembangan() {
		return waktuCatatanPerkembangan;
	}
	public void setWaktuCatatanPerkembangan(Timestamp waktuCatatanPerkembangan) {
		this.waktuCatatanPerkembangan = waktuCatatanPerkembangan;
	}
	public String getUtkDokter() {
		return utkDokter;
	}
	public void setUtkDokter(String utkDokter) {
		this.utkDokter = utkDokter;
	}
	public String getUtkStaff() {
		return utkStaff;
	}
	public void setUtkStaff(String utkStaff) {
		this.utkStaff = utkStaff;
	}
	public String getTtdPerawat() {
		return ttdPerawat;
	}
	public void setTtdPerawat(String ttdPerawat) {
		this.ttdPerawat = ttdPerawat;
	}
	public String getRekomendasiGizi() {
		return rekomendasiGizi;
	}
	public void setRekomendasiGizi(String rekomendasiGizi) {
		this.rekomendasiGizi = rekomendasiGizi;
	}
	public String getKaloriGizi() {
		return kaloriGizi;
	}
	public void setKaloriGizi(String kaloriGizi) {
		this.kaloriGizi = kaloriGizi;
	}
	public String getProteinGizi() {
		return proteinGizi;
	}
	public void setProteinGizi(String proteinGizi) {
		this.proteinGizi = proteinGizi;
	}
	public String getPengkajianGizi() {
		return pengkajianGizi;
	}
	public void setPengkajianGizi(String pengkajianGizi) {
		this.pengkajianGizi = pengkajianGizi;
	}
	public String getEdukasiMateri() {
		return edukasiMateri;
	}
	public void setEdukasiMateri(String edukasiMateri) {
		this.edukasiMateri = edukasiMateri;
	}
	public String getLeafletBrosur() {
		return leafletBrosur;
	}
	public void setLeafletBrosur(String leafletBrosur) {
		this.leafletBrosur = leafletBrosur;
	}
	public String getRencanaKedepan() {
		return rencanaKedepan;
	}
	public void setRencanaKedepan(String rencanaKedepan) {
		this.rencanaKedepan = rencanaKedepan;
	}
	public String getNamaMesinHd() {
		return namaMesinHd;
	}
	public void setNamaMesinHd(String namaMesinHd) {
		this.namaMesinHd = namaMesinHd;
	}
	public String getDialyzerHd() {
		return dialyzerHd;
	}
	public void setDialyzerHd(String dialyzerHd) {
		this.dialyzerHd = dialyzerHd;
	}
	public BigDecimal getDesinfectansSebelumHd() {
		return desinfectansSebelumHd;
	}
	public void setDesinfectansSebelumHd(BigDecimal desinfectansSebelumHd) {
		this.desinfectansSebelumHd = desinfectansSebelumHd;
	}
	public BigDecimal getRinserSebelumHD() {
		return rinserSebelumHD;
	}
	public void setRinserSebelumHD(BigDecimal rinserSebelumHD) {
		this.rinserSebelumHD = rinserSebelumHD;
	}
	public String getJenisHd() {
		return jenisHd;
	}
	public void setJenisHd(String jenisHd) {
		this.jenisHd = jenisHd;
	}
	public String getKomplikasiHd() {
		return komplikasiHd;
	}
	public void setKomplikasiHd(String komplikasiHd) {
		this.komplikasiHd = komplikasiHd;
	}
	public String getConductivitySebelumHd() {
		return conductivitySebelumHd;
	}
	public void setConductivitySebelumHd(String conductivitySebelumHd) {
		this.conductivitySebelumHd = conductivitySebelumHd;
	}
	public String getTemperaturSebelumHd() {
		return temperaturSebelumHd;
	}
	public void setTemperaturSebelumHd(String temperaturSebelumHd) {
		this.temperaturSebelumHd = temperaturSebelumHd;
	}
	public String getDialisatSebelumHd() {
		return dialisatSebelumHd;
	}
	public void setDialisatSebelumHd(String dialisatSebelumHd) {
		this.dialisatSebelumHd = dialisatSebelumHd;
	}
	public String getSisaPriming() {
		return sisaPriming;
	}
	public void setSisaPriming(String sisaPriming) {
		this.sisaPriming = sisaPriming;
	}
	public String getDripMasuk() {
		return dripMasuk;
	}
	public void setDripMasuk(String dripMasuk) {
		this.dripMasuk = dripMasuk;
	}
	public String getDarahMasuk() {
		return darahMasuk;
	}
	public void setDarahMasuk(String darahMasuk) {
		this.darahMasuk = darahMasuk;
	}
	public String getWashOut() {
		return washOut;
	}
	public void setWashOut(String washOut) {
		this.washOut = washOut;
	}
	public String getMinumMasuk() {
		return minumMasuk;
	}
	public void setMinumMasuk(String minumMasuk) {
		this.minumMasuk = minumMasuk;
	}
	public String getJumlahMasuk() {
		return jumlahMasuk;
	}
	public void setJumlahMasuk(String jumlahMasuk) {
		this.jumlahMasuk = jumlahMasuk;
	}
	public BigDecimal getPenekananMasuk() {
		return penekananMasuk;
	}
	public void setPenekananMasuk(BigDecimal penekananMasuk) {
		this.penekananMasuk = penekananMasuk;
	}
	public BigDecimal getDesinfectantsSesudahHd() {
		return desinfectantsSesudahHd;
	}
	public void setDesinfectantsSesudahHd(BigDecimal desinfectantsSesudahHd) {
		this.desinfectantsSesudahHd = desinfectantsSesudahHd;
	}
	public BigDecimal getRinseSesudahHd() {
		return rinseSesudahHd;
	}
	public void setRinseSesudahHd(BigDecimal rinseSesudahHd) {
		this.rinseSesudahHd = rinseSesudahHd;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Timestamp createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public BigDecimal getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Timestamp getLastUpdatedDateTime() {
		return lastUpdatedDateTime;
	}
	public void setLastUpdatedDateTime(Timestamp lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
	}
	public String getDefunctInd() {
		return defunctInd;
	}
	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}
	public BigDecimal getCardNo() {
		return cardNo;
	}
	public void setCardNo(BigDecimal cardNo) {
		this.cardNo = cardNo;
	}
	public Timestamp getTanggal() {
		return tanggal;
	}
	public void setTanggal(Timestamp tanggal) {
		this.tanggal = tanggal;
	}
	public String getTanggalString() {
		return tanggalString;
	}
	public void setTanggalString(String tanggalString) {
		this.tanggalString = tanggalString;
	}
}
