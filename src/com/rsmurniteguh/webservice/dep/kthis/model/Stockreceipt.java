package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKRECEIPT database table.
 * 
 */
@Entity
@NamedQuery(name="Stockreceipt.findAll", query="SELECT s FROM Stockreceipt s")
public class Stockreceipt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKRECEIPT_ID")
	private long stockreceiptId;

	@Column(name="ACCOUNTCLOSING_BY")
	private BigDecimal accountclosingBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ACCOUNTCLOSING_DATETIME")
	private Date accountclosingDatetime;

	@Column(name="APPROVED_BY")
	private BigDecimal approvedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="APPROVED_DATETIME")
	private Date approvedDatetime;

	@Column(name="AUDIT_CANCELLED_BY")
	private BigDecimal auditCancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="AUDIT_CANCELLED_DATETIME")
	private Date auditCancelledDatetime;

	@Column(name="AUDIT_STATUS")
	private String auditStatus;

	@Column(name="AUDITED_BY")
	private BigDecimal auditedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="AUDITED_DATETIME")
	private Date auditedDatetime;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="RECEIPT_DATETIME")
	private Date receiptDatetime;

	@Column(name="RECEIPT_ITEM_TYPE")
	private String receiptItemType;

	@Column(name="RECEIPT_NO")
	private String receiptNo;

	@Column(name="RECEIPT_STATUS")
	private String receiptStatus;

	@Column(name="RECEIPT_TYPE")
	private String receiptType;

	private String remarks;

	@Column(name="STOCKISSUE_ID")
	private BigDecimal stockissueId;

	@Column(name="STOREMSTR_ID")
	private BigDecimal storemstrId;

	@Column(name="VENDORMSTR_ID")
	private BigDecimal vendormstrId;

	//bi-directional many-to-one association to Stockissue
	@OneToMany(mappedBy="stockreceipt")
	private List<Stockissue> stockissues;

	//bi-directional many-to-one association to Stockreceiptdetail
	@OneToMany(mappedBy="stockreceipt")
	private List<Stockreceiptdetail> stockreceiptdetails;

	public Stockreceipt() {
	}

	public long getStockreceiptId() {
		return this.stockreceiptId;
	}

	public void setStockreceiptId(long stockreceiptId) {
		this.stockreceiptId = stockreceiptId;
	}

	public BigDecimal getAccountclosingBy() {
		return this.accountclosingBy;
	}

	public void setAccountclosingBy(BigDecimal accountclosingBy) {
		this.accountclosingBy = accountclosingBy;
	}

	public Date getAccountclosingDatetime() {
		return this.accountclosingDatetime;
	}

	public void setAccountclosingDatetime(Date accountclosingDatetime) {
		this.accountclosingDatetime = accountclosingDatetime;
	}

	public BigDecimal getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(BigDecimal approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedDatetime() {
		return this.approvedDatetime;
	}

	public void setApprovedDatetime(Date approvedDatetime) {
		this.approvedDatetime = approvedDatetime;
	}

	public BigDecimal getAuditCancelledBy() {
		return this.auditCancelledBy;
	}

	public void setAuditCancelledBy(BigDecimal auditCancelledBy) {
		this.auditCancelledBy = auditCancelledBy;
	}

	public Date getAuditCancelledDatetime() {
		return this.auditCancelledDatetime;
	}

	public void setAuditCancelledDatetime(Date auditCancelledDatetime) {
		this.auditCancelledDatetime = auditCancelledDatetime;
	}

	public String getAuditStatus() {
		return this.auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}

	public BigDecimal getAuditedBy() {
		return this.auditedBy;
	}

	public void setAuditedBy(BigDecimal auditedBy) {
		this.auditedBy = auditedBy;
	}

	public Date getAuditedDatetime() {
		return this.auditedDatetime;
	}

	public void setAuditedDatetime(Date auditedDatetime) {
		this.auditedDatetime = auditedDatetime;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Date getReceiptDatetime() {
		return this.receiptDatetime;
	}

	public void setReceiptDatetime(Date receiptDatetime) {
		this.receiptDatetime = receiptDatetime;
	}

	public String getReceiptItemType() {
		return this.receiptItemType;
	}

	public void setReceiptItemType(String receiptItemType) {
		this.receiptItemType = receiptItemType;
	}

	public String getReceiptNo() {
		return this.receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getReceiptStatus() {
		return this.receiptStatus;
	}

	public void setReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}

	public String getReceiptType() {
		return this.receiptType;
	}

	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStockissueId() {
		return this.stockissueId;
	}

	public void setStockissueId(BigDecimal stockissueId) {
		this.stockissueId = stockissueId;
	}

	public BigDecimal getStoremstrId() {
		return this.storemstrId;
	}

	public void setStoremstrId(BigDecimal storemstrId) {
		this.storemstrId = storemstrId;
	}

	public BigDecimal getVendormstrId() {
		return this.vendormstrId;
	}

	public void setVendormstrId(BigDecimal vendormstrId) {
		this.vendormstrId = vendormstrId;
	}

	public List<Stockissue> getStockissues() {
		return this.stockissues;
	}

	public void setStockissues(List<Stockissue> stockissues) {
		this.stockissues = stockissues;
	}

	public Stockissue addStockissue(Stockissue stockissue) {
		getStockissues().add(stockissue);
		stockissue.setStockreceipt(this);

		return stockissue;
	}

	public Stockissue removeStockissue(Stockissue stockissue) {
		getStockissues().remove(stockissue);
		stockissue.setStockreceipt(null);

		return stockissue;
	}

	public List<Stockreceiptdetail> getStockreceiptdetails() {
		return this.stockreceiptdetails;
	}

	public void setStockreceiptdetails(List<Stockreceiptdetail> stockreceiptdetails) {
		this.stockreceiptdetails = stockreceiptdetails;
	}

	public Stockreceiptdetail addStockreceiptdetail(Stockreceiptdetail stockreceiptdetail) {
		getStockreceiptdetails().add(stockreceiptdetail);
		stockreceiptdetail.setStockreceipt(this);

		return stockreceiptdetail;
	}

	public Stockreceiptdetail removeStockreceiptdetail(Stockreceiptdetail stockreceiptdetail) {
		getStockreceiptdetails().remove(stockreceiptdetail);
		stockreceiptdetail.setStockreceipt(null);

		return stockreceiptdetail;
	}

}