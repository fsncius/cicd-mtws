package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CARD database table.
 * 
 */
@Entity
@NamedQuery(name="Card.findAll", query="SELECT c FROM Card c")
public class Card implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CARD_ID")
	private BigDecimal cardId;

	@Column(name="CARD_NO")
	private String cardNo;

	@Column(name="CARD_PASSWORD")
	private String cardPassword;

	@Column(name="CARD_STATUS")
	private String cardStatus;

	@Column(name="CARD_TYPE")
	private String cardType;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Timestamp lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="STAKEHOLDERACCOUNTTXN_ID")
	private BigDecimal stakeholderaccounttxnId;

	//bi-directional many-to-one association to Person
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PERSON_ID")
	private Person person;
	
	private BigDecimal personId;

	//bi-directional many-to-one association to Cardstatushistory
	@OneToMany(mappedBy="card")
	private List<Cardstatushistory> cardstatushistories;

	public Card() {
	}

	public BigDecimal getCardId() {
		return this.cardId;
	}

	public void setCardId(BigDecimal cardId) {
		this.cardId = cardId;
	}

	public String getCardNo() {
		return this.cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardPassword() {
		return this.cardPassword;
	}

	public void setCardPassword(String cardPassword) {
		this.cardPassword = cardPassword;
	}

	public String getCardStatus() {
		return this.cardStatus;
	}

	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}

	public String getCardType() {
		return this.cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Timestamp getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Timestamp lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStakeholderaccounttxnId() {
		return this.stakeholderaccounttxnId;
	}

	public void setStakeholderaccounttxnId(BigDecimal stakeholderaccounttxnId) {
		this.stakeholderaccounttxnId = stakeholderaccounttxnId;
	}

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public List<Cardstatushistory> getCardstatushistories() {
		return this.cardstatushistories;
	}

	public void setCardstatushistories(List<Cardstatushistory> cardstatushistories) {
		this.cardstatushistories = cardstatushistories;
	}

	public Cardstatushistory addCardstatushistory(Cardstatushistory cardstatushistory) {
		getCardstatushistories().add(cardstatushistory);
		cardstatushistory.setCard(this);

		return cardstatushistory;
	}

	public Cardstatushistory removeCardstatushistory(Cardstatushistory cardstatushistory) {
		getCardstatushistories().remove(cardstatushistory);
		cardstatushistory.setCard(null);

		return cardstatushistory;
	}

	public BigDecimal getPersonId() {
		return personId;
	}

	public void setPersonId(BigDecimal personId) {
		this.personId = personId;
	}

}