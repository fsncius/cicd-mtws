package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the SUBSPECIALTYMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Subspecialtymstr.findAll", query="SELECT s FROM Subspecialtymstr s")
public class Subspecialtymstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SUBSPECIALTYMSTR_ID")
	private long subspecialtymstrId;

	@Column(name="CN_REVENUECENTREMSTR_ID")
	private BigDecimal cnRevenuecentremstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="LOCATIONMSTR_ID")
	private BigDecimal locationmstrId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="RESOURCEMSTR_ID")
	private BigDecimal resourcemstrId;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="SPECIALTYMSTR_ID")
	private BigDecimal specialtymstrId;

	@Column(name="SUBSPECIALTY_CODE")
	private String subspecialtyCode;

	@Column(name="SUBSPECIALTY_DESC")
	private String subspecialtyDesc;

	@Column(name="SUBSPECIALTY_DESC_LANG1")
	private String subspecialtyDescLang1;

	@Column(name="SUBSPECIALTY_DESC_LANG2")
	private String subspecialtyDescLang2;

	@Column(name="SUBSPECIALTY_DESC_LANG3")
	private String subspecialtyDescLang3;

	@Column(name="SUBSPECIALTY_TYPE")
	private String subspecialtyType;

	//bi-directional many-to-one association to Adtdailysummary
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Adtdailysummary> adtdailysummaries;

	//bi-directional many-to-one association to Bedhistory
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Bedhistory> bedhistories;

	//bi-directional many-to-one association to Bedmstr
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Bedmstr> bedmstrs;

	//bi-directional many-to-one association to Careprovider
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Careprovider> careproviders;

	//bi-directional many-to-one association to Charge
	@OneToMany(mappedBy="subspecialtymstr1")
	private List<Charge> charges1;

	//bi-directional many-to-one association to Charge
	@OneToMany(mappedBy="subspecialtymstr2")
	private List<Charge> charges2;

	//bi-directional many-to-one association to Chargeentry
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Chargeentry> chargeentries;

	//bi-directional many-to-one association to Chargematerialcomp
	@OneToMany(mappedBy="subspecialtymstr1")
	private List<Chargematerialcomp> chargematerialcomps1;

	//bi-directional many-to-one association to Chargematerialcomp
	@OneToMany(mappedBy="subspecialtymstr2")
	private List<Chargematerialcomp> chargematerialcomps2;

	//bi-directional many-to-one association to Chargerollingplan
	@OneToMany(mappedBy="subspecialtymstr1")
	private List<Chargerollingplan> chargerollingplans1;

	//bi-directional many-to-one association to Chargerollingplan
	@OneToMany(mappedBy="subspecialtymstr2")
	private List<Chargerollingplan> chargerollingplans2;

	//bi-directional many-to-one association to Consultationorderitemmstr
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Consultationorderitemmstr> consultationorderitemmstrs;

	//bi-directional many-to-one association to Consultationrequisition
	@OneToMany(mappedBy="subspecialtymstr1")
	private List<Consultationrequisition> consultationrequisitions1;

	//bi-directional many-to-one association to Consultationrequisition
	@OneToMany(mappedBy="subspecialtymstr2")
	private List<Consultationrequisition> consultationrequisitions2;

	//bi-directional many-to-one association to Consultationrequisition
	@OneToMany(mappedBy="subspecialtymstr3")
	private List<Consultationrequisition> consultationrequisitions3;

	//bi-directional many-to-one association to Countedpatdailysummary
	@OneToMany(mappedBy="subspecialtymstr1")
	private List<Countedpatdailysummary> countedpatdailysummaries1;

	//bi-directional many-to-one association to Countedpatdailysummary
	@OneToMany(mappedBy="subspecialtymstr2")
	private List<Countedpatdailysummary> countedpatdailysummaries2;

	//bi-directional many-to-one association to Diagnosisincommonusemstr
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Diagnosisincommonusemstr> diagnosisincommonusemstrs;


	//bi-directional many-to-one association to Materialconsumption
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Materialconsumption> materialconsumptions;

	//bi-directional many-to-one association to Regndailysummary
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Regndailysummary> regndailysummaries;

	//bi-directional many-to-one association to Regnlocationlimit
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Regnlocationlimit> regnlocationlimits;

	//bi-directional many-to-one association to Regntypecharge
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Regntypecharge> regntypecharges;

	//bi-directional many-to-one association to Subspecialtywardrequest
	@OneToMany(mappedBy="subspecialtymstr1")
	private List<Subspecialtywardrequest> subspecialtywardrequests1;

	//bi-directional many-to-one association to Subspecialtywardrequest
	@OneToMany(mappedBy="subspecialtymstr2")
	private List<Subspecialtywardrequest> subspecialtywardrequests2;

	//bi-directional many-to-one association to Visit
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Visit> visits;

	//bi-directional many-to-one association to Visitappointment
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Visitappointment> visitappointments;

	//bi-directional many-to-one association to Visitcasenote
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Visitcasenote> visitcasenotes;

	//bi-directional many-to-one association to Visitdailysummary
	@OneToMany(mappedBy="subspecialtymstr1")
	private List<Visitdailysummary> visitdailysummaries1;

	//bi-directional many-to-one association to Visitdailysummary
	@OneToMany(mappedBy="subspecialtymstr2")
	private List<Visitdailysummary> visitdailysummaries2;

	//bi-directional many-to-one association to Visitdailysummarydetail
	@OneToMany(mappedBy="subspecialtymstr1")
	private List<Visitdailysummarydetail> visitdailysummarydetails1;

	//bi-directional many-to-one association to Visitdailysummarydetail
	@OneToMany(mappedBy="subspecialtymstr2")
	private List<Visitdailysummarydetail> visitdailysummarydetails2;

	//bi-directional many-to-one association to Visitdiagnosi
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Visitdiagnosi> visitdiagnosis;

	//bi-directional many-to-one association to Visitdoctor
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Visitdoctor> visitdoctors;

	//bi-directional many-to-one association to Visitreferral
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Visitreferral> visitreferrals;

	//bi-directional many-to-one association to Visitregntype
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Visitregntype> visitregntypes;

	//bi-directional many-to-one association to Visitsubspecialtyhistory
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Visitsubspecialtyhistory> visitsubspecialtyhistories;

	//bi-directional many-to-one association to Wardsubspecialty
	@OneToMany(mappedBy="subspecialtymstr")
	private List<Wardsubspecialty> wardsubspecialties;

	public Subspecialtymstr() {
	}

	public long getSubspecialtymstrId() {
		return this.subspecialtymstrId;
	}

	public void setSubspecialtymstrId(long subspecialtymstrId) {
		this.subspecialtymstrId = subspecialtymstrId;
	}

	public BigDecimal getCnRevenuecentremstrId() {
		return this.cnRevenuecentremstrId;
	}

	public void setCnRevenuecentremstrId(BigDecimal cnRevenuecentremstrId) {
		this.cnRevenuecentremstrId = cnRevenuecentremstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getLocationmstrId() {
		return this.locationmstrId;
	}

	public void setLocationmstrId(BigDecimal locationmstrId) {
		this.locationmstrId = locationmstrId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getResourcemstrId() {
		return this.resourcemstrId;
	}

	public void setResourcemstrId(BigDecimal resourcemstrId) {
		this.resourcemstrId = resourcemstrId;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public BigDecimal getSpecialtymstrId() {
		return this.specialtymstrId;
	}

	public void setSpecialtymstrId(BigDecimal specialtymstrId) {
		this.specialtymstrId = specialtymstrId;
	}

	public String getSubspecialtyCode() {
		return this.subspecialtyCode;
	}

	public void setSubspecialtyCode(String subspecialtyCode) {
		this.subspecialtyCode = subspecialtyCode;
	}

	public String getSubspecialtyDesc() {
		return this.subspecialtyDesc;
	}

	public void setSubspecialtyDesc(String subspecialtyDesc) {
		this.subspecialtyDesc = subspecialtyDesc;
	}

	public String getSubspecialtyDescLang1() {
		return this.subspecialtyDescLang1;
	}

	public void setSubspecialtyDescLang1(String subspecialtyDescLang1) {
		this.subspecialtyDescLang1 = subspecialtyDescLang1;
	}

	public String getSubspecialtyDescLang2() {
		return this.subspecialtyDescLang2;
	}

	public void setSubspecialtyDescLang2(String subspecialtyDescLang2) {
		this.subspecialtyDescLang2 = subspecialtyDescLang2;
	}

	public String getSubspecialtyDescLang3() {
		return this.subspecialtyDescLang3;
	}

	public void setSubspecialtyDescLang3(String subspecialtyDescLang3) {
		this.subspecialtyDescLang3 = subspecialtyDescLang3;
	}

	public String getSubspecialtyType() {
		return this.subspecialtyType;
	}

	public void setSubspecialtyType(String subspecialtyType) {
		this.subspecialtyType = subspecialtyType;
	}

	public List<Adtdailysummary> getAdtdailysummaries() {
		return this.adtdailysummaries;
	}

	public void setAdtdailysummaries(List<Adtdailysummary> adtdailysummaries) {
		this.adtdailysummaries = adtdailysummaries;
	}

	public Adtdailysummary addAdtdailysummary(Adtdailysummary adtdailysummary) {
		getAdtdailysummaries().add(adtdailysummary);
		adtdailysummary.setSubspecialtymstr(this);

		return adtdailysummary;
	}

	public Adtdailysummary removeAdtdailysummary(Adtdailysummary adtdailysummary) {
		getAdtdailysummaries().remove(adtdailysummary);
		adtdailysummary.setSubspecialtymstr(null);

		return adtdailysummary;
	}

	public List<Bedhistory> getBedhistories() {
		return this.bedhistories;
	}

	public void setBedhistories(List<Bedhistory> bedhistories) {
		this.bedhistories = bedhistories;
	}

	public Bedhistory addBedhistory(Bedhistory bedhistory) {
		getBedhistories().add(bedhistory);
		bedhistory.setSubspecialtymstr(this);

		return bedhistory;
	}

	public Bedhistory removeBedhistory(Bedhistory bedhistory) {
		getBedhistories().remove(bedhistory);
		bedhistory.setSubspecialtymstr(null);

		return bedhistory;
	}

	public List<Bedmstr> getBedmstrs() {
		return this.bedmstrs;
	}

	public void setBedmstrs(List<Bedmstr> bedmstrs) {
		this.bedmstrs = bedmstrs;
	}

	public Bedmstr addBedmstr(Bedmstr bedmstr) {
		getBedmstrs().add(bedmstr);
		bedmstr.setSubspecialtymstr(this);

		return bedmstr;
	}

	public Bedmstr removeBedmstr(Bedmstr bedmstr) {
		getBedmstrs().remove(bedmstr);
		bedmstr.setSubspecialtymstr(null);

		return bedmstr;
	}

	public List<Careprovider> getCareproviders() {
		return this.careproviders;
	}

	public void setCareproviders(List<Careprovider> careproviders) {
		this.careproviders = careproviders;
	}

	public Careprovider addCareprovider(Careprovider careprovider) {
		getCareproviders().add(careprovider);
		careprovider.setSubspecialtymstr(this);

		return careprovider;
	}

	public Careprovider removeCareprovider(Careprovider careprovider) {
		getCareproviders().remove(careprovider);
		careprovider.setSubspecialtymstr(null);

		return careprovider;
	}

	public List<Charge> getCharges1() {
		return this.charges1;
	}

	public void setCharges1(List<Charge> charges1) {
		this.charges1 = charges1;
	}

	public Charge addCharges1(Charge charges1) {
		getCharges1().add(charges1);
		charges1.setSubspecialtymstr1(this);

		return charges1;
	}

	public Charge removeCharges1(Charge charges1) {
		getCharges1().remove(charges1);
		charges1.setSubspecialtymstr1(null);

		return charges1;
	}

	public List<Charge> getCharges2() {
		return this.charges2;
	}

	public void setCharges2(List<Charge> charges2) {
		this.charges2 = charges2;
	}

	public Charge addCharges2(Charge charges2) {
		getCharges2().add(charges2);
		charges2.setSubspecialtymstr2(this);

		return charges2;
	}

	public Charge removeCharges2(Charge charges2) {
		getCharges2().remove(charges2);
		charges2.setSubspecialtymstr2(null);

		return charges2;
	}

	public List<Chargeentry> getChargeentries() {
		return this.chargeentries;
	}

	public void setChargeentries(List<Chargeentry> chargeentries) {
		this.chargeentries = chargeentries;
	}

	public Chargeentry addChargeentry(Chargeentry chargeentry) {
		getChargeentries().add(chargeentry);
		chargeentry.setSubspecialtymstr(this);

		return chargeentry;
	}

	public Chargeentry removeChargeentry(Chargeentry chargeentry) {
		getChargeentries().remove(chargeentry);
		chargeentry.setSubspecialtymstr(null);

		return chargeentry;
	}

	public List<Chargematerialcomp> getChargematerialcomps1() {
		return this.chargematerialcomps1;
	}

	public void setChargematerialcomps1(List<Chargematerialcomp> chargematerialcomps1) {
		this.chargematerialcomps1 = chargematerialcomps1;
	}

	public Chargematerialcomp addChargematerialcomps1(Chargematerialcomp chargematerialcomps1) {
		getChargematerialcomps1().add(chargematerialcomps1);
		chargematerialcomps1.setSubspecialtymstr1(this);

		return chargematerialcomps1;
	}

	public Chargematerialcomp removeChargematerialcomps1(Chargematerialcomp chargematerialcomps1) {
		getChargematerialcomps1().remove(chargematerialcomps1);
		chargematerialcomps1.setSubspecialtymstr1(null);

		return chargematerialcomps1;
	}

	public List<Chargematerialcomp> getChargematerialcomps2() {
		return this.chargematerialcomps2;
	}

	public void setChargematerialcomps2(List<Chargematerialcomp> chargematerialcomps2) {
		this.chargematerialcomps2 = chargematerialcomps2;
	}

	public Chargematerialcomp addChargematerialcomps2(Chargematerialcomp chargematerialcomps2) {
		getChargematerialcomps2().add(chargematerialcomps2);
		chargematerialcomps2.setSubspecialtymstr2(this);

		return chargematerialcomps2;
	}

	public Chargematerialcomp removeChargematerialcomps2(Chargematerialcomp chargematerialcomps2) {
		getChargematerialcomps2().remove(chargematerialcomps2);
		chargematerialcomps2.setSubspecialtymstr2(null);

		return chargematerialcomps2;
	}

	public List<Chargerollingplan> getChargerollingplans1() {
		return this.chargerollingplans1;
	}

	public void setChargerollingplans1(List<Chargerollingplan> chargerollingplans1) {
		this.chargerollingplans1 = chargerollingplans1;
	}

	public Chargerollingplan addChargerollingplans1(Chargerollingplan chargerollingplans1) {
		getChargerollingplans1().add(chargerollingplans1);
		chargerollingplans1.setSubspecialtymstr1(this);

		return chargerollingplans1;
	}

	public Chargerollingplan removeChargerollingplans1(Chargerollingplan chargerollingplans1) {
		getChargerollingplans1().remove(chargerollingplans1);
		chargerollingplans1.setSubspecialtymstr1(null);

		return chargerollingplans1;
	}

	public List<Chargerollingplan> getChargerollingplans2() {
		return this.chargerollingplans2;
	}

	public void setChargerollingplans2(List<Chargerollingplan> chargerollingplans2) {
		this.chargerollingplans2 = chargerollingplans2;
	}

	public Chargerollingplan addChargerollingplans2(Chargerollingplan chargerollingplans2) {
		getChargerollingplans2().add(chargerollingplans2);
		chargerollingplans2.setSubspecialtymstr2(this);

		return chargerollingplans2;
	}

	public Chargerollingplan removeChargerollingplans2(Chargerollingplan chargerollingplans2) {
		getChargerollingplans2().remove(chargerollingplans2);
		chargerollingplans2.setSubspecialtymstr2(null);

		return chargerollingplans2;
	}

	public List<Consultationorderitemmstr> getConsultationorderitemmstrs() {
		return this.consultationorderitemmstrs;
	}

	public void setConsultationorderitemmstrs(List<Consultationorderitemmstr> consultationorderitemmstrs) {
		this.consultationorderitemmstrs = consultationorderitemmstrs;
	}

	public Consultationorderitemmstr addConsultationorderitemmstr(Consultationorderitemmstr consultationorderitemmstr) {
		getConsultationorderitemmstrs().add(consultationorderitemmstr);
		consultationorderitemmstr.setSubspecialtymstr(this);

		return consultationorderitemmstr;
	}

	public Consultationorderitemmstr removeConsultationorderitemmstr(Consultationorderitemmstr consultationorderitemmstr) {
		getConsultationorderitemmstrs().remove(consultationorderitemmstr);
		consultationorderitemmstr.setSubspecialtymstr(null);

		return consultationorderitemmstr;
	}

	public List<Consultationrequisition> getConsultationrequisitions1() {
		return this.consultationrequisitions1;
	}

	public void setConsultationrequisitions1(List<Consultationrequisition> consultationrequisitions1) {
		this.consultationrequisitions1 = consultationrequisitions1;
	}

	public Consultationrequisition addConsultationrequisitions1(Consultationrequisition consultationrequisitions1) {
		getConsultationrequisitions1().add(consultationrequisitions1);
		consultationrequisitions1.setSubspecialtymstr1(this);

		return consultationrequisitions1;
	}

	public Consultationrequisition removeConsultationrequisitions1(Consultationrequisition consultationrequisitions1) {
		getConsultationrequisitions1().remove(consultationrequisitions1);
		consultationrequisitions1.setSubspecialtymstr1(null);

		return consultationrequisitions1;
	}

	public List<Consultationrequisition> getConsultationrequisitions2() {
		return this.consultationrequisitions2;
	}

	public void setConsultationrequisitions2(List<Consultationrequisition> consultationrequisitions2) {
		this.consultationrequisitions2 = consultationrequisitions2;
	}

	public Consultationrequisition addConsultationrequisitions2(Consultationrequisition consultationrequisitions2) {
		getConsultationrequisitions2().add(consultationrequisitions2);
		consultationrequisitions2.setSubspecialtymstr2(this);

		return consultationrequisitions2;
	}

	public Consultationrequisition removeConsultationrequisitions2(Consultationrequisition consultationrequisitions2) {
		getConsultationrequisitions2().remove(consultationrequisitions2);
		consultationrequisitions2.setSubspecialtymstr2(null);

		return consultationrequisitions2;
	}

	public List<Consultationrequisition> getConsultationrequisitions3() {
		return this.consultationrequisitions3;
	}

	public void setConsultationrequisitions3(List<Consultationrequisition> consultationrequisitions3) {
		this.consultationrequisitions3 = consultationrequisitions3;
	}

	public Consultationrequisition addConsultationrequisitions3(Consultationrequisition consultationrequisitions3) {
		getConsultationrequisitions3().add(consultationrequisitions3);
		consultationrequisitions3.setSubspecialtymstr3(this);

		return consultationrequisitions3;
	}

	public Consultationrequisition removeConsultationrequisitions3(Consultationrequisition consultationrequisitions3) {
		getConsultationrequisitions3().remove(consultationrequisitions3);
		consultationrequisitions3.setSubspecialtymstr3(null);

		return consultationrequisitions3;
	}

	public List<Countedpatdailysummary> getCountedpatdailysummaries1() {
		return this.countedpatdailysummaries1;
	}

	public void setCountedpatdailysummaries1(List<Countedpatdailysummary> countedpatdailysummaries1) {
		this.countedpatdailysummaries1 = countedpatdailysummaries1;
	}

	public Countedpatdailysummary addCountedpatdailysummaries1(Countedpatdailysummary countedpatdailysummaries1) {
		getCountedpatdailysummaries1().add(countedpatdailysummaries1);
		countedpatdailysummaries1.setSubspecialtymstr1(this);

		return countedpatdailysummaries1;
	}

	public Countedpatdailysummary removeCountedpatdailysummaries1(Countedpatdailysummary countedpatdailysummaries1) {
		getCountedpatdailysummaries1().remove(countedpatdailysummaries1);
		countedpatdailysummaries1.setSubspecialtymstr1(null);

		return countedpatdailysummaries1;
	}

	public List<Countedpatdailysummary> getCountedpatdailysummaries2() {
		return this.countedpatdailysummaries2;
	}

	public void setCountedpatdailysummaries2(List<Countedpatdailysummary> countedpatdailysummaries2) {
		this.countedpatdailysummaries2 = countedpatdailysummaries2;
	}

	public Countedpatdailysummary addCountedpatdailysummaries2(Countedpatdailysummary countedpatdailysummaries2) {
		getCountedpatdailysummaries2().add(countedpatdailysummaries2);
		countedpatdailysummaries2.setSubspecialtymstr2(this);

		return countedpatdailysummaries2;
	}

	public Countedpatdailysummary removeCountedpatdailysummaries2(Countedpatdailysummary countedpatdailysummaries2) {
		getCountedpatdailysummaries2().remove(countedpatdailysummaries2);
		countedpatdailysummaries2.setSubspecialtymstr2(null);

		return countedpatdailysummaries2;
	}

	public List<Diagnosisincommonusemstr> getDiagnosisincommonusemstrs() {
		return this.diagnosisincommonusemstrs;
	}

	public void setDiagnosisincommonusemstrs(List<Diagnosisincommonusemstr> diagnosisincommonusemstrs) {
		this.diagnosisincommonusemstrs = diagnosisincommonusemstrs;
	}

	public Diagnosisincommonusemstr addDiagnosisincommonusemstr(Diagnosisincommonusemstr diagnosisincommonusemstr) {
		getDiagnosisincommonusemstrs().add(diagnosisincommonusemstr);
		diagnosisincommonusemstr.setSubspecialtymstr(this);

		return diagnosisincommonusemstr;
	}

	public Diagnosisincommonusemstr removeDiagnosisincommonusemstr(Diagnosisincommonusemstr diagnosisincommonusemstr) {
		getDiagnosisincommonusemstrs().remove(diagnosisincommonusemstr);
		diagnosisincommonusemstr.setSubspecialtymstr(null);

		return diagnosisincommonusemstr;
	}

	public List<Materialconsumption> getMaterialconsumptions() {
		return this.materialconsumptions;
	}

	public void setMaterialconsumptions(List<Materialconsumption> materialconsumptions) {
		this.materialconsumptions = materialconsumptions;
	}

	public Materialconsumption addMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().add(materialconsumption);
		materialconsumption.setSubspecialtymstr(this);

		return materialconsumption;
	}

	public Materialconsumption removeMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().remove(materialconsumption);
		materialconsumption.setSubspecialtymstr(null);

		return materialconsumption;
	}

	public List<Regndailysummary> getRegndailysummaries() {
		return this.regndailysummaries;
	}

	public void setRegndailysummaries(List<Regndailysummary> regndailysummaries) {
		this.regndailysummaries = regndailysummaries;
	}

	public Regndailysummary addRegndailysummary(Regndailysummary regndailysummary) {
		getRegndailysummaries().add(regndailysummary);
		regndailysummary.setSubspecialtymstr(this);

		return regndailysummary;
	}

	public Regndailysummary removeRegndailysummary(Regndailysummary regndailysummary) {
		getRegndailysummaries().remove(regndailysummary);
		regndailysummary.setSubspecialtymstr(null);

		return regndailysummary;
	}

	public List<Regnlocationlimit> getRegnlocationlimits() {
		return this.regnlocationlimits;
	}

	public void setRegnlocationlimits(List<Regnlocationlimit> regnlocationlimits) {
		this.regnlocationlimits = regnlocationlimits;
	}

	public Regnlocationlimit addRegnlocationlimit(Regnlocationlimit regnlocationlimit) {
		getRegnlocationlimits().add(regnlocationlimit);
		regnlocationlimit.setSubspecialtymstr(this);

		return regnlocationlimit;
	}

	public Regnlocationlimit removeRegnlocationlimit(Regnlocationlimit regnlocationlimit) {
		getRegnlocationlimits().remove(regnlocationlimit);
		regnlocationlimit.setSubspecialtymstr(null);

		return regnlocationlimit;
	}

	public List<Regntypecharge> getRegntypecharges() {
		return this.regntypecharges;
	}

	public void setRegntypecharges(List<Regntypecharge> regntypecharges) {
		this.regntypecharges = regntypecharges;
	}

	public Regntypecharge addRegntypecharge(Regntypecharge regntypecharge) {
		getRegntypecharges().add(regntypecharge);
		regntypecharge.setSubspecialtymstr(this);

		return regntypecharge;
	}

	public Regntypecharge removeRegntypecharge(Regntypecharge regntypecharge) {
		getRegntypecharges().remove(regntypecharge);
		regntypecharge.setSubspecialtymstr(null);

		return regntypecharge;
	}

	public List<Subspecialtywardrequest> getSubspecialtywardrequests1() {
		return this.subspecialtywardrequests1;
	}

	public void setSubspecialtywardrequests1(List<Subspecialtywardrequest> subspecialtywardrequests1) {
		this.subspecialtywardrequests1 = subspecialtywardrequests1;
	}

	public Subspecialtywardrequest addSubspecialtywardrequests1(Subspecialtywardrequest subspecialtywardrequests1) {
		getSubspecialtywardrequests1().add(subspecialtywardrequests1);
		subspecialtywardrequests1.setSubspecialtymstr1(this);

		return subspecialtywardrequests1;
	}

	public Subspecialtywardrequest removeSubspecialtywardrequests1(Subspecialtywardrequest subspecialtywardrequests1) {
		getSubspecialtywardrequests1().remove(subspecialtywardrequests1);
		subspecialtywardrequests1.setSubspecialtymstr1(null);

		return subspecialtywardrequests1;
	}

	public List<Subspecialtywardrequest> getSubspecialtywardrequests2() {
		return this.subspecialtywardrequests2;
	}

	public void setSubspecialtywardrequests2(List<Subspecialtywardrequest> subspecialtywardrequests2) {
		this.subspecialtywardrequests2 = subspecialtywardrequests2;
	}

	public Subspecialtywardrequest addSubspecialtywardrequests2(Subspecialtywardrequest subspecialtywardrequests2) {
		getSubspecialtywardrequests2().add(subspecialtywardrequests2);
		subspecialtywardrequests2.setSubspecialtymstr2(this);

		return subspecialtywardrequests2;
	}

	public Subspecialtywardrequest removeSubspecialtywardrequests2(Subspecialtywardrequest subspecialtywardrequests2) {
		getSubspecialtywardrequests2().remove(subspecialtywardrequests2);
		subspecialtywardrequests2.setSubspecialtymstr2(null);

		return subspecialtywardrequests2;
	}

	public List<Visit> getVisits() {
		return this.visits;
	}

	public void setVisits(List<Visit> visits) {
		this.visits = visits;
	}

	public Visit addVisit(Visit visit) {
		getVisits().add(visit);
		visit.setSubspecialtymstr(this);

		return visit;
	}

	public Visit removeVisit(Visit visit) {
		getVisits().remove(visit);
		visit.setSubspecialtymstr(null);

		return visit;
	}

	public List<Visitappointment> getVisitappointments() {
		return this.visitappointments;
	}

	public void setVisitappointments(List<Visitappointment> visitappointments) {
		this.visitappointments = visitappointments;
	}

	public Visitappointment addVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().add(visitappointment);
		visitappointment.setSubspecialtymstr(this);

		return visitappointment;
	}

	public Visitappointment removeVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().remove(visitappointment);
		visitappointment.setSubspecialtymstr(null);

		return visitappointment;
	}

	public List<Visitcasenote> getVisitcasenotes() {
		return this.visitcasenotes;
	}

	public void setVisitcasenotes(List<Visitcasenote> visitcasenotes) {
		this.visitcasenotes = visitcasenotes;
	}

	public Visitcasenote addVisitcasenote(Visitcasenote visitcasenote) {
		getVisitcasenotes().add(visitcasenote);
		visitcasenote.setSubspecialtymstr(this);

		return visitcasenote;
	}

	public Visitcasenote removeVisitcasenote(Visitcasenote visitcasenote) {
		getVisitcasenotes().remove(visitcasenote);
		visitcasenote.setSubspecialtymstr(null);

		return visitcasenote;
	}

	public List<Visitdailysummary> getVisitdailysummaries1() {
		return this.visitdailysummaries1;
	}

	public void setVisitdailysummaries1(List<Visitdailysummary> visitdailysummaries1) {
		this.visitdailysummaries1 = visitdailysummaries1;
	}

	public Visitdailysummary addVisitdailysummaries1(Visitdailysummary visitdailysummaries1) {
		getVisitdailysummaries1().add(visitdailysummaries1);
		visitdailysummaries1.setSubspecialtymstr1(this);

		return visitdailysummaries1;
	}

	public Visitdailysummary removeVisitdailysummaries1(Visitdailysummary visitdailysummaries1) {
		getVisitdailysummaries1().remove(visitdailysummaries1);
		visitdailysummaries1.setSubspecialtymstr1(null);

		return visitdailysummaries1;
	}

	public List<Visitdailysummary> getVisitdailysummaries2() {
		return this.visitdailysummaries2;
	}

	public void setVisitdailysummaries2(List<Visitdailysummary> visitdailysummaries2) {
		this.visitdailysummaries2 = visitdailysummaries2;
	}

	public Visitdailysummary addVisitdailysummaries2(Visitdailysummary visitdailysummaries2) {
		getVisitdailysummaries2().add(visitdailysummaries2);
		visitdailysummaries2.setSubspecialtymstr2(this);

		return visitdailysummaries2;
	}

	public Visitdailysummary removeVisitdailysummaries2(Visitdailysummary visitdailysummaries2) {
		getVisitdailysummaries2().remove(visitdailysummaries2);
		visitdailysummaries2.setSubspecialtymstr2(null);

		return visitdailysummaries2;
	}

	public List<Visitdailysummarydetail> getVisitdailysummarydetails1() {
		return this.visitdailysummarydetails1;
	}

	public void setVisitdailysummarydetails1(List<Visitdailysummarydetail> visitdailysummarydetails1) {
		this.visitdailysummarydetails1 = visitdailysummarydetails1;
	}

	public Visitdailysummarydetail addVisitdailysummarydetails1(Visitdailysummarydetail visitdailysummarydetails1) {
		getVisitdailysummarydetails1().add(visitdailysummarydetails1);
		visitdailysummarydetails1.setSubspecialtymstr1(this);

		return visitdailysummarydetails1;
	}

	public Visitdailysummarydetail removeVisitdailysummarydetails1(Visitdailysummarydetail visitdailysummarydetails1) {
		getVisitdailysummarydetails1().remove(visitdailysummarydetails1);
		visitdailysummarydetails1.setSubspecialtymstr1(null);

		return visitdailysummarydetails1;
	}

	public List<Visitdailysummarydetail> getVisitdailysummarydetails2() {
		return this.visitdailysummarydetails2;
	}

	public void setVisitdailysummarydetails2(List<Visitdailysummarydetail> visitdailysummarydetails2) {
		this.visitdailysummarydetails2 = visitdailysummarydetails2;
	}

	public Visitdailysummarydetail addVisitdailysummarydetails2(Visitdailysummarydetail visitdailysummarydetails2) {
		getVisitdailysummarydetails2().add(visitdailysummarydetails2);
		visitdailysummarydetails2.setSubspecialtymstr2(this);

		return visitdailysummarydetails2;
	}

	public Visitdailysummarydetail removeVisitdailysummarydetails2(Visitdailysummarydetail visitdailysummarydetails2) {
		getVisitdailysummarydetails2().remove(visitdailysummarydetails2);
		visitdailysummarydetails2.setSubspecialtymstr2(null);

		return visitdailysummarydetails2;
	}

	public List<Visitdiagnosi> getVisitdiagnosis() {
		return this.visitdiagnosis;
	}

	public void setVisitdiagnosis(List<Visitdiagnosi> visitdiagnosis) {
		this.visitdiagnosis = visitdiagnosis;
	}

	public Visitdiagnosi addVisitdiagnosi(Visitdiagnosi visitdiagnosi) {
		getVisitdiagnosis().add(visitdiagnosi);
		visitdiagnosi.setSubspecialtymstr(this);

		return visitdiagnosi;
	}

	public Visitdiagnosi removeVisitdiagnosi(Visitdiagnosi visitdiagnosi) {
		getVisitdiagnosis().remove(visitdiagnosi);
		visitdiagnosi.setSubspecialtymstr(null);

		return visitdiagnosi;
	}

	public List<Visitdoctor> getVisitdoctors() {
		return this.visitdoctors;
	}

	public void setVisitdoctors(List<Visitdoctor> visitdoctors) {
		this.visitdoctors = visitdoctors;
	}

	public Visitdoctor addVisitdoctor(Visitdoctor visitdoctor) {
		getVisitdoctors().add(visitdoctor);
		visitdoctor.setSubspecialtymstr(this);

		return visitdoctor;
	}

	public Visitdoctor removeVisitdoctor(Visitdoctor visitdoctor) {
		getVisitdoctors().remove(visitdoctor);
		visitdoctor.setSubspecialtymstr(null);

		return visitdoctor;
	}

	public List<Visitreferral> getVisitreferrals() {
		return this.visitreferrals;
	}

	public void setVisitreferrals(List<Visitreferral> visitreferrals) {
		this.visitreferrals = visitreferrals;
	}

	public Visitreferral addVisitreferral(Visitreferral visitreferral) {
		getVisitreferrals().add(visitreferral);
		visitreferral.setSubspecialtymstr(this);

		return visitreferral;
	}

	public Visitreferral removeVisitreferral(Visitreferral visitreferral) {
		getVisitreferrals().remove(visitreferral);
		visitreferral.setSubspecialtymstr(null);

		return visitreferral;
	}

	public List<Visitregntype> getVisitregntypes() {
		return this.visitregntypes;
	}

	public void setVisitregntypes(List<Visitregntype> visitregntypes) {
		this.visitregntypes = visitregntypes;
	}

	public Visitregntype addVisitregntype(Visitregntype visitregntype) {
		getVisitregntypes().add(visitregntype);
		visitregntype.setSubspecialtymstr(this);

		return visitregntype;
	}

	public Visitregntype removeVisitregntype(Visitregntype visitregntype) {
		getVisitregntypes().remove(visitregntype);
		visitregntype.setSubspecialtymstr(null);

		return visitregntype;
	}

	public List<Visitsubspecialtyhistory> getVisitsubspecialtyhistories() {
		return this.visitsubspecialtyhistories;
	}

	public void setVisitsubspecialtyhistories(List<Visitsubspecialtyhistory> visitsubspecialtyhistories) {
		this.visitsubspecialtyhistories = visitsubspecialtyhistories;
	}

	public Visitsubspecialtyhistory addVisitsubspecialtyhistory(Visitsubspecialtyhistory visitsubspecialtyhistory) {
		getVisitsubspecialtyhistories().add(visitsubspecialtyhistory);
		visitsubspecialtyhistory.setSubspecialtymstr(this);

		return visitsubspecialtyhistory;
	}

	public Visitsubspecialtyhistory removeVisitsubspecialtyhistory(Visitsubspecialtyhistory visitsubspecialtyhistory) {
		getVisitsubspecialtyhistories().remove(visitsubspecialtyhistory);
		visitsubspecialtyhistory.setSubspecialtymstr(null);

		return visitsubspecialtyhistory;
	}

	public List<Wardsubspecialty> getWardsubspecialties() {
		return this.wardsubspecialties;
	}

	public void setWardsubspecialties(List<Wardsubspecialty> wardsubspecialties) {
		this.wardsubspecialties = wardsubspecialties;
	}

	public Wardsubspecialty addWardsubspecialty(Wardsubspecialty wardsubspecialty) {
		getWardsubspecialties().add(wardsubspecialty);
		wardsubspecialty.setSubspecialtymstr(this);

		return wardsubspecialty;
	}

	public Wardsubspecialty removeWardsubspecialty(Wardsubspecialty wardsubspecialty) {
		getWardsubspecialties().remove(wardsubspecialty);
		wardsubspecialty.setSubspecialtymstr(null);

		return wardsubspecialty;
	}

}