package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PATIENTACCOUNTTXN database table.
 * 
 */
@Entity
@NamedQuery(name="Patientaccounttxn.findAll", query="SELECT p FROM Patientaccounttxn p")
public class Patientaccounttxn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTACCOUNTTXN_ID")
	private long patientaccounttxnId;

	@Column(name="ALTERNATE_PATIENTACCOUNT_ID")
	private BigDecimal alternatePatientaccountId;

	@Column(name="CHARGE_STATUS")
	private String chargeStatus;

	@Column(name="CLAIM_OVERWRITTEN_IND")
	private String claimOverwrittenInd;

	@Column(name="CLAIMABLE_AMOUNT")
	private BigDecimal claimableAmount;

	@Column(name="CLAIMABLE_PERCENT")
	private BigDecimal claimablePercent;

	@Column(name="DEPOSIT_TXN_AMOUNT")
	private BigDecimal depositTxnAmount;

	@Column(name="DISCOUNT_AMOUNT")
	private BigDecimal discountAmount;

	@Column(name="DRCRNOTE_ID")
	private BigDecimal drcrnoteId;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATETIME")
	private Date enteredDatetime;

	@Column(name="GLACCOUNTMSTR_ID")
	private BigDecimal glaccountmstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="POSTED_DATETIME")
	private Date postedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REFERENCE_NO")
	private String referenceNo;

	private String remarks;

	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenuecentremstrId;

	@Column(name="TXN_AMOUNT")
	private BigDecimal txnAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="TXN_DATETIME")
	private Date txnDatetime;

	@Column(name="TXN_DESC")
	private String txnDesc;

	@Column(name="TXN_NO")
	private String txnNo;

	@Column(name="TXN_STATUS")
	private String txnStatus;

	@Column(name="TXNCODEMSTR_ID")
	private BigDecimal txncodemstrId;

	//bi-directional many-to-one association to Billdetail
	@OneToMany(mappedBy="patientaccounttxn")
	private List<Billdetail> billdetails;

	//bi-directional many-to-one association to Billdiscountdetail
	@OneToMany(mappedBy="patientaccounttxn")
	private List<Billdiscountdetail> billdiscountdetails;

	//bi-directional many-to-one association to Billpayment
	@OneToMany(mappedBy="patientaccounttxn")
	private List<Billpayment> billpayments;

	//bi-directional many-to-one association to Charge
	@OneToMany(mappedBy="patientaccounttxn")
	private List<Charge> charges;

	//bi-directional many-to-one association to Deposit
	@OneToMany(mappedBy="patientaccounttxn1")
	private List<Deposit> deposits1;

	//bi-directional many-to-one association to Deposit
	@OneToMany(mappedBy="patientaccounttxn2")
	private List<Deposit> deposits2;


	//bi-directional many-to-one association to Bill
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BILL_ID")
	private Bill bill;

	//bi-directional many-to-one association to Patientaccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNT_ID")
	private Patientaccount patientaccount;

	//bi-directional many-to-one association to Patientaccounttxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CANCEL_FOR_PAT_ID")
	private Patientaccounttxn patientaccounttxn1;

	//bi-directional many-to-one association to Patientaccounttxn
	@OneToMany(mappedBy="patientaccounttxn1")
	private List<Patientaccounttxn> patientaccounttxns1;

	//bi-directional many-to-one association to Patientaccounttxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PARENT_PATIENTACCOUNTTXN_ID")
	private Patientaccounttxn patientaccounttxn2;

	//bi-directional many-to-one association to Patientaccounttxn
	@OneToMany(mappedBy="patientaccounttxn2")
	private List<Patientaccounttxn> patientaccounttxns2;

	//bi-directional many-to-one association to Visitregntypecharge
	@OneToMany(mappedBy="patientaccounttxn")
	private List<Visitregntypecharge> visitregntypecharges;

	//bi-directional many-to-one association to Wardsspmstocktxn
	@OneToMany(mappedBy="patientaccounttxn")
	private List<Wardsspmstocktxn> wardsspmstocktxns;

	public Patientaccounttxn() {
	}

	public long getPatientaccounttxnId() {
		return this.patientaccounttxnId;
	}

	public void setPatientaccounttxnId(long patientaccounttxnId) {
		this.patientaccounttxnId = patientaccounttxnId;
	}

	public BigDecimal getAlternatePatientaccountId() {
		return this.alternatePatientaccountId;
	}

	public void setAlternatePatientaccountId(BigDecimal alternatePatientaccountId) {
		this.alternatePatientaccountId = alternatePatientaccountId;
	}

	public String getChargeStatus() {
		return this.chargeStatus;
	}

	public void setChargeStatus(String chargeStatus) {
		this.chargeStatus = chargeStatus;
	}

	public String getClaimOverwrittenInd() {
		return this.claimOverwrittenInd;
	}

	public void setClaimOverwrittenInd(String claimOverwrittenInd) {
		this.claimOverwrittenInd = claimOverwrittenInd;
	}

	public BigDecimal getClaimableAmount() {
		return this.claimableAmount;
	}

	public void setClaimableAmount(BigDecimal claimableAmount) {
		this.claimableAmount = claimableAmount;
	}

	public BigDecimal getClaimablePercent() {
		return this.claimablePercent;
	}

	public void setClaimablePercent(BigDecimal claimablePercent) {
		this.claimablePercent = claimablePercent;
	}

	public BigDecimal getDepositTxnAmount() {
		return this.depositTxnAmount;
	}

	public void setDepositTxnAmount(BigDecimal depositTxnAmount) {
		this.depositTxnAmount = depositTxnAmount;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getDrcrnoteId() {
		return this.drcrnoteId;
	}

	public void setDrcrnoteId(BigDecimal drcrnoteId) {
		this.drcrnoteId = drcrnoteId;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredDatetime() {
		return this.enteredDatetime;
	}

	public void setEnteredDatetime(Date enteredDatetime) {
		this.enteredDatetime = enteredDatetime;
	}

	public BigDecimal getGlaccountmstrId() {
		return this.glaccountmstrId;
	}

	public void setGlaccountmstrId(BigDecimal glaccountmstrId) {
		this.glaccountmstrId = glaccountmstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public Date getPostedDatetime() {
		return this.postedDatetime;
	}

	public void setPostedDatetime(Date postedDatetime) {
		this.postedDatetime = postedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getReferenceNo() {
		return this.referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getRevenuecentremstrId() {
		return this.revenuecentremstrId;
	}

	public void setRevenuecentremstrId(BigDecimal revenuecentremstrId) {
		this.revenuecentremstrId = revenuecentremstrId;
	}

	public BigDecimal getTxnAmount() {
		return this.txnAmount;
	}

	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}

	public Date getTxnDatetime() {
		return this.txnDatetime;
	}

	public void setTxnDatetime(Date txnDatetime) {
		this.txnDatetime = txnDatetime;
	}

	public String getTxnDesc() {
		return this.txnDesc;
	}

	public void setTxnDesc(String txnDesc) {
		this.txnDesc = txnDesc;
	}

	public String getTxnNo() {
		return this.txnNo;
	}

	public void setTxnNo(String txnNo) {
		this.txnNo = txnNo;
	}

	public String getTxnStatus() {
		return this.txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public BigDecimal getTxncodemstrId() {
		return this.txncodemstrId;
	}

	public void setTxncodemstrId(BigDecimal txncodemstrId) {
		this.txncodemstrId = txncodemstrId;
	}

	public List<Billdetail> getBilldetails() {
		return this.billdetails;
	}

	public void setBilldetails(List<Billdetail> billdetails) {
		this.billdetails = billdetails;
	}

	public Billdetail addBilldetail(Billdetail billdetail) {
		getBilldetails().add(billdetail);
		billdetail.setPatientaccounttxn(this);

		return billdetail;
	}

	public Billdetail removeBilldetail(Billdetail billdetail) {
		getBilldetails().remove(billdetail);
		billdetail.setPatientaccounttxn(null);

		return billdetail;
	}

	public List<Billdiscountdetail> getBilldiscountdetails() {
		return this.billdiscountdetails;
	}

	public void setBilldiscountdetails(List<Billdiscountdetail> billdiscountdetails) {
		this.billdiscountdetails = billdiscountdetails;
	}

	public Billdiscountdetail addBilldiscountdetail(Billdiscountdetail billdiscountdetail) {
		getBilldiscountdetails().add(billdiscountdetail);
		billdiscountdetail.setPatientaccounttxn(this);

		return billdiscountdetail;
	}

	public Billdiscountdetail removeBilldiscountdetail(Billdiscountdetail billdiscountdetail) {
		getBilldiscountdetails().remove(billdiscountdetail);
		billdiscountdetail.setPatientaccounttxn(null);

		return billdiscountdetail;
	}

	public List<Billpayment> getBillpayments() {
		return this.billpayments;
	}

	public void setBillpayments(List<Billpayment> billpayments) {
		this.billpayments = billpayments;
	}

	public Billpayment addBillpayment(Billpayment billpayment) {
		getBillpayments().add(billpayment);
		billpayment.setPatientaccounttxn(this);

		return billpayment;
	}

	public Billpayment removeBillpayment(Billpayment billpayment) {
		getBillpayments().remove(billpayment);
		billpayment.setPatientaccounttxn(null);

		return billpayment;
	}

	public List<Charge> getCharges() {
		return this.charges;
	}

	public void setCharges(List<Charge> charges) {
		this.charges = charges;
	}

	public Charge addCharge(Charge charge) {
		getCharges().add(charge);
		charge.setPatientaccounttxn(this);

		return charge;
	}

	public Charge removeCharge(Charge charge) {
		getCharges().remove(charge);
		charge.setPatientaccounttxn(null);

		return charge;
	}

	public List<Deposit> getDeposits1() {
		return this.deposits1;
	}

	public void setDeposits1(List<Deposit> deposits1) {
		this.deposits1 = deposits1;
	}

	public Deposit addDeposits1(Deposit deposits1) {
		getDeposits1().add(deposits1);
		deposits1.setPatientaccounttxn1(this);

		return deposits1;
	}

	public Deposit removeDeposits1(Deposit deposits1) {
		getDeposits1().remove(deposits1);
		deposits1.setPatientaccounttxn1(null);

		return deposits1;
	}

	public List<Deposit> getDeposits2() {
		return this.deposits2;
	}

	public void setDeposits2(List<Deposit> deposits2) {
		this.deposits2 = deposits2;
	}

	public Deposit addDeposits2(Deposit deposits2) {
		getDeposits2().add(deposits2);
		deposits2.setPatientaccounttxn2(this);

		return deposits2;
	}

	public Deposit removeDeposits2(Deposit deposits2) {
		getDeposits2().remove(deposits2);
		deposits2.setPatientaccounttxn2(null);

		return deposits2;
	}


	public Bill getBill() {
		return this.bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Patientaccount getPatientaccount() {
		return this.patientaccount;
	}

	public void setPatientaccount(Patientaccount patientaccount) {
		this.patientaccount = patientaccount;
	}

	public Patientaccounttxn getPatientaccounttxn1() {
		return this.patientaccounttxn1;
	}

	public void setPatientaccounttxn1(Patientaccounttxn patientaccounttxn1) {
		this.patientaccounttxn1 = patientaccounttxn1;
	}

	public List<Patientaccounttxn> getPatientaccounttxns1() {
		return this.patientaccounttxns1;
	}

	public void setPatientaccounttxns1(List<Patientaccounttxn> patientaccounttxns1) {
		this.patientaccounttxns1 = patientaccounttxns1;
	}

	public Patientaccounttxn addPatientaccounttxns1(Patientaccounttxn patientaccounttxns1) {
		getPatientaccounttxns1().add(patientaccounttxns1);
		patientaccounttxns1.setPatientaccounttxn1(this);

		return patientaccounttxns1;
	}

	public Patientaccounttxn removePatientaccounttxns1(Patientaccounttxn patientaccounttxns1) {
		getPatientaccounttxns1().remove(patientaccounttxns1);
		patientaccounttxns1.setPatientaccounttxn1(null);

		return patientaccounttxns1;
	}

	public Patientaccounttxn getPatientaccounttxn2() {
		return this.patientaccounttxn2;
	}

	public void setPatientaccounttxn2(Patientaccounttxn patientaccounttxn2) {
		this.patientaccounttxn2 = patientaccounttxn2;
	}

	public List<Patientaccounttxn> getPatientaccounttxns2() {
		return this.patientaccounttxns2;
	}

	public void setPatientaccounttxns2(List<Patientaccounttxn> patientaccounttxns2) {
		this.patientaccounttxns2 = patientaccounttxns2;
	}

	public Patientaccounttxn addPatientaccounttxns2(Patientaccounttxn patientaccounttxns2) {
		getPatientaccounttxns2().add(patientaccounttxns2);
		patientaccounttxns2.setPatientaccounttxn2(this);

		return patientaccounttxns2;
	}

	public Patientaccounttxn removePatientaccounttxns2(Patientaccounttxn patientaccounttxns2) {
		getPatientaccounttxns2().remove(patientaccounttxns2);
		patientaccounttxns2.setPatientaccounttxn2(null);

		return patientaccounttxns2;
	}

	public List<Visitregntypecharge> getVisitregntypecharges() {
		return this.visitregntypecharges;
	}

	public void setVisitregntypecharges(List<Visitregntypecharge> visitregntypecharges) {
		this.visitregntypecharges = visitregntypecharges;
	}

	public Visitregntypecharge addVisitregntypecharge(Visitregntypecharge visitregntypecharge) {
		getVisitregntypecharges().add(visitregntypecharge);
		visitregntypecharge.setPatientaccounttxn(this);

		return visitregntypecharge;
	}

	public Visitregntypecharge removeVisitregntypecharge(Visitregntypecharge visitregntypecharge) {
		getVisitregntypecharges().remove(visitregntypecharge);
		visitregntypecharge.setPatientaccounttxn(null);

		return visitregntypecharge;
	}

	public List<Wardsspmstocktxn> getWardsspmstocktxns() {
		return this.wardsspmstocktxns;
	}

	public void setWardsspmstocktxns(List<Wardsspmstocktxn> wardsspmstocktxns) {
		this.wardsspmstocktxns = wardsspmstocktxns;
	}

	public Wardsspmstocktxn addWardsspmstocktxn(Wardsspmstocktxn wardsspmstocktxn) {
		getWardsspmstocktxns().add(wardsspmstocktxn);
		wardsspmstocktxn.setPatientaccounttxn(this);

		return wardsspmstocktxn;
	}

	public Wardsspmstocktxn removeWardsspmstocktxn(Wardsspmstocktxn wardsspmstocktxn) {
		getWardsspmstocktxns().remove(wardsspmstocktxn);
		wardsspmstocktxn.setPatientaccounttxn(null);

		return wardsspmstocktxn;
	}

}