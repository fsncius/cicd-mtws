package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CHARGEMATERIALCOMPDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Chargematerialcompdetail.findAll", query="SELECT c FROM Chargematerialcompdetail c")
public class Chargematerialcompdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGEMATERIALCOMPDETAIL_ID")
	private long chargematerialcompdetailId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIAL_QTY")
	private BigDecimal materialQty;

	@Column(name="QTY_UOM")
	private String qtyUom;

	private String remarks;

	//bi-directional many-to-one association to Chargematerialcomp
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEMATERIALCOMP_ID")
	private Chargematerialcomp chargematerialcomp;

	//bi-directional many-to-one association to Materialitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMMSTR_ID")
	private Materialitemmstr materialitemmstr;

	public Chargematerialcompdetail() {
	}

	public long getChargematerialcompdetailId() {
		return this.chargematerialcompdetailId;
	}

	public void setChargematerialcompdetailId(long chargematerialcompdetailId) {
		this.chargematerialcompdetailId = chargematerialcompdetailId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaterialQty() {
		return this.materialQty;
	}

	public void setMaterialQty(BigDecimal materialQty) {
		this.materialQty = materialQty;
	}

	public String getQtyUom() {
		return this.qtyUom;
	}

	public void setQtyUom(String qtyUom) {
		this.qtyUom = qtyUom;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Chargematerialcomp getChargematerialcomp() {
		return this.chargematerialcomp;
	}

	public void setChargematerialcomp(Chargematerialcomp chargematerialcomp) {
		this.chargematerialcomp = chargematerialcomp;
	}

	public Materialitemmstr getMaterialitemmstr() {
		return this.materialitemmstr;
	}

	public void setMaterialitemmstr(Materialitemmstr materialitemmstr) {
		this.materialitemmstr = materialitemmstr;
	}

}