package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PATIENTDEBTORPLAN database table.
 * 
 */
@Entity
@NamedQuery(name="Patientdebtorplan.findAll", query="SELECT p FROM Patientdebtorplan p")
public class Patientdebtorplan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTDEBTORPLAN_ID")
	private long patientdebtorplanId;

	@Column(name="DEBTOR_PLAN_CODE")
	private String debtorPlanCode;

	@Column(name="DEBTOR_PLAN_DESC")
	private String debtorPlanDesc;

	@Column(name="DEBTOR_PLAN_DESC_LANG1")
	private String debtorPlanDescLang1;

	@Column(name="DEBTOR_PLAN_DESC_LANG2")
	private String debtorPlanDescLang2;

	@Column(name="DEBTOR_PLAN_DESC_LANG3")
	private String debtorPlanDescLang3;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Column(name="ENTITYMSTR_ID")
	private BigDecimal entitymstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="ORGANISATION_ID")
	private BigDecimal organisationId;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SHORT_CODE")
	private String shortCode;

	//bi-directional many-to-one association to Patient
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENT_ID")
	private Patient patient;

	//bi-directional many-to-one association to Patientdebtorplandetail
	@OneToMany(mappedBy="patientdebtorplan")
	private List<Patientdebtorplandetail> patientdebtorplandetails;

	public Patientdebtorplan() {
	}

	public long getPatientdebtorplanId() {
		return this.patientdebtorplanId;
	}

	public void setPatientdebtorplanId(long patientdebtorplanId) {
		this.patientdebtorplanId = patientdebtorplanId;
	}

	public String getDebtorPlanCode() {
		return this.debtorPlanCode;
	}

	public void setDebtorPlanCode(String debtorPlanCode) {
		this.debtorPlanCode = debtorPlanCode;
	}

	public String getDebtorPlanDesc() {
		return this.debtorPlanDesc;
	}

	public void setDebtorPlanDesc(String debtorPlanDesc) {
		this.debtorPlanDesc = debtorPlanDesc;
	}

	public String getDebtorPlanDescLang1() {
		return this.debtorPlanDescLang1;
	}

	public void setDebtorPlanDescLang1(String debtorPlanDescLang1) {
		this.debtorPlanDescLang1 = debtorPlanDescLang1;
	}

	public String getDebtorPlanDescLang2() {
		return this.debtorPlanDescLang2;
	}

	public void setDebtorPlanDescLang2(String debtorPlanDescLang2) {
		this.debtorPlanDescLang2 = debtorPlanDescLang2;
	}

	public String getDebtorPlanDescLang3() {
		return this.debtorPlanDescLang3;
	}

	public void setDebtorPlanDescLang3(String debtorPlanDescLang3) {
		this.debtorPlanDescLang3 = debtorPlanDescLang3;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public BigDecimal getEntitymstrId() {
		return this.entitymstrId;
	}

	public void setEntitymstrId(BigDecimal entitymstrId) {
		this.entitymstrId = entitymstrId;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getOrganisationId() {
		return this.organisationId;
	}

	public void setOrganisationId(BigDecimal organisationId) {
		this.organisationId = organisationId;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public List<Patientdebtorplandetail> getPatientdebtorplandetails() {
		return this.patientdebtorplandetails;
	}

	public void setPatientdebtorplandetails(List<Patientdebtorplandetail> patientdebtorplandetails) {
		this.patientdebtorplandetails = patientdebtorplandetails;
	}

	public Patientdebtorplandetail addPatientdebtorplandetail(Patientdebtorplandetail patientdebtorplandetail) {
		getPatientdebtorplandetails().add(patientdebtorplandetail);
		patientdebtorplandetail.setPatientdebtorplan(this);

		return patientdebtorplandetail;
	}

	public Patientdebtorplandetail removePatientdebtorplandetail(Patientdebtorplandetail patientdebtorplandetail) {
		getPatientdebtorplandetails().remove(patientdebtorplandetail);
		patientdebtorplandetail.setPatientdebtorplan(null);

		return patientdebtorplandetail;
	}

}