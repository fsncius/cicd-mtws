package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKCOUNTDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Stockcountdetail.findAll", query="SELECT s FROM Stockcountdetail s")
public class Stockcountdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKCOUNTDETAIL_ID")
	private long stockcountdetailId;

	@Column(name="ADJUST_PRICE_UOM")
	private String adjustPriceUom;

	@Column(name="ADJUST_RETAIL_PRICE")
	private BigDecimal adjustRetailPrice;

	@Column(name="ADJUST_WHOLESALE_PRICE")
	private BigDecimal adjustWholesalePrice;

	@Column(name="BATCH_NO")
	private String batchNo;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="COUNT_DETAIL_STATUS")
	private String countDetailStatus;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFINE_PRICE_UOM")
	private String definePriceUom;

	@Column(name="DEFINE_RETAIL_PRICE")
	private BigDecimal defineRetailPrice;

	@Column(name="DEFINE_WHOLESALE_PRICE")
	private BigDecimal defineWholesalePrice;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="QTY_ON_COUNTBALANCE")
	private BigDecimal qtyOnCountbalance;

	@Column(name="QTY_ON_COUNTINPUT")
	private BigDecimal qtyOnCountinput;

	private String remarks;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Stockcountbalance
	@OneToMany(mappedBy="stockcountdetail")
	private List<Stockcountbalance> stockcountbalances;

	//bi-directional many-to-one association to Materialitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMMSTR_ID")
	private Materialitemmstr materialitemmstr;

	//bi-directional many-to-one association to Materialtxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALTXN_ID")
	private Materialtxn materialtxn;

	//bi-directional many-to-one association to Stockcount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKCOUNT_ID")
	private Stockcount stockcount;

	//bi-directional many-to-one association to Stockreceiptdetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKSET_ID")
	private Stockreceiptdetail stockreceiptdetail;

	//bi-directional many-to-one association to Storebinmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREBINMSTR_ID")
	private Storebinmstr storebinmstr;

	//bi-directional many-to-one association to Vendormstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MANUFACTURER_VENDORMSTR_ID")
	private Vendormstr vendormstr;

	public Stockcountdetail() {
	}

	public long getStockcountdetailId() {
		return this.stockcountdetailId;
	}

	public void setStockcountdetailId(long stockcountdetailId) {
		this.stockcountdetailId = stockcountdetailId;
	}

	public String getAdjustPriceUom() {
		return this.adjustPriceUom;
	}

	public void setAdjustPriceUom(String adjustPriceUom) {
		this.adjustPriceUom = adjustPriceUom;
	}

	public BigDecimal getAdjustRetailPrice() {
		return this.adjustRetailPrice;
	}

	public void setAdjustRetailPrice(BigDecimal adjustRetailPrice) {
		this.adjustRetailPrice = adjustRetailPrice;
	}

	public BigDecimal getAdjustWholesalePrice() {
		return this.adjustWholesalePrice;
	}

	public void setAdjustWholesalePrice(BigDecimal adjustWholesalePrice) {
		this.adjustWholesalePrice = adjustWholesalePrice;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public String getCountDetailStatus() {
		return this.countDetailStatus;
	}

	public void setCountDetailStatus(String countDetailStatus) {
		this.countDetailStatus = countDetailStatus;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefinePriceUom() {
		return this.definePriceUom;
	}

	public void setDefinePriceUom(String definePriceUom) {
		this.definePriceUom = definePriceUom;
	}

	public BigDecimal getDefineRetailPrice() {
		return this.defineRetailPrice;
	}

	public void setDefineRetailPrice(BigDecimal defineRetailPrice) {
		this.defineRetailPrice = defineRetailPrice;
	}

	public BigDecimal getDefineWholesalePrice() {
		return this.defineWholesalePrice;
	}

	public void setDefineWholesalePrice(BigDecimal defineWholesalePrice) {
		this.defineWholesalePrice = defineWholesalePrice;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getQtyOnCountbalance() {
		return this.qtyOnCountbalance;
	}

	public void setQtyOnCountbalance(BigDecimal qtyOnCountbalance) {
		this.qtyOnCountbalance = qtyOnCountbalance;
	}

	public BigDecimal getQtyOnCountinput() {
		return this.qtyOnCountinput;
	}

	public void setQtyOnCountinput(BigDecimal qtyOnCountinput) {
		this.qtyOnCountinput = qtyOnCountinput;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public List<Stockcountbalance> getStockcountbalances() {
		return this.stockcountbalances;
	}

	public void setStockcountbalances(List<Stockcountbalance> stockcountbalances) {
		this.stockcountbalances = stockcountbalances;
	}

	public Stockcountbalance addStockcountbalance(Stockcountbalance stockcountbalance) {
		getStockcountbalances().add(stockcountbalance);
		stockcountbalance.setStockcountdetail(this);

		return stockcountbalance;
	}

	public Stockcountbalance removeStockcountbalance(Stockcountbalance stockcountbalance) {
		getStockcountbalances().remove(stockcountbalance);
		stockcountbalance.setStockcountdetail(null);

		return stockcountbalance;
	}

	public Materialitemmstr getMaterialitemmstr() {
		return this.materialitemmstr;
	}

	public void setMaterialitemmstr(Materialitemmstr materialitemmstr) {
		this.materialitemmstr = materialitemmstr;
	}

	public Materialtxn getMaterialtxn() {
		return this.materialtxn;
	}

	public void setMaterialtxn(Materialtxn materialtxn) {
		this.materialtxn = materialtxn;
	}

	public Stockcount getStockcount() {
		return this.stockcount;
	}

	public void setStockcount(Stockcount stockcount) {
		this.stockcount = stockcount;
	}

	public Stockreceiptdetail getStockreceiptdetail() {
		return this.stockreceiptdetail;
	}

	public void setStockreceiptdetail(Stockreceiptdetail stockreceiptdetail) {
		this.stockreceiptdetail = stockreceiptdetail;
	}

	public Storebinmstr getStorebinmstr() {
		return this.storebinmstr;
	}

	public void setStorebinmstr(Storebinmstr storebinmstr) {
		this.storebinmstr = storebinmstr;
	}

	public Vendormstr getVendormstr() {
		return this.vendormstr;
	}

	public void setVendormstr(Vendormstr vendormstr) {
		this.vendormstr = vendormstr;
	}

}