package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CHARGE database table.
 * 
 */
@Entity
@NamedQuery(name="Charge.findAll", query="SELECT c FROM Charge c")
public class Charge implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGE_ID")
	private long chargeId;

	@Column(name="BASE_UNIT_PRICE")
	private BigDecimal baseUnitPrice;

	@Column(name="CANCEL_REASON")
	private String cancelReason;

	@Column(name="CHARGE_REVENUECENTREMSTR_ID")
	private BigDecimal chargeRevenuecentremstrId;

	@Column(name="CHARGE_TXN_NO")
	private String chargeTxnNo;

	@Column(name="CHARGING_SOURCE")
	private String chargingSource;

	@Column(name="CN_MIO_INDICATION")
	private String cnMioIndication;

	@Column(name="COST_PRICE")
	private BigDecimal costPrice;

	@Column(name="DIAGNOSISMSTR_ID")
	private BigDecimal diagnosismstrId;

	@Column(name="DISCOUNT_IND")
	private String discountInd;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DATETIME")
	private Date endDatetime;

	@Column(name="FLEXI_PANELORDER_ID")
	private BigDecimal flexiPanelorderId;

	@Column(name="FORM_NO")
	private String formNo;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MIO_CLAIM_CLASS")
	private String mioClaimClass;

	@Column(name="MIO_INDICATION")
	private String mioIndication;

	@Column(name="ORDERED_TREATMENTGROUPMSTR_ID")
	private BigDecimal orderedTreatmentgroupmstrId;

	@Column(name="ORDERENTRYITEM_ID")
	private BigDecimal orderentryitemId;

	@Column(name="OTHER_CANCEL_REASON")
	private String otherCancelReason;

	@Column(name="PANELORDER_ID")
	private BigDecimal panelorderId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PRICINGPOLICYDETAIL_ID")
	private BigDecimal pricingpolicydetailId;

	private BigDecimal qty;

	@Column(name="QTY_UOM")
	private String qtyUom;

	@Column(name="REQUISITION_ID")
	private BigDecimal requisitionId;

	@Column(name="STANDARD_UNIT_PRICE")
	private BigDecimal standardUnitPrice;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DATETIME")
	private Date startDatetime;

	@Column(name="TREATMENTGROUPMSTR_ID")
	private BigDecimal treatmentgroupmstrId;

	//bi-directional many-to-one association to Bedhistory
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BEDHISTORY_ID")
	private Bedhistory bedhistory;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ORDERED_CAREPROVIDER_ID")
	private Careprovider careprovider1;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PERFORMED_CAREPROVIDER_ID")
	private Careprovider careprovider2;

	//bi-directional many-to-one association to Chargeitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PANEL_CHARGEITEM_ID")
	private Chargeitemmstr chargeitemmstr;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to Patientaccounttxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNTTXN_ID")
	private Patientaccounttxn patientaccounttxn;

	//bi-directional many-to-one association to Storedispensewindowmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREDISPENSEWINDOWMSTR_ID")
	private Storedispensewindowmstr storedispensewindowmstr;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr1;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr2;

	//bi-directional many-to-one association to Surchargepolicydetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SURCHARGEPOLICYDETAIL_ID")
	private Surchargepolicydetail surchargepolicydetail;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_WARDMSTR_ID")
	private Wardmstr wardmstr;

	//bi-directional many-to-one association to Chargerefundrequisition
	@OneToMany(mappedBy="charge")
	private List<Chargerefundrequisition> chargerefundrequisitions;

	//bi-directional many-to-one association to Materialconsumption
	@OneToMany(mappedBy="charge")
	private List<Materialconsumption> materialconsumptions;

	public Charge() {
	}

	public long getChargeId() {
		return this.chargeId;
	}

	public void setChargeId(long chargeId) {
		this.chargeId = chargeId;
	}

	public BigDecimal getBaseUnitPrice() {
		return this.baseUnitPrice;
	}

	public void setBaseUnitPrice(BigDecimal baseUnitPrice) {
		this.baseUnitPrice = baseUnitPrice;
	}

	public String getCancelReason() {
		return this.cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public BigDecimal getChargeRevenuecentremstrId() {
		return this.chargeRevenuecentremstrId;
	}

	public void setChargeRevenuecentremstrId(BigDecimal chargeRevenuecentremstrId) {
		this.chargeRevenuecentremstrId = chargeRevenuecentremstrId;
	}

	public String getChargeTxnNo() {
		return this.chargeTxnNo;
	}

	public void setChargeTxnNo(String chargeTxnNo) {
		this.chargeTxnNo = chargeTxnNo;
	}

	public String getChargingSource() {
		return this.chargingSource;
	}

	public void setChargingSource(String chargingSource) {
		this.chargingSource = chargingSource;
	}

	public String getCnMioIndication() {
		return this.cnMioIndication;
	}

	public void setCnMioIndication(String cnMioIndication) {
		this.cnMioIndication = cnMioIndication;
	}

	public BigDecimal getCostPrice() {
		return this.costPrice;
	}

	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}

	public BigDecimal getDiagnosismstrId() {
		return this.diagnosismstrId;
	}

	public void setDiagnosismstrId(BigDecimal diagnosismstrId) {
		this.diagnosismstrId = diagnosismstrId;
	}

	public String getDiscountInd() {
		return this.discountInd;
	}

	public void setDiscountInd(String discountInd) {
		this.discountInd = discountInd;
	}

	public Date getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public BigDecimal getFlexiPanelorderId() {
		return this.flexiPanelorderId;
	}

	public void setFlexiPanelorderId(BigDecimal flexiPanelorderId) {
		this.flexiPanelorderId = flexiPanelorderId;
	}

	public String getFormNo() {
		return this.formNo;
	}

	public void setFormNo(String formNo) {
		this.formNo = formNo;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMioClaimClass() {
		return this.mioClaimClass;
	}

	public void setMioClaimClass(String mioClaimClass) {
		this.mioClaimClass = mioClaimClass;
	}

	public String getMioIndication() {
		return this.mioIndication;
	}

	public void setMioIndication(String mioIndication) {
		this.mioIndication = mioIndication;
	}

	public BigDecimal getOrderedTreatmentgroupmstrId() {
		return this.orderedTreatmentgroupmstrId;
	}

	public void setOrderedTreatmentgroupmstrId(BigDecimal orderedTreatmentgroupmstrId) {
		this.orderedTreatmentgroupmstrId = orderedTreatmentgroupmstrId;
	}

	public BigDecimal getOrderentryitemId() {
		return this.orderentryitemId;
	}

	public void setOrderentryitemId(BigDecimal orderentryitemId) {
		this.orderentryitemId = orderentryitemId;
	}

	public String getOtherCancelReason() {
		return this.otherCancelReason;
	}

	public void setOtherCancelReason(String otherCancelReason) {
		this.otherCancelReason = otherCancelReason;
	}

	public BigDecimal getPanelorderId() {
		return this.panelorderId;
	}

	public void setPanelorderId(BigDecimal panelorderId) {
		this.panelorderId = panelorderId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getPricingpolicydetailId() {
		return this.pricingpolicydetailId;
	}

	public void setPricingpolicydetailId(BigDecimal pricingpolicydetailId) {
		this.pricingpolicydetailId = pricingpolicydetailId;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getQtyUom() {
		return this.qtyUom;
	}

	public void setQtyUom(String qtyUom) {
		this.qtyUom = qtyUom;
	}

	public BigDecimal getRequisitionId() {
		return this.requisitionId;
	}

	public void setRequisitionId(BigDecimal requisitionId) {
		this.requisitionId = requisitionId;
	}

	public BigDecimal getStandardUnitPrice() {
		return this.standardUnitPrice;
	}

	public void setStandardUnitPrice(BigDecimal standardUnitPrice) {
		this.standardUnitPrice = standardUnitPrice;
	}

	public Date getStartDatetime() {
		return this.startDatetime;
	}

	public void setStartDatetime(Date startDatetime) {
		this.startDatetime = startDatetime;
	}

	public BigDecimal getTreatmentgroupmstrId() {
		return this.treatmentgroupmstrId;
	}

	public void setTreatmentgroupmstrId(BigDecimal treatmentgroupmstrId) {
		this.treatmentgroupmstrId = treatmentgroupmstrId;
	}

	public Bedhistory getBedhistory() {
		return this.bedhistory;
	}

	public void setBedhistory(Bedhistory bedhistory) {
		this.bedhistory = bedhistory;
	}

	public Careprovider getCareprovider1() {
		return this.careprovider1;
	}

	public void setCareprovider1(Careprovider careprovider1) {
		this.careprovider1 = careprovider1;
	}

	public Careprovider getCareprovider2() {
		return this.careprovider2;
	}

	public void setCareprovider2(Careprovider careprovider2) {
		this.careprovider2 = careprovider2;
	}

	public Chargeitemmstr getChargeitemmstr() {
		return this.chargeitemmstr;
	}

	public void setChargeitemmstr(Chargeitemmstr chargeitemmstr) {
		this.chargeitemmstr = chargeitemmstr;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public Patientaccounttxn getPatientaccounttxn() {
		return this.patientaccounttxn;
	}

	public void setPatientaccounttxn(Patientaccounttxn patientaccounttxn) {
		this.patientaccounttxn = patientaccounttxn;
	}

	public Storedispensewindowmstr getStoredispensewindowmstr() {
		return this.storedispensewindowmstr;
	}

	public void setStoredispensewindowmstr(Storedispensewindowmstr storedispensewindowmstr) {
		this.storedispensewindowmstr = storedispensewindowmstr;
	}

	public Subspecialtymstr getSubspecialtymstr1() {
		return this.subspecialtymstr1;
	}

	public void setSubspecialtymstr1(Subspecialtymstr subspecialtymstr1) {
		this.subspecialtymstr1 = subspecialtymstr1;
	}

	public Subspecialtymstr getSubspecialtymstr2() {
		return this.subspecialtymstr2;
	}

	public void setSubspecialtymstr2(Subspecialtymstr subspecialtymstr2) {
		this.subspecialtymstr2 = subspecialtymstr2;
	}

	public Surchargepolicydetail getSurchargepolicydetail() {
		return this.surchargepolicydetail;
	}

	public void setSurchargepolicydetail(Surchargepolicydetail surchargepolicydetail) {
		this.surchargepolicydetail = surchargepolicydetail;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

	public List<Chargerefundrequisition> getChargerefundrequisitions() {
		return this.chargerefundrequisitions;
	}

	public void setChargerefundrequisitions(List<Chargerefundrequisition> chargerefundrequisitions) {
		this.chargerefundrequisitions = chargerefundrequisitions;
	}

	public Chargerefundrequisition addChargerefundrequisition(Chargerefundrequisition chargerefundrequisition) {
		getChargerefundrequisitions().add(chargerefundrequisition);
		chargerefundrequisition.setCharge(this);

		return chargerefundrequisition;
	}

	public Chargerefundrequisition removeChargerefundrequisition(Chargerefundrequisition chargerefundrequisition) {
		getChargerefundrequisitions().remove(chargerefundrequisition);
		chargerefundrequisition.setCharge(null);

		return chargerefundrequisition;
	}

	public List<Materialconsumption> getMaterialconsumptions() {
		return this.materialconsumptions;
	}

	public void setMaterialconsumptions(List<Materialconsumption> materialconsumptions) {
		this.materialconsumptions = materialconsumptions;
	}

	public Materialconsumption addMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().add(materialconsumption);
		materialconsumption.setCharge(this);

		return materialconsumption;
	}

	public Materialconsumption removeMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().remove(materialconsumption);
		materialconsumption.setCharge(null);

		return materialconsumption;
	}

}