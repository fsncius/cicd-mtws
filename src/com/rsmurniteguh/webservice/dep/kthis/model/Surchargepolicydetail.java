package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the SURCHARGEPOLICYDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Surchargepolicydetail.findAll", query="SELECT s FROM Surchargepolicydetail s")
public class Surchargepolicydetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SURCHARGEPOLICYDETAIL_ID")
	private long surchargepolicydetailId;

	@Column(name="DEFAULT_SELECTED")
	private String defaultSelected;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private BigDecimal qty;

	@Column(name="QTY_UOM")
	private String qtyUom;

	//bi-directional many-to-one association to Charge
	@OneToMany(mappedBy="surchargepolicydetail")
	private List<Charge> charges;

	//bi-directional many-to-one association to Chargeitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEITEMMSTR_ID")
	private Chargeitemmstr chargeitemmstr;

	//bi-directional many-to-one association to Surchargepolicy
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SURCHARGEPOLICY_ID")
	private Surchargepolicy surchargepolicy;

	public Surchargepolicydetail() {
	}

	public long getSurchargepolicydetailId() {
		return this.surchargepolicydetailId;
	}

	public void setSurchargepolicydetailId(long surchargepolicydetailId) {
		this.surchargepolicydetailId = surchargepolicydetailId;
	}

	public String getDefaultSelected() {
		return this.defaultSelected;
	}

	public void setDefaultSelected(String defaultSelected) {
		this.defaultSelected = defaultSelected;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getQtyUom() {
		return this.qtyUom;
	}

	public void setQtyUom(String qtyUom) {
		this.qtyUom = qtyUom;
	}

	public List<Charge> getCharges() {
		return this.charges;
	}

	public void setCharges(List<Charge> charges) {
		this.charges = charges;
	}

	public Charge addCharge(Charge charge) {
		getCharges().add(charge);
		charge.setSurchargepolicydetail(this);

		return charge;
	}

	public Charge removeCharge(Charge charge) {
		getCharges().remove(charge);
		charge.setSurchargepolicydetail(null);

		return charge;
	}

	public Chargeitemmstr getChargeitemmstr() {
		return this.chargeitemmstr;
	}

	public void setChargeitemmstr(Chargeitemmstr chargeitemmstr) {
		this.chargeitemmstr = chargeitemmstr;
	}

	public Surchargepolicy getSurchargepolicy() {
		return this.surchargepolicy;
	}

	public void setSurchargepolicy(Surchargepolicy surchargepolicy) {
		this.surchargepolicy = surchargepolicy;
	}

}