package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MEDICALRECORDNO database table.
 * 
 */
@Entity
@NamedQuery(name="Medicalrecordno.findAll", query="SELECT m FROM Medicalrecordno m")
public class Medicalrecordno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MEDICALRECORDNO_ID")
	private long medicalrecordnoId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="ENTITYMSTR_ID")
	private BigDecimal entitymstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MEDICAL_RECORD_NO")
	private String medicalRecordNo;

	@Column(name="MEDICAL_RECORD_NO_TYPE")
	private String medicalRecordNoType;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="SYSTEM_IND")
	private String systemInd;

	//bi-directional many-to-one association to Patient
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENT_ID")
	private Patient patient;

	public Medicalrecordno() {
	}

	public long getMedicalrecordnoId() {
		return this.medicalrecordnoId;
	}

	public void setMedicalrecordnoId(long medicalrecordnoId) {
		this.medicalrecordnoId = medicalrecordnoId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getEntitymstrId() {
		return this.entitymstrId;
	}

	public void setEntitymstrId(BigDecimal entitymstrId) {
		this.entitymstrId = entitymstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMedicalRecordNo() {
		return this.medicalRecordNo;
	}

	public void setMedicalRecordNo(String medicalRecordNo) {
		this.medicalRecordNo = medicalRecordNo;
	}

	public String getMedicalRecordNoType() {
		return this.medicalRecordNoType;
	}

	public void setMedicalRecordNoType(String medicalRecordNoType) {
		this.medicalRecordNoType = medicalRecordNoType;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSystemInd() {
		return this.systemInd;
	}

	public void setSystemInd(String systemInd) {
		this.systemInd = systemInd;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

}