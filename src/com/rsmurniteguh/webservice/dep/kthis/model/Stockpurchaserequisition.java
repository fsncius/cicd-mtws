package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKPURCHASEREQUISITION database table.
 * 
 */
@Entity
@NamedQuery(name="Stockpurchaserequisition.findAll", query="SELECT s FROM Stockpurchaserequisition s")
public class Stockpurchaserequisition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKPURCHASEREQUISITION_ID")
	private long stockpurchaserequisitionId;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PLANNED_BY")
	private BigDecimal plannedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PLANNED_DATETIME")
	private Date plannedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PURCHASE_REQUISITION_NO")
	private String purchaseRequisitionNo;

	@Column(name="PURCHASE_STATUS")
	private String purchaseStatus;

	private String remarks;

	@Column(name="REQUESTED_BY")
	private BigDecimal requestedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="REQUESTED_DATETIME")
	private Date requestedDatetime;

	//bi-directional many-to-one association to Stockpurchaseplan
	@OneToMany(mappedBy="stockpurchaserequisition")
	private List<Stockpurchaseplan> stockpurchaseplans;

	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREMSTR_ID")
	private Storemstr storemstr;

	public Stockpurchaserequisition() {
	}

	public long getStockpurchaserequisitionId() {
		return this.stockpurchaserequisitionId;
	}

	public void setStockpurchaserequisitionId(long stockpurchaserequisitionId) {
		this.stockpurchaserequisitionId = stockpurchaserequisitionId;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPlannedBy() {
		return this.plannedBy;
	}

	public void setPlannedBy(BigDecimal plannedBy) {
		this.plannedBy = plannedBy;
	}

	public Date getPlannedDatetime() {
		return this.plannedDatetime;
	}

	public void setPlannedDatetime(Date plannedDatetime) {
		this.plannedDatetime = plannedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPurchaseRequisitionNo() {
		return this.purchaseRequisitionNo;
	}

	public void setPurchaseRequisitionNo(String purchaseRequisitionNo) {
		this.purchaseRequisitionNo = purchaseRequisitionNo;
	}

	public String getPurchaseStatus() {
		return this.purchaseStatus;
	}

	public void setPurchaseStatus(String purchaseStatus) {
		this.purchaseStatus = purchaseStatus;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getRequestedBy() {
		return this.requestedBy;
	}

	public void setRequestedBy(BigDecimal requestedBy) {
		this.requestedBy = requestedBy;
	}

	public Date getRequestedDatetime() {
		return this.requestedDatetime;
	}

	public void setRequestedDatetime(Date requestedDatetime) {
		this.requestedDatetime = requestedDatetime;
	}

	public List<Stockpurchaseplan> getStockpurchaseplans() {
		return this.stockpurchaseplans;
	}

	public void setStockpurchaseplans(List<Stockpurchaseplan> stockpurchaseplans) {
		this.stockpurchaseplans = stockpurchaseplans;
	}

	public Stockpurchaseplan addStockpurchaseplan(Stockpurchaseplan stockpurchaseplan) {
		getStockpurchaseplans().add(stockpurchaseplan);
		stockpurchaseplan.setStockpurchaserequisition(this);

		return stockpurchaseplan;
	}

	public Stockpurchaseplan removeStockpurchaseplan(Stockpurchaseplan stockpurchaseplan) {
		getStockpurchaseplans().remove(stockpurchaseplan);
		stockpurchaseplan.setStockpurchaserequisition(null);

		return stockpurchaseplan;
	}

	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

}