package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BASEUNITPRICEHISTORY database table.
 * 
 */
@Entity
@NamedQuery(name="Baseunitpricehistory.findAll", query="SELECT b FROM Baseunitpricehistory b")
public class Baseunitpricehistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BASEUNITPRICEHISTORY_ID")
	private long baseunitpricehistoryId;

	@Temporal(TemporalType.DATE)
	@Column(name="ADJUSTED_DATETIME")
	private Date adjustedDatetime;

	@Column(name="ADJUSTMENT_REASON")
	private String adjustmentReason;

	@Column(name="ADJUSTMENT_REMARKS")
	private String adjustmentRemarks;

	@Column(name="APPLY_IND")
	private String applyInd;

	@Column(name="BASE_IND")
	private String baseInd;

	@Column(name="CAREPROVIDER_GRADE")
	private String careproviderGrade;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATETIME")
	private Date effectiveDatetime;

	@Column(name="INITIAL_IND")
	private String initialInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="NEW_BASE_UNIT_PRICE")
	private BigDecimal newBaseUnitPrice;

	@Column(name="NEW_MAX_BASE_PRICE")
	private BigDecimal newMaxBasePrice;

	@Column(name="NEW_MIN_BASE_PRICE")
	private BigDecimal newMinBasePrice;

	@Column(name="NEW_QTY_UOM")
	private String newQtyUom;

	@Column(name="NEW_WHOLESALE_PRICE")
	private BigDecimal newWholesalePrice;

	@Column(name="OLD_BASE_UNIT_PRICE")
	private BigDecimal oldBaseUnitPrice;

	@Column(name="OLD_MAX_BASE_PRICE")
	private BigDecimal oldMaxBasePrice;

	@Column(name="OLD_MIN_BASE_PRICE")
	private BigDecimal oldMinBasePrice;

	@Column(name="OLD_QTY_UOM")
	private String oldQtyUom;

	@Column(name="OLD_WHOLESALE_PRICE")
	private BigDecimal oldWholesalePrice;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PRICE_ADJUSTMENT_LICENSE")
	private String priceAdjustmentLicense;

	@Column(name="PRICE_EFFECT_METHOD")
	private String priceEffectMethod;

	@Column(name="PRICEADJUSTMENTBATCH_ID")
	private BigDecimal priceadjustmentbatchId;

	@Column(name="WHOLESALE_PRICE")
	private BigDecimal wholesalePrice;

	//bi-directional many-to-one association to Baseunitprice
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BASEUNITPRICE_ID")
	private Baseunitprice baseunitprice;

	//bi-directional many-to-one association to Chargeitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEITEMMSTR_ID")
	private Chargeitemmstr chargeitemmstr;

	//bi-directional many-to-one association to Materialpriceadjust
	@OneToMany(mappedBy="baseunitpricehistory")
	private List<Materialpriceadjust> materialpriceadjusts;

	//bi-directional many-to-one association to Materialpricehistory
	@OneToMany(mappedBy="baseunitpricehistory")
	private List<Materialpricehistory> materialpricehistories;

	public Baseunitpricehistory() {
	}

	public long getBaseunitpricehistoryId() {
		return this.baseunitpricehistoryId;
	}

	public void setBaseunitpricehistoryId(long baseunitpricehistoryId) {
		this.baseunitpricehistoryId = baseunitpricehistoryId;
	}

	public Date getAdjustedDatetime() {
		return this.adjustedDatetime;
	}

	public void setAdjustedDatetime(Date adjustedDatetime) {
		this.adjustedDatetime = adjustedDatetime;
	}

	public String getAdjustmentReason() {
		return this.adjustmentReason;
	}

	public void setAdjustmentReason(String adjustmentReason) {
		this.adjustmentReason = adjustmentReason;
	}

	public String getAdjustmentRemarks() {
		return this.adjustmentRemarks;
	}

	public void setAdjustmentRemarks(String adjustmentRemarks) {
		this.adjustmentRemarks = adjustmentRemarks;
	}

	public String getApplyInd() {
		return this.applyInd;
	}

	public void setApplyInd(String applyInd) {
		this.applyInd = applyInd;
	}

	public String getBaseInd() {
		return this.baseInd;
	}

	public void setBaseInd(String baseInd) {
		this.baseInd = baseInd;
	}

	public String getCareproviderGrade() {
		return this.careproviderGrade;
	}

	public void setCareproviderGrade(String careproviderGrade) {
		this.careproviderGrade = careproviderGrade;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffectiveDatetime() {
		return this.effectiveDatetime;
	}

	public void setEffectiveDatetime(Date effectiveDatetime) {
		this.effectiveDatetime = effectiveDatetime;
	}

	public String getInitialInd() {
		return this.initialInd;
	}

	public void setInitialInd(String initialInd) {
		this.initialInd = initialInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getNewBaseUnitPrice() {
		return this.newBaseUnitPrice;
	}

	public void setNewBaseUnitPrice(BigDecimal newBaseUnitPrice) {
		this.newBaseUnitPrice = newBaseUnitPrice;
	}

	public BigDecimal getNewMaxBasePrice() {
		return this.newMaxBasePrice;
	}

	public void setNewMaxBasePrice(BigDecimal newMaxBasePrice) {
		this.newMaxBasePrice = newMaxBasePrice;
	}

	public BigDecimal getNewMinBasePrice() {
		return this.newMinBasePrice;
	}

	public void setNewMinBasePrice(BigDecimal newMinBasePrice) {
		this.newMinBasePrice = newMinBasePrice;
	}

	public String getNewQtyUom() {
		return this.newQtyUom;
	}

	public void setNewQtyUom(String newQtyUom) {
		this.newQtyUom = newQtyUom;
	}

	public BigDecimal getNewWholesalePrice() {
		return this.newWholesalePrice;
	}

	public void setNewWholesalePrice(BigDecimal newWholesalePrice) {
		this.newWholesalePrice = newWholesalePrice;
	}

	public BigDecimal getOldBaseUnitPrice() {
		return this.oldBaseUnitPrice;
	}

	public void setOldBaseUnitPrice(BigDecimal oldBaseUnitPrice) {
		this.oldBaseUnitPrice = oldBaseUnitPrice;
	}

	public BigDecimal getOldMaxBasePrice() {
		return this.oldMaxBasePrice;
	}

	public void setOldMaxBasePrice(BigDecimal oldMaxBasePrice) {
		this.oldMaxBasePrice = oldMaxBasePrice;
	}

	public BigDecimal getOldMinBasePrice() {
		return this.oldMinBasePrice;
	}

	public void setOldMinBasePrice(BigDecimal oldMinBasePrice) {
		this.oldMinBasePrice = oldMinBasePrice;
	}

	public String getOldQtyUom() {
		return this.oldQtyUom;
	}

	public void setOldQtyUom(String oldQtyUom) {
		this.oldQtyUom = oldQtyUom;
	}

	public BigDecimal getOldWholesalePrice() {
		return this.oldWholesalePrice;
	}

	public void setOldWholesalePrice(BigDecimal oldWholesalePrice) {
		this.oldWholesalePrice = oldWholesalePrice;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPriceAdjustmentLicense() {
		return this.priceAdjustmentLicense;
	}

	public void setPriceAdjustmentLicense(String priceAdjustmentLicense) {
		this.priceAdjustmentLicense = priceAdjustmentLicense;
	}

	public String getPriceEffectMethod() {
		return this.priceEffectMethod;
	}

	public void setPriceEffectMethod(String priceEffectMethod) {
		this.priceEffectMethod = priceEffectMethod;
	}

	public BigDecimal getPriceadjustmentbatchId() {
		return this.priceadjustmentbatchId;
	}

	public void setPriceadjustmentbatchId(BigDecimal priceadjustmentbatchId) {
		this.priceadjustmentbatchId = priceadjustmentbatchId;
	}

	public BigDecimal getWholesalePrice() {
		return this.wholesalePrice;
	}

	public void setWholesalePrice(BigDecimal wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}

	public Baseunitprice getBaseunitprice() {
		return this.baseunitprice;
	}

	public void setBaseunitprice(Baseunitprice baseunitprice) {
		this.baseunitprice = baseunitprice;
	}

	public Chargeitemmstr getChargeitemmstr() {
		return this.chargeitemmstr;
	}

	public void setChargeitemmstr(Chargeitemmstr chargeitemmstr) {
		this.chargeitemmstr = chargeitemmstr;
	}

	public List<Materialpriceadjust> getMaterialpriceadjusts() {
		return this.materialpriceadjusts;
	}

	public void setMaterialpriceadjusts(List<Materialpriceadjust> materialpriceadjusts) {
		this.materialpriceadjusts = materialpriceadjusts;
	}

	public Materialpriceadjust addMaterialpriceadjust(Materialpriceadjust materialpriceadjust) {
		getMaterialpriceadjusts().add(materialpriceadjust);
		materialpriceadjust.setBaseunitpricehistory(this);

		return materialpriceadjust;
	}

	public Materialpriceadjust removeMaterialpriceadjust(Materialpriceadjust materialpriceadjust) {
		getMaterialpriceadjusts().remove(materialpriceadjust);
		materialpriceadjust.setBaseunitpricehistory(null);

		return materialpriceadjust;
	}

	public List<Materialpricehistory> getMaterialpricehistories() {
		return this.materialpricehistories;
	}

	public void setMaterialpricehistories(List<Materialpricehistory> materialpricehistories) {
		this.materialpricehistories = materialpricehistories;
	}

	public Materialpricehistory addMaterialpricehistory(Materialpricehistory materialpricehistory) {
		getMaterialpricehistories().add(materialpricehistory);
		materialpricehistory.setBaseunitpricehistory(this);

		return materialpricehistory;
	}

	public Materialpricehistory removeMaterialpricehistory(Materialpricehistory materialpricehistory) {
		getMaterialpricehistories().remove(materialpricehistory);
		materialpricehistory.setBaseunitpricehistory(null);

		return materialpricehistory;
	}

}