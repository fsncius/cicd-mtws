package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DISCHARGE database table.
 * 
 */
@Entity
@NamedQuery(name="Discharge.findAll", query="SELECT d FROM Discharge d")
public class Discharge implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DISCHARGE_ID")
	private long dischargeId;

	@Column(name="CANCEL_REASON")
	private String cancelReason;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="DISCHARGE_DATETIME")
	private Date dischargeDatetime;

	@Column(name="DISCHARGE_NOTE")
	private String dischargeNote;

	@Column(name="DISCHARGE_STATUS")
	private String dischargeStatus;

	@Column(name="DISCHARGED_BY")
	private BigDecimal dischargedBy;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="OTHER_CANCEL_REASON")
	private String otherCancelReason;

	@Column(name="OTHER_REFER_TO")
	private String otherReferTo;

	@Column(name="OUTCOME_TYPE")
	private String outcomeType;

	@Column(name="PENDING_DISCHARGED_BY")
	private BigDecimal pendingDischargedBy;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REFER_TO")
	private String referTo;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public Discharge() {
	}

	public long getDischargeId() {
		return this.dischargeId;
	}

	public void setDischargeId(long dischargeId) {
		this.dischargeId = dischargeId;
	}

	public String getCancelReason() {
		return this.cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getDischargeDatetime() {
		return this.dischargeDatetime;
	}

	public void setDischargeDatetime(Date dischargeDatetime) {
		this.dischargeDatetime = dischargeDatetime;
	}

	public String getDischargeNote() {
		return this.dischargeNote;
	}

	public void setDischargeNote(String dischargeNote) {
		this.dischargeNote = dischargeNote;
	}

	public String getDischargeStatus() {
		return this.dischargeStatus;
	}

	public void setDischargeStatus(String dischargeStatus) {
		this.dischargeStatus = dischargeStatus;
	}

	public BigDecimal getDischargedBy() {
		return this.dischargedBy;
	}

	public void setDischargedBy(BigDecimal dischargedBy) {
		this.dischargedBy = dischargedBy;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getOtherCancelReason() {
		return this.otherCancelReason;
	}

	public void setOtherCancelReason(String otherCancelReason) {
		this.otherCancelReason = otherCancelReason;
	}

	public String getOtherReferTo() {
		return this.otherReferTo;
	}

	public void setOtherReferTo(String otherReferTo) {
		this.otherReferTo = otherReferTo;
	}

	public String getOutcomeType() {
		return this.outcomeType;
	}

	public void setOutcomeType(String outcomeType) {
		this.outcomeType = outcomeType;
	}

	public BigDecimal getPendingDischargedBy() {
		return this.pendingDischargedBy;
	}

	public void setPendingDischargedBy(BigDecimal pendingDischargedBy) {
		this.pendingDischargedBy = pendingDischargedBy;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getReferTo() {
		return this.referTo;
	}

	public void setReferTo(String referTo) {
		this.referTo = referTo;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}