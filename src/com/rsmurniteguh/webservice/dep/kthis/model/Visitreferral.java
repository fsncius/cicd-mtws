package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITREFERRAL database table.
 * 
 */
@Entity
@NamedQuery(name="Visitreferral.findAll", query="SELECT v FROM Visitreferral v")
public class Visitreferral implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITREFERRAL_ID")
	private long visitreferralId;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_BY")
	private BigDecimal defunctBy;

	@Temporal(TemporalType.DATE)
	@Column(name="DEFUNCT_DATETIME")
	private Date defunctDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="EXT_DEPARTMENT")
	private String extDepartment;

	@Column(name="EXT_DOCTOR")
	private String extDoctor;

	@Column(name="EXT_HOSPITAL")
	private String extHospital;

	@Column(name="INT_CAREPROVIDER_ID")
	private BigDecimal intCareproviderId;

	@Column(name="INT_SUBSPECIALTYMSTR_ID")
	private BigDecimal intSubspecialtymstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="REFERENCE_NO")
	private String referenceNo;

	@Column(name="VISITREFERRAL_DESC")
	private String visitreferralDesc;

	@Column(name="VISITREFERRAL_DESTINATION")
	private String visitreferralDestination;

	@Column(name="VISITREFERRAL_PURPOSE")
	private String visitreferralPurpose;

	@Column(name="VISITREFERRAL_STATUS")
	private String visitreferralStatus;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CREATED_BY")
	private Careprovider careprovider;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public Visitreferral() {
	}

	public long getVisitreferralId() {
		return this.visitreferralId;
	}

	public void setVisitreferralId(long visitreferralId) {
		this.visitreferralId = visitreferralId;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getDefunctBy() {
		return this.defunctBy;
	}

	public void setDefunctBy(BigDecimal defunctBy) {
		this.defunctBy = defunctBy;
	}

	public Date getDefunctDatetime() {
		return this.defunctDatetime;
	}

	public void setDefunctDatetime(Date defunctDatetime) {
		this.defunctDatetime = defunctDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getExtDepartment() {
		return this.extDepartment;
	}

	public void setExtDepartment(String extDepartment) {
		this.extDepartment = extDepartment;
	}

	public String getExtDoctor() {
		return this.extDoctor;
	}

	public void setExtDoctor(String extDoctor) {
		this.extDoctor = extDoctor;
	}

	public String getExtHospital() {
		return this.extHospital;
	}

	public void setExtHospital(String extHospital) {
		this.extHospital = extHospital;
	}

	public BigDecimal getIntCareproviderId() {
		return this.intCareproviderId;
	}

	public void setIntCareproviderId(BigDecimal intCareproviderId) {
		this.intCareproviderId = intCareproviderId;
	}

	public BigDecimal getIntSubspecialtymstrId() {
		return this.intSubspecialtymstrId;
	}

	public void setIntSubspecialtymstrId(BigDecimal intSubspecialtymstrId) {
		this.intSubspecialtymstrId = intSubspecialtymstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getReferenceNo() {
		return this.referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getVisitreferralDesc() {
		return this.visitreferralDesc;
	}

	public void setVisitreferralDesc(String visitreferralDesc) {
		this.visitreferralDesc = visitreferralDesc;
	}

	public String getVisitreferralDestination() {
		return this.visitreferralDestination;
	}

	public void setVisitreferralDestination(String visitreferralDestination) {
		this.visitreferralDestination = visitreferralDestination;
	}

	public String getVisitreferralPurpose() {
		return this.visitreferralPurpose;
	}

	public void setVisitreferralPurpose(String visitreferralPurpose) {
		this.visitreferralPurpose = visitreferralPurpose;
	}

	public String getVisitreferralStatus() {
		return this.visitreferralStatus;
	}

	public void setVisitreferralStatus(String visitreferralStatus) {
		this.visitreferralStatus = visitreferralStatus;
	}

	public Careprovider getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(Careprovider careprovider) {
		this.careprovider = careprovider;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}