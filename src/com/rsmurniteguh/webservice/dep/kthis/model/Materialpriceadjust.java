package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the MATERIALPRICEADJUST database table.
 * 
 */
@Entity
@NamedQuery(name="Materialpriceadjust.findAll", query="SELECT m FROM Materialpriceadjust m")
public class Materialpriceadjust implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MATERIALPRICEADJUST_ID")
	private long materialpriceadjustId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIALPRICEADJUST_NO")
	private String materialpriceadjustNo;

	@Column(name="MATERIALPRICEADJUST_TYPE")
	private String materialpriceadjustType;

	@Column(name="MATERIALTXN_ID")
	private BigDecimal materialtxnId;

	@Column(name="NEW_SALEPRICE")
	private BigDecimal newSaleprice;

	@Column(name="NEW_SALEPRICE_UOM")
	private String newSalepriceUom;

	@Column(name="NEW_WHOLESALE_PRICE")
	private BigDecimal newWholesalePrice;

	@Column(name="OLD_SALEPRICE")
	private BigDecimal oldSaleprice;

	@Column(name="OLD_SALEPRICE_UOM")
	private String oldSalepriceUom;

	@Column(name="OLD_WHOLESALE_PRICE")
	private BigDecimal oldWholesalePrice;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="TOTAL_ADJUSTPRICE")
	private BigDecimal totalAdjustprice;

	@Column(name="TOTAL_ADJUSTWHOLESALEPRICE")
	private BigDecimal totalAdjustwholesaleprice;

	@Column(name="TOTAL_QTY_OF_BASE_UOM")
	private BigDecimal totalQtyOfBaseUom;

	//bi-directional many-to-one association to Baseunitpricehistory
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BASEUNITPRICEHISTORY_ID")
	private Baseunitpricehistory baseunitpricehistory;

	//bi-directional many-to-one association to Materialitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMMSTR_ID")
	private Materialitemmstr materialitemmstr;

	//bi-directional many-to-one association to Materialpriceadjustdetail
	@OneToMany(mappedBy="materialpriceadjust")
	private List<Materialpriceadjustdetail> materialpriceadjustdetails;

	public Materialpriceadjust() {
	}

	public long getMaterialpriceadjustId() {
		return this.materialpriceadjustId;
	}

	public void setMaterialpriceadjustId(long materialpriceadjustId) {
		this.materialpriceadjustId = materialpriceadjustId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMaterialpriceadjustNo() {
		return this.materialpriceadjustNo;
	}

	public void setMaterialpriceadjustNo(String materialpriceadjustNo) {
		this.materialpriceadjustNo = materialpriceadjustNo;
	}

	public String getMaterialpriceadjustType() {
		return this.materialpriceadjustType;
	}

	public void setMaterialpriceadjustType(String materialpriceadjustType) {
		this.materialpriceadjustType = materialpriceadjustType;
	}

	public BigDecimal getMaterialtxnId() {
		return this.materialtxnId;
	}

	public void setMaterialtxnId(BigDecimal materialtxnId) {
		this.materialtxnId = materialtxnId;
	}

	public BigDecimal getNewSaleprice() {
		return this.newSaleprice;
	}

	public void setNewSaleprice(BigDecimal newSaleprice) {
		this.newSaleprice = newSaleprice;
	}

	public String getNewSalepriceUom() {
		return this.newSalepriceUom;
	}

	public void setNewSalepriceUom(String newSalepriceUom) {
		this.newSalepriceUom = newSalepriceUom;
	}

	public BigDecimal getNewWholesalePrice() {
		return this.newWholesalePrice;
	}

	public void setNewWholesalePrice(BigDecimal newWholesalePrice) {
		this.newWholesalePrice = newWholesalePrice;
	}

	public BigDecimal getOldSaleprice() {
		return this.oldSaleprice;
	}

	public void setOldSaleprice(BigDecimal oldSaleprice) {
		this.oldSaleprice = oldSaleprice;
	}

	public String getOldSalepriceUom() {
		return this.oldSalepriceUom;
	}

	public void setOldSalepriceUom(String oldSalepriceUom) {
		this.oldSalepriceUom = oldSalepriceUom;
	}

	public BigDecimal getOldWholesalePrice() {
		return this.oldWholesalePrice;
	}

	public void setOldWholesalePrice(BigDecimal oldWholesalePrice) {
		this.oldWholesalePrice = oldWholesalePrice;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getTotalAdjustprice() {
		return this.totalAdjustprice;
	}

	public void setTotalAdjustprice(BigDecimal totalAdjustprice) {
		this.totalAdjustprice = totalAdjustprice;
	}

	public BigDecimal getTotalAdjustwholesaleprice() {
		return this.totalAdjustwholesaleprice;
	}

	public void setTotalAdjustwholesaleprice(BigDecimal totalAdjustwholesaleprice) {
		this.totalAdjustwholesaleprice = totalAdjustwholesaleprice;
	}

	public BigDecimal getTotalQtyOfBaseUom() {
		return this.totalQtyOfBaseUom;
	}

	public void setTotalQtyOfBaseUom(BigDecimal totalQtyOfBaseUom) {
		this.totalQtyOfBaseUom = totalQtyOfBaseUom;
	}

	public Baseunitpricehistory getBaseunitpricehistory() {
		return this.baseunitpricehistory;
	}

	public void setBaseunitpricehistory(Baseunitpricehistory baseunitpricehistory) {
		this.baseunitpricehistory = baseunitpricehistory;
	}

	public Materialitemmstr getMaterialitemmstr() {
		return this.materialitemmstr;
	}

	public void setMaterialitemmstr(Materialitemmstr materialitemmstr) {
		this.materialitemmstr = materialitemmstr;
	}

	public List<Materialpriceadjustdetail> getMaterialpriceadjustdetails() {
		return this.materialpriceadjustdetails;
	}

	public void setMaterialpriceadjustdetails(List<Materialpriceadjustdetail> materialpriceadjustdetails) {
		this.materialpriceadjustdetails = materialpriceadjustdetails;
	}

	public Materialpriceadjustdetail addMaterialpriceadjustdetail(Materialpriceadjustdetail materialpriceadjustdetail) {
		getMaterialpriceadjustdetails().add(materialpriceadjustdetail);
		materialpriceadjustdetail.setMaterialpriceadjust(this);

		return materialpriceadjustdetail;
	}

	public Materialpriceadjustdetail removeMaterialpriceadjustdetail(Materialpriceadjustdetail materialpriceadjustdetail) {
		getMaterialpriceadjustdetails().remove(materialpriceadjustdetail);
		materialpriceadjustdetail.setMaterialpriceadjust(null);

		return materialpriceadjustdetail;
	}

}