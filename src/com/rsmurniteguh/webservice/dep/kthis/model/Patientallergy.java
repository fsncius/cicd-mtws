package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PATIENTALLERGY database table.
 * 
 */
@Entity
@NamedQuery(name="Patientallergy.findAll", query="SELECT p FROM Patientallergy p")
public class Patientallergy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTALLERGY_ID")
	private long patientallergyId;

	@Column(name="ALLERGY_DESC")
	private String allergyDesc;

	@Column(name="ALLERGY_GROUP")
	private String allergyGroup;

	@Column(name="ALLERGY_STATUS")
	private String allergyStatus;

	@Column(name="ALLERGY_TYPE")
	private String allergyType;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="GENERICDRUGMSTR_ID")
	private BigDecimal genericdrugmstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String probability;

	@Column(name="REACTION_TYPE")
	private String reactionType;

	private String remarks;

	private String roa;

	@Column(name="SYSTEM_INVOLVED")
	private String systemInvolved;

	//bi-directional many-to-one association to Person
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PERSON_ID")
	private Person person;

	public Patientallergy() {
	}

	public long getPatientallergyId() {
		return this.patientallergyId;
	}

	public void setPatientallergyId(long patientallergyId) {
		this.patientallergyId = patientallergyId;
	}

	public String getAllergyDesc() {
		return this.allergyDesc;
	}

	public void setAllergyDesc(String allergyDesc) {
		this.allergyDesc = allergyDesc;
	}

	public String getAllergyGroup() {
		return this.allergyGroup;
	}

	public void setAllergyGroup(String allergyGroup) {
		this.allergyGroup = allergyGroup;
	}

	public String getAllergyStatus() {
		return this.allergyStatus;
	}

	public void setAllergyStatus(String allergyStatus) {
		this.allergyStatus = allergyStatus;
	}

	public String getAllergyType() {
		return this.allergyType;
	}

	public void setAllergyType(String allergyType) {
		this.allergyType = allergyType;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getGenericdrugmstrId() {
		return this.genericdrugmstrId;
	}

	public void setGenericdrugmstrId(BigDecimal genericdrugmstrId) {
		this.genericdrugmstrId = genericdrugmstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getProbability() {
		return this.probability;
	}

	public void setProbability(String probability) {
		this.probability = probability;
	}

	public String getReactionType() {
		return this.reactionType;
	}

	public void setReactionType(String reactionType) {
		this.reactionType = reactionType;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRoa() {
		return this.roa;
	}

	public void setRoa(String roa) {
		this.roa = roa;
	}

	public String getSystemInvolved() {
		return this.systemInvolved;
	}

	public void setSystemInvolved(String systemInvolved) {
		this.systemInvolved = systemInvolved;
	}

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}