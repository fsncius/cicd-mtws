package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the REGNAPPOINTMENTREQUISITION database table.
 * 
 */
@Entity
@NamedQuery(name="Regnappointmentrequisition.findAll", query="SELECT r FROM Regnappointmentrequisition r")
public class Regnappointmentrequisition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REGNAPPOINTMENTREQUISITION_ID")
	private long regnappointmentrequisitionId;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CAREPROVIDER_ID")
	private BigDecimal careproviderId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="FROM_DATE")
	private Date fromDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENT_ID")
	private BigDecimal patientId;

	@Column(name="REGN_TYPE")
	private String regnType;

	private String remarks;

	@Column(name="REQUISITION_NO")
	private String requisitionNo;

	@Column(name="REQUISITION_STATUS")
	private String requisitionStatus;

	@Column(name="SESSIONMSTR_ID")
	private BigDecimal sessionmstrId;

	@Column(name="SUBSPECIALTYMSTR_ID")
	private BigDecimal subspecialtymstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="TO_DATE")
	private Date toDate;

	//bi-directional many-to-one association to Regnappointment
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REGNAPPOINTMENT_ID")
	private Regnappointment regnappointment;

	public Regnappointmentrequisition() {
	}

	public long getRegnappointmentrequisitionId() {
		return this.regnappointmentrequisitionId;
	}

	public void setRegnappointmentrequisitionId(long regnappointmentrequisitionId) {
		this.regnappointmentrequisitionId = regnappointmentrequisitionId;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public BigDecimal getCareproviderId() {
		return this.careproviderId;
	}

	public void setCareproviderId(BigDecimal careproviderId) {
		this.careproviderId = careproviderId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPatientId() {
		return this.patientId;
	}

	public void setPatientId(BigDecimal patientId) {
		this.patientId = patientId;
	}

	public String getRegnType() {
		return this.regnType;
	}

	public void setRegnType(String regnType) {
		this.regnType = regnType;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRequisitionNo() {
		return this.requisitionNo;
	}

	public void setRequisitionNo(String requisitionNo) {
		this.requisitionNo = requisitionNo;
	}

	public String getRequisitionStatus() {
		return this.requisitionStatus;
	}

	public void setRequisitionStatus(String requisitionStatus) {
		this.requisitionStatus = requisitionStatus;
	}

	public BigDecimal getSessionmstrId() {
		return this.sessionmstrId;
	}

	public void setSessionmstrId(BigDecimal sessionmstrId) {
		this.sessionmstrId = sessionmstrId;
	}

	public BigDecimal getSubspecialtymstrId() {
		return this.subspecialtymstrId;
	}

	public void setSubspecialtymstrId(BigDecimal subspecialtymstrId) {
		this.subspecialtymstrId = subspecialtymstrId;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Regnappointment getRegnappointment() {
		return this.regnappointment;
	}

	public void setRegnappointment(Regnappointment regnappointment) {
		this.regnappointment = regnappointment;
	}

}