package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PATIENTTYPECLASSHISTORY database table.
 * 
 */
@Entity
@NamedQuery(name="Patienttypeclasshistory.findAll", query="SELECT p FROM Patienttypeclasshistory p")
public class Patienttypeclasshistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTTYPECLASSHISTORY_ID")
	private long patienttypeclasshistoryId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DATETIME")
	private Date endDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="NEW_PATIENT_CLASS")
	private String newPatientClass;

	@Column(name="NEW_PATIENT_TYPE")
	private String newPatientType;

	@Column(name="OLD_PATIENT_CLASS")
	private String oldPatientClass;

	@Column(name="OLD_PATIENT_TYPE")
	private String oldPatientType;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DATETIME")
	private Date startDatetime;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public Patienttypeclasshistory() {
	}

	public long getPatienttypeclasshistoryId() {
		return this.patienttypeclasshistoryId;
	}

	public void setPatienttypeclasshistoryId(long patienttypeclasshistoryId) {
		this.patienttypeclasshistoryId = patienttypeclasshistoryId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Date getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getNewPatientClass() {
		return this.newPatientClass;
	}

	public void setNewPatientClass(String newPatientClass) {
		this.newPatientClass = newPatientClass;
	}

	public String getNewPatientType() {
		return this.newPatientType;
	}

	public void setNewPatientType(String newPatientType) {
		this.newPatientType = newPatientType;
	}

	public String getOldPatientClass() {
		return this.oldPatientClass;
	}

	public void setOldPatientClass(String oldPatientClass) {
		this.oldPatientClass = oldPatientClass;
	}

	public String getOldPatientType() {
		return this.oldPatientType;
	}

	public void setOldPatientType(String oldPatientType) {
		this.oldPatientType = oldPatientType;
	}

	public Date getStartDatetime() {
		return this.startDatetime;
	}

	public void setStartDatetime(Date startDatetime) {
		this.startDatetime = startDatetime;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}