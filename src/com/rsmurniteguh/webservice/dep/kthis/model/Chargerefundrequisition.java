package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CHARGEREFUNDREQUISITION database table.
 * 
 */
@Entity
@NamedQuery(name="Chargerefundrequisition.findAll", query="SELECT c FROM Chargerefundrequisition c")
public class Chargerefundrequisition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGEREFUNDREQUISITION_ID")
	private long chargerefundrequisitionId;

	@Column(name="CHARGE_REFUNDED_BY")
	private BigDecimal chargeRefundedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CHARGE_REFUNDED_DATETIME")
	private Date chargeRefundedDatetime;

	@Column(name="DRUG_RETURNED_BY")
	private BigDecimal drugReturnedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="DRUG_RETURNED_DATETIME")
	private Date drugReturnedDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REFUND_REASON")
	private String refundReason;

	private String remarks;

	@Column(name="REQUISITION_BY")
	private BigDecimal requisitionBy;

	@Temporal(TemporalType.DATE)
	@Column(name="REQUISITION_DATETIME")
	private Date requisitionDatetime;

	@Column(name="REQUISITION_QTY")
	private BigDecimal requisitionQty;

	@Column(name="REQUISITION_STATUS")
	private String requisitionStatus;

	@Column(name="REQUISITION_UOM")
	private String requisitionUom;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REQUISITION_BY_CAREPROVIDER")
	private Careprovider careprovider;

	//bi-directional many-to-one association to Charge
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGE_ID")
	private Charge charge;

	//bi-directional many-to-one association to Drugreturn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DRUGRETURN_ID")
	private Drugreturn drugreturn;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REQUISITION_LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public Chargerefundrequisition() {
	}

	public long getChargerefundrequisitionId() {
		return this.chargerefundrequisitionId;
	}

	public void setChargerefundrequisitionId(long chargerefundrequisitionId) {
		this.chargerefundrequisitionId = chargerefundrequisitionId;
	}

	public BigDecimal getChargeRefundedBy() {
		return this.chargeRefundedBy;
	}

	public void setChargeRefundedBy(BigDecimal chargeRefundedBy) {
		this.chargeRefundedBy = chargeRefundedBy;
	}

	public Date getChargeRefundedDatetime() {
		return this.chargeRefundedDatetime;
	}

	public void setChargeRefundedDatetime(Date chargeRefundedDatetime) {
		this.chargeRefundedDatetime = chargeRefundedDatetime;
	}

	public BigDecimal getDrugReturnedBy() {
		return this.drugReturnedBy;
	}

	public void setDrugReturnedBy(BigDecimal drugReturnedBy) {
		this.drugReturnedBy = drugReturnedBy;
	}

	public Date getDrugReturnedDatetime() {
		return this.drugReturnedDatetime;
	}

	public void setDrugReturnedDatetime(Date drugReturnedDatetime) {
		this.drugReturnedDatetime = drugReturnedDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRefundReason() {
		return this.refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getRequisitionBy() {
		return this.requisitionBy;
	}

	public void setRequisitionBy(BigDecimal requisitionBy) {
		this.requisitionBy = requisitionBy;
	}

	public Date getRequisitionDatetime() {
		return this.requisitionDatetime;
	}

	public void setRequisitionDatetime(Date requisitionDatetime) {
		this.requisitionDatetime = requisitionDatetime;
	}

	public BigDecimal getRequisitionQty() {
		return this.requisitionQty;
	}

	public void setRequisitionQty(BigDecimal requisitionQty) {
		this.requisitionQty = requisitionQty;
	}

	public String getRequisitionStatus() {
		return this.requisitionStatus;
	}

	public void setRequisitionStatus(String requisitionStatus) {
		this.requisitionStatus = requisitionStatus;
	}

	public String getRequisitionUom() {
		return this.requisitionUom;
	}

	public void setRequisitionUom(String requisitionUom) {
		this.requisitionUom = requisitionUom;
	}

	public Careprovider getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(Careprovider careprovider) {
		this.careprovider = careprovider;
	}

	public Charge getCharge() {
		return this.charge;
	}

	public void setCharge(Charge charge) {
		this.charge = charge;
	}

	public Drugreturn getDrugreturn() {
		return this.drugreturn;
	}

	public void setDrugreturn(Drugreturn drugreturn) {
		this.drugreturn = drugreturn;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}