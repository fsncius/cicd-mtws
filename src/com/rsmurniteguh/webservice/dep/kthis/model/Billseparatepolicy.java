package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the BILLSEPARATEPOLICY database table.
 * 
 */
@Entity
@NamedQuery(name="Billseparatepolicy.findAll", query="SELECT b FROM Billseparatepolicy b")
public class Billseparatepolicy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BILLSEPARATEPOLICY_ID")
	private long billseparatepolicyId;

	@Column(name="BILL_SIZE_ROUNDING_RULE")
	private String billSizeRoundingRule;

	@Column(name="CHARGE_RCM_IND")
	private String chargeRcmInd;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="FORM_NO_IND")
	private String formNoInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MAX_TXN_RECNUM")
	private BigDecimal maxTxnRecnum;

	@Column(name="ORDERED_SUB_IND")
	private String orderedSubInd;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="RECEIPT_CAT")
	private String receiptCat;

	@Column(name="RECEIPT_ITEM_CAT_IND")
	private String receiptItemCatInd;

	public Billseparatepolicy() {
	}

	public long getBillseparatepolicyId() {
		return this.billseparatepolicyId;
	}

	public void setBillseparatepolicyId(long billseparatepolicyId) {
		this.billseparatepolicyId = billseparatepolicyId;
	}

	public String getBillSizeRoundingRule() {
		return this.billSizeRoundingRule;
	}

	public void setBillSizeRoundingRule(String billSizeRoundingRule) {
		this.billSizeRoundingRule = billSizeRoundingRule;
	}

	public String getChargeRcmInd() {
		return this.chargeRcmInd;
	}

	public void setChargeRcmInd(String chargeRcmInd) {
		this.chargeRcmInd = chargeRcmInd;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getFormNoInd() {
		return this.formNoInd;
	}

	public void setFormNoInd(String formNoInd) {
		this.formNoInd = formNoInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaxTxnRecnum() {
		return this.maxTxnRecnum;
	}

	public void setMaxTxnRecnum(BigDecimal maxTxnRecnum) {
		this.maxTxnRecnum = maxTxnRecnum;
	}

	public String getOrderedSubInd() {
		return this.orderedSubInd;
	}

	public void setOrderedSubInd(String orderedSubInd) {
		this.orderedSubInd = orderedSubInd;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public String getReceiptCat() {
		return this.receiptCat;
	}

	public void setReceiptCat(String receiptCat) {
		this.receiptCat = receiptCat;
	}

	public String getReceiptItemCatInd() {
		return this.receiptItemCatInd;
	}

	public void setReceiptItemCatInd(String receiptItemCatInd) {
		this.receiptItemCatInd = receiptItemCatInd;
	}

}