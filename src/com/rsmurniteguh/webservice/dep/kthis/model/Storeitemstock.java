package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOREITEMSTOCK database table.
 * 
 */
@Entity
@NamedQuery(name="Storeitemstock.findAll", query="SELECT s FROM Storeitemstock s")
public class Storeitemstock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOREITEMSTOCK_ID")
	private long storeitemstockId;

	@Column(name="BALANCE_QTY")
	private BigDecimal balanceQty;

	@Column(name="BATCH_NO")
	private String batchNo;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="LOCK_IND")
	private String lockInd;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="RESERVE_QTY")
	private BigDecimal reserveQty;

	@Column(name="STOCKRECEIPTDETAIL_ID")
	private BigDecimal stockreceiptdetailId;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Materialpriceadjustdetail
	@OneToMany(mappedBy="storeitemstock")
	private List<Materialpriceadjustdetail> materialpriceadjustdetails;

	//bi-directional many-to-one association to Materialtxndetail
	@OneToMany(mappedBy="storeitemstock")
	private List<Materialtxndetail> materialtxndetails;

	//bi-directional many-to-one association to Stockaccountclosingdetail
	@OneToMany(mappedBy="storeitemstock")
	private List<Stockaccountclosingdetail> stockaccountclosingdetails;

	//bi-directional many-to-one association to Stockcountbalance
	@OneToMany(mappedBy="storeitemstock")
	private List<Stockcountbalance> stockcountbalances;

	//bi-directional many-to-one association to Storebinmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREBINMSTR_ID")
	private Storebinmstr storebinmstr;

	//bi-directional many-to-one association to Storeitem
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREITEM_ID")
	private Storeitem storeitem;

	//bi-directional many-to-one association to Vendormstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MANUFACTURER_VENDORMSTR_ID")
	private Vendormstr vendormstr;

	public Storeitemstock() {
	}

	public long getStoreitemstockId() {
		return this.storeitemstockId;
	}

	public void setStoreitemstockId(long storeitemstockId) {
		this.storeitemstockId = storeitemstockId;
	}

	public BigDecimal getBalanceQty() {
		return this.balanceQty;
	}

	public void setBalanceQty(BigDecimal balanceQty) {
		this.balanceQty = balanceQty;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getLockInd() {
		return this.lockInd;
	}

	public void setLockInd(String lockInd) {
		this.lockInd = lockInd;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getReserveQty() {
		return this.reserveQty;
	}

	public void setReserveQty(BigDecimal reserveQty) {
		this.reserveQty = reserveQty;
	}

	public BigDecimal getStockreceiptdetailId() {
		return this.stockreceiptdetailId;
	}

	public void setStockreceiptdetailId(BigDecimal stockreceiptdetailId) {
		this.stockreceiptdetailId = stockreceiptdetailId;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public List<Materialpriceadjustdetail> getMaterialpriceadjustdetails() {
		return this.materialpriceadjustdetails;
	}

	public void setMaterialpriceadjustdetails(List<Materialpriceadjustdetail> materialpriceadjustdetails) {
		this.materialpriceadjustdetails = materialpriceadjustdetails;
	}

	public Materialpriceadjustdetail addMaterialpriceadjustdetail(Materialpriceadjustdetail materialpriceadjustdetail) {
		getMaterialpriceadjustdetails().add(materialpriceadjustdetail);
		materialpriceadjustdetail.setStoreitemstock(this);

		return materialpriceadjustdetail;
	}

	public Materialpriceadjustdetail removeMaterialpriceadjustdetail(Materialpriceadjustdetail materialpriceadjustdetail) {
		getMaterialpriceadjustdetails().remove(materialpriceadjustdetail);
		materialpriceadjustdetail.setStoreitemstock(null);

		return materialpriceadjustdetail;
	}

	public List<Materialtxndetail> getMaterialtxndetails() {
		return this.materialtxndetails;
	}

	public void setMaterialtxndetails(List<Materialtxndetail> materialtxndetails) {
		this.materialtxndetails = materialtxndetails;
	}

	public Materialtxndetail addMaterialtxndetail(Materialtxndetail materialtxndetail) {
		getMaterialtxndetails().add(materialtxndetail);
		materialtxndetail.setStoreitemstock(this);

		return materialtxndetail;
	}

	public Materialtxndetail removeMaterialtxndetail(Materialtxndetail materialtxndetail) {
		getMaterialtxndetails().remove(materialtxndetail);
		materialtxndetail.setStoreitemstock(null);

		return materialtxndetail;
	}

	public List<Stockaccountclosingdetail> getStockaccountclosingdetails() {
		return this.stockaccountclosingdetails;
	}

	public void setStockaccountclosingdetails(List<Stockaccountclosingdetail> stockaccountclosingdetails) {
		this.stockaccountclosingdetails = stockaccountclosingdetails;
	}

	public Stockaccountclosingdetail addStockaccountclosingdetail(Stockaccountclosingdetail stockaccountclosingdetail) {
		getStockaccountclosingdetails().add(stockaccountclosingdetail);
		stockaccountclosingdetail.setStoreitemstock(this);

		return stockaccountclosingdetail;
	}

	public Stockaccountclosingdetail removeStockaccountclosingdetail(Stockaccountclosingdetail stockaccountclosingdetail) {
		getStockaccountclosingdetails().remove(stockaccountclosingdetail);
		stockaccountclosingdetail.setStoreitemstock(null);

		return stockaccountclosingdetail;
	}

	public List<Stockcountbalance> getStockcountbalances() {
		return this.stockcountbalances;
	}

	public void setStockcountbalances(List<Stockcountbalance> stockcountbalances) {
		this.stockcountbalances = stockcountbalances;
	}

	public Stockcountbalance addStockcountbalance(Stockcountbalance stockcountbalance) {
		getStockcountbalances().add(stockcountbalance);
		stockcountbalance.setStoreitemstock(this);

		return stockcountbalance;
	}

	public Stockcountbalance removeStockcountbalance(Stockcountbalance stockcountbalance) {
		getStockcountbalances().remove(stockcountbalance);
		stockcountbalance.setStoreitemstock(null);

		return stockcountbalance;
	}

	public Storebinmstr getStorebinmstr() {
		return this.storebinmstr;
	}

	public void setStorebinmstr(Storebinmstr storebinmstr) {
		this.storebinmstr = storebinmstr;
	}

	public Storeitem getStoreitem() {
		return this.storeitem;
	}

	public void setStoreitem(Storeitem storeitem) {
		this.storeitem = storeitem;
	}

	public Vendormstr getVendormstr() {
		return this.vendormstr;
	}

	public void setVendormstr(Vendormstr vendormstr) {
		this.vendormstr = vendormstr;
	}

}