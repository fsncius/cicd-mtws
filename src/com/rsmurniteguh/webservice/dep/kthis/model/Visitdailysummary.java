package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the VISITDAILYSUMMARY database table.
 * 
 */
@Entity
@NamedQuery(name="Visitdailysummary.findAll", query="SELECT v FROM Visitdailysummary v")
public class Visitdailysummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITDAILYSUMMARY_ID")
	private long visitdailysummaryId;

	@Temporal(TemporalType.DATE)
	@Column(name="ADMISSION_DATETIME")
	private Date admissionDatetime;

	@Column(name="ADMIT_STATUS")
	private String admitStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="DISCHARGE_DATETIME")
	private Date dischargeDatetime;

	@Column(name="HERBAL_AMOUNT")
	private BigDecimal herbalAmount;

	@Column(name="HERBAL_CNT")
	private BigDecimal herbalCnt;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MEDICINE_AMOUNT")
	private BigDecimal medicineAmount;

	@Column(name="MEDICINE_CNT")
	private BigDecimal medicineCnt;

	@Column(name="MIO_MIC1_AMOUNT")
	private BigDecimal mioMic1Amount;

	@Column(name="MIO_MIC2_AMOUNT")
	private BigDecimal mioMic2Amount;

	@Column(name="MIO_MIC3_AMOUNT")
	private BigDecimal mioMic3Amount;

	@Column(name="NON_DRUG_AMOUNT")
	private BigDecimal nonDrugAmount;

	@Column(name="NON_DRUG_CNT")
	private BigDecimal nonDrugCnt;

	@Column(name="NON_PAY_AMOUNT")
	private BigDecimal nonPayAmount;

	@Column(name="OPERATION_IND")
	private String operationInd;

	@Column(name="PATIENT_CAT")
	private String patientCat;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="PAY_AMOUNT")
	private BigDecimal payAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="RECEIPT_DATETIME")
	private Date receiptDatetime;

	private String status;

	@Column(name="SUM_AMOUNT")
	private BigDecimal sumAmount;

	@Column(name="SUM_DAY")
	private BigDecimal sumDay;

	@Column(name="VISIT_ID")
	private BigDecimal visitId;

	@Column(name="VISIT_TYPE")
	private String visitType;

	@Column(name="WESTERN_MEDICINE_AMOUNT")
	private BigDecimal westernMedicineAmount;

	@Column(name="WESTERN_MEDICINE_CNT")
	private BigDecimal westernMedicineCnt;

	//bi-directional many-to-one association to Diagnosismstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="OP_DIAGNOSISMSTR_ID")
	private Diagnosismstr diagnosismstr1;

	//bi-directional many-to-one association to Diagnosismstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IP_DIAGNOSISMSTR_ID")
	private Diagnosismstr diagnosismstr2;

	//bi-directional many-to-one association to Diagnosismstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DISCHARGE_DIAGNOSISMSTR_ID")
	private Diagnosismstr diagnosismstr3;

	//bi-directional many-to-one association to Diagnosismstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOGIC_DIAGNOSISMSTR_ID")
	private Diagnosismstr diagnosismstr4;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr1;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DISCHARGE_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr2;

	//bi-directional many-to-one association to Visitdailysummarydetail
	@OneToMany(mappedBy="visitdailysummary")
	private List<Visitdailysummarydetail> visitdailysummarydetails;

	public Visitdailysummary() {
	}

	public long getVisitdailysummaryId() {
		return this.visitdailysummaryId;
	}

	public void setVisitdailysummaryId(long visitdailysummaryId) {
		this.visitdailysummaryId = visitdailysummaryId;
	}

	public Date getAdmissionDatetime() {
		return this.admissionDatetime;
	}

	public void setAdmissionDatetime(Date admissionDatetime) {
		this.admissionDatetime = admissionDatetime;
	}

	public String getAdmitStatus() {
		return this.admitStatus;
	}

	public void setAdmitStatus(String admitStatus) {
		this.admitStatus = admitStatus;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Date getDischargeDatetime() {
		return this.dischargeDatetime;
	}

	public void setDischargeDatetime(Date dischargeDatetime) {
		this.dischargeDatetime = dischargeDatetime;
	}

	public BigDecimal getHerbalAmount() {
		return this.herbalAmount;
	}

	public void setHerbalAmount(BigDecimal herbalAmount) {
		this.herbalAmount = herbalAmount;
	}

	public BigDecimal getHerbalCnt() {
		return this.herbalCnt;
	}

	public void setHerbalCnt(BigDecimal herbalCnt) {
		this.herbalCnt = herbalCnt;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMedicineAmount() {
		return this.medicineAmount;
	}

	public void setMedicineAmount(BigDecimal medicineAmount) {
		this.medicineAmount = medicineAmount;
	}

	public BigDecimal getMedicineCnt() {
		return this.medicineCnt;
	}

	public void setMedicineCnt(BigDecimal medicineCnt) {
		this.medicineCnt = medicineCnt;
	}

	public BigDecimal getMioMic1Amount() {
		return this.mioMic1Amount;
	}

	public void setMioMic1Amount(BigDecimal mioMic1Amount) {
		this.mioMic1Amount = mioMic1Amount;
	}

	public BigDecimal getMioMic2Amount() {
		return this.mioMic2Amount;
	}

	public void setMioMic2Amount(BigDecimal mioMic2Amount) {
		this.mioMic2Amount = mioMic2Amount;
	}

	public BigDecimal getMioMic3Amount() {
		return this.mioMic3Amount;
	}

	public void setMioMic3Amount(BigDecimal mioMic3Amount) {
		this.mioMic3Amount = mioMic3Amount;
	}

	public BigDecimal getNonDrugAmount() {
		return this.nonDrugAmount;
	}

	public void setNonDrugAmount(BigDecimal nonDrugAmount) {
		this.nonDrugAmount = nonDrugAmount;
	}

	public BigDecimal getNonDrugCnt() {
		return this.nonDrugCnt;
	}

	public void setNonDrugCnt(BigDecimal nonDrugCnt) {
		this.nonDrugCnt = nonDrugCnt;
	}

	public BigDecimal getNonPayAmount() {
		return this.nonPayAmount;
	}

	public void setNonPayAmount(BigDecimal nonPayAmount) {
		this.nonPayAmount = nonPayAmount;
	}

	public String getOperationInd() {
		return this.operationInd;
	}

	public void setOperationInd(String operationInd) {
		this.operationInd = operationInd;
	}

	public String getPatientCat() {
		return this.patientCat;
	}

	public void setPatientCat(String patientCat) {
		this.patientCat = patientCat;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public BigDecimal getPayAmount() {
		return this.payAmount;
	}

	public void setPayAmount(BigDecimal payAmount) {
		this.payAmount = payAmount;
	}

	public Date getReceiptDatetime() {
		return this.receiptDatetime;
	}

	public void setReceiptDatetime(Date receiptDatetime) {
		this.receiptDatetime = receiptDatetime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getSumAmount() {
		return this.sumAmount;
	}

	public void setSumAmount(BigDecimal sumAmount) {
		this.sumAmount = sumAmount;
	}

	public BigDecimal getSumDay() {
		return this.sumDay;
	}

	public void setSumDay(BigDecimal sumDay) {
		this.sumDay = sumDay;
	}

	public BigDecimal getVisitId() {
		return this.visitId;
	}

	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}

	public String getVisitType() {
		return this.visitType;
	}

	public void setVisitType(String visitType) {
		this.visitType = visitType;
	}

	public BigDecimal getWesternMedicineAmount() {
		return this.westernMedicineAmount;
	}

	public void setWesternMedicineAmount(BigDecimal westernMedicineAmount) {
		this.westernMedicineAmount = westernMedicineAmount;
	}

	public BigDecimal getWesternMedicineCnt() {
		return this.westernMedicineCnt;
	}

	public void setWesternMedicineCnt(BigDecimal westernMedicineCnt) {
		this.westernMedicineCnt = westernMedicineCnt;
	}

	public Diagnosismstr getDiagnosismstr1() {
		return this.diagnosismstr1;
	}

	public void setDiagnosismstr1(Diagnosismstr diagnosismstr1) {
		this.diagnosismstr1 = diagnosismstr1;
	}

	public Diagnosismstr getDiagnosismstr2() {
		return this.diagnosismstr2;
	}

	public void setDiagnosismstr2(Diagnosismstr diagnosismstr2) {
		this.diagnosismstr2 = diagnosismstr2;
	}

	public Diagnosismstr getDiagnosismstr3() {
		return this.diagnosismstr3;
	}

	public void setDiagnosismstr3(Diagnosismstr diagnosismstr3) {
		this.diagnosismstr3 = diagnosismstr3;
	}

	public Diagnosismstr getDiagnosismstr4() {
		return this.diagnosismstr4;
	}

	public void setDiagnosismstr4(Diagnosismstr diagnosismstr4) {
		this.diagnosismstr4 = diagnosismstr4;
	}

	public Subspecialtymstr getSubspecialtymstr1() {
		return this.subspecialtymstr1;
	}

	public void setSubspecialtymstr1(Subspecialtymstr subspecialtymstr1) {
		this.subspecialtymstr1 = subspecialtymstr1;
	}

	public Subspecialtymstr getSubspecialtymstr2() {
		return this.subspecialtymstr2;
	}

	public void setSubspecialtymstr2(Subspecialtymstr subspecialtymstr2) {
		this.subspecialtymstr2 = subspecialtymstr2;
	}

	public List<Visitdailysummarydetail> getVisitdailysummarydetails() {
		return this.visitdailysummarydetails;
	}

	public void setVisitdailysummarydetails(List<Visitdailysummarydetail> visitdailysummarydetails) {
		this.visitdailysummarydetails = visitdailysummarydetails;
	}

	public Visitdailysummarydetail addVisitdailysummarydetail(Visitdailysummarydetail visitdailysummarydetail) {
		getVisitdailysummarydetails().add(visitdailysummarydetail);
		visitdailysummarydetail.setVisitdailysummary(this);

		return visitdailysummarydetail;
	}

	public Visitdailysummarydetail removeVisitdailysummarydetail(Visitdailysummarydetail visitdailysummarydetail) {
		getVisitdailysummarydetails().remove(visitdailysummarydetail);
		visitdailysummarydetail.setVisitdailysummary(null);

		return visitdailysummarydetail;
	}

}