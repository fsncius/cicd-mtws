package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the DIAGNOSISCATEGORYMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Diagnosiscategorymstr.findAll", query="SELECT d FROM Diagnosiscategorymstr d")
public class Diagnosiscategorymstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DIAGNOSISCATEGORYMSTR_ID")
	private long diagnosiscategorymstrId;

	@Column(name="CODE_SCOPE")
	private String codeScope;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DIAGNOSISCATEGORY_DESC")
	private String diagnosiscategoryDesc;

	@Column(name="DIAGNOSISCATEGORY_SEQUENCE")
	private BigDecimal diagnosiscategorySequence;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="\"VERSION\"")
	private String version;

	//bi-directional many-to-one association to Diagnosiscategorymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PARENT_ID")
	private Diagnosiscategorymstr diagnosiscategorymstr;

	//bi-directional many-to-one association to Diagnosiscategorymstr
	@OneToMany(mappedBy="diagnosiscategorymstr")
	private List<Diagnosiscategorymstr> diagnosiscategorymstrs;

	//bi-directional many-to-one association to Diagnosismstr
	@OneToMany(mappedBy="diagnosiscategorymstr")
	private List<Diagnosismstr> diagnosismstrs;

	public Diagnosiscategorymstr() {
	}

	public long getDiagnosiscategorymstrId() {
		return this.diagnosiscategorymstrId;
	}

	public void setDiagnosiscategorymstrId(long diagnosiscategorymstrId) {
		this.diagnosiscategorymstrId = diagnosiscategorymstrId;
	}

	public String getCodeScope() {
		return this.codeScope;
	}

	public void setCodeScope(String codeScope) {
		this.codeScope = codeScope;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDiagnosiscategoryDesc() {
		return this.diagnosiscategoryDesc;
	}

	public void setDiagnosiscategoryDesc(String diagnosiscategoryDesc) {
		this.diagnosiscategoryDesc = diagnosiscategoryDesc;
	}

	public BigDecimal getDiagnosiscategorySequence() {
		return this.diagnosiscategorySequence;
	}

	public void setDiagnosiscategorySequence(BigDecimal diagnosiscategorySequence) {
		this.diagnosiscategorySequence = diagnosiscategorySequence;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Diagnosiscategorymstr getDiagnosiscategorymstr() {
		return this.diagnosiscategorymstr;
	}

	public void setDiagnosiscategorymstr(Diagnosiscategorymstr diagnosiscategorymstr) {
		this.diagnosiscategorymstr = diagnosiscategorymstr;
	}

	public List<Diagnosiscategorymstr> getDiagnosiscategorymstrs() {
		return this.diagnosiscategorymstrs;
	}

	public void setDiagnosiscategorymstrs(List<Diagnosiscategorymstr> diagnosiscategorymstrs) {
		this.diagnosiscategorymstrs = diagnosiscategorymstrs;
	}

	public Diagnosiscategorymstr addDiagnosiscategorymstr(Diagnosiscategorymstr diagnosiscategorymstr) {
		getDiagnosiscategorymstrs().add(diagnosiscategorymstr);
		diagnosiscategorymstr.setDiagnosiscategorymstr(this);

		return diagnosiscategorymstr;
	}

	public Diagnosiscategorymstr removeDiagnosiscategorymstr(Diagnosiscategorymstr diagnosiscategorymstr) {
		getDiagnosiscategorymstrs().remove(diagnosiscategorymstr);
		diagnosiscategorymstr.setDiagnosiscategorymstr(null);

		return diagnosiscategorymstr;
	}

	public List<Diagnosismstr> getDiagnosismstrs() {
		return this.diagnosismstrs;
	}

	public void setDiagnosismstrs(List<Diagnosismstr> diagnosismstrs) {
		this.diagnosismstrs = diagnosismstrs;
	}

	public Diagnosismstr addDiagnosismstr(Diagnosismstr diagnosismstr) {
		getDiagnosismstrs().add(diagnosismstr);
		diagnosismstr.setDiagnosiscategorymstr(this);

		return diagnosismstr;
	}

	public Diagnosismstr removeDiagnosismstr(Diagnosismstr diagnosismstr) {
		getDiagnosismstrs().remove(diagnosismstr);
		diagnosismstr.setDiagnosiscategorymstr(null);

		return diagnosismstr;
	}

}