package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CHARGEENTRYDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Chargeentrydetail.findAll", query="SELECT c FROM Chargeentrydetail c")
public class Chargeentrydetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGEENTRYDETAIL_ID")
	private long chargeentrydetailId;

	@Column(name="ACTUAL_UNIT_PRICE")
	private BigDecimal actualUnitPrice;

	@Column(name="CHARGE_ID")
	private BigDecimal chargeId;

	@Column(name="COST_PRICE")
	private BigDecimal costPrice;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private BigDecimal qty;

	@Column(name="QTY_UOM")
	private String qtyUom;

	@Column(name="STANDARD_UNIT_PRICE")
	private BigDecimal standardUnitPrice;

	@Column(name="TXN_DESC")
	private String txnDesc;

	@Column(name="TXNCODEMSTR_ID")
	private BigDecimal txncodemstrId;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Chargeentry
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEENTRY_ID")
	private Chargeentry chargeentry;

	public Chargeentrydetail() {
	}

	public long getChargeentrydetailId() {
		return this.chargeentrydetailId;
	}

	public void setChargeentrydetailId(long chargeentrydetailId) {
		this.chargeentrydetailId = chargeentrydetailId;
	}

	public BigDecimal getActualUnitPrice() {
		return this.actualUnitPrice;
	}

	public void setActualUnitPrice(BigDecimal actualUnitPrice) {
		this.actualUnitPrice = actualUnitPrice;
	}

	public BigDecimal getChargeId() {
		return this.chargeId;
	}

	public void setChargeId(BigDecimal chargeId) {
		this.chargeId = chargeId;
	}

	public BigDecimal getCostPrice() {
		return this.costPrice;
	}

	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getQtyUom() {
		return this.qtyUom;
	}

	public void setQtyUom(String qtyUom) {
		this.qtyUom = qtyUom;
	}

	public BigDecimal getStandardUnitPrice() {
		return this.standardUnitPrice;
	}

	public void setStandardUnitPrice(BigDecimal standardUnitPrice) {
		this.standardUnitPrice = standardUnitPrice;
	}

	public String getTxnDesc() {
		return this.txnDesc;
	}

	public void setTxnDesc(String txnDesc) {
		this.txnDesc = txnDesc;
	}

	public BigDecimal getTxncodemstrId() {
		return this.txncodemstrId;
	}

	public void setTxncodemstrId(BigDecimal txncodemstrId) {
		this.txncodemstrId = txncodemstrId;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Chargeentry getChargeentry() {
		return this.chargeentry;
	}

	public void setChargeentry(Chargeentry chargeentry) {
		this.chargeentry = chargeentry;
	}

}