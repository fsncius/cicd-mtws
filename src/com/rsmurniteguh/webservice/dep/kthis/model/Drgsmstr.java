package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the DRGSMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Drgsmstr.findAll", query="SELECT d FROM Drgsmstr d")
public class Drgsmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DRGSMSTR_ID")
	private long drgsmstrId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DRGS_CAT")
	private String drgsCat;

	@Column(name="DRGS_CODE")
	private String drgsCode;

	@Column(name="DRGS_DESC")
	private String drgsDesc;

	@Column(name="DRGS_DESC_LANG1")
	private String drgsDescLang1;

	@Column(name="DRGS_PRICE")
	private BigDecimal drgsPrice;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="YOUNG_LARGE_IND")
	private String youngLargeInd;

	//bi-directional many-to-one association to Visit
	@OneToMany(mappedBy="drgsmstr")
	private List<Visit> visits;

	public Drgsmstr() {
	}

	public long getDrgsmstrId() {
		return this.drgsmstrId;
	}

	public void setDrgsmstrId(long drgsmstrId) {
		this.drgsmstrId = drgsmstrId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDrgsCat() {
		return this.drgsCat;
	}

	public void setDrgsCat(String drgsCat) {
		this.drgsCat = drgsCat;
	}

	public String getDrgsCode() {
		return this.drgsCode;
	}

	public void setDrgsCode(String drgsCode) {
		this.drgsCode = drgsCode;
	}

	public String getDrgsDesc() {
		return this.drgsDesc;
	}

	public void setDrgsDesc(String drgsDesc) {
		this.drgsDesc = drgsDesc;
	}

	public String getDrgsDescLang1() {
		return this.drgsDescLang1;
	}

	public void setDrgsDescLang1(String drgsDescLang1) {
		this.drgsDescLang1 = drgsDescLang1;
	}

	public BigDecimal getDrgsPrice() {
		return this.drgsPrice;
	}

	public void setDrgsPrice(BigDecimal drgsPrice) {
		this.drgsPrice = drgsPrice;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getYoungLargeInd() {
		return this.youngLargeInd;
	}

	public void setYoungLargeInd(String youngLargeInd) {
		this.youngLargeInd = youngLargeInd;
	}

	public List<Visit> getVisits() {
		return this.visits;
	}

	public void setVisits(List<Visit> visits) {
		this.visits = visits;
	}

	public Visit addVisit(Visit visit) {
		getVisits().add(visit);
		visit.setDrgsmstr(this);

		return visit;
	}

	public Visit removeVisit(Visit visit) {
		getVisits().remove(visit);
		visit.setDrgsmstr(null);

		return visit;
	}

}