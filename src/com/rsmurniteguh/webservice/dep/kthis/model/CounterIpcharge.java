package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the COUNTER_IPCHARGE database table.
 * 
 */
@Entity
@Table(name="COUNTER_IPCHARGE")
@NamedQuery(name="CounterIpcharge.findAll", query="SELECT c FROM CounterIpcharge c")
public class CounterIpcharge implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTER_IPCHARGE_ID")
	private long counterIpchargeId;

	@Column(name="ADMISSION_COUNT")
	private BigDecimal admissionCount;

	@Column(name="ADMISSION_COUNT_1")
	private BigDecimal admissionCount1;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DISCHARGE_COUNT")
	private BigDecimal dischargeCount;

	@Column(name="DISCHARGE_COUNT_1")
	private BigDecimal dischargeCount1;

	@Column(name="HOSPITAL_COUNT")
	private BigDecimal hospitalCount;

	@Column(name="HOSPITAL_COUNT_1")
	private BigDecimal hospitalCount1;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="TODAY_AMOUNT")
	private BigDecimal todayAmount;

	@Column(name="TODAY_BALANCE")
	private BigDecimal todayBalance;

	@Column(name="TODAY_DISCHARGEAMOUNT")
	private BigDecimal todayDischargeamount;

	@Column(name="TODAY_FRACTIONAMOUNT")
	private BigDecimal todayFractionamount;

	@Column(name="YESTERDAY_BALANCE")
	private BigDecimal yesterdayBalance;

	//bi-directional many-to-one association to Countertotal
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COUNTERTOTAL_ID")
	private Countertotal countertotal;

	public CounterIpcharge() {
	}

	public long getCounterIpchargeId() {
		return this.counterIpchargeId;
	}

	public void setCounterIpchargeId(long counterIpchargeId) {
		this.counterIpchargeId = counterIpchargeId;
	}

	public BigDecimal getAdmissionCount() {
		return this.admissionCount;
	}

	public void setAdmissionCount(BigDecimal admissionCount) {
		this.admissionCount = admissionCount;
	}

	public BigDecimal getAdmissionCount1() {
		return this.admissionCount1;
	}

	public void setAdmissionCount1(BigDecimal admissionCount1) {
		this.admissionCount1 = admissionCount1;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getDischargeCount() {
		return this.dischargeCount;
	}

	public void setDischargeCount(BigDecimal dischargeCount) {
		this.dischargeCount = dischargeCount;
	}

	public BigDecimal getDischargeCount1() {
		return this.dischargeCount1;
	}

	public void setDischargeCount1(BigDecimal dischargeCount1) {
		this.dischargeCount1 = dischargeCount1;
	}

	public BigDecimal getHospitalCount() {
		return this.hospitalCount;
	}

	public void setHospitalCount(BigDecimal hospitalCount) {
		this.hospitalCount = hospitalCount;
	}

	public BigDecimal getHospitalCount1() {
		return this.hospitalCount1;
	}

	public void setHospitalCount1(BigDecimal hospitalCount1) {
		this.hospitalCount1 = hospitalCount1;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getTodayAmount() {
		return this.todayAmount;
	}

	public void setTodayAmount(BigDecimal todayAmount) {
		this.todayAmount = todayAmount;
	}

	public BigDecimal getTodayBalance() {
		return this.todayBalance;
	}

	public void setTodayBalance(BigDecimal todayBalance) {
		this.todayBalance = todayBalance;
	}

	public BigDecimal getTodayDischargeamount() {
		return this.todayDischargeamount;
	}

	public void setTodayDischargeamount(BigDecimal todayDischargeamount) {
		this.todayDischargeamount = todayDischargeamount;
	}

	public BigDecimal getTodayFractionamount() {
		return this.todayFractionamount;
	}

	public void setTodayFractionamount(BigDecimal todayFractionamount) {
		this.todayFractionamount = todayFractionamount;
	}

	public BigDecimal getYesterdayBalance() {
		return this.yesterdayBalance;
	}

	public void setYesterdayBalance(BigDecimal yesterdayBalance) {
		this.yesterdayBalance = yesterdayBalance;
	}

	public Countertotal getCountertotal() {
		return this.countertotal;
	}

	public void setCountertotal(Countertotal countertotal) {
		this.countertotal = countertotal;
	}

}