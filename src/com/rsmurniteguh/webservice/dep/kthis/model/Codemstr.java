package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CODEMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Codemstr.findAll", query="SELECT c FROM Codemstr c")
public class Codemstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CODE_ABBR")
	private String codeAbbr;

	@Column(name="CODE_CAT")
	private String codeCat;

	@Column(name="CODE_DESC")
	private String codeDesc;

	@Column(name="CODE_DESC_LANG1")
	private String codeDescLang1;

	@Column(name="CODE_DESC_LANG2")
	private String codeDescLang2;

	@Column(name="CODE_DESC_LANG3")
	private String codeDescLang3;

	@Column(name="CODE_SEQ")
	private BigDecimal codeSeq;

	@Column(name="CODEMSTR_ID")
	private BigDecimal codemstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;
	@Id
	@Column(name="ENTITYMSTR_ID")
	private BigDecimal entitymstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	private String magic;

	@Column(name="MODULE_CAT")
	private String moduleCat;

	@Column(name="PARENT_CODE_CAT")
	private String parentCodeCat;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="SYSTEM_IND")
	private String systemInd;

	public Codemstr() {
	}

	public String getCodeAbbr() {
		return this.codeAbbr;
	}

	public void setCodeAbbr(String codeAbbr) {
		this.codeAbbr = codeAbbr;
	}

	public String getCodeCat() {
		return this.codeCat;
	}

	public void setCodeCat(String codeCat) {
		this.codeCat = codeCat;
	}

	public String getCodeDesc() {
		return this.codeDesc;
	}

	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}

	public String getCodeDescLang1() {
		return this.codeDescLang1;
	}

	public void setCodeDescLang1(String codeDescLang1) {
		this.codeDescLang1 = codeDescLang1;
	}

	public String getCodeDescLang2() {
		return this.codeDescLang2;
	}

	public void setCodeDescLang2(String codeDescLang2) {
		this.codeDescLang2 = codeDescLang2;
	}

	public String getCodeDescLang3() {
		return this.codeDescLang3;
	}

	public void setCodeDescLang3(String codeDescLang3) {
		this.codeDescLang3 = codeDescLang3;
	}

	public BigDecimal getCodeSeq() {
		return this.codeSeq;
	}

	public void setCodeSeq(BigDecimal codeSeq) {
		this.codeSeq = codeSeq;
	}

	public BigDecimal getCodemstrId() {
		return this.codemstrId;
	}

	public void setCodemstrId(BigDecimal codemstrId) {
		this.codemstrId = codemstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getEntitymstrId() {
		return this.entitymstrId;
	}

	public void setEntitymstrId(BigDecimal entitymstrId) {
		this.entitymstrId = entitymstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMagic() {
		return this.magic;
	}

	public void setMagic(String magic) {
		this.magic = magic;
	}

	public String getModuleCat() {
		return this.moduleCat;
	}

	public void setModuleCat(String moduleCat) {
		this.moduleCat = moduleCat;
	}

	public String getParentCodeCat() {
		return this.parentCodeCat;
	}

	public void setParentCodeCat(String parentCodeCat) {
		this.parentCodeCat = parentCodeCat;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getSystemInd() {
		return this.systemInd;
	}

	public void setSystemInd(String systemInd) {
		this.systemInd = systemInd;
	}

}