package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the REFUNDBILLEDPOLICY database table.
 * 
 */
@Entity
@NamedQuery(name="Refundbilledpolicy.findAll", query="SELECT r FROM Refundbilledpolicy r")
public class Refundbilledpolicy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REFUNDBILLEDPOLICY_ID")
	private long refundbilledpolicyId;

	@Column(name="ALLOW_IND")
	private String allowInd;

	@Column(name="BILLING_TYPE")
	private String billingType;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	public Refundbilledpolicy() {
	}

	public long getRefundbilledpolicyId() {
		return this.refundbilledpolicyId;
	}

	public void setRefundbilledpolicyId(long refundbilledpolicyId) {
		this.refundbilledpolicyId = refundbilledpolicyId;
	}

	public String getAllowInd() {
		return this.allowInd;
	}

	public void setAllowInd(String allowInd) {
		this.allowInd = allowInd;
	}

	public String getBillingType() {
		return this.billingType;
	}

	public void setBillingType(String billingType) {
		this.billingType = billingType;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

}