package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the VISITREGNTYPE database table.
 * 
 */
@Entity
@NamedQuery(name="Visitregntype.findAll", query="SELECT v FROM Visitregntype v")
public class Visitregntype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITREGNTYPE_ID")
	private long visitregntypeId;

	@Column(name="APPOINTMENTRESOURCE_ID")
	private BigDecimal appointmentresourceId;

	@Column(name="CANCEL_REASON")
	private String cancelReason;

	@Column(name="CANCEL_REMARKS")
	private String cancelRemarks;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CREATED_AT")
	private BigDecimal createdAt;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFICTIVE_DATETIME")
	private Date effictiveDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REFERAL_CAREPROVIDER_ID")
	private BigDecimal referalCareproviderId;

	@Column(name="REGISTRATION_METHOD")
	private String registrationMethod;

	@Column(name="REGISTRATION_TYPE")
	private String registrationType;

	@Column(name="REGN_NO")
	private String regnNo;

	private String remarks;

	@Column(name="RESOURCEMSTR_ID")
	private BigDecimal resourcemstrId;

	@Column(name="SESSIONMSTR_ID")
	private BigDecimal sessionmstrId;

	@Column(name="TIMESLOT_ID")
	private BigDecimal timeslotId;

	@Column(name="VISIT_TYPE")
	private String visitType;

	//bi-directional many-to-one association to Regnappointment
	@OneToMany(mappedBy="visitregntype")
	private List<Regnappointment> regnappointments;

	//bi-directional many-to-one association to Regnvisitactivation
	@OneToMany(mappedBy="visitregntype")
	private List<Regnvisitactivation> regnvisitactivations;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CAREPROVIDER_ID")
	private Careprovider careprovider;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Visitregntypecharge
	@OneToMany(mappedBy="visitregntype")
	private List<Visitregntypecharge> visitregntypecharges;

	public Visitregntype() {
	}

	public long getVisitregntypeId() {
		return this.visitregntypeId;
	}

	public void setVisitregntypeId(long visitregntypeId) {
		this.visitregntypeId = visitregntypeId;
	}

	public BigDecimal getAppointmentresourceId() {
		return this.appointmentresourceId;
	}

	public void setAppointmentresourceId(BigDecimal appointmentresourceId) {
		this.appointmentresourceId = appointmentresourceId;
	}

	public String getCancelReason() {
		return this.cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getCancelRemarks() {
		return this.cancelRemarks;
	}

	public void setCancelRemarks(String cancelRemarks) {
		this.cancelRemarks = cancelRemarks;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public BigDecimal getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(BigDecimal createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffictiveDatetime() {
		return this.effictiveDatetime;
	}

	public void setEffictiveDatetime(Date effictiveDatetime) {
		this.effictiveDatetime = effictiveDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getReferalCareproviderId() {
		return this.referalCareproviderId;
	}

	public void setReferalCareproviderId(BigDecimal referalCareproviderId) {
		this.referalCareproviderId = referalCareproviderId;
	}

	public String getRegistrationMethod() {
		return this.registrationMethod;
	}

	public void setRegistrationMethod(String registrationMethod) {
		this.registrationMethod = registrationMethod;
	}

	public String getRegistrationType() {
		return this.registrationType;
	}

	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}

	public String getRegnNo() {
		return this.regnNo;
	}

	public void setRegnNo(String regnNo) {
		this.regnNo = regnNo;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getResourcemstrId() {
		return this.resourcemstrId;
	}

	public void setResourcemstrId(BigDecimal resourcemstrId) {
		this.resourcemstrId = resourcemstrId;
	}

	public BigDecimal getSessionmstrId() {
		return this.sessionmstrId;
	}

	public void setSessionmstrId(BigDecimal sessionmstrId) {
		this.sessionmstrId = sessionmstrId;
	}

	public BigDecimal getTimeslotId() {
		return this.timeslotId;
	}

	public void setTimeslotId(BigDecimal timeslotId) {
		this.timeslotId = timeslotId;
	}

	public String getVisitType() {
		return this.visitType;
	}

	public void setVisitType(String visitType) {
		this.visitType = visitType;
	}

	public List<Regnappointment> getRegnappointments() {
		return this.regnappointments;
	}

	public void setRegnappointments(List<Regnappointment> regnappointments) {
		this.regnappointments = regnappointments;
	}

	public Regnappointment addRegnappointment(Regnappointment regnappointment) {
		getRegnappointments().add(regnappointment);
		regnappointment.setVisitregntype(this);

		return regnappointment;
	}

	public Regnappointment removeRegnappointment(Regnappointment regnappointment) {
		getRegnappointments().remove(regnappointment);
		regnappointment.setVisitregntype(null);

		return regnappointment;
	}

	public List<Regnvisitactivation> getRegnvisitactivations() {
		return this.regnvisitactivations;
	}

	public void setRegnvisitactivations(List<Regnvisitactivation> regnvisitactivations) {
		this.regnvisitactivations = regnvisitactivations;
	}

	public Regnvisitactivation addRegnvisitactivation(Regnvisitactivation regnvisitactivation) {
		getRegnvisitactivations().add(regnvisitactivation);
		regnvisitactivation.setVisitregntype(this);

		return regnvisitactivation;
	}

	public Regnvisitactivation removeRegnvisitactivation(Regnvisitactivation regnvisitactivation) {
		getRegnvisitactivations().remove(regnvisitactivation);
		regnvisitactivation.setVisitregntype(null);

		return regnvisitactivation;
	}

	public Careprovider getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(Careprovider careprovider) {
		this.careprovider = careprovider;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public List<Visitregntypecharge> getVisitregntypecharges() {
		return this.visitregntypecharges;
	}

	public void setVisitregntypecharges(List<Visitregntypecharge> visitregntypecharges) {
		this.visitregntypecharges = visitregntypecharges;
	}

	public Visitregntypecharge addVisitregntypecharge(Visitregntypecharge visitregntypecharge) {
		getVisitregntypecharges().add(visitregntypecharge);
		visitregntypecharge.setVisitregntype(this);

		return visitregntypecharge;
	}

	public Visitregntypecharge removeVisitregntypecharge(Visitregntypecharge visitregntypecharge) {
		getVisitregntypecharges().remove(visitregntypecharge);
		visitregntypecharge.setVisitregntype(null);

		return visitregntypecharge;
	}

}