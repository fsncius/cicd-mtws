package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity(name = "TXNCODEMSTR")
public class TxnCodeMstr implements Serializable {

    public TxnCodeMstr() {}

    @Transient
    private Set<String> modifiedSet = new HashSet<String>();
    public Set<String> getModifiedSet() {
        return modifiedSet;
    }

    // FIELDS

    @Id
    @Column(name = "TXNCODEMSTR_ID")
    private BigDecimal txnCodeMstrId;
    @Column(name = "TXNTYPEMSTR_ID")
    private BigDecimal txnTypeMstrId;
    @Column(name = "PARENT_TXNCODEMSTR_ID")
    private BigDecimal parentTxnCodeMstrId;
    @Column(name = "BILLINGSUMMARYMSTR_ID")
    private BigDecimal billingSummaryMstrId;
    @Column(name = "PREV_UPDATED_BY")
    private BigDecimal prevUpdatedBy;
    @Column(name = "LAST_UPDATED_BY")
    private BigDecimal lastUpdatedBy;
    @Column(name = "GLACCOUNTMSTR_ID")
    private BigDecimal glAccountMstrId;
    @Column(name = "PREV_UPDATED_DATETIME")
    private Timestamp prevUpdatedDateTime;
    @Column(name = "LAST_UPDATED_DATETIME")
    private Timestamp lastUpdatedDateTime;
    @Column(name = "DEFUNCT_IND")
    private String defunctInd;
    @Column(name = "ITEM_CODE_TYPE")
    private String itemCodeType;
    @Column(name = "TXN_CODE")
    private String txnCode;
    @Column(name = "SHORT_CODE")
    private String shortCode;
    @Column(name = "TXN_DESC")
    private String txnDesc;
    @Column(name = "TXN_DESC_LANG1")
    private String txnDescLang1;
    @Column(name = "TXN_DESC_LANG2")
    private String txnDescLang2;
    @Column(name = "TXN_DESC_LANG3")
    private String txnDescLang3;

    @Transient
    private boolean _txnCodeMstrIdFlag;
    @Transient
    private boolean _txnTypeMstrIdFlag;
    @Transient
    private boolean _parentTxnCodeMstrIdFlag;
    @Transient
    private boolean _billingSummaryMstrIdFlag;
    @Transient
    private boolean _prevUpdatedByFlag;
    @Transient
    private boolean _lastUpdatedByFlag;
    @Transient
    private boolean _glAccountMstrIdFlag;
    @Transient
    private boolean _prevUpdatedDateTimeFlag;
    @Transient
    private boolean _lastUpdatedDateTimeFlag;
    @Transient
    private boolean _defunctIndFlag;
    @Transient
    private boolean _itemCodeTypeFlag;
    @Transient
    private boolean _txnCodeFlag;
    @Transient
    private boolean _shortCodeFlag;
    @Transient
    private boolean _txnDescFlag;
    @Transient
    private boolean _txnDescLang1Flag;
    @Transient
    private boolean _txnDescLang2Flag;
    @Transient
    private boolean _txnDescLang3Flag;

    // GETTERS

    public BigDecimal getTxnCodeMstrId() {
        return txnCodeMstrId;
    }

    public BigDecimal getTxnTypeMstrId() {
        return txnTypeMstrId;
    }

    public BigDecimal getParentTxnCodeMstrId() {
        return parentTxnCodeMstrId;
    }

    public BigDecimal getBillingSummaryMstrId() {
        return billingSummaryMstrId;
    }

    public BigDecimal getPrevUpdatedBy() {
        return prevUpdatedBy;
    }

    public BigDecimal getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public BigDecimal getGlAccountMstrId() {
        return glAccountMstrId;
    }

    public Timestamp getPrevUpdatedDateTime() {
        return prevUpdatedDateTime;
    }

    public Timestamp getLastUpdatedDateTime() {
        return lastUpdatedDateTime;
    }

    public String getDefunctInd() {
        return defunctInd;
    }

    public String getItemCodeType() {
        return itemCodeType;
    }

    public String getTxnCode() {
        return txnCode;
    }

    public String getShortCode() {
        return shortCode;
    }

    public String getTxnDesc() {
        return txnDesc;
    }

    public String getTxnDescLang1() {
        return txnDescLang1;
    }

    public String getTxnDescLang2() {
        return txnDescLang2;
    }

    public String getTxnDescLang3() {
        return txnDescLang3;
    }

    public boolean get_txnCodeMstrIdFlag() {
        return _txnCodeMstrIdFlag;
    }

    public boolean get_txnTypeMstrIdFlag() {
        return _txnTypeMstrIdFlag;
    }

    public boolean get_parentTxnCodeMstrIdFlag() {
        return _parentTxnCodeMstrIdFlag;
    }

    public boolean get_billingSummaryMstrIdFlag() {
        return _billingSummaryMstrIdFlag;
    }

    public boolean get_prevUpdatedByFlag() {
        return _prevUpdatedByFlag;
    }

    public boolean get_lastUpdatedByFlag() {
        return _lastUpdatedByFlag;
    }

    public boolean get_glAccountMstrIdFlag() {
        return _glAccountMstrIdFlag;
    }

    public boolean get_prevUpdatedDateTimeFlag() {
        return _prevUpdatedDateTimeFlag;
    }

    public boolean get_lastUpdatedDateTimeFlag() {
        return _lastUpdatedDateTimeFlag;
    }

    public boolean get_defunctIndFlag() {
        return _defunctIndFlag;
    }

    public boolean get_itemCodeTypeFlag() {
        return _itemCodeTypeFlag;
    }

    public boolean get_txnCodeFlag() {
        return _txnCodeFlag;
    }

    public boolean get_shortCodeFlag() {
        return _shortCodeFlag;
    }

    public boolean get_txnDescFlag() {
        return _txnDescFlag;
    }

    public boolean get_txnDescLang1Flag() {
        return _txnDescLang1Flag;
    }

    public boolean get_txnDescLang2Flag() {
        return _txnDescLang2Flag;
    }

    public boolean get_txnDescLang3Flag() {
        return _txnDescLang3Flag;
    }

    // SETTERS

    public void setTxnCodeMstrId(BigDecimal txnCodeMstrId) {
        this.txnCodeMstrId = txnCodeMstrId;
        this._txnCodeMstrIdFlag = true;
        this.modifiedSet.add("txnCodeMstrId");
    }

    public void setTxnTypeMstrId(BigDecimal txnTypeMstrId) {
        this.txnTypeMstrId = txnTypeMstrId;
        this._txnTypeMstrIdFlag = true;
        this.modifiedSet.add("txnTypeMstrId");
    }

    public void setParentTxnCodeMstrId(BigDecimal parentTxnCodeMstrId) {
        this.parentTxnCodeMstrId = parentTxnCodeMstrId;
        this._parentTxnCodeMstrIdFlag = true;
        this.modifiedSet.add("parentTxnCodeMstrId");
    }

    public void setBillingSummaryMstrId(BigDecimal billingSummaryMstrId) {
        this.billingSummaryMstrId = billingSummaryMstrId;
        this._billingSummaryMstrIdFlag = true;
        this.modifiedSet.add("billingSummaryMstrId");
    }

    public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
        this.prevUpdatedBy = prevUpdatedBy;
        this._prevUpdatedByFlag = true;
        this.modifiedSet.add("prevUpdatedBy");
    }

    public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
        this._lastUpdatedByFlag = true;
        this.modifiedSet.add("lastUpdatedBy");
    }

    public void setGlAccountMstrId(BigDecimal glAccountMstrId) {
        this.glAccountMstrId = glAccountMstrId;
        this._glAccountMstrIdFlag = true;
        this.modifiedSet.add("glAccountMstrId");
    }

    public void setPrevUpdatedDateTime(Timestamp prevUpdatedDateTime) {
        this.prevUpdatedDateTime = prevUpdatedDateTime;
        this._prevUpdatedDateTimeFlag = true;
        this.modifiedSet.add("prevUpdatedDateTime");
    }

    public void setLastUpdatedDateTime(Timestamp lastUpdatedDateTime) {
        this.lastUpdatedDateTime = lastUpdatedDateTime;
        this._lastUpdatedDateTimeFlag = true;
        this.modifiedSet.add("lastUpdatedDateTime");
    }

    public void setDefunctInd(String defunctInd) {
        this.defunctInd = defunctInd;
        this._defunctIndFlag = true;
        this.modifiedSet.add("defunctInd");
    }

    public void setItemCodeType(String itemCodeType) {
        this.itemCodeType = itemCodeType;
        this._itemCodeTypeFlag = true;
        this.modifiedSet.add("itemCodeType");
    }

    public void setTxnCode(String txnCode) {
        this.txnCode = txnCode;
        this._txnCodeFlag = true;
        this.modifiedSet.add("txnCode");
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
        this._shortCodeFlag = true;
        this.modifiedSet.add("shortCode");
    }

    public void setTxnDesc(String txnDesc) {
        this.txnDesc = txnDesc;
        this._txnDescFlag = true;
        this.modifiedSet.add("txnDesc");
    }

    public void setTxnDescLang1(String txnDescLang1) {
        this.txnDescLang1 = txnDescLang1;
        this._txnDescLang1Flag = true;
        this.modifiedSet.add("txnDescLang1");
    }

    public void setTxnDescLang2(String txnDescLang2) {
        this.txnDescLang2 = txnDescLang2;
        this._txnDescLang2Flag = true;
        this.modifiedSet.add("txnDescLang2");
    }

    public void setTxnDescLang3(String txnDescLang3) {
        this.txnDescLang3 = txnDescLang3;
        this._txnDescLang3Flag = true;
        this.modifiedSet.add("txnDescLang3");
    }

    // RESET

    public void resetModifiedFlag(boolean flag) {
        _txnCodeMstrIdFlag = flag;
        _txnTypeMstrIdFlag = flag;
        _parentTxnCodeMstrIdFlag = flag;
        _billingSummaryMstrIdFlag = flag;
        _prevUpdatedByFlag = flag;
        _lastUpdatedByFlag = flag;
        _glAccountMstrIdFlag = flag;
        _prevUpdatedDateTimeFlag = flag;
        _lastUpdatedDateTimeFlag = flag;
        _defunctIndFlag = flag;
        _itemCodeTypeFlag = flag;
        _txnCodeFlag = flag;
        _shortCodeFlag = flag;
        _txnDescFlag = flag;
        _txnDescLang1Flag = flag;
        _txnDescLang2Flag = flag;
        _txnDescLang3Flag = flag;
        if (flag) {
            modifiedSet.add("txnCodeMstrId");
            modifiedSet.add("txnTypeMstrId");
            modifiedSet.add("parentTxnCodeMstrId");
            modifiedSet.add("billingSummaryMstrId");
            modifiedSet.add("prevUpdatedBy");
            modifiedSet.add("lastUpdatedBy");
            modifiedSet.add("glAccountMstrId");
            modifiedSet.add("prevUpdatedDateTime");
            modifiedSet.add("lastUpdatedDateTime");
            modifiedSet.add("defunctInd");
            modifiedSet.add("itemCodeType");
            modifiedSet.add("txnCode");
            modifiedSet.add("shortCode");
            modifiedSet.add("txnDesc");
            modifiedSet.add("txnDescLang1");
            modifiedSet.add("txnDescLang2");
            modifiedSet.add("txnDescLang3");
        } else {
            modifiedSet.clear();
        }
    }

    public void resetModifiedFlag() {
        resetModifiedFlag(false);
    }

    // TOSTRING

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        sb.append("TxnCodeMstr: ");
        sb.append(txnCodeMstrId + ",");
        sb.append(txnTypeMstrId + ",");
        sb.append(parentTxnCodeMstrId + ",");
        sb.append(billingSummaryMstrId + ",");
        sb.append(prevUpdatedBy + ",");
        sb.append(lastUpdatedBy + ",");
        sb.append(glAccountMstrId + ",");
        sb.append(prevUpdatedDateTime + ",");
        sb.append(lastUpdatedDateTime + ",");
        sb.append(defunctInd + ",");
        sb.append(itemCodeType + ",");
        sb.append(txnCode + ",");
        sb.append(shortCode + ",");
        sb.append(txnDesc + ",");
        sb.append(txnDescLang1 + ",");
        sb.append(txnDescLang2 + ",");
        sb.append(txnDescLang3 + ",");
        sb.append("]");
        return sb.toString();
    }
}