package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITDOCTOR database table.
 * 
 */
@Entity
@NamedQuery(name="Visitdoctor.findAll", query="SELECT v FROM Visitdoctor v")
public class Visitdoctor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITDOCTOR_ID")
	private long visitdoctorId;

	@Column(name="CHARGE_IND")
	private String chargeInd;

	@Column(name="COMMISSION_IND")
	private String commissionInd;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DOCTOR_TYPE")
	private String doctorType;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DATETIME")
	private Date endDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="OTHER_REFERRAL_REASON")
	private String otherReferralReason;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REFERAL_CAREPROVIDER_ID")
	private BigDecimal referalCareproviderId;

	@Column(name="REFERRAL_CLINIC")
	private String referralClinic;

	@Column(name="REFERRAL_DOCTOR")
	private String referralDoctor;

	@Column(name="REFERRAL_DOCU_IND")
	private String referralDocuInd;

	@Column(name="REFERRAL_REASON")
	private String referralReason;

	@Column(name="REFERRAL_SOURCE")
	private String referralSource;

	private String remarks;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DATETIME")
	private Date startDatetime;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CAREPROVIDER_ID")
	private Careprovider careprovider;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="WARDMSTR_ID")
	private Wardmstr wardmstr;

	public Visitdoctor() {
	}

	public long getVisitdoctorId() {
		return this.visitdoctorId;
	}

	public void setVisitdoctorId(long visitdoctorId) {
		this.visitdoctorId = visitdoctorId;
	}

	public String getChargeInd() {
		return this.chargeInd;
	}

	public void setChargeInd(String chargeInd) {
		this.chargeInd = chargeInd;
	}

	public String getCommissionInd() {
		return this.commissionInd;
	}

	public void setCommissionInd(String commissionInd) {
		this.commissionInd = commissionInd;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDoctorType() {
		return this.doctorType;
	}

	public void setDoctorType(String doctorType) {
		this.doctorType = doctorType;
	}

	public Date getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getOtherReferralReason() {
		return this.otherReferralReason;
	}

	public void setOtherReferralReason(String otherReferralReason) {
		this.otherReferralReason = otherReferralReason;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getReferalCareproviderId() {
		return this.referalCareproviderId;
	}

	public void setReferalCareproviderId(BigDecimal referalCareproviderId) {
		this.referalCareproviderId = referalCareproviderId;
	}

	public String getReferralClinic() {
		return this.referralClinic;
	}

	public void setReferralClinic(String referralClinic) {
		this.referralClinic = referralClinic;
	}

	public String getReferralDoctor() {
		return this.referralDoctor;
	}

	public void setReferralDoctor(String referralDoctor) {
		this.referralDoctor = referralDoctor;
	}

	public String getReferralDocuInd() {
		return this.referralDocuInd;
	}

	public void setReferralDocuInd(String referralDocuInd) {
		this.referralDocuInd = referralDocuInd;
	}

	public String getReferralReason() {
		return this.referralReason;
	}

	public void setReferralReason(String referralReason) {
		this.referralReason = referralReason;
	}

	public String getReferralSource() {
		return this.referralSource;
	}

	public void setReferralSource(String referralSource) {
		this.referralSource = referralSource;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getStartDatetime() {
		return this.startDatetime;
	}

	public void setStartDatetime(Date startDatetime) {
		this.startDatetime = startDatetime;
	}

	public Careprovider getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(Careprovider careprovider) {
		this.careprovider = careprovider;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

}