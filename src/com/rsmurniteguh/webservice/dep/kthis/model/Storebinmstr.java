package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOREBINMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Storebinmstr.findAll", query="SELECT s FROM Storebinmstr s")
public class Storebinmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOREBINMSTR_ID")
	private long storebinmstrId;

	@Column(name="BIN_CODE")
	private String binCode;

	@Column(name="BIN_DESC")
	private String binDesc;

	@Column(name="BIN_DESC_LANG1")
	private String binDescLang1;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;


	//bi-directional many-to-one association to Stockcountdetail
	@OneToMany(mappedBy="storebinmstr")
	private List<Stockcountdetail> stockcountdetails;


	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREMSTR_ID")
	private Storemstr storemstr;

	//bi-directional many-to-one association to Storecountdefinedetail
	@OneToMany(mappedBy="storebinmstr")
	private List<Storecountdefinedetail> storecountdefinedetails;

	//bi-directional many-to-one association to Storeitembin
	@OneToMany(mappedBy="storebinmstr")
	private List<Storeitembin> storeitembins;

	//bi-directional many-to-one association to Storeitemstock
	@OneToMany(mappedBy="storebinmstr")
	private List<Storeitemstock> storeitemstocks;

	//bi-directional many-to-one association to Wardsspmstocktxn
	@OneToMany(mappedBy="storebinmstr")
	private List<Wardsspmstocktxn> wardsspmstocktxns;

	public Storebinmstr() {
	}

	public long getStorebinmstrId() {
		return this.storebinmstrId;
	}

	public void setStorebinmstrId(long storebinmstrId) {
		this.storebinmstrId = storebinmstrId;
	}

	public String getBinCode() {
		return this.binCode;
	}

	public void setBinCode(String binCode) {
		this.binCode = binCode;
	}

	public String getBinDesc() {
		return this.binDesc;
	}

	public void setBinDesc(String binDesc) {
		this.binDesc = binDesc;
	}

	public String getBinDescLang1() {
		return this.binDescLang1;
	}

	public void setBinDescLang1(String binDescLang1) {
		this.binDescLang1 = binDescLang1;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}


	public List<Stockcountdetail> getStockcountdetails() {
		return this.stockcountdetails;
	}

	public void setStockcountdetails(List<Stockcountdetail> stockcountdetails) {
		this.stockcountdetails = stockcountdetails;
	}

	public Stockcountdetail addStockcountdetail(Stockcountdetail stockcountdetail) {
		getStockcountdetails().add(stockcountdetail);
		stockcountdetail.setStorebinmstr(this);

		return stockcountdetail;
	}

	public Stockcountdetail removeStockcountdetail(Stockcountdetail stockcountdetail) {
		getStockcountdetails().remove(stockcountdetail);
		stockcountdetail.setStorebinmstr(null);

		return stockcountdetail;
	}


	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

	public List<Storecountdefinedetail> getStorecountdefinedetails() {
		return this.storecountdefinedetails;
	}

	public void setStorecountdefinedetails(List<Storecountdefinedetail> storecountdefinedetails) {
		this.storecountdefinedetails = storecountdefinedetails;
	}

	public Storecountdefinedetail addStorecountdefinedetail(Storecountdefinedetail storecountdefinedetail) {
		getStorecountdefinedetails().add(storecountdefinedetail);
		storecountdefinedetail.setStorebinmstr(this);

		return storecountdefinedetail;
	}

	public Storecountdefinedetail removeStorecountdefinedetail(Storecountdefinedetail storecountdefinedetail) {
		getStorecountdefinedetails().remove(storecountdefinedetail);
		storecountdefinedetail.setStorebinmstr(null);

		return storecountdefinedetail;
	}

	public List<Storeitembin> getStoreitembins() {
		return this.storeitembins;
	}

	public void setStoreitembins(List<Storeitembin> storeitembins) {
		this.storeitembins = storeitembins;
	}

	public Storeitembin addStoreitembin(Storeitembin storeitembin) {
		getStoreitembins().add(storeitembin);
		storeitembin.setStorebinmstr(this);

		return storeitembin;
	}

	public Storeitembin removeStoreitembin(Storeitembin storeitembin) {
		getStoreitembins().remove(storeitembin);
		storeitembin.setStorebinmstr(null);

		return storeitembin;
	}

	public List<Storeitemstock> getStoreitemstocks() {
		return this.storeitemstocks;
	}

	public void setStoreitemstocks(List<Storeitemstock> storeitemstocks) {
		this.storeitemstocks = storeitemstocks;
	}

	public Storeitemstock addStoreitemstock(Storeitemstock storeitemstock) {
		getStoreitemstocks().add(storeitemstock);
		storeitemstock.setStorebinmstr(this);

		return storeitemstock;
	}

	public Storeitemstock removeStoreitemstock(Storeitemstock storeitemstock) {
		getStoreitemstocks().remove(storeitemstock);
		storeitemstock.setStorebinmstr(null);

		return storeitemstock;
	}

	public List<Wardsspmstocktxn> getWardsspmstocktxns() {
		return this.wardsspmstocktxns;
	}

	public void setWardsspmstocktxns(List<Wardsspmstocktxn> wardsspmstocktxns) {
		this.wardsspmstocktxns = wardsspmstocktxns;
	}

	public Wardsspmstocktxn addWardsspmstocktxn(Wardsspmstocktxn wardsspmstocktxn) {
		getWardsspmstocktxns().add(wardsspmstocktxn);
		wardsspmstocktxn.setStorebinmstr(this);

		return wardsspmstocktxn;
	}

	public Wardsspmstocktxn removeWardsspmstocktxn(Wardsspmstocktxn wardsspmstocktxn) {
		getWardsspmstocktxns().remove(wardsspmstocktxn);
		wardsspmstocktxn.setStorebinmstr(null);

		return wardsspmstocktxn;
	}

}