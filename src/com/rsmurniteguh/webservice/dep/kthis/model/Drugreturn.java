package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DRUGRETURN database table.
 * 
 */
@Entity
@NamedQuery(name="Drugreturn.findAll", query="SELECT d FROM Drugreturn d")
public class Drugreturn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DRUGRETURN_ID")
	private long drugreturnId;

	@Temporal(TemporalType.DATE)
	@Column(name="APPROVAL_DATETIME")
	private Date approvalDatetime;

	@Column(name="APPROVED_BY")
	private BigDecimal approvedBy;

	@Column(name="CHARGE_DEPT_LOC")
	private BigDecimal chargeDeptLoc;

	@Column(name="CHARGERETURN_IND")
	private String chargereturnInd;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DRUGRETURN_NO")
	private String drugreturnNo;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIALTXN_ID")
	private BigDecimal materialtxnId;

	@Column(name="PATIENTACCOUNTTXN_ID")
	private BigDecimal patientaccounttxnId;

	@Column(name="PERFORMING_LOCATION")
	private BigDecimal performingLocation;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="RETURN_DATETIME")
	private Date returnDatetime;

	@Column(name="RETURN_QTY")
	private BigDecimal returnQty;

	@Column(name="RETURN_QTY_OF_BASE_UOM")
	private BigDecimal returnQtyOfBaseUom;

	@Column(name="RETURN_REASON")
	private String returnReason;

	@Column(name="RETURN_REMARKS")
	private String returnRemarks;

	@Column(name="RETURN_UOM")
	private String returnUom;

	@Column(name="RETURNED_BY")
	private BigDecimal returnedBy;

	@Column(name="STOREMSTR_ID")
	private BigDecimal storemstrId;

	//bi-directional many-to-one association to Drugdispense
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DRUGDISPENSE_ID")
	private Drugdispense drugdispense;

	public Drugreturn() {
	}

	public long getDrugreturnId() {
		return this.drugreturnId;
	}

	public void setDrugreturnId(long drugreturnId) {
		this.drugreturnId = drugreturnId;
	}

	public Date getApprovalDatetime() {
		return this.approvalDatetime;
	}

	public void setApprovalDatetime(Date approvalDatetime) {
		this.approvalDatetime = approvalDatetime;
	}

	public BigDecimal getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(BigDecimal approvedBy) {
		this.approvedBy = approvedBy;
	}

	public BigDecimal getChargeDeptLoc() {
		return this.chargeDeptLoc;
	}

	public void setChargeDeptLoc(BigDecimal chargeDeptLoc) {
		this.chargeDeptLoc = chargeDeptLoc;
	}

	public String getChargereturnInd() {
		return this.chargereturnInd;
	}

	public void setChargereturnInd(String chargereturnInd) {
		this.chargereturnInd = chargereturnInd;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDrugreturnNo() {
		return this.drugreturnNo;
	}

	public void setDrugreturnNo(String drugreturnNo) {
		this.drugreturnNo = drugreturnNo;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaterialtxnId() {
		return this.materialtxnId;
	}

	public void setMaterialtxnId(BigDecimal materialtxnId) {
		this.materialtxnId = materialtxnId;
	}

	public BigDecimal getPatientaccounttxnId() {
		return this.patientaccounttxnId;
	}

	public void setPatientaccounttxnId(BigDecimal patientaccounttxnId) {
		this.patientaccounttxnId = patientaccounttxnId;
	}

	public BigDecimal getPerformingLocation() {
		return this.performingLocation;
	}

	public void setPerformingLocation(BigDecimal performingLocation) {
		this.performingLocation = performingLocation;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Date getReturnDatetime() {
		return this.returnDatetime;
	}

	public void setReturnDatetime(Date returnDatetime) {
		this.returnDatetime = returnDatetime;
	}

	public BigDecimal getReturnQty() {
		return this.returnQty;
	}

	public void setReturnQty(BigDecimal returnQty) {
		this.returnQty = returnQty;
	}

	public BigDecimal getReturnQtyOfBaseUom() {
		return this.returnQtyOfBaseUom;
	}

	public void setReturnQtyOfBaseUom(BigDecimal returnQtyOfBaseUom) {
		this.returnQtyOfBaseUom = returnQtyOfBaseUom;
	}

	public String getReturnReason() {
		return this.returnReason;
	}

	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}

	public String getReturnRemarks() {
		return this.returnRemarks;
	}

	public void setReturnRemarks(String returnRemarks) {
		this.returnRemarks = returnRemarks;
	}

	public String getReturnUom() {
		return this.returnUom;
	}

	public void setReturnUom(String returnUom) {
		this.returnUom = returnUom;
	}

	public BigDecimal getReturnedBy() {
		return this.returnedBy;
	}

	public void setReturnedBy(BigDecimal returnedBy) {
		this.returnedBy = returnedBy;
	}

	public BigDecimal getStoremstrId() {
		return this.storemstrId;
	}

	public void setStoremstrId(BigDecimal storemstrId) {
		this.storemstrId = storemstrId;
	}

	public Drugdispense getDrugdispense() {
		return this.drugdispense;
	}

	public void setDrugdispense(Drugdispense drugdispense) {
		this.drugdispense = drugdispense;
	}

}