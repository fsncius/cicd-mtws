package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITAPPOINTMENT database table.
 * 
 */
@Entity
@NamedQuery(name="Visitappointment.findAll", query="SELECT v FROM Visitappointment v")
public class Visitappointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITAPPOINTMENT_ID")
	private long visitappointmentId;

	@Column(name="ADMITTING_SOURCE")
	private String admittingSource;

	@Column(name="APPOINTMENT_CAT")
	private String appointmentCat;

	@Temporal(TemporalType.DATE)
	@Column(name="APPOINTMENT_DATETIME")
	private Date appointmentDatetime;

	@Column(name="APPOINTMENT_STATUS")
	private String appointmentStatus;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="DIAGNOSED_DATETIME")
	private Date diagnosedDatetime;

	@Column(name="DIAGNOSIS_DESC")
	private String diagnosisDesc;

	@Column(name="DIAGNOSISMSTR_ID")
	private BigDecimal diagnosismstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MEDICAL_CONDITION")
	private String medicalCondition;

	private String remarks;

	@Column(name="VISIT_APPOINTMENT_NO")
	private String visitAppointmentNo;

	@Column(name="VISIT_REASON")
	private String visitReason;

	//bi-directional many-to-one association to Bedmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BEDMSTR_ID")
	private Bedmstr bedmstr;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CAREPROVIDER_ID")
	private Careprovider careprovider;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to Patient
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENT_ID")
	private Patient patient;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="WARDMSTR_ID")
	private Wardmstr wardmstr;

	public Visitappointment() {
	}

	public long getVisitappointmentId() {
		return this.visitappointmentId;
	}

	public void setVisitappointmentId(long visitappointmentId) {
		this.visitappointmentId = visitappointmentId;
	}

	public String getAdmittingSource() {
		return this.admittingSource;
	}

	public void setAdmittingSource(String admittingSource) {
		this.admittingSource = admittingSource;
	}

	public String getAppointmentCat() {
		return this.appointmentCat;
	}

	public void setAppointmentCat(String appointmentCat) {
		this.appointmentCat = appointmentCat;
	}

	public Date getAppointmentDatetime() {
		return this.appointmentDatetime;
	}

	public void setAppointmentDatetime(Date appointmentDatetime) {
		this.appointmentDatetime = appointmentDatetime;
	}

	public String getAppointmentStatus() {
		return this.appointmentStatus;
	}

	public void setAppointmentStatus(String appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Date getDiagnosedDatetime() {
		return this.diagnosedDatetime;
	}

	public void setDiagnosedDatetime(Date diagnosedDatetime) {
		this.diagnosedDatetime = diagnosedDatetime;
	}

	public String getDiagnosisDesc() {
		return this.diagnosisDesc;
	}

	public void setDiagnosisDesc(String diagnosisDesc) {
		this.diagnosisDesc = diagnosisDesc;
	}

	public BigDecimal getDiagnosismstrId() {
		return this.diagnosismstrId;
	}

	public void setDiagnosismstrId(BigDecimal diagnosismstrId) {
		this.diagnosismstrId = diagnosismstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMedicalCondition() {
		return this.medicalCondition;
	}

	public void setMedicalCondition(String medicalCondition) {
		this.medicalCondition = medicalCondition;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getVisitAppointmentNo() {
		return this.visitAppointmentNo;
	}

	public void setVisitAppointmentNo(String visitAppointmentNo) {
		this.visitAppointmentNo = visitAppointmentNo;
	}

	public String getVisitReason() {
		return this.visitReason;
	}

	public void setVisitReason(String visitReason) {
		this.visitReason = visitReason;
	}

	public Bedmstr getBedmstr() {
		return this.bedmstr;
	}

	public void setBedmstr(Bedmstr bedmstr) {
		this.bedmstr = bedmstr;
	}

	public Careprovider getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(Careprovider careprovider) {
		this.careprovider = careprovider;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

}