package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERSON database table.
 * 
 */
@Entity
@NamedQuery(name="Person.findAll", query="SELECT p FROM Person p")
public class Person implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PERSON_ID")
	private BigDecimal personId;

	@Column(name="ALIAS_NAME")
	private String aliasName;

	@Column(name="ALT_PERSON_ID")
	private BigDecimal altPersonId;

	@Temporal(TemporalType.DATE)
	@Column(name="BIRTH_DATE")
	private Date birthDate;

	@Column(name="BLOOD_GROUP")
	private String bloodGroup;

	@Column(name="COMPANY_ID")
	private BigDecimal companyId;

	@Column(name="COMPANY_NAME")
	private String companyName;

	@Column(name="DECEASED_ID")
	private BigDecimal deceasedId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	private String email;

	@Column(name="EMPLOYEE_NO")
	private String employeeNo;

	private String employer;

	@Column(name="ETHNIC_GROUP")
	private String ethnicGroup;

	@Column(name="FILEMSTR_ID")
	private BigDecimal filemstrId;

	@Column(name="HOUSEHOLD_SIZE")
	private BigDecimal householdSize;

	@Column(name="ID_NO")
	private String idNo;

	@Column(name="ID_TYPE")
	private String idType;

	@Column(name="INCOME_RANGE")
	private String incomeRange;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MARITAL_STATUS")
	private String maritalStatus;

	@Column(name="MOBILE_PHONE_NO")
	private String mobilePhoneNo;

	@Column(name="MOTHER_LANGUAGE")
	private String motherLanguage;

	@Column(name="NATIONAL_SERVICE")
	private String nationalService;

	private String nationality;

	@Column(name="OCCUPATION_GROUP")
	private String occupationGroup;

	@Column(name="PAGER_NO")
	private String pagerNo;

	@Column(name="PERSON_NAME")
	private String personName;

	@Column(name="PINYIN_CODE")
	private String pinyinCode;

	@Column(name="PREFERRED_NAME")
	private String preferredName;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PSEUDO_AGE_IND")
	private String pseudoAgeInd;

	private String race;

	private String ras;

	private String religion;

	@Column(name="RESIDENT_TYPE")
	private String residentType;

	private String sex;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="STAKEHOLDER_ID")
	private BigDecimal stakeholderId;

	private String title;

	//bi-directional many-to-one association to Card
	@OneToMany(mappedBy="person")
	private List<Card> cards;

	//bi-directional many-to-one association to Careprovider
	@OneToMany(mappedBy="person")
	private List<Careprovider> careproviders;

	//bi-directional many-to-one association to Patient
	@OneToMany(mappedBy="person1")
	private List<Patient> patients1;

	//bi-directional many-to-one association to Patient
	@OneToMany(mappedBy="person2")
	private List<Patient> patients2;

	//bi-directional many-to-one association to Patientallergy
	@OneToMany(mappedBy="person")
	private List<Patientallergy> patientallergies;

	//bi-directional many-to-one association to Patientmedicalalert
	@OneToMany(mappedBy="person")
	private List<Patientmedicalalert> patientmedicalalerts;
	
	private String ethnic;
	private String nik;

	public Person() {
	}

	public BigDecimal getPersonId() {
		return this.personId;
	}

	public void setPersonId(BigDecimal personId) {
		this.personId = personId;
	}

	public String getAliasName() {
		return this.aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public BigDecimal getAltPersonId() {
		return this.altPersonId;
	}

	public void setAltPersonId(BigDecimal altPersonId) {
		this.altPersonId = altPersonId;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBloodGroup() {
		return this.bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public BigDecimal getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(BigDecimal companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public BigDecimal getDeceasedId() {
		return this.deceasedId;
	}

	public void setDeceasedId(BigDecimal deceasedId) {
		this.deceasedId = deceasedId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmployeeNo() {
		return this.employeeNo;
	}

	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	public String getEmployer() {
		return this.employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getEthnicGroup() {
		return this.ethnicGroup;
	}

	public void setEthnicGroup(String ethnicGroup) {
		this.ethnicGroup = ethnicGroup;
	}

	public BigDecimal getFilemstrId() {
		return this.filemstrId;
	}

	public void setFilemstrId(BigDecimal filemstrId) {
		this.filemstrId = filemstrId;
	}

	public BigDecimal getHouseholdSize() {
		return this.householdSize;
	}

	public void setHouseholdSize(BigDecimal householdSize) {
		this.householdSize = householdSize;
	}

	public String getIdNo() {
		return this.idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getIdType() {
		return this.idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIncomeRange() {
		return this.incomeRange;
	}

	public void setIncomeRange(String incomeRange) {
		this.incomeRange = incomeRange;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getMobilePhoneNo() {
		return this.mobilePhoneNo;
	}

	public void setMobilePhoneNo(String mobilePhoneNo) {
		this.mobilePhoneNo = mobilePhoneNo;
	}

	public String getMotherLanguage() {
		return this.motherLanguage;
	}

	public void setMotherLanguage(String motherLanguage) {
		this.motherLanguage = motherLanguage;
	}

	public String getNationalService() {
		return this.nationalService;
	}

	public void setNationalService(String nationalService) {
		this.nationalService = nationalService;
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getOccupationGroup() {
		return this.occupationGroup;
	}

	public void setOccupationGroup(String occupationGroup) {
		this.occupationGroup = occupationGroup;
	}

	public String getPagerNo() {
		return this.pagerNo;
	}

	public void setPagerNo(String pagerNo) {
		this.pagerNo = pagerNo;
	}

	public String getPersonName() {
		return this.personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getPinyinCode() {
		return this.pinyinCode;
	}

	public void setPinyinCode(String pinyinCode) {
		this.pinyinCode = pinyinCode;
	}

	public String getPreferredName() {
		return this.preferredName;
	}

	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPseudoAgeInd() {
		return this.pseudoAgeInd;
	}

	public void setPseudoAgeInd(String pseudoAgeInd) {
		this.pseudoAgeInd = pseudoAgeInd;
	}

	public String getRace() {
		return this.race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getRas() {
		return this.ras;
	}

	public void setRas(String ras) {
		this.ras = ras;
	}

	public String getReligion() {
		return this.religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getResidentType() {
		return this.residentType;
	}

	public void setResidentType(String residentType) {
		this.residentType = residentType;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public BigDecimal getStakeholderId() {
		return this.stakeholderId;
	}

	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Card> getCards() {
		return this.cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public Card addCard(Card card) {
		getCards().add(card);
		card.setPerson(this);

		return card;
	}

	public Card removeCard(Card card) {
		getCards().remove(card);
		card.setPerson(null);

		return card;
	}

	public List<Careprovider> getCareproviders() {
		return this.careproviders;
	}

	public void setCareproviders(List<Careprovider> careproviders) {
		this.careproviders = careproviders;
	}

	public Careprovider addCareprovider(Careprovider careprovider) {
		getCareproviders().add(careprovider);
		careprovider.setPerson(this);

		return careprovider;
	}

	public Careprovider removeCareprovider(Careprovider careprovider) {
		getCareproviders().remove(careprovider);
		careprovider.setPerson(null);

		return careprovider;
	}

	public List<Patient> getPatients1() {
		return this.patients1;
	}

	public void setPatients1(List<Patient> patients1) {
		this.patients1 = patients1;
	}

	public Patient addPatients1(Patient patients1) {
		getPatients1().add(patients1);
		patients1.setPerson1(this);

		return patients1;
	}

	public Patient removePatients1(Patient patients1) {
		getPatients1().remove(patients1);
		patients1.setPerson1(null);

		return patients1;
	}

	public List<Patient> getPatients2() {
		return this.patients2;
	}

	public void setPatients2(List<Patient> patients2) {
		this.patients2 = patients2;
	}

	public Patient addPatients2(Patient patients2) {
		getPatients2().add(patients2);
		patients2.setPerson2(this);

		return patients2;
	}

	public Patient removePatients2(Patient patients2) {
		getPatients2().remove(patients2);
		patients2.setPerson2(null);

		return patients2;
	}

	public List<Patientallergy> getPatientallergies() {
		return this.patientallergies;
	}

	public void setPatientallergies(List<Patientallergy> patientallergies) {
		this.patientallergies = patientallergies;
	}

	public Patientallergy addPatientallergy(Patientallergy patientallergy) {
		getPatientallergies().add(patientallergy);
		patientallergy.setPerson(this);

		return patientallergy;
	}

	public Patientallergy removePatientallergy(Patientallergy patientallergy) {
		getPatientallergies().remove(patientallergy);
		patientallergy.setPerson(null);

		return patientallergy;
	}

	public List<Patientmedicalalert> getPatientmedicalalerts() {
		return this.patientmedicalalerts;
	}

	public void setPatientmedicalalerts(List<Patientmedicalalert> patientmedicalalerts) {
		this.patientmedicalalerts = patientmedicalalerts;
	}

	public Patientmedicalalert addPatientmedicalalert(Patientmedicalalert patientmedicalalert) {
		getPatientmedicalalerts().add(patientmedicalalert);
		patientmedicalalert.setPerson(this);

		return patientmedicalalert;
	}

	public Patientmedicalalert removePatientmedicalalert(Patientmedicalalert patientmedicalalert) {
		getPatientmedicalalerts().remove(patientmedicalalert);
		patientmedicalalert.setPerson(null);

		return patientmedicalalert;
	}

	public String getEthnic() {
		return ethnic;
	}

	public void setEthnic(String ethnic) {
		this.ethnic = ethnic;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

}