package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DIAGNOSISINCOMMONUSEMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Diagnosisincommonusemstr.findAll", query="SELECT d FROM Diagnosisincommonusemstr d")
public class Diagnosisincommonusemstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DIAGNOSISINCOMMONUSEMSTR_ID")
	private long diagnosisincommonusemstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DIAGNOSIS_CATEGORY")
	private String diagnosisCategory;

	@Column(name="DIAGNOSIS_DESC")
	private String diagnosisDesc;

	@Column(name="INCOMMONUSE_LEVEL")
	private String incommonuseLevel;

	@Column(name="INCOMMONUSE_TYPE")
	private String incommonuseType;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CAREPROVIDER_ID")
	private Careprovider careprovider;

	//bi-directional many-to-one association to Diagnosismstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DIAGNOSISMSTR_ID")
	private Diagnosismstr diagnosismstr;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="WARDMSTR_ID")
	private Wardmstr wardmstr;

	public Diagnosisincommonusemstr() {
	}

	public long getDiagnosisincommonusemstrId() {
		return this.diagnosisincommonusemstrId;
	}

	public void setDiagnosisincommonusemstrId(long diagnosisincommonusemstrId) {
		this.diagnosisincommonusemstrId = diagnosisincommonusemstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDiagnosisCategory() {
		return this.diagnosisCategory;
	}

	public void setDiagnosisCategory(String diagnosisCategory) {
		this.diagnosisCategory = diagnosisCategory;
	}

	public String getDiagnosisDesc() {
		return this.diagnosisDesc;
	}

	public void setDiagnosisDesc(String diagnosisDesc) {
		this.diagnosisDesc = diagnosisDesc;
	}

	public String getIncommonuseLevel() {
		return this.incommonuseLevel;
	}

	public void setIncommonuseLevel(String incommonuseLevel) {
		this.incommonuseLevel = incommonuseLevel;
	}

	public String getIncommonuseType() {
		return this.incommonuseType;
	}

	public void setIncommonuseType(String incommonuseType) {
		this.incommonuseType = incommonuseType;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Careprovider getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(Careprovider careprovider) {
		this.careprovider = careprovider;
	}

	public Diagnosismstr getDiagnosismstr() {
		return this.diagnosismstr;
	}

	public void setDiagnosismstr(Diagnosismstr diagnosismstr) {
		this.diagnosismstr = diagnosismstr;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

}