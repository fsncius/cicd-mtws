package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CONSUMERCARDTXN database table.
 * 
 */
@Entity
@NamedQuery(name="Consumercardtxn.findAll", query="SELECT c FROM Consumercardtxn c")
public class Consumercardtxn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CONSUMERCARDTXN_ID")
	private long consumercardtxnId;

	@Column(name="CONSUMERCARDMSTR_ID")
	private BigDecimal consumercardmstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTER_DATETIME")
	private Date enterDatetime;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Column(name="TXN_AMOUNT")
	private BigDecimal txnAmount;

	@Column(name="TXN_TYPE")
	private String txnType;

	public Consumercardtxn() {
	}

	public long getConsumercardtxnId() {
		return this.consumercardtxnId;
	}

	public void setConsumercardtxnId(long consumercardtxnId) {
		this.consumercardtxnId = consumercardtxnId;
	}

	public BigDecimal getConsumercardmstrId() {
		return this.consumercardmstrId;
	}

	public void setConsumercardmstrId(BigDecimal consumercardmstrId) {
		this.consumercardmstrId = consumercardmstrId;
	}

	public Date getEnterDatetime() {
		return this.enterDatetime;
	}

	public void setEnterDatetime(Date enterDatetime) {
		this.enterDatetime = enterDatetime;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public BigDecimal getTxnAmount() {
		return this.txnAmount;
	}

	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}

	public String getTxnType() {
		return this.txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

}