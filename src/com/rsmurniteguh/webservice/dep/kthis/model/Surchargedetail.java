package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the SURCHARGEDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Surchargedetail.findAll", query="SELECT s FROM Surchargedetail s")
public class Surchargedetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SURCHARGEDETAIL_ID")
	private long surchargedetailId;

	@Column(name="DEFAULT_SELECTED")
	private String defaultSelected;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MIO_INDICATION")
	private String mioIndication;

	@Column(name="ORDER_STATUS")
	private String orderStatus;

	@Column(name="ORDERENTRY_ID")
	private BigDecimal orderentryId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private BigDecimal qty;

	@Column(name="QTY_UOM")
	private String qtyUom;

	//bi-directional many-to-one association to Chargeitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEITEMMSTR_ID")
	private Chargeitemmstr chargeitemmstr;

	public Surchargedetail() {
	}

	public long getSurchargedetailId() {
		return this.surchargedetailId;
	}

	public void setSurchargedetailId(long surchargedetailId) {
		this.surchargedetailId = surchargedetailId;
	}

	public String getDefaultSelected() {
		return this.defaultSelected;
	}

	public void setDefaultSelected(String defaultSelected) {
		this.defaultSelected = defaultSelected;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMioIndication() {
		return this.mioIndication;
	}

	public void setMioIndication(String mioIndication) {
		this.mioIndication = mioIndication;
	}

	public String getOrderStatus() {
		return this.orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public BigDecimal getOrderentryId() {
		return this.orderentryId;
	}

	public void setOrderentryId(BigDecimal orderentryId) {
		this.orderentryId = orderentryId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getQtyUom() {
		return this.qtyUom;
	}

	public void setQtyUom(String qtyUom) {
		this.qtyUom = qtyUom;
	}

	public Chargeitemmstr getChargeitemmstr() {
		return this.chargeitemmstr;
	}

	public void setChargeitemmstr(Chargeitemmstr chargeitemmstr) {
		this.chargeitemmstr = chargeitemmstr;
	}

}