package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the RECEIPTHISTORY database table.
 * 
 */
@Entity
@NamedQuery(name="Receipthistory.findAll", query="SELECT r FROM Receipthistory r")
public class Receipthistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RECEIPTHISTORY_ID")
	private BigDecimal receipthistoryId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="RECEIPT_DATETIME")
	private Date receiptDatetime;

	@Column(name="RECEIPT_STATUS")
	private String receiptStatus;

	@Column(name="USERMSTR_ID")
	private BigDecimal usermstrId;

	//bi-directional many-to-one association to Countercollection
	@OneToMany(mappedBy="receipthistory")
	private List<Countercollection> countercollections;

	//bi-directional many-to-one association to Receipt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="RECEIPT_ID")
	private Receipt receipt;
	
	private BigDecimal receiptId;

	public Receipthistory() {
	}

	public BigDecimal getReceipthistoryId() {
		return this.receipthistoryId;
	}

	public void setReceipthistoryId(BigDecimal receipthistoryId) {
		this.receipthistoryId = receipthistoryId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public Date getReceiptDatetime() {
		return this.receiptDatetime;
	}

	public void setReceiptDatetime(Date receiptDatetime) {
		this.receiptDatetime = receiptDatetime;
	}

	public String getReceiptStatus() {
		return this.receiptStatus;
	}

	public void setReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}

	public BigDecimal getUsermstrId() {
		return this.usermstrId;
	}

	public void setUsermstrId(BigDecimal usermstrId) {
		this.usermstrId = usermstrId;
	}

	public List<Countercollection> getCountercollections() {
		return this.countercollections;
	}

	public void setCountercollections(List<Countercollection> countercollections) {
		this.countercollections = countercollections;
	}

	public Countercollection addCountercollection(Countercollection countercollection) {
		getCountercollections().add(countercollection);
		countercollection.setReceipthistory(this);

		return countercollection;
	}

	public Countercollection removeCountercollection(Countercollection countercollection) {
		getCountercollections().remove(countercollection);
		countercollection.setReceipthistory(null);

		return countercollection;
	}

	public Receipt getReceipt() {
		return this.receipt;
	}

	public void setReceipt(Receipt receipt) {
		this.receipt = receipt;
	}

	public BigDecimal getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(BigDecimal receiptId) {
		this.receiptId = receiptId;
	}

}