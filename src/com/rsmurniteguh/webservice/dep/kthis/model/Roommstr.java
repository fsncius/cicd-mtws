package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ROOMMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Roommstr.findAll", query="SELECT r FROM Roommstr r")
public class Roommstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ROOMMSTR_ID")
	private long roommstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="GENDER_CHECK")
	private String genderCheck;

	@Column(name="GENDER_RESTRICTION")
	private String genderRestriction;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MAX_VISITOR")
	private BigDecimal maxVisitor;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="ROOM_CODE")
	private String roomCode;

	@Column(name="ROOM_DESC")
	private String roomDesc;

	@Column(name="ROOM_DESC_LANG1")
	private String roomDescLang1;

	@Column(name="ROOM_DESC_LANG2")
	private String roomDescLang2;

	@Column(name="ROOM_DESC_LANG3")
	private String roomDescLang3;

	@Column(name="ROOM_GENDER")
	private String roomGender;

	@Column(name="SMOKING_FLAG")
	private String smokingFlag;

	//bi-directional many-to-one association to Bedmstr
	@OneToMany(mappedBy="roommstr")
	private List<Bedmstr> bedmstrs;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="WARDMSTR_ID")
	private Wardmstr wardmstr;

	public Roommstr() {
	}

	public long getRoommstrId() {
		return this.roommstrId;
	}

	public void setRoommstrId(long roommstrId) {
		this.roommstrId = roommstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getGenderCheck() {
		return this.genderCheck;
	}

	public void setGenderCheck(String genderCheck) {
		this.genderCheck = genderCheck;
	}

	public String getGenderRestriction() {
		return this.genderRestriction;
	}

	public void setGenderRestriction(String genderRestriction) {
		this.genderRestriction = genderRestriction;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaxVisitor() {
		return this.maxVisitor;
	}

	public void setMaxVisitor(BigDecimal maxVisitor) {
		this.maxVisitor = maxVisitor;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRoomCode() {
		return this.roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public String getRoomDesc() {
		return this.roomDesc;
	}

	public void setRoomDesc(String roomDesc) {
		this.roomDesc = roomDesc;
	}

	public String getRoomDescLang1() {
		return this.roomDescLang1;
	}

	public void setRoomDescLang1(String roomDescLang1) {
		this.roomDescLang1 = roomDescLang1;
	}

	public String getRoomDescLang2() {
		return this.roomDescLang2;
	}

	public void setRoomDescLang2(String roomDescLang2) {
		this.roomDescLang2 = roomDescLang2;
	}

	public String getRoomDescLang3() {
		return this.roomDescLang3;
	}

	public void setRoomDescLang3(String roomDescLang3) {
		this.roomDescLang3 = roomDescLang3;
	}

	public String getRoomGender() {
		return this.roomGender;
	}

	public void setRoomGender(String roomGender) {
		this.roomGender = roomGender;
	}

	public String getSmokingFlag() {
		return this.smokingFlag;
	}

	public void setSmokingFlag(String smokingFlag) {
		this.smokingFlag = smokingFlag;
	}

	public List<Bedmstr> getBedmstrs() {
		return this.bedmstrs;
	}

	public void setBedmstrs(List<Bedmstr> bedmstrs) {
		this.bedmstrs = bedmstrs;
	}

	public Bedmstr addBedmstr(Bedmstr bedmstr) {
		getBedmstrs().add(bedmstr);
		bedmstr.setRoommstr(this);

		return bedmstr;
	}

	public Bedmstr removeBedmstr(Bedmstr bedmstr) {
		getBedmstrs().remove(bedmstr);
		bedmstr.setRoommstr(null);

		return bedmstr;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

}