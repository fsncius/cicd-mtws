package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CHARGETEMPLATE database table.
 * 
 */
@Entity
@NamedQuery(name="Chargetemplate.findAll", query="SELECT c FROM Chargetemplate c")
public class Chargetemplate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGETEMPLATE_ID")
	private long chargetemplateId;

	@Column(name="CHARGETEMPLATE_CATEGORY")
	private String chargetemplateCategory;

	@Column(name="CHARGETEMPLATE_CODE")
	private String chargetemplateCode;

	@Column(name="CHARGETEMPLATE_DESC")
	private String chargetemplateDesc;

	@Column(name="CHARGETEMPLATE_DESC_LANG1")
	private String chargetemplateDescLang1;

	@Column(name="CHARGETEMPLATE_DESC_LANG2")
	private String chargetemplateDescLang2;

	@Column(name="CHARGETEMPLATE_DESC_LANG3")
	private String chargetemplateDescLang3;

	@Column(name="CHARGETEMPLATE_LEVEL")
	private String chargetemplateLevel;

	@Column(name="CHARGETEMPLATE_TYPE")
	private String chargetemplateType;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="CREATED_REVENUECENTREMSTR_ID")
	private BigDecimal createdRevenuecentremstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SHORT_CODE")
	private String shortCode;

	//bi-directional many-to-one association to Chargetemplatedetail
	@OneToMany(mappedBy="chargetemplate")
	private List<Chargetemplatedetail> chargetemplatedetails;

	public Chargetemplate() {
	}

	public long getChargetemplateId() {
		return this.chargetemplateId;
	}

	public void setChargetemplateId(long chargetemplateId) {
		this.chargetemplateId = chargetemplateId;
	}

	public String getChargetemplateCategory() {
		return this.chargetemplateCategory;
	}

	public void setChargetemplateCategory(String chargetemplateCategory) {
		this.chargetemplateCategory = chargetemplateCategory;
	}

	public String getChargetemplateCode() {
		return this.chargetemplateCode;
	}

	public void setChargetemplateCode(String chargetemplateCode) {
		this.chargetemplateCode = chargetemplateCode;
	}

	public String getChargetemplateDesc() {
		return this.chargetemplateDesc;
	}

	public void setChargetemplateDesc(String chargetemplateDesc) {
		this.chargetemplateDesc = chargetemplateDesc;
	}

	public String getChargetemplateDescLang1() {
		return this.chargetemplateDescLang1;
	}

	public void setChargetemplateDescLang1(String chargetemplateDescLang1) {
		this.chargetemplateDescLang1 = chargetemplateDescLang1;
	}

	public String getChargetemplateDescLang2() {
		return this.chargetemplateDescLang2;
	}

	public void setChargetemplateDescLang2(String chargetemplateDescLang2) {
		this.chargetemplateDescLang2 = chargetemplateDescLang2;
	}

	public String getChargetemplateDescLang3() {
		return this.chargetemplateDescLang3;
	}

	public void setChargetemplateDescLang3(String chargetemplateDescLang3) {
		this.chargetemplateDescLang3 = chargetemplateDescLang3;
	}

	public String getChargetemplateLevel() {
		return this.chargetemplateLevel;
	}

	public void setChargetemplateLevel(String chargetemplateLevel) {
		this.chargetemplateLevel = chargetemplateLevel;
	}

	public String getChargetemplateType() {
		return this.chargetemplateType;
	}

	public void setChargetemplateType(String chargetemplateType) {
		this.chargetemplateType = chargetemplateType;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getCreatedRevenuecentremstrId() {
		return this.createdRevenuecentremstrId;
	}

	public void setCreatedRevenuecentremstrId(BigDecimal createdRevenuecentremstrId) {
		this.createdRevenuecentremstrId = createdRevenuecentremstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public List<Chargetemplatedetail> getChargetemplatedetails() {
		return this.chargetemplatedetails;
	}

	public void setChargetemplatedetails(List<Chargetemplatedetail> chargetemplatedetails) {
		this.chargetemplatedetails = chargetemplatedetails;
	}

	public Chargetemplatedetail addChargetemplatedetail(Chargetemplatedetail chargetemplatedetail) {
		getChargetemplatedetails().add(chargetemplatedetail);
		chargetemplatedetail.setChargetemplate(this);

		return chargetemplatedetail;
	}

	public Chargetemplatedetail removeChargetemplatedetail(Chargetemplatedetail chargetemplatedetail) {
		getChargetemplatedetails().remove(chargetemplatedetail);
		chargetemplatedetail.setChargetemplate(null);

		return chargetemplatedetail;
	}

}