package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CONSUMERCARDSTATUSHISTORY database table.
 * 
 */
@Entity
@NamedQuery(name="Consumercardstatushistory.findAll", query="SELECT c FROM Consumercardstatushistory c")
public class Consumercardstatushistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CONSUMERCARDSTATUSHISTORY_ID")
	private long consumercardstatushistoryId;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTER_DATETIME")
	private Date enterDatetime;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Column(name="ENTERED_BY_SECOND")
	private BigDecimal enteredBySecond;

	@Column(name="NEW_STAUTS")
	private String newStauts;

	@Column(name="OLD_STATUS")
	private String oldStatus;

	private String remarks;

	//bi-directional many-to-one association to Consumercardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONSUMERCARDMSTR_ID")
	private Consumercardmstr consumercardmstr;

	public Consumercardstatushistory() {
	}

	public long getConsumercardstatushistoryId() {
		return this.consumercardstatushistoryId;
	}

	public void setConsumercardstatushistoryId(long consumercardstatushistoryId) {
		this.consumercardstatushistoryId = consumercardstatushistoryId;
	}

	public Date getEnterDatetime() {
		return this.enterDatetime;
	}

	public void setEnterDatetime(Date enterDatetime) {
		this.enterDatetime = enterDatetime;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public BigDecimal getEnteredBySecond() {
		return this.enteredBySecond;
	}

	public void setEnteredBySecond(BigDecimal enteredBySecond) {
		this.enteredBySecond = enteredBySecond;
	}

	public String getNewStauts() {
		return this.newStauts;
	}

	public void setNewStauts(String newStauts) {
		this.newStauts = newStauts;
	}

	public String getOldStatus() {
		return this.oldStatus;
	}

	public void setOldStatus(String oldStatus) {
		this.oldStatus = oldStatus;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Consumercardmstr getConsumercardmstr() {
		return this.consumercardmstr;
	}

	public void setConsumercardmstr(Consumercardmstr consumercardmstr) {
		this.consumercardmstr = consumercardmstr;
	}

}