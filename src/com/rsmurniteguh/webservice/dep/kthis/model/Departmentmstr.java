package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DEPARTMENTMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Departmentmstr.findAll", query="SELECT d FROM Departmentmstr d")
public class Departmentmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DEPARTMENTMSTR_ID")
	private long departmentmstrId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DEPARTMENT_CODE")
	private String departmentCode;

	@Column(name="DEPARTMENT_NAME")
	private String departmentName;

	@Column(name="DEPARTMENT_NAME_LANG1")
	private String departmentNameLang1;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	private BigDecimal leader;

	private String remarks;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	@Column(name="SHORT_CODE")
	private String shortCode;

	//bi-directional many-to-one association to Departmentgroupmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DEPARTMENTGROUPMSTR_ID")
	private Departmentgroupmstr departmentgroupmstr;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	public Departmentmstr() {
	}

	public long getDepartmentmstrId() {
		return this.departmentmstrId;
	}

	public void setDepartmentmstrId(long departmentmstrId) {
		this.departmentmstrId = departmentmstrId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDepartmentCode() {
		return this.departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getDepartmentName() {
		return this.departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getDepartmentNameLang1() {
		return this.departmentNameLang1;
	}

	public void setDepartmentNameLang1(String departmentNameLang1) {
		this.departmentNameLang1 = departmentNameLang1;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getLeader() {
		return this.leader;
	}

	public void setLeader(BigDecimal leader) {
		this.leader = leader;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Departmentgroupmstr getDepartmentgroupmstr() {
		return this.departmentgroupmstr;
	}

	public void setDepartmentgroupmstr(Departmentgroupmstr departmentgroupmstr) {
		this.departmentgroupmstr = departmentgroupmstr;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

}