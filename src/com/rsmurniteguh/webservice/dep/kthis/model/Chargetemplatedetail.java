package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CHARGETEMPLATEDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Chargetemplatedetail.findAll", query="SELECT c FROM Chargetemplatedetail c")
public class Chargetemplatedetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGETEMPLATEDETAIL_ID")
	private long chargetemplatedetailId;

	@Column(name="CHARGE_SEQUENCE")
	private BigDecimal chargeSequence;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private BigDecimal qty;

	//bi-directional many-to-one association to Chargeitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEITEMMSTR_ID")
	private Chargeitemmstr chargeitemmstr;

	//bi-directional many-to-one association to Chargetemplate
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGETEMPLATE_ID")
	private Chargetemplate chargetemplate;

	public Chargetemplatedetail() {
	}

	public long getChargetemplatedetailId() {
		return this.chargetemplatedetailId;
	}

	public void setChargetemplatedetailId(long chargetemplatedetailId) {
		this.chargetemplatedetailId = chargetemplatedetailId;
	}

	public BigDecimal getChargeSequence() {
		return this.chargeSequence;
	}

	public void setChargeSequence(BigDecimal chargeSequence) {
		this.chargeSequence = chargeSequence;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public Chargeitemmstr getChargeitemmstr() {
		return this.chargeitemmstr;
	}

	public void setChargeitemmstr(Chargeitemmstr chargeitemmstr) {
		this.chargeitemmstr = chargeitemmstr;
	}

	public Chargetemplate getChargetemplate() {
		return this.chargetemplate;
	}

	public void setChargetemplate(Chargetemplate chargetemplate) {
		this.chargetemplate = chargetemplate;
	}

}