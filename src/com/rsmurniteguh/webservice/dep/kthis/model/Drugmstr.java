package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DRUGMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Drugmstr.findAll", query="SELECT d FROM Drugmstr d")
public class Drugmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DRUGMSTR_ID")
	private long drugmstrId;

	@Column(name="ALLERGY_TEST_IND")
	private String allergyTestInd;

	@Column(name="ANAESTHETIC_ITEM_IND")
	private String anaestheticItemInd;

	@Column(name="ANTIBIOTIC_GRADE")
	private String antibioticGrade;

	@Column(name="ANTIBIOTIC_IND")
	private String antibioticInd;

	@Column(name="BASE_DRUG_IND")
	private String baseDrugInd;

	@Column(name="BLOOD_PRODUCT_IND")
	private String bloodProductInd;

	@Column(name="CODEINE_IND")
	private String codeineInd;

	@Column(name="CONCENTRATION_QTY")
	private BigDecimal concentrationQty;

	@Column(name="CONTROL_ITEM_IND")
	private String controlItemInd;

	@Column(name="DEFAULT_DURATION_QTY")
	private BigDecimal defaultDurationQty;

	@Column(name="DEFAULT_DURATIONMSTR_ID")
	private BigDecimal defaultDurationmstrId;

	@Column(name="DEFAULT_FREQUENCYMSTR_ID")
	private BigDecimal defaultFrequencymstrId;

	@Column(name="DEFAULT_INSTRUCTION")
	private String defaultInstruction;

	@Column(name="DEFAULT_INSTRUCTION_LANG1")
	private String defaultInstructionLang1;

	@Column(name="DEFAULT_INSTRUCTION_LANG2")
	private String defaultInstructionLang2;

	@Column(name="DEFAULT_INSTRUCTION_LANG3")
	private String defaultInstructionLang3;

	@Column(name="DEFAULT_INTAKE_QTY")
	private BigDecimal defaultIntakeQty;

	@Column(name="DEFAULT_QTY")
	private BigDecimal defaultQty;

	@Column(name="DEFAULT_QTY_UOM")
	private String defaultQtyUom;

	@Column(name="DEFAULT_ROA")
	private String defaultRoa;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DIRECTORY_IND")
	private String directoryInd;

	@Column(name="DISPENSE_AUTHORISED_IND")
	private String dispenseAuthorisedInd;

	@Column(name="DOSAGE_ROUNDING_RULE")
	private String dosageRoundingRule;

	@Column(name="DOSAGEFORMMSTR_ID")
	private BigDecimal dosageformmstrId;

	@Column(name="DOSE_UNIT")
	private String doseUnit;

	@Column(name="DOSECODEMSTR_ID")
	private BigDecimal dosecodemstrId;

	@Column(name="DRUG_ISSUE_TYPE")
	private String drugIssueType;

	@Column(name="DRUG_MANUFACTURE_ITEM_IND")
	private String drugManufactureItemInd;

	@Column(name="EXPORT_ALLOW_IND")
	private String exportAllowInd;

	@Column(name="FIRST_AID_DRUG_IND")
	private String firstAidDrugInd;

	@Column(name="FORMULARY_ITEM_IND")
	private String formularyItemInd;

	@Column(name="GENERIC_NAME")
	private String genericName;

	@Column(name="GENERIC_NAME_LANG1")
	private String genericNameLang1;

	@Column(name="GENERIC_NAME_LANG2")
	private String genericNameLang2;

	@Column(name="GENERIC_NAME_LANG3")
	private String genericNameLang3;

	@Column(name="GENERICDRUGMSTR_ID")
	private BigDecimal genericdrugmstrId;

	@Column(name="HIGH_RISK_DRUG_IND")
	private String highRiskDrugInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIALITEMMSTR_ID")
	private BigDecimal materialitemmstrId;

	@Column(name="NATIONAL_STANDARD_CODE")
	private String nationalStandardCode;

	@Column(name="NO_OF_DOSE_PER_BASE_UOM")
	private BigDecimal noOfDosePerBaseUom;

	@Column(name="PATIENT_SELFPAY_ITEM_IND")
	private String patientSelfpayItemInd;

	@Column(name="POISONOUS_ITEM_IND")
	private String poisonousItemInd;

	@Column(name="PRESCRIPTION_ITEM_IND")
	private String prescriptionItemInd;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PSYCHOTROPIC_ITEM_GRADE")
	private String psychotropicItemGrade;

	@Column(name="PSYCHOTROPIC_ITEM_IND")
	private String psychotropicItemInd;

	private String strength;

	@Column(name="SUBSTITUTEDRUGMSTR_ID")
	private BigDecimal substitutedrugmstrId;

	@Column(name="VOLUME_QTY")
	private BigDecimal volumeQty;

	@Column(name="VOLUME_UNIT")
	private String volumeUnit;

	@Column(name="WEIGHT_QTY")
	private BigDecimal weightQty;

	@Column(name="WEIGHT_UNIT")
	private String weightUnit;

	public Drugmstr() {
	}

	public long getDrugmstrId() {
		return this.drugmstrId;
	}

	public void setDrugmstrId(long drugmstrId) {
		this.drugmstrId = drugmstrId;
	}

	public String getAllergyTestInd() {
		return this.allergyTestInd;
	}

	public void setAllergyTestInd(String allergyTestInd) {
		this.allergyTestInd = allergyTestInd;
	}

	public String getAnaestheticItemInd() {
		return this.anaestheticItemInd;
	}

	public void setAnaestheticItemInd(String anaestheticItemInd) {
		this.anaestheticItemInd = anaestheticItemInd;
	}

	public String getAntibioticGrade() {
		return this.antibioticGrade;
	}

	public void setAntibioticGrade(String antibioticGrade) {
		this.antibioticGrade = antibioticGrade;
	}

	public String getAntibioticInd() {
		return this.antibioticInd;
	}

	public void setAntibioticInd(String antibioticInd) {
		this.antibioticInd = antibioticInd;
	}

	public String getBaseDrugInd() {
		return this.baseDrugInd;
	}

	public void setBaseDrugInd(String baseDrugInd) {
		this.baseDrugInd = baseDrugInd;
	}

	public String getBloodProductInd() {
		return this.bloodProductInd;
	}

	public void setBloodProductInd(String bloodProductInd) {
		this.bloodProductInd = bloodProductInd;
	}

	public String getCodeineInd() {
		return this.codeineInd;
	}

	public void setCodeineInd(String codeineInd) {
		this.codeineInd = codeineInd;
	}

	public BigDecimal getConcentrationQty() {
		return this.concentrationQty;
	}

	public void setConcentrationQty(BigDecimal concentrationQty) {
		this.concentrationQty = concentrationQty;
	}

	public String getControlItemInd() {
		return this.controlItemInd;
	}

	public void setControlItemInd(String controlItemInd) {
		this.controlItemInd = controlItemInd;
	}

	public BigDecimal getDefaultDurationQty() {
		return this.defaultDurationQty;
	}

	public void setDefaultDurationQty(BigDecimal defaultDurationQty) {
		this.defaultDurationQty = defaultDurationQty;
	}

	public BigDecimal getDefaultDurationmstrId() {
		return this.defaultDurationmstrId;
	}

	public void setDefaultDurationmstrId(BigDecimal defaultDurationmstrId) {
		this.defaultDurationmstrId = defaultDurationmstrId;
	}

	public BigDecimal getDefaultFrequencymstrId() {
		return this.defaultFrequencymstrId;
	}

	public void setDefaultFrequencymstrId(BigDecimal defaultFrequencymstrId) {
		this.defaultFrequencymstrId = defaultFrequencymstrId;
	}

	public String getDefaultInstruction() {
		return this.defaultInstruction;
	}

	public void setDefaultInstruction(String defaultInstruction) {
		this.defaultInstruction = defaultInstruction;
	}

	public String getDefaultInstructionLang1() {
		return this.defaultInstructionLang1;
	}

	public void setDefaultInstructionLang1(String defaultInstructionLang1) {
		this.defaultInstructionLang1 = defaultInstructionLang1;
	}

	public String getDefaultInstructionLang2() {
		return this.defaultInstructionLang2;
	}

	public void setDefaultInstructionLang2(String defaultInstructionLang2) {
		this.defaultInstructionLang2 = defaultInstructionLang2;
	}

	public String getDefaultInstructionLang3() {
		return this.defaultInstructionLang3;
	}

	public void setDefaultInstructionLang3(String defaultInstructionLang3) {
		this.defaultInstructionLang3 = defaultInstructionLang3;
	}

	public BigDecimal getDefaultIntakeQty() {
		return this.defaultIntakeQty;
	}

	public void setDefaultIntakeQty(BigDecimal defaultIntakeQty) {
		this.defaultIntakeQty = defaultIntakeQty;
	}

	public BigDecimal getDefaultQty() {
		return this.defaultQty;
	}

	public void setDefaultQty(BigDecimal defaultQty) {
		this.defaultQty = defaultQty;
	}

	public String getDefaultQtyUom() {
		return this.defaultQtyUom;
	}

	public void setDefaultQtyUom(String defaultQtyUom) {
		this.defaultQtyUom = defaultQtyUom;
	}

	public String getDefaultRoa() {
		return this.defaultRoa;
	}

	public void setDefaultRoa(String defaultRoa) {
		this.defaultRoa = defaultRoa;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDirectoryInd() {
		return this.directoryInd;
	}

	public void setDirectoryInd(String directoryInd) {
		this.directoryInd = directoryInd;
	}

	public String getDispenseAuthorisedInd() {
		return this.dispenseAuthorisedInd;
	}

	public void setDispenseAuthorisedInd(String dispenseAuthorisedInd) {
		this.dispenseAuthorisedInd = dispenseAuthorisedInd;
	}

	public String getDosageRoundingRule() {
		return this.dosageRoundingRule;
	}

	public void setDosageRoundingRule(String dosageRoundingRule) {
		this.dosageRoundingRule = dosageRoundingRule;
	}

	public BigDecimal getDosageformmstrId() {
		return this.dosageformmstrId;
	}

	public void setDosageformmstrId(BigDecimal dosageformmstrId) {
		this.dosageformmstrId = dosageformmstrId;
	}

	public String getDoseUnit() {
		return this.doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public BigDecimal getDosecodemstrId() {
		return this.dosecodemstrId;
	}

	public void setDosecodemstrId(BigDecimal dosecodemstrId) {
		this.dosecodemstrId = dosecodemstrId;
	}

	public String getDrugIssueType() {
		return this.drugIssueType;
	}

	public void setDrugIssueType(String drugIssueType) {
		this.drugIssueType = drugIssueType;
	}

	public String getDrugManufactureItemInd() {
		return this.drugManufactureItemInd;
	}

	public void setDrugManufactureItemInd(String drugManufactureItemInd) {
		this.drugManufactureItemInd = drugManufactureItemInd;
	}

	public String getExportAllowInd() {
		return this.exportAllowInd;
	}

	public void setExportAllowInd(String exportAllowInd) {
		this.exportAllowInd = exportAllowInd;
	}

	public String getFirstAidDrugInd() {
		return this.firstAidDrugInd;
	}

	public void setFirstAidDrugInd(String firstAidDrugInd) {
		this.firstAidDrugInd = firstAidDrugInd;
	}

	public String getFormularyItemInd() {
		return this.formularyItemInd;
	}

	public void setFormularyItemInd(String formularyItemInd) {
		this.formularyItemInd = formularyItemInd;
	}

	public String getGenericName() {
		return this.genericName;
	}

	public void setGenericName(String genericName) {
		this.genericName = genericName;
	}

	public String getGenericNameLang1() {
		return this.genericNameLang1;
	}

	public void setGenericNameLang1(String genericNameLang1) {
		this.genericNameLang1 = genericNameLang1;
	}

	public String getGenericNameLang2() {
		return this.genericNameLang2;
	}

	public void setGenericNameLang2(String genericNameLang2) {
		this.genericNameLang2 = genericNameLang2;
	}

	public String getGenericNameLang3() {
		return this.genericNameLang3;
	}

	public void setGenericNameLang3(String genericNameLang3) {
		this.genericNameLang3 = genericNameLang3;
	}

	public BigDecimal getGenericdrugmstrId() {
		return this.genericdrugmstrId;
	}

	public void setGenericdrugmstrId(BigDecimal genericdrugmstrId) {
		this.genericdrugmstrId = genericdrugmstrId;
	}

	public String getHighRiskDrugInd() {
		return this.highRiskDrugInd;
	}

	public void setHighRiskDrugInd(String highRiskDrugInd) {
		this.highRiskDrugInd = highRiskDrugInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaterialitemmstrId() {
		return this.materialitemmstrId;
	}

	public void setMaterialitemmstrId(BigDecimal materialitemmstrId) {
		this.materialitemmstrId = materialitemmstrId;
	}

	public String getNationalStandardCode() {
		return this.nationalStandardCode;
	}

	public void setNationalStandardCode(String nationalStandardCode) {
		this.nationalStandardCode = nationalStandardCode;
	}

	public BigDecimal getNoOfDosePerBaseUom() {
		return this.noOfDosePerBaseUom;
	}

	public void setNoOfDosePerBaseUom(BigDecimal noOfDosePerBaseUom) {
		this.noOfDosePerBaseUom = noOfDosePerBaseUom;
	}

	public String getPatientSelfpayItemInd() {
		return this.patientSelfpayItemInd;
	}

	public void setPatientSelfpayItemInd(String patientSelfpayItemInd) {
		this.patientSelfpayItemInd = patientSelfpayItemInd;
	}

	public String getPoisonousItemInd() {
		return this.poisonousItemInd;
	}

	public void setPoisonousItemInd(String poisonousItemInd) {
		this.poisonousItemInd = poisonousItemInd;
	}

	public String getPrescriptionItemInd() {
		return this.prescriptionItemInd;
	}

	public void setPrescriptionItemInd(String prescriptionItemInd) {
		this.prescriptionItemInd = prescriptionItemInd;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPsychotropicItemGrade() {
		return this.psychotropicItemGrade;
	}

	public void setPsychotropicItemGrade(String psychotropicItemGrade) {
		this.psychotropicItemGrade = psychotropicItemGrade;
	}

	public String getPsychotropicItemInd() {
		return this.psychotropicItemInd;
	}

	public void setPsychotropicItemInd(String psychotropicItemInd) {
		this.psychotropicItemInd = psychotropicItemInd;
	}

	public String getStrength() {
		return this.strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public BigDecimal getSubstitutedrugmstrId() {
		return this.substitutedrugmstrId;
	}

	public void setSubstitutedrugmstrId(BigDecimal substitutedrugmstrId) {
		this.substitutedrugmstrId = substitutedrugmstrId;
	}

	public BigDecimal getVolumeQty() {
		return this.volumeQty;
	}

	public void setVolumeQty(BigDecimal volumeQty) {
		this.volumeQty = volumeQty;
	}

	public String getVolumeUnit() {
		return this.volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public BigDecimal getWeightQty() {
		return this.weightQty;
	}

	public void setWeightQty(BigDecimal weightQty) {
		this.weightQty = weightQty;
	}

	public String getWeightUnit() {
		return this.weightUnit;
	}

	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}

}