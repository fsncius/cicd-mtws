package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the STOCKACCOUNTCLOSINGDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Stockaccountclosingdetail.findAll", query="SELECT s FROM Stockaccountclosingdetail s")
public class Stockaccountclosingdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKACCOUNTCLOSINGDETAIL_ID")
	private long stockaccountclosingdetailId;

	@Column(name="ACCOUNTCLOSING_QTY")
	private BigDecimal accountclosingQty;

	@Column(name="ACCOUNTCLOSING_RETAIL_PRICE")
	private BigDecimal accountclosingRetailPrice;

	@Column(name="ACCOUNTCLOSING_WHOLESALE_PRICE")
	private BigDecimal accountclosingWholesalePrice;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	//bi-directional many-to-one association to Stockaccountclosing
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKACCOUNTCLOSING_ID")
	private Stockaccountclosing stockaccountclosing;

	//bi-directional many-to-one association to Storeitemstock
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREITEMSTOCK_ID")
	private Storeitemstock storeitemstock;

	public Stockaccountclosingdetail() {
	}

	public long getStockaccountclosingdetailId() {
		return this.stockaccountclosingdetailId;
	}

	public void setStockaccountclosingdetailId(long stockaccountclosingdetailId) {
		this.stockaccountclosingdetailId = stockaccountclosingdetailId;
	}

	public BigDecimal getAccountclosingQty() {
		return this.accountclosingQty;
	}

	public void setAccountclosingQty(BigDecimal accountclosingQty) {
		this.accountclosingQty = accountclosingQty;
	}

	public BigDecimal getAccountclosingRetailPrice() {
		return this.accountclosingRetailPrice;
	}

	public void setAccountclosingRetailPrice(BigDecimal accountclosingRetailPrice) {
		this.accountclosingRetailPrice = accountclosingRetailPrice;
	}

	public BigDecimal getAccountclosingWholesalePrice() {
		return this.accountclosingWholesalePrice;
	}

	public void setAccountclosingWholesalePrice(BigDecimal accountclosingWholesalePrice) {
		this.accountclosingWholesalePrice = accountclosingWholesalePrice;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Stockaccountclosing getStockaccountclosing() {
		return this.stockaccountclosing;
	}

	public void setStockaccountclosing(Stockaccountclosing stockaccountclosing) {
		this.stockaccountclosing = stockaccountclosing;
	}

	public Storeitemstock getStoreitemstock() {
		return this.storeitemstock;
	}

	public void setStoreitemstock(Storeitemstock storeitemstock) {
		this.storeitemstock = storeitemstock;
	}

}