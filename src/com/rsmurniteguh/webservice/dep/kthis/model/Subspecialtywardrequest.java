package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the SUBSPECIALTYWARDREQUEST database table.
 * 
 */
@Entity
@NamedQuery(name="Subspecialtywardrequest.findAll", query="SELECT s FROM Subspecialtywardrequest s")
public class Subspecialtywardrequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SUBSPECIALTYWARDREQUEST_ID")
	private long subspecialtywardrequestId;

	@Column(name="APPROVED_BY")
	private BigDecimal approvedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="APPROVED_DATETIME")
	private Date approvedDatetime;

	@Column(name="APPROVED_REMARKS")
	private String approvedRemarks;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATETIME")
	private Date enteredDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REQUEST_NO")
	private String requestNo;

	@Column(name="REQUEST_REMARKS")
	private String requestRemarks;

	@Column(name="REQUEST_STATUS")
	private String requestStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="REQUESTED_DATETIME")
	private Date requestedDatetime;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REQUESTED_BY")
	private Careprovider careprovider;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REQUEST_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr1;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="APPROVED_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr2;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REQUEST_WARDMSTR_ID")
	private Wardmstr wardmstr1;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="APPROVED_WARDMSTR_ID")
	private Wardmstr wardmstr2;

	public Subspecialtywardrequest() {
	}

	public long getSubspecialtywardrequestId() {
		return this.subspecialtywardrequestId;
	}

	public void setSubspecialtywardrequestId(long subspecialtywardrequestId) {
		this.subspecialtywardrequestId = subspecialtywardrequestId;
	}

	public BigDecimal getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(BigDecimal approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedDatetime() {
		return this.approvedDatetime;
	}

	public void setApprovedDatetime(Date approvedDatetime) {
		this.approvedDatetime = approvedDatetime;
	}

	public String getApprovedRemarks() {
		return this.approvedRemarks;
	}

	public void setApprovedRemarks(String approvedRemarks) {
		this.approvedRemarks = approvedRemarks;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredDatetime() {
		return this.enteredDatetime;
	}

	public void setEnteredDatetime(Date enteredDatetime) {
		this.enteredDatetime = enteredDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRequestNo() {
		return this.requestNo;
	}

	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}

	public String getRequestRemarks() {
		return this.requestRemarks;
	}

	public void setRequestRemarks(String requestRemarks) {
		this.requestRemarks = requestRemarks;
	}

	public String getRequestStatus() {
		return this.requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public Date getRequestedDatetime() {
		return this.requestedDatetime;
	}

	public void setRequestedDatetime(Date requestedDatetime) {
		this.requestedDatetime = requestedDatetime;
	}

	public Careprovider getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(Careprovider careprovider) {
		this.careprovider = careprovider;
	}

	public Subspecialtymstr getSubspecialtymstr1() {
		return this.subspecialtymstr1;
	}

	public void setSubspecialtymstr1(Subspecialtymstr subspecialtymstr1) {
		this.subspecialtymstr1 = subspecialtymstr1;
	}

	public Subspecialtymstr getSubspecialtymstr2() {
		return this.subspecialtymstr2;
	}

	public void setSubspecialtymstr2(Subspecialtymstr subspecialtymstr2) {
		this.subspecialtymstr2 = subspecialtymstr2;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public Wardmstr getWardmstr1() {
		return this.wardmstr1;
	}

	public void setWardmstr1(Wardmstr wardmstr1) {
		this.wardmstr1 = wardmstr1;
	}

	public Wardmstr getWardmstr2() {
		return this.wardmstr2;
	}

	public void setWardmstr2(Wardmstr wardmstr2) {
		this.wardmstr2 = wardmstr2;
	}

}