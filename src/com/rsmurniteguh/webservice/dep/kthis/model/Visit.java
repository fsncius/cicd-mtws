package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the VISIT database table.
 * 
 */
@Entity
@NamedQuery(name="Visit.findAll", query="SELECT v FROM Visit v")
public class Visit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISIT_ID")
	private long visitId;

	@Temporal(TemporalType.DATE)
	@Column(name="ADMISSION_DATETIME")
	private Date admissionDatetime;

	@Column(name="ADMISSION_TIMES")
	private BigDecimal admissionTimes;

	@Column(name="ADMIT_CLASS")
	private String admitClass;

	@Column(name="ADMIT_STATUS")
	private String admitStatus;

	@Column(name="ADMITTED_BY")
	private BigDecimal admittedBy;

	@Column(name="ADMITTING_SOURCE")
	private String admittingSource;

	@Column(name="ALT_VISIT_NO")
	private String altVisitNo;

	@Column(name="ASSURED_BY")
	private BigDecimal assuredBy;

	@Column(name="CANCEL_REASON")
	private String cancelReason;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CARD_FEE_IND")
	private String cardFeeInd;

	@Column(name="CAUSE_OF_INJURY")
	private String causeOfInjury;

	@Column(name="CONSULT_CAREPROVIDER_ID")
	private BigDecimal consultCareproviderId;

	@Temporal(TemporalType.DATE)
	@Column(name="CONSULT_END_TIME")
	private Date consultEndTime;

	@Temporal(TemporalType.DATE)
	@Column(name="CONSULT_START_TIME")
	private Date consultStartTime;

	@Column(name="CONSULT_STATUS")
	private String consultStatus;

	@Column(name="DIET_TYPE")
	private String dietType;

	@Temporal(TemporalType.DATE)
	@Column(name="DISCHARGE_DATETIME")
	private Date dischargeDatetime;

	@Column(name="DISCHARGED_BY")
	private BigDecimal dischargedBy;

	@Column(name="ENTITYMSTR_ID")
	private BigDecimal entitymstrId;

	@Column(name="HEALTH_BOOK_FEE_IND")
	private String healthBookFeeInd;

	@Column(name="IN_PATH_IND")
	private String inPathInd;

	@Column(name="IPT_VISIT_TO_DATE")
	private BigDecimal iptVisitToDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="LAST_VISIT_NO")
	private String lastVisitNo;

	@Column(name="MA_TYPE")
	private String maType;

	@Column(name="MEDICAL_CONDITION")
	private String medicalCondition;

	@Column(name="NO_OF_LIVING_CHILDREN")
	private BigDecimal noOfLivingChildren;

	@Column(name="NURSING_LEVEL")
	private String nursingLevel;

	@Column(name="OPT_VISIT_TO_DATE")
	private BigDecimal optVisitToDate;

	@Column(name="ORGANISATION_ID")
	private BigDecimal organisationId;

	@Column(name="OTHER_CANCEL_REASON")
	private String otherCancelReason;

	@Column(name="OTHER_VISIT_REASON")
	private String otherVisitReason;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Temporal(TemporalType.DATE)
	@Column(name="PREADMISSION_DATETIME")
	private Date preadmissionDatetime;

	@Column(name="PREADMISSION_NO")
	private String preadmissionNo;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="QUEUE_NO")
	private String queueNo;

	@Column(name="QUEUE_PRIORITY")
	private String queuePriority;

	@Column(name="REFERENCE_NO")
	private String referenceNo;

	private String remarks;

	@Temporal(TemporalType.DATE)
	@Column(name="REVIEW_DATE")
	private Date reviewDate;

	@Column(name="SPECIAL_CARE_IND")
	private String specialCareInd;

	@Column(name="TREATMENTGROUPMSTR_ID")
	private BigDecimal treatmentgroupmstrId;

	@Column(name="VISIT_NO")
	private String visitNo;

	@Column(name="VISIT_PRIORITY")
	private String visitPriority;

	@Column(name="VISIT_REASON")
	private String visitReason;

	@Column(name="VISIT_TYPE")
	private String visitType;

	//bi-directional many-to-one association to Bedhistory
	@OneToMany(mappedBy="visit")
	private List<Bedhistory> bedhistories;

	//bi-directional many-to-one association to Chargerefundrequisition
	@OneToMany(mappedBy="visit")
	private List<Chargerefundrequisition> chargerefundrequisitions;

	//bi-directional many-to-one association to Consultationrequisition
	@OneToMany(mappedBy="visit")
	private List<Consultationrequisition> consultationrequisitions;

	//bi-directional many-to-one association to CounterDaychargebillAy
	@OneToMany(mappedBy="visit")
	private List<CounterDaychargebillAy> counterDaychargebillAys;

	//bi-directional many-to-one association to Deposit
	@OneToMany(mappedBy="visit")
	private List<Deposit> deposits;

	//bi-directional many-to-one association to Discharge
	@OneToMany(mappedBy="visit")
	private List<Discharge> discharges;

	//bi-directional many-to-one association to DischargedCertificate
	@OneToMany(mappedBy="visit")
	private List<DischargedCertificate> dischargedCertificates;


	//bi-directional many-to-one association to Materialconsumption
	@OneToMany(mappedBy="visit")
	private List<Materialconsumption> materialconsumptions;

	//bi-directional many-to-one association to MedicalResume
	@OneToMany(mappedBy="visit")
	private List<MedicalResume> medicalResumes;

	//bi-directional many-to-one association to Patientaccount
	@OneToMany(mappedBy="visit")
	private List<Patientaccount> patientaccounts;

	//bi-directional many-to-one association to Patienttypeclasshistory
	@OneToMany(mappedBy="visit")
	private List<Patienttypeclasshistory> patienttypeclasshistories;

	//bi-directional many-to-one association to Regnvisitactivation
	@OneToMany(mappedBy="visit")
	private List<Regnvisitactivation> regnvisitactivations;

	//bi-directional many-to-one association to Subspecialtywardrequest
	@OneToMany(mappedBy="visit")
	private List<Subspecialtywardrequest> subspecialtywardrequests;

	//bi-directional many-to-one association to Bedmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BEDMSTR_ID")
	private Bedmstr bedmstr;

	//bi-directional many-to-one association to Drgsmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DRGSMSTR_ID")
	private Drgsmstr drgsmstr;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ADMITTED_AT")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to Patient
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENT_ID")
	private Patient patient;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="WARDMSTR_ID")
	private Wardmstr wardmstr;

	//bi-directional many-to-one association to Visitappointment
	@OneToMany(mappedBy="visit")
	private List<Visitappointment> visitappointments;

	//bi-directional many-to-one association to Visitdiagnosi
	@OneToMany(mappedBy="visit")
	private List<Visitdiagnosi> visitdiagnosis;

	//bi-directional many-to-one association to Visitdoctor
	@OneToMany(mappedBy="visit")
	private List<Visitdoctor> visitdoctors;

	//bi-directional many-to-one association to Visithealthcareinfo
	@OneToMany(mappedBy="visit")
	private List<Visithealthcareinfo> visithealthcareinfos;

	//bi-directional many-to-one association to Visitlocation
	@OneToMany(mappedBy="visit")
	private List<Visitlocation> visitlocations;

	//bi-directional many-to-one association to Visitpicture
	@OneToMany(mappedBy="visit")
	private List<Visitpicture> visitpictures;

	//bi-directional many-to-one association to Visitreferral
	@OneToMany(mappedBy="visit")
	private List<Visitreferral> visitreferrals;

	//bi-directional many-to-one association to Visitregntype
	@OneToMany(mappedBy="visit")
	private List<Visitregntype> visitregntypes;

	//bi-directional many-to-one association to Visitsubspecialtyhistory
	@OneToMany(mappedBy="visit")
	private List<Visitsubspecialtyhistory> visitsubspecialtyhistories;

	//bi-directional many-to-one association to Visitvitalsignitem
	@OneToMany(mappedBy="visit")
	private List<Visitvitalsignitem> visitvitalsignitems;

	//bi-directional many-to-one association to Visitvitalsignrecord
	@OneToMany(mappedBy="visit")
	private List<Visitvitalsignrecord> visitvitalsignrecords;

	//bi-directional many-to-one association to Visitwardhistory
	@OneToMany(mappedBy="visit")
	private List<Visitwardhistory> visitwardhistories;

	public Visit() {
	}

	public long getVisitId() {
		return this.visitId;
	}

	public void setVisitId(long visitId) {
		this.visitId = visitId;
	}

	public Date getAdmissionDatetime() {
		return this.admissionDatetime;
	}

	public void setAdmissionDatetime(Date admissionDatetime) {
		this.admissionDatetime = admissionDatetime;
	}

	public BigDecimal getAdmissionTimes() {
		return this.admissionTimes;
	}

	public void setAdmissionTimes(BigDecimal admissionTimes) {
		this.admissionTimes = admissionTimes;
	}

	public String getAdmitClass() {
		return this.admitClass;
	}

	public void setAdmitClass(String admitClass) {
		this.admitClass = admitClass;
	}

	public String getAdmitStatus() {
		return this.admitStatus;
	}

	public void setAdmitStatus(String admitStatus) {
		this.admitStatus = admitStatus;
	}

	public BigDecimal getAdmittedBy() {
		return this.admittedBy;
	}

	public void setAdmittedBy(BigDecimal admittedBy) {
		this.admittedBy = admittedBy;
	}

	public String getAdmittingSource() {
		return this.admittingSource;
	}

	public void setAdmittingSource(String admittingSource) {
		this.admittingSource = admittingSource;
	}

	public String getAltVisitNo() {
		return this.altVisitNo;
	}

	public void setAltVisitNo(String altVisitNo) {
		this.altVisitNo = altVisitNo;
	}

	public BigDecimal getAssuredBy() {
		return this.assuredBy;
	}

	public void setAssuredBy(BigDecimal assuredBy) {
		this.assuredBy = assuredBy;
	}

	public String getCancelReason() {
		return this.cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public String getCardFeeInd() {
		return this.cardFeeInd;
	}

	public void setCardFeeInd(String cardFeeInd) {
		this.cardFeeInd = cardFeeInd;
	}

	public String getCauseOfInjury() {
		return this.causeOfInjury;
	}

	public void setCauseOfInjury(String causeOfInjury) {
		this.causeOfInjury = causeOfInjury;
	}

	public BigDecimal getConsultCareproviderId() {
		return this.consultCareproviderId;
	}

	public void setConsultCareproviderId(BigDecimal consultCareproviderId) {
		this.consultCareproviderId = consultCareproviderId;
	}

	public Date getConsultEndTime() {
		return this.consultEndTime;
	}

	public void setConsultEndTime(Date consultEndTime) {
		this.consultEndTime = consultEndTime;
	}

	public Date getConsultStartTime() {
		return this.consultStartTime;
	}

	public void setConsultStartTime(Date consultStartTime) {
		this.consultStartTime = consultStartTime;
	}

	public String getConsultStatus() {
		return this.consultStatus;
	}

	public void setConsultStatus(String consultStatus) {
		this.consultStatus = consultStatus;
	}

	public String getDietType() {
		return this.dietType;
	}

	public void setDietType(String dietType) {
		this.dietType = dietType;
	}

	public Date getDischargeDatetime() {
		return this.dischargeDatetime;
	}

	public void setDischargeDatetime(Date dischargeDatetime) {
		this.dischargeDatetime = dischargeDatetime;
	}

	public BigDecimal getDischargedBy() {
		return this.dischargedBy;
	}

	public void setDischargedBy(BigDecimal dischargedBy) {
		this.dischargedBy = dischargedBy;
	}

	public BigDecimal getEntitymstrId() {
		return this.entitymstrId;
	}

	public void setEntitymstrId(BigDecimal entitymstrId) {
		this.entitymstrId = entitymstrId;
	}

	public String getHealthBookFeeInd() {
		return this.healthBookFeeInd;
	}

	public void setHealthBookFeeInd(String healthBookFeeInd) {
		this.healthBookFeeInd = healthBookFeeInd;
	}

	public String getInPathInd() {
		return this.inPathInd;
	}

	public void setInPathInd(String inPathInd) {
		this.inPathInd = inPathInd;
	}

	public BigDecimal getIptVisitToDate() {
		return this.iptVisitToDate;
	}

	public void setIptVisitToDate(BigDecimal iptVisitToDate) {
		this.iptVisitToDate = iptVisitToDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getLastVisitNo() {
		return this.lastVisitNo;
	}

	public void setLastVisitNo(String lastVisitNo) {
		this.lastVisitNo = lastVisitNo;
	}

	public String getMaType() {
		return this.maType;
	}

	public void setMaType(String maType) {
		this.maType = maType;
	}

	public String getMedicalCondition() {
		return this.medicalCondition;
	}

	public void setMedicalCondition(String medicalCondition) {
		this.medicalCondition = medicalCondition;
	}

	public BigDecimal getNoOfLivingChildren() {
		return this.noOfLivingChildren;
	}

	public void setNoOfLivingChildren(BigDecimal noOfLivingChildren) {
		this.noOfLivingChildren = noOfLivingChildren;
	}

	public String getNursingLevel() {
		return this.nursingLevel;
	}

	public void setNursingLevel(String nursingLevel) {
		this.nursingLevel = nursingLevel;
	}

	public BigDecimal getOptVisitToDate() {
		return this.optVisitToDate;
	}

	public void setOptVisitToDate(BigDecimal optVisitToDate) {
		this.optVisitToDate = optVisitToDate;
	}

	public BigDecimal getOrganisationId() {
		return this.organisationId;
	}

	public void setOrganisationId(BigDecimal organisationId) {
		this.organisationId = organisationId;
	}

	public String getOtherCancelReason() {
		return this.otherCancelReason;
	}

	public void setOtherCancelReason(String otherCancelReason) {
		this.otherCancelReason = otherCancelReason;
	}

	public String getOtherVisitReason() {
		return this.otherVisitReason;
	}

	public void setOtherVisitReason(String otherVisitReason) {
		this.otherVisitReason = otherVisitReason;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public Date getPreadmissionDatetime() {
		return this.preadmissionDatetime;
	}

	public void setPreadmissionDatetime(Date preadmissionDatetime) {
		this.preadmissionDatetime = preadmissionDatetime;
	}

	public String getPreadmissionNo() {
		return this.preadmissionNo;
	}

	public void setPreadmissionNo(String preadmissionNo) {
		this.preadmissionNo = preadmissionNo;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getQueueNo() {
		return this.queueNo;
	}

	public void setQueueNo(String queueNo) {
		this.queueNo = queueNo;
	}

	public String getQueuePriority() {
		return this.queuePriority;
	}

	public void setQueuePriority(String queuePriority) {
		this.queuePriority = queuePriority;
	}

	public String getReferenceNo() {
		return this.referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getReviewDate() {
		return this.reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public String getSpecialCareInd() {
		return this.specialCareInd;
	}

	public void setSpecialCareInd(String specialCareInd) {
		this.specialCareInd = specialCareInd;
	}

	public BigDecimal getTreatmentgroupmstrId() {
		return this.treatmentgroupmstrId;
	}

	public void setTreatmentgroupmstrId(BigDecimal treatmentgroupmstrId) {
		this.treatmentgroupmstrId = treatmentgroupmstrId;
	}

	public String getVisitNo() {
		return this.visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public String getVisitPriority() {
		return this.visitPriority;
	}

	public void setVisitPriority(String visitPriority) {
		this.visitPriority = visitPriority;
	}

	public String getVisitReason() {
		return this.visitReason;
	}

	public void setVisitReason(String visitReason) {
		this.visitReason = visitReason;
	}

	public String getVisitType() {
		return this.visitType;
	}

	public void setVisitType(String visitType) {
		this.visitType = visitType;
	}

	public List<Bedhistory> getBedhistories() {
		return this.bedhistories;
	}

	public void setBedhistories(List<Bedhistory> bedhistories) {
		this.bedhistories = bedhistories;
	}

	public Bedhistory addBedhistory(Bedhistory bedhistory) {
		getBedhistories().add(bedhistory);
		bedhistory.setVisit(this);

		return bedhistory;
	}

	public Bedhistory removeBedhistory(Bedhistory bedhistory) {
		getBedhistories().remove(bedhistory);
		bedhistory.setVisit(null);

		return bedhistory;
	}

	public List<Chargerefundrequisition> getChargerefundrequisitions() {
		return this.chargerefundrequisitions;
	}

	public void setChargerefundrequisitions(List<Chargerefundrequisition> chargerefundrequisitions) {
		this.chargerefundrequisitions = chargerefundrequisitions;
	}

	public Chargerefundrequisition addChargerefundrequisition(Chargerefundrequisition chargerefundrequisition) {
		getChargerefundrequisitions().add(chargerefundrequisition);
		chargerefundrequisition.setVisit(this);

		return chargerefundrequisition;
	}

	public Chargerefundrequisition removeChargerefundrequisition(Chargerefundrequisition chargerefundrequisition) {
		getChargerefundrequisitions().remove(chargerefundrequisition);
		chargerefundrequisition.setVisit(null);

		return chargerefundrequisition;
	}

	public List<Consultationrequisition> getConsultationrequisitions() {
		return this.consultationrequisitions;
	}

	public void setConsultationrequisitions(List<Consultationrequisition> consultationrequisitions) {
		this.consultationrequisitions = consultationrequisitions;
	}

	public Consultationrequisition addConsultationrequisition(Consultationrequisition consultationrequisition) {
		getConsultationrequisitions().add(consultationrequisition);
		consultationrequisition.setVisit(this);

		return consultationrequisition;
	}

	public Consultationrequisition removeConsultationrequisition(Consultationrequisition consultationrequisition) {
		getConsultationrequisitions().remove(consultationrequisition);
		consultationrequisition.setVisit(null);

		return consultationrequisition;
	}

	public List<CounterDaychargebillAy> getCounterDaychargebillAys() {
		return this.counterDaychargebillAys;
	}

	public void setCounterDaychargebillAys(List<CounterDaychargebillAy> counterDaychargebillAys) {
		this.counterDaychargebillAys = counterDaychargebillAys;
	}

	public CounterDaychargebillAy addCounterDaychargebillAy(CounterDaychargebillAy counterDaychargebillAy) {
		getCounterDaychargebillAys().add(counterDaychargebillAy);
		counterDaychargebillAy.setVisit(this);

		return counterDaychargebillAy;
	}

	public CounterDaychargebillAy removeCounterDaychargebillAy(CounterDaychargebillAy counterDaychargebillAy) {
		getCounterDaychargebillAys().remove(counterDaychargebillAy);
		counterDaychargebillAy.setVisit(null);

		return counterDaychargebillAy;
	}

	public List<Deposit> getDeposits() {
		return this.deposits;
	}

	public void setDeposits(List<Deposit> deposits) {
		this.deposits = deposits;
	}

	public Deposit addDeposit(Deposit deposit) {
		getDeposits().add(deposit);
		deposit.setVisit(this);

		return deposit;
	}

	public Deposit removeDeposit(Deposit deposit) {
		getDeposits().remove(deposit);
		deposit.setVisit(null);

		return deposit;
	}

	public List<Discharge> getDischarges() {
		return this.discharges;
	}

	public void setDischarges(List<Discharge> discharges) {
		this.discharges = discharges;
	}

	public Discharge addDischarge(Discharge discharge) {
		getDischarges().add(discharge);
		discharge.setVisit(this);

		return discharge;
	}

	public Discharge removeDischarge(Discharge discharge) {
		getDischarges().remove(discharge);
		discharge.setVisit(null);

		return discharge;
	}

	public List<DischargedCertificate> getDischargedCertificates() {
		return this.dischargedCertificates;
	}

	public void setDischargedCertificates(List<DischargedCertificate> dischargedCertificates) {
		this.dischargedCertificates = dischargedCertificates;
	}

	public DischargedCertificate addDischargedCertificate(DischargedCertificate dischargedCertificate) {
		getDischargedCertificates().add(dischargedCertificate);
		dischargedCertificate.setVisit(this);

		return dischargedCertificate;
	}

	public DischargedCertificate removeDischargedCertificate(DischargedCertificate dischargedCertificate) {
		getDischargedCertificates().remove(dischargedCertificate);
		dischargedCertificate.setVisit(null);

		return dischargedCertificate;
	}


	public List<Materialconsumption> getMaterialconsumptions() {
		return this.materialconsumptions;
	}

	public void setMaterialconsumptions(List<Materialconsumption> materialconsumptions) {
		this.materialconsumptions = materialconsumptions;
	}

	public Materialconsumption addMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().add(materialconsumption);
		materialconsumption.setVisit(this);

		return materialconsumption;
	}

	public Materialconsumption removeMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().remove(materialconsumption);
		materialconsumption.setVisit(null);

		return materialconsumption;
	}

	public List<MedicalResume> getMedicalResumes() {
		return this.medicalResumes;
	}

	public void setMedicalResumes(List<MedicalResume> medicalResumes) {
		this.medicalResumes = medicalResumes;
	}

	public MedicalResume addMedicalResume(MedicalResume medicalResume) {
		getMedicalResumes().add(medicalResume);
		medicalResume.setVisit(this);

		return medicalResume;
	}

	public MedicalResume removeMedicalResume(MedicalResume medicalResume) {
		getMedicalResumes().remove(medicalResume);
		medicalResume.setVisit(null);

		return medicalResume;
	}

	public List<Patientaccount> getPatientaccounts() {
		return this.patientaccounts;
	}

	public void setPatientaccounts(List<Patientaccount> patientaccounts) {
		this.patientaccounts = patientaccounts;
	}

	public Patientaccount addPatientaccount(Patientaccount patientaccount) {
		getPatientaccounts().add(patientaccount);
		patientaccount.setVisit(this);

		return patientaccount;
	}

	public Patientaccount removePatientaccount(Patientaccount patientaccount) {
		getPatientaccounts().remove(patientaccount);
		patientaccount.setVisit(null);

		return patientaccount;
	}

	public List<Patienttypeclasshistory> getPatienttypeclasshistories() {
		return this.patienttypeclasshistories;
	}

	public void setPatienttypeclasshistories(List<Patienttypeclasshistory> patienttypeclasshistories) {
		this.patienttypeclasshistories = patienttypeclasshistories;
	}

	public Patienttypeclasshistory addPatienttypeclasshistory(Patienttypeclasshistory patienttypeclasshistory) {
		getPatienttypeclasshistories().add(patienttypeclasshistory);
		patienttypeclasshistory.setVisit(this);

		return patienttypeclasshistory;
	}

	public Patienttypeclasshistory removePatienttypeclasshistory(Patienttypeclasshistory patienttypeclasshistory) {
		getPatienttypeclasshistories().remove(patienttypeclasshistory);
		patienttypeclasshistory.setVisit(null);

		return patienttypeclasshistory;
	}

	public List<Regnvisitactivation> getRegnvisitactivations() {
		return this.regnvisitactivations;
	}

	public void setRegnvisitactivations(List<Regnvisitactivation> regnvisitactivations) {
		this.regnvisitactivations = regnvisitactivations;
	}

	public Regnvisitactivation addRegnvisitactivation(Regnvisitactivation regnvisitactivation) {
		getRegnvisitactivations().add(regnvisitactivation);
		regnvisitactivation.setVisit(this);

		return regnvisitactivation;
	}

	public Regnvisitactivation removeRegnvisitactivation(Regnvisitactivation regnvisitactivation) {
		getRegnvisitactivations().remove(regnvisitactivation);
		regnvisitactivation.setVisit(null);

		return regnvisitactivation;
	}

	public List<Subspecialtywardrequest> getSubspecialtywardrequests() {
		return this.subspecialtywardrequests;
	}

	public void setSubspecialtywardrequests(List<Subspecialtywardrequest> subspecialtywardrequests) {
		this.subspecialtywardrequests = subspecialtywardrequests;
	}

	public Subspecialtywardrequest addSubspecialtywardrequest(Subspecialtywardrequest subspecialtywardrequest) {
		getSubspecialtywardrequests().add(subspecialtywardrequest);
		subspecialtywardrequest.setVisit(this);

		return subspecialtywardrequest;
	}

	public Subspecialtywardrequest removeSubspecialtywardrequest(Subspecialtywardrequest subspecialtywardrequest) {
		getSubspecialtywardrequests().remove(subspecialtywardrequest);
		subspecialtywardrequest.setVisit(null);

		return subspecialtywardrequest;
	}

	public Bedmstr getBedmstr() {
		return this.bedmstr;
	}

	public void setBedmstr(Bedmstr bedmstr) {
		this.bedmstr = bedmstr;
	}

	public Drgsmstr getDrgsmstr() {
		return this.drgsmstr;
	}

	public void setDrgsmstr(Drgsmstr drgsmstr) {
		this.drgsmstr = drgsmstr;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

	public List<Visitappointment> getVisitappointments() {
		return this.visitappointments;
	}

	public void setVisitappointments(List<Visitappointment> visitappointments) {
		this.visitappointments = visitappointments;
	}

	public Visitappointment addVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().add(visitappointment);
		visitappointment.setVisit(this);

		return visitappointment;
	}

	public Visitappointment removeVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().remove(visitappointment);
		visitappointment.setVisit(null);

		return visitappointment;
	}

	public List<Visitdiagnosi> getVisitdiagnosis() {
		return this.visitdiagnosis;
	}

	public void setVisitdiagnosis(List<Visitdiagnosi> visitdiagnosis) {
		this.visitdiagnosis = visitdiagnosis;
	}

	public Visitdiagnosi addVisitdiagnosi(Visitdiagnosi visitdiagnosi) {
		getVisitdiagnosis().add(visitdiagnosi);
		visitdiagnosi.setVisit(this);

		return visitdiagnosi;
	}

	public Visitdiagnosi removeVisitdiagnosi(Visitdiagnosi visitdiagnosi) {
		getVisitdiagnosis().remove(visitdiagnosi);
		visitdiagnosi.setVisit(null);

		return visitdiagnosi;
	}

	public List<Visitdoctor> getVisitdoctors() {
		return this.visitdoctors;
	}

	public void setVisitdoctors(List<Visitdoctor> visitdoctors) {
		this.visitdoctors = visitdoctors;
	}

	public Visitdoctor addVisitdoctor(Visitdoctor visitdoctor) {
		getVisitdoctors().add(visitdoctor);
		visitdoctor.setVisit(this);

		return visitdoctor;
	}

	public Visitdoctor removeVisitdoctor(Visitdoctor visitdoctor) {
		getVisitdoctors().remove(visitdoctor);
		visitdoctor.setVisit(null);

		return visitdoctor;
	}

	public List<Visithealthcareinfo> getVisithealthcareinfos() {
		return this.visithealthcareinfos;
	}

	public void setVisithealthcareinfos(List<Visithealthcareinfo> visithealthcareinfos) {
		this.visithealthcareinfos = visithealthcareinfos;
	}

	public Visithealthcareinfo addVisithealthcareinfo(Visithealthcareinfo visithealthcareinfo) {
		getVisithealthcareinfos().add(visithealthcareinfo);
		visithealthcareinfo.setVisit(this);

		return visithealthcareinfo;
	}

	public Visithealthcareinfo removeVisithealthcareinfo(Visithealthcareinfo visithealthcareinfo) {
		getVisithealthcareinfos().remove(visithealthcareinfo);
		visithealthcareinfo.setVisit(null);

		return visithealthcareinfo;
	}

	public List<Visitlocation> getVisitlocations() {
		return this.visitlocations;
	}

	public void setVisitlocations(List<Visitlocation> visitlocations) {
		this.visitlocations = visitlocations;
	}

	public Visitlocation addVisitlocation(Visitlocation visitlocation) {
		getVisitlocations().add(visitlocation);
		visitlocation.setVisit(this);

		return visitlocation;
	}

	public Visitlocation removeVisitlocation(Visitlocation visitlocation) {
		getVisitlocations().remove(visitlocation);
		visitlocation.setVisit(null);

		return visitlocation;
	}

	public List<Visitpicture> getVisitpictures() {
		return this.visitpictures;
	}

	public void setVisitpictures(List<Visitpicture> visitpictures) {
		this.visitpictures = visitpictures;
	}

	public Visitpicture addVisitpicture(Visitpicture visitpicture) {
		getVisitpictures().add(visitpicture);
		visitpicture.setVisit(this);

		return visitpicture;
	}

	public Visitpicture removeVisitpicture(Visitpicture visitpicture) {
		getVisitpictures().remove(visitpicture);
		visitpicture.setVisit(null);

		return visitpicture;
	}

	public List<Visitreferral> getVisitreferrals() {
		return this.visitreferrals;
	}

	public void setVisitreferrals(List<Visitreferral> visitreferrals) {
		this.visitreferrals = visitreferrals;
	}

	public Visitreferral addVisitreferral(Visitreferral visitreferral) {
		getVisitreferrals().add(visitreferral);
		visitreferral.setVisit(this);

		return visitreferral;
	}

	public Visitreferral removeVisitreferral(Visitreferral visitreferral) {
		getVisitreferrals().remove(visitreferral);
		visitreferral.setVisit(null);

		return visitreferral;
	}

	public List<Visitregntype> getVisitregntypes() {
		return this.visitregntypes;
	}

	public void setVisitregntypes(List<Visitregntype> visitregntypes) {
		this.visitregntypes = visitregntypes;
	}

	public Visitregntype addVisitregntype(Visitregntype visitregntype) {
		getVisitregntypes().add(visitregntype);
		visitregntype.setVisit(this);

		return visitregntype;
	}

	public Visitregntype removeVisitregntype(Visitregntype visitregntype) {
		getVisitregntypes().remove(visitregntype);
		visitregntype.setVisit(null);

		return visitregntype;
	}

	public List<Visitsubspecialtyhistory> getVisitsubspecialtyhistories() {
		return this.visitsubspecialtyhistories;
	}

	public void setVisitsubspecialtyhistories(List<Visitsubspecialtyhistory> visitsubspecialtyhistories) {
		this.visitsubspecialtyhistories = visitsubspecialtyhistories;
	}

	public Visitsubspecialtyhistory addVisitsubspecialtyhistory(Visitsubspecialtyhistory visitsubspecialtyhistory) {
		getVisitsubspecialtyhistories().add(visitsubspecialtyhistory);
		visitsubspecialtyhistory.setVisit(this);

		return visitsubspecialtyhistory;
	}

	public Visitsubspecialtyhistory removeVisitsubspecialtyhistory(Visitsubspecialtyhistory visitsubspecialtyhistory) {
		getVisitsubspecialtyhistories().remove(visitsubspecialtyhistory);
		visitsubspecialtyhistory.setVisit(null);

		return visitsubspecialtyhistory;
	}

	public List<Visitvitalsignitem> getVisitvitalsignitems() {
		return this.visitvitalsignitems;
	}

	public void setVisitvitalsignitems(List<Visitvitalsignitem> visitvitalsignitems) {
		this.visitvitalsignitems = visitvitalsignitems;
	}

	public Visitvitalsignitem addVisitvitalsignitem(Visitvitalsignitem visitvitalsignitem) {
		getVisitvitalsignitems().add(visitvitalsignitem);
		visitvitalsignitem.setVisit(this);

		return visitvitalsignitem;
	}

	public Visitvitalsignitem removeVisitvitalsignitem(Visitvitalsignitem visitvitalsignitem) {
		getVisitvitalsignitems().remove(visitvitalsignitem);
		visitvitalsignitem.setVisit(null);

		return visitvitalsignitem;
	}

	public List<Visitvitalsignrecord> getVisitvitalsignrecords() {
		return this.visitvitalsignrecords;
	}

	public void setVisitvitalsignrecords(List<Visitvitalsignrecord> visitvitalsignrecords) {
		this.visitvitalsignrecords = visitvitalsignrecords;
	}

	public Visitvitalsignrecord addVisitvitalsignrecord(Visitvitalsignrecord visitvitalsignrecord) {
		getVisitvitalsignrecords().add(visitvitalsignrecord);
		visitvitalsignrecord.setVisit(this);

		return visitvitalsignrecord;
	}

	public Visitvitalsignrecord removeVisitvitalsignrecord(Visitvitalsignrecord visitvitalsignrecord) {
		getVisitvitalsignrecords().remove(visitvitalsignrecord);
		visitvitalsignrecord.setVisit(null);

		return visitvitalsignrecord;
	}

	public List<Visitwardhistory> getVisitwardhistories() {
		return this.visitwardhistories;
	}

	public void setVisitwardhistories(List<Visitwardhistory> visitwardhistories) {
		this.visitwardhistories = visitwardhistories;
	}

	public Visitwardhistory addVisitwardhistory(Visitwardhistory visitwardhistory) {
		getVisitwardhistories().add(visitwardhistory);
		visitwardhistory.setVisit(this);

		return visitwardhistory;
	}

	public Visitwardhistory removeVisitwardhistory(Visitwardhistory visitwardhistory) {
		getVisitwardhistories().remove(visitwardhistory);
		visitwardhistory.setVisit(null);

		return visitwardhistory;
	}

}