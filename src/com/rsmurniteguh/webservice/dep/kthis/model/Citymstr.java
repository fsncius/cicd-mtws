package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CITYMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Citymstr.findAll", query="SELECT c FROM Citymstr c")
public class Citymstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CITYMSTR_ID")
	private long citymstrId;

	@Column(name="CITY_CODE")
	private String cityCode;

	@Column(name="CITY_DESC")
	private String cityDesc;

	@Column(name="CITY_DESC_LANG1")
	private String cityDescLang1;

	@Column(name="CITY_DESC_LANG2")
	private String cityDescLang2;

	@Column(name="CITY_DESC_LANG3")
	private String cityDescLang3;

	@Column(name="CODE_SEQ")
	private BigDecimal codeSeq;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SHORT_CODE")
	private String shortCode;

	//bi-directional many-to-one association to Statemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATEMSTR_ID")
	private Statemstr statemstr;

	//bi-directional many-to-one association to Districtmstr
	@OneToMany(mappedBy="citymstr")
	private List<Districtmstr> districtmstrs;

	//bi-directional many-to-one association to Vendoraddress
	@OneToMany(mappedBy="citymstr")
	private List<Vendoraddress> vendoraddresses;

	public Citymstr() {
	}

	public long getCitymstrId() {
		return this.citymstrId;
	}

	public void setCitymstrId(long citymstrId) {
		this.citymstrId = citymstrId;
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityDesc() {
		return this.cityDesc;
	}

	public void setCityDesc(String cityDesc) {
		this.cityDesc = cityDesc;
	}

	public String getCityDescLang1() {
		return this.cityDescLang1;
	}

	public void setCityDescLang1(String cityDescLang1) {
		this.cityDescLang1 = cityDescLang1;
	}

	public String getCityDescLang2() {
		return this.cityDescLang2;
	}

	public void setCityDescLang2(String cityDescLang2) {
		this.cityDescLang2 = cityDescLang2;
	}

	public String getCityDescLang3() {
		return this.cityDescLang3;
	}

	public void setCityDescLang3(String cityDescLang3) {
		this.cityDescLang3 = cityDescLang3;
	}

	public BigDecimal getCodeSeq() {
		return this.codeSeq;
	}

	public void setCodeSeq(BigDecimal codeSeq) {
		this.codeSeq = codeSeq;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Statemstr getStatemstr() {
		return this.statemstr;
	}

	public void setStatemstr(Statemstr statemstr) {
		this.statemstr = statemstr;
	}

	public List<Districtmstr> getDistrictmstrs() {
		return this.districtmstrs;
	}

	public void setDistrictmstrs(List<Districtmstr> districtmstrs) {
		this.districtmstrs = districtmstrs;
	}

	public Districtmstr addDistrictmstr(Districtmstr districtmstr) {
		getDistrictmstrs().add(districtmstr);
		districtmstr.setCitymstr(this);

		return districtmstr;
	}

	public Districtmstr removeDistrictmstr(Districtmstr districtmstr) {
		getDistrictmstrs().remove(districtmstr);
		districtmstr.setCitymstr(null);

		return districtmstr;
	}

	public List<Vendoraddress> getVendoraddresses() {
		return this.vendoraddresses;
	}

	public void setVendoraddresses(List<Vendoraddress> vendoraddresses) {
		this.vendoraddresses = vendoraddresses;
	}

	public Vendoraddress addVendoraddress(Vendoraddress vendoraddress) {
		getVendoraddresses().add(vendoraddress);
		vendoraddress.setCitymstr(this);

		return vendoraddress;
	}

	public Vendoraddress removeVendoraddress(Vendoraddress vendoraddress) {
		getVendoraddresses().remove(vendoraddress);
		vendoraddress.setCitymstr(null);

		return vendoraddress;
	}

}