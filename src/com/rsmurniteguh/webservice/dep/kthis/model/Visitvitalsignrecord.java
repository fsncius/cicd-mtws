package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITVITALSIGNRECORDS database table.
 * 
 */
@Entity
@Table(name="VISITVITALSIGNRECORDS")
@NamedQuery(name="Visitvitalsignrecord.findAll", query="SELECT v FROM Visitvitalsignrecord v")
public class Visitvitalsignrecord implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITVITALSIGNRECORDS_ID")
	private long visitvitalsignrecordsId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="RECORD_DATETIME")
	private Date recordDatetime;

	@Column(name="VITALSIGNITEM_VALUE")
	private String vitalsignitemValue;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Vitalsignitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VITALSIGNITEMMSTR_ID")
	private Vitalsignitemmstr vitalsignitemmstr;

	public Visitvitalsignrecord() {
	}

	public long getVisitvitalsignrecordsId() {
		return this.visitvitalsignrecordsId;
	}

	public void setVisitvitalsignrecordsId(long visitvitalsignrecordsId) {
		this.visitvitalsignrecordsId = visitvitalsignrecordsId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Date getRecordDatetime() {
		return this.recordDatetime;
	}

	public void setRecordDatetime(Date recordDatetime) {
		this.recordDatetime = recordDatetime;
	}

	public String getVitalsignitemValue() {
		return this.vitalsignitemValue;
	}

	public void setVitalsignitemValue(String vitalsignitemValue) {
		this.vitalsignitemValue = vitalsignitemValue;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public Vitalsignitemmstr getVitalsignitemmstr() {
		return this.vitalsignitemmstr;
	}

	public void setVitalsignitemmstr(Vitalsignitemmstr vitalsignitemmstr) {
		this.vitalsignitemmstr = vitalsignitemmstr;
	}

}