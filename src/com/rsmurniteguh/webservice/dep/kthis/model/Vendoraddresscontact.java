package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VENDORADDRESSCONTACT database table.
 * 
 */
@Entity
@NamedQuery(name="Vendoraddresscontact.findAll", query="SELECT v FROM Vendoraddresscontact v")
public class Vendoraddresscontact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VENDORADDRESSCONTACT_ID")
	private long vendoraddresscontactId;

	@Column(name="DEFAULT_CONTACT_IND")
	private String defaultContactInd;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	private String designation;

	private String email;

	@Column(name="FAX_NO")
	private String faxNo;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MOBILE_PHONE_NO")
	private String mobilePhoneNo;

	@Column(name="PERSON_NAME")
	private String personName;

	@Column(name="PHONE_NO")
	private String phoneNo;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String title;

	//bi-directional many-to-one association to Vendoraddress
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VENDORADDRESS_ID")
	private Vendoraddress vendoraddress;

	public Vendoraddresscontact() {
	}

	public long getVendoraddresscontactId() {
		return this.vendoraddresscontactId;
	}

	public void setVendoraddresscontactId(long vendoraddresscontactId) {
		this.vendoraddresscontactId = vendoraddresscontactId;
	}

	public String getDefaultContactInd() {
		return this.defaultContactInd;
	}

	public void setDefaultContactInd(String defaultContactInd) {
		this.defaultContactInd = defaultContactInd;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFaxNo() {
		return this.faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMobilePhoneNo() {
		return this.mobilePhoneNo;
	}

	public void setMobilePhoneNo(String mobilePhoneNo) {
		this.mobilePhoneNo = mobilePhoneNo;
	}

	public String getPersonName() {
		return this.personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Vendoraddress getVendoraddress() {
		return this.vendoraddress;
	}

	public void setVendoraddress(Vendoraddress vendoraddress) {
		this.vendoraddress = vendoraddress;
	}

}