package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the COUNTER_DAYCHARGEBILL_AY database table.
 * 
 */
@Entity
@Table(name="COUNTER_DAYCHARGEBILL_AY")
@NamedQuery(name="CounterDaychargebillAy.findAll", query="SELECT c FROM CounterDaychargebillAy c")
public class CounterDaychargebillAy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTER_DAYCHARGEBILL_ID")
	private long counterDaychargebillId;

	@Temporal(TemporalType.DATE)
	@Column(name="CHARGEBILL_DATE")
	private Date chargebillDate;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DEPOSIT_AMOUNT")
	private BigDecimal depositAmount;

	@Column(name="HIS_AMOUNT")
	private BigDecimal hisAmount;

	@Column(name="LARGE_AMOUNT")
	private BigDecimal largeAmount;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MIO_AMOUNT")
	private BigDecimal mioAmount;

	@Column(name="OFFICIAL_AMOUNT")
	private BigDecimal officialAmount;

	@Column(name="PAYMENT_CAT")
	private String paymentCat;

	@Column(name="PERSONAL_PAY_AMOUNT")
	private BigDecimal personalPayAmount;

	@Column(name="STARTLINE_AMOUNT")
	private BigDecimal startlineAmount;

	@Column(name="WHOLEFUND_AMOUNT")
	private BigDecimal wholefundAmount;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public CounterDaychargebillAy() {
	}

	public long getCounterDaychargebillId() {
		return this.counterDaychargebillId;
	}

	public void setCounterDaychargebillId(long counterDaychargebillId) {
		this.counterDaychargebillId = counterDaychargebillId;
	}

	public Date getChargebillDate() {
		return this.chargebillDate;
	}

	public void setChargebillDate(Date chargebillDate) {
		this.chargebillDate = chargebillDate;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getDepositAmount() {
		return this.depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	public BigDecimal getHisAmount() {
		return this.hisAmount;
	}

	public void setHisAmount(BigDecimal hisAmount) {
		this.hisAmount = hisAmount;
	}

	public BigDecimal getLargeAmount() {
		return this.largeAmount;
	}

	public void setLargeAmount(BigDecimal largeAmount) {
		this.largeAmount = largeAmount;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMioAmount() {
		return this.mioAmount;
	}

	public void setMioAmount(BigDecimal mioAmount) {
		this.mioAmount = mioAmount;
	}

	public BigDecimal getOfficialAmount() {
		return this.officialAmount;
	}

	public void setOfficialAmount(BigDecimal officialAmount) {
		this.officialAmount = officialAmount;
	}

	public String getPaymentCat() {
		return this.paymentCat;
	}

	public void setPaymentCat(String paymentCat) {
		this.paymentCat = paymentCat;
	}

	public BigDecimal getPersonalPayAmount() {
		return this.personalPayAmount;
	}

	public void setPersonalPayAmount(BigDecimal personalPayAmount) {
		this.personalPayAmount = personalPayAmount;
	}

	public BigDecimal getStartlineAmount() {
		return this.startlineAmount;
	}

	public void setStartlineAmount(BigDecimal startlineAmount) {
		this.startlineAmount = startlineAmount;
	}

	public BigDecimal getWholefundAmount() {
		return this.wholefundAmount;
	}

	public void setWholefundAmount(BigDecimal wholefundAmount) {
		this.wholefundAmount = wholefundAmount;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}