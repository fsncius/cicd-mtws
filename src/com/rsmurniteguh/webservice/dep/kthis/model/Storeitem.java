package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOREITEM database table.
 * 
 */
@Entity
@NamedQuery(name="Storeitem.findAll", query="SELECT s FROM Storeitem s")
public class Storeitem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOREITEM_ID")
	private long storeitemId;

	@Column(name="AVERAGE_COST")
	private BigDecimal averageCost;

	@Column(name="BALANCE_QTY")
	private BigDecimal balanceQty;

	@Column(name="BATCH_CONTROL_ITEM_IND")
	private String batchControlItemInd;

	@Column(name="COSTING_ITEM_IND")
	private String costingItemInd;

	@Column(name="DAYS_FOR_MARKER")
	private BigDecimal daysForMarker;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="EXPENSIVE_ITEM_IND")
	private String expensiveItemInd;

	@Column(name="FAST_MARKER")
	private BigDecimal fastMarker;

	@Column(name="LAST_COST")
	private BigDecimal lastCost;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_ISSUED_DATETIME")
	private Date lastIssuedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_STOCK_COUNT_DATETIME")
	private Date lastStockCountDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="LOCKED_ITEM_IND")
	private String lockedItemInd;

	@Column(name="MANAGEMENT_MODE")
	private String managementMode;

	@Column(name="MAX_DAY")
	private BigDecimal maxDay;

	@Column(name="MAX_QTY")
	private BigDecimal maxQty;

	@Column(name="MAX_REORDER_QTY")
	private BigDecimal maxReorderQty;

	@Column(name="MIN_DAY")
	private BigDecimal minDay;

	@Column(name="MIN_QTY")
	private BigDecimal minQty;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="QTY_ON_ORDER")
	private BigDecimal qtyOnOrder;

	@Column(name="REORDER_POINT")
	private BigDecimal reorderPoint;

	@Column(name="REORDER_QTY")
	private BigDecimal reorderQty;

	@Column(name="RESERVE_QTY")
	private BigDecimal reserveQty;

	@Column(name="SLOW_MARKER")
	private BigDecimal slowMarker;

	@Column(name="STANDARD_COST")
	private BigDecimal standardCost;

	@Column(name="STOCK_ITEM_IND")
	private String stockItemInd;

	@Column(name="STOCKCOUNT_IN_DAYS")
	private BigDecimal stockcountInDays;

	@Column(name="STORE_UOM")
	private String storeUom;

	@Column(name="TRANSACTABLE_ITEM_IND")
	private String transactableItemInd;

	//bi-directional many-to-one association to Materialitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMMSTR_ID")
	private Materialitemmstr materialitemmstr;

	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREMSTR_ID")
	private Storemstr storemstr;

	//bi-directional many-to-one association to Storeitemstock
	@OneToMany(mappedBy="storeitem")
	private List<Storeitemstock> storeitemstocks;

	public Storeitem() {
	}

	public long getStoreitemId() {
		return this.storeitemId;
	}

	public void setStoreitemId(long storeitemId) {
		this.storeitemId = storeitemId;
	}

	public BigDecimal getAverageCost() {
		return this.averageCost;
	}

	public void setAverageCost(BigDecimal averageCost) {
		this.averageCost = averageCost;
	}

	public BigDecimal getBalanceQty() {
		return this.balanceQty;
	}

	public void setBalanceQty(BigDecimal balanceQty) {
		this.balanceQty = balanceQty;
	}

	public String getBatchControlItemInd() {
		return this.batchControlItemInd;
	}

	public void setBatchControlItemInd(String batchControlItemInd) {
		this.batchControlItemInd = batchControlItemInd;
	}

	public String getCostingItemInd() {
		return this.costingItemInd;
	}

	public void setCostingItemInd(String costingItemInd) {
		this.costingItemInd = costingItemInd;
	}

	public BigDecimal getDaysForMarker() {
		return this.daysForMarker;
	}

	public void setDaysForMarker(BigDecimal daysForMarker) {
		this.daysForMarker = daysForMarker;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getExpensiveItemInd() {
		return this.expensiveItemInd;
	}

	public void setExpensiveItemInd(String expensiveItemInd) {
		this.expensiveItemInd = expensiveItemInd;
	}

	public BigDecimal getFastMarker() {
		return this.fastMarker;
	}

	public void setFastMarker(BigDecimal fastMarker) {
		this.fastMarker = fastMarker;
	}

	public BigDecimal getLastCost() {
		return this.lastCost;
	}

	public void setLastCost(BigDecimal lastCost) {
		this.lastCost = lastCost;
	}

	public Date getLastIssuedDatetime() {
		return this.lastIssuedDatetime;
	}

	public void setLastIssuedDatetime(Date lastIssuedDatetime) {
		this.lastIssuedDatetime = lastIssuedDatetime;
	}

	public Date getLastStockCountDatetime() {
		return this.lastStockCountDatetime;
	}

	public void setLastStockCountDatetime(Date lastStockCountDatetime) {
		this.lastStockCountDatetime = lastStockCountDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getLockedItemInd() {
		return this.lockedItemInd;
	}

	public void setLockedItemInd(String lockedItemInd) {
		this.lockedItemInd = lockedItemInd;
	}

	public String getManagementMode() {
		return this.managementMode;
	}

	public void setManagementMode(String managementMode) {
		this.managementMode = managementMode;
	}

	public BigDecimal getMaxDay() {
		return this.maxDay;
	}

	public void setMaxDay(BigDecimal maxDay) {
		this.maxDay = maxDay;
	}

	public BigDecimal getMaxQty() {
		return this.maxQty;
	}

	public void setMaxQty(BigDecimal maxQty) {
		this.maxQty = maxQty;
	}

	public BigDecimal getMaxReorderQty() {
		return this.maxReorderQty;
	}

	public void setMaxReorderQty(BigDecimal maxReorderQty) {
		this.maxReorderQty = maxReorderQty;
	}

	public BigDecimal getMinDay() {
		return this.minDay;
	}

	public void setMinDay(BigDecimal minDay) {
		this.minDay = minDay;
	}

	public BigDecimal getMinQty() {
		return this.minQty;
	}

	public void setMinQty(BigDecimal minQty) {
		this.minQty = minQty;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getQtyOnOrder() {
		return this.qtyOnOrder;
	}

	public void setQtyOnOrder(BigDecimal qtyOnOrder) {
		this.qtyOnOrder = qtyOnOrder;
	}

	public BigDecimal getReorderPoint() {
		return this.reorderPoint;
	}

	public void setReorderPoint(BigDecimal reorderPoint) {
		this.reorderPoint = reorderPoint;
	}

	public BigDecimal getReorderQty() {
		return this.reorderQty;
	}

	public void setReorderQty(BigDecimal reorderQty) {
		this.reorderQty = reorderQty;
	}

	public BigDecimal getReserveQty() {
		return this.reserveQty;
	}

	public void setReserveQty(BigDecimal reserveQty) {
		this.reserveQty = reserveQty;
	}

	public BigDecimal getSlowMarker() {
		return this.slowMarker;
	}

	public void setSlowMarker(BigDecimal slowMarker) {
		this.slowMarker = slowMarker;
	}

	public BigDecimal getStandardCost() {
		return this.standardCost;
	}

	public void setStandardCost(BigDecimal standardCost) {
		this.standardCost = standardCost;
	}

	public String getStockItemInd() {
		return this.stockItemInd;
	}

	public void setStockItemInd(String stockItemInd) {
		this.stockItemInd = stockItemInd;
	}

	public BigDecimal getStockcountInDays() {
		return this.stockcountInDays;
	}

	public void setStockcountInDays(BigDecimal stockcountInDays) {
		this.stockcountInDays = stockcountInDays;
	}

	public String getStoreUom() {
		return this.storeUom;
	}

	public void setStoreUom(String storeUom) {
		this.storeUom = storeUom;
	}

	public String getTransactableItemInd() {
		return this.transactableItemInd;
	}

	public void setTransactableItemInd(String transactableItemInd) {
		this.transactableItemInd = transactableItemInd;
	}

	public Materialitemmstr getMaterialitemmstr() {
		return this.materialitemmstr;
	}

	public void setMaterialitemmstr(Materialitemmstr materialitemmstr) {
		this.materialitemmstr = materialitemmstr;
	}

	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

	public List<Storeitemstock> getStoreitemstocks() {
		return this.storeitemstocks;
	}

	public void setStoreitemstocks(List<Storeitemstock> storeitemstocks) {
		this.storeitemstocks = storeitemstocks;
	}

	public Storeitemstock addStoreitemstock(Storeitemstock storeitemstock) {
		getStoreitemstocks().add(storeitemstock);
		storeitemstock.setStoreitem(this);

		return storeitemstock;
	}

	public Storeitemstock removeStoreitemstock(Storeitemstock storeitemstock) {
		getStoreitemstocks().remove(storeitemstock);
		storeitemstock.setStoreitem(null);

		return storeitemstock;
	}

}