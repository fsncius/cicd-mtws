package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKRECEIPTDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Stockreceiptdetail.findAll", query="SELECT s FROM Stockreceiptdetail s")
public class Stockreceiptdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKRECEIPTDETAIL_ID")
	private long stockreceiptdetailId;

	@Column(name="BASE_UOM")
	private String baseUom;

	@Column(name="BATCH_NO")
	private String batchNo;

	@Column(name="DISC_UNIT")
	private BigDecimal discUnit;

	@Column(name="DISCOUNT_RATE")
	private BigDecimal discountRate;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Temporal(TemporalType.DATE)
	@Column(name="INVOICE_DATETIME")
	private Date invoiceDatetime;

	@Column(name="INVOICE_NO")
	private String invoiceNo;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="MANUFACTURED_DATE")
	private Date manufacturedDate;

	@Column(name="MANUFACTURER_VENDORMSTR_ID")
	private BigDecimal manufacturerVendormstrId;

	@Column(name="MATERIALITEMMSTR_ID")
	private BigDecimal materialitemmstrId;

	@Column(name="MATERIALTXN_ID")
	private BigDecimal materialtxnId;

	@Column(name="NORMAL_PRICE")
	private BigDecimal normalPrice;

	@Column(name="PPN_RATE")
	private BigDecimal ppnRate;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="RECEIPT_DETAIL_STATUS")
	private String receiptDetailStatus;

	@Column(name="RECEIPT_QTY")
	private BigDecimal receiptQty;

	@Column(name="RECEIPT_QTY_OF_BASE_UOM")
	private BigDecimal receiptQtyOfBaseUom;

	@Column(name="RECEIPT_UOM")
	private String receiptUom;

	private String remarks;

	@Column(name="STOCKISSUEDETAIL_ID")
	private BigDecimal stockissuedetailId;

	@Column(name="STOREBINMSTR_ID")
	private BigDecimal storebinmstrId;

	@Column(name="TOTAL_PRICE")
	private BigDecimal totalPrice;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	@Column(name="VENDORMSTR_ID")
	private BigDecimal vendormstrId;

	@Column(name="WHOLESALE_PRICE")
	private BigDecimal wholesalePrice;

	//bi-directional many-to-one association to Stockadjustmentdetail
	@OneToMany(mappedBy="stockreceiptdetail")
	private List<Stockadjustmentdetail> stockadjustmentdetails;

	//bi-directional many-to-one association to Stockissuedetail
	@OneToMany(mappedBy="stockreceiptdetail")
	private List<Stockissuedetail> stockissuedetails;

	//bi-directional many-to-one association to Stockissuereturnreqdetail
	@OneToMany(mappedBy="stockreceiptdetail")
	private List<Stockissuereturnreqdetail> stockissuereturnreqdetails;

	//bi-directional many-to-one association to Stockreceipt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKRECEIPT_ID")
	private Stockreceipt stockreceipt;

	//bi-directional many-to-one association to Stockreceiptdetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKSET_ID")
	private Stockreceiptdetail stockreceiptdetail;

	//bi-directional many-to-one association to Stockreceiptdetail
	@OneToMany(mappedBy="stockreceiptdetail")
	private List<Stockreceiptdetail> stockreceiptdetails;

	public Stockreceiptdetail() {
	}

	public long getStockreceiptdetailId() {
		return this.stockreceiptdetailId;
	}

	public void setStockreceiptdetailId(long stockreceiptdetailId) {
		this.stockreceiptdetailId = stockreceiptdetailId;
	}

	public String getBaseUom() {
		return this.baseUom;
	}

	public void setBaseUom(String baseUom) {
		this.baseUom = baseUom;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public BigDecimal getDiscUnit() {
		return this.discUnit;
	}

	public void setDiscUnit(BigDecimal discUnit) {
		this.discUnit = discUnit;
	}

	public BigDecimal getDiscountRate() {
		return this.discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getInvoiceDatetime() {
		return this.invoiceDatetime;
	}

	public void setInvoiceDatetime(Date invoiceDatetime) {
		this.invoiceDatetime = invoiceDatetime;
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public Date getManufacturedDate() {
		return this.manufacturedDate;
	}

	public void setManufacturedDate(Date manufacturedDate) {
		this.manufacturedDate = manufacturedDate;
	}

	public BigDecimal getManufacturerVendormstrId() {
		return this.manufacturerVendormstrId;
	}

	public void setManufacturerVendormstrId(BigDecimal manufacturerVendormstrId) {
		this.manufacturerVendormstrId = manufacturerVendormstrId;
	}

	public BigDecimal getMaterialitemmstrId() {
		return this.materialitemmstrId;
	}

	public void setMaterialitemmstrId(BigDecimal materialitemmstrId) {
		this.materialitemmstrId = materialitemmstrId;
	}

	public BigDecimal getMaterialtxnId() {
		return this.materialtxnId;
	}

	public void setMaterialtxnId(BigDecimal materialtxnId) {
		this.materialtxnId = materialtxnId;
	}

	public BigDecimal getNormalPrice() {
		return this.normalPrice;
	}

	public void setNormalPrice(BigDecimal normalPrice) {
		this.normalPrice = normalPrice;
	}

	public BigDecimal getPpnRate() {
		return this.ppnRate;
	}

	public void setPpnRate(BigDecimal ppnRate) {
		this.ppnRate = ppnRate;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getReceiptDetailStatus() {
		return this.receiptDetailStatus;
	}

	public void setReceiptDetailStatus(String receiptDetailStatus) {
		this.receiptDetailStatus = receiptDetailStatus;
	}

	public BigDecimal getReceiptQty() {
		return this.receiptQty;
	}

	public void setReceiptQty(BigDecimal receiptQty) {
		this.receiptQty = receiptQty;
	}

	public BigDecimal getReceiptQtyOfBaseUom() {
		return this.receiptQtyOfBaseUom;
	}

	public void setReceiptQtyOfBaseUom(BigDecimal receiptQtyOfBaseUom) {
		this.receiptQtyOfBaseUom = receiptQtyOfBaseUom;
	}

	public String getReceiptUom() {
		return this.receiptUom;
	}

	public void setReceiptUom(String receiptUom) {
		this.receiptUom = receiptUom;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStockissuedetailId() {
		return this.stockissuedetailId;
	}

	public void setStockissuedetailId(BigDecimal stockissuedetailId) {
		this.stockissuedetailId = stockissuedetailId;
	}

	public BigDecimal getStorebinmstrId() {
		return this.storebinmstrId;
	}

	public void setStorebinmstrId(BigDecimal storebinmstrId) {
		this.storebinmstrId = storebinmstrId;
	}

	public BigDecimal getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getVendormstrId() {
		return this.vendormstrId;
	}

	public void setVendormstrId(BigDecimal vendormstrId) {
		this.vendormstrId = vendormstrId;
	}

	public BigDecimal getWholesalePrice() {
		return this.wholesalePrice;
	}

	public void setWholesalePrice(BigDecimal wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}

	public List<Stockadjustmentdetail> getStockadjustmentdetails() {
		return this.stockadjustmentdetails;
	}

	public void setStockadjustmentdetails(List<Stockadjustmentdetail> stockadjustmentdetails) {
		this.stockadjustmentdetails = stockadjustmentdetails;
	}

	public Stockadjustmentdetail addStockadjustmentdetail(Stockadjustmentdetail stockadjustmentdetail) {
		getStockadjustmentdetails().add(stockadjustmentdetail);
		stockadjustmentdetail.setStockreceiptdetail(this);

		return stockadjustmentdetail;
	}

	public Stockadjustmentdetail removeStockadjustmentdetail(Stockadjustmentdetail stockadjustmentdetail) {
		getStockadjustmentdetails().remove(stockadjustmentdetail);
		stockadjustmentdetail.setStockreceiptdetail(null);

		return stockadjustmentdetail;
	}

	public List<Stockissuedetail> getStockissuedetails() {
		return this.stockissuedetails;
	}

	public void setStockissuedetails(List<Stockissuedetail> stockissuedetails) {
		this.stockissuedetails = stockissuedetails;
	}

	public Stockissuedetail addStockissuedetail(Stockissuedetail stockissuedetail) {
		getStockissuedetails().add(stockissuedetail);
		stockissuedetail.setStockreceiptdetail(this);

		return stockissuedetail;
	}

	public Stockissuedetail removeStockissuedetail(Stockissuedetail stockissuedetail) {
		getStockissuedetails().remove(stockissuedetail);
		stockissuedetail.setStockreceiptdetail(null);

		return stockissuedetail;
	}

	public List<Stockissuereturnreqdetail> getStockissuereturnreqdetails() {
		return this.stockissuereturnreqdetails;
	}

	public void setStockissuereturnreqdetails(List<Stockissuereturnreqdetail> stockissuereturnreqdetails) {
		this.stockissuereturnreqdetails = stockissuereturnreqdetails;
	}

	public Stockissuereturnreqdetail addStockissuereturnreqdetail(Stockissuereturnreqdetail stockissuereturnreqdetail) {
		getStockissuereturnreqdetails().add(stockissuereturnreqdetail);
		stockissuereturnreqdetail.setStockreceiptdetail(this);

		return stockissuereturnreqdetail;
	}

	public Stockissuereturnreqdetail removeStockissuereturnreqdetail(Stockissuereturnreqdetail stockissuereturnreqdetail) {
		getStockissuereturnreqdetails().remove(stockissuereturnreqdetail);
		stockissuereturnreqdetail.setStockreceiptdetail(null);

		return stockissuereturnreqdetail;
	}

	public Stockreceipt getStockreceipt() {
		return this.stockreceipt;
	}

	public void setStockreceipt(Stockreceipt stockreceipt) {
		this.stockreceipt = stockreceipt;
	}

	public Stockreceiptdetail getStockreceiptdetail() {
		return this.stockreceiptdetail;
	}

	public void setStockreceiptdetail(Stockreceiptdetail stockreceiptdetail) {
		this.stockreceiptdetail = stockreceiptdetail;
	}

	public List<Stockreceiptdetail> getStockreceiptdetails() {
		return this.stockreceiptdetails;
	}

	public void setStockreceiptdetails(List<Stockreceiptdetail> stockreceiptdetails) {
		this.stockreceiptdetails = stockreceiptdetails;
	}

	public Stockreceiptdetail addStockreceiptdetail(Stockreceiptdetail stockreceiptdetail) {
		getStockreceiptdetails().add(stockreceiptdetail);
		stockreceiptdetail.setStockreceiptdetail(this);

		return stockreceiptdetail;
	}

	public Stockreceiptdetail removeStockreceiptdetail(Stockreceiptdetail stockreceiptdetail) {
		getStockreceiptdetails().remove(stockreceiptdetail);
		stockreceiptdetail.setStockreceiptdetail(null);

		return stockreceiptdetail;
	}

}