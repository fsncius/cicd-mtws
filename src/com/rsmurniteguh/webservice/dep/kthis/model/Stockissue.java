package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKISSUE database table.
 * 
 */
@Entity
@NamedQuery(name="Stockissue.findAll", query="SELECT s FROM Stockissue s")
public class Stockissue implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKISSUE_ID")
	private long stockissueId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="ISSUE_NO")
	private String issueNo;

	@Column(name="ISSUE_STATUS")
	private String issueStatus;

	@Column(name="ISSUE_TO_PERSON_ID")
	private BigDecimal issueToPersonId;

	@Column(name="ISSUE_TO_RCM_ID")
	private BigDecimal issueToRcmId;

	@Column(name="ISSUE_TO_STOREMSTR_ID")
	private BigDecimal issueToStoremstrId;

	@Column(name="ISSUE_TYPE")
	private String issueType;

	@Column(name="ISSUED_BY")
	private BigDecimal issuedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ISSUED_DATETIME")
	private Date issuedDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="STOCKREQUISITION_ID")
	private BigDecimal stockrequisitionId;

	@Column(name="STOREMSTR_ID")
	private BigDecimal storemstrId;

	//bi-directional many-to-one association to Stockissue
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="RETURN_FOR_ISSUE_ID")
	private Stockissue stockissue;

	//bi-directional many-to-one association to Stockissue
	@OneToMany(mappedBy="stockissue")
	private List<Stockissue> stockissues;

	//bi-directional many-to-one association to Stockreceipt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKRECEIPT_ID")
	private Stockreceipt stockreceipt;

	//bi-directional many-to-one association to Stockissuedetail
	@OneToMany(mappedBy="stockissue")
	private List<Stockissuedetail> stockissuedetails;

	public Stockissue() {
	}

	public long getStockissueId() {
		return this.stockissueId;
	}

	public void setStockissueId(long stockissueId) {
		this.stockissueId = stockissueId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getIssueNo() {
		return this.issueNo;
	}

	public void setIssueNo(String issueNo) {
		this.issueNo = issueNo;
	}

	public String getIssueStatus() {
		return this.issueStatus;
	}

	public void setIssueStatus(String issueStatus) {
		this.issueStatus = issueStatus;
	}

	public BigDecimal getIssueToPersonId() {
		return this.issueToPersonId;
	}

	public void setIssueToPersonId(BigDecimal issueToPersonId) {
		this.issueToPersonId = issueToPersonId;
	}

	public BigDecimal getIssueToRcmId() {
		return this.issueToRcmId;
	}

	public void setIssueToRcmId(BigDecimal issueToRcmId) {
		this.issueToRcmId = issueToRcmId;
	}

	public BigDecimal getIssueToStoremstrId() {
		return this.issueToStoremstrId;
	}

	public void setIssueToStoremstrId(BigDecimal issueToStoremstrId) {
		this.issueToStoremstrId = issueToStoremstrId;
	}

	public String getIssueType() {
		return this.issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public BigDecimal getIssuedBy() {
		return this.issuedBy;
	}

	public void setIssuedBy(BigDecimal issuedBy) {
		this.issuedBy = issuedBy;
	}

	public Date getIssuedDatetime() {
		return this.issuedDatetime;
	}

	public void setIssuedDatetime(Date issuedDatetime) {
		this.issuedDatetime = issuedDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStockrequisitionId() {
		return this.stockrequisitionId;
	}

	public void setStockrequisitionId(BigDecimal stockrequisitionId) {
		this.stockrequisitionId = stockrequisitionId;
	}

	public BigDecimal getStoremstrId() {
		return this.storemstrId;
	}

	public void setStoremstrId(BigDecimal storemstrId) {
		this.storemstrId = storemstrId;
	}

	public Stockissue getStockissue() {
		return this.stockissue;
	}

	public void setStockissue(Stockissue stockissue) {
		this.stockissue = stockissue;
	}

	public List<Stockissue> getStockissues() {
		return this.stockissues;
	}

	public void setStockissues(List<Stockissue> stockissues) {
		this.stockissues = stockissues;
	}

	public Stockissue addStockissue(Stockissue stockissue) {
		getStockissues().add(stockissue);
		stockissue.setStockissue(this);

		return stockissue;
	}

	public Stockissue removeStockissue(Stockissue stockissue) {
		getStockissues().remove(stockissue);
		stockissue.setStockissue(null);

		return stockissue;
	}

	public Stockreceipt getStockreceipt() {
		return this.stockreceipt;
	}

	public void setStockreceipt(Stockreceipt stockreceipt) {
		this.stockreceipt = stockreceipt;
	}

	public List<Stockissuedetail> getStockissuedetails() {
		return this.stockissuedetails;
	}

	public void setStockissuedetails(List<Stockissuedetail> stockissuedetails) {
		this.stockissuedetails = stockissuedetails;
	}

	public Stockissuedetail addStockissuedetail(Stockissuedetail stockissuedetail) {
		getStockissuedetails().add(stockissuedetail);
		stockissuedetail.setStockissue(this);

		return stockissuedetail;
	}

	public Stockissuedetail removeStockissuedetail(Stockissuedetail stockissuedetail) {
		getStockissuedetails().remove(stockissuedetail);
		stockissuedetail.setStockissue(null);

		return stockissuedetail;
	}

}