package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the APPOINTMENT database table.
 * 
 */
@Entity
@NamedQuery(name="Appointment.findAll", query="SELECT a FROM Appointment a")
public class Appointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPOINTMENT_ID")
	private long appointmentId;

	@Column(name="APPIONTMENT_STATUS")
	private String appiontmentStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="APPOINTMENT_BEGINDATETIME")
	private Date appointmentBegindatetime;

	@Column(name="APPOINTMENT_DESC")
	private String appointmentDesc;

	@Column(name="APPOINTMENT_DESC_LANG1")
	private String appointmentDescLang1;

	@Temporal(TemporalType.DATE)
	@Column(name="APPOINTMENT_ENDDATETIME")
	private Date appointmentEnddatetime;

	@Column(name="APPOINTMENT_TYPE")
	private String appointmentType;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	private String remarks;

	//bi-directional many-to-one association to Appointmentresourcemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="APPOINTMENTRESOURCEMSTR_ID")
	private Appointmentresourcemstr appointmentresourcemstr;

	//bi-directional many-to-one association to Appointmentstatushistory
	@OneToMany(mappedBy="appointment")
	private List<Appointmentstatushistory> appointmentstatushistories;

	//bi-directional many-to-one association to Regnappointment
	@OneToMany(mappedBy="appointment")
	private List<Regnappointment> regnappointments;

	public Appointment() {
	}

	public long getAppointmentId() {
		return this.appointmentId;
	}

	public void setAppointmentId(long appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getAppiontmentStatus() {
		return this.appiontmentStatus;
	}

	public void setAppiontmentStatus(String appiontmentStatus) {
		this.appiontmentStatus = appiontmentStatus;
	}

	public Date getAppointmentBegindatetime() {
		return this.appointmentBegindatetime;
	}

	public void setAppointmentBegindatetime(Date appointmentBegindatetime) {
		this.appointmentBegindatetime = appointmentBegindatetime;
	}

	public String getAppointmentDesc() {
		return this.appointmentDesc;
	}

	public void setAppointmentDesc(String appointmentDesc) {
		this.appointmentDesc = appointmentDesc;
	}

	public String getAppointmentDescLang1() {
		return this.appointmentDescLang1;
	}

	public void setAppointmentDescLang1(String appointmentDescLang1) {
		this.appointmentDescLang1 = appointmentDescLang1;
	}

	public Date getAppointmentEnddatetime() {
		return this.appointmentEnddatetime;
	}

	public void setAppointmentEnddatetime(Date appointmentEnddatetime) {
		this.appointmentEnddatetime = appointmentEnddatetime;
	}

	public String getAppointmentType() {
		return this.appointmentType;
	}

	public void setAppointmentType(String appointmentType) {
		this.appointmentType = appointmentType;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Appointmentresourcemstr getAppointmentresourcemstr() {
		return this.appointmentresourcemstr;
	}

	public void setAppointmentresourcemstr(Appointmentresourcemstr appointmentresourcemstr) {
		this.appointmentresourcemstr = appointmentresourcemstr;
	}

	public List<Appointmentstatushistory> getAppointmentstatushistories() {
		return this.appointmentstatushistories;
	}

	public void setAppointmentstatushistories(List<Appointmentstatushistory> appointmentstatushistories) {
		this.appointmentstatushistories = appointmentstatushistories;
	}

	public Appointmentstatushistory addAppointmentstatushistory(Appointmentstatushistory appointmentstatushistory) {
		getAppointmentstatushistories().add(appointmentstatushistory);
		appointmentstatushistory.setAppointment(this);

		return appointmentstatushistory;
	}

	public Appointmentstatushistory removeAppointmentstatushistory(Appointmentstatushistory appointmentstatushistory) {
		getAppointmentstatushistories().remove(appointmentstatushistory);
		appointmentstatushistory.setAppointment(null);

		return appointmentstatushistory;
	}

	public List<Regnappointment> getRegnappointments() {
		return this.regnappointments;
	}

	public void setRegnappointments(List<Regnappointment> regnappointments) {
		this.regnappointments = regnappointments;
	}

	public Regnappointment addRegnappointment(Regnappointment regnappointment) {
		getRegnappointments().add(regnappointment);
		regnappointment.setAppointment(this);

		return regnappointment;
	}

	public Regnappointment removeRegnappointment(Regnappointment regnappointment) {
		getRegnappointments().remove(regnappointment);
		regnappointment.setAppointment(null);

		return regnappointment;
	}

}