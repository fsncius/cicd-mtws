package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MATERIALPRICEADJUSTDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Materialpriceadjustdetail.findAll", query="SELECT m FROM Materialpriceadjustdetail m")
public class Materialpriceadjustdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MATERIALPRICEADJUSTDETAIL_ID")
	private long materialpriceadjustdetailId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="QTY_OF_BASE_UOM")
	private BigDecimal qtyOfBaseUom;

	//bi-directional many-to-one association to Materialpriceadjust
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALPRICEADJUST_ID")
	private Materialpriceadjust materialpriceadjust;

	//bi-directional many-to-one association to Storeitemstock
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREITEMSTOCK_ID")
	private Storeitemstock storeitemstock;

	public Materialpriceadjustdetail() {
	}

	public long getMaterialpriceadjustdetailId() {
		return this.materialpriceadjustdetailId;
	}

	public void setMaterialpriceadjustdetailId(long materialpriceadjustdetailId) {
		this.materialpriceadjustdetailId = materialpriceadjustdetailId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getQtyOfBaseUom() {
		return this.qtyOfBaseUom;
	}

	public void setQtyOfBaseUom(BigDecimal qtyOfBaseUom) {
		this.qtyOfBaseUom = qtyOfBaseUom;
	}

	public Materialpriceadjust getMaterialpriceadjust() {
		return this.materialpriceadjust;
	}

	public void setMaterialpriceadjust(Materialpriceadjust materialpriceadjust) {
		this.materialpriceadjust = materialpriceadjust;
	}

	public Storeitemstock getStoreitemstock() {
		return this.storeitemstock;
	}

	public void setStoreitemstock(Storeitemstock storeitemstock) {
		this.storeitemstock = storeitemstock;
	}

}