package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DIAGNOSISCATEGORYMSTR_INA database table.
 * 
 */
@Entity
@Table(name="DIAGNOSISCATEGORYMSTR_INA")
@NamedQuery(name="DiagnosiscategorymstrIna.findAll", query="SELECT d FROM DiagnosiscategorymstrIna d")
public class DiagnosiscategorymstrIna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CODE_SCOPE")
	private String codeScope;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DIAGNOSISCATEGORY_DESC")
	private String diagnosiscategoryDesc;

	@Column(name="DIAGNOSISCATEGORY_SEQUENCE")
	private BigDecimal diagnosiscategorySequence;
	@Id
	@Column(name="DIAGNOSISCATEGORYMSTR_ID")
	private BigDecimal diagnosiscategorymstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PARENT_ID")
	private BigDecimal parentId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="\"VERSION\"")
	private String version;

	public DiagnosiscategorymstrIna() {
	}

	public String getCodeScope() {
		return this.codeScope;
	}

	public void setCodeScope(String codeScope) {
		this.codeScope = codeScope;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDiagnosiscategoryDesc() {
		return this.diagnosiscategoryDesc;
	}

	public void setDiagnosiscategoryDesc(String diagnosiscategoryDesc) {
		this.diagnosiscategoryDesc = diagnosiscategoryDesc;
	}

	public BigDecimal getDiagnosiscategorySequence() {
		return this.diagnosiscategorySequence;
	}

	public void setDiagnosiscategorySequence(BigDecimal diagnosiscategorySequence) {
		this.diagnosiscategorySequence = diagnosiscategorySequence;
	}

	public BigDecimal getDiagnosiscategorymstrId() {
		return this.diagnosiscategorymstrId;
	}

	public void setDiagnosiscategorymstrId(BigDecimal diagnosiscategorymstrId) {
		this.diagnosiscategorymstrId = diagnosiscategorymstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getParentId() {
		return this.parentId;
	}

	public void setParentId(BigDecimal parentId) {
		this.parentId = parentId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}