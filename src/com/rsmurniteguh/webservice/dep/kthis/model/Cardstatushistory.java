package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CARDSTATUSHISTORY database table.
 * 
 */
@Entity
@NamedQuery(name="Cardstatushistory.findAll", query="SELECT c FROM Cardstatushistory c")
public class Cardstatushistory implements Serializable {

	@Id
	@Column(name="CARDSTATUSHISTORY_ID")
	private BigDecimal cardstatushistoryId;

	@Column(name="CARD_NO")
	private String cardNo;

	@Column(name="CARD_TYPE")
	private String cardType;

	@Column(name="COLLECTION_STATUS")
	private String collectionStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="NEW_CARD_STATUS")
	private String newCardStatus;

	@Column(name="OLD_CARD_STATUS")
	private String oldCardStatus;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="STAKEHOLDERACCOUNTTXN_ID")
	private BigDecimal stakeholderaccounttxnId;

	//bi-directional many-to-one association to Card
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CARD_ID")
	private Card card;

	//bi-directional many-to-one association to Counter
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COUNTER_ID")
	private Counter counter;

	private BigDecimal cardId;
	
	private BigDecimal counterId;
	
	public Cardstatushistory() {
	}

	public BigDecimal getCardstatushistoryId() {
		return this.cardstatushistoryId;
	}

	public void setCardstatushistoryId(BigDecimal cardstatushistoryId) {
		this.cardstatushistoryId = cardstatushistoryId;
	}

	public String getCardNo() {
		return this.cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardType() {
		return this.cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCollectionStatus() {
		return this.collectionStatus;
	}

	public void setCollectionStatus(String collectionStatus) {
		this.collectionStatus = collectionStatus;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getNewCardStatus() {
		return this.newCardStatus;
	}

	public void setNewCardStatus(String newCardStatus) {
		this.newCardStatus = newCardStatus;
	}

	public String getOldCardStatus() {
		return this.oldCardStatus;
	}

	public void setOldCardStatus(String oldCardStatus) {
		this.oldCardStatus = oldCardStatus;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getStakeholderaccounttxnId() {
		return this.stakeholderaccounttxnId;
	}

	public void setStakeholderaccounttxnId(BigDecimal stakeholderaccounttxnId) {
		this.stakeholderaccounttxnId = stakeholderaccounttxnId;
	}

	public Card getCard() {
		return this.card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public Counter getCounter() {
		return this.counter;
	}

	public void setCounter(Counter counter) {
		this.counter = counter;
	}

	public BigDecimal getCardId() {
		return cardId;
	}

	public void setCardId(BigDecimal cardId) {
		this.cardId = cardId;
	}

	public BigDecimal getCounterId() {
		return counterId;
	}

	public void setCounterId(BigDecimal counterId) {
		this.counterId = counterId;
	}

}