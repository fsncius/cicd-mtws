package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKISSUERETURNREQ database table.
 * 
 */
@Entity
@NamedQuery(name="Stockissuereturnreq.findAll", query="SELECT s FROM Stockissuereturnreq s")
public class Stockissuereturnreq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKISSUERETURNREQ_ID")
	private long stockissuereturnreqId;

	@Column(name="APPROVED_BY")
	private BigDecimal approvedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="APPROVED_DATETIME")
	private Date approvedDatetime;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="ISSUE_RETURN_REQ_NO")
	private String issueReturnReqNo;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="REQUISITION_STATUS")
	private String requisitionStatus;

	@Column(name="REQUISITION_STOREMSTR_ID")
	private BigDecimal requisitionStoremstrId;

	@Column(name="RETURN_TO_STOREMSTR_ID")
	private BigDecimal returnToStoremstrId;

	//bi-directional many-to-one association to Stockissuereturnreqdetail
	@OneToMany(mappedBy="stockissuereturnreq")
	private List<Stockissuereturnreqdetail> stockissuereturnreqdetails;

	public Stockissuereturnreq() {
	}

	public long getStockissuereturnreqId() {
		return this.stockissuereturnreqId;
	}

	public void setStockissuereturnreqId(long stockissuereturnreqId) {
		this.stockissuereturnreqId = stockissuereturnreqId;
	}

	public BigDecimal getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(BigDecimal approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedDatetime() {
		return this.approvedDatetime;
	}

	public void setApprovedDatetime(Date approvedDatetime) {
		this.approvedDatetime = approvedDatetime;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getIssueReturnReqNo() {
		return this.issueReturnReqNo;
	}

	public void setIssueReturnReqNo(String issueReturnReqNo) {
		this.issueReturnReqNo = issueReturnReqNo;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRequisitionStatus() {
		return this.requisitionStatus;
	}

	public void setRequisitionStatus(String requisitionStatus) {
		this.requisitionStatus = requisitionStatus;
	}

	public BigDecimal getRequisitionStoremstrId() {
		return this.requisitionStoremstrId;
	}

	public void setRequisitionStoremstrId(BigDecimal requisitionStoremstrId) {
		this.requisitionStoremstrId = requisitionStoremstrId;
	}

	public BigDecimal getReturnToStoremstrId() {
		return this.returnToStoremstrId;
	}

	public void setReturnToStoremstrId(BigDecimal returnToStoremstrId) {
		this.returnToStoremstrId = returnToStoremstrId;
	}

	public List<Stockissuereturnreqdetail> getStockissuereturnreqdetails() {
		return this.stockissuereturnreqdetails;
	}

	public void setStockissuereturnreqdetails(List<Stockissuereturnreqdetail> stockissuereturnreqdetails) {
		this.stockissuereturnreqdetails = stockissuereturnreqdetails;
	}

	public Stockissuereturnreqdetail addStockissuereturnreqdetail(Stockissuereturnreqdetail stockissuereturnreqdetail) {
		getStockissuereturnreqdetails().add(stockissuereturnreqdetail);
		stockissuereturnreqdetail.setStockissuereturnreq(this);

		return stockissuereturnreqdetail;
	}

	public Stockissuereturnreqdetail removeStockissuereturnreqdetail(Stockissuereturnreqdetail stockissuereturnreqdetail) {
		getStockissuereturnreqdetails().remove(stockissuereturnreqdetail);
		stockissuereturnreqdetail.setStockissuereturnreq(null);

		return stockissuereturnreqdetail;
	}

}