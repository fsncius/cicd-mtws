package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PATIENTTRACKINGINFOTYPEROLE database table.
 * 
 */
@Entity
@NamedQuery(name="Patienttrackinginfotyperole.findAll", query="SELECT p FROM Patienttrackinginfotyperole p")
public class Patienttrackinginfotyperole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTTRACKINGINFOTYPEROLE_ID")
	private long patienttrackinginfotyperoleId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	//bi-directional many-to-one association to Patienttrackinginfotypemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTTRACKINGINFOTYPEMSTR_ID")
	private Patienttrackinginfotypemstr patienttrackinginfotypemstr;

	//bi-directional many-to-one association to Rolemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ROLEMSTR_ID")
	private Rolemstr rolemstr;

	public Patienttrackinginfotyperole() {
	}

	public long getPatienttrackinginfotyperoleId() {
		return this.patienttrackinginfotyperoleId;
	}

	public void setPatienttrackinginfotyperoleId(long patienttrackinginfotyperoleId) {
		this.patienttrackinginfotyperoleId = patienttrackinginfotyperoleId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public Patienttrackinginfotypemstr getPatienttrackinginfotypemstr() {
		return this.patienttrackinginfotypemstr;
	}

	public void setPatienttrackinginfotypemstr(Patienttrackinginfotypemstr patienttrackinginfotypemstr) {
		this.patienttrackinginfotypemstr = patienttrackinginfotypemstr;
	}

	public Rolemstr getRolemstr() {
		return this.rolemstr;
	}

	public void setRolemstr(Rolemstr rolemstr) {
		this.rolemstr = rolemstr;
	}

}