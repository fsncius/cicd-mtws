package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITDIAGNOSISDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Visitdiagnosisdetail.findAll", query="SELECT v FROM Visitdiagnosisdetail v")
public class Visitdiagnosisdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITDIAGNOSISDETAIL_ID")
	private long visitdiagnosisdetailId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DIAGNOSIS_SEQUENCE")
	private BigDecimal diagnosisSequence;

	@Column(name="DIAGNOSIS_STATUS")
	private String diagnosisStatus;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MRO_DIAGNOSIS_REMARKS")
	private String mroDiagnosisRemarks;

	@Column(name="OUTCOME_STATUS")
	private String outcomeStatus;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="VISIT_DIAGNOSIS_DESC")
	private String visitDiagnosisDesc;

	//bi-directional many-to-one association to Diagnosismstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_DIAGNOSISMSTR_ID")
	private Diagnosismstr diagnosismstr1;

	//bi-directional many-to-one association to Diagnosismstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MRO_DIAGNOSISMSTR_ID")
	private Diagnosismstr diagnosismstr2;

	//bi-directional many-to-one association to Visitdiagnosi
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISITDIAGNOSIS_ID")
	private Visitdiagnosi visitdiagnosi;

	public Visitdiagnosisdetail() {
	}

	public long getVisitdiagnosisdetailId() {
		return this.visitdiagnosisdetailId;
	}

	public void setVisitdiagnosisdetailId(long visitdiagnosisdetailId) {
		this.visitdiagnosisdetailId = visitdiagnosisdetailId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getDiagnosisSequence() {
		return this.diagnosisSequence;
	}

	public void setDiagnosisSequence(BigDecimal diagnosisSequence) {
		this.diagnosisSequence = diagnosisSequence;
	}

	public String getDiagnosisStatus() {
		return this.diagnosisStatus;
	}

	public void setDiagnosisStatus(String diagnosisStatus) {
		this.diagnosisStatus = diagnosisStatus;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMroDiagnosisRemarks() {
		return this.mroDiagnosisRemarks;
	}

	public void setMroDiagnosisRemarks(String mroDiagnosisRemarks) {
		this.mroDiagnosisRemarks = mroDiagnosisRemarks;
	}

	public String getOutcomeStatus() {
		return this.outcomeStatus;
	}

	public void setOutcomeStatus(String outcomeStatus) {
		this.outcomeStatus = outcomeStatus;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getVisitDiagnosisDesc() {
		return this.visitDiagnosisDesc;
	}

	public void setVisitDiagnosisDesc(String visitDiagnosisDesc) {
		this.visitDiagnosisDesc = visitDiagnosisDesc;
	}

	public Diagnosismstr getDiagnosismstr1() {
		return this.diagnosismstr1;
	}

	public void setDiagnosismstr1(Diagnosismstr diagnosismstr1) {
		this.diagnosismstr1 = diagnosismstr1;
	}

	public Diagnosismstr getDiagnosismstr2() {
		return this.diagnosismstr2;
	}

	public void setDiagnosismstr2(Diagnosismstr diagnosismstr2) {
		this.diagnosismstr2 = diagnosismstr2;
	}

	public Visitdiagnosi getVisitdiagnosi() {
		return this.visitdiagnosi;
	}

	public void setVisitdiagnosi(Visitdiagnosi visitdiagnosi) {
		this.visitdiagnosi = visitdiagnosi;
	}

}