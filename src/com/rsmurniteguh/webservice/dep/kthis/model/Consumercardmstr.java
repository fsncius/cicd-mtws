package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CONSUMERCARDMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Consumercardmstr.findAll", query="SELECT c FROM Consumercardmstr c")
public class Consumercardmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CONSUMERCARDMSTR_ID")
	private long consumercardmstrId;

	private BigDecimal balance;

	@Column(name="CHECK_CODE")
	private String checkCode;

	@Column(name="CONSUMERCARD_NO")
	private String consumercardNo;

	@Column(name="CONSUMERCARD_STATUS")
	private String consumercardStatus;

	@Column(name="CONSUMERCARD_TYPE")
	private String consumercardType;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATETIME")
	private Date expiryDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	private String password;

	@Column(name="STAKEHOLDER_ID")
	private BigDecimal stakeholderId;

	//bi-directional many-to-one association to Consumercardstatushistory
	@OneToMany(mappedBy="consumercardmstr")
	private List<Consumercardstatushistory> consumercardstatushistories;

	public Consumercardmstr() {
	}

	public long getConsumercardmstrId() {
		return this.consumercardmstrId;
	}

	public void setConsumercardmstrId(long consumercardmstrId) {
		this.consumercardmstrId = consumercardmstrId;
	}

	public BigDecimal getBalance() {
		return this.balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getCheckCode() {
		return this.checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getConsumercardNo() {
		return this.consumercardNo;
	}

	public void setConsumercardNo(String consumercardNo) {
		this.consumercardNo = consumercardNo;
	}

	public String getConsumercardStatus() {
		return this.consumercardStatus;
	}

	public void setConsumercardStatus(String consumercardStatus) {
		this.consumercardStatus = consumercardStatus;
	}

	public String getConsumercardType() {
		return this.consumercardType;
	}

	public void setConsumercardType(String consumercardType) {
		this.consumercardType = consumercardType;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Date getExpiryDatetime() {
		return this.expiryDatetime;
	}

	public void setExpiryDatetime(Date expiryDatetime) {
		this.expiryDatetime = expiryDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BigDecimal getStakeholderId() {
		return this.stakeholderId;
	}

	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
	}

	public List<Consumercardstatushistory> getConsumercardstatushistories() {
		return this.consumercardstatushistories;
	}

	public void setConsumercardstatushistories(List<Consumercardstatushistory> consumercardstatushistories) {
		this.consumercardstatushistories = consumercardstatushistories;
	}

	public Consumercardstatushistory addConsumercardstatushistory(Consumercardstatushistory consumercardstatushistory) {
		getConsumercardstatushistories().add(consumercardstatushistory);
		consumercardstatushistory.setConsumercardmstr(this);

		return consumercardstatushistory;
	}

	public Consumercardstatushistory removeConsumercardstatushistory(Consumercardstatushistory consumercardstatushistory) {
		getConsumercardstatushistories().remove(consumercardstatushistory);
		consumercardstatushistory.setConsumercardmstr(null);

		return consumercardstatushistory;
	}

}