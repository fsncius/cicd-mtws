package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MATERIALITEMUOM database table.
 * 
 */
@Entity
@NamedQuery(name="Materialitemuom.findAll", query="SELECT m FROM Materialitemuom m")
public class Materialitemuom implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MATERIALITEMUOM_ID")
	private long materialitemuomId;

	@Column(name="BASE_UOM_IND")
	private String baseUomInd;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="ITEM_UOM_CODE")
	private String itemUomCode;

	@Column(name="ITEM_UOM_DESC")
	private String itemUomDesc;

	@Column(name="ITEM_UOM_DESC_LANG1")
	private String itemUomDescLang1;

	@Column(name="ITEM_UOM_DESC_LANG2")
	private String itemUomDescLang2;

	@Column(name="ITEM_UOM_DESC_LANG3")
	private String itemUomDescLang3;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIALITEMMSTR_ID")
	private BigDecimal materialitemmstrId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="QTY_OF_BASE_UOM")
	private BigDecimal qtyOfBaseUom;

	public Materialitemuom() {
	}

	public long getMaterialitemuomId() {
		return this.materialitemuomId;
	}

	public void setMaterialitemuomId(long materialitemuomId) {
		this.materialitemuomId = materialitemuomId;
	}

	public String getBaseUomInd() {
		return this.baseUomInd;
	}

	public void setBaseUomInd(String baseUomInd) {
		this.baseUomInd = baseUomInd;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getItemUomCode() {
		return this.itemUomCode;
	}

	public void setItemUomCode(String itemUomCode) {
		this.itemUomCode = itemUomCode;
	}

	public String getItemUomDesc() {
		return this.itemUomDesc;
	}

	public void setItemUomDesc(String itemUomDesc) {
		this.itemUomDesc = itemUomDesc;
	}

	public String getItemUomDescLang1() {
		return this.itemUomDescLang1;
	}

	public void setItemUomDescLang1(String itemUomDescLang1) {
		this.itemUomDescLang1 = itemUomDescLang1;
	}

	public String getItemUomDescLang2() {
		return this.itemUomDescLang2;
	}

	public void setItemUomDescLang2(String itemUomDescLang2) {
		this.itemUomDescLang2 = itemUomDescLang2;
	}

	public String getItemUomDescLang3() {
		return this.itemUomDescLang3;
	}

	public void setItemUomDescLang3(String itemUomDescLang3) {
		this.itemUomDescLang3 = itemUomDescLang3;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaterialitemmstrId() {
		return this.materialitemmstrId;
	}

	public void setMaterialitemmstrId(BigDecimal materialitemmstrId) {
		this.materialitemmstrId = materialitemmstrId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getQtyOfBaseUom() {
		return this.qtyOfBaseUom;
	}

	public void setQtyOfBaseUom(BigDecimal qtyOfBaseUom) {
		this.qtyOfBaseUom = qtyOfBaseUom;
	}

}