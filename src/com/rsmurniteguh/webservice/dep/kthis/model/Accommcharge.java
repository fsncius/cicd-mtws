package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the ACCOMMCHARGE database table.
 * 
 */
@Entity
@NamedQuery(name="Accommcharge.findAll", query="SELECT a FROM Accommcharge a")
public class Accommcharge implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ACCOMMCHARGE_ID")
	private long accommchargeId;

	@Column(name="APPLY_TO_COMPANION_BED_IND")
	private String applyToCompanionBedInd;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	//bi-directional many-to-one association to Accommmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ACCOMMMSTR_ID")
	private Accommmstr accommmstr;

	//bi-directional many-to-one association to Chargeitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEITEMMSTR_ID")
	private Chargeitemmstr chargeitemmstr;

	public Accommcharge() {
	}

	public long getAccommchargeId() {
		return this.accommchargeId;
	}

	public void setAccommchargeId(long accommchargeId) {
		this.accommchargeId = accommchargeId;
	}

	public String getApplyToCompanionBedInd() {
		return this.applyToCompanionBedInd;
	}

	public void setApplyToCompanionBedInd(String applyToCompanionBedInd) {
		this.applyToCompanionBedInd = applyToCompanionBedInd;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Accommmstr getAccommmstr() {
		return this.accommmstr;
	}

	public void setAccommmstr(Accommmstr accommmstr) {
		this.accommmstr = accommmstr;
	}

	public Chargeitemmstr getChargeitemmstr() {
		return this.chargeitemmstr;
	}

	public void setChargeitemmstr(Chargeitemmstr chargeitemmstr) {
		this.chargeitemmstr = chargeitemmstr;
	}

}