package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the DIAGNOSISMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Diagnosismstr.findAll", query="SELECT d FROM Diagnosismstr d")
public class Diagnosismstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DIAGNOSISMSTR_ID")
	private long diagnosismstrId;

	@Column(name="ALTERNATE_DESC")
	private String alternateDesc;

	@Column(name="ALTERNATE_DESC_LANG1")
	private String alternateDescLang1;

	@Column(name="ALTERNATE_DESC_LANG2")
	private String alternateDescLang2;

	@Column(name="ALTERNATE_DESC_LANG3")
	private String alternateDescLang3;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DIAGNOSIS_CODE")
	private String diagnosisCode;

	@Column(name="DIAGNOSIS_DESC")
	private String diagnosisDesc;

	@Column(name="DIAGNOSIS_DESC_LANG1")
	private String diagnosisDescLang1;

	@Column(name="DIAGNOSIS_DESC_LANG2")
	private String diagnosisDescLang2;

	@Column(name="DIAGNOSIS_DESC_LANG3")
	private String diagnosisDescLang3;

	@Column(name="DIAGNOSIS_EXPLANATION")
	private String diagnosisExplanation;

	@Column(name="DIAGNOSIS_PROPOSAL")
	private String diagnosisProposal;

	@Column(name="DIAGNOSIS_SUB_CODE")
	private String diagnosisSubCode;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="NOTIFIABLE_IND")
	private String notifiableInd;

	@Column(name="OPERATION_IND")
	private String operationInd;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="SURGERY_CLASS")
	private String surgeryClass;

	@Column(name="\"VERSION\"")
	private String version;

	//bi-directional many-to-one association to Diagnosisgroupdetail
	@OneToMany(mappedBy="diagnosismstr")
	private List<Diagnosisgroupdetail> diagnosisgroupdetails;

	//bi-directional many-to-one association to Diagnosisincommonusemstr
	@OneToMany(mappedBy="diagnosismstr")
	private List<Diagnosisincommonusemstr> diagnosisincommonusemstrs;

	//bi-directional many-to-one association to Diagnosiscategorymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DIAGNOSISCATEGORYMSTR_ID")
	private Diagnosiscategorymstr diagnosiscategorymstr;

	//bi-directional many-to-one association to Visitdailysummary
	@OneToMany(mappedBy="diagnosismstr1")
	private List<Visitdailysummary> visitdailysummaries1;

	//bi-directional many-to-one association to Visitdailysummary
	@OneToMany(mappedBy="diagnosismstr2")
	private List<Visitdailysummary> visitdailysummaries2;

	//bi-directional many-to-one association to Visitdailysummary
	@OneToMany(mappedBy="diagnosismstr3")
	private List<Visitdailysummary> visitdailysummaries3;

	//bi-directional many-to-one association to Visitdailysummary
	@OneToMany(mappedBy="diagnosismstr4")
	private List<Visitdailysummary> visitdailysummaries4;

	//bi-directional many-to-one association to Visitdiagnosisdetail
	@OneToMany(mappedBy="diagnosismstr1")
	private List<Visitdiagnosisdetail> visitdiagnosisdetails1;

	//bi-directional many-to-one association to Visitdiagnosisdetail
	@OneToMany(mappedBy="diagnosismstr2")
	private List<Visitdiagnosisdetail> visitdiagnosisdetails2;

	public Diagnosismstr() {
	}

	public long getDiagnosismstrId() {
		return this.diagnosismstrId;
	}

	public void setDiagnosismstrId(long diagnosismstrId) {
		this.diagnosismstrId = diagnosismstrId;
	}

	public String getAlternateDesc() {
		return this.alternateDesc;
	}

	public void setAlternateDesc(String alternateDesc) {
		this.alternateDesc = alternateDesc;
	}

	public String getAlternateDescLang1() {
		return this.alternateDescLang1;
	}

	public void setAlternateDescLang1(String alternateDescLang1) {
		this.alternateDescLang1 = alternateDescLang1;
	}

	public String getAlternateDescLang2() {
		return this.alternateDescLang2;
	}

	public void setAlternateDescLang2(String alternateDescLang2) {
		this.alternateDescLang2 = alternateDescLang2;
	}

	public String getAlternateDescLang3() {
		return this.alternateDescLang3;
	}

	public void setAlternateDescLang3(String alternateDescLang3) {
		this.alternateDescLang3 = alternateDescLang3;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDiagnosisCode() {
		return this.diagnosisCode;
	}

	public void setDiagnosisCode(String diagnosisCode) {
		this.diagnosisCode = diagnosisCode;
	}

	public String getDiagnosisDesc() {
		return this.diagnosisDesc;
	}

	public void setDiagnosisDesc(String diagnosisDesc) {
		this.diagnosisDesc = diagnosisDesc;
	}

	public String getDiagnosisDescLang1() {
		return this.diagnosisDescLang1;
	}

	public void setDiagnosisDescLang1(String diagnosisDescLang1) {
		this.diagnosisDescLang1 = diagnosisDescLang1;
	}

	public String getDiagnosisDescLang2() {
		return this.diagnosisDescLang2;
	}

	public void setDiagnosisDescLang2(String diagnosisDescLang2) {
		this.diagnosisDescLang2 = diagnosisDescLang2;
	}

	public String getDiagnosisDescLang3() {
		return this.diagnosisDescLang3;
	}

	public void setDiagnosisDescLang3(String diagnosisDescLang3) {
		this.diagnosisDescLang3 = diagnosisDescLang3;
	}

	public String getDiagnosisExplanation() {
		return this.diagnosisExplanation;
	}

	public void setDiagnosisExplanation(String diagnosisExplanation) {
		this.diagnosisExplanation = diagnosisExplanation;
	}

	public String getDiagnosisProposal() {
		return this.diagnosisProposal;
	}

	public void setDiagnosisProposal(String diagnosisProposal) {
		this.diagnosisProposal = diagnosisProposal;
	}

	public String getDiagnosisSubCode() {
		return this.diagnosisSubCode;
	}

	public void setDiagnosisSubCode(String diagnosisSubCode) {
		this.diagnosisSubCode = diagnosisSubCode;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getNotifiableInd() {
		return this.notifiableInd;
	}

	public void setNotifiableInd(String notifiableInd) {
		this.notifiableInd = notifiableInd;
	}

	public String getOperationInd() {
		return this.operationInd;
	}

	public void setOperationInd(String operationInd) {
		this.operationInd = operationInd;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getSurgeryClass() {
		return this.surgeryClass;
	}

	public void setSurgeryClass(String surgeryClass) {
		this.surgeryClass = surgeryClass;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<Diagnosisgroupdetail> getDiagnosisgroupdetails() {
		return this.diagnosisgroupdetails;
	}

	public void setDiagnosisgroupdetails(List<Diagnosisgroupdetail> diagnosisgroupdetails) {
		this.diagnosisgroupdetails = diagnosisgroupdetails;
	}

	public Diagnosisgroupdetail addDiagnosisgroupdetail(Diagnosisgroupdetail diagnosisgroupdetail) {
		getDiagnosisgroupdetails().add(diagnosisgroupdetail);
		diagnosisgroupdetail.setDiagnosismstr(this);

		return diagnosisgroupdetail;
	}

	public Diagnosisgroupdetail removeDiagnosisgroupdetail(Diagnosisgroupdetail diagnosisgroupdetail) {
		getDiagnosisgroupdetails().remove(diagnosisgroupdetail);
		diagnosisgroupdetail.setDiagnosismstr(null);

		return diagnosisgroupdetail;
	}

	public List<Diagnosisincommonusemstr> getDiagnosisincommonusemstrs() {
		return this.diagnosisincommonusemstrs;
	}

	public void setDiagnosisincommonusemstrs(List<Diagnosisincommonusemstr> diagnosisincommonusemstrs) {
		this.diagnosisincommonusemstrs = diagnosisincommonusemstrs;
	}

	public Diagnosisincommonusemstr addDiagnosisincommonusemstr(Diagnosisincommonusemstr diagnosisincommonusemstr) {
		getDiagnosisincommonusemstrs().add(diagnosisincommonusemstr);
		diagnosisincommonusemstr.setDiagnosismstr(this);

		return diagnosisincommonusemstr;
	}

	public Diagnosisincommonusemstr removeDiagnosisincommonusemstr(Diagnosisincommonusemstr diagnosisincommonusemstr) {
		getDiagnosisincommonusemstrs().remove(diagnosisincommonusemstr);
		diagnosisincommonusemstr.setDiagnosismstr(null);

		return diagnosisincommonusemstr;
	}

	public Diagnosiscategorymstr getDiagnosiscategorymstr() {
		return this.diagnosiscategorymstr;
	}

	public void setDiagnosiscategorymstr(Diagnosiscategorymstr diagnosiscategorymstr) {
		this.diagnosiscategorymstr = diagnosiscategorymstr;
	}

	public List<Visitdailysummary> getVisitdailysummaries1() {
		return this.visitdailysummaries1;
	}

	public void setVisitdailysummaries1(List<Visitdailysummary> visitdailysummaries1) {
		this.visitdailysummaries1 = visitdailysummaries1;
	}

	public Visitdailysummary addVisitdailysummaries1(Visitdailysummary visitdailysummaries1) {
		getVisitdailysummaries1().add(visitdailysummaries1);
		visitdailysummaries1.setDiagnosismstr1(this);

		return visitdailysummaries1;
	}

	public Visitdailysummary removeVisitdailysummaries1(Visitdailysummary visitdailysummaries1) {
		getVisitdailysummaries1().remove(visitdailysummaries1);
		visitdailysummaries1.setDiagnosismstr1(null);

		return visitdailysummaries1;
	}

	public List<Visitdailysummary> getVisitdailysummaries2() {
		return this.visitdailysummaries2;
	}

	public void setVisitdailysummaries2(List<Visitdailysummary> visitdailysummaries2) {
		this.visitdailysummaries2 = visitdailysummaries2;
	}

	public Visitdailysummary addVisitdailysummaries2(Visitdailysummary visitdailysummaries2) {
		getVisitdailysummaries2().add(visitdailysummaries2);
		visitdailysummaries2.setDiagnosismstr2(this);

		return visitdailysummaries2;
	}

	public Visitdailysummary removeVisitdailysummaries2(Visitdailysummary visitdailysummaries2) {
		getVisitdailysummaries2().remove(visitdailysummaries2);
		visitdailysummaries2.setDiagnosismstr2(null);

		return visitdailysummaries2;
	}

	public List<Visitdailysummary> getVisitdailysummaries3() {
		return this.visitdailysummaries3;
	}

	public void setVisitdailysummaries3(List<Visitdailysummary> visitdailysummaries3) {
		this.visitdailysummaries3 = visitdailysummaries3;
	}

	public Visitdailysummary addVisitdailysummaries3(Visitdailysummary visitdailysummaries3) {
		getVisitdailysummaries3().add(visitdailysummaries3);
		visitdailysummaries3.setDiagnosismstr3(this);

		return visitdailysummaries3;
	}

	public Visitdailysummary removeVisitdailysummaries3(Visitdailysummary visitdailysummaries3) {
		getVisitdailysummaries3().remove(visitdailysummaries3);
		visitdailysummaries3.setDiagnosismstr3(null);

		return visitdailysummaries3;
	}

	public List<Visitdailysummary> getVisitdailysummaries4() {
		return this.visitdailysummaries4;
	}

	public void setVisitdailysummaries4(List<Visitdailysummary> visitdailysummaries4) {
		this.visitdailysummaries4 = visitdailysummaries4;
	}

	public Visitdailysummary addVisitdailysummaries4(Visitdailysummary visitdailysummaries4) {
		getVisitdailysummaries4().add(visitdailysummaries4);
		visitdailysummaries4.setDiagnosismstr4(this);

		return visitdailysummaries4;
	}

	public Visitdailysummary removeVisitdailysummaries4(Visitdailysummary visitdailysummaries4) {
		getVisitdailysummaries4().remove(visitdailysummaries4);
		visitdailysummaries4.setDiagnosismstr4(null);

		return visitdailysummaries4;
	}

	public List<Visitdiagnosisdetail> getVisitdiagnosisdetails1() {
		return this.visitdiagnosisdetails1;
	}

	public void setVisitdiagnosisdetails1(List<Visitdiagnosisdetail> visitdiagnosisdetails1) {
		this.visitdiagnosisdetails1 = visitdiagnosisdetails1;
	}

	public Visitdiagnosisdetail addVisitdiagnosisdetails1(Visitdiagnosisdetail visitdiagnosisdetails1) {
		getVisitdiagnosisdetails1().add(visitdiagnosisdetails1);
		visitdiagnosisdetails1.setDiagnosismstr1(this);

		return visitdiagnosisdetails1;
	}

	public Visitdiagnosisdetail removeVisitdiagnosisdetails1(Visitdiagnosisdetail visitdiagnosisdetails1) {
		getVisitdiagnosisdetails1().remove(visitdiagnosisdetails1);
		visitdiagnosisdetails1.setDiagnosismstr1(null);

		return visitdiagnosisdetails1;
	}

	public List<Visitdiagnosisdetail> getVisitdiagnosisdetails2() {
		return this.visitdiagnosisdetails2;
	}

	public void setVisitdiagnosisdetails2(List<Visitdiagnosisdetail> visitdiagnosisdetails2) {
		this.visitdiagnosisdetails2 = visitdiagnosisdetails2;
	}

	public Visitdiagnosisdetail addVisitdiagnosisdetails2(Visitdiagnosisdetail visitdiagnosisdetails2) {
		getVisitdiagnosisdetails2().add(visitdiagnosisdetails2);
		visitdiagnosisdetails2.setDiagnosismstr2(this);

		return visitdiagnosisdetails2;
	}

	public Visitdiagnosisdetail removeVisitdiagnosisdetails2(Visitdiagnosisdetail visitdiagnosisdetails2) {
		getVisitdiagnosisdetails2().remove(visitdiagnosisdetails2);
		visitdiagnosisdetails2.setDiagnosismstr2(null);

		return visitdiagnosisdetails2;
	}

}