package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKADJUSTMENT database table.
 * 
 */
@Entity
@NamedQuery(name="Stockadjustment.findAll", query="SELECT s FROM Stockadjustment s")
public class Stockadjustment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKADJUSTMENT_ID")
	private long stockadjustmentId;

	@Column(name="ADJUSTMENT_NO")
	private String adjustmentNo;

	@Column(name="ADJUSTMENT_STATUS")
	private String adjustmentStatus;

	@Column(name="ADJUSTMENT_TYPE")
	private String adjustmentType;

	@Column(name="APPROVED_BY")
	private BigDecimal approvedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="APPROVED_DATETIME")
	private Date approvedDatetime;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="STOCKCOUNT_ID")
	private BigDecimal stockcountId;

	@Column(name="STOREMSTR_ID")
	private BigDecimal storemstrId;

	//bi-directional many-to-one association to Stockadjustmentdetail
	@OneToMany(mappedBy="stockadjustment")
	private List<Stockadjustmentdetail> stockadjustmentdetails;

	public Stockadjustment() {
	}

	public long getStockadjustmentId() {
		return this.stockadjustmentId;
	}

	public void setStockadjustmentId(long stockadjustmentId) {
		this.stockadjustmentId = stockadjustmentId;
	}

	public String getAdjustmentNo() {
		return this.adjustmentNo;
	}

	public void setAdjustmentNo(String adjustmentNo) {
		this.adjustmentNo = adjustmentNo;
	}

	public String getAdjustmentStatus() {
		return this.adjustmentStatus;
	}

	public void setAdjustmentStatus(String adjustmentStatus) {
		this.adjustmentStatus = adjustmentStatus;
	}

	public String getAdjustmentType() {
		return this.adjustmentType;
	}

	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}

	public BigDecimal getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(BigDecimal approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedDatetime() {
		return this.approvedDatetime;
	}

	public void setApprovedDatetime(Date approvedDatetime) {
		this.approvedDatetime = approvedDatetime;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStockcountId() {
		return this.stockcountId;
	}

	public void setStockcountId(BigDecimal stockcountId) {
		this.stockcountId = stockcountId;
	}

	public BigDecimal getStoremstrId() {
		return this.storemstrId;
	}

	public void setStoremstrId(BigDecimal storemstrId) {
		this.storemstrId = storemstrId;
	}

	public List<Stockadjustmentdetail> getStockadjustmentdetails() {
		return this.stockadjustmentdetails;
	}

	public void setStockadjustmentdetails(List<Stockadjustmentdetail> stockadjustmentdetails) {
		this.stockadjustmentdetails = stockadjustmentdetails;
	}

	public Stockadjustmentdetail addStockadjustmentdetail(Stockadjustmentdetail stockadjustmentdetail) {
		getStockadjustmentdetails().add(stockadjustmentdetail);
		stockadjustmentdetail.setStockadjustment(this);

		return stockadjustmentdetail;
	}

	public Stockadjustmentdetail removeStockadjustmentdetail(Stockadjustmentdetail stockadjustmentdetail) {
		getStockadjustmentdetails().remove(stockadjustmentdetail);
		stockadjustmentdetail.setStockadjustment(null);

		return stockadjustmentdetail;
	}

}