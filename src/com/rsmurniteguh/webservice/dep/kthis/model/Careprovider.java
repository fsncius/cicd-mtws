package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CAREPROVIDER database table.
 * 
 */
@Entity
@NamedQuery(name="Careprovider.findAll", query="SELECT c FROM Careprovider c")
public class Careprovider implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CAREPROVIDER_ID")
	private long careproviderId;

	@Column(name="ACCOUNT_BALANCE")
	private BigDecimal accountBalance;

	@Column(name="ACCOUNT_NAME")
	private String accountName;

	@Column(name="ACCR_IND")
	private String accrInd;

	@Column(name="ADMIN_FEE_CHARGE_IND")
	private String adminFeeChargeInd;

	@Column(name="BANK_ACCOUNT_NO")
	private String bankAccountNo;

	@Column(name="BANK_NO")
	private String bankNo;

	@Column(name="BRANCH_CODE")
	private String branchCode;

	@Column(name="CAREPROVIDER_CODE")
	private String careproviderCode;

	@Column(name="CAREPROVIDER_TYPE")
	private String careproviderType;

	@Column(name="COLLECTION_TYPE")
	private String collectionType;

	@Column(name="COMMITTEE_1")
	private String committee1;

	@Column(name="COMMITTEE_2")
	private String committee2;

	@Column(name="COMMITTEE_3")
	private String committee3;

	@Column(name="COMMITTEE_4")
	private String committee4;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DOCTOR_CAT")
	private String doctorCat;

	@Column(name="DOCTOR_PAYMENT_MODE")
	private String doctorPaymentMode;

	@Column(name="EMPLOYEE_IND")
	private String employeeInd;

	@Column(name="GLACCOUNTMSTR_ID")
	private BigDecimal glaccountmstrId;

	private String grade;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MCR_NO")
	private String mcrNo;

	@Column(name="MY_MMA_NO")
	private String myMmaNo;

	@Column(name="OTHER_1_SUBSPECIALTYMSTR_ID")
	private BigDecimal other1SubspecialtymstrId;

	@Column(name="OTHER_2_SUBSPECIALTYMSTR_ID")
	private BigDecimal other2SubspecialtymstrId;

	@Column(name="OTHER_3_SUBSPECIALTYMSTR_ID")
	private BigDecimal other3SubspecialtymstrId;

	@Column(name="OTHER_4_SUBSPECIALTYMSTR_ID")
	private BigDecimal other4SubspecialtymstrId;

	@Column(name="OTHER_5_SUBSPECIALTYMSTR_ID")
	private BigDecimal other5SubspecialtymstrId;

	@Column(name="PAYMENT_PERIOD")
	private String paymentPeriod;

	@Column(name="PAYMENT_PROCESS_IND")
	private String paymentProcessInd;

	@Column(name="PRACTICE_TYPE")
	private String practiceType;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PROCESS_ELIGIBILITY_IND")
	private String processEligibilityInd;

	@Temporal(TemporalType.DATE)
	@Column(name="REGISTRATION_DATE")
	private Date registrationDate;

	private String remarks;

	@Column(name="SG_GIRO_ACCOUNT_NAME")
	private String sgGiroAccountName;

	@Column(name="SG_GIRO_ACCOUNT_NO")
	private String sgGiroAccountNo;

	@Column(name="SG_GIRO_BANK_CODE")
	private String sgGiroBankCode;

	@Column(name="SG_GIRO_BRANCH_CODE")
	private String sgGiroBranchCode;

	@Column(name="SG_GIRO_PRE_SIGN_FLAG")
	private String sgGiroPreSignFlag;

	@Column(name="SG_GIRO_RESTRICTED_VIEW_FLAG")
	private String sgGiroRestrictedViewFlag;

	@Column(name="SG_GIRO_SIGN_LIMIT_1")
	private BigDecimal sgGiroSignLimit1;

	@Column(name="SG_GIRO_SIGN_LIMIT_2")
	private BigDecimal sgGiroSignLimit2;

	@Temporal(TemporalType.DATE)
	@Column(name="SG_MSV_ACCR_EFFECTIVE_DATE")
	private Date sgMsvAccrEffectiveDate;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="SPECIALIST_PANELS_1")
	private String specialistPanels1;

	@Column(name="SPECIALIST_PANELS_2")
	private String specialistPanels2;

	@Column(name="SPECIALIST_PANELS_3")
	private String specialistPanels3;

	@Column(name="SPECIALIST_PANELS_4")
	private String specialistPanels4;

	@Column(name="SPECIALIST_PANELS_5")
	private String specialistPanels5;

	@Temporal(TemporalType.DATE)
	@Column(name="SUSPENDED_FROM_DATE")
	private Date suspendedFromDate;

	@Temporal(TemporalType.DATE)
	@Column(name="SUSPENDED_TO_DATE")
	private Date suspendedToDate;

	@Column(name="TAX_IND")
	private String taxInd;

	//bi-directional many-to-one association to Person
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PERSON_ID")
	private Person person;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PRIMARY_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="WARDMSTR_ID")
	private Wardmstr wardmstr;

	//bi-directional many-to-one association to Careproviderprofile
	@OneToMany(mappedBy="careprovider")
	private List<Careproviderprofile> careproviderprofiles;

	//bi-directional many-to-one association to Charge
	@OneToMany(mappedBy="careprovider1")
	private List<Charge> charges1;

	//bi-directional many-to-one association to Charge
	@OneToMany(mappedBy="careprovider2")
	private List<Charge> charges2;

	//bi-directional many-to-one association to Chargerefundrequisition
	@OneToMany(mappedBy="careprovider")
	private List<Chargerefundrequisition> chargerefundrequisitions;

	//bi-directional many-to-one association to Chargerollingplan
	@OneToMany(mappedBy="careprovider1")
	private List<Chargerollingplan> chargerollingplans1;

	//bi-directional many-to-one association to Chargerollingplan
	@OneToMany(mappedBy="careprovider2")
	private List<Chargerollingplan> chargerollingplans2;

	//bi-directional many-to-one association to Consultationrequisition
	@OneToMany(mappedBy="careprovider1")
	private List<Consultationrequisition> consultationrequisitions1;

	//bi-directional many-to-one association to Consultationrequisition
	@OneToMany(mappedBy="careprovider2")
	private List<Consultationrequisition> consultationrequisitions2;

	//bi-directional many-to-one association to Diagnosisincommonusemstr
	@OneToMany(mappedBy="careprovider")
	private List<Diagnosisincommonusemstr> diagnosisincommonusemstrs;

	//bi-directional many-to-one association to Materialconsumption
	@OneToMany(mappedBy="careprovider")
	private List<Materialconsumption> materialconsumptions;

	//bi-directional many-to-one association to Patient
	@OneToMany(mappedBy="careprovider1")
	private List<Patient> patients1;

	//bi-directional many-to-one association to Patient
	@OneToMany(mappedBy="careprovider2")
	private List<Patient> patients2;

	//bi-directional many-to-one association to Regndailysummary
	@OneToMany(mappedBy="careprovider")
	private List<Regndailysummary> regndailysummaries;

	//bi-directional many-to-one association to Subspecialtywardrequest
	@OneToMany(mappedBy="careprovider")
	private List<Subspecialtywardrequest> subspecialtywardrequests;

	//bi-directional many-to-one association to Visitappointment
	@OneToMany(mappedBy="careprovider")
	private List<Visitappointment> visitappointments;

	//bi-directional many-to-one association to Visitcasenote
	@OneToMany(mappedBy="careprovider")
	private List<Visitcasenote> visitcasenotes;

	//bi-directional many-to-one association to Visitdiagnosi
	@OneToMany(mappedBy="careprovider1")
	private List<Visitdiagnosi> visitdiagnosis1;

	//bi-directional many-to-one association to Visitdiagnosi
	@OneToMany(mappedBy="careprovider2")
	private List<Visitdiagnosi> visitdiagnosis2;

	//bi-directional many-to-one association to Visitdoctor
	@OneToMany(mappedBy="careprovider")
	private List<Visitdoctor> visitdoctors;

	//bi-directional many-to-one association to Visitreferral
	@OneToMany(mappedBy="careprovider")
	private List<Visitreferral> visitreferrals;

	//bi-directional many-to-one association to Visitregntype
	@OneToMany(mappedBy="careprovider")
	private List<Visitregntype> visitregntypes;

	public Careprovider() {
	}

	public long getCareproviderId() {
		return this.careproviderId;
	}

	public void setCareproviderId(long careproviderId) {
		this.careproviderId = careproviderId;
	}

	public BigDecimal getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getAccountName() {
		return this.accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccrInd() {
		return this.accrInd;
	}

	public void setAccrInd(String accrInd) {
		this.accrInd = accrInd;
	}

	public String getAdminFeeChargeInd() {
		return this.adminFeeChargeInd;
	}

	public void setAdminFeeChargeInd(String adminFeeChargeInd) {
		this.adminFeeChargeInd = adminFeeChargeInd;
	}

	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankNo() {
		return this.bankNo;
	}

	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}

	public String getBranchCode() {
		return this.branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCareproviderCode() {
		return this.careproviderCode;
	}

	public void setCareproviderCode(String careproviderCode) {
		this.careproviderCode = careproviderCode;
	}

	public String getCareproviderType() {
		return this.careproviderType;
	}

	public void setCareproviderType(String careproviderType) {
		this.careproviderType = careproviderType;
	}

	public String getCollectionType() {
		return this.collectionType;
	}

	public void setCollectionType(String collectionType) {
		this.collectionType = collectionType;
	}

	public String getCommittee1() {
		return this.committee1;
	}

	public void setCommittee1(String committee1) {
		this.committee1 = committee1;
	}

	public String getCommittee2() {
		return this.committee2;
	}

	public void setCommittee2(String committee2) {
		this.committee2 = committee2;
	}

	public String getCommittee3() {
		return this.committee3;
	}

	public void setCommittee3(String committee3) {
		this.committee3 = committee3;
	}

	public String getCommittee4() {
		return this.committee4;
	}

	public void setCommittee4(String committee4) {
		this.committee4 = committee4;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDoctorCat() {
		return this.doctorCat;
	}

	public void setDoctorCat(String doctorCat) {
		this.doctorCat = doctorCat;
	}

	public String getDoctorPaymentMode() {
		return this.doctorPaymentMode;
	}

	public void setDoctorPaymentMode(String doctorPaymentMode) {
		this.doctorPaymentMode = doctorPaymentMode;
	}

	public String getEmployeeInd() {
		return this.employeeInd;
	}

	public void setEmployeeInd(String employeeInd) {
		this.employeeInd = employeeInd;
	}

	public BigDecimal getGlaccountmstrId() {
		return this.glaccountmstrId;
	}

	public void setGlaccountmstrId(BigDecimal glaccountmstrId) {
		this.glaccountmstrId = glaccountmstrId;
	}

	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMcrNo() {
		return this.mcrNo;
	}

	public void setMcrNo(String mcrNo) {
		this.mcrNo = mcrNo;
	}

	public String getMyMmaNo() {
		return this.myMmaNo;
	}

	public void setMyMmaNo(String myMmaNo) {
		this.myMmaNo = myMmaNo;
	}

	public BigDecimal getOther1SubspecialtymstrId() {
		return this.other1SubspecialtymstrId;
	}

	public void setOther1SubspecialtymstrId(BigDecimal other1SubspecialtymstrId) {
		this.other1SubspecialtymstrId = other1SubspecialtymstrId;
	}

	public BigDecimal getOther2SubspecialtymstrId() {
		return this.other2SubspecialtymstrId;
	}

	public void setOther2SubspecialtymstrId(BigDecimal other2SubspecialtymstrId) {
		this.other2SubspecialtymstrId = other2SubspecialtymstrId;
	}

	public BigDecimal getOther3SubspecialtymstrId() {
		return this.other3SubspecialtymstrId;
	}

	public void setOther3SubspecialtymstrId(BigDecimal other3SubspecialtymstrId) {
		this.other3SubspecialtymstrId = other3SubspecialtymstrId;
	}

	public BigDecimal getOther4SubspecialtymstrId() {
		return this.other4SubspecialtymstrId;
	}

	public void setOther4SubspecialtymstrId(BigDecimal other4SubspecialtymstrId) {
		this.other4SubspecialtymstrId = other4SubspecialtymstrId;
	}

	public BigDecimal getOther5SubspecialtymstrId() {
		return this.other5SubspecialtymstrId;
	}

	public void setOther5SubspecialtymstrId(BigDecimal other5SubspecialtymstrId) {
		this.other5SubspecialtymstrId = other5SubspecialtymstrId;
	}

	public String getPaymentPeriod() {
		return this.paymentPeriod;
	}

	public void setPaymentPeriod(String paymentPeriod) {
		this.paymentPeriod = paymentPeriod;
	}

	public String getPaymentProcessInd() {
		return this.paymentProcessInd;
	}

	public void setPaymentProcessInd(String paymentProcessInd) {
		this.paymentProcessInd = paymentProcessInd;
	}

	public String getPracticeType() {
		return this.practiceType;
	}

	public void setPracticeType(String practiceType) {
		this.practiceType = practiceType;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getProcessEligibilityInd() {
		return this.processEligibilityInd;
	}

	public void setProcessEligibilityInd(String processEligibilityInd) {
		this.processEligibilityInd = processEligibilityInd;
	}

	public Date getRegistrationDate() {
		return this.registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSgGiroAccountName() {
		return this.sgGiroAccountName;
	}

	public void setSgGiroAccountName(String sgGiroAccountName) {
		this.sgGiroAccountName = sgGiroAccountName;
	}

	public String getSgGiroAccountNo() {
		return this.sgGiroAccountNo;
	}

	public void setSgGiroAccountNo(String sgGiroAccountNo) {
		this.sgGiroAccountNo = sgGiroAccountNo;
	}

	public String getSgGiroBankCode() {
		return this.sgGiroBankCode;
	}

	public void setSgGiroBankCode(String sgGiroBankCode) {
		this.sgGiroBankCode = sgGiroBankCode;
	}

	public String getSgGiroBranchCode() {
		return this.sgGiroBranchCode;
	}

	public void setSgGiroBranchCode(String sgGiroBranchCode) {
		this.sgGiroBranchCode = sgGiroBranchCode;
	}

	public String getSgGiroPreSignFlag() {
		return this.sgGiroPreSignFlag;
	}

	public void setSgGiroPreSignFlag(String sgGiroPreSignFlag) {
		this.sgGiroPreSignFlag = sgGiroPreSignFlag;
	}

	public String getSgGiroRestrictedViewFlag() {
		return this.sgGiroRestrictedViewFlag;
	}

	public void setSgGiroRestrictedViewFlag(String sgGiroRestrictedViewFlag) {
		this.sgGiroRestrictedViewFlag = sgGiroRestrictedViewFlag;
	}

	public BigDecimal getSgGiroSignLimit1() {
		return this.sgGiroSignLimit1;
	}

	public void setSgGiroSignLimit1(BigDecimal sgGiroSignLimit1) {
		this.sgGiroSignLimit1 = sgGiroSignLimit1;
	}

	public BigDecimal getSgGiroSignLimit2() {
		return this.sgGiroSignLimit2;
	}

	public void setSgGiroSignLimit2(BigDecimal sgGiroSignLimit2) {
		this.sgGiroSignLimit2 = sgGiroSignLimit2;
	}

	public Date getSgMsvAccrEffectiveDate() {
		return this.sgMsvAccrEffectiveDate;
	}

	public void setSgMsvAccrEffectiveDate(Date sgMsvAccrEffectiveDate) {
		this.sgMsvAccrEffectiveDate = sgMsvAccrEffectiveDate;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getSpecialistPanels1() {
		return this.specialistPanels1;
	}

	public void setSpecialistPanels1(String specialistPanels1) {
		this.specialistPanels1 = specialistPanels1;
	}

	public String getSpecialistPanels2() {
		return this.specialistPanels2;
	}

	public void setSpecialistPanels2(String specialistPanels2) {
		this.specialistPanels2 = specialistPanels2;
	}

	public String getSpecialistPanels3() {
		return this.specialistPanels3;
	}

	public void setSpecialistPanels3(String specialistPanels3) {
		this.specialistPanels3 = specialistPanels3;
	}

	public String getSpecialistPanels4() {
		return this.specialistPanels4;
	}

	public void setSpecialistPanels4(String specialistPanels4) {
		this.specialistPanels4 = specialistPanels4;
	}

	public String getSpecialistPanels5() {
		return this.specialistPanels5;
	}

	public void setSpecialistPanels5(String specialistPanels5) {
		this.specialistPanels5 = specialistPanels5;
	}

	public Date getSuspendedFromDate() {
		return this.suspendedFromDate;
	}

	public void setSuspendedFromDate(Date suspendedFromDate) {
		this.suspendedFromDate = suspendedFromDate;
	}

	public Date getSuspendedToDate() {
		return this.suspendedToDate;
	}

	public void setSuspendedToDate(Date suspendedToDate) {
		this.suspendedToDate = suspendedToDate;
	}

	public String getTaxInd() {
		return this.taxInd;
	}

	public void setTaxInd(String taxInd) {
		this.taxInd = taxInd;
	}

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

	public List<Careproviderprofile> getCareproviderprofiles() {
		return this.careproviderprofiles;
	}

	public void setCareproviderprofiles(List<Careproviderprofile> careproviderprofiles) {
		this.careproviderprofiles = careproviderprofiles;
	}

	public Careproviderprofile addCareproviderprofile(Careproviderprofile careproviderprofile) {
		getCareproviderprofiles().add(careproviderprofile);
		careproviderprofile.setCareprovider(this);

		return careproviderprofile;
	}

	public Careproviderprofile removeCareproviderprofile(Careproviderprofile careproviderprofile) {
		getCareproviderprofiles().remove(careproviderprofile);
		careproviderprofile.setCareprovider(null);

		return careproviderprofile;
	}

	public List<Charge> getCharges1() {
		return this.charges1;
	}

	public void setCharges1(List<Charge> charges1) {
		this.charges1 = charges1;
	}

	public Charge addCharges1(Charge charges1) {
		getCharges1().add(charges1);
		charges1.setCareprovider1(this);

		return charges1;
	}

	public Charge removeCharges1(Charge charges1) {
		getCharges1().remove(charges1);
		charges1.setCareprovider1(null);

		return charges1;
	}

	public List<Charge> getCharges2() {
		return this.charges2;
	}

	public void setCharges2(List<Charge> charges2) {
		this.charges2 = charges2;
	}

	public Charge addCharges2(Charge charges2) {
		getCharges2().add(charges2);
		charges2.setCareprovider2(this);

		return charges2;
	}

	public Charge removeCharges2(Charge charges2) {
		getCharges2().remove(charges2);
		charges2.setCareprovider2(null);

		return charges2;
	}

	public List<Chargerefundrequisition> getChargerefundrequisitions() {
		return this.chargerefundrequisitions;
	}

	public void setChargerefundrequisitions(List<Chargerefundrequisition> chargerefundrequisitions) {
		this.chargerefundrequisitions = chargerefundrequisitions;
	}

	public Chargerefundrequisition addChargerefundrequisition(Chargerefundrequisition chargerefundrequisition) {
		getChargerefundrequisitions().add(chargerefundrequisition);
		chargerefundrequisition.setCareprovider(this);

		return chargerefundrequisition;
	}

	public Chargerefundrequisition removeChargerefundrequisition(Chargerefundrequisition chargerefundrequisition) {
		getChargerefundrequisitions().remove(chargerefundrequisition);
		chargerefundrequisition.setCareprovider(null);

		return chargerefundrequisition;
	}

	public List<Chargerollingplan> getChargerollingplans1() {
		return this.chargerollingplans1;
	}

	public void setChargerollingplans1(List<Chargerollingplan> chargerollingplans1) {
		this.chargerollingplans1 = chargerollingplans1;
	}

	public Chargerollingplan addChargerollingplans1(Chargerollingplan chargerollingplans1) {
		getChargerollingplans1().add(chargerollingplans1);
		chargerollingplans1.setCareprovider1(this);

		return chargerollingplans1;
	}

	public Chargerollingplan removeChargerollingplans1(Chargerollingplan chargerollingplans1) {
		getChargerollingplans1().remove(chargerollingplans1);
		chargerollingplans1.setCareprovider1(null);

		return chargerollingplans1;
	}

	public List<Chargerollingplan> getChargerollingplans2() {
		return this.chargerollingplans2;
	}

	public void setChargerollingplans2(List<Chargerollingplan> chargerollingplans2) {
		this.chargerollingplans2 = chargerollingplans2;
	}

	public Chargerollingplan addChargerollingplans2(Chargerollingplan chargerollingplans2) {
		getChargerollingplans2().add(chargerollingplans2);
		chargerollingplans2.setCareprovider2(this);

		return chargerollingplans2;
	}

	public Chargerollingplan removeChargerollingplans2(Chargerollingplan chargerollingplans2) {
		getChargerollingplans2().remove(chargerollingplans2);
		chargerollingplans2.setCareprovider2(null);

		return chargerollingplans2;
	}

	public List<Consultationrequisition> getConsultationrequisitions1() {
		return this.consultationrequisitions1;
	}

	public void setConsultationrequisitions1(List<Consultationrequisition> consultationrequisitions1) {
		this.consultationrequisitions1 = consultationrequisitions1;
	}

	public Consultationrequisition addConsultationrequisitions1(Consultationrequisition consultationrequisitions1) {
		getConsultationrequisitions1().add(consultationrequisitions1);
		consultationrequisitions1.setCareprovider1(this);

		return consultationrequisitions1;
	}

	public Consultationrequisition removeConsultationrequisitions1(Consultationrequisition consultationrequisitions1) {
		getConsultationrequisitions1().remove(consultationrequisitions1);
		consultationrequisitions1.setCareprovider1(null);

		return consultationrequisitions1;
	}

	public List<Consultationrequisition> getConsultationrequisitions2() {
		return this.consultationrequisitions2;
	}

	public void setConsultationrequisitions2(List<Consultationrequisition> consultationrequisitions2) {
		this.consultationrequisitions2 = consultationrequisitions2;
	}

	public Consultationrequisition addConsultationrequisitions2(Consultationrequisition consultationrequisitions2) {
		getConsultationrequisitions2().add(consultationrequisitions2);
		consultationrequisitions2.setCareprovider2(this);

		return consultationrequisitions2;
	}

	public Consultationrequisition removeConsultationrequisitions2(Consultationrequisition consultationrequisitions2) {
		getConsultationrequisitions2().remove(consultationrequisitions2);
		consultationrequisitions2.setCareprovider2(null);

		return consultationrequisitions2;
	}

	public List<Diagnosisincommonusemstr> getDiagnosisincommonusemstrs() {
		return this.diagnosisincommonusemstrs;
	}

	public void setDiagnosisincommonusemstrs(List<Diagnosisincommonusemstr> diagnosisincommonusemstrs) {
		this.diagnosisincommonusemstrs = diagnosisincommonusemstrs;
	}

	public Diagnosisincommonusemstr addDiagnosisincommonusemstr(Diagnosisincommonusemstr diagnosisincommonusemstr) {
		getDiagnosisincommonusemstrs().add(diagnosisincommonusemstr);
		diagnosisincommonusemstr.setCareprovider(this);

		return diagnosisincommonusemstr;
	}

	public Diagnosisincommonusemstr removeDiagnosisincommonusemstr(Diagnosisincommonusemstr diagnosisincommonusemstr) {
		getDiagnosisincommonusemstrs().remove(diagnosisincommonusemstr);
		diagnosisincommonusemstr.setCareprovider(null);

		return diagnosisincommonusemstr;
	}

	public List<Materialconsumption> getMaterialconsumptions() {
		return this.materialconsumptions;
	}

	public void setMaterialconsumptions(List<Materialconsumption> materialconsumptions) {
		this.materialconsumptions = materialconsumptions;
	}

	public Materialconsumption addMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().add(materialconsumption);
		materialconsumption.setCareprovider(this);

		return materialconsumption;
	}

	public Materialconsumption removeMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().remove(materialconsumption);
		materialconsumption.setCareprovider(null);

		return materialconsumption;
	}

	public List<Patient> getPatients1() {
		return this.patients1;
	}

	public void setPatients1(List<Patient> patients1) {
		this.patients1 = patients1;
	}

	public Patient addPatients1(Patient patients1) {
		getPatients1().add(patients1);
		patients1.setCareprovider1(this);

		return patients1;
	}

	public Patient removePatients1(Patient patients1) {
		getPatients1().remove(patients1);
		patients1.setCareprovider1(null);

		return patients1;
	}

	public List<Patient> getPatients2() {
		return this.patients2;
	}

	public void setPatients2(List<Patient> patients2) {
		this.patients2 = patients2;
	}

	public Patient addPatients2(Patient patients2) {
		getPatients2().add(patients2);
		patients2.setCareprovider2(this);

		return patients2;
	}

	public Patient removePatients2(Patient patients2) {
		getPatients2().remove(patients2);
		patients2.setCareprovider2(null);

		return patients2;
	}

	public List<Regndailysummary> getRegndailysummaries() {
		return this.regndailysummaries;
	}

	public void setRegndailysummaries(List<Regndailysummary> regndailysummaries) {
		this.regndailysummaries = regndailysummaries;
	}

	public Regndailysummary addRegndailysummary(Regndailysummary regndailysummary) {
		getRegndailysummaries().add(regndailysummary);
		regndailysummary.setCareprovider(this);

		return regndailysummary;
	}

	public Regndailysummary removeRegndailysummary(Regndailysummary regndailysummary) {
		getRegndailysummaries().remove(regndailysummary);
		regndailysummary.setCareprovider(null);

		return regndailysummary;
	}

	public List<Subspecialtywardrequest> getSubspecialtywardrequests() {
		return this.subspecialtywardrequests;
	}

	public void setSubspecialtywardrequests(List<Subspecialtywardrequest> subspecialtywardrequests) {
		this.subspecialtywardrequests = subspecialtywardrequests;
	}

	public Subspecialtywardrequest addSubspecialtywardrequest(Subspecialtywardrequest subspecialtywardrequest) {
		getSubspecialtywardrequests().add(subspecialtywardrequest);
		subspecialtywardrequest.setCareprovider(this);

		return subspecialtywardrequest;
	}

	public Subspecialtywardrequest removeSubspecialtywardrequest(Subspecialtywardrequest subspecialtywardrequest) {
		getSubspecialtywardrequests().remove(subspecialtywardrequest);
		subspecialtywardrequest.setCareprovider(null);

		return subspecialtywardrequest;
	}

	public List<Visitappointment> getVisitappointments() {
		return this.visitappointments;
	}

	public void setVisitappointments(List<Visitappointment> visitappointments) {
		this.visitappointments = visitappointments;
	}

	public Visitappointment addVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().add(visitappointment);
		visitappointment.setCareprovider(this);

		return visitappointment;
	}

	public Visitappointment removeVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().remove(visitappointment);
		visitappointment.setCareprovider(null);

		return visitappointment;
	}

	public List<Visitcasenote> getVisitcasenotes() {
		return this.visitcasenotes;
	}

	public void setVisitcasenotes(List<Visitcasenote> visitcasenotes) {
		this.visitcasenotes = visitcasenotes;
	}

	public Visitcasenote addVisitcasenote(Visitcasenote visitcasenote) {
		getVisitcasenotes().add(visitcasenote);
		visitcasenote.setCareprovider(this);

		return visitcasenote;
	}

	public Visitcasenote removeVisitcasenote(Visitcasenote visitcasenote) {
		getVisitcasenotes().remove(visitcasenote);
		visitcasenote.setCareprovider(null);

		return visitcasenote;
	}

	public List<Visitdiagnosi> getVisitdiagnosis1() {
		return this.visitdiagnosis1;
	}

	public void setVisitdiagnosis1(List<Visitdiagnosi> visitdiagnosis1) {
		this.visitdiagnosis1 = visitdiagnosis1;
	}

	public Visitdiagnosi addVisitdiagnosis1(Visitdiagnosi visitdiagnosis1) {
		getVisitdiagnosis1().add(visitdiagnosis1);
		visitdiagnosis1.setCareprovider1(this);

		return visitdiagnosis1;
	}

	public Visitdiagnosi removeVisitdiagnosis1(Visitdiagnosi visitdiagnosis1) {
		getVisitdiagnosis1().remove(visitdiagnosis1);
		visitdiagnosis1.setCareprovider1(null);

		return visitdiagnosis1;
	}

	public List<Visitdiagnosi> getVisitdiagnosis2() {
		return this.visitdiagnosis2;
	}

	public void setVisitdiagnosis2(List<Visitdiagnosi> visitdiagnosis2) {
		this.visitdiagnosis2 = visitdiagnosis2;
	}

	public Visitdiagnosi addVisitdiagnosis2(Visitdiagnosi visitdiagnosis2) {
		getVisitdiagnosis2().add(visitdiagnosis2);
		visitdiagnosis2.setCareprovider2(this);

		return visitdiagnosis2;
	}

	public Visitdiagnosi removeVisitdiagnosis2(Visitdiagnosi visitdiagnosis2) {
		getVisitdiagnosis2().remove(visitdiagnosis2);
		visitdiagnosis2.setCareprovider2(null);

		return visitdiagnosis2;
	}

	public List<Visitdoctor> getVisitdoctors() {
		return this.visitdoctors;
	}

	public void setVisitdoctors(List<Visitdoctor> visitdoctors) {
		this.visitdoctors = visitdoctors;
	}

//	public Visitdoctor addVisitdoctor(Visitdoctor visitdoctor) {
//		getVisitdoctors().add(visitdoctor);
//		visitdoctor.setCareprovider(this);
//
//		return visitdoctor;
//	}
//
//	public Visitdoctor removeVisitdoctor(Visitdoctor visitdoctor) {
//		getVisitdoctors().remove(visitdoctor);
//		visitdoctor.setCareprovider(null);
//
//		return visitdoctor;
//	}

	public List<Visitreferral> getVisitreferrals() {
		return this.visitreferrals;
	}

	public void setVisitreferrals(List<Visitreferral> visitreferrals) {
		this.visitreferrals = visitreferrals;
	}

	public Visitreferral addVisitreferral(Visitreferral visitreferral) {
		getVisitreferrals().add(visitreferral);
		visitreferral.setCareprovider(this);

		return visitreferral;
	}

	public Visitreferral removeVisitreferral(Visitreferral visitreferral) {
		getVisitreferrals().remove(visitreferral);
		visitreferral.setCareprovider(null);

		return visitreferral;
	}

	public List<Visitregntype> getVisitregntypes() {
		return this.visitregntypes;
	}

	public void setVisitregntypes(List<Visitregntype> visitregntypes) {
		this.visitregntypes = visitregntypes;
	}

	public Visitregntype addVisitregntype(Visitregntype visitregntype) {
		getVisitregntypes().add(visitregntype);
		visitregntype.setCareprovider(this);

		return visitregntype;
	}

	public Visitregntype removeVisitregntype(Visitregntype visitregntype) {
		getVisitregntypes().remove(visitregntype);
		visitregntype.setCareprovider(null);

		return visitregntype;
	}

}