package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITPICTURE database table.
 * 
 */
@Entity
@NamedQuery(name="Visitpicture.findAll", query="SELECT v FROM Visitpicture v")
public class Visitpicture implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITPICTURE_ID")
	private long visitpictureId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="FILEINFO_ID")
	private BigDecimal fileinfoId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PICTURE_REMARK")
	private String pictureRemark;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="UPLOAD_DATETIME")
	private Date uploadDatetime;

	@Column(name="UPLOAD_USER_ID")
	private BigDecimal uploadUserId;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public Visitpicture() {
	}

	public long getVisitpictureId() {
		return this.visitpictureId;
	}

	public void setVisitpictureId(long visitpictureId) {
		this.visitpictureId = visitpictureId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getFileinfoId() {
		return this.fileinfoId;
	}

	public void setFileinfoId(BigDecimal fileinfoId) {
		this.fileinfoId = fileinfoId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPictureRemark() {
		return this.pictureRemark;
	}

	public void setPictureRemark(String pictureRemark) {
		this.pictureRemark = pictureRemark;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Date getUploadDatetime() {
		return this.uploadDatetime;
	}

	public void setUploadDatetime(Date uploadDatetime) {
		this.uploadDatetime = uploadDatetime;
	}

	public BigDecimal getUploadUserId() {
		return this.uploadUserId;
	}

	public void setUploadUserId(BigDecimal uploadUserId) {
		this.uploadUserId = uploadUserId;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}