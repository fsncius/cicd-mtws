package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STORECOUNTDEFINE database table.
 * 
 */
@Entity
@NamedQuery(name="Storecountdefine.findAll", query="SELECT s FROM Storecountdefine s")
public class Storecountdefine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STORECOUNTDEFINE_ID")
	private long storecountdefineId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="STORECOUNTDEFINE_DESC")
	private String storecountdefineDesc;

	@Column(name="STORECOUNTDEFINE_DESC_LANG1")
	private String storecountdefineDescLang1;

	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREMSTR_ID")
	private Storemstr storemstr;

	//bi-directional many-to-one association to Storecountdefinedetail
	@OneToMany(mappedBy="storecountdefine")
	private List<Storecountdefinedetail> storecountdefinedetails;

	public Storecountdefine() {
	}

	public long getStorecountdefineId() {
		return this.storecountdefineId;
	}

	public void setStorecountdefineId(long storecountdefineId) {
		this.storecountdefineId = storecountdefineId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStorecountdefineDesc() {
		return this.storecountdefineDesc;
	}

	public void setStorecountdefineDesc(String storecountdefineDesc) {
		this.storecountdefineDesc = storecountdefineDesc;
	}

	public String getStorecountdefineDescLang1() {
		return this.storecountdefineDescLang1;
	}

	public void setStorecountdefineDescLang1(String storecountdefineDescLang1) {
		this.storecountdefineDescLang1 = storecountdefineDescLang1;
	}

	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

	public List<Storecountdefinedetail> getStorecountdefinedetails() {
		return this.storecountdefinedetails;
	}

	public void setStorecountdefinedetails(List<Storecountdefinedetail> storecountdefinedetails) {
		this.storecountdefinedetails = storecountdefinedetails;
	}

	public Storecountdefinedetail addStorecountdefinedetail(Storecountdefinedetail storecountdefinedetail) {
		getStorecountdefinedetails().add(storecountdefinedetail);
		storecountdefinedetail.setStorecountdefine(this);

		return storecountdefinedetail;
	}

	public Storecountdefinedetail removeStorecountdefinedetail(Storecountdefinedetail storecountdefinedetail) {
		getStorecountdefinedetails().remove(storecountdefinedetail);
		storecountdefinedetail.setStorecountdefine(null);

		return storecountdefinedetail;
	}

}