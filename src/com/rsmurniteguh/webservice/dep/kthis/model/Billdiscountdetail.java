package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the BILLDISCOUNTDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Billdiscountdetail.findAll", query="SELECT b FROM Billdiscountdetail b")
public class Billdiscountdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BILLDISCOUNTDETAIL_ID")
	private long billdiscountdetailId;

	@Column(name="DISCOUNT_AMOUNT")
	private BigDecimal discountAmount;

	@Column(name="ORDERENTRYITEM_ID")
	private BigDecimal orderentryitemId;

	//bi-directional many-to-one association to Billdiscount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BILLDISCOUNT_ID")
	private Billdiscount billdiscount;

	//bi-directional many-to-one association to Patientaccounttxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNTTXN_ID")
	private Patientaccounttxn patientaccounttxn;

	public Billdiscountdetail() {
	}

	public long getBilldiscountdetailId() {
		return this.billdiscountdetailId;
	}

	public void setBilldiscountdetailId(long billdiscountdetailId) {
		this.billdiscountdetailId = billdiscountdetailId;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getOrderentryitemId() {
		return this.orderentryitemId;
	}

	public void setOrderentryitemId(BigDecimal orderentryitemId) {
		this.orderentryitemId = orderentryitemId;
	}

	public Billdiscount getBilldiscount() {
		return this.billdiscount;
	}

	public void setBilldiscount(Billdiscount billdiscount) {
		this.billdiscount = billdiscount;
	}

	public Patientaccounttxn getPatientaccounttxn() {
		return this.patientaccounttxn;
	}

	public void setPatientaccounttxn(Patientaccounttxn patientaccounttxn) {
		this.patientaccounttxn = patientaccounttxn;
	}

}