package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the MATERIALITEMMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Materialitemmstr.findAll", query="SELECT m FROM Materialitemmstr m")
public class Materialitemmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MATERIALITEMMSTR_ID")
	private long materialitemmstrId;

	@Column(name="ALARM_DAY")
	private BigDecimal alarmDay;

	@Column(name="ATTACHMENT_IND")
	private String attachmentInd;

	@Column(name="AVERAGE_COST")
	private BigDecimal averageCost;

	@Column(name="BASE_UOM")
	private String baseUom;

	@Column(name="BATCH_CONTROL_ITEM_IND")
	private String batchControlItemInd;

	@Column(name="BID_ITEM_IND")
	private String bidItemInd;

	@Column(name="BID_PRICE")
	private BigDecimal bidPrice;

	@Column(name="BID_UOM")
	private String bidUom;

	@Column(name="BIN_MANAGEMENT_IND")
	private String binManagementInd;

	@Column(name="BLANK_ORDER_ITEM_IND")
	private String blankOrderItemInd;

	@Column(name="BPJS_IND")
	private String bpjsInd;

	@Column(name="BRAND_NAME")
	private String brandName;

	@Column(name="BRAND_NAME_LANG1")
	private String brandNameLang1;

	@Column(name="BRAND_NAME_LANG2")
	private String brandNameLang2;

	@Column(name="BRAND_NAME_LANG3")
	private String brandNameLang3;

	@Column(name="BULK_ITEM_IND")
	private String bulkItemInd;

	@Column(name="CHARGE_IND")
	private String chargeInd;

	@Column(name="CONCESSIONAIRE_ITEM_IND")
	private String concessionaireItemInd;

	@Column(name="CONSIGNMENT_ITEM_IND")
	private String consignmentItemInd;

	@Column(name="COSTING_ITEM_IND")
	private String costingItemInd;

	@Column(name="DAYS_FOR_MARKER")
	private BigDecimal daysForMarker;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="EFFECTIVE_MONTHS")
	private BigDecimal effectiveMonths;

	@Column(name="EXPENSIVE_ITEM_IND")
	private String expensiveItemInd;

	@Column(name="FAST_MARKER")
	private BigDecimal fastMarker;

	@Column(name="HIGH_VALUE_SUPPLIES_IND")
	private String highValueSuppliesInd;

	@Column(name="IMPORT_ITEM_IND")
	private String importItemInd;

	@Column(name="ISSUE_ONLY_ITEM_IND")
	private String issueOnlyItemInd;

	@Column(name="LABEL_UOM")
	private String labelUom;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="LATEST_COST")
	private BigDecimal latestCost;

	@Column(name="LOCKED_ITEM_IND")
	private String lockedItemInd;

	@Column(name="LOSS_RATE")
	private BigDecimal lossRate;

	@Column(name="MANUFACTURE_IND")
	private String manufactureInd;

	@Column(name="MANUFACTURER_VENDORMSTR_ID")
	private BigDecimal manufacturerVendormstrId;

	@Column(name="MATERIAL_ITEM_CODE")
	private String materialItemCode;

	@Column(name="MATERIAL_ITEM_MANAGE_TYPE")
	private String materialItemManageType;

	@Column(name="MATERIAL_ITEM_MODEL")
	private String materialItemModel;

	@Column(name="MATERIAL_ITEM_NAME")
	private String materialItemName;

	@Column(name="MATERIAL_ITEM_NAME_LANG1")
	private String materialItemNameLang1;

	@Column(name="MATERIAL_ITEM_NAME_LANG2")
	private String materialItemNameLang2;

	@Column(name="MATERIAL_ITEM_NAME_LANG3")
	private String materialItemNameLang3;

	@Column(name="MATERIAL_ITEM_ORIGIN")
	private String materialItemOrigin;

	@Column(name="MATERIAL_ITEM_TYPE")
	private String materialItemType;

	@Column(name="MAX_QTY_PER_LABEL")
	private BigDecimal maxQtyPerLabel;

	@Column(name="ONE_TIME_PURCHASE_ITEM_IND")
	private String oneTimePurchaseItemInd;

	@Column(name="OTHER_CODE")
	private String otherCode;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PRICE_IND")
	private String priceInd;

	@Column(name="PURCHASE_IND")
	private String purchaseInd;

	@Column(name="QTY_ROUNDING_RULE")
	private String qtyRoundingRule;

	@Column(name="RECYCLED_ITEM_IND")
	private String recycledItemInd;

	@Column(name="RENTAL_ITEM_IND")
	private String rentalItemInd;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="SLOW_MARKER")
	private BigDecimal slowMarker;

	private String spec;

	@Column(name="SPEC_LANG1")
	private String specLang1;

	@Column(name="STANDARD_COST")
	private BigDecimal standardCost;

	@Column(name="STOCK_ITEM_IND")
	private String stockItemInd;

	@Column(name="TOTAL_COST")
	private BigDecimal totalCost;

	@Column(name="TOTAL_QTY")
	private BigDecimal totalQty;

	@Column(name="TRACK_IND")
	private String trackInd;

	@Column(name="TRANSACTABLE_ITEM_IND")
	private String transactableItemInd;

	@Column(name="TXNCODEMSTR_ID")
	private BigDecimal txncodemstrId;

	@Column(name="USE_BEFORE_RECEIPTED_IND")
	private String useBeforeReceiptedInd;

	@Column(name="WHOLESALE_PRICE")
	private BigDecimal wholesalePrice;

	//bi-directional many-to-one association to Chargematerialcompdetail
	@OneToMany(mappedBy="materialitemmstr")
	private List<Chargematerialcompdetail> chargematerialcompdetails;

	//bi-directional many-to-one association to Materialconsumption
	@OneToMany(mappedBy="materialitemmstr")
	private List<Materialconsumption> materialconsumptions;

	//bi-directional many-to-one association to Materialitemcatmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMCATMSTR_ID")
	private Materialitemcatmstr materialitemcatmstr;

	//bi-directional many-to-one association to Materialprice
	@OneToMany(mappedBy="materialitemmstr")
	private List<Materialprice> materialprices;

	//bi-directional many-to-one association to Materialpriceadjust
	@OneToMany(mappedBy="materialitemmstr")
	private List<Materialpriceadjust> materialpriceadjusts;

	//bi-directional many-to-one association to Materialtxn
	@OneToMany(mappedBy="materialitemmstr")
	private List<Materialtxn> materialtxns;



	//bi-directional many-to-one association to Storecountdefinedetail
	@OneToMany(mappedBy="materialitemmstr")
	private List<Storecountdefinedetail> storecountdefinedetails;

	//bi-directional many-to-one association to Storeitem
	@OneToMany(mappedBy="materialitemmstr")
	private List<Storeitem> storeitems;

	//bi-directional many-to-one association to Vendoritem
	@OneToMany(mappedBy="materialitemmstr")
	private List<Vendoritem> vendoritems;

	//bi-directional many-to-one association to Wardsspmstocktxn
	@OneToMany(mappedBy="materialitemmstr")
	private List<Wardsspmstocktxn> wardsspmstocktxns;

	public Materialitemmstr() {
	}

	public long getMaterialitemmstrId() {
		return this.materialitemmstrId;
	}

	public void setMaterialitemmstrId(long materialitemmstrId) {
		this.materialitemmstrId = materialitemmstrId;
	}

	public BigDecimal getAlarmDay() {
		return this.alarmDay;
	}

	public void setAlarmDay(BigDecimal alarmDay) {
		this.alarmDay = alarmDay;
	}

	public String getAttachmentInd() {
		return this.attachmentInd;
	}

	public void setAttachmentInd(String attachmentInd) {
		this.attachmentInd = attachmentInd;
	}

	public BigDecimal getAverageCost() {
		return this.averageCost;
	}

	public void setAverageCost(BigDecimal averageCost) {
		this.averageCost = averageCost;
	}

	public String getBaseUom() {
		return this.baseUom;
	}

	public void setBaseUom(String baseUom) {
		this.baseUom = baseUom;
	}

	public String getBatchControlItemInd() {
		return this.batchControlItemInd;
	}

	public void setBatchControlItemInd(String batchControlItemInd) {
		this.batchControlItemInd = batchControlItemInd;
	}

	public String getBidItemInd() {
		return this.bidItemInd;
	}

	public void setBidItemInd(String bidItemInd) {
		this.bidItemInd = bidItemInd;
	}

	public BigDecimal getBidPrice() {
		return this.bidPrice;
	}

	public void setBidPrice(BigDecimal bidPrice) {
		this.bidPrice = bidPrice;
	}

	public String getBidUom() {
		return this.bidUom;
	}

	public void setBidUom(String bidUom) {
		this.bidUom = bidUom;
	}

	public String getBinManagementInd() {
		return this.binManagementInd;
	}

	public void setBinManagementInd(String binManagementInd) {
		this.binManagementInd = binManagementInd;
	}

	public String getBlankOrderItemInd() {
		return this.blankOrderItemInd;
	}

	public void setBlankOrderItemInd(String blankOrderItemInd) {
		this.blankOrderItemInd = blankOrderItemInd;
	}

	public String getBpjsInd() {
		return this.bpjsInd;
	}

	public void setBpjsInd(String bpjsInd) {
		this.bpjsInd = bpjsInd;
	}

	public String getBrandName() {
		return this.brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getBrandNameLang1() {
		return this.brandNameLang1;
	}

	public void setBrandNameLang1(String brandNameLang1) {
		this.brandNameLang1 = brandNameLang1;
	}

	public String getBrandNameLang2() {
		return this.brandNameLang2;
	}

	public void setBrandNameLang2(String brandNameLang2) {
		this.brandNameLang2 = brandNameLang2;
	}

	public String getBrandNameLang3() {
		return this.brandNameLang3;
	}

	public void setBrandNameLang3(String brandNameLang3) {
		this.brandNameLang3 = brandNameLang3;
	}

	public String getBulkItemInd() {
		return this.bulkItemInd;
	}

	public void setBulkItemInd(String bulkItemInd) {
		this.bulkItemInd = bulkItemInd;
	}

	public String getChargeInd() {
		return this.chargeInd;
	}

	public void setChargeInd(String chargeInd) {
		this.chargeInd = chargeInd;
	}

	public String getConcessionaireItemInd() {
		return this.concessionaireItemInd;
	}

	public void setConcessionaireItemInd(String concessionaireItemInd) {
		this.concessionaireItemInd = concessionaireItemInd;
	}

	public String getConsignmentItemInd() {
		return this.consignmentItemInd;
	}

	public void setConsignmentItemInd(String consignmentItemInd) {
		this.consignmentItemInd = consignmentItemInd;
	}

	public String getCostingItemInd() {
		return this.costingItemInd;
	}

	public void setCostingItemInd(String costingItemInd) {
		this.costingItemInd = costingItemInd;
	}

	public BigDecimal getDaysForMarker() {
		return this.daysForMarker;
	}

	public void setDaysForMarker(BigDecimal daysForMarker) {
		this.daysForMarker = daysForMarker;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getEffectiveMonths() {
		return this.effectiveMonths;
	}

	public void setEffectiveMonths(BigDecimal effectiveMonths) {
		this.effectiveMonths = effectiveMonths;
	}

	public String getExpensiveItemInd() {
		return this.expensiveItemInd;
	}

	public void setExpensiveItemInd(String expensiveItemInd) {
		this.expensiveItemInd = expensiveItemInd;
	}

	public BigDecimal getFastMarker() {
		return this.fastMarker;
	}

	public void setFastMarker(BigDecimal fastMarker) {
		this.fastMarker = fastMarker;
	}

	public String getHighValueSuppliesInd() {
		return this.highValueSuppliesInd;
	}

	public void setHighValueSuppliesInd(String highValueSuppliesInd) {
		this.highValueSuppliesInd = highValueSuppliesInd;
	}

	public String getImportItemInd() {
		return this.importItemInd;
	}

	public void setImportItemInd(String importItemInd) {
		this.importItemInd = importItemInd;
	}

	public String getIssueOnlyItemInd() {
		return this.issueOnlyItemInd;
	}

	public void setIssueOnlyItemInd(String issueOnlyItemInd) {
		this.issueOnlyItemInd = issueOnlyItemInd;
	}

	public String getLabelUom() {
		return this.labelUom;
	}

	public void setLabelUom(String labelUom) {
		this.labelUom = labelUom;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getLatestCost() {
		return this.latestCost;
	}

	public void setLatestCost(BigDecimal latestCost) {
		this.latestCost = latestCost;
	}

	public String getLockedItemInd() {
		return this.lockedItemInd;
	}

	public void setLockedItemInd(String lockedItemInd) {
		this.lockedItemInd = lockedItemInd;
	}

	public BigDecimal getLossRate() {
		return this.lossRate;
	}

	public void setLossRate(BigDecimal lossRate) {
		this.lossRate = lossRate;
	}

	public String getManufactureInd() {
		return this.manufactureInd;
	}

	public void setManufactureInd(String manufactureInd) {
		this.manufactureInd = manufactureInd;
	}

	public BigDecimal getManufacturerVendormstrId() {
		return this.manufacturerVendormstrId;
	}

	public void setManufacturerVendormstrId(BigDecimal manufacturerVendormstrId) {
		this.manufacturerVendormstrId = manufacturerVendormstrId;
	}

	public String getMaterialItemCode() {
		return this.materialItemCode;
	}

	public void setMaterialItemCode(String materialItemCode) {
		this.materialItemCode = materialItemCode;
	}

	public String getMaterialItemManageType() {
		return this.materialItemManageType;
	}

	public void setMaterialItemManageType(String materialItemManageType) {
		this.materialItemManageType = materialItemManageType;
	}

	public String getMaterialItemModel() {
		return this.materialItemModel;
	}

	public void setMaterialItemModel(String materialItemModel) {
		this.materialItemModel = materialItemModel;
	}

	public String getMaterialItemName() {
		return this.materialItemName;
	}

	public void setMaterialItemName(String materialItemName) {
		this.materialItemName = materialItemName;
	}

	public String getMaterialItemNameLang1() {
		return this.materialItemNameLang1;
	}

	public void setMaterialItemNameLang1(String materialItemNameLang1) {
		this.materialItemNameLang1 = materialItemNameLang1;
	}

	public String getMaterialItemNameLang2() {
		return this.materialItemNameLang2;
	}

	public void setMaterialItemNameLang2(String materialItemNameLang2) {
		this.materialItemNameLang2 = materialItemNameLang2;
	}

	public String getMaterialItemNameLang3() {
		return this.materialItemNameLang3;
	}

	public void setMaterialItemNameLang3(String materialItemNameLang3) {
		this.materialItemNameLang3 = materialItemNameLang3;
	}

	public String getMaterialItemOrigin() {
		return this.materialItemOrigin;
	}

	public void setMaterialItemOrigin(String materialItemOrigin) {
		this.materialItemOrigin = materialItemOrigin;
	}

	public String getMaterialItemType() {
		return this.materialItemType;
	}

	public void setMaterialItemType(String materialItemType) {
		this.materialItemType = materialItemType;
	}

	public BigDecimal getMaxQtyPerLabel() {
		return this.maxQtyPerLabel;
	}

	public void setMaxQtyPerLabel(BigDecimal maxQtyPerLabel) {
		this.maxQtyPerLabel = maxQtyPerLabel;
	}

	public String getOneTimePurchaseItemInd() {
		return this.oneTimePurchaseItemInd;
	}

	public void setOneTimePurchaseItemInd(String oneTimePurchaseItemInd) {
		this.oneTimePurchaseItemInd = oneTimePurchaseItemInd;
	}

	public String getOtherCode() {
		return this.otherCode;
	}

	public void setOtherCode(String otherCode) {
		this.otherCode = otherCode;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPriceInd() {
		return this.priceInd;
	}

	public void setPriceInd(String priceInd) {
		this.priceInd = priceInd;
	}

	public String getPurchaseInd() {
		return this.purchaseInd;
	}

	public void setPurchaseInd(String purchaseInd) {
		this.purchaseInd = purchaseInd;
	}

	public String getQtyRoundingRule() {
		return this.qtyRoundingRule;
	}

	public void setQtyRoundingRule(String qtyRoundingRule) {
		this.qtyRoundingRule = qtyRoundingRule;
	}

	public String getRecycledItemInd() {
		return this.recycledItemInd;
	}

	public void setRecycledItemInd(String recycledItemInd) {
		this.recycledItemInd = recycledItemInd;
	}

	public String getRentalItemInd() {
		return this.rentalItemInd;
	}

	public void setRentalItemInd(String rentalItemInd) {
		this.rentalItemInd = rentalItemInd;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public BigDecimal getSlowMarker() {
		return this.slowMarker;
	}

	public void setSlowMarker(BigDecimal slowMarker) {
		this.slowMarker = slowMarker;
	}

	public String getSpec() {
		return this.spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public String getSpecLang1() {
		return this.specLang1;
	}

	public void setSpecLang1(String specLang1) {
		this.specLang1 = specLang1;
	}

	public BigDecimal getStandardCost() {
		return this.standardCost;
	}

	public void setStandardCost(BigDecimal standardCost) {
		this.standardCost = standardCost;
	}

	public String getStockItemInd() {
		return this.stockItemInd;
	}

	public void setStockItemInd(String stockItemInd) {
		this.stockItemInd = stockItemInd;
	}

	public BigDecimal getTotalCost() {
		return this.totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	public BigDecimal getTotalQty() {
		return this.totalQty;
	}

	public void setTotalQty(BigDecimal totalQty) {
		this.totalQty = totalQty;
	}

	public String getTrackInd() {
		return this.trackInd;
	}

	public void setTrackInd(String trackInd) {
		this.trackInd = trackInd;
	}

	public String getTransactableItemInd() {
		return this.transactableItemInd;
	}

	public void setTransactableItemInd(String transactableItemInd) {
		this.transactableItemInd = transactableItemInd;
	}

	public BigDecimal getTxncodemstrId() {
		return this.txncodemstrId;
	}

	public void setTxncodemstrId(BigDecimal txncodemstrId) {
		this.txncodemstrId = txncodemstrId;
	}

	public String getUseBeforeReceiptedInd() {
		return this.useBeforeReceiptedInd;
	}

	public void setUseBeforeReceiptedInd(String useBeforeReceiptedInd) {
		this.useBeforeReceiptedInd = useBeforeReceiptedInd;
	}

	public BigDecimal getWholesalePrice() {
		return this.wholesalePrice;
	}

	public void setWholesalePrice(BigDecimal wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}

	public List<Chargematerialcompdetail> getChargematerialcompdetails() {
		return this.chargematerialcompdetails;
	}

	public void setChargematerialcompdetails(List<Chargematerialcompdetail> chargematerialcompdetails) {
		this.chargematerialcompdetails = chargematerialcompdetails;
	}

	public Chargematerialcompdetail addChargematerialcompdetail(Chargematerialcompdetail chargematerialcompdetail) {
		getChargematerialcompdetails().add(chargematerialcompdetail);
		chargematerialcompdetail.setMaterialitemmstr(this);

		return chargematerialcompdetail;
	}

	public Chargematerialcompdetail removeChargematerialcompdetail(Chargematerialcompdetail chargematerialcompdetail) {
		getChargematerialcompdetails().remove(chargematerialcompdetail);
		chargematerialcompdetail.setMaterialitemmstr(null);

		return chargematerialcompdetail;
	}

	public List<Materialconsumption> getMaterialconsumptions() {
		return this.materialconsumptions;
	}

	public void setMaterialconsumptions(List<Materialconsumption> materialconsumptions) {
		this.materialconsumptions = materialconsumptions;
	}

	public Materialconsumption addMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().add(materialconsumption);
		materialconsumption.setMaterialitemmstr(this);

		return materialconsumption;
	}

	public Materialconsumption removeMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().remove(materialconsumption);
		materialconsumption.setMaterialitemmstr(null);

		return materialconsumption;
	}

	public Materialitemcatmstr getMaterialitemcatmstr() {
		return this.materialitemcatmstr;
	}

	public void setMaterialitemcatmstr(Materialitemcatmstr materialitemcatmstr) {
		this.materialitemcatmstr = materialitemcatmstr;
	}

	public List<Materialprice> getMaterialprices() {
		return this.materialprices;
	}

	public void setMaterialprices(List<Materialprice> materialprices) {
		this.materialprices = materialprices;
	}

	public Materialprice addMaterialprice(Materialprice materialprice) {
		getMaterialprices().add(materialprice);
		materialprice.setMaterialitemmstr(this);

		return materialprice;
	}

	public Materialprice removeMaterialprice(Materialprice materialprice) {
		getMaterialprices().remove(materialprice);
		materialprice.setMaterialitemmstr(null);

		return materialprice;
	}

	public List<Materialpriceadjust> getMaterialpriceadjusts() {
		return this.materialpriceadjusts;
	}

	public void setMaterialpriceadjusts(List<Materialpriceadjust> materialpriceadjusts) {
		this.materialpriceadjusts = materialpriceadjusts;
	}

	public Materialpriceadjust addMaterialpriceadjust(Materialpriceadjust materialpriceadjust) {
		getMaterialpriceadjusts().add(materialpriceadjust);
		materialpriceadjust.setMaterialitemmstr(this);

		return materialpriceadjust;
	}

	public Materialpriceadjust removeMaterialpriceadjust(Materialpriceadjust materialpriceadjust) {
		getMaterialpriceadjusts().remove(materialpriceadjust);
		materialpriceadjust.setMaterialitemmstr(null);

		return materialpriceadjust;
	}

	public List<Materialtxn> getMaterialtxns() {
		return this.materialtxns;
	}

	public void setMaterialtxns(List<Materialtxn> materialtxns) {
		this.materialtxns = materialtxns;
	}

	public Materialtxn addMaterialtxn(Materialtxn materialtxn) {
		getMaterialtxns().add(materialtxn);
		materialtxn.setMaterialitemmstr(this);

		return materialtxn;
	}

	public Materialtxn removeMaterialtxn(Materialtxn materialtxn) {
		getMaterialtxns().remove(materialtxn);
		materialtxn.setMaterialitemmstr(null);

		return materialtxn;
	}



	public List<Storecountdefinedetail> getStorecountdefinedetails() {
		return this.storecountdefinedetails;
	}

	public void setStorecountdefinedetails(List<Storecountdefinedetail> storecountdefinedetails) {
		this.storecountdefinedetails = storecountdefinedetails;
	}

	public Storecountdefinedetail addStorecountdefinedetail(Storecountdefinedetail storecountdefinedetail) {
		getStorecountdefinedetails().add(storecountdefinedetail);
		storecountdefinedetail.setMaterialitemmstr(this);

		return storecountdefinedetail;
	}

	public Storecountdefinedetail removeStorecountdefinedetail(Storecountdefinedetail storecountdefinedetail) {
		getStorecountdefinedetails().remove(storecountdefinedetail);
		storecountdefinedetail.setMaterialitemmstr(null);

		return storecountdefinedetail;
	}

	public List<Storeitem> getStoreitems() {
		return this.storeitems;
	}

	public void setStoreitems(List<Storeitem> storeitems) {
		this.storeitems = storeitems;
	}

	public Storeitem addStoreitem(Storeitem storeitem) {
		getStoreitems().add(storeitem);
		storeitem.setMaterialitemmstr(this);

		return storeitem;
	}

	public Storeitem removeStoreitem(Storeitem storeitem) {
		getStoreitems().remove(storeitem);
		storeitem.setMaterialitemmstr(null);

		return storeitem;
	}

	public List<Vendoritem> getVendoritems() {
		return this.vendoritems;
	}

	public void setVendoritems(List<Vendoritem> vendoritems) {
		this.vendoritems = vendoritems;
	}

	public Vendoritem addVendoritem(Vendoritem vendoritem) {
		getVendoritems().add(vendoritem);
		vendoritem.setMaterialitemmstr(this);

		return vendoritem;
	}

	public Vendoritem removeVendoritem(Vendoritem vendoritem) {
		getVendoritems().remove(vendoritem);
		vendoritem.setMaterialitemmstr(null);

		return vendoritem;
	}

	public List<Wardsspmstocktxn> getWardsspmstocktxns() {
		return this.wardsspmstocktxns;
	}

	public void setWardsspmstocktxns(List<Wardsspmstocktxn> wardsspmstocktxns) {
		this.wardsspmstocktxns = wardsspmstocktxns;
	}

	public Wardsspmstocktxn addWardsspmstocktxn(Wardsspmstocktxn wardsspmstocktxn) {
		getWardsspmstocktxns().add(wardsspmstocktxn);
		wardsspmstocktxn.setMaterialitemmstr(this);

		return wardsspmstocktxn;
	}

	public Wardsspmstocktxn removeWardsspmstocktxn(Wardsspmstocktxn wardsspmstocktxn) {
		getWardsspmstocktxns().remove(wardsspmstocktxn);
		wardsspmstocktxn.setMaterialitemmstr(null);

		return wardsspmstocktxn;
	}

}