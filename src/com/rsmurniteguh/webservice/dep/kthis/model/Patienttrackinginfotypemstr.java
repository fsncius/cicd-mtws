package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PATIENTTRACKINGINFOTYPEMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Patienttrackinginfotypemstr.findAll", query="SELECT p FROM Patienttrackinginfotypemstr p")
public class Patienttrackinginfotypemstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTTRACKINGINFOTYPEMSTR_ID")
	private long patienttrackinginfotypemstrId;

	@Column(name="ALARM_IND")
	private String alarmInd;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="INFO_LEVEL")
	private String infoLevel;

	@Column(name="INFO_TYPE")
	private String infoType;

	@Column(name="INFO_TYPE_CODE")
	private String infoTypeCode;

	@Column(name="INFO_TYPE_DESC")
	private String infoTypeDesc;

	@Column(name="INFO_TYPE_DESC_LANG1")
	private String infoTypeDescLang1;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	private String remarks;

	//bi-directional many-to-one association to Patienttrackinginfo
	@OneToMany(mappedBy="patienttrackinginfotypemstr")
	private List<Patienttrackinginfo> patienttrackinginfos;

	//bi-directional many-to-one association to Patienttrackinginfotemplate
	@OneToMany(mappedBy="patienttrackinginfotypemstr")
	private List<Patienttrackinginfotemplate> patienttrackinginfotemplates;

	//bi-directional many-to-one association to Patienttrackinginfotyperole
	@OneToMany(mappedBy="patienttrackinginfotypemstr")
	private List<Patienttrackinginfotyperole> patienttrackinginfotyperoles;

	public Patienttrackinginfotypemstr() {
	}

	public long getPatienttrackinginfotypemstrId() {
		return this.patienttrackinginfotypemstrId;
	}

	public void setPatienttrackinginfotypemstrId(long patienttrackinginfotypemstrId) {
		this.patienttrackinginfotypemstrId = patienttrackinginfotypemstrId;
	}

	public String getAlarmInd() {
		return this.alarmInd;
	}

	public void setAlarmInd(String alarmInd) {
		this.alarmInd = alarmInd;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getInfoLevel() {
		return this.infoLevel;
	}

	public void setInfoLevel(String infoLevel) {
		this.infoLevel = infoLevel;
	}

	public String getInfoType() {
		return this.infoType;
	}

	public void setInfoType(String infoType) {
		this.infoType = infoType;
	}

	public String getInfoTypeCode() {
		return this.infoTypeCode;
	}

	public void setInfoTypeCode(String infoTypeCode) {
		this.infoTypeCode = infoTypeCode;
	}

	public String getInfoTypeDesc() {
		return this.infoTypeDesc;
	}

	public void setInfoTypeDesc(String infoTypeDesc) {
		this.infoTypeDesc = infoTypeDesc;
	}

	public String getInfoTypeDescLang1() {
		return this.infoTypeDescLang1;
	}

	public void setInfoTypeDescLang1(String infoTypeDescLang1) {
		this.infoTypeDescLang1 = infoTypeDescLang1;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<Patienttrackinginfo> getPatienttrackinginfos() {
		return this.patienttrackinginfos;
	}

	public void setPatienttrackinginfos(List<Patienttrackinginfo> patienttrackinginfos) {
		this.patienttrackinginfos = patienttrackinginfos;
	}

	public Patienttrackinginfo addPatienttrackinginfo(Patienttrackinginfo patienttrackinginfo) {
		getPatienttrackinginfos().add(patienttrackinginfo);
		patienttrackinginfo.setPatienttrackinginfotypemstr(this);

		return patienttrackinginfo;
	}

	public Patienttrackinginfo removePatienttrackinginfo(Patienttrackinginfo patienttrackinginfo) {
		getPatienttrackinginfos().remove(patienttrackinginfo);
		patienttrackinginfo.setPatienttrackinginfotypemstr(null);

		return patienttrackinginfo;
	}

	public List<Patienttrackinginfotemplate> getPatienttrackinginfotemplates() {
		return this.patienttrackinginfotemplates;
	}

	public void setPatienttrackinginfotemplates(List<Patienttrackinginfotemplate> patienttrackinginfotemplates) {
		this.patienttrackinginfotemplates = patienttrackinginfotemplates;
	}

	public Patienttrackinginfotemplate addPatienttrackinginfotemplate(Patienttrackinginfotemplate patienttrackinginfotemplate) {
		getPatienttrackinginfotemplates().add(patienttrackinginfotemplate);
		patienttrackinginfotemplate.setPatienttrackinginfotypemstr(this);

		return patienttrackinginfotemplate;
	}

	public Patienttrackinginfotemplate removePatienttrackinginfotemplate(Patienttrackinginfotemplate patienttrackinginfotemplate) {
		getPatienttrackinginfotemplates().remove(patienttrackinginfotemplate);
		patienttrackinginfotemplate.setPatienttrackinginfotypemstr(null);

		return patienttrackinginfotemplate;
	}

	public List<Patienttrackinginfotyperole> getPatienttrackinginfotyperoles() {
		return this.patienttrackinginfotyperoles;
	}

	public void setPatienttrackinginfotyperoles(List<Patienttrackinginfotyperole> patienttrackinginfotyperoles) {
		this.patienttrackinginfotyperoles = patienttrackinginfotyperoles;
	}

	public Patienttrackinginfotyperole addPatienttrackinginfotyperole(Patienttrackinginfotyperole patienttrackinginfotyperole) {
		getPatienttrackinginfotyperoles().add(patienttrackinginfotyperole);
		patienttrackinginfotyperole.setPatienttrackinginfotypemstr(this);

		return patienttrackinginfotyperole;
	}

	public Patienttrackinginfotyperole removePatienttrackinginfotyperole(Patienttrackinginfotyperole patienttrackinginfotyperole) {
		getPatienttrackinginfotyperoles().remove(patienttrackinginfotyperole);
		patienttrackinginfotyperole.setPatienttrackinginfotypemstr(null);

		return patienttrackinginfotyperole;
	}

}