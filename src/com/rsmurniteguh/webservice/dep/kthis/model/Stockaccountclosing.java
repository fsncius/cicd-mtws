package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKACCOUNTCLOSING database table.
 * 
 */
@Entity
@NamedQuery(name="Stockaccountclosing.findAll", query="SELECT s FROM Stockaccountclosing s")
public class Stockaccountclosing implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKACCOUNTCLOSING_ID")
	private long stockaccountclosingId;

	@Column(name="ACCOUNTCLOSING_BY")
	private BigDecimal accountclosingBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ACCOUNTCLOSING_DATETIME")
	private Date accountclosingDatetime;

	@Column(name="ACCOUNTCLOSING_NO")
	private String accountclosingNo;

	@Column(name="ACCOUNTCLOSING_STATUS")
	private String accountclosingStatus;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREMSTR_ID")
	private Storemstr storemstr;

	//bi-directional many-to-one association to Stockaccountclosingdetail
	@OneToMany(mappedBy="stockaccountclosing")
	private List<Stockaccountclosingdetail> stockaccountclosingdetails;

	public Stockaccountclosing() {
	}

	public long getStockaccountclosingId() {
		return this.stockaccountclosingId;
	}

	public void setStockaccountclosingId(long stockaccountclosingId) {
		this.stockaccountclosingId = stockaccountclosingId;
	}

	public BigDecimal getAccountclosingBy() {
		return this.accountclosingBy;
	}

	public void setAccountclosingBy(BigDecimal accountclosingBy) {
		this.accountclosingBy = accountclosingBy;
	}

	public Date getAccountclosingDatetime() {
		return this.accountclosingDatetime;
	}

	public void setAccountclosingDatetime(Date accountclosingDatetime) {
		this.accountclosingDatetime = accountclosingDatetime;
	}

	public String getAccountclosingNo() {
		return this.accountclosingNo;
	}

	public void setAccountclosingNo(String accountclosingNo) {
		this.accountclosingNo = accountclosingNo;
	}

	public String getAccountclosingStatus() {
		return this.accountclosingStatus;
	}

	public void setAccountclosingStatus(String accountclosingStatus) {
		this.accountclosingStatus = accountclosingStatus;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

	public List<Stockaccountclosingdetail> getStockaccountclosingdetails() {
		return this.stockaccountclosingdetails;
	}

	public void setStockaccountclosingdetails(List<Stockaccountclosingdetail> stockaccountclosingdetails) {
		this.stockaccountclosingdetails = stockaccountclosingdetails;
	}

	public Stockaccountclosingdetail addStockaccountclosingdetail(Stockaccountclosingdetail stockaccountclosingdetail) {
		getStockaccountclosingdetails().add(stockaccountclosingdetail);
		stockaccountclosingdetail.setStockaccountclosing(this);

		return stockaccountclosingdetail;
	}

	public Stockaccountclosingdetail removeStockaccountclosingdetail(Stockaccountclosingdetail stockaccountclosingdetail) {
		getStockaccountclosingdetails().remove(stockaccountclosingdetail);
		stockaccountclosingdetail.setStockaccountclosing(null);

		return stockaccountclosingdetail;
	}

}