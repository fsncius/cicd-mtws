package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKREQUISITION database table.
 * 
 */
@Entity
@NamedQuery(name="Stockrequisition.findAll", query="SELECT s FROM Stockrequisition s")
public class Stockrequisition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKREQUISITION_ID")
	private long stockrequisitionId;

	@Column(name="APPROVED_BY")
	private BigDecimal approvedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="APPROVED_DATETIME")
	private Date approvedDatetime;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPECTED_DELIVERY_DATE")
	private Date expectedDeliveryDate;

	@Column(name="INTERVENTION_REASON")
	private String interventionReason;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="REQUISITION_NO")
	private String requisitionNo;

	@Column(name="REQUISITION_STATUS")
	private String requisitionStatus;

	@Column(name="REQUISITION_STOREMSTR_ID")
	private BigDecimal requisitionStoremstrId;

	@Column(name="REQUISITION_TYPE")
	private String requisitionType;

	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenuecentremstrId;

	@Column(name="SUPPLY_STOREMSTR_ID")
	private BigDecimal supplyStoremstrId;

	@Column(name="URGENT_REQUEST_IND")
	private String urgentRequestInd;

	//bi-directional many-to-one association to Stockrequisitiondetail
	@OneToMany(mappedBy="stockrequisition")
	private List<Stockrequisitiondetail> stockrequisitiondetails;

	public Stockrequisition() {
	}

	public long getStockrequisitionId() {
		return this.stockrequisitionId;
	}

	public void setStockrequisitionId(long stockrequisitionId) {
		this.stockrequisitionId = stockrequisitionId;
	}

	public BigDecimal getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(BigDecimal approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedDatetime() {
		return this.approvedDatetime;
	}

	public void setApprovedDatetime(Date approvedDatetime) {
		this.approvedDatetime = approvedDatetime;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Date getExpectedDeliveryDate() {
		return this.expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public String getInterventionReason() {
		return this.interventionReason;
	}

	public void setInterventionReason(String interventionReason) {
		this.interventionReason = interventionReason;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRequisitionNo() {
		return this.requisitionNo;
	}

	public void setRequisitionNo(String requisitionNo) {
		this.requisitionNo = requisitionNo;
	}

	public String getRequisitionStatus() {
		return this.requisitionStatus;
	}

	public void setRequisitionStatus(String requisitionStatus) {
		this.requisitionStatus = requisitionStatus;
	}

	public BigDecimal getRequisitionStoremstrId() {
		return this.requisitionStoremstrId;
	}

	public void setRequisitionStoremstrId(BigDecimal requisitionStoremstrId) {
		this.requisitionStoremstrId = requisitionStoremstrId;
	}

	public String getRequisitionType() {
		return this.requisitionType;
	}

	public void setRequisitionType(String requisitionType) {
		this.requisitionType = requisitionType;
	}

	public BigDecimal getRevenuecentremstrId() {
		return this.revenuecentremstrId;
	}

	public void setRevenuecentremstrId(BigDecimal revenuecentremstrId) {
		this.revenuecentremstrId = revenuecentremstrId;
	}

	public BigDecimal getSupplyStoremstrId() {
		return this.supplyStoremstrId;
	}

	public void setSupplyStoremstrId(BigDecimal supplyStoremstrId) {
		this.supplyStoremstrId = supplyStoremstrId;
	}

	public String getUrgentRequestInd() {
		return this.urgentRequestInd;
	}

	public void setUrgentRequestInd(String urgentRequestInd) {
		this.urgentRequestInd = urgentRequestInd;
	}

	public List<Stockrequisitiondetail> getStockrequisitiondetails() {
		return this.stockrequisitiondetails;
	}

	public void setStockrequisitiondetails(List<Stockrequisitiondetail> stockrequisitiondetails) {
		this.stockrequisitiondetails = stockrequisitiondetails;
	}

	public Stockrequisitiondetail addStockrequisitiondetail(Stockrequisitiondetail stockrequisitiondetail) {
		getStockrequisitiondetails().add(stockrequisitiondetail);
		stockrequisitiondetail.setStockrequisition(this);

		return stockrequisitiondetail;
	}

	public Stockrequisitiondetail removeStockrequisitiondetail(Stockrequisitiondetail stockrequisitiondetail) {
		getStockrequisitiondetails().remove(stockrequisitiondetail);
		stockrequisitiondetail.setStockrequisition(null);

		return stockrequisitiondetail;
	}

}