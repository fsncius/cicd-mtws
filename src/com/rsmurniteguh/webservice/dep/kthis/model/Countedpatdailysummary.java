package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the COUNTEDPATDAILYSUMMARY database table.
 * 
 */
@Entity
@NamedQuery(name="Countedpatdailysummary.findAll", query="SELECT c FROM Countedpatdailysummary c")
public class Countedpatdailysummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTEDPATDAILYSUMMARY_ID")
	private long countedpatdailysummaryId;

	@Column(name="COUNTED_AMOUNT")
	private BigDecimal countedAmount;

	@Column(name="COUNTED_AMOUNT_NOR")
	private BigDecimal countedAmountNor;

	@Column(name="COUNTED_AMOUNT_RFD")
	private BigDecimal countedAmountRfd;

	@Column(name="COUNTED_COUNT")
	private BigDecimal countedCount;

	@Column(name="COUNTED_COUNT_NOR")
	private BigDecimal countedCountNor;

	@Column(name="COUNTED_COUNT_RFD")
	private BigDecimal countedCountRfd;

	@Temporal(TemporalType.DATE)
	@Column(name="COUNTED_DATE")
	private Date countedDate;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DRUG_IND")
	private String drugInd;

	@Column(name="ITEM_TYPE")
	private String itemType;

	@Column(name="ITEMSUBCATEGORYMSTR_ID")
	private BigDecimal itemsubcategorymstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MIO_MIC1_AMOUNT")
	private BigDecimal mioMic1Amount;

	@Column(name="MIO_MIC2_AMOUNT")
	private BigDecimal mioMic2Amount;

	@Column(name="MIO_MIC3_AMOUNT")
	private BigDecimal mioMic3Amount;

	@Column(name="PATIENT_CAT")
	private String patientCat;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="RECEIPT_ITEM_CAT")
	private String receiptItemCat;

	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenuecentremstrId;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr1;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr2;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_WARDMSTR_ID")
	private Wardmstr wardmstr;

	public Countedpatdailysummary() {
	}

	public long getCountedpatdailysummaryId() {
		return this.countedpatdailysummaryId;
	}

	public void setCountedpatdailysummaryId(long countedpatdailysummaryId) {
		this.countedpatdailysummaryId = countedpatdailysummaryId;
	}

	public BigDecimal getCountedAmount() {
		return this.countedAmount;
	}

	public void setCountedAmount(BigDecimal countedAmount) {
		this.countedAmount = countedAmount;
	}

	public BigDecimal getCountedAmountNor() {
		return this.countedAmountNor;
	}

	public void setCountedAmountNor(BigDecimal countedAmountNor) {
		this.countedAmountNor = countedAmountNor;
	}

	public BigDecimal getCountedAmountRfd() {
		return this.countedAmountRfd;
	}

	public void setCountedAmountRfd(BigDecimal countedAmountRfd) {
		this.countedAmountRfd = countedAmountRfd;
	}

	public BigDecimal getCountedCount() {
		return this.countedCount;
	}

	public void setCountedCount(BigDecimal countedCount) {
		this.countedCount = countedCount;
	}

	public BigDecimal getCountedCountNor() {
		return this.countedCountNor;
	}

	public void setCountedCountNor(BigDecimal countedCountNor) {
		this.countedCountNor = countedCountNor;
	}

	public BigDecimal getCountedCountRfd() {
		return this.countedCountRfd;
	}

	public void setCountedCountRfd(BigDecimal countedCountRfd) {
		this.countedCountRfd = countedCountRfd;
	}

	public Date getCountedDate() {
		return this.countedDate;
	}

	public void setCountedDate(Date countedDate) {
		this.countedDate = countedDate;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDrugInd() {
		return this.drugInd;
	}

	public void setDrugInd(String drugInd) {
		this.drugInd = drugInd;
	}

	public String getItemType() {
		return this.itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public BigDecimal getItemsubcategorymstrId() {
		return this.itemsubcategorymstrId;
	}

	public void setItemsubcategorymstrId(BigDecimal itemsubcategorymstrId) {
		this.itemsubcategorymstrId = itemsubcategorymstrId;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMioMic1Amount() {
		return this.mioMic1Amount;
	}

	public void setMioMic1Amount(BigDecimal mioMic1Amount) {
		this.mioMic1Amount = mioMic1Amount;
	}

	public BigDecimal getMioMic2Amount() {
		return this.mioMic2Amount;
	}

	public void setMioMic2Amount(BigDecimal mioMic2Amount) {
		this.mioMic2Amount = mioMic2Amount;
	}

	public BigDecimal getMioMic3Amount() {
		return this.mioMic3Amount;
	}

	public void setMioMic3Amount(BigDecimal mioMic3Amount) {
		this.mioMic3Amount = mioMic3Amount;
	}

	public String getPatientCat() {
		return this.patientCat;
	}

	public void setPatientCat(String patientCat) {
		this.patientCat = patientCat;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public String getReceiptItemCat() {
		return this.receiptItemCat;
	}

	public void setReceiptItemCat(String receiptItemCat) {
		this.receiptItemCat = receiptItemCat;
	}

	public BigDecimal getRevenuecentremstrId() {
		return this.revenuecentremstrId;
	}

	public void setRevenuecentremstrId(BigDecimal revenuecentremstrId) {
		this.revenuecentremstrId = revenuecentremstrId;
	}

	public Subspecialtymstr getSubspecialtymstr1() {
		return this.subspecialtymstr1;
	}

	public void setSubspecialtymstr1(Subspecialtymstr subspecialtymstr1) {
		this.subspecialtymstr1 = subspecialtymstr1;
	}

	public Subspecialtymstr getSubspecialtymstr2() {
		return this.subspecialtymstr2;
	}

	public void setSubspecialtymstr2(Subspecialtymstr subspecialtymstr2) {
		this.subspecialtymstr2 = subspecialtymstr2;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

}