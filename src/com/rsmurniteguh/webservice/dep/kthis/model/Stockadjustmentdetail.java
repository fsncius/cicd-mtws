package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the STOCKADJUSTMENTDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Stockadjustmentdetail.findAll", query="SELECT s FROM Stockadjustmentdetail s")
public class Stockadjustmentdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKADJUSTMENTDETAIL_ID")
	private long stockadjustmentdetailId;

	@Column(name="ADJUSTED_QTY")
	private BigDecimal adjustedQty;

	@Column(name="ADJUSTED_QTY_OF_BASE_UOM")
	private BigDecimal adjustedQtyOfBaseUom;

	@Column(name="ADJUSTED_UOM")
	private String adjustedUom;

	@Column(name="ADJUSTMENT_DETAIL_STATUS")
	private String adjustmentDetailStatus;

	@Column(name="BATCH_NO")
	private String batchNo;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MANUFACTURER_VENDORMSTR_ID")
	private BigDecimal manufacturerVendormstrId;

	@Column(name="MATERIALITEMMSTR_ID")
	private BigDecimal materialitemmstrId;

	@Column(name="MATERIALTXN_ID")
	private BigDecimal materialtxnId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String reason;

	private String remarks;

	@Column(name="STOCKCOUNTDETAIL_ID")
	private BigDecimal stockcountdetailId;

	@Column(name="STOREBINMSTR_ID")
	private BigDecimal storebinmstrId;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Stockadjustment
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKADJUSTMENT_ID")
	private Stockadjustment stockadjustment;

	//bi-directional many-to-one association to Stockreceiptdetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKSET_ID")
	private Stockreceiptdetail stockreceiptdetail;

	public Stockadjustmentdetail() {
	}

	public long getStockadjustmentdetailId() {
		return this.stockadjustmentdetailId;
	}

	public void setStockadjustmentdetailId(long stockadjustmentdetailId) {
		this.stockadjustmentdetailId = stockadjustmentdetailId;
	}

	public BigDecimal getAdjustedQty() {
		return this.adjustedQty;
	}

	public void setAdjustedQty(BigDecimal adjustedQty) {
		this.adjustedQty = adjustedQty;
	}

	public BigDecimal getAdjustedQtyOfBaseUom() {
		return this.adjustedQtyOfBaseUom;
	}

	public void setAdjustedQtyOfBaseUom(BigDecimal adjustedQtyOfBaseUom) {
		this.adjustedQtyOfBaseUom = adjustedQtyOfBaseUom;
	}

	public String getAdjustedUom() {
		return this.adjustedUom;
	}

	public void setAdjustedUom(String adjustedUom) {
		this.adjustedUom = adjustedUom;
	}

	public String getAdjustmentDetailStatus() {
		return this.adjustmentDetailStatus;
	}

	public void setAdjustmentDetailStatus(String adjustmentDetailStatus) {
		this.adjustmentDetailStatus = adjustmentDetailStatus;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getManufacturerVendormstrId() {
		return this.manufacturerVendormstrId;
	}

	public void setManufacturerVendormstrId(BigDecimal manufacturerVendormstrId) {
		this.manufacturerVendormstrId = manufacturerVendormstrId;
	}

	public BigDecimal getMaterialitemmstrId() {
		return this.materialitemmstrId;
	}

	public void setMaterialitemmstrId(BigDecimal materialitemmstrId) {
		this.materialitemmstrId = materialitemmstrId;
	}

	public BigDecimal getMaterialtxnId() {
		return this.materialtxnId;
	}

	public void setMaterialtxnId(BigDecimal materialtxnId) {
		this.materialtxnId = materialtxnId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStockcountdetailId() {
		return this.stockcountdetailId;
	}

	public void setStockcountdetailId(BigDecimal stockcountdetailId) {
		this.stockcountdetailId = stockcountdetailId;
	}

	public BigDecimal getStorebinmstrId() {
		return this.storebinmstrId;
	}

	public void setStorebinmstrId(BigDecimal storebinmstrId) {
		this.storebinmstrId = storebinmstrId;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Stockadjustment getStockadjustment() {
		return this.stockadjustment;
	}

	public void setStockadjustment(Stockadjustment stockadjustment) {
		this.stockadjustment = stockadjustment;
	}

	public Stockreceiptdetail getStockreceiptdetail() {
		return this.stockreceiptdetail;
	}

	public void setStockreceiptdetail(Stockreceiptdetail stockreceiptdetail) {
		this.stockreceiptdetail = stockreceiptdetail;
	}

}