package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the WARDSSPMSTOCKTXN database table.
 * 
 */
@Entity
@NamedQuery(name="Wardsspmstocktxn.findAll", query="SELECT w FROM Wardsspmstocktxn w")
public class Wardsspmstocktxn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WARDSSPMSTOCKTXN_ID")
	private long wardsspmstocktxnId;

	@Column(name="BATCH_NO")
	private String batchNo;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIALTXN_TYPE")
	private String materialtxnType;

	@Column(name="REQUESTED_IND")
	private String requestedInd;

	@Column(name="STOCKSET_ID")
	private BigDecimal stocksetId;

	@Column(name="TXN_QTY")
	private BigDecimal txnQty;

	@Column(name="TXN_QTY_UOM")
	private String txnQtyUom;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Drugdispense
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DRUGDISPENSE_ID")
	private Drugdispense drugdispense;

	//bi-directional many-to-one association to Drugreturn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DRUGRETURN_ID")
	private Drugreturn drugreturn;

	//bi-directional many-to-one association to Materialitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMMSTR_ID")
	private Materialitemmstr materialitemmstr;

	//bi-directional many-to-one association to Materialtxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALTXN_ID")
	private Materialtxn materialtxn;

	//bi-directional many-to-one association to Patientaccounttxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNTTXN_ID")
	private Patientaccounttxn patientaccounttxn;

	//bi-directional many-to-one association to Stockreceiptdetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKRECEIPTDETAIL_ID")
	private Stockreceiptdetail stockreceiptdetail;

	//bi-directional many-to-one association to Storebinmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREBINMSTR_ID")
	private Storebinmstr storebinmstr;

	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREMSTR_ID")
	private Storemstr storemstr;

	public Wardsspmstocktxn() {
	}

	public long getWardsspmstocktxnId() {
		return this.wardsspmstocktxnId;
	}

	public void setWardsspmstocktxnId(long wardsspmstocktxnId) {
		this.wardsspmstocktxnId = wardsspmstocktxnId;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMaterialtxnType() {
		return this.materialtxnType;
	}

	public void setMaterialtxnType(String materialtxnType) {
		this.materialtxnType = materialtxnType;
	}

	public String getRequestedInd() {
		return this.requestedInd;
	}

	public void setRequestedInd(String requestedInd) {
		this.requestedInd = requestedInd;
	}

	public BigDecimal getStocksetId() {
		return this.stocksetId;
	}

	public void setStocksetId(BigDecimal stocksetId) {
		this.stocksetId = stocksetId;
	}

	public BigDecimal getTxnQty() {
		return this.txnQty;
	}

	public void setTxnQty(BigDecimal txnQty) {
		this.txnQty = txnQty;
	}

	public String getTxnQtyUom() {
		return this.txnQtyUom;
	}

	public void setTxnQtyUom(String txnQtyUom) {
		this.txnQtyUom = txnQtyUom;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Drugdispense getDrugdispense() {
		return this.drugdispense;
	}

	public void setDrugdispense(Drugdispense drugdispense) {
		this.drugdispense = drugdispense;
	}

	public Drugreturn getDrugreturn() {
		return this.drugreturn;
	}

	public void setDrugreturn(Drugreturn drugreturn) {
		this.drugreturn = drugreturn;
	}

	public Materialitemmstr getMaterialitemmstr() {
		return this.materialitemmstr;
	}

	public void setMaterialitemmstr(Materialitemmstr materialitemmstr) {
		this.materialitemmstr = materialitemmstr;
	}

	public Materialtxn getMaterialtxn() {
		return this.materialtxn;
	}

	public void setMaterialtxn(Materialtxn materialtxn) {
		this.materialtxn = materialtxn;
	}

	public Patientaccounttxn getPatientaccounttxn() {
		return this.patientaccounttxn;
	}

	public void setPatientaccounttxn(Patientaccounttxn patientaccounttxn) {
		this.patientaccounttxn = patientaccounttxn;
	}

	public Stockreceiptdetail getStockreceiptdetail() {
		return this.stockreceiptdetail;
	}

	public void setStockreceiptdetail(Stockreceiptdetail stockreceiptdetail) {
		this.stockreceiptdetail = stockreceiptdetail;
	}

	public Storebinmstr getStorebinmstr() {
		return this.storebinmstr;
	}

	public void setStorebinmstr(Storebinmstr storebinmstr) {
		this.storebinmstr = storebinmstr;
	}

	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

}