package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BEDHISTORY database table.
 * 
 */
@Entity
@NamedQuery(name="Bedhistory.findAll", query="SELECT b FROM Bedhistory b")
public class Bedhistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BEDHISTORY_ID")
	private long bedhistoryId;

	@Column(name="ACCOMMMSTR_ID")
	private BigDecimal accommmstrId;

	@Column(name="BED_SUBSPECIALTYMSTR_ID")
	private BigDecimal bedSubspecialtymstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	private String diet;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_END_DATETIME")
	private Date effectiveEndDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_START_DATETIME")
	private Date effectiveStartDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENT_IND")
	private String patientInd;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="TRANSFER_REASON")
	private String transferReason;

	//bi-directional many-to-one association to Bedhistory
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TRANSFER_BEDHISTORY_ID")
	private Bedhistory bedhistory;

	//bi-directional many-to-one association to Bedhistory
	@OneToMany(mappedBy="bedhistory")
	private List<Bedhistory> bedhistories;

	//bi-directional many-to-one association to Bedmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BEDMSTR_ID")
	private Bedmstr bedmstr;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_WARDMSTR_ID")
	private Wardmstr wardmstr;

	//bi-directional many-to-one association to Charge
	@OneToMany(mappedBy="bedhistory")
	private List<Charge> charges;

	public Bedhistory() {
	}

	public long getBedhistoryId() {
		return this.bedhistoryId;
	}

	public void setBedhistoryId(long bedhistoryId) {
		this.bedhistoryId = bedhistoryId;
	}

	public BigDecimal getAccommmstrId() {
		return this.accommmstrId;
	}

	public void setAccommmstrId(BigDecimal accommmstrId) {
		this.accommmstrId = accommmstrId;
	}

	public BigDecimal getBedSubspecialtymstrId() {
		return this.bedSubspecialtymstrId;
	}

	public void setBedSubspecialtymstrId(BigDecimal bedSubspecialtymstrId) {
		this.bedSubspecialtymstrId = bedSubspecialtymstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDiet() {
		return this.diet;
	}

	public void setDiet(String diet) {
		this.diet = diet;
	}

	public Date getEffectiveEndDatetime() {
		return this.effectiveEndDatetime;
	}

	public void setEffectiveEndDatetime(Date effectiveEndDatetime) {
		this.effectiveEndDatetime = effectiveEndDatetime;
	}

	public Date getEffectiveStartDatetime() {
		return this.effectiveStartDatetime;
	}

	public void setEffectiveStartDatetime(Date effectiveStartDatetime) {
		this.effectiveStartDatetime = effectiveStartDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPatientInd() {
		return this.patientInd;
	}

	public void setPatientInd(String patientInd) {
		this.patientInd = patientInd;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getTransferReason() {
		return this.transferReason;
	}

	public void setTransferReason(String transferReason) {
		this.transferReason = transferReason;
	}

	public Bedhistory getBedhistory() {
		return this.bedhistory;
	}

	public void setBedhistory(Bedhistory bedhistory) {
		this.bedhistory = bedhistory;
	}

	public List<Bedhistory> getBedhistories() {
		return this.bedhistories;
	}

	public void setBedhistories(List<Bedhistory> bedhistories) {
		this.bedhistories = bedhistories;
	}

	public Bedhistory addBedhistory(Bedhistory bedhistory) {
		getBedhistories().add(bedhistory);
		bedhistory.setBedhistory(this);

		return bedhistory;
	}

	public Bedhistory removeBedhistory(Bedhistory bedhistory) {
		getBedhistories().remove(bedhistory);
		bedhistory.setBedhistory(null);

		return bedhistory;
	}

	public Bedmstr getBedmstr() {
		return this.bedmstr;
	}

	public void setBedmstr(Bedmstr bedmstr) {
		this.bedmstr = bedmstr;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

	public List<Charge> getCharges() {
		return this.charges;
	}

	public void setCharges(List<Charge> charges) {
		this.charges = charges;
	}

	public Charge addCharge(Charge charge) {
		getCharges().add(charge);
		charge.setBedhistory(this);

		return charge;
	}

	public Charge removeCharge(Charge charge) {
		getCharges().remove(charge);
		charge.setBedhistory(null);

		return charge;
	}

}