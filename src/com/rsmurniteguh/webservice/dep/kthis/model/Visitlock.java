package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the VISITLOCK database table.
 * 
 */
@Entity
@NamedQuery(name="Visitlock.findAll", query="SELECT v FROM Visitlock v")
public class Visitlock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISIT_ID")
	private long visitId;

	@Temporal(TemporalType.DATE)
	@Column(name="LOCK_TIMESTAMP")
	private Date lockTimestamp;

	public Visitlock() {
	}

	public long getVisitId() {
		return this.visitId;
	}

	public void setVisitId(long visitId) {
		this.visitId = visitId;
	}

	public Date getLockTimestamp() {
		return this.lockTimestamp;
	}

	public void setLockTimestamp(Date lockTimestamp) {
		this.lockTimestamp = lockTimestamp;
	}

}