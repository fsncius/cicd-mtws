package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the DIAGNOSISGROUPMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Diagnosisgroupmstr.findAll", query="SELECT d FROM Diagnosisgroupmstr d")
public class Diagnosisgroupmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DIAGNOSISGROUPMSTR_ID")
	private long diagnosisgroupmstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DIAGNOSISGROUP_CODE")
	private String diagnosisgroupCode;

	@Column(name="DIAGNOSISGROUP_DESC")
	private String diagnosisgroupDesc;

	@Column(name="DIAGNOSISGROUP_DESC_LANG1")
	private String diagnosisgroupDescLang1;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PARENT_ID")
	private BigDecimal parentId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	//bi-directional many-to-one association to Diagnosisgroupdetail
	@OneToMany(mappedBy="diagnosisgroupmstr")
	private List<Diagnosisgroupdetail> diagnosisgroupdetails;

	public Diagnosisgroupmstr() {
	}

	public long getDiagnosisgroupmstrId() {
		return this.diagnosisgroupmstrId;
	}

	public void setDiagnosisgroupmstrId(long diagnosisgroupmstrId) {
		this.diagnosisgroupmstrId = diagnosisgroupmstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDiagnosisgroupCode() {
		return this.diagnosisgroupCode;
	}

	public void setDiagnosisgroupCode(String diagnosisgroupCode) {
		this.diagnosisgroupCode = diagnosisgroupCode;
	}

	public String getDiagnosisgroupDesc() {
		return this.diagnosisgroupDesc;
	}

	public void setDiagnosisgroupDesc(String diagnosisgroupDesc) {
		this.diagnosisgroupDesc = diagnosisgroupDesc;
	}

	public String getDiagnosisgroupDescLang1() {
		return this.diagnosisgroupDescLang1;
	}

	public void setDiagnosisgroupDescLang1(String diagnosisgroupDescLang1) {
		this.diagnosisgroupDescLang1 = diagnosisgroupDescLang1;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getParentId() {
		return this.parentId;
	}

	public void setParentId(BigDecimal parentId) {
		this.parentId = parentId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public List<Diagnosisgroupdetail> getDiagnosisgroupdetails() {
		return this.diagnosisgroupdetails;
	}

	public void setDiagnosisgroupdetails(List<Diagnosisgroupdetail> diagnosisgroupdetails) {
		this.diagnosisgroupdetails = diagnosisgroupdetails;
	}

	public Diagnosisgroupdetail addDiagnosisgroupdetail(Diagnosisgroupdetail diagnosisgroupdetail) {
		getDiagnosisgroupdetails().add(diagnosisgroupdetail);
		diagnosisgroupdetail.setDiagnosisgroupmstr(this);

		return diagnosisgroupdetail;
	}

	public Diagnosisgroupdetail removeDiagnosisgroupdetail(Diagnosisgroupdetail diagnosisgroupdetail) {
		getDiagnosisgroupdetails().remove(diagnosisgroupdetail);
		diagnosisgroupdetail.setDiagnosisgroupmstr(null);

		return diagnosisgroupdetail;
	}

}