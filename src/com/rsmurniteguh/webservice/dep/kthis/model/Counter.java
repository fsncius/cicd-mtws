package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the COUNTER database table.
 * 
 */
@Entity
@NamedQuery(name="Counter.findAll", query="SELECT c FROM Counter c")
public class Counter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTER_ID")
	private long counterId;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="CLOSING_DATETIME")
	private Date closingDatetime;

	@Column(name="COUNTER_NO")
	private String counterNo;

	@Column(name="COUNTER_STATUS")
	private String counterStatus;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="HANDOVERGROUPMSTR_ID")
	private BigDecimal handovergroupmstrId;

	@Column(name="LAST_INVOICE_NO")
	private String lastInvoiceNo;

	@Column(name="LAST_RECEIPT_NO")
	private String lastReceiptNo;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="OPENING_DATETIME")
	private Date openingDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="TOTAL_AMOUNT")
	private BigDecimal totalAmount;

	@Column(name="USERMSTR_ID")
	private BigDecimal usermstrId;

	//bi-directional many-to-one association to Cardstatushistory
	@OneToMany(mappedBy="counter")
	private List<Cardstatushistory> cardstatushistories;

	//bi-directional many-to-one association to Countertotal
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COUNTERTOTAL_ID")
	private Countertotal countertotal;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to Countercollection
	@OneToMany(mappedBy="counter")
	private List<Countercollection> countercollections;

	//bi-directional many-to-one association to Countermodedetail
	@OneToMany(mappedBy="counter")
	private List<Countermodedetail> countermodedetails;

	public Counter() {
	}

	public long getCounterId() {
		return this.counterId;
	}

	public void setCounterId(long counterId) {
		this.counterId = counterId;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public Date getClosingDatetime() {
		return this.closingDatetime;
	}

	public void setClosingDatetime(Date closingDatetime) {
		this.closingDatetime = closingDatetime;
	}

	public String getCounterNo() {
		return this.counterNo;
	}

	public void setCounterNo(String counterNo) {
		this.counterNo = counterNo;
	}

	public String getCounterStatus() {
		return this.counterStatus;
	}

	public void setCounterStatus(String counterStatus) {
		this.counterStatus = counterStatus;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getHandovergroupmstrId() {
		return this.handovergroupmstrId;
	}

	public void setHandovergroupmstrId(BigDecimal handovergroupmstrId) {
		this.handovergroupmstrId = handovergroupmstrId;
	}

	public String getLastInvoiceNo() {
		return this.lastInvoiceNo;
	}

	public void setLastInvoiceNo(String lastInvoiceNo) {
		this.lastInvoiceNo = lastInvoiceNo;
	}

	public String getLastReceiptNo() {
		return this.lastReceiptNo;
	}

	public void setLastReceiptNo(String lastReceiptNo) {
		this.lastReceiptNo = lastReceiptNo;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public Date getOpeningDatetime() {
		return this.openingDatetime;
	}

	public void setOpeningDatetime(Date openingDatetime) {
		this.openingDatetime = openingDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getUsermstrId() {
		return this.usermstrId;
	}

	public void setUsermstrId(BigDecimal usermstrId) {
		this.usermstrId = usermstrId;
	}

	public List<Cardstatushistory> getCardstatushistories() {
		return this.cardstatushistories;
	}

	public void setCardstatushistories(List<Cardstatushistory> cardstatushistories) {
		this.cardstatushistories = cardstatushistories;
	}

	public Cardstatushistory addCardstatushistory(Cardstatushistory cardstatushistory) {
		getCardstatushistories().add(cardstatushistory);
		cardstatushistory.setCounter(this);

		return cardstatushistory;
	}

	public Cardstatushistory removeCardstatushistory(Cardstatushistory cardstatushistory) {
		getCardstatushistories().remove(cardstatushistory);
		cardstatushistory.setCounter(null);

		return cardstatushistory;
	}

	public Countertotal getCountertotal() {
		return this.countertotal;
	}

	public void setCountertotal(Countertotal countertotal) {
		this.countertotal = countertotal;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public List<Countercollection> getCountercollections() {
		return this.countercollections;
	}

	public void setCountercollections(List<Countercollection> countercollections) {
		this.countercollections = countercollections;
	}

	public Countercollection addCountercollection(Countercollection countercollection) {
		getCountercollections().add(countercollection);
		countercollection.setCounter(this);

		return countercollection;
	}

	public Countercollection removeCountercollection(Countercollection countercollection) {
		getCountercollections().remove(countercollection);
		countercollection.setCounter(null);

		return countercollection;
	}

	public List<Countermodedetail> getCountermodedetails() {
		return this.countermodedetails;
	}

	public void setCountermodedetails(List<Countermodedetail> countermodedetails) {
		this.countermodedetails = countermodedetails;
	}

	public Countermodedetail addCountermodedetail(Countermodedetail countermodedetail) {
		getCountermodedetails().add(countermodedetail);
		countermodedetail.setCounter(this);

		return countermodedetail;
	}

	public Countermodedetail removeCountermodedetail(Countermodedetail countermodedetail) {
		getCountermodedetails().remove(countermodedetail);
		countermodedetail.setCounter(null);

		return countermodedetail;
	}

}