package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DEPOSIT database table.
 * 
 */
@Entity
@NamedQuery(name="Deposit.findAll", query="SELECT d FROM Deposit d")
public class Deposit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DEPOSIT_ID")
	private long depositId;

	@Column(name="AUTHORISATION_CODE")
	private String authorisationCode;

	@Column(name="BANK_ACCOUNT_NO")
	private String bankAccountNo;

	@Column(name="BANK_CODE")
	private String bankCode;

	@Column(name="CANCEL_REASON")
	private String cancelReason;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="CARD_EXPIRY_DATE")
	private Date cardExpiryDate;

	@Column(name="COLLECTED_AMOUNT")
	private BigDecimal collectedAmount;

	@Column(name="COLLECTION_MODE")
	private String collectionMode;

	@Column(name="CURRENCY_CODE")
	private String currencyCode;

	@Column(name="DEPOSIT_STATUS")
	private String depositStatus;

	@Column(name="DEPOSIT_USAGE")
	private String depositUsage;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATETIME")
	private Date enteredDatetime;

	@Column(name="EXCHANGE_RATE")
	private BigDecimal exchangeRate;

	@Column(name="FOREIGN_AMOUNT")
	private BigDecimal foreignAmount;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PAYEE_ADDRESS")
	private String payeeAddress;

	@Column(name="PAYEE_NAME")
	private String payeeName;

	@Temporal(TemporalType.DATE)
	@Column(name="REFERENCE_DATE")
	private Date referenceDate;

	@Column(name="REFERENCE_NO")
	private String referenceNo;

	private String remarks;

	@Column(name="STAKEHOLDER_ID")
	private BigDecimal stakeholderId;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CANCELLED_LOCATIONMSTR_ID")
	private Locationmstr locationmstr1;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ENTERED_LOCATIONMSTR_ID")
	private Locationmstr locationmstr2;

	//bi-directional many-to-one association to Patientaccounttxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CANCELLED_PAT_ID")
	private Patientaccounttxn patientaccounttxn1;

	//bi-directional many-to-one association to Patientaccounttxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ENTERED_PAT_ID")
	private Patientaccounttxn patientaccounttxn2;

	//bi-directional many-to-one association to Receipt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="RECEIPT_ID")
	private Receipt receipt;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public Deposit() {
	}

	public long getDepositId() {
		return this.depositId;
	}

	public void setDepositId(long depositId) {
		this.depositId = depositId;
	}

	public String getAuthorisationCode() {
		return this.authorisationCode;
	}

	public void setAuthorisationCode(String authorisationCode) {
		this.authorisationCode = authorisationCode;
	}

	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getCancelReason() {
		return this.cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public Date getCardExpiryDate() {
		return this.cardExpiryDate;
	}

	public void setCardExpiryDate(Date cardExpiryDate) {
		this.cardExpiryDate = cardExpiryDate;
	}

	public BigDecimal getCollectedAmount() {
		return this.collectedAmount;
	}

	public void setCollectedAmount(BigDecimal collectedAmount) {
		this.collectedAmount = collectedAmount;
	}

	public String getCollectionMode() {
		return this.collectionMode;
	}

	public void setCollectionMode(String collectionMode) {
		this.collectionMode = collectionMode;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDepositStatus() {
		return this.depositStatus;
	}

	public void setDepositStatus(String depositStatus) {
		this.depositStatus = depositStatus;
	}

	public String getDepositUsage() {
		return this.depositUsage;
	}

	public void setDepositUsage(String depositUsage) {
		this.depositUsage = depositUsage;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredDatetime() {
		return this.enteredDatetime;
	}

	public void setEnteredDatetime(Date enteredDatetime) {
		this.enteredDatetime = enteredDatetime;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public BigDecimal getForeignAmount() {
		return this.foreignAmount;
	}

	public void setForeignAmount(BigDecimal foreignAmount) {
		this.foreignAmount = foreignAmount;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPayeeAddress() {
		return this.payeeAddress;
	}

	public void setPayeeAddress(String payeeAddress) {
		this.payeeAddress = payeeAddress;
	}

	public String getPayeeName() {
		return this.payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public Date getReferenceDate() {
		return this.referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	public String getReferenceNo() {
		return this.referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStakeholderId() {
		return this.stakeholderId;
	}

	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
	}

	public Locationmstr getLocationmstr1() {
		return this.locationmstr1;
	}

	public void setLocationmstr1(Locationmstr locationmstr1) {
		this.locationmstr1 = locationmstr1;
	}

	public Locationmstr getLocationmstr2() {
		return this.locationmstr2;
	}

	public void setLocationmstr2(Locationmstr locationmstr2) {
		this.locationmstr2 = locationmstr2;
	}

	public Patientaccounttxn getPatientaccounttxn1() {
		return this.patientaccounttxn1;
	}

	public void setPatientaccounttxn1(Patientaccounttxn patientaccounttxn1) {
		this.patientaccounttxn1 = patientaccounttxn1;
	}

	public Patientaccounttxn getPatientaccounttxn2() {
		return this.patientaccounttxn2;
	}

	public void setPatientaccounttxn2(Patientaccounttxn patientaccounttxn2) {
		this.patientaccounttxn2 = patientaccounttxn2;
	}

	public Receipt getReceipt() {
		return this.receipt;
	}

	public void setReceipt(Receipt receipt) {
		this.receipt = receipt;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}