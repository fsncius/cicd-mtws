package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the CHARGEPLANSPECIALTIMES database table.
 * 
 */
@Entity
@Table(name="CHARGEPLANSPECIALTIMES")
@NamedQuery(name="Chargeplanspecialtime.findAll", query="SELECT c FROM Chargeplanspecialtime c")
public class Chargeplanspecialtime implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGEPLANSPECIALTIMES_ID")
	private long chargeplanspecialtimesId;

	@Column(name="FRI_CHARGE_IND")
	private String friChargeInd;

	@Column(name="MON_CHARGE_IND")
	private String monChargeInd;

	@Column(name="SAT_CHARGE_IND")
	private String satChargeInd;

	@Column(name="SUN_CHARGE_IND")
	private String sunChargeInd;

	@Column(name="THU_CHARGE_IND")
	private String thuChargeInd;

	@Column(name="TUE_CHARGE_IND")
	private String tueChargeInd;

	@Column(name="WED_CHARGE_IND")
	private String wedChargeInd;

	//bi-directional many-to-one association to Chargerollingplan
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEROLLINGPLAN_ID")
	private Chargerollingplan chargerollingplan;

	public Chargeplanspecialtime() {
	}

	public long getChargeplanspecialtimesId() {
		return this.chargeplanspecialtimesId;
	}

	public void setChargeplanspecialtimesId(long chargeplanspecialtimesId) {
		this.chargeplanspecialtimesId = chargeplanspecialtimesId;
	}

	public String getFriChargeInd() {
		return this.friChargeInd;
	}

	public void setFriChargeInd(String friChargeInd) {
		this.friChargeInd = friChargeInd;
	}

	public String getMonChargeInd() {
		return this.monChargeInd;
	}

	public void setMonChargeInd(String monChargeInd) {
		this.monChargeInd = monChargeInd;
	}

	public String getSatChargeInd() {
		return this.satChargeInd;
	}

	public void setSatChargeInd(String satChargeInd) {
		this.satChargeInd = satChargeInd;
	}

	public String getSunChargeInd() {
		return this.sunChargeInd;
	}

	public void setSunChargeInd(String sunChargeInd) {
		this.sunChargeInd = sunChargeInd;
	}

	public String getThuChargeInd() {
		return this.thuChargeInd;
	}

	public void setThuChargeInd(String thuChargeInd) {
		this.thuChargeInd = thuChargeInd;
	}

	public String getTueChargeInd() {
		return this.tueChargeInd;
	}

	public void setTueChargeInd(String tueChargeInd) {
		this.tueChargeInd = tueChargeInd;
	}

	public String getWedChargeInd() {
		return this.wedChargeInd;
	}

	public void setWedChargeInd(String wedChargeInd) {
		this.wedChargeInd = wedChargeInd;
	}

	public Chargerollingplan getChargerollingplan() {
		return this.chargerollingplan;
	}

	public void setChargerollingplan(Chargerollingplan chargerollingplan) {
		this.chargerollingplan = chargerollingplan;
	}

}