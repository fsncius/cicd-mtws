package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the STOREDAILYSUMMARY database table.
 * 
 */
@Entity
@NamedQuery(name="Storedailysummary.findAll", query="SELECT s FROM Storedailysummary s")
public class Storedailysummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOREDAILYSUMMARY_ID")
	private long storedailysummaryId;

	@Temporal(TemporalType.DATE)
	@Column(name="BALANCE_DATETIME")
	private Date balanceDatetime;

	@Column(name="BALANCE_TOTAL_COST")
	private BigDecimal balanceTotalCost;

	@Column(name="BALANCE_TOTAL_PRICE")
	private BigDecimal balanceTotalPrice;

	@Column(name="BALANCE_TOTAL_WHOLESALE_PRICE")
	private BigDecimal balanceTotalWholesalePrice;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="ISSUE_TOTAL_COST")
	private BigDecimal issueTotalCost;

	@Column(name="ISSUE_TOTAL_PRICE")
	private BigDecimal issueTotalPrice;

	@Column(name="ISSUE_TOTAL_WHOLESALE_PRICE")
	private BigDecimal issueTotalWholesalePrice;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="RECEIPT_ISSUE_DATE")
	private Date receiptIssueDate;

	@Column(name="RECEIPT_TOTAL_COST")
	private BigDecimal receiptTotalCost;

	@Column(name="RECEIPT_TOTAL_PRICE")
	private BigDecimal receiptTotalPrice;

	@Column(name="RECEIPT_TOTAL_WHOLESALE_PRICE")
	private BigDecimal receiptTotalWholesalePrice;

	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREMSTR_ID")
	private Storemstr storemstr;

	public Storedailysummary() {
	}

	public long getStoredailysummaryId() {
		return this.storedailysummaryId;
	}

	public void setStoredailysummaryId(long storedailysummaryId) {
		this.storedailysummaryId = storedailysummaryId;
	}

	public Date getBalanceDatetime() {
		return this.balanceDatetime;
	}

	public void setBalanceDatetime(Date balanceDatetime) {
		this.balanceDatetime = balanceDatetime;
	}

	public BigDecimal getBalanceTotalCost() {
		return this.balanceTotalCost;
	}

	public void setBalanceTotalCost(BigDecimal balanceTotalCost) {
		this.balanceTotalCost = balanceTotalCost;
	}

	public BigDecimal getBalanceTotalPrice() {
		return this.balanceTotalPrice;
	}

	public void setBalanceTotalPrice(BigDecimal balanceTotalPrice) {
		this.balanceTotalPrice = balanceTotalPrice;
	}

	public BigDecimal getBalanceTotalWholesalePrice() {
		return this.balanceTotalWholesalePrice;
	}

	public void setBalanceTotalWholesalePrice(BigDecimal balanceTotalWholesalePrice) {
		this.balanceTotalWholesalePrice = balanceTotalWholesalePrice;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getIssueTotalCost() {
		return this.issueTotalCost;
	}

	public void setIssueTotalCost(BigDecimal issueTotalCost) {
		this.issueTotalCost = issueTotalCost;
	}

	public BigDecimal getIssueTotalPrice() {
		return this.issueTotalPrice;
	}

	public void setIssueTotalPrice(BigDecimal issueTotalPrice) {
		this.issueTotalPrice = issueTotalPrice;
	}

	public BigDecimal getIssueTotalWholesalePrice() {
		return this.issueTotalWholesalePrice;
	}

	public void setIssueTotalWholesalePrice(BigDecimal issueTotalWholesalePrice) {
		this.issueTotalWholesalePrice = issueTotalWholesalePrice;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public Date getReceiptIssueDate() {
		return this.receiptIssueDate;
	}

	public void setReceiptIssueDate(Date receiptIssueDate) {
		this.receiptIssueDate = receiptIssueDate;
	}

	public BigDecimal getReceiptTotalCost() {
		return this.receiptTotalCost;
	}

	public void setReceiptTotalCost(BigDecimal receiptTotalCost) {
		this.receiptTotalCost = receiptTotalCost;
	}

	public BigDecimal getReceiptTotalPrice() {
		return this.receiptTotalPrice;
	}

	public void setReceiptTotalPrice(BigDecimal receiptTotalPrice) {
		this.receiptTotalPrice = receiptTotalPrice;
	}

	public BigDecimal getReceiptTotalWholesalePrice() {
		return this.receiptTotalWholesalePrice;
	}

	public void setReceiptTotalWholesalePrice(BigDecimal receiptTotalWholesalePrice) {
		this.receiptTotalWholesalePrice = receiptTotalWholesalePrice;
	}

	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

}