package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VENDORITEM database table.
 * 
 */
@Entity
@NamedQuery(name="Vendoritem.findAll", query="SELECT v FROM Vendoritem v")
public class Vendoritem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VENDORITEM_ID")
	private long vendoritemId;

	@Column(name="DEFAULT_ORDER_QTY")
	private BigDecimal defaultOrderQty;

	@Column(name="DEFAULT_PURCHASE_IND")
	private String defaultPurchaseInd;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="FIRST_TIME_ORDER_IND")
	private String firstTimeOrderInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MANUFACTURER_ITEM_CODE")
	private String manufacturerItemCode;

	@Column(name="MAX_ORDER_QTY")
	private BigDecimal maxOrderQty;

	@Column(name="MIN_ORDER_QTY")
	private BigDecimal minOrderQty;

	@Column(name="ORDER_PROCESSING_DAYS")
	private BigDecimal orderProcessingDays;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PURCHASE_UOM")
	private String purchaseUom;

	@Column(name="VENDOR_ITEM_CODE")
	private String vendorItemCode;

	//bi-directional many-to-one association to Materialitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMMSTR_ID")
	private Materialitemmstr materialitemmstr;

	//bi-directional many-to-one association to Vendormstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VENDORMSTR_ID")
	private Vendormstr vendormstr;

	public Vendoritem() {
	}

	public long getVendoritemId() {
		return this.vendoritemId;
	}

	public void setVendoritemId(long vendoritemId) {
		this.vendoritemId = vendoritemId;
	}

	public BigDecimal getDefaultOrderQty() {
		return this.defaultOrderQty;
	}

	public void setDefaultOrderQty(BigDecimal defaultOrderQty) {
		this.defaultOrderQty = defaultOrderQty;
	}

	public String getDefaultPurchaseInd() {
		return this.defaultPurchaseInd;
	}

	public void setDefaultPurchaseInd(String defaultPurchaseInd) {
		this.defaultPurchaseInd = defaultPurchaseInd;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getFirstTimeOrderInd() {
		return this.firstTimeOrderInd;
	}

	public void setFirstTimeOrderInd(String firstTimeOrderInd) {
		this.firstTimeOrderInd = firstTimeOrderInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getManufacturerItemCode() {
		return this.manufacturerItemCode;
	}

	public void setManufacturerItemCode(String manufacturerItemCode) {
		this.manufacturerItemCode = manufacturerItemCode;
	}

	public BigDecimal getMaxOrderQty() {
		return this.maxOrderQty;
	}

	public void setMaxOrderQty(BigDecimal maxOrderQty) {
		this.maxOrderQty = maxOrderQty;
	}

	public BigDecimal getMinOrderQty() {
		return this.minOrderQty;
	}

	public void setMinOrderQty(BigDecimal minOrderQty) {
		this.minOrderQty = minOrderQty;
	}

	public BigDecimal getOrderProcessingDays() {
		return this.orderProcessingDays;
	}

	public void setOrderProcessingDays(BigDecimal orderProcessingDays) {
		this.orderProcessingDays = orderProcessingDays;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPurchaseUom() {
		return this.purchaseUom;
	}

	public void setPurchaseUom(String purchaseUom) {
		this.purchaseUom = purchaseUom;
	}

	public String getVendorItemCode() {
		return this.vendorItemCode;
	}

	public void setVendorItemCode(String vendorItemCode) {
		this.vendorItemCode = vendorItemCode;
	}

	public Materialitemmstr getMaterialitemmstr() {
		return this.materialitemmstr;
	}

	public void setMaterialitemmstr(Materialitemmstr materialitemmstr) {
		this.materialitemmstr = materialitemmstr;
	}

	public Vendormstr getVendormstr() {
		return this.vendormstr;
	}

	public void setVendormstr(Vendormstr vendormstr) {
		this.vendormstr = vendormstr;
	}

}