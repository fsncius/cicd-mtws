package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKCOUNT database table.
 * 
 */
@Entity
@NamedQuery(name="Stockcount.findAll", query="SELECT s FROM Stockcount s")
public class Stockcount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKCOUNT_ID")
	private long stockcountId;

	@Column(name="ADJUST_BY")
	private BigDecimal adjustBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ADJUST_DATETIME")
	private Date adjustDatetime;

	@Column(name="APPROVED_BY")
	private BigDecimal approvedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="APPROVED_DATETIME")
	private Date approvedDatetime;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="COUNT_NO")
	private String countNo;

	@Column(name="COUNT_STATUS")
	private String countStatus;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREMSTR_ID")
	private Storemstr storemstr;

	//bi-directional many-to-one association to Stockcountdetail
	@OneToMany(mappedBy="stockcount")
	private List<Stockcountdetail> stockcountdetails;

	public Stockcount() {
	}

	public long getStockcountId() {
		return this.stockcountId;
	}

	public void setStockcountId(long stockcountId) {
		this.stockcountId = stockcountId;
	}

	public BigDecimal getAdjustBy() {
		return this.adjustBy;
	}

	public void setAdjustBy(BigDecimal adjustBy) {
		this.adjustBy = adjustBy;
	}

	public Date getAdjustDatetime() {
		return this.adjustDatetime;
	}

	public void setAdjustDatetime(Date adjustDatetime) {
		this.adjustDatetime = adjustDatetime;
	}

	public BigDecimal getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(BigDecimal approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedDatetime() {
		return this.approvedDatetime;
	}

	public void setApprovedDatetime(Date approvedDatetime) {
		this.approvedDatetime = approvedDatetime;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public String getCountNo() {
		return this.countNo;
	}

	public void setCountNo(String countNo) {
		this.countNo = countNo;
	}

	public String getCountStatus() {
		return this.countStatus;
	}

	public void setCountStatus(String countStatus) {
		this.countStatus = countStatus;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

	public List<Stockcountdetail> getStockcountdetails() {
		return this.stockcountdetails;
	}

	public void setStockcountdetails(List<Stockcountdetail> stockcountdetails) {
		this.stockcountdetails = stockcountdetails;
	}

	public Stockcountdetail addStockcountdetail(Stockcountdetail stockcountdetail) {
		getStockcountdetails().add(stockcountdetail);
		stockcountdetail.setStockcount(this);

		return stockcountdetail;
	}

	public Stockcountdetail removeStockcountdetail(Stockcountdetail stockcountdetail) {
		getStockcountdetails().remove(stockcountdetail);
		stockcountdetail.setStockcount(null);

		return stockcountdetail;
	}

}