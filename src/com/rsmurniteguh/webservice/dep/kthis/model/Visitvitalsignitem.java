package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITVITALSIGNITEM database table.
 * 
 */
@Entity
@NamedQuery(name="Visitvitalsignitem.findAll", query="SELECT v FROM Visitvitalsignitem v")
public class Visitvitalsignitem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITVITALSIGNITEM_ID")
	private long visitvitalsignitemId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Vitalsignitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VITALSIGNITEMMSTR_ID")
	private Vitalsignitemmstr vitalsignitemmstr;

	public Visitvitalsignitem() {
	}

	public long getVisitvitalsignitemId() {
		return this.visitvitalsignitemId;
	}

	public void setVisitvitalsignitemId(long visitvitalsignitemId) {
		this.visitvitalsignitemId = visitvitalsignitemId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public Vitalsignitemmstr getVitalsignitemmstr() {
		return this.vitalsignitemmstr;
	}

	public void setVitalsignitemmstr(Vitalsignitemmstr vitalsignitemmstr) {
		this.vitalsignitemmstr = vitalsignitemmstr;
	}

}