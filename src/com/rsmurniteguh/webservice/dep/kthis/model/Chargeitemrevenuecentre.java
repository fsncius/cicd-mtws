package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CHARGEITEMREVENUECENTRE database table.
 * 
 */
@Entity
@NamedQuery(name="Chargeitemrevenuecentre.findAll", query="SELECT c FROM Chargeitemrevenuecentre c")
public class Chargeitemrevenuecentre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGEITEMREVENUECENTRE_ID")
	private long chargeitemrevenuecentreId;

	@Column(name="CHARGEITEMMSTR_ID")
	private BigDecimal chargeitemmstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenuecentremstrId;

	public Chargeitemrevenuecentre() {
	}

	public long getChargeitemrevenuecentreId() {
		return this.chargeitemrevenuecentreId;
	}

	public void setChargeitemrevenuecentreId(long chargeitemrevenuecentreId) {
		this.chargeitemrevenuecentreId = chargeitemrevenuecentreId;
	}

	public BigDecimal getChargeitemmstrId() {
		return this.chargeitemmstrId;
	}

	public void setChargeitemmstrId(BigDecimal chargeitemmstrId) {
		this.chargeitemmstrId = chargeitemmstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getRevenuecentremstrId() {
		return this.revenuecentremstrId;
	}

	public void setRevenuecentremstrId(BigDecimal revenuecentremstrId) {
		this.revenuecentremstrId = revenuecentremstrId;
	}

}