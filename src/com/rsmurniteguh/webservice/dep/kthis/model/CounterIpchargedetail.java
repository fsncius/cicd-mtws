package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the COUNTER_IPCHARGEDETAIL database table.
 * 
 */
@Entity
@Table(name="COUNTER_IPCHARGEDETAIL")
@NamedQuery(name="CounterIpchargedetail.findAll", query="SELECT c FROM CounterIpchargedetail c")
public class CounterIpchargedetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTER_IPCHARGEDETAIL_ID")
	private long counterIpchargedetailId;

	private BigDecimal actualamount;

	private BigDecimal amount;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DATA_CAT")
	private String dataCat;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="ITEMSUBCATEGORYMSTR_ID")
	private BigDecimal itemsubcategorymstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	private BigDecimal refundamount;

	//bi-directional many-to-one association to Countertotal
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COUNTERTOTAL_ID")
	private Countertotal countertotal;

	public CounterIpchargedetail() {
	}

	public long getCounterIpchargedetailId() {
		return this.counterIpchargedetailId;
	}

	public void setCounterIpchargedetailId(long counterIpchargedetailId) {
		this.counterIpchargedetailId = counterIpchargedetailId;
	}

	public BigDecimal getActualamount() {
		return this.actualamount;
	}

	public void setActualamount(BigDecimal actualamount) {
		this.actualamount = actualamount;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDataCat() {
		return this.dataCat;
	}

	public void setDataCat(String dataCat) {
		this.dataCat = dataCat;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getItemsubcategorymstrId() {
		return this.itemsubcategorymstrId;
	}

	public void setItemsubcategorymstrId(BigDecimal itemsubcategorymstrId) {
		this.itemsubcategorymstrId = itemsubcategorymstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getRefundamount() {
		return this.refundamount;
	}

	public void setRefundamount(BigDecimal refundamount) {
		this.refundamount = refundamount;
	}

	public Countertotal getCountertotal() {
		return this.countertotal;
	}

	public void setCountertotal(Countertotal countertotal) {
		this.countertotal = countertotal;
	}

}