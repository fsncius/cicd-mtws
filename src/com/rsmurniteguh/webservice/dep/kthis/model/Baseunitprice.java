package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BASEUNITPRICE database table.
 * 
 */
@Entity
@NamedQuery(name="Baseunitprice.findAll", query="SELECT b FROM Baseunitprice b")
public class Baseunitprice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BASEUNITPRICE_ID")
	private long baseunitpriceId;

	@Temporal(TemporalType.DATE)
	@Column(name="ADJUSTED_DATETIME")
	private Date adjustedDatetime;

	@Column(name="ADJUSTMENT_REASON")
	private String adjustmentReason;

	@Column(name="ADJUSTMENT_REMARKS")
	private String adjustmentRemarks;

	@Column(name="BASE_IND")
	private String baseInd;

	@Column(name="BASE_UNIT_PRICE")
	private BigDecimal baseUnitPrice;

	@Column(name="BASE_UNIT_PRICE_TYPE")
	private String baseUnitPriceType;

	@Column(name="CAREPROVIDER_GRADE")
	private String careproviderGrade;

	@Column(name="COST_PRICE")
	private BigDecimal costPrice;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATETIME")
	private Date effectiveDatetime;

	@Column(name="ENTITYMSTR_ID")
	private BigDecimal entitymstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATETIME")
	private Date expiryDatetime;

	@Column(name="FLEXI_PACKAGE_EXCESS")
	private BigDecimal flexiPackageExcess;

	@Column(name="FLEXI_PKG_CHARGEITEMMSTR_ID")
	private BigDecimal flexiPkgChargeitemmstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MAX_BASE_PRICE")
	private BigDecimal maxBasePrice;

	@Column(name="MIN_BASE_PRICE")
	private BigDecimal minBasePrice;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="QTY_UOM")
	private String qtyUom;

	private String remarks;

	@Column(name="WHOLESALE_PRICE")
	private BigDecimal wholesalePrice;

	//bi-directional many-to-one association to Chargeitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEITEMMSTR_ID")
	private Chargeitemmstr chargeitemmstr;

	//bi-directional many-to-one association to Baseunitpricehistory
	@OneToMany(mappedBy="baseunitprice")
	private List<Baseunitpricehistory> baseunitpricehistories;

	public Baseunitprice() {
	}

	public long getBaseunitpriceId() {
		return this.baseunitpriceId;
	}

	public void setBaseunitpriceId(long baseunitpriceId) {
		this.baseunitpriceId = baseunitpriceId;
	}

	public Date getAdjustedDatetime() {
		return this.adjustedDatetime;
	}

	public void setAdjustedDatetime(Date adjustedDatetime) {
		this.adjustedDatetime = adjustedDatetime;
	}

	public String getAdjustmentReason() {
		return this.adjustmentReason;
	}

	public void setAdjustmentReason(String adjustmentReason) {
		this.adjustmentReason = adjustmentReason;
	}

	public String getAdjustmentRemarks() {
		return this.adjustmentRemarks;
	}

	public void setAdjustmentRemarks(String adjustmentRemarks) {
		this.adjustmentRemarks = adjustmentRemarks;
	}

	public String getBaseInd() {
		return this.baseInd;
	}

	public void setBaseInd(String baseInd) {
		this.baseInd = baseInd;
	}

	public BigDecimal getBaseUnitPrice() {
		return this.baseUnitPrice;
	}

	public void setBaseUnitPrice(BigDecimal baseUnitPrice) {
		this.baseUnitPrice = baseUnitPrice;
	}

	public String getBaseUnitPriceType() {
		return this.baseUnitPriceType;
	}

	public void setBaseUnitPriceType(String baseUnitPriceType) {
		this.baseUnitPriceType = baseUnitPriceType;
	}

	public String getCareproviderGrade() {
		return this.careproviderGrade;
	}

	public void setCareproviderGrade(String careproviderGrade) {
		this.careproviderGrade = careproviderGrade;
	}

	public BigDecimal getCostPrice() {
		return this.costPrice;
	}

	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffectiveDatetime() {
		return this.effectiveDatetime;
	}

	public void setEffectiveDatetime(Date effectiveDatetime) {
		this.effectiveDatetime = effectiveDatetime;
	}

	public BigDecimal getEntitymstrId() {
		return this.entitymstrId;
	}

	public void setEntitymstrId(BigDecimal entitymstrId) {
		this.entitymstrId = entitymstrId;
	}

	public Date getExpiryDatetime() {
		return this.expiryDatetime;
	}

	public void setExpiryDatetime(Date expiryDatetime) {
		this.expiryDatetime = expiryDatetime;
	}

	public BigDecimal getFlexiPackageExcess() {
		return this.flexiPackageExcess;
	}

	public void setFlexiPackageExcess(BigDecimal flexiPackageExcess) {
		this.flexiPackageExcess = flexiPackageExcess;
	}

	public BigDecimal getFlexiPkgChargeitemmstrId() {
		return this.flexiPkgChargeitemmstrId;
	}

	public void setFlexiPkgChargeitemmstrId(BigDecimal flexiPkgChargeitemmstrId) {
		this.flexiPkgChargeitemmstrId = flexiPkgChargeitemmstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaxBasePrice() {
		return this.maxBasePrice;
	}

	public void setMaxBasePrice(BigDecimal maxBasePrice) {
		this.maxBasePrice = maxBasePrice;
	}

	public BigDecimal getMinBasePrice() {
		return this.minBasePrice;
	}

	public void setMinBasePrice(BigDecimal minBasePrice) {
		this.minBasePrice = minBasePrice;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getQtyUom() {
		return this.qtyUom;
	}

	public void setQtyUom(String qtyUom) {
		this.qtyUom = qtyUom;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getWholesalePrice() {
		return this.wholesalePrice;
	}

	public void setWholesalePrice(BigDecimal wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}

	public Chargeitemmstr getChargeitemmstr() {
		return this.chargeitemmstr;
	}

	public void setChargeitemmstr(Chargeitemmstr chargeitemmstr) {
		this.chargeitemmstr = chargeitemmstr;
	}

	public List<Baseunitpricehistory> getBaseunitpricehistories() {
		return this.baseunitpricehistories;
	}

	public void setBaseunitpricehistories(List<Baseunitpricehistory> baseunitpricehistories) {
		this.baseunitpricehistories = baseunitpricehistories;
	}

	public Baseunitpricehistory addBaseunitpricehistory(Baseunitpricehistory baseunitpricehistory) {
		getBaseunitpricehistories().add(baseunitpricehistory);
		baseunitpricehistory.setBaseunitprice(this);

		return baseunitpricehistory;
	}

	public Baseunitpricehistory removeBaseunitpricehistory(Baseunitpricehistory baseunitpricehistory) {
		getBaseunitpricehistories().remove(baseunitpricehistory);
		baseunitpricehistory.setBaseunitprice(null);

		return baseunitpricehistory;
	}

}