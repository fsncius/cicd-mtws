package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ACCOUNTDEBTOR database table.
 * 
 */
@Entity
@NamedQuery(name="Accountdebtor.findAll", query="SELECT a FROM Accountdebtor a")
public class Accountdebtor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ACCOUNTDEBTOR_ID")
	private BigDecimal accountdebtorId;

	@Column(name="CANCEL_REASON")
	private String cancelReason;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CLAIM_NO")
	private String claimNo;

	@Column(name="CLAIM_POLICY_CODE")
	private String claimPolicyCode;

	@Column(name="CN_APPOINTED_HOSPITAL_IND")
	private String cnAppointedHospitalInd;

	@Column(name="CN_HEALTHCARE_PLAN_IND")
	private String cnHealthcarePlanInd;

	@Column(name="CN_HEALTHCARE_PLAN_NO")
	private String cnHealthcarePlanNo;

	@Column(name="CN_INDUSTRIAL_SICKNESS")
	private String cnIndustrialSickness;

	@Column(name="CN_LAST_CLAIM_AMOUNT")
	private BigDecimal cnLastClaimAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="CN_LAST_CLAIM_DATE")
	private Date cnLastClaimDate;

	@Column(name="CN_MAJOR_DISEASE_IND")
	private String cnMajorDiseaseInd;

	@Column(name="CN_MAJOR_DISEASE_TYPE")
	private String cnMajorDiseaseType;

	@Column(name="CN_MIO_INSURANCE")
	private String cnMioInsurance;

	@Column(name="CN_MIO_METHOD")
	private String cnMioMethod;

	@Column(name="CN_MIO_PERSON_GROUP")
	private String cnMioPersonGroup;

	@Column(name="CN_SELF_PAYABLE_AMOUNT")
	private BigDecimal cnSelfPayableAmount;

	@Column(name="CN_YTD_SELFPAID_AMOUNT")
	private BigDecimal cnYtdSelfpaidAmount;

	@Column(name="CONCESSION_CODE")
	private String concessionCode;

	@Column(name="CONTRACT_BALANCE")
	private BigDecimal contractBalance;

	@Column(name="CONTRACT_NO")
	private String contractNo;

	@Column(name="CONTRACT_STATUS")
	private String contractStatus;

	@Column(name="CONTRACT_TYPE")
	private String contractType;

	@Column(name="DEBTOR_CHANGED_IND")
	private String debtorChangedInd;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="FINANCIAL_CLASS")
	private String financialClass;

	@Column(name="FROZEN_CONTRACT_BALANCE")
	private BigDecimal frozenContractBalance;

	@Column(name="FROZEN_CONTRACT_SIZE")
	private BigDecimal frozenContractSize;

	@Temporal(TemporalType.DATE)
	@Column(name="FROZEN_DATETIME")
	private Date frozenDatetime;

	@Column(name="GUARANTEELETTER_ID")
	private BigDecimal guaranteeletterId;

	@Column(name="LAST_BILL_AMOUNT")
	private BigDecimal lastBillAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_BILL_DATE")
	private Date lastBillDate;

	@Column(name="LAST_BILL_NO")
	private String lastBillNo;

	@Column(name="LAST_BILL_TYPE")
	private String lastBillType;

	@Column(name="LAST_SUPPLEMENT_NO")
	private String lastSupplementNo;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PAID_AMOUNT")
	private BigDecimal paidAmount;

	@Column(name="PAYER_SEQ")
	private BigDecimal payerSeq;

	@Column(name="POLICY_NO")
	private String policyNo;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String relationship;

	private String remarks;

	@Column(name="STAKEHOLDER_ID")
	private BigDecimal stakeholderId;

	@Column(name="YTD_CLAIMED_AMOUNT")
	private BigDecimal ytdClaimedAmount;

	//bi-directional many-to-one association to Patientaccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNT_ID")
	private Patientaccount patientaccount;
	
	private BigDecimal patientAccountId;

	//bi-directional many-to-one association to Billaccount
	@OneToMany(mappedBy="accountdebtor")
	private List<Billaccount> billaccounts;

	public Accountdebtor() {
	}

	public BigDecimal getAccountdebtorId() {
		return this.accountdebtorId;
	}

	public void setAccountdebtorId(BigDecimal accountdebtorId) {
		this.accountdebtorId = accountdebtorId;
	}

	public String getCancelReason() {
		return this.cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public String getClaimNo() {
		return this.claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	public String getClaimPolicyCode() {
		return this.claimPolicyCode;
	}

	public void setClaimPolicyCode(String claimPolicyCode) {
		this.claimPolicyCode = claimPolicyCode;
	}

	public String getCnAppointedHospitalInd() {
		return this.cnAppointedHospitalInd;
	}

	public void setCnAppointedHospitalInd(String cnAppointedHospitalInd) {
		this.cnAppointedHospitalInd = cnAppointedHospitalInd;
	}

	public String getCnHealthcarePlanInd() {
		return this.cnHealthcarePlanInd;
	}

	public void setCnHealthcarePlanInd(String cnHealthcarePlanInd) {
		this.cnHealthcarePlanInd = cnHealthcarePlanInd;
	}

	public String getCnHealthcarePlanNo() {
		return this.cnHealthcarePlanNo;
	}

	public void setCnHealthcarePlanNo(String cnHealthcarePlanNo) {
		this.cnHealthcarePlanNo = cnHealthcarePlanNo;
	}

	public String getCnIndustrialSickness() {
		return this.cnIndustrialSickness;
	}

	public void setCnIndustrialSickness(String cnIndustrialSickness) {
		this.cnIndustrialSickness = cnIndustrialSickness;
	}

	public BigDecimal getCnLastClaimAmount() {
		return this.cnLastClaimAmount;
	}

	public void setCnLastClaimAmount(BigDecimal cnLastClaimAmount) {
		this.cnLastClaimAmount = cnLastClaimAmount;
	}

	public Date getCnLastClaimDate() {
		return this.cnLastClaimDate;
	}

	public void setCnLastClaimDate(Date cnLastClaimDate) {
		this.cnLastClaimDate = cnLastClaimDate;
	}

	public String getCnMajorDiseaseInd() {
		return this.cnMajorDiseaseInd;
	}

	public void setCnMajorDiseaseInd(String cnMajorDiseaseInd) {
		this.cnMajorDiseaseInd = cnMajorDiseaseInd;
	}

	public String getCnMajorDiseaseType() {
		return this.cnMajorDiseaseType;
	}

	public void setCnMajorDiseaseType(String cnMajorDiseaseType) {
		this.cnMajorDiseaseType = cnMajorDiseaseType;
	}

	public String getCnMioInsurance() {
		return this.cnMioInsurance;
	}

	public void setCnMioInsurance(String cnMioInsurance) {
		this.cnMioInsurance = cnMioInsurance;
	}

	public String getCnMioMethod() {
		return this.cnMioMethod;
	}

	public void setCnMioMethod(String cnMioMethod) {
		this.cnMioMethod = cnMioMethod;
	}

	public String getCnMioPersonGroup() {
		return this.cnMioPersonGroup;
	}

	public void setCnMioPersonGroup(String cnMioPersonGroup) {
		this.cnMioPersonGroup = cnMioPersonGroup;
	}

	public BigDecimal getCnSelfPayableAmount() {
		return this.cnSelfPayableAmount;
	}

	public void setCnSelfPayableAmount(BigDecimal cnSelfPayableAmount) {
		this.cnSelfPayableAmount = cnSelfPayableAmount;
	}

	public BigDecimal getCnYtdSelfpaidAmount() {
		return this.cnYtdSelfpaidAmount;
	}

	public void setCnYtdSelfpaidAmount(BigDecimal cnYtdSelfpaidAmount) {
		this.cnYtdSelfpaidAmount = cnYtdSelfpaidAmount;
	}

	public String getConcessionCode() {
		return this.concessionCode;
	}

	public void setConcessionCode(String concessionCode) {
		this.concessionCode = concessionCode;
	}

	public BigDecimal getContractBalance() {
		return this.contractBalance;
	}

	public void setContractBalance(BigDecimal contractBalance) {
		this.contractBalance = contractBalance;
	}

	public String getContractNo() {
		return this.contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getContractStatus() {
		return this.contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getContractType() {
		return this.contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getDebtorChangedInd() {
		return this.debtorChangedInd;
	}

	public void setDebtorChangedInd(String debtorChangedInd) {
		this.debtorChangedInd = debtorChangedInd;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getFinancialClass() {
		return this.financialClass;
	}

	public void setFinancialClass(String financialClass) {
		this.financialClass = financialClass;
	}

	public BigDecimal getFrozenContractBalance() {
		return this.frozenContractBalance;
	}

	public void setFrozenContractBalance(BigDecimal frozenContractBalance) {
		this.frozenContractBalance = frozenContractBalance;
	}

	public BigDecimal getFrozenContractSize() {
		return this.frozenContractSize;
	}

	public void setFrozenContractSize(BigDecimal frozenContractSize) {
		this.frozenContractSize = frozenContractSize;
	}

	public Date getFrozenDatetime() {
		return this.frozenDatetime;
	}

	public void setFrozenDatetime(Date frozenDatetime) {
		this.frozenDatetime = frozenDatetime;
	}

	public BigDecimal getGuaranteeletterId() {
		return this.guaranteeletterId;
	}

	public void setGuaranteeletterId(BigDecimal guaranteeletterId) {
		this.guaranteeletterId = guaranteeletterId;
	}

	public BigDecimal getLastBillAmount() {
		return this.lastBillAmount;
	}

	public void setLastBillAmount(BigDecimal lastBillAmount) {
		this.lastBillAmount = lastBillAmount;
	}

	public Date getLastBillDate() {
		return this.lastBillDate;
	}

	public void setLastBillDate(Date lastBillDate) {
		this.lastBillDate = lastBillDate;
	}

	public String getLastBillNo() {
		return this.lastBillNo;
	}

	public void setLastBillNo(String lastBillNo) {
		this.lastBillNo = lastBillNo;
	}

	public String getLastBillType() {
		return this.lastBillType;
	}

	public void setLastBillType(String lastBillType) {
		this.lastBillType = lastBillType;
	}

	public String getLastSupplementNo() {
		return this.lastSupplementNo;
	}

	public void setLastSupplementNo(String lastSupplementNo) {
		this.lastSupplementNo = lastSupplementNo;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public BigDecimal getPayerSeq() {
		return this.payerSeq;
	}

	public void setPayerSeq(BigDecimal payerSeq) {
		this.payerSeq = payerSeq;
	}

	public String getPolicyNo() {
		return this.policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRelationship() {
		return this.relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStakeholderId() {
		return this.stakeholderId;
	}

	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
	}

	public BigDecimal getYtdClaimedAmount() {
		return this.ytdClaimedAmount;
	}

	public void setYtdClaimedAmount(BigDecimal ytdClaimedAmount) {
		this.ytdClaimedAmount = ytdClaimedAmount;
	}

	public Patientaccount getPatientaccount() {
		return this.patientaccount;
	}

	public void setPatientaccount(Patientaccount patientaccount) {
		this.patientaccount = patientaccount;
	}

	public List<Billaccount> getBillaccounts() {
		return this.billaccounts;
	}

	public void setBillaccounts(List<Billaccount> billaccounts) {
		this.billaccounts = billaccounts;
	}

	public Billaccount addBillaccount(Billaccount billaccount) {
		getBillaccounts().add(billaccount);
		billaccount.setAccountdebtor(this);

		return billaccount;
	}

	public Billaccount removeBillaccount(Billaccount billaccount) {
		getBillaccounts().remove(billaccount);
		billaccount.setAccountdebtor(null);

		return billaccount;
	}

	public BigDecimal getPatientAccountId() {
		return patientAccountId;
	}

	public void setPatientAccountId(BigDecimal patientAccountId) {
		this.patientAccountId = patientAccountId;
	}

}