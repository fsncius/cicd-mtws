package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MATERIALTXNDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Materialtxndetail.findAll", query="SELECT m FROM Materialtxndetail m")
public class Materialtxndetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MATERIALTXNDETAIL_ID")
	private long materialtxndetailId;

	@Column(name="BASE_UOM_QTY")
	private BigDecimal baseUomQty;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="TXN_COST")
	private BigDecimal txnCost;

	@Column(name="TXN_PRICE")
	private BigDecimal txnPrice;

	@Column(name="TXN_WHOLESALE_PRICE")
	private BigDecimal txnWholesalePrice;

	@Column(name="WHOLESALE_PRICE")
	private BigDecimal wholesalePrice;

	//bi-directional many-to-one association to Materialtxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALTXN_ID")
	private Materialtxn materialtxn;

	//bi-directional many-to-one association to Storeitemstock
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREITEMSTOCK_ID")
	private Storeitemstock storeitemstock;

	public Materialtxndetail() {
	}

	public long getMaterialtxndetailId() {
		return this.materialtxndetailId;
	}

	public void setMaterialtxndetailId(long materialtxndetailId) {
		this.materialtxndetailId = materialtxndetailId;
	}

	public BigDecimal getBaseUomQty() {
		return this.baseUomQty;
	}

	public void setBaseUomQty(BigDecimal baseUomQty) {
		this.baseUomQty = baseUomQty;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getTxnCost() {
		return this.txnCost;
	}

	public void setTxnCost(BigDecimal txnCost) {
		this.txnCost = txnCost;
	}

	public BigDecimal getTxnPrice() {
		return this.txnPrice;
	}

	public void setTxnPrice(BigDecimal txnPrice) {
		this.txnPrice = txnPrice;
	}

	public BigDecimal getTxnWholesalePrice() {
		return this.txnWholesalePrice;
	}

	public void setTxnWholesalePrice(BigDecimal txnWholesalePrice) {
		this.txnWholesalePrice = txnWholesalePrice;
	}

	public BigDecimal getWholesalePrice() {
		return this.wholesalePrice;
	}

	public void setWholesalePrice(BigDecimal wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}

	public Materialtxn getMaterialtxn() {
		return this.materialtxn;
	}

	public void setMaterialtxn(Materialtxn materialtxn) {
		this.materialtxn = materialtxn;
	}

	public Storeitemstock getStoreitemstock() {
		return this.storeitemstock;
	}

	public void setStoreitemstock(Storeitemstock storeitemstock) {
		this.storeitemstock = storeitemstock;
	}

}