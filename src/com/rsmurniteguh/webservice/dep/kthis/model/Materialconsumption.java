package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MATERIALCONSUMPTION database table.
 * 
 */
@Entity
@NamedQuery(name="Materialconsumption.findAll", query="SELECT m FROM Materialconsumption m")
public class Materialconsumption implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MATERIALCONSUMPTION_ID")
	private long materialconsumptionId;

	private String barcode;

	@Column(name="CONSUME_QTY")
	private BigDecimal consumeQty;

	@Column(name="CONSUME_REVENUECENTREMSTR_ID")
	private BigDecimal consumeRevenuecentremstrId;

	@Column(name="CONSUMPTION_TYPE")
	private String consumptionType;

	@Column(name="COST_PRICE")
	private BigDecimal costPrice;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="INVENTORYDEDUCTION_IND")
	private String inventorydeductionInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIALCONSUME_STATUS")
	private String materialconsumeStatus;

	@Column(name="QTY_UOM")
	private String qtyUom;

	private String remarks;

	@Column(name="REQUISITION_ID")
	private BigDecimal requisitionId;

	@Column(name="RETURN_MATERIALCONSUMPTION_ID")
	private BigDecimal returnMaterialconsumptionId;

	@Column(name="SOURCE_MODE")
	private String sourceMode;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CAREPROVIDER_ID")
	private Careprovider careprovider;

	//bi-directional many-to-one association to Charge
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGE_ID")
	private Charge charge;

	//bi-directional many-to-one association to Materialitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMMSTR_ID")
	private Materialitemmstr materialitemmstr;

	//bi-directional many-to-one association to Materialtxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALTXN_ID")
	private Materialtxn materialtxn;

	//bi-directional many-to-one association to Patient
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENT_ID")
	private Patient patient;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public Materialconsumption() {
	}

	public long getMaterialconsumptionId() {
		return this.materialconsumptionId;
	}

	public void setMaterialconsumptionId(long materialconsumptionId) {
		this.materialconsumptionId = materialconsumptionId;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public BigDecimal getConsumeQty() {
		return this.consumeQty;
	}

	public void setConsumeQty(BigDecimal consumeQty) {
		this.consumeQty = consumeQty;
	}

	public BigDecimal getConsumeRevenuecentremstrId() {
		return this.consumeRevenuecentremstrId;
	}

	public void setConsumeRevenuecentremstrId(BigDecimal consumeRevenuecentremstrId) {
		this.consumeRevenuecentremstrId = consumeRevenuecentremstrId;
	}

	public String getConsumptionType() {
		return this.consumptionType;
	}

	public void setConsumptionType(String consumptionType) {
		this.consumptionType = consumptionType;
	}

	public BigDecimal getCostPrice() {
		return this.costPrice;
	}

	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getInventorydeductionInd() {
		return this.inventorydeductionInd;
	}

	public void setInventorydeductionInd(String inventorydeductionInd) {
		this.inventorydeductionInd = inventorydeductionInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMaterialconsumeStatus() {
		return this.materialconsumeStatus;
	}

	public void setMaterialconsumeStatus(String materialconsumeStatus) {
		this.materialconsumeStatus = materialconsumeStatus;
	}

	public String getQtyUom() {
		return this.qtyUom;
	}

	public void setQtyUom(String qtyUom) {
		this.qtyUom = qtyUom;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getRequisitionId() {
		return this.requisitionId;
	}

	public void setRequisitionId(BigDecimal requisitionId) {
		this.requisitionId = requisitionId;
	}

	public BigDecimal getReturnMaterialconsumptionId() {
		return this.returnMaterialconsumptionId;
	}

	public void setReturnMaterialconsumptionId(BigDecimal returnMaterialconsumptionId) {
		this.returnMaterialconsumptionId = returnMaterialconsumptionId;
	}

	public String getSourceMode() {
		return this.sourceMode;
	}

	public void setSourceMode(String sourceMode) {
		this.sourceMode = sourceMode;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Careprovider getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(Careprovider careprovider) {
		this.careprovider = careprovider;
	}

	public Charge getCharge() {
		return this.charge;
	}

	public void setCharge(Charge charge) {
		this.charge = charge;
	}

	public Materialitemmstr getMaterialitemmstr() {
		return this.materialitemmstr;
	}

	public void setMaterialitemmstr(Materialitemmstr materialitemmstr) {
		this.materialitemmstr = materialitemmstr;
	}

	public Materialtxn getMaterialtxn() {
		return this.materialtxn;
	}

	public void setMaterialtxn(Materialtxn materialtxn) {
		this.materialtxn = materialtxn;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}