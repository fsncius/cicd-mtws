package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the USERPROFILE database table.
 * 
 */
@Entity
@NamedQuery(name="Userprofile.findAll", query="SELECT u FROM Userprofile u")
public class Userprofile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="USERPROFILE_ID")
	private long userprofileId;

	@Column(name="DEFAULT_LANGUAGE")
	private String defaultLanguage;

	@Column(name="DEFAULT_LOCATIONMSTR_ID")
	private BigDecimal defaultLocationmstrId;

	@Column(name="DEFAULT_ROLEMSTR_ID")
	private BigDecimal defaultRolemstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="JOB_NO")
	private String jobNo;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PERSON_ID")
	private BigDecimal personId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	//bi-directional many-to-one association to Usermstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="USERMSTR_ID")
	private Usermstr usermstr;

	public Userprofile() {
	}

	public long getUserprofileId() {
		return this.userprofileId;
	}

	public void setUserprofileId(long userprofileId) {
		this.userprofileId = userprofileId;
	}

	public String getDefaultLanguage() {
		return this.defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public BigDecimal getDefaultLocationmstrId() {
		return this.defaultLocationmstrId;
	}

	public void setDefaultLocationmstrId(BigDecimal defaultLocationmstrId) {
		this.defaultLocationmstrId = defaultLocationmstrId;
	}

	public BigDecimal getDefaultRolemstrId() {
		return this.defaultRolemstrId;
	}

	public void setDefaultRolemstrId(BigDecimal defaultRolemstrId) {
		this.defaultRolemstrId = defaultRolemstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getJobNo() {
		return this.jobNo;
	}

	public void setJobNo(String jobNo) {
		this.jobNo = jobNo;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPersonId() {
		return this.personId;
	}

	public void setPersonId(BigDecimal personId) {
		this.personId = personId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Usermstr getUsermstr() {
		return this.usermstr;
	}

	public void setUsermstr(Usermstr usermstr) {
		this.usermstr = usermstr;
	}

}