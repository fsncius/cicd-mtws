package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the COUNTER_IPDEPOSIT database table.
 * 
 */
@Entity
@Table(name="COUNTER_IPDEPOSIT")
@NamedQuery(name="CounterIpdeposit.findAll", query="SELECT c FROM CounterIpdeposit c")
public class CounterIpdeposit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTER_IPCHARGEDETAIL_ID")
	private long counterIpchargedetailId;

	private BigDecimal amount;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DATA_CAT")
	private String dataCat;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PAYMANET_MODE")
	private String paymanetMode;

	//bi-directional many-to-one association to Countertotal
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COUNTERTOTAL_ID")
	private Countertotal countertotal;

	public CounterIpdeposit() {
	}

	public long getCounterIpchargedetailId() {
		return this.counterIpchargedetailId;
	}

	public void setCounterIpchargedetailId(long counterIpchargedetailId) {
		this.counterIpchargedetailId = counterIpchargedetailId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDataCat() {
		return this.dataCat;
	}

	public void setDataCat(String dataCat) {
		this.dataCat = dataCat;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPaymanetMode() {
		return this.paymanetMode;
	}

	public void setPaymanetMode(String paymanetMode) {
		this.paymanetMode = paymanetMode;
	}

	public Countertotal getCountertotal() {
		return this.countertotal;
	}

	public void setCountertotal(Countertotal countertotal) {
		this.countertotal = countertotal;
	}

}