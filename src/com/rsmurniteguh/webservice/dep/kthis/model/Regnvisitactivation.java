package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the REGNVISITACTIVATION database table.
 * 
 */
@Entity
@NamedQuery(name="Regnvisitactivation.findAll", query="SELECT r FROM Regnvisitactivation r")
public class Regnvisitactivation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REGNVISITACTIVATION_ID")
	private long regnvisitactivationId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CREATED_LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Visitregntype
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISITREGNTYPE_ID")
	private Visitregntype visitregntype;

	public Regnvisitactivation() {
	}

	public long getRegnvisitactivationId() {
		return this.regnvisitactivationId;
	}

	public void setRegnvisitactivationId(long regnvisitactivationId) {
		this.regnvisitactivationId = regnvisitactivationId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public Visitregntype getVisitregntype() {
		return this.visitregntype;
	}

	public void setVisitregntype(Visitregntype visitregntype) {
		this.visitregntype = visitregntype;
	}

}