package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITDAILYSUMMARYDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Visitdailysummarydetail.findAll", query="SELECT v FROM Visitdailysummarydetail v")
public class Visitdailysummarydetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITDAILYSUMMARYDETAIL_ID")
	private long visitdailysummarydetailId;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DRUG_IND")
	private String drugInd;

	@Column(name="ITEM_TYPE")
	private String itemType;

	@Column(name="ITEMSUBCATEGORYMSTR_ID")
	private BigDecimal itemsubcategorymstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MIO_MIC1_AMOUNT")
	private BigDecimal mioMic1Amount;

	@Column(name="MIO_MIC2_AMOUNT")
	private BigDecimal mioMic2Amount;

	@Column(name="MIO_MIC3_AMOUNT")
	private BigDecimal mioMic3Amount;

	@Column(name="RECEIPT_ITEM_CAT")
	private String receiptItemCat;

	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenuecentremstrId;

	@Column(name="TXN_AMOUNT")
	private BigDecimal txnAmount;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr1;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr2;

	//bi-directional many-to-one association to Visitdailysummary
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISITDAILYSUMMARY_ID")
	private Visitdailysummary visitdailysummary;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_WARDMSTR_ID")
	private Wardmstr wardmstr;

	public Visitdailysummarydetail() {
	}

	public long getVisitdailysummarydetailId() {
		return this.visitdailysummarydetailId;
	}

	public void setVisitdailysummarydetailId(long visitdailysummarydetailId) {
		this.visitdailysummarydetailId = visitdailysummarydetailId;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDrugInd() {
		return this.drugInd;
	}

	public void setDrugInd(String drugInd) {
		this.drugInd = drugInd;
	}

	public String getItemType() {
		return this.itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public BigDecimal getItemsubcategorymstrId() {
		return this.itemsubcategorymstrId;
	}

	public void setItemsubcategorymstrId(BigDecimal itemsubcategorymstrId) {
		this.itemsubcategorymstrId = itemsubcategorymstrId;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMioMic1Amount() {
		return this.mioMic1Amount;
	}

	public void setMioMic1Amount(BigDecimal mioMic1Amount) {
		this.mioMic1Amount = mioMic1Amount;
	}

	public BigDecimal getMioMic2Amount() {
		return this.mioMic2Amount;
	}

	public void setMioMic2Amount(BigDecimal mioMic2Amount) {
		this.mioMic2Amount = mioMic2Amount;
	}

	public BigDecimal getMioMic3Amount() {
		return this.mioMic3Amount;
	}

	public void setMioMic3Amount(BigDecimal mioMic3Amount) {
		this.mioMic3Amount = mioMic3Amount;
	}

	public String getReceiptItemCat() {
		return this.receiptItemCat;
	}

	public void setReceiptItemCat(String receiptItemCat) {
		this.receiptItemCat = receiptItemCat;
	}

	public BigDecimal getRevenuecentremstrId() {
		return this.revenuecentremstrId;
	}

	public void setRevenuecentremstrId(BigDecimal revenuecentremstrId) {
		this.revenuecentremstrId = revenuecentremstrId;
	}

	public BigDecimal getTxnAmount() {
		return this.txnAmount;
	}

	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}

	public Subspecialtymstr getSubspecialtymstr1() {
		return this.subspecialtymstr1;
	}

	public void setSubspecialtymstr1(Subspecialtymstr subspecialtymstr1) {
		this.subspecialtymstr1 = subspecialtymstr1;
	}

	public Subspecialtymstr getSubspecialtymstr2() {
		return this.subspecialtymstr2;
	}

	public void setSubspecialtymstr2(Subspecialtymstr subspecialtymstr2) {
		this.subspecialtymstr2 = subspecialtymstr2;
	}

	public Visitdailysummary getVisitdailysummary() {
		return this.visitdailysummary;
	}

	public void setVisitdailysummary(Visitdailysummary visitdailysummary) {
		this.visitdailysummary = visitdailysummary;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

}