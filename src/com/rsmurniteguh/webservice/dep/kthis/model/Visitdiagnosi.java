package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the VISITDIAGNOSIS database table.
 * 
 */
@Entity
@Table(name="VISITDIAGNOSIS")
@NamedQuery(name="Visitdiagnosi.findAll", query="SELECT v FROM Visitdiagnosi v")
public class Visitdiagnosi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITDIAGNOSIS_ID")
	private long visitdiagnosisId;

	@Temporal(TemporalType.DATE)
	@Column(name="CODED_DATE")
	private Date codedDate;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="DIAGNOSED_DATETIME")
	private Date diagnosedDatetime;

	@Column(name="DIAGNOSIS_TYPE")
	private String diagnosisType;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MCR_NO")
	private String mcrNo;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REPORTED_IND")
	private String reportedInd;

	@Column(name="VISIT_DIAGNOSIS_REMARKS")
	private String visitDiagnosisRemarks;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CODED_BY")
	private Careprovider careprovider1;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DIAGNOSED_BY")
	private Careprovider careprovider2;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Visitdiagnosisdetail
	@OneToMany(mappedBy="visitdiagnosi")
	private List<Visitdiagnosisdetail> visitdiagnosisdetails;

	public Visitdiagnosi() {
	}

	public long getVisitdiagnosisId() {
		return this.visitdiagnosisId;
	}

	public void setVisitdiagnosisId(long visitdiagnosisId) {
		this.visitdiagnosisId = visitdiagnosisId;
	}

	public Date getCodedDate() {
		return this.codedDate;
	}

	public void setCodedDate(Date codedDate) {
		this.codedDate = codedDate;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getDiagnosedDatetime() {
		return this.diagnosedDatetime;
	}

	public void setDiagnosedDatetime(Date diagnosedDatetime) {
		this.diagnosedDatetime = diagnosedDatetime;
	}

	public String getDiagnosisType() {
		return this.diagnosisType;
	}

	public void setDiagnosisType(String diagnosisType) {
		this.diagnosisType = diagnosisType;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMcrNo() {
		return this.mcrNo;
	}

	public void setMcrNo(String mcrNo) {
		this.mcrNo = mcrNo;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getReportedInd() {
		return this.reportedInd;
	}

	public void setReportedInd(String reportedInd) {
		this.reportedInd = reportedInd;
	}

	public String getVisitDiagnosisRemarks() {
		return this.visitDiagnosisRemarks;
	}

	public void setVisitDiagnosisRemarks(String visitDiagnosisRemarks) {
		this.visitDiagnosisRemarks = visitDiagnosisRemarks;
	}

	public Careprovider getCareprovider1() {
		return this.careprovider1;
	}

	public void setCareprovider1(Careprovider careprovider1) {
		this.careprovider1 = careprovider1;
	}

	public Careprovider getCareprovider2() {
		return this.careprovider2;
	}

	public void setCareprovider2(Careprovider careprovider2) {
		this.careprovider2 = careprovider2;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public List<Visitdiagnosisdetail> getVisitdiagnosisdetails() {
		return this.visitdiagnosisdetails;
	}

	public void setVisitdiagnosisdetails(List<Visitdiagnosisdetail> visitdiagnosisdetails) {
		this.visitdiagnosisdetails = visitdiagnosisdetails;
	}

	public Visitdiagnosisdetail addVisitdiagnosisdetail(Visitdiagnosisdetail visitdiagnosisdetail) {
		getVisitdiagnosisdetails().add(visitdiagnosisdetail);
		visitdiagnosisdetail.setVisitdiagnosi(this);

		return visitdiagnosisdetail;
	}

	public Visitdiagnosisdetail removeVisitdiagnosisdetail(Visitdiagnosisdetail visitdiagnosisdetail) {
		getVisitdiagnosisdetails().remove(visitdiagnosisdetail);
		visitdiagnosisdetail.setVisitdiagnosi(null);

		return visitdiagnosisdetail;
	}

}