package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BILL database table.
 * 
 */
@Entity
@NamedQuery(name="Bill.findAll", query="SELECT b FROM Bill b")
public class Bill implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BILL_ID")
	private long billId;

	@Temporal(TemporalType.DATE)
	@Column(name="BILL_DATETIME")
	private Date billDatetime;

	@Column(name="BILL_NO")
	private String billNo;

	@Column(name="BILL_REMARKS")
	private String billRemarks;

	@Column(name="BILL_SIZE")
	private BigDecimal billSize;

	@Column(name="BILL_STATUS")
	private String billStatus;

	@Column(name="BILLING_TYPE")
	private String billingType;

	@Column(name="CANCEL_REASON")
	private String cancelReason;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="DATETIME_POINT_OF_LAST_PAT")
	private Date datetimePointOfLastPat;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="ORIGIN_BILL_SIZE")
	private BigDecimal originBillSize;

	@Column(name="PAID_AMOUNT")
	private BigDecimal paidAmount;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	//bi-directional many-to-one association to Patientaccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNT_ID")
	private Patientaccount patientaccount;

	//bi-directional many-to-one association to Receipt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="RECEIPT_ID")
	private Receipt receipt;

	//bi-directional many-to-one association to Billaccount
	@OneToMany(mappedBy="bill")
	private List<Billaccount> billaccounts;

	//bi-directional many-to-one association to Billdiscount
	@OneToMany(mappedBy="bill")
	private List<Billdiscount> billdiscounts;

	//bi-directional many-to-one association to Billreceipt
	@OneToMany(mappedBy="bill")
	private List<Billreceipt> billreceipts;

	//bi-directional many-to-one association to Patientaccounttxn
	@OneToMany(mappedBy="bill")
	private List<Patientaccounttxn> patientaccounttxns;

	public Bill() {
	}

	public long getBillId() {
		return this.billId;
	}

	public void setBillId(long billId) {
		this.billId = billId;
	}

	public Date getBillDatetime() {
		return this.billDatetime;
	}

	public void setBillDatetime(Date billDatetime) {
		this.billDatetime = billDatetime;
	}

	public String getBillNo() {
		return this.billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getBillRemarks() {
		return this.billRemarks;
	}

	public void setBillRemarks(String billRemarks) {
		this.billRemarks = billRemarks;
	}

	public BigDecimal getBillSize() {
		return this.billSize;
	}

	public void setBillSize(BigDecimal billSize) {
		this.billSize = billSize;
	}

	public String getBillStatus() {
		return this.billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public String getBillingType() {
		return this.billingType;
	}

	public void setBillingType(String billingType) {
		this.billingType = billingType;
	}

	public String getCancelReason() {
		return this.cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public Date getDatetimePointOfLastPat() {
		return this.datetimePointOfLastPat;
	}

	public void setDatetimePointOfLastPat(Date datetimePointOfLastPat) {
		this.datetimePointOfLastPat = datetimePointOfLastPat;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getOriginBillSize() {
		return this.originBillSize;
	}

	public void setOriginBillSize(BigDecimal originBillSize) {
		this.originBillSize = originBillSize;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Patientaccount getPatientaccount() {
		return this.patientaccount;
	}

	public void setPatientaccount(Patientaccount patientaccount) {
		this.patientaccount = patientaccount;
	}

	public Receipt getReceipt() {
		return this.receipt;
	}

	public void setReceipt(Receipt receipt) {
		this.receipt = receipt;
	}

	public List<Billaccount> getBillaccounts() {
		return this.billaccounts;
	}

	public void setBillaccounts(List<Billaccount> billaccounts) {
		this.billaccounts = billaccounts;
	}

	public Billaccount addBillaccount(Billaccount billaccount) {
		getBillaccounts().add(billaccount);
		billaccount.setBill(this);

		return billaccount;
	}

	public Billaccount removeBillaccount(Billaccount billaccount) {
		getBillaccounts().remove(billaccount);
		billaccount.setBill(null);

		return billaccount;
	}

	public List<Billdiscount> getBilldiscounts() {
		return this.billdiscounts;
	}

	public void setBilldiscounts(List<Billdiscount> billdiscounts) {
		this.billdiscounts = billdiscounts;
	}

	public Billdiscount addBilldiscount(Billdiscount billdiscount) {
		getBilldiscounts().add(billdiscount);
		billdiscount.setBill(this);

		return billdiscount;
	}

	public Billdiscount removeBilldiscount(Billdiscount billdiscount) {
		getBilldiscounts().remove(billdiscount);
		billdiscount.setBill(null);

		return billdiscount;
	}

	public List<Billreceipt> getBillreceipts() {
		return this.billreceipts;
	}

	public void setBillreceipts(List<Billreceipt> billreceipts) {
		this.billreceipts = billreceipts;
	}

	public Billreceipt addBillreceipt(Billreceipt billreceipt) {
		getBillreceipts().add(billreceipt);
		billreceipt.setBill(this);

		return billreceipt;
	}

	public Billreceipt removeBillreceipt(Billreceipt billreceipt) {
		getBillreceipts().remove(billreceipt);
		billreceipt.setBill(null);

		return billreceipt;
	}

	public List<Patientaccounttxn> getPatientaccounttxns() {
		return this.patientaccounttxns;
	}

	public void setPatientaccounttxns(List<Patientaccounttxn> patientaccounttxns) {
		this.patientaccounttxns = patientaccounttxns;
	}

	public Patientaccounttxn addPatientaccounttxn(Patientaccounttxn patientaccounttxn) {
		getPatientaccounttxns().add(patientaccounttxn);
		patientaccounttxn.setBill(this);

		return patientaccounttxn;
	}

	public Patientaccounttxn removePatientaccounttxn(Patientaccounttxn patientaccounttxn) {
		getPatientaccounttxns().remove(patientaccounttxn);
		patientaccounttxn.setBill(null);

		return patientaccounttxn;
	}

}