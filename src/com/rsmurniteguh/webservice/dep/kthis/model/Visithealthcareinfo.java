package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITHEALTHCAREINFO database table.
 * 
 */
@Entity
@NamedQuery(name="Visithealthcareinfo.findAll", query="SELECT v FROM Visithealthcareinfo v")
public class Visithealthcareinfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITHEALTHCAREINFO_ID")
	private long visithealthcareinfoId;

	@Column(name="BODY_TEMPERATURE")
	private BigDecimal bodyTemperature;

	@Column(name="BREATHE_TIMES")
	private BigDecimal breatheTimes;

	@Column(name="CONDITION_LEVEL")
	private String conditionLevel;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DIASTOLIC_BLOOD_PRESSURE")
	private BigDecimal diastolicBloodPressure;

	@Column(name="HEAD_CIRCUMFERENCE")
	private BigDecimal headCircumference;

	private BigDecimal height;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PULSE_RATE")
	private BigDecimal pulseRate;

	private String remarks;

	@Column(name="SYSTOLIC_BLOOD_PRESSURE")
	private BigDecimal systolicBloodPressure;

	private BigDecimal weight;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public Visithealthcareinfo() {
	}

	public long getVisithealthcareinfoId() {
		return this.visithealthcareinfoId;
	}

	public void setVisithealthcareinfoId(long visithealthcareinfoId) {
		this.visithealthcareinfoId = visithealthcareinfoId;
	}

	public BigDecimal getBodyTemperature() {
		return this.bodyTemperature;
	}

	public void setBodyTemperature(BigDecimal bodyTemperature) {
		this.bodyTemperature = bodyTemperature;
	}

	public BigDecimal getBreatheTimes() {
		return this.breatheTimes;
	}

	public void setBreatheTimes(BigDecimal breatheTimes) {
		this.breatheTimes = breatheTimes;
	}

	public String getConditionLevel() {
		return this.conditionLevel;
	}

	public void setConditionLevel(String conditionLevel) {
		this.conditionLevel = conditionLevel;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getDiastolicBloodPressure() {
		return this.diastolicBloodPressure;
	}

	public void setDiastolicBloodPressure(BigDecimal diastolicBloodPressure) {
		this.diastolicBloodPressure = diastolicBloodPressure;
	}

	public BigDecimal getHeadCircumference() {
		return this.headCircumference;
	}

	public void setHeadCircumference(BigDecimal headCircumference) {
		this.headCircumference = headCircumference;
	}

	public BigDecimal getHeight() {
		return this.height;
	}

	public void setHeight(BigDecimal height) {
		this.height = height;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPulseRate() {
		return this.pulseRate;
	}

	public void setPulseRate(BigDecimal pulseRate) {
		this.pulseRate = pulseRate;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getSystolicBloodPressure() {
		return this.systolicBloodPressure;
	}

	public void setSystolicBloodPressure(BigDecimal systolicBloodPressure) {
		this.systolicBloodPressure = systolicBloodPressure;
	}

	public BigDecimal getWeight() {
		return this.weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}