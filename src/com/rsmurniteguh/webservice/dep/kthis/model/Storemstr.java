package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOREMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Storemstr.findAll", query="SELECT s FROM Storemstr s")
public class Storemstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOREMSTR_ID")
	private long storemstrId;

	@Column(name="AUTO_DISPENSING_IND")
	private String autoDispensingInd;

	@Column(name="AUTO_VERIFICATION_IND")
	private String autoVerificationInd;

	@Column(name="COMPUTATION_PERIOD_IN_DAYS")
	private BigDecimal computationPeriodInDays;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DELIVERY_ADDRESS_1")
	private String deliveryAddress1;

	@Column(name="DELIVERY_ADDRESS_2")
	private String deliveryAddress2;

	@Column(name="DELIVERY_ADDRESS_3")
	private String deliveryAddress3;

	@Column(name="FAX_NO")
	private String faxNo;

	@Column(name="IP_DISPENSING_MODE")
	private String ipDispensingMode;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MANAGER_MODE")
	private String managerMode;

	@Column(name="MONTHLYACCOUNTCLOSING_IND")
	private String monthlyaccountclosingInd;

	@Column(name="OP_DISPENSING_MODE")
	private String opDispensingMode;

	@Column(name="PERSON_IN_CHARGE")
	private String personInCharge;

	@Column(name="PHONE_NO")
	private String phoneNo;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="QUICK_CODE")
	private String quickCode;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="STORE_CODE")
	private String storeCode;

	@Column(name="STORE_DESC")
	private String storeDesc;

	@Column(name="STORE_DESC_LANG1")
	private String storeDescLang1;

	@Column(name="STORE_TYPE")
	private String storeType;

	//bi-directional many-to-one association to Stockaccountclosing
	@OneToMany(mappedBy="storemstr")
	private List<Stockaccountclosing> stockaccountclosings;


	//bi-directional many-to-one association to Stockcount
	@OneToMany(mappedBy="storemstr")
	private List<Stockcount> stockcounts;


	//bi-directional many-to-one association to Stockpurchaseplan
	@OneToMany(mappedBy="storemstr")
	private List<Stockpurchaseplan> stockpurchaseplans;

	//bi-directional many-to-one association to Stockpurchaserequisition
	@OneToMany(mappedBy="storemstr")
	private List<Stockpurchaserequisition> stockpurchaserequisitions;


	//bi-directional many-to-one association to Storebinmstr
	@OneToMany(mappedBy="storemstr")
	private List<Storebinmstr> storebinmstrs;

	//bi-directional many-to-one association to Storecountdefine
	@OneToMany(mappedBy="storemstr")
	private List<Storecountdefine> storecountdefines;

	//bi-directional many-to-one association to Storedailysummary
	@OneToMany(mappedBy="storemstr")
	private List<Storedailysummary> storedailysummaries;

	//bi-directional many-to-one association to Storedispensewindowmstr
	@OneToMany(mappedBy="storemstr")
	private List<Storedispensewindowmstr> storedispensewindowmstrs;

	//bi-directional many-to-one association to Storeitem
	@OneToMany(mappedBy="storemstr")
	private List<Storeitem> storeitems;

	//bi-directional many-to-one association to Storemanagementprofile
	@OneToMany(mappedBy="storemstr")
	private List<Storemanagementprofile> storemanagementprofiles;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DEFAULT_SUPPLY_STOREMSTR_ID")
	private Storemstr storemstr;

	//bi-directional many-to-one association to Storemstr
	@OneToMany(mappedBy="storemstr")
	private List<Storemstr> storemstrs;

	//bi-directional many-to-one association to Storesupplystore
	@OneToMany(mappedBy="storemstr1")
	private List<Storesupplystore> storesupplystores1;

	//bi-directional many-to-one association to Storesupplystore
	@OneToMany(mappedBy="storemstr2")
	private List<Storesupplystore> storesupplystores2;

	//bi-directional many-to-one association to Wardsspmstocktxn
	@OneToMany(mappedBy="storemstr")
	private List<Wardsspmstocktxn> wardsspmstocktxns;

	public Storemstr() {
	}

	public long getStoremstrId() {
		return this.storemstrId;
	}

	public void setStoremstrId(long storemstrId) {
		this.storemstrId = storemstrId;
	}

	public String getAutoDispensingInd() {
		return this.autoDispensingInd;
	}

	public void setAutoDispensingInd(String autoDispensingInd) {
		this.autoDispensingInd = autoDispensingInd;
	}

	public String getAutoVerificationInd() {
		return this.autoVerificationInd;
	}

	public void setAutoVerificationInd(String autoVerificationInd) {
		this.autoVerificationInd = autoVerificationInd;
	}

	public BigDecimal getComputationPeriodInDays() {
		return this.computationPeriodInDays;
	}

	public void setComputationPeriodInDays(BigDecimal computationPeriodInDays) {
		this.computationPeriodInDays = computationPeriodInDays;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDeliveryAddress1() {
		return this.deliveryAddress1;
	}

	public void setDeliveryAddress1(String deliveryAddress1) {
		this.deliveryAddress1 = deliveryAddress1;
	}

	public String getDeliveryAddress2() {
		return this.deliveryAddress2;
	}

	public void setDeliveryAddress2(String deliveryAddress2) {
		this.deliveryAddress2 = deliveryAddress2;
	}

	public String getDeliveryAddress3() {
		return this.deliveryAddress3;
	}

	public void setDeliveryAddress3(String deliveryAddress3) {
		this.deliveryAddress3 = deliveryAddress3;
	}

	public String getFaxNo() {
		return this.faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getIpDispensingMode() {
		return this.ipDispensingMode;
	}

	public void setIpDispensingMode(String ipDispensingMode) {
		this.ipDispensingMode = ipDispensingMode;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getManagerMode() {
		return this.managerMode;
	}

	public void setManagerMode(String managerMode) {
		this.managerMode = managerMode;
	}

	public String getMonthlyaccountclosingInd() {
		return this.monthlyaccountclosingInd;
	}

	public void setMonthlyaccountclosingInd(String monthlyaccountclosingInd) {
		this.monthlyaccountclosingInd = monthlyaccountclosingInd;
	}

	public String getOpDispensingMode() {
		return this.opDispensingMode;
	}

	public void setOpDispensingMode(String opDispensingMode) {
		this.opDispensingMode = opDispensingMode;
	}

	public String getPersonInCharge() {
		return this.personInCharge;
	}

	public void setPersonInCharge(String personInCharge) {
		this.personInCharge = personInCharge;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getQuickCode() {
		return this.quickCode;
	}

	public void setQuickCode(String quickCode) {
		this.quickCode = quickCode;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getStoreCode() {
		return this.storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getStoreDesc() {
		return this.storeDesc;
	}

	public void setStoreDesc(String storeDesc) {
		this.storeDesc = storeDesc;
	}

	public String getStoreDescLang1() {
		return this.storeDescLang1;
	}

	public void setStoreDescLang1(String storeDescLang1) {
		this.storeDescLang1 = storeDescLang1;
	}

	public String getStoreType() {
		return this.storeType;
	}

	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}

	public List<Stockaccountclosing> getStockaccountclosings() {
		return this.stockaccountclosings;
	}

	public void setStockaccountclosings(List<Stockaccountclosing> stockaccountclosings) {
		this.stockaccountclosings = stockaccountclosings;
	}

	public Stockaccountclosing addStockaccountclosing(Stockaccountclosing stockaccountclosing) {
		getStockaccountclosings().add(stockaccountclosing);
		stockaccountclosing.setStoremstr(this);

		return stockaccountclosing;
	}

	public Stockaccountclosing removeStockaccountclosing(Stockaccountclosing stockaccountclosing) {
		getStockaccountclosings().remove(stockaccountclosing);
		stockaccountclosing.setStoremstr(null);

		return stockaccountclosing;
	}


	public List<Stockcount> getStockcounts() {
		return this.stockcounts;
	}

	public void setStockcounts(List<Stockcount> stockcounts) {
		this.stockcounts = stockcounts;
	}

	public Stockcount addStockcount(Stockcount stockcount) {
		getStockcounts().add(stockcount);
		stockcount.setStoremstr(this);

		return stockcount;
	}

	public Stockcount removeStockcount(Stockcount stockcount) {
		getStockcounts().remove(stockcount);
		stockcount.setStoremstr(null);

		return stockcount;
	}


	public List<Stockpurchaseplan> getStockpurchaseplans() {
		return this.stockpurchaseplans;
	}

	public void setStockpurchaseplans(List<Stockpurchaseplan> stockpurchaseplans) {
		this.stockpurchaseplans = stockpurchaseplans;
	}

	public Stockpurchaseplan addStockpurchaseplan(Stockpurchaseplan stockpurchaseplan) {
		getStockpurchaseplans().add(stockpurchaseplan);
		stockpurchaseplan.setStoremstr(this);

		return stockpurchaseplan;
	}

	public Stockpurchaseplan removeStockpurchaseplan(Stockpurchaseplan stockpurchaseplan) {
		getStockpurchaseplans().remove(stockpurchaseplan);
		stockpurchaseplan.setStoremstr(null);

		return stockpurchaseplan;
	}

	public List<Stockpurchaserequisition> getStockpurchaserequisitions() {
		return this.stockpurchaserequisitions;
	}

	public void setStockpurchaserequisitions(List<Stockpurchaserequisition> stockpurchaserequisitions) {
		this.stockpurchaserequisitions = stockpurchaserequisitions;
	}

	public Stockpurchaserequisition addStockpurchaserequisition(Stockpurchaserequisition stockpurchaserequisition) {
		getStockpurchaserequisitions().add(stockpurchaserequisition);
		stockpurchaserequisition.setStoremstr(this);

		return stockpurchaserequisition;
	}

	public Stockpurchaserequisition removeStockpurchaserequisition(Stockpurchaserequisition stockpurchaserequisition) {
		getStockpurchaserequisitions().remove(stockpurchaserequisition);
		stockpurchaserequisition.setStoremstr(null);

		return stockpurchaserequisition;
	}


	public List<Storebinmstr> getStorebinmstrs() {
		return this.storebinmstrs;
	}

	public void setStorebinmstrs(List<Storebinmstr> storebinmstrs) {
		this.storebinmstrs = storebinmstrs;
	}

	public Storebinmstr addStorebinmstr(Storebinmstr storebinmstr) {
		getStorebinmstrs().add(storebinmstr);
		storebinmstr.setStoremstr(this);

		return storebinmstr;
	}

	public Storebinmstr removeStorebinmstr(Storebinmstr storebinmstr) {
		getStorebinmstrs().remove(storebinmstr);
		storebinmstr.setStoremstr(null);

		return storebinmstr;
	}

	public List<Storecountdefine> getStorecountdefines() {
		return this.storecountdefines;
	}

	public void setStorecountdefines(List<Storecountdefine> storecountdefines) {
		this.storecountdefines = storecountdefines;
	}

	public Storecountdefine addStorecountdefine(Storecountdefine storecountdefine) {
		getStorecountdefines().add(storecountdefine);
		storecountdefine.setStoremstr(this);

		return storecountdefine;
	}

	public Storecountdefine removeStorecountdefine(Storecountdefine storecountdefine) {
		getStorecountdefines().remove(storecountdefine);
		storecountdefine.setStoremstr(null);

		return storecountdefine;
	}

	public List<Storedailysummary> getStoredailysummaries() {
		return this.storedailysummaries;
	}

	public void setStoredailysummaries(List<Storedailysummary> storedailysummaries) {
		this.storedailysummaries = storedailysummaries;
	}

	public Storedailysummary addStoredailysummary(Storedailysummary storedailysummary) {
		getStoredailysummaries().add(storedailysummary);
		storedailysummary.setStoremstr(this);

		return storedailysummary;
	}

	public Storedailysummary removeStoredailysummary(Storedailysummary storedailysummary) {
		getStoredailysummaries().remove(storedailysummary);
		storedailysummary.setStoremstr(null);

		return storedailysummary;
	}

	public List<Storedispensewindowmstr> getStoredispensewindowmstrs() {
		return this.storedispensewindowmstrs;
	}

	public void setStoredispensewindowmstrs(List<Storedispensewindowmstr> storedispensewindowmstrs) {
		this.storedispensewindowmstrs = storedispensewindowmstrs;
	}

	public Storedispensewindowmstr addStoredispensewindowmstr(Storedispensewindowmstr storedispensewindowmstr) {
		getStoredispensewindowmstrs().add(storedispensewindowmstr);
		storedispensewindowmstr.setStoremstr(this);

		return storedispensewindowmstr;
	}

	public Storedispensewindowmstr removeStoredispensewindowmstr(Storedispensewindowmstr storedispensewindowmstr) {
		getStoredispensewindowmstrs().remove(storedispensewindowmstr);
		storedispensewindowmstr.setStoremstr(null);

		return storedispensewindowmstr;
	}

	public List<Storeitem> getStoreitems() {
		return this.storeitems;
	}

	public void setStoreitems(List<Storeitem> storeitems) {
		this.storeitems = storeitems;
	}

	public Storeitem addStoreitem(Storeitem storeitem) {
		getStoreitems().add(storeitem);
		storeitem.setStoremstr(this);

		return storeitem;
	}

	public Storeitem removeStoreitem(Storeitem storeitem) {
		getStoreitems().remove(storeitem);
		storeitem.setStoremstr(null);

		return storeitem;
	}

	public List<Storemanagementprofile> getStoremanagementprofiles() {
		return this.storemanagementprofiles;
	}

	public void setStoremanagementprofiles(List<Storemanagementprofile> storemanagementprofiles) {
		this.storemanagementprofiles = storemanagementprofiles;
	}

	public Storemanagementprofile addStoremanagementprofile(Storemanagementprofile storemanagementprofile) {
		getStoremanagementprofiles().add(storemanagementprofile);
		storemanagementprofile.setStoremstr(this);

		return storemanagementprofile;
	}

	public Storemanagementprofile removeStoremanagementprofile(Storemanagementprofile storemanagementprofile) {
		getStoremanagementprofiles().remove(storemanagementprofile);
		storemanagementprofile.setStoremstr(null);

		return storemanagementprofile;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

	public List<Storemstr> getStoremstrs() {
		return this.storemstrs;
	}

	public void setStoremstrs(List<Storemstr> storemstrs) {
		this.storemstrs = storemstrs;
	}

	public Storemstr addStoremstr(Storemstr storemstr) {
		getStoremstrs().add(storemstr);
		storemstr.setStoremstr(this);

		return storemstr;
	}

	public Storemstr removeStoremstr(Storemstr storemstr) {
		getStoremstrs().remove(storemstr);
		storemstr.setStoremstr(null);

		return storemstr;
	}

	public List<Storesupplystore> getStoresupplystores1() {
		return this.storesupplystores1;
	}

	public void setStoresupplystores1(List<Storesupplystore> storesupplystores1) {
		this.storesupplystores1 = storesupplystores1;
	}

	public Storesupplystore addStoresupplystores1(Storesupplystore storesupplystores1) {
		getStoresupplystores1().add(storesupplystores1);
		storesupplystores1.setStoremstr1(this);

		return storesupplystores1;
	}

	public Storesupplystore removeStoresupplystores1(Storesupplystore storesupplystores1) {
		getStoresupplystores1().remove(storesupplystores1);
		storesupplystores1.setStoremstr1(null);

		return storesupplystores1;
	}

	public List<Storesupplystore> getStoresupplystores2() {
		return this.storesupplystores2;
	}

	public void setStoresupplystores2(List<Storesupplystore> storesupplystores2) {
		this.storesupplystores2 = storesupplystores2;
	}

	public Storesupplystore addStoresupplystores2(Storesupplystore storesupplystores2) {
		getStoresupplystores2().add(storesupplystores2);
		storesupplystores2.setStoremstr2(this);

		return storesupplystores2;
	}

	public Storesupplystore removeStoresupplystores2(Storesupplystore storesupplystores2) {
		getStoresupplystores2().remove(storesupplystores2);
		storesupplystores2.setStoremstr2(null);

		return storesupplystores2;
	}

	public List<Wardsspmstocktxn> getWardsspmstocktxns() {
		return this.wardsspmstocktxns;
	}

	public void setWardsspmstocktxns(List<Wardsspmstocktxn> wardsspmstocktxns) {
		this.wardsspmstocktxns = wardsspmstocktxns;
	}

	public Wardsspmstocktxn addWardsspmstocktxn(Wardsspmstocktxn wardsspmstocktxn) {
		getWardsspmstocktxns().add(wardsspmstocktxn);
		wardsspmstocktxn.setStoremstr(this);

		return wardsspmstocktxn;
	}

	public Wardsspmstocktxn removeWardsspmstocktxn(Wardsspmstocktxn wardsspmstocktxn) {
		getWardsspmstocktxns().remove(wardsspmstocktxn);
		wardsspmstocktxn.setStoremstr(null);

		return wardsspmstocktxn;
	}

}