package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DEPOSITOVERDRAFT database table.
 * 
 */
@Entity
@NamedQuery(name="Depositoverdraft.findAll", query="SELECT d FROM Depositoverdraft d")
public class Depositoverdraft implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DEPOSITOVERDRAFT_ID")
	private long depositoverdraftId;

	@Column(name="APPROVAL_CANCEL_REASON")
	private String approvalCancelReason;

	@Column(name="APPROVAL_NOTE")
	private String approvalNote;

	@Column(name="APPROVED_BY")
	private BigDecimal approvedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="APPROVED_DATETIME")
	private Date approvedDatetime;

	@Column(name="APPROVED_PERSON_NAME")
	private String approvedPersonName;

	@Column(name="DEPOSITOVERDRAFT_NO")
	private String depositoverdraftNo;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATETIME")
	private Date enteredDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="OVERDRAFT_AMOUNT")
	private BigDecimal overdraftAmount;

	@Column(name="OVERDRAFT_PERCENT")
	private BigDecimal overdraftPercent;

	@Column(name="OVERDRAFT_PERIOD")
	private BigDecimal overdraftPeriod;

	@Column(name="OVERDRAFT_REASON")
	private String overdraftReason;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REQUEST_CANCEL_REASON")
	private String requestCancelReason;

	@Column(name="REQUEST_STATUS")
	private String requestStatus;

	@Column(name="REQUEST_TYPE")
	private String requestType;

	@Column(name="REQUESTED_BY")
	private BigDecimal requestedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="REQUESTED_DATETIME")
	private Date requestedDatetime;

	@Column(name="REQUESTED_PERSON_NAME")
	private String requestedPersonName;

	@Column(name="STAKEHOLDER_ID")
	private BigDecimal stakeholderId;

	//bi-directional many-to-one association to Patientaccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNT_ID")
	private Patientaccount patientaccount;

	public Depositoverdraft() {
	}

	public long getDepositoverdraftId() {
		return this.depositoverdraftId;
	}

	public void setDepositoverdraftId(long depositoverdraftId) {
		this.depositoverdraftId = depositoverdraftId;
	}

	public String getApprovalCancelReason() {
		return this.approvalCancelReason;
	}

	public void setApprovalCancelReason(String approvalCancelReason) {
		this.approvalCancelReason = approvalCancelReason;
	}

	public String getApprovalNote() {
		return this.approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}

	public BigDecimal getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(BigDecimal approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedDatetime() {
		return this.approvedDatetime;
	}

	public void setApprovedDatetime(Date approvedDatetime) {
		this.approvedDatetime = approvedDatetime;
	}

	public String getApprovedPersonName() {
		return this.approvedPersonName;
	}

	public void setApprovedPersonName(String approvedPersonName) {
		this.approvedPersonName = approvedPersonName;
	}

	public String getDepositoverdraftNo() {
		return this.depositoverdraftNo;
	}

	public void setDepositoverdraftNo(String depositoverdraftNo) {
		this.depositoverdraftNo = depositoverdraftNo;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredDatetime() {
		return this.enteredDatetime;
	}

	public void setEnteredDatetime(Date enteredDatetime) {
		this.enteredDatetime = enteredDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getOverdraftAmount() {
		return this.overdraftAmount;
	}

	public void setOverdraftAmount(BigDecimal overdraftAmount) {
		this.overdraftAmount = overdraftAmount;
	}

	public BigDecimal getOverdraftPercent() {
		return this.overdraftPercent;
	}

	public void setOverdraftPercent(BigDecimal overdraftPercent) {
		this.overdraftPercent = overdraftPercent;
	}

	public BigDecimal getOverdraftPeriod() {
		return this.overdraftPeriod;
	}

	public void setOverdraftPeriod(BigDecimal overdraftPeriod) {
		this.overdraftPeriod = overdraftPeriod;
	}

	public String getOverdraftReason() {
		return this.overdraftReason;
	}

	public void setOverdraftReason(String overdraftReason) {
		this.overdraftReason = overdraftReason;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRequestCancelReason() {
		return this.requestCancelReason;
	}

	public void setRequestCancelReason(String requestCancelReason) {
		this.requestCancelReason = requestCancelReason;
	}

	public String getRequestStatus() {
		return this.requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getRequestType() {
		return this.requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public BigDecimal getRequestedBy() {
		return this.requestedBy;
	}

	public void setRequestedBy(BigDecimal requestedBy) {
		this.requestedBy = requestedBy;
	}

	public Date getRequestedDatetime() {
		return this.requestedDatetime;
	}

	public void setRequestedDatetime(Date requestedDatetime) {
		this.requestedDatetime = requestedDatetime;
	}

	public String getRequestedPersonName() {
		return this.requestedPersonName;
	}

	public void setRequestedPersonName(String requestedPersonName) {
		this.requestedPersonName = requestedPersonName;
	}

	public BigDecimal getStakeholderId() {
		return this.stakeholderId;
	}

	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
	}

	public Patientaccount getPatientaccount() {
		return this.patientaccount;
	}

	public void setPatientaccount(Patientaccount patientaccount) {
		this.patientaccount = patientaccount;
	}

}