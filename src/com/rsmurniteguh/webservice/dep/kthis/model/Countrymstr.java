package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the COUNTRYMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Countrymstr.findAll", query="SELECT c FROM Countrymstr c")
public class Countrymstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTRYMSTR_ID")
	private long countrymstrId;

	@Column(name="CODE_SEQ")
	private BigDecimal codeSeq;

	@Column(name="COUNTRY_CODE")
	private String countryCode;

	@Column(name="COUNTRY_DESC")
	private String countryDesc;

	@Column(name="COUNTRY_DESC_LANG1")
	private String countryDescLang1;

	@Column(name="COUNTRY_DESC_LANG2")
	private String countryDescLang2;

	@Column(name="COUNTRY_DESC_LANG3")
	private String countryDescLang3;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SHORT_CODE")
	private String shortCode;

	//bi-directional many-to-one association to Statemstr
	@OneToMany(mappedBy="countrymstr")
	private List<Statemstr> statemstrs;

	//bi-directional many-to-one association to Vendoraddress
	@OneToMany(mappedBy="countrymstr")
	private List<Vendoraddress> vendoraddresses;

	public Countrymstr() {
	}

	public long getCountrymstrId() {
		return this.countrymstrId;
	}

	public void setCountrymstrId(long countrymstrId) {
		this.countrymstrId = countrymstrId;
	}

	public BigDecimal getCodeSeq() {
		return this.codeSeq;
	}

	public void setCodeSeq(BigDecimal codeSeq) {
		this.codeSeq = codeSeq;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryDesc() {
		return this.countryDesc;
	}

	public void setCountryDesc(String countryDesc) {
		this.countryDesc = countryDesc;
	}

	public String getCountryDescLang1() {
		return this.countryDescLang1;
	}

	public void setCountryDescLang1(String countryDescLang1) {
		this.countryDescLang1 = countryDescLang1;
	}

	public String getCountryDescLang2() {
		return this.countryDescLang2;
	}

	public void setCountryDescLang2(String countryDescLang2) {
		this.countryDescLang2 = countryDescLang2;
	}

	public String getCountryDescLang3() {
		return this.countryDescLang3;
	}

	public void setCountryDescLang3(String countryDescLang3) {
		this.countryDescLang3 = countryDescLang3;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public List<Statemstr> getStatemstrs() {
		return this.statemstrs;
	}

	public void setStatemstrs(List<Statemstr> statemstrs) {
		this.statemstrs = statemstrs;
	}

	public Statemstr addStatemstr(Statemstr statemstr) {
		getStatemstrs().add(statemstr);
		statemstr.setCountrymstr(this);

		return statemstr;
	}

	public Statemstr removeStatemstr(Statemstr statemstr) {
		getStatemstrs().remove(statemstr);
		statemstr.setCountrymstr(null);

		return statemstr;
	}

	public List<Vendoraddress> getVendoraddresses() {
		return this.vendoraddresses;
	}

	public void setVendoraddresses(List<Vendoraddress> vendoraddresses) {
		this.vendoraddresses = vendoraddresses;
	}

	public Vendoraddress addVendoraddress(Vendoraddress vendoraddress) {
		getVendoraddresses().add(vendoraddress);
		vendoraddress.setCountrymstr(this);

		return vendoraddress;
	}

	public Vendoraddress removeVendoraddress(Vendoraddress vendoraddress) {
		getVendoraddresses().remove(vendoraddress);
		vendoraddress.setCountrymstr(null);

		return vendoraddress;
	}

}