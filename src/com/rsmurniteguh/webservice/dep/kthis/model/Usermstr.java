package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the USERMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Usermstr.findAll", query="SELECT u FROM Usermstr u")
public class Usermstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="USERMSTR_ID")
	private long usermstrId;

	@Column(name="CUSTOM_PROPERTIES")
	private String customProperties;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_END_DATE")
	private Date effectiveEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_START_DATE")
	private Date effectiveStartDate;

	private String email;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_LOGOFF_DATETIME")
	private Date lastLogoffDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_LOGON_DATETIME")
	private Date lastLogonDatetime;

	@Column(name="LAST_LOGON_FROM")
	private String lastLogonFrom;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="LOCKED_IND")
	private String lockedInd;

	@Column(name="LOGON_FAILED_COUNTER")
	private BigDecimal logonFailedCounter;

	@Column(name="LOGON_SUCCESS_COUNTER")
	private BigDecimal logonSuccessCounter;

	@Column(name="MAX_LOGON")
	private BigDecimal maxLogon;

	private String password;

	@Temporal(TemporalType.DATE)
	@Column(name="PASSWORD_CHANGED_DATETIME")
	private Date passwordChangedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="PASSWORD_EXPIRY_DATE")
	private Date passwordExpiryDate;

	@Column(name="PASSWORD_RESET_IND")
	private String passwordResetInd;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String timezone;

	@Column(name="USER_CODE")
	private String userCode;

	@Column(name="USER_NAME")
	private String userName;

	//bi-directional many-to-one association to Userprofile
	@OneToMany(mappedBy="usermstr")
	private List<Userprofile> userprofiles;

	//bi-directional many-to-one association to UserLocation
	@OneToMany(mappedBy="usermstr")
	private List<UserLocation> userLocations;

	//bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy="usermstr")
	private List<UserRole> userRoles;

	public Usermstr() {
	}

	public long getUsermstrId() {
		return this.usermstrId;
	}

	public void setUsermstrId(long usermstrId) {
		this.usermstrId = usermstrId;
	}

	public String getCustomProperties() {
		return this.customProperties;
	}

	public void setCustomProperties(String customProperties) {
		this.customProperties = customProperties;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffectiveEndDate() {
		return this.effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public Date getEffectiveStartDate() {
		return this.effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getLastLogoffDatetime() {
		return this.lastLogoffDatetime;
	}

	public void setLastLogoffDatetime(Date lastLogoffDatetime) {
		this.lastLogoffDatetime = lastLogoffDatetime;
	}

	public Date getLastLogonDatetime() {
		return this.lastLogonDatetime;
	}

	public void setLastLogonDatetime(Date lastLogonDatetime) {
		this.lastLogonDatetime = lastLogonDatetime;
	}

	public String getLastLogonFrom() {
		return this.lastLogonFrom;
	}

	public void setLastLogonFrom(String lastLogonFrom) {
		this.lastLogonFrom = lastLogonFrom;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getLockedInd() {
		return this.lockedInd;
	}

	public void setLockedInd(String lockedInd) {
		this.lockedInd = lockedInd;
	}

	public BigDecimal getLogonFailedCounter() {
		return this.logonFailedCounter;
	}

	public void setLogonFailedCounter(BigDecimal logonFailedCounter) {
		this.logonFailedCounter = logonFailedCounter;
	}

	public BigDecimal getLogonSuccessCounter() {
		return this.logonSuccessCounter;
	}

	public void setLogonSuccessCounter(BigDecimal logonSuccessCounter) {
		this.logonSuccessCounter = logonSuccessCounter;
	}

	public BigDecimal getMaxLogon() {
		return this.maxLogon;
	}

	public void setMaxLogon(BigDecimal maxLogon) {
		this.maxLogon = maxLogon;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getPasswordChangedDatetime() {
		return this.passwordChangedDatetime;
	}

	public void setPasswordChangedDatetime(Date passwordChangedDatetime) {
		this.passwordChangedDatetime = passwordChangedDatetime;
	}

	public Date getPasswordExpiryDate() {
		return this.passwordExpiryDate;
	}

	public void setPasswordExpiryDate(Date passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}

	public String getPasswordResetInd() {
		return this.passwordResetInd;
	}

	public void setPasswordResetInd(String passwordResetInd) {
		this.passwordResetInd = passwordResetInd;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getTimezone() {
		return this.timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<Userprofile> getUserprofiles() {
		return this.userprofiles;
	}

	public void setUserprofiles(List<Userprofile> userprofiles) {
		this.userprofiles = userprofiles;
	}

	public Userprofile addUserprofile(Userprofile userprofile) {
		getUserprofiles().add(userprofile);
		userprofile.setUsermstr(this);

		return userprofile;
	}

	public Userprofile removeUserprofile(Userprofile userprofile) {
		getUserprofiles().remove(userprofile);
		userprofile.setUsermstr(null);

		return userprofile;
	}

	public List<UserLocation> getUserLocations() {
		return this.userLocations;
	}

	public void setUserLocations(List<UserLocation> userLocations) {
		this.userLocations = userLocations;
	}

	public UserLocation addUserLocation(UserLocation userLocation) {
		getUserLocations().add(userLocation);
		userLocation.setUsermstr(this);

		return userLocation;
	}

	public UserLocation removeUserLocation(UserLocation userLocation) {
		getUserLocations().remove(userLocation);
		userLocation.setUsermstr(null);

		return userLocation;
	}

	public List<UserRole> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public UserRole addUserRole(UserRole userRole) {
		getUserRoles().add(userRole);
		userRole.setUsermstr(this);

		return userRole;
	}

	public UserRole removeUserRole(UserRole userRole) {
		getUserRoles().remove(userRole);
		userRole.setUsermstr(null);

		return userRole;
	}

}