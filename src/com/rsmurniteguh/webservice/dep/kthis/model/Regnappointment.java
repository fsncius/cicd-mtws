package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the REGNAPPOINTMENT database table.
 * 
 */
@Entity
@NamedQuery(name="Regnappointment.findAll", query="SELECT r FROM Regnappointment r")
public class Regnappointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REGNAPPOINTMENT_ID")
	private long regnappointmentId;

	@Column(name="APPOINTMENT_CAT")
	private String appointmentCat;

	@Temporal(TemporalType.DATE)
	@Column(name="APPOINTMENT_DATE")
	private Date appointmentDate;

	@Column(name="APPOINTMENT_STATUS")
	private String appointmentStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="APPOINTMENT_TIME")
	private Date appointmentTime;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CAREPROVIDER_ID")
	private BigDecimal careproviderId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="DIAGNOSED_DATETIME")
	private Date diagnosedDatetime;

	@Column(name="DIAGNOSIS_DESC")
	private String diagnosisDesc;

	@Column(name="DIAGNOSISMSTR_ID")
	private BigDecimal diagnosismstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENT_ID")
	private BigDecimal patientId;

	@Column(name="REGN_APPOINTMENT_NO")
	private String regnAppointmentNo;

	@Column(name="REGN_TYPE")
	private String regnType;

	private String remarks;

	@Column(name="RESOURCESCHEME_ID")
	private BigDecimal resourceschemeId;

	@Column(name="SESSIONMSTR_ID")
	private BigDecimal sessionmstrId;

	@Column(name="SUBSPECIALTYMSTR_ID")
	private BigDecimal subspecialtymstrId;

	@Column(name="VISIT_REASON")
	private String visitReason;

	//bi-directional many-to-one association to Appointment
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="APPOINTMENT_ID")
	private Appointment appointment;

	//bi-directional many-to-one association to Visitregntype
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISITREGNTYPE_ID")
	private Visitregntype visitregntype;

	//bi-directional many-to-one association to Regnappointmentrequisition
	@OneToMany(mappedBy="regnappointment")
	private List<Regnappointmentrequisition> regnappointmentrequisitions;

	public Regnappointment() {
	}

	public long getRegnappointmentId() {
		return this.regnappointmentId;
	}

	public void setRegnappointmentId(long regnappointmentId) {
		this.regnappointmentId = regnappointmentId;
	}

	public String getAppointmentCat() {
		return this.appointmentCat;
	}

	public void setAppointmentCat(String appointmentCat) {
		this.appointmentCat = appointmentCat;
	}

	public Date getAppointmentDate() {
		return this.appointmentDate;
	}

	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getAppointmentStatus() {
		return this.appointmentStatus;
	}

	public void setAppointmentStatus(String appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public Date getAppointmentTime() {
		return this.appointmentTime;
	}

	public void setAppointmentTime(Date appointmentTime) {
		this.appointmentTime = appointmentTime;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public BigDecimal getCareproviderId() {
		return this.careproviderId;
	}

	public void setCareproviderId(BigDecimal careproviderId) {
		this.careproviderId = careproviderId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getDiagnosedDatetime() {
		return this.diagnosedDatetime;
	}

	public void setDiagnosedDatetime(Date diagnosedDatetime) {
		this.diagnosedDatetime = diagnosedDatetime;
	}

	public String getDiagnosisDesc() {
		return this.diagnosisDesc;
	}

	public void setDiagnosisDesc(String diagnosisDesc) {
		this.diagnosisDesc = diagnosisDesc;
	}

	public BigDecimal getDiagnosismstrId() {
		return this.diagnosismstrId;
	}

	public void setDiagnosismstrId(BigDecimal diagnosismstrId) {
		this.diagnosismstrId = diagnosismstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPatientId() {
		return this.patientId;
	}

	public void setPatientId(BigDecimal patientId) {
		this.patientId = patientId;
	}

	public String getRegnAppointmentNo() {
		return this.regnAppointmentNo;
	}

	public void setRegnAppointmentNo(String regnAppointmentNo) {
		this.regnAppointmentNo = regnAppointmentNo;
	}

	public String getRegnType() {
		return this.regnType;
	}

	public void setRegnType(String regnType) {
		this.regnType = regnType;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getResourceschemeId() {
		return this.resourceschemeId;
	}

	public void setResourceschemeId(BigDecimal resourceschemeId) {
		this.resourceschemeId = resourceschemeId;
	}

	public BigDecimal getSessionmstrId() {
		return this.sessionmstrId;
	}

	public void setSessionmstrId(BigDecimal sessionmstrId) {
		this.sessionmstrId = sessionmstrId;
	}

	public BigDecimal getSubspecialtymstrId() {
		return this.subspecialtymstrId;
	}

	public void setSubspecialtymstrId(BigDecimal subspecialtymstrId) {
		this.subspecialtymstrId = subspecialtymstrId;
	}

	public String getVisitReason() {
		return this.visitReason;
	}

	public void setVisitReason(String visitReason) {
		this.visitReason = visitReason;
	}

	public Appointment getAppointment() {
		return this.appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	public Visitregntype getVisitregntype() {
		return this.visitregntype;
	}

	public void setVisitregntype(Visitregntype visitregntype) {
		this.visitregntype = visitregntype;
	}

	public List<Regnappointmentrequisition> getRegnappointmentrequisitions() {
		return this.regnappointmentrequisitions;
	}

	public void setRegnappointmentrequisitions(List<Regnappointmentrequisition> regnappointmentrequisitions) {
		this.regnappointmentrequisitions = regnappointmentrequisitions;
	}

	public Regnappointmentrequisition addRegnappointmentrequisition(Regnappointmentrequisition regnappointmentrequisition) {
		getRegnappointmentrequisitions().add(regnappointmentrequisition);
		regnappointmentrequisition.setRegnappointment(this);

		return regnappointmentrequisition;
	}

	public Regnappointmentrequisition removeRegnappointmentrequisition(Regnappointmentrequisition regnappointmentrequisition) {
		getRegnappointmentrequisitions().remove(regnappointmentrequisition);
		regnappointmentrequisition.setRegnappointment(null);

		return regnappointmentrequisition;
	}

}