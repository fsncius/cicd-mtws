package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BILLACCOUNT database table.
 * 
 */
@Entity
@NamedQuery(name="Billaccount.findAll", query="SELECT b FROM Billaccount b")
public class Billaccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BILLACCOUNT_ID")
	private long billaccountId;

	@Column(name="ACCOUNT_BALANCE")
	private BigDecimal accountBalance;

	@Column(name="BILL_AMOUNT")
	private BigDecimal billAmount;

	@Column(name="BILL_NO")
	private String billNo;

	@Column(name="BILL_SIZE")
	private BigDecimal billSize;

	@Column(name="BILLHISTORY_ID")
	private BigDecimal billhistoryId;

	@Column(name="CO_PAYMENT_AMOUNT")
	private BigDecimal coPaymentAmount;

	@Column(name="CREDIT_TERM")
	private String creditTerm;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PAID_AMOUNT")
	private BigDecimal paidAmount;

	@Column(name="PAYMENT_TYPE")
	private String paymentType;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="TOTAL_AMOUNT_TO_PAY")
	private BigDecimal totalAmountToPay;

	//bi-directional many-to-one association to Accountdebtor
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ACCOUNTDEBTOR_ID")
	private Accountdebtor accountdebtor;

	//bi-directional many-to-one association to Bill
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BILL_ID")
	private Bill bill;

	//bi-directional many-to-one association to Billdetail
	@OneToMany(mappedBy="billaccount")
	private List<Billdetail> billdetails;

	//bi-directional many-to-one association to Billpayment
	@OneToMany(mappedBy="billaccount")
	private List<Billpayment> billpayments;

	public Billaccount() {
	}

	public long getBillaccountId() {
		return this.billaccountId;
	}

	public void setBillaccountId(long billaccountId) {
		this.billaccountId = billaccountId;
	}

	public BigDecimal getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public BigDecimal getBillAmount() {
		return this.billAmount;
	}

	public void setBillAmount(BigDecimal billAmount) {
		this.billAmount = billAmount;
	}

	public String getBillNo() {
		return this.billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public BigDecimal getBillSize() {
		return this.billSize;
	}

	public void setBillSize(BigDecimal billSize) {
		this.billSize = billSize;
	}

	public BigDecimal getBillhistoryId() {
		return this.billhistoryId;
	}

	public void setBillhistoryId(BigDecimal billhistoryId) {
		this.billhistoryId = billhistoryId;
	}

	public BigDecimal getCoPaymentAmount() {
		return this.coPaymentAmount;
	}

	public void setCoPaymentAmount(BigDecimal coPaymentAmount) {
		this.coPaymentAmount = coPaymentAmount;
	}

	public String getCreditTerm() {
		return this.creditTerm;
	}

	public void setCreditTerm(String creditTerm) {
		this.creditTerm = creditTerm;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getTotalAmountToPay() {
		return this.totalAmountToPay;
	}

	public void setTotalAmountToPay(BigDecimal totalAmountToPay) {
		this.totalAmountToPay = totalAmountToPay;
	}

	public Accountdebtor getAccountdebtor() {
		return this.accountdebtor;
	}

	public void setAccountdebtor(Accountdebtor accountdebtor) {
		this.accountdebtor = accountdebtor;
	}

	public Bill getBill() {
		return this.bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public List<Billdetail> getBilldetails() {
		return this.billdetails;
	}

	public void setBilldetails(List<Billdetail> billdetails) {
		this.billdetails = billdetails;
	}

	public Billdetail addBilldetail(Billdetail billdetail) {
		getBilldetails().add(billdetail);
		billdetail.setBillaccount(this);

		return billdetail;
	}

	public Billdetail removeBilldetail(Billdetail billdetail) {
		getBilldetails().remove(billdetail);
		billdetail.setBillaccount(null);

		return billdetail;
	}

	public List<Billpayment> getBillpayments() {
		return this.billpayments;
	}

	public void setBillpayments(List<Billpayment> billpayments) {
		this.billpayments = billpayments;
	}

	public Billpayment addBillpayment(Billpayment billpayment) {
		getBillpayments().add(billpayment);
		billpayment.setBillaccount(this);

		return billpayment;
	}

	public Billpayment removeBillpayment(Billpayment billpayment) {
		getBillpayments().remove(billpayment);
		billpayment.setBillaccount(null);

		return billpayment;
	}

}