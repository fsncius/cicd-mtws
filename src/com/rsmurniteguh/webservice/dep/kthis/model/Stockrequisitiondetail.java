package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the STOCKREQUISITIONDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Stockrequisitiondetail.findAll", query="SELECT s FROM Stockrequisitiondetail s")
public class Stockrequisitiondetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKREQUISITIONDETAIL_ID")
	private long stockrequisitiondetailId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="ISSUED_QTY")
	private BigDecimal issuedQty;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIALITEMMSTR_ID")
	private BigDecimal materialitemmstrId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="RECEIVED_QTY")
	private BigDecimal receivedQty;

	private String remarks;

	@Column(name="REQUISITION_DETAIL_STATUS")
	private String requisitionDetailStatus;

	@Column(name="REQUISITION_QTY")
	private BigDecimal requisitionQty;

	@Column(name="REQUISITION_QTY_OF_BASE_UOM")
	private BigDecimal requisitionQtyOfBaseUom;

	@Column(name="REQUISITION_UOM")
	private String requisitionUom;

	//bi-directional many-to-one association to Stockrequisition
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKREQUISITION_ID")
	private Stockrequisition stockrequisition;

	public Stockrequisitiondetail() {
	}

	public long getStockrequisitiondetailId() {
		return this.stockrequisitiondetailId;
	}

	public void setStockrequisitiondetailId(long stockrequisitiondetailId) {
		this.stockrequisitiondetailId = stockrequisitiondetailId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getIssuedQty() {
		return this.issuedQty;
	}

	public void setIssuedQty(BigDecimal issuedQty) {
		this.issuedQty = issuedQty;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaterialitemmstrId() {
		return this.materialitemmstrId;
	}

	public void setMaterialitemmstrId(BigDecimal materialitemmstrId) {
		this.materialitemmstrId = materialitemmstrId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getReceivedQty() {
		return this.receivedQty;
	}

	public void setReceivedQty(BigDecimal receivedQty) {
		this.receivedQty = receivedQty;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRequisitionDetailStatus() {
		return this.requisitionDetailStatus;
	}

	public void setRequisitionDetailStatus(String requisitionDetailStatus) {
		this.requisitionDetailStatus = requisitionDetailStatus;
	}

	public BigDecimal getRequisitionQty() {
		return this.requisitionQty;
	}

	public void setRequisitionQty(BigDecimal requisitionQty) {
		this.requisitionQty = requisitionQty;
	}

	public BigDecimal getRequisitionQtyOfBaseUom() {
		return this.requisitionQtyOfBaseUom;
	}

	public void setRequisitionQtyOfBaseUom(BigDecimal requisitionQtyOfBaseUom) {
		this.requisitionQtyOfBaseUom = requisitionQtyOfBaseUom;
	}

	public String getRequisitionUom() {
		return this.requisitionUom;
	}

	public void setRequisitionUom(String requisitionUom) {
		this.requisitionUom = requisitionUom;
	}

	public Stockrequisition getStockrequisition() {
		return this.stockrequisition;
	}

	public void setStockrequisition(Stockrequisition stockrequisition) {
		this.stockrequisition = stockrequisition;
	}

}