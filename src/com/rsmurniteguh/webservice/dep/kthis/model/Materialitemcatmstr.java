package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the MATERIALITEMCATMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Materialitemcatmstr.findAll", query="SELECT m FROM Materialitemcatmstr m")
public class Materialitemcatmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MATERIALITEMCATMSTR_ID")
	private long materialitemcatmstrId;

	@Column(name="AST_DEPRECIATIONTYPEMSTR_ID")
	private BigDecimal astDepreciationtypemstrId;

	@Column(name="CREATE_CARD_IND")
	private String createCardInd;

	@Column(name="CREATE_CARD_TYPE")
	private String createCardType;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DEPRECIATION_IND")
	private String depreciationInd;

	@Column(name="EFFECTIVE_MONTHS")
	private BigDecimal effectiveMonths;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIAL_ITEM_CAT_CODE")
	private String materialItemCatCode;

	@Column(name="MATERIAL_ITEM_CAT_DESC")
	private String materialItemCatDesc;

	@Column(name="MATERIAL_ITEM_CAT_DESC_LANG1")
	private String materialItemCatDescLang1;

	@Column(name="MATERIAL_ITEM_CAT_DESC_LANG2")
	private String materialItemCatDescLang2;

	@Column(name="MATERIAL_ITEM_CAT_DESC_LANG3")
	private String materialItemCatDescLang3;

	@Column(name="MATERIAL_ITEM_CAT_LEVEL")
	private String materialItemCatLevel;

	@Column(name="MATERIAL_ITEM_MANAGE_TYPE")
	private String materialItemManageType;

	@Column(name="MEASUREMENT_IND")
	private String measurementInd;

	@Column(name="MEASUREMENT_MONTHS")
	private BigDecimal measurementMonths;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="RESIDUAL_RATE")
	private BigDecimal residualRate;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="TRACK_IND")
	private String trackInd;

	//bi-directional many-to-one association to Materialitemcatmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PARENT_MATERIALITEMCATMSTR_ID")
	private Materialitemcatmstr materialitemcatmstr;

	//bi-directional many-to-one association to Materialitemcatmstr
	@OneToMany(mappedBy="materialitemcatmstr")
	private List<Materialitemcatmstr> materialitemcatmstrs;

	//bi-directional many-to-one association to Materialitemmstr
	@OneToMany(mappedBy="materialitemcatmstr")
	private List<Materialitemmstr> materialitemmstrs;

	public Materialitemcatmstr() {
	}

	public long getMaterialitemcatmstrId() {
		return this.materialitemcatmstrId;
	}

	public void setMaterialitemcatmstrId(long materialitemcatmstrId) {
		this.materialitemcatmstrId = materialitemcatmstrId;
	}

	public BigDecimal getAstDepreciationtypemstrId() {
		return this.astDepreciationtypemstrId;
	}

	public void setAstDepreciationtypemstrId(BigDecimal astDepreciationtypemstrId) {
		this.astDepreciationtypemstrId = astDepreciationtypemstrId;
	}

	public String getCreateCardInd() {
		return this.createCardInd;
	}

	public void setCreateCardInd(String createCardInd) {
		this.createCardInd = createCardInd;
	}

	public String getCreateCardType() {
		return this.createCardType;
	}

	public void setCreateCardType(String createCardType) {
		this.createCardType = createCardType;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDepreciationInd() {
		return this.depreciationInd;
	}

	public void setDepreciationInd(String depreciationInd) {
		this.depreciationInd = depreciationInd;
	}

	public BigDecimal getEffectiveMonths() {
		return this.effectiveMonths;
	}

	public void setEffectiveMonths(BigDecimal effectiveMonths) {
		this.effectiveMonths = effectiveMonths;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMaterialItemCatCode() {
		return this.materialItemCatCode;
	}

	public void setMaterialItemCatCode(String materialItemCatCode) {
		this.materialItemCatCode = materialItemCatCode;
	}

	public String getMaterialItemCatDesc() {
		return this.materialItemCatDesc;
	}

	public void setMaterialItemCatDesc(String materialItemCatDesc) {
		this.materialItemCatDesc = materialItemCatDesc;
	}

	public String getMaterialItemCatDescLang1() {
		return this.materialItemCatDescLang1;
	}

	public void setMaterialItemCatDescLang1(String materialItemCatDescLang1) {
		this.materialItemCatDescLang1 = materialItemCatDescLang1;
	}

	public String getMaterialItemCatDescLang2() {
		return this.materialItemCatDescLang2;
	}

	public void setMaterialItemCatDescLang2(String materialItemCatDescLang2) {
		this.materialItemCatDescLang2 = materialItemCatDescLang2;
	}

	public String getMaterialItemCatDescLang3() {
		return this.materialItemCatDescLang3;
	}

	public void setMaterialItemCatDescLang3(String materialItemCatDescLang3) {
		this.materialItemCatDescLang3 = materialItemCatDescLang3;
	}

	public String getMaterialItemCatLevel() {
		return this.materialItemCatLevel;
	}

	public void setMaterialItemCatLevel(String materialItemCatLevel) {
		this.materialItemCatLevel = materialItemCatLevel;
	}

	public String getMaterialItemManageType() {
		return this.materialItemManageType;
	}

	public void setMaterialItemManageType(String materialItemManageType) {
		this.materialItemManageType = materialItemManageType;
	}

	public String getMeasurementInd() {
		return this.measurementInd;
	}

	public void setMeasurementInd(String measurementInd) {
		this.measurementInd = measurementInd;
	}

	public BigDecimal getMeasurementMonths() {
		return this.measurementMonths;
	}

	public void setMeasurementMonths(BigDecimal measurementMonths) {
		this.measurementMonths = measurementMonths;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getResidualRate() {
		return this.residualRate;
	}

	public void setResidualRate(BigDecimal residualRate) {
		this.residualRate = residualRate;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getTrackInd() {
		return this.trackInd;
	}

	public void setTrackInd(String trackInd) {
		this.trackInd = trackInd;
	}

	public Materialitemcatmstr getMaterialitemcatmstr() {
		return this.materialitemcatmstr;
	}

	public void setMaterialitemcatmstr(Materialitemcatmstr materialitemcatmstr) {
		this.materialitemcatmstr = materialitemcatmstr;
	}

	public List<Materialitemcatmstr> getMaterialitemcatmstrs() {
		return this.materialitemcatmstrs;
	}

	public void setMaterialitemcatmstrs(List<Materialitemcatmstr> materialitemcatmstrs) {
		this.materialitemcatmstrs = materialitemcatmstrs;
	}

	public Materialitemcatmstr addMaterialitemcatmstr(Materialitemcatmstr materialitemcatmstr) {
		getMaterialitemcatmstrs().add(materialitemcatmstr);
		materialitemcatmstr.setMaterialitemcatmstr(this);

		return materialitemcatmstr;
	}

	public Materialitemcatmstr removeMaterialitemcatmstr(Materialitemcatmstr materialitemcatmstr) {
		getMaterialitemcatmstrs().remove(materialitemcatmstr);
		materialitemcatmstr.setMaterialitemcatmstr(null);

		return materialitemcatmstr;
	}

	public List<Materialitemmstr> getMaterialitemmstrs() {
		return this.materialitemmstrs;
	}

	public void setMaterialitemmstrs(List<Materialitemmstr> materialitemmstrs) {
		this.materialitemmstrs = materialitemmstrs;
	}

	public Materialitemmstr addMaterialitemmstr(Materialitemmstr materialitemmstr) {
		getMaterialitemmstrs().add(materialitemmstr);
		materialitemmstr.setMaterialitemcatmstr(this);

		return materialitemmstr;
	}

	public Materialitemmstr removeMaterialitemmstr(Materialitemmstr materialitemmstr) {
		getMaterialitemmstrs().remove(materialitemmstr);
		materialitemmstr.setMaterialitemcatmstr(null);

		return materialitemmstr;
	}

}