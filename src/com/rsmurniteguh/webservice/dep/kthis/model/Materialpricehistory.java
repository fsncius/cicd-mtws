package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MATERIALPRICEHISTORY database table.
 * 
 */
@Entity
@NamedQuery(name="Materialpricehistory.findAll", query="SELECT m FROM Materialpricehistory m")
public class Materialpricehistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MATERIALPRICEHISTORY_ID")
	private long materialpricehistoryId;

	@Column(name="ADJUSTMENT_REASON")
	private String adjustmentReason;

	@Column(name="ADJUSTMENT_REMARKS")
	private String adjustmentRemarks;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="NEW_PRICE_UOM")
	private String newPriceUom;

	@Column(name="NEW_SALES_PRICE")
	private BigDecimal newSalesPrice;

	@Column(name="NEW_WHOLESALE_PRICE")
	private BigDecimal newWholesalePrice;

	@Column(name="OLD_PRICE_UOM")
	private String oldPriceUom;

	@Column(name="OLD_SALES_PRICE")
	private BigDecimal oldSalesPrice;

	@Column(name="OLD_WHOLESALE_PRICE")
	private BigDecimal oldWholesalePrice;

	//bi-directional many-to-one association to Baseunitpricehistory
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BASEUNITPRICEHISTORY_ID")
	private Baseunitpricehistory baseunitpricehistory;

	//bi-directional many-to-one association to Materialprice
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALPRICE_ID")
	private Materialprice materialprice;

	public Materialpricehistory() {
	}

	public long getMaterialpricehistoryId() {
		return this.materialpricehistoryId;
	}

	public void setMaterialpricehistoryId(long materialpricehistoryId) {
		this.materialpricehistoryId = materialpricehistoryId;
	}

	public String getAdjustmentReason() {
		return this.adjustmentReason;
	}

	public void setAdjustmentReason(String adjustmentReason) {
		this.adjustmentReason = adjustmentReason;
	}

	public String getAdjustmentRemarks() {
		return this.adjustmentRemarks;
	}

	public void setAdjustmentRemarks(String adjustmentRemarks) {
		this.adjustmentRemarks = adjustmentRemarks;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getNewPriceUom() {
		return this.newPriceUom;
	}

	public void setNewPriceUom(String newPriceUom) {
		this.newPriceUom = newPriceUom;
	}

	public BigDecimal getNewSalesPrice() {
		return this.newSalesPrice;
	}

	public void setNewSalesPrice(BigDecimal newSalesPrice) {
		this.newSalesPrice = newSalesPrice;
	}

	public BigDecimal getNewWholesalePrice() {
		return this.newWholesalePrice;
	}

	public void setNewWholesalePrice(BigDecimal newWholesalePrice) {
		this.newWholesalePrice = newWholesalePrice;
	}

	public String getOldPriceUom() {
		return this.oldPriceUom;
	}

	public void setOldPriceUom(String oldPriceUom) {
		this.oldPriceUom = oldPriceUom;
	}

	public BigDecimal getOldSalesPrice() {
		return this.oldSalesPrice;
	}

	public void setOldSalesPrice(BigDecimal oldSalesPrice) {
		this.oldSalesPrice = oldSalesPrice;
	}

	public BigDecimal getOldWholesalePrice() {
		return this.oldWholesalePrice;
	}

	public void setOldWholesalePrice(BigDecimal oldWholesalePrice) {
		this.oldWholesalePrice = oldWholesalePrice;
	}

	public Baseunitpricehistory getBaseunitpricehistory() {
		return this.baseunitpricehistory;
	}

	public void setBaseunitpricehistory(Baseunitpricehistory baseunitpricehistory) {
		this.baseunitpricehistory = baseunitpricehistory;
	}

	public Materialprice getMaterialprice() {
		return this.materialprice;
	}

	public void setMaterialprice(Materialprice materialprice) {
		this.materialprice = materialprice;
	}

}