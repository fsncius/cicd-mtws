package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PATIENTCLASSPAYMENT database table.
 * 
 */
@Entity
@NamedQuery(name="Patientclasspayment.findAll", query="SELECT p FROM Patientclasspayment p")
public class Patientclasspayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTCLASSPAYMENT_ID")
	private long patientclasspaymentId;

	@Column(name="ACCOUNT_PAY_IND")
	private String accountPayInd;

	@Column(name="CASH_PAY_IND")
	private String cashPayInd;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFAULTOVERDRAFT_IND")
	private String defaultoverdraftInd;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	public Patientclasspayment() {
	}

	public long getPatientclasspaymentId() {
		return this.patientclasspaymentId;
	}

	public void setPatientclasspaymentId(long patientclasspaymentId) {
		this.patientclasspaymentId = patientclasspaymentId;
	}

	public String getAccountPayInd() {
		return this.accountPayInd;
	}

	public void setAccountPayInd(String accountPayInd) {
		this.accountPayInd = accountPayInd;
	}

	public String getCashPayInd() {
		return this.cashPayInd;
	}

	public void setCashPayInd(String cashPayInd) {
		this.cashPayInd = cashPayInd;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefaultoverdraftInd() {
		return this.defaultoverdraftInd;
	}

	public void setDefaultoverdraftInd(String defaultoverdraftInd) {
		this.defaultoverdraftInd = defaultoverdraftInd;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

}