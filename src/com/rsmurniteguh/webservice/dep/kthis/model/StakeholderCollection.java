package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity(name="STAKEHOLDERCOLLECTION")
public final class StakeholderCollection implements Serializable {

	public StakeholderCollection() {}

	@Transient
	private Set<String> modifiedSet = new HashSet<String>();
	public Set<String> getModifiedSet() { return modifiedSet; }

	// FIELDS

	@Id
	@Column(name="STAKEHOLDERCOLLECTION_ID")
	private BigDecimal stakeholderCollectionId;
	@Column(name="STAKEHOLDERACCOUNTTXN_ID")
	private BigDecimal stakeholderAccountTxnId;
	@Column(name="COUNTERCOLLECTION_ID")
	private BigDecimal counterCollectionId;
	@Column(name="CANCEL_REASON")
	private String cancelReason;
	@Column(name="OTHER_CANCEL_REASON")
	private String otherCancelReason;
	@Column(name="ADDRESS_1")
	private String address1;
	@Column(name="ADDRESS_2")
	private String address2;
	@Column(name="ADDRESS_3")
	private String address3;
	@Column(name="PAYEE_NAME")
	private String payeeName;
	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;
	@Column(name="PREV_UPDATED_DATETIME")
	private Timestamp prevUpdatedDateTime;
	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;
	@Column(name="LAST_UPDATED_DATETIME")
	private Timestamp lastUpdatedDateTime;

	// GETTERS

	public BigDecimal getStakeholderCollectionId() { return stakeholderCollectionId; }
	public BigDecimal getStakeholderAccountTxnId() { return stakeholderAccountTxnId; }
	public BigDecimal getCounterCollectionId() { return counterCollectionId; }
	public String getCancelReason() { return cancelReason; }
	public String getOtherCancelReason() { return otherCancelReason; }
	public String getAddress1() { return address1; }
	public String getAddress2() { return address2; }
	public String getAddress3() { return address3; }
	public String getPayeeName() { return payeeName; }
	public BigDecimal getPrevUpdatedBy() { return prevUpdatedBy; }
	public Timestamp getPrevUpdatedDateTime() { return prevUpdatedDateTime; }
	public BigDecimal getLastUpdatedBy() { return lastUpdatedBy; }
	public Timestamp getLastUpdatedDateTime() { return lastUpdatedDateTime; }

	// SETTERS

	public void setStakeholderCollectionId(BigDecimal stakeholderCollectionId) {
		this.stakeholderCollectionId = stakeholderCollectionId;
		this.modifiedSet.add("stakeholderCollectionId");
	}
	public void setStakeholderAccountTxnId(BigDecimal stakeholderAccountTxnId) {
		this.stakeholderAccountTxnId = stakeholderAccountTxnId;
		this.modifiedSet.add("stakeholderAccountTxnId");
	}
	public void setCounterCollectionId(BigDecimal counterCollectionId) {
		this.counterCollectionId = counterCollectionId;
		this.modifiedSet.add("counterCollectionId");
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
		this.modifiedSet.add("cancelReason");
	}
	public void setOtherCancelReason(String otherCancelReason) {
		this.otherCancelReason = otherCancelReason;
		this.modifiedSet.add("otherCancelReason");
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
		this.modifiedSet.add("address1");
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
		this.modifiedSet.add("address2");
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
		this.modifiedSet.add("address3");
	}
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
		this.modifiedSet.add("payeeName");
	}
	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
		this.modifiedSet.add("prevUpdatedBy");
	}
	public void setPrevUpdatedDateTime(Timestamp prevUpdatedDateTime) {
		this.prevUpdatedDateTime = prevUpdatedDateTime;
		this.modifiedSet.add("prevUpdatedDateTime");
	}
	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
		this.modifiedSet.add("lastUpdatedBy");
	}
	public void setLastUpdatedDateTime(Timestamp lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
		this.modifiedSet.add("lastUpdatedDateTime");
	}

	// RESET

	public void resetModifiedFlag(boolean flag) {
		if (flag) {
			modifiedSet.add("stakeholderCollectionId");
			modifiedSet.add("stakeholderAccountTxnId");
			modifiedSet.add("counterCollectionId");
			modifiedSet.add("cancelReason");
			modifiedSet.add("otherCancelReason");
			modifiedSet.add("address1");
			modifiedSet.add("address2");
			modifiedSet.add("address3");
			modifiedSet.add("payeeName");
			modifiedSet.add("prevUpdatedBy");
			modifiedSet.add("prevUpdatedDateTime");
			modifiedSet.add("lastUpdatedBy");
			modifiedSet.add("lastUpdatedDateTime");
		} else {
			modifiedSet.clear();
		}
	}

	public void resetModifiedFlag() {
		resetModifiedFlag(false);
	}

	// TOSTRING

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("StakeholderCollection: ");
		sb.append(stakeholderCollectionId + ",");
		sb.append(stakeholderAccountTxnId + ",");
		sb.append(counterCollectionId + ",");
		sb.append(cancelReason + ",");
		sb.append(otherCancelReason + ",");
		sb.append(address1 + ",");
		sb.append(address2 + ",");
		sb.append(address3 + ",");
		sb.append(payeeName + ",");
		sb.append(prevUpdatedBy + ",");
		sb.append(prevUpdatedDateTime + ",");
		sb.append(lastUpdatedBy + ",");
		sb.append(lastUpdatedDateTime + ",");
		sb.append("]");
		return sb.toString();
	}

}
