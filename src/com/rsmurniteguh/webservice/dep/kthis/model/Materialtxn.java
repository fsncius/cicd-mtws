package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the MATERIALTXN database table.
 * 
 */
@Entity
@NamedQuery(name="Materialtxn.findAll", query="SELECT m FROM Materialtxn m")
public class Materialtxn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MATERIALTXN_ID")
	private long materialtxnId;

	@Column(name="BASE_UOM_QTY")
	private BigDecimal baseUomQty;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="ITEM_STOCK_BALANCE")
	private BigDecimal itemStockBalance;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIALTXN_NO")
	private String materialtxnNo;

	@Column(name="MATERIALTXN_TYPE")
	private String materialtxnType;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="RETAIL_PRICE")
	private BigDecimal retailPrice;

	@Column(name="RETAIL_PRICE_UOM")
	private String retailPriceUom;

	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenuecentremstrId;

	@Column(name="STOREMSTR_ID")
	private BigDecimal storemstrId;

	@Column(name="TRANSFER_RCM_ID")
	private BigDecimal transferRcmId;

	@Column(name="TRANSFER_STOREMSTR_ID")
	private BigDecimal transferStoremstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="TXN_DATETIME")
	private Date txnDatetime;

	@Column(name="TXN_QTY")
	private BigDecimal txnQty;

	@Column(name="TXN_TOTAL_COST")
	private BigDecimal txnTotalCost;

	@Column(name="TXN_TOTAL_PRICE")
	private BigDecimal txnTotalPrice;

	@Column(name="TXN_TOTAL_WHOLESALE_PRICE")
	private BigDecimal txnTotalWholesalePrice;

	@Column(name="TXN_UOM")
	private String txnUom;

	//bi-directional many-to-one association to Materialconsumption
	@OneToMany(mappedBy="materialtxn")
	private List<Materialconsumption> materialconsumptions;

	//bi-directional many-to-one association to Materialitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMMSTR_ID")
	private Materialitemmstr materialitemmstr;

	//bi-directional many-to-one association to Materialtxndetail
	@OneToMany(mappedBy="materialtxn")
	private List<Materialtxndetail> materialtxndetails;

	//bi-directional many-to-one association to Stockcountdetail
	@OneToMany(mappedBy="materialtxn")
	private List<Stockcountdetail> stockcountdetails;



	//bi-directional many-to-one association to Wardsspmstocktxn
	@OneToMany(mappedBy="materialtxn")
	private List<Wardsspmstocktxn> wardsspmstocktxns;

	public Materialtxn() {
	}

	public long getMaterialtxnId() {
		return this.materialtxnId;
	}

	public void setMaterialtxnId(long materialtxnId) {
		this.materialtxnId = materialtxnId;
	}

	public BigDecimal getBaseUomQty() {
		return this.baseUomQty;
	}

	public void setBaseUomQty(BigDecimal baseUomQty) {
		this.baseUomQty = baseUomQty;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getItemStockBalance() {
		return this.itemStockBalance;
	}

	public void setItemStockBalance(BigDecimal itemStockBalance) {
		this.itemStockBalance = itemStockBalance;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMaterialtxnNo() {
		return this.materialtxnNo;
	}

	public void setMaterialtxnNo(String materialtxnNo) {
		this.materialtxnNo = materialtxnNo;
	}

	public String getMaterialtxnType() {
		return this.materialtxnType;
	}

	public void setMaterialtxnType(String materialtxnType) {
		this.materialtxnType = materialtxnType;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getRetailPrice() {
		return this.retailPrice;
	}

	public void setRetailPrice(BigDecimal retailPrice) {
		this.retailPrice = retailPrice;
	}

	public String getRetailPriceUom() {
		return this.retailPriceUom;
	}

	public void setRetailPriceUom(String retailPriceUom) {
		this.retailPriceUom = retailPriceUom;
	}

	public BigDecimal getRevenuecentremstrId() {
		return this.revenuecentremstrId;
	}

	public void setRevenuecentremstrId(BigDecimal revenuecentremstrId) {
		this.revenuecentremstrId = revenuecentremstrId;
	}

	public BigDecimal getStoremstrId() {
		return this.storemstrId;
	}

	public void setStoremstrId(BigDecimal storemstrId) {
		this.storemstrId = storemstrId;
	}

	public BigDecimal getTransferRcmId() {
		return this.transferRcmId;
	}

	public void setTransferRcmId(BigDecimal transferRcmId) {
		this.transferRcmId = transferRcmId;
	}

	public BigDecimal getTransferStoremstrId() {
		return this.transferStoremstrId;
	}

	public void setTransferStoremstrId(BigDecimal transferStoremstrId) {
		this.transferStoremstrId = transferStoremstrId;
	}

	public Date getTxnDatetime() {
		return this.txnDatetime;
	}

	public void setTxnDatetime(Date txnDatetime) {
		this.txnDatetime = txnDatetime;
	}

	public BigDecimal getTxnQty() {
		return this.txnQty;
	}

	public void setTxnQty(BigDecimal txnQty) {
		this.txnQty = txnQty;
	}

	public BigDecimal getTxnTotalCost() {
		return this.txnTotalCost;
	}

	public void setTxnTotalCost(BigDecimal txnTotalCost) {
		this.txnTotalCost = txnTotalCost;
	}

	public BigDecimal getTxnTotalPrice() {
		return this.txnTotalPrice;
	}

	public void setTxnTotalPrice(BigDecimal txnTotalPrice) {
		this.txnTotalPrice = txnTotalPrice;
	}

	public BigDecimal getTxnTotalWholesalePrice() {
		return this.txnTotalWholesalePrice;
	}

	public void setTxnTotalWholesalePrice(BigDecimal txnTotalWholesalePrice) {
		this.txnTotalWholesalePrice = txnTotalWholesalePrice;
	}

	public String getTxnUom() {
		return this.txnUom;
	}

	public void setTxnUom(String txnUom) {
		this.txnUom = txnUom;
	}

	public List<Materialconsumption> getMaterialconsumptions() {
		return this.materialconsumptions;
	}

	public void setMaterialconsumptions(List<Materialconsumption> materialconsumptions) {
		this.materialconsumptions = materialconsumptions;
	}

	public Materialconsumption addMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().add(materialconsumption);
		materialconsumption.setMaterialtxn(this);

		return materialconsumption;
	}

	public Materialconsumption removeMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().remove(materialconsumption);
		materialconsumption.setMaterialtxn(null);

		return materialconsumption;
	}

	public Materialitemmstr getMaterialitemmstr() {
		return this.materialitemmstr;
	}

	public void setMaterialitemmstr(Materialitemmstr materialitemmstr) {
		this.materialitemmstr = materialitemmstr;
	}

	public List<Materialtxndetail> getMaterialtxndetails() {
		return this.materialtxndetails;
	}

	public void setMaterialtxndetails(List<Materialtxndetail> materialtxndetails) {
		this.materialtxndetails = materialtxndetails;
	}

	public Materialtxndetail addMaterialtxndetail(Materialtxndetail materialtxndetail) {
		getMaterialtxndetails().add(materialtxndetail);
		materialtxndetail.setMaterialtxn(this);

		return materialtxndetail;
	}

	public Materialtxndetail removeMaterialtxndetail(Materialtxndetail materialtxndetail) {
		getMaterialtxndetails().remove(materialtxndetail);
		materialtxndetail.setMaterialtxn(null);

		return materialtxndetail;
	}


	public List<Stockcountdetail> getStockcountdetails() {
		return this.stockcountdetails;
	}

	public void setStockcountdetails(List<Stockcountdetail> stockcountdetails) {
		this.stockcountdetails = stockcountdetails;
	}

	public Stockcountdetail addStockcountdetail(Stockcountdetail stockcountdetail) {
		getStockcountdetails().add(stockcountdetail);
		stockcountdetail.setMaterialtxn(this);

		return stockcountdetail;
	}

	public Stockcountdetail removeStockcountdetail(Stockcountdetail stockcountdetail) {
		getStockcountdetails().remove(stockcountdetail);
		stockcountdetail.setMaterialtxn(null);

		return stockcountdetail;
	}

	public List<Wardsspmstocktxn> getWardsspmstocktxns() {
		return this.wardsspmstocktxns;
	}

	public void setWardsspmstocktxns(List<Wardsspmstocktxn> wardsspmstocktxns) {
		this.wardsspmstocktxns = wardsspmstocktxns;
	}

	public Wardsspmstocktxn addWardsspmstocktxn(Wardsspmstocktxn wardsspmstocktxn) {
		getWardsspmstocktxns().add(wardsspmstocktxn);
		wardsspmstocktxn.setMaterialtxn(this);

		return wardsspmstocktxn;
	}

	public Wardsspmstocktxn removeWardsspmstocktxn(Wardsspmstocktxn wardsspmstocktxn) {
		getWardsspmstocktxns().remove(wardsspmstocktxn);
		wardsspmstocktxn.setMaterialtxn(null);

		return wardsspmstocktxn;
	}

}