package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITREGNTYPECHARGE database table.
 * 
 */
@Entity
@NamedQuery(name="Visitregntypecharge.findAll", query="SELECT v FROM Visitregntypecharge v")
public class Visitregntypecharge implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITREGNTYPECHARGE_ID")
	private long visitregntypechargeId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	//bi-directional many-to-one association to Patientaccounttxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNTTXN_ID")
	private Patientaccounttxn patientaccounttxn;

	//bi-directional many-to-one association to Visitregntype
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISITREGNTYPE_ID")
	private Visitregntype visitregntype;

	public Visitregntypecharge() {
	}

	public long getVisitregntypechargeId() {
		return this.visitregntypechargeId;
	}

	public void setVisitregntypechargeId(long visitregntypechargeId) {
		this.visitregntypechargeId = visitregntypechargeId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Patientaccounttxn getPatientaccounttxn() {
		return this.patientaccounttxn;
	}

	public void setPatientaccounttxn(Patientaccounttxn patientaccounttxn) {
		this.patientaccounttxn = patientaccounttxn;
	}

	public Visitregntype getVisitregntype() {
		return this.visitregntype;
	}

	public void setVisitregntype(Visitregntype visitregntype) {
		this.visitregntype = visitregntype;
	}

}