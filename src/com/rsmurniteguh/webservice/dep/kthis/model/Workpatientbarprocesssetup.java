package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the WORKPATIENTBARPROCESSSETUP database table.
 * 
 */
@Entity
@NamedQuery(name="Workpatientbarprocesssetup.findAll", query="SELECT w FROM Workpatientbarprocesssetup w")
public class Workpatientbarprocesssetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WORKPATIENTBARPROCESSSETUP_ID")
	private long workpatientbarprocesssetupId;

	@Column(name="ACTION_CODE")
	private String actionCode;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="EDITOR_ID")
	private String editorId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PROCESS_MODE")
	private String processMode;

	private String remarks;

	public Workpatientbarprocesssetup() {
	}

	public long getWorkpatientbarprocesssetupId() {
		return this.workpatientbarprocesssetupId;
	}

	public void setWorkpatientbarprocesssetupId(long workpatientbarprocesssetupId) {
		this.workpatientbarprocesssetupId = workpatientbarprocesssetupId;
	}

	public String getActionCode() {
		return this.actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getEditorId() {
		return this.editorId;
	}

	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getProcessMode() {
		return this.processMode;
	}

	public void setProcessMode(String processMode) {
		this.processMode = processMode;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}