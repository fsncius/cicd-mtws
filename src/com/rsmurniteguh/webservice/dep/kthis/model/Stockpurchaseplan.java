package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the STOCKPURCHASEPLAN database table.
 * 
 */
@Entity
@NamedQuery(name="Stockpurchaseplan.findAll", query="SELECT s FROM Stockpurchaseplan s")
public class Stockpurchaseplan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKPURCHASEPLAN_ID")
	private long stockpurchaseplanId;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PLANNED_BY")
	private BigDecimal plannedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PLANNED_DATETIME")
	private Date plannedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PURCHASE_PLAN_NO")
	private String purchasePlanNo;

	@Column(name="PURCHASE_STATUS")
	private String purchaseStatus;

	@Column(name="RECEIPTED_BY")
	private BigDecimal receiptedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="RECEIPTED_DATETIME")
	private Date receiptedDatetime;

	private String remarks;

	//bi-directional many-to-one association to Stockpurchaserequisition
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKPURCHASEREQUISITION_ID")
	private Stockpurchaserequisition stockpurchaserequisition;

	//bi-directional many-to-one association to Stockreceipt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKRECEIPT_ID")
	private Stockreceipt stockreceipt;

	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREMSTR_ID")
	private Storemstr storemstr;

	//bi-directional many-to-one association to Vendormstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VENDORMSTR_ID")
	private Vendormstr vendormstr;

	public Stockpurchaseplan() {
	}

	public long getStockpurchaseplanId() {
		return this.stockpurchaseplanId;
	}

	public void setStockpurchaseplanId(long stockpurchaseplanId) {
		this.stockpurchaseplanId = stockpurchaseplanId;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPlannedBy() {
		return this.plannedBy;
	}

	public void setPlannedBy(BigDecimal plannedBy) {
		this.plannedBy = plannedBy;
	}

	public Date getPlannedDatetime() {
		return this.plannedDatetime;
	}

	public void setPlannedDatetime(Date plannedDatetime) {
		this.plannedDatetime = plannedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPurchasePlanNo() {
		return this.purchasePlanNo;
	}

	public void setPurchasePlanNo(String purchasePlanNo) {
		this.purchasePlanNo = purchasePlanNo;
	}

	public String getPurchaseStatus() {
		return this.purchaseStatus;
	}

	public void setPurchaseStatus(String purchaseStatus) {
		this.purchaseStatus = purchaseStatus;
	}

	public BigDecimal getReceiptedBy() {
		return this.receiptedBy;
	}

	public void setReceiptedBy(BigDecimal receiptedBy) {
		this.receiptedBy = receiptedBy;
	}

	public Date getReceiptedDatetime() {
		return this.receiptedDatetime;
	}

	public void setReceiptedDatetime(Date receiptedDatetime) {
		this.receiptedDatetime = receiptedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Stockpurchaserequisition getStockpurchaserequisition() {
		return this.stockpurchaserequisition;
	}

	public void setStockpurchaserequisition(Stockpurchaserequisition stockpurchaserequisition) {
		this.stockpurchaserequisition = stockpurchaserequisition;
	}

	public Stockreceipt getStockreceipt() {
		return this.stockreceipt;
	}

	public void setStockreceipt(Stockreceipt stockreceipt) {
		this.stockreceipt = stockreceipt;
	}

	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

	public Vendormstr getVendormstr() {
		return this.vendormstr;
	}

	public void setVendormstr(Vendormstr vendormstr) {
		this.vendormstr = vendormstr;
	}

}