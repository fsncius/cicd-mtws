package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the REGNDAILYSUMMARY database table.
 * 
 */
@Entity
@NamedQuery(name="Regndailysummary.findAll", query="SELECT r FROM Regndailysummary r")
public class Regndailysummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REGNDAILYSUMMARY_ID")
	private long regndailysummaryId;

	@Column(name="CANCELLED_AMOUNT")
	private BigDecimal cancelledAmount;

	@Column(name="CANCELLED_CLINIC_AMOUNT")
	private BigDecimal cancelledClinicAmount;

	@Column(name="CANCELLED_CNT")
	private BigDecimal cancelledCnt;

	@Column(name="CLINIC_AMOUNT")
	private BigDecimal clinicAmount;

	@Column(name="CREATED_CANCELLED_BY")
	private BigDecimal createdCancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="REGISTERED_AMOUNT")
	private BigDecimal registeredAmount;

	@Column(name="REGISTERED_CNT")
	private BigDecimal registeredCnt;

	@Column(name="REGISTRATION_TYPE")
	private String registrationType;

	@Column(name="SESSIONMSTR_ID")
	private BigDecimal sessionmstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="STAT_DATE")
	private Date statDate;

	@Column(name="VISIT_TYPE")
	private String visitType;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CAREPROVIDER_ID")
	private Careprovider careprovider;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	public Regndailysummary() {
	}

	public long getRegndailysummaryId() {
		return this.regndailysummaryId;
	}

	public void setRegndailysummaryId(long regndailysummaryId) {
		this.regndailysummaryId = regndailysummaryId;
	}

	public BigDecimal getCancelledAmount() {
		return this.cancelledAmount;
	}

	public void setCancelledAmount(BigDecimal cancelledAmount) {
		this.cancelledAmount = cancelledAmount;
	}

	public BigDecimal getCancelledClinicAmount() {
		return this.cancelledClinicAmount;
	}

	public void setCancelledClinicAmount(BigDecimal cancelledClinicAmount) {
		this.cancelledClinicAmount = cancelledClinicAmount;
	}

	public BigDecimal getCancelledCnt() {
		return this.cancelledCnt;
	}

	public void setCancelledCnt(BigDecimal cancelledCnt) {
		this.cancelledCnt = cancelledCnt;
	}

	public BigDecimal getClinicAmount() {
		return this.clinicAmount;
	}

	public void setClinicAmount(BigDecimal clinicAmount) {
		this.clinicAmount = clinicAmount;
	}

	public BigDecimal getCreatedCancelledBy() {
		return this.createdCancelledBy;
	}

	public void setCreatedCancelledBy(BigDecimal createdCancelledBy) {
		this.createdCancelledBy = createdCancelledBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public BigDecimal getRegisteredAmount() {
		return this.registeredAmount;
	}

	public void setRegisteredAmount(BigDecimal registeredAmount) {
		this.registeredAmount = registeredAmount;
	}

	public BigDecimal getRegisteredCnt() {
		return this.registeredCnt;
	}

	public void setRegisteredCnt(BigDecimal registeredCnt) {
		this.registeredCnt = registeredCnt;
	}

	public String getRegistrationType() {
		return this.registrationType;
	}

	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}

	public BigDecimal getSessionmstrId() {
		return this.sessionmstrId;
	}

	public void setSessionmstrId(BigDecimal sessionmstrId) {
		this.sessionmstrId = sessionmstrId;
	}

	public Date getStatDate() {
		return this.statDate;
	}

	public void setStatDate(Date statDate) {
		this.statDate = statDate;
	}

	public String getVisitType() {
		return this.visitType;
	}

	public void setVisitType(String visitType) {
		this.visitType = visitType;
	}

	public Careprovider getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(Careprovider careprovider) {
		this.careprovider = careprovider;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

}