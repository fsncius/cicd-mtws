package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CHARGEMATERIALCOMP database table.
 * 
 */
@Entity
@NamedQuery(name="Chargematerialcomp.findAll", query="SELECT c FROM Chargematerialcomp c")
public class Chargematerialcomp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGEMATERIALCOMP_ID")
	private long chargematerialcompId;

	@Column(name="CONSUME_REVENUECENTREMSTR_ID")
	private BigDecimal consumeRevenuecentremstrId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="ORDER_LEVEL")
	private String orderLevel;

	@Column(name="ORDER_LEVEL_CODE")
	private String orderLevelCode;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	private String remarks;

	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenuecentremstrId;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr1;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENT_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr2;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENT_WARDMSTR_ID")
	private Wardmstr wardmstr;

	//bi-directional many-to-one association to Chargematerialcompdetail
	@OneToMany(mappedBy="chargematerialcomp")
	private List<Chargematerialcompdetail> chargematerialcompdetails;

	public Chargematerialcomp() {
	}

	public long getChargematerialcompId() {
		return this.chargematerialcompId;
	}

	public void setChargematerialcompId(long chargematerialcompId) {
		this.chargematerialcompId = chargematerialcompId;
	}

	public BigDecimal getConsumeRevenuecentremstrId() {
		return this.consumeRevenuecentremstrId;
	}

	public void setConsumeRevenuecentremstrId(BigDecimal consumeRevenuecentremstrId) {
		this.consumeRevenuecentremstrId = consumeRevenuecentremstrId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getOrderLevel() {
		return this.orderLevel;
	}

	public void setOrderLevel(String orderLevel) {
		this.orderLevel = orderLevel;
	}

	public String getOrderLevelCode() {
		return this.orderLevelCode;
	}

	public void setOrderLevelCode(String orderLevelCode) {
		this.orderLevelCode = orderLevelCode;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getRevenuecentremstrId() {
		return this.revenuecentremstrId;
	}

	public void setRevenuecentremstrId(BigDecimal revenuecentremstrId) {
		this.revenuecentremstrId = revenuecentremstrId;
	}

	public Subspecialtymstr getSubspecialtymstr1() {
		return this.subspecialtymstr1;
	}

	public void setSubspecialtymstr1(Subspecialtymstr subspecialtymstr1) {
		this.subspecialtymstr1 = subspecialtymstr1;
	}

	public Subspecialtymstr getSubspecialtymstr2() {
		return this.subspecialtymstr2;
	}

	public void setSubspecialtymstr2(Subspecialtymstr subspecialtymstr2) {
		this.subspecialtymstr2 = subspecialtymstr2;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

	public List<Chargematerialcompdetail> getChargematerialcompdetails() {
		return this.chargematerialcompdetails;
	}

	public void setChargematerialcompdetails(List<Chargematerialcompdetail> chargematerialcompdetails) {
		this.chargematerialcompdetails = chargematerialcompdetails;
	}

	public Chargematerialcompdetail addChargematerialcompdetail(Chargematerialcompdetail chargematerialcompdetail) {
		getChargematerialcompdetails().add(chargematerialcompdetail);
		chargematerialcompdetail.setChargematerialcomp(this);

		return chargematerialcompdetail;
	}

	public Chargematerialcompdetail removeChargematerialcompdetail(Chargematerialcompdetail chargematerialcompdetail) {
		getChargematerialcompdetails().remove(chargematerialcompdetail);
		chargematerialcompdetail.setChargematerialcomp(null);

		return chargematerialcompdetail;
	}

}