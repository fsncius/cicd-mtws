package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the RECEIPT database table.
 * 
 */
@Entity
@NamedQuery(name="Receipt.findAll", query="SELECT r FROM Receipt r")
public class Receipt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RECEIPT_ID")
	private BigDecimal receiptId;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATETIME")
	private Date enteredDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PAYEE_NAME")
	private String payeeName;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="RECEIPT_CAT")
	private String receiptCat;

	@Column(name="RECEIPT_STATUS")
	private String receiptStatus;

	@Column(name="RECEIPT_TYPE")
	private String receiptType;

	@Column(name="STAKEHOLDER_ID")
	private BigDecimal stakeholderId;

	@Column(name="SYSTEM_RECEIPT_NO")
	private String systemReceiptNo;

	@Column(name="USER_RECEIPT_NO")
	private String userReceiptNo;

	//bi-directional many-to-one association to Bill
	@OneToMany(mappedBy="receipt")
	private List<Bill> bills;

	//bi-directional many-to-one association to Billreceipt
	@OneToMany(mappedBy="receipt")
	private List<Billreceipt> billreceipts;

	//bi-directional many-to-one association to Countercollection
	@OneToMany(mappedBy="receipt")
	private List<Countercollection> countercollections;

	//bi-directional many-to-one association to Deposit
	@OneToMany(mappedBy="receipt")
	private List<Deposit> deposits;

	//bi-directional many-to-one association to Receipt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PARENT_RECEIPT_ID")
	private Receipt receipt;

	//bi-directional many-to-one association to Receipt
	@OneToMany(mappedBy="receipt")
	private List<Receipt> receipts;

	//bi-directional many-to-one association to Receipthistory
	@OneToMany(mappedBy="receipt")
	private List<Receipthistory> receipthistories;

	public Receipt() {
	}

	public BigDecimal getReceiptId() {
		return this.receiptId;
	}

	public void setReceiptId(BigDecimal receiptId) {
		this.receiptId = receiptId;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredDatetime() {
		return this.enteredDatetime;
	}

	public void setEnteredDatetime(Date enteredDatetime) {
		this.enteredDatetime = enteredDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPayeeName() {
		return this.payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getReceiptCat() {
		return this.receiptCat;
	}

	public void setReceiptCat(String receiptCat) {
		this.receiptCat = receiptCat;
	}

	public String getReceiptStatus() {
		return this.receiptStatus;
	}

	public void setReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}

	public String getReceiptType() {
		return this.receiptType;
	}

	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}

	public BigDecimal getStakeholderId() {
		return this.stakeholderId;
	}

	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
	}

	public String getSystemReceiptNo() {
		return this.systemReceiptNo;
	}

	public void setSystemReceiptNo(String systemReceiptNo) {
		this.systemReceiptNo = systemReceiptNo;
	}

	public String getUserReceiptNo() {
		return this.userReceiptNo;
	}

	public void setUserReceiptNo(String userReceiptNo) {
		this.userReceiptNo = userReceiptNo;
	}

	public List<Bill> getBills() {
		return this.bills;
	}

	public void setBills(List<Bill> bills) {
		this.bills = bills;
	}

	public Bill addBill(Bill bill) {
		getBills().add(bill);
		bill.setReceipt(this);

		return bill;
	}

	public Bill removeBill(Bill bill) {
		getBills().remove(bill);
		bill.setReceipt(null);

		return bill;
	}

	public List<Billreceipt> getBillreceipts() {
		return this.billreceipts;
	}

	public void setBillreceipts(List<Billreceipt> billreceipts) {
		this.billreceipts = billreceipts;
	}

	public Billreceipt addBillreceipt(Billreceipt billreceipt) {
		getBillreceipts().add(billreceipt);
		billreceipt.setReceipt(this);

		return billreceipt;
	}

	public Billreceipt removeBillreceipt(Billreceipt billreceipt) {
		getBillreceipts().remove(billreceipt);
		billreceipt.setReceipt(null);

		return billreceipt;
	}

	public List<Countercollection> getCountercollections() {
		return this.countercollections;
	}

	public void setCountercollections(List<Countercollection> countercollections) {
		this.countercollections = countercollections;
	}

	public Countercollection addCountercollection(Countercollection countercollection) {
		getCountercollections().add(countercollection);
		countercollection.setReceipt(this);

		return countercollection;
	}

	public Countercollection removeCountercollection(Countercollection countercollection) {
		getCountercollections().remove(countercollection);
		countercollection.setReceipt(null);

		return countercollection;
	}

	public List<Deposit> getDeposits() {
		return this.deposits;
	}

	public void setDeposits(List<Deposit> deposits) {
		this.deposits = deposits;
	}

	public Deposit addDeposit(Deposit deposit) {
		getDeposits().add(deposit);
		deposit.setReceipt(this);

		return deposit;
	}

	public Deposit removeDeposit(Deposit deposit) {
		getDeposits().remove(deposit);
		deposit.setReceipt(null);

		return deposit;
	}

	public Receipt getReceipt() {
		return this.receipt;
	}

	public void setReceipt(Receipt receipt) {
		this.receipt = receipt;
	}

	public List<Receipt> getReceipts() {
		return this.receipts;
	}

	public void setReceipts(List<Receipt> receipts) {
		this.receipts = receipts;
	}

	public Receipt addReceipt(Receipt receipt) {
		getReceipts().add(receipt);
		receipt.setReceipt(this);

		return receipt;
	}

	public Receipt removeReceipt(Receipt receipt) {
		getReceipts().remove(receipt);
		receipt.setReceipt(null);

		return receipt;
	}

	public List<Receipthistory> getReceipthistories() {
		return this.receipthistories;
	}

	public void setReceipthistories(List<Receipthistory> receipthistories) {
		this.receipthistories = receipthistories;
	}

	public Receipthistory addReceipthistory(Receipthistory receipthistory) {
		getReceipthistories().add(receipthistory);
		receipthistory.setReceipt(this);

		return receipthistory;
	}

	public Receipthistory removeReceipthistory(Receipthistory receipthistory) {
		getReceipthistories().remove(receipthistory);
		receipthistory.setReceipt(null);

		return receipthistory;
	}

}