package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the VENDORMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Vendormstr.findAll", query="SELECT v FROM Vendormstr v")
public class Vendormstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VENDORMSTR_ID")
	private long vendormstrId;

	private String area;

	@Column(name="BANK_ACCOUNT_NO")
	private String bankAccountNo;

	@Column(name="CREDIT_LEVEL")
	private String creditLevel;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="NEW_VENDOR_IND")
	private String newVendorInd;

	@Column(name="PAYMENT_MODE")
	private String paymentMode;

	@Column(name="PAYMENT_TERM")
	private String paymentTerm;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="VENDOR_CODE")
	private String vendorCode;

	@Column(name="VENDOR_NAME")
	private String vendorName;

	@Column(name="VENDOR_NAME_LANG1")
	private String vendorNameLang1;

	@Column(name="VENDOR_NAME_LANG2")
	private String vendorNameLang2;

	@Column(name="VENDOR_NAME_LANG3")
	private String vendorNameLang3;

	@Column(name="VENDOR_REFERENCE_CODE")
	private String vendorReferenceCode;

	@Column(name="VENDOR_TYPE")
	private String vendorType;


	//bi-directional many-to-one association to Stockcountdetail
	@OneToMany(mappedBy="vendormstr")
	private List<Stockcountdetail> stockcountdetails;


	//bi-directional many-to-one association to Stockpurchaseplan
	@OneToMany(mappedBy="vendormstr")
	private List<Stockpurchaseplan> stockpurchaseplans;



	//bi-directional many-to-one association to Storeitemstock
	@OneToMany(mappedBy="vendormstr")
	private List<Storeitemstock> storeitemstocks;

	//bi-directional many-to-one association to Vendoritem
	@OneToMany(mappedBy="vendormstr")
	private List<Vendoritem> vendoritems;

	//bi-directional many-to-one association to Bankmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BANKMSTR_ID")
	private Bankmstr bankmstr;

	public Vendormstr() {
	}

	public long getVendormstrId() {
		return this.vendormstrId;
	}

	public void setVendormstrId(long vendormstrId) {
		this.vendormstrId = vendormstrId;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getCreditLevel() {
		return this.creditLevel;
	}

	public void setCreditLevel(String creditLevel) {
		this.creditLevel = creditLevel;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getNewVendorInd() {
		return this.newVendorInd;
	}

	public void setNewVendorInd(String newVendorInd) {
		this.newVendorInd = newVendorInd;
	}

	public String getPaymentMode() {
		return this.paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getPaymentTerm() {
		return this.paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getVendorCode() {
		return this.vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getVendorName() {
		return this.vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorNameLang1() {
		return this.vendorNameLang1;
	}

	public void setVendorNameLang1(String vendorNameLang1) {
		this.vendorNameLang1 = vendorNameLang1;
	}

	public String getVendorNameLang2() {
		return this.vendorNameLang2;
	}

	public void setVendorNameLang2(String vendorNameLang2) {
		this.vendorNameLang2 = vendorNameLang2;
	}

	public String getVendorNameLang3() {
		return this.vendorNameLang3;
	}

	public void setVendorNameLang3(String vendorNameLang3) {
		this.vendorNameLang3 = vendorNameLang3;
	}

	public String getVendorReferenceCode() {
		return this.vendorReferenceCode;
	}

	public void setVendorReferenceCode(String vendorReferenceCode) {
		this.vendorReferenceCode = vendorReferenceCode;
	}

	public String getVendorType() {
		return this.vendorType;
	}

	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}


	public List<Stockcountdetail> getStockcountdetails() {
		return this.stockcountdetails;
	}

	public void setStockcountdetails(List<Stockcountdetail> stockcountdetails) {
		this.stockcountdetails = stockcountdetails;
	}

	public Stockcountdetail addStockcountdetail(Stockcountdetail stockcountdetail) {
		getStockcountdetails().add(stockcountdetail);
		stockcountdetail.setVendormstr(this);

		return stockcountdetail;
	}

	public Stockcountdetail removeStockcountdetail(Stockcountdetail stockcountdetail) {
		getStockcountdetails().remove(stockcountdetail);
		stockcountdetail.setVendormstr(null);

		return stockcountdetail;
	}


	public List<Stockpurchaseplan> getStockpurchaseplans() {
		return this.stockpurchaseplans;
	}

	public void setStockpurchaseplans(List<Stockpurchaseplan> stockpurchaseplans) {
		this.stockpurchaseplans = stockpurchaseplans;
	}

	public Stockpurchaseplan addStockpurchaseplan(Stockpurchaseplan stockpurchaseplan) {
		getStockpurchaseplans().add(stockpurchaseplan);
		stockpurchaseplan.setVendormstr(this);

		return stockpurchaseplan;
	}

	public Stockpurchaseplan removeStockpurchaseplan(Stockpurchaseplan stockpurchaseplan) {
		getStockpurchaseplans().remove(stockpurchaseplan);
		stockpurchaseplan.setVendormstr(null);

		return stockpurchaseplan;
	}


	public List<Storeitemstock> getStoreitemstocks() {
		return this.storeitemstocks;
	}

	public void setStoreitemstocks(List<Storeitemstock> storeitemstocks) {
		this.storeitemstocks = storeitemstocks;
	}

	public Storeitemstock addStoreitemstock(Storeitemstock storeitemstock) {
		getStoreitemstocks().add(storeitemstock);
		storeitemstock.setVendormstr(this);

		return storeitemstock;
	}

	public Storeitemstock removeStoreitemstock(Storeitemstock storeitemstock) {
		getStoreitemstocks().remove(storeitemstock);
		storeitemstock.setVendormstr(null);

		return storeitemstock;
	}

	public List<Vendoritem> getVendoritems() {
		return this.vendoritems;
	}

	public void setVendoritems(List<Vendoritem> vendoritems) {
		this.vendoritems = vendoritems;
	}

	public Vendoritem addVendoritem(Vendoritem vendoritem) {
		getVendoritems().add(vendoritem);
		vendoritem.setVendormstr(this);

		return vendoritem;
	}

	public Vendoritem removeVendoritem(Vendoritem vendoritem) {
		getVendoritems().remove(vendoritem);
		vendoritem.setVendormstr(null);

		return vendoritem;
	}

	public Bankmstr getBankmstr() {
		return this.bankmstr;
	}

	public void setBankmstr(Bankmstr bankmstr) {
		this.bankmstr = bankmstr;
	}

}