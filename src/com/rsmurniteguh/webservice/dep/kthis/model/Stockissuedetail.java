package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOCKISSUEDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Stockissuedetail.findAll", query="SELECT s FROM Stockissuedetail s")
public class Stockissuedetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKISSUEDETAIL_ID")
	private long stockissuedetailId;

	@Column(name="BATCH_NO")
	private String batchNo;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="ISSUE_DETAIL_STATUS")
	private String issueDetailStatus;

	@Column(name="ISSUE_UOM")
	private String issueUom;

	@Column(name="ISSUED_QTY")
	private BigDecimal issuedQty;

	@Column(name="ISSUED_QTY_OF_BASE_UOM")
	private BigDecimal issuedQtyOfBaseUom;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MANUFACTURER_VENDORMSTR_ID")
	private BigDecimal manufacturerVendormstrId;

	@Column(name="MATERIALITEMMSTR_ID")
	private BigDecimal materialitemmstrId;

	@Column(name="MATERIALTXN_ID")
	private BigDecimal materialtxnId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="STOCKREQUISITIONDETAIL_ID")
	private BigDecimal stockrequisitiondetailId;

	@Column(name="STOREBINMSTR_ID")
	private BigDecimal storebinmstrId;

	@Column(name="TOTAL_RETURN_QTY_OF_BASE_UOM")
	private BigDecimal totalReturnQtyOfBaseUom;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Stockissue
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKISSUE_ID")
	private Stockissue stockissue;

	//bi-directional many-to-one association to Stockissuedetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="RETURN_FOR_ISSUEDETAIL_ID")
	private Stockissuedetail stockissuedetail;

	//bi-directional many-to-one association to Stockissuedetail
	@OneToMany(mappedBy="stockissuedetail")
	private List<Stockissuedetail> stockissuedetails;

	//bi-directional many-to-one association to Stockreceiptdetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKSET_ID")
	private Stockreceiptdetail stockreceiptdetail;

	public Stockissuedetail() {
	}

	public long getStockissuedetailId() {
		return this.stockissuedetailId;
	}

	public void setStockissuedetailId(long stockissuedetailId) {
		this.stockissuedetailId = stockissuedetailId;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getIssueDetailStatus() {
		return this.issueDetailStatus;
	}

	public void setIssueDetailStatus(String issueDetailStatus) {
		this.issueDetailStatus = issueDetailStatus;
	}

	public String getIssueUom() {
		return this.issueUom;
	}

	public void setIssueUom(String issueUom) {
		this.issueUom = issueUom;
	}

	public BigDecimal getIssuedQty() {
		return this.issuedQty;
	}

	public void setIssuedQty(BigDecimal issuedQty) {
		this.issuedQty = issuedQty;
	}

	public BigDecimal getIssuedQtyOfBaseUom() {
		return this.issuedQtyOfBaseUom;
	}

	public void setIssuedQtyOfBaseUom(BigDecimal issuedQtyOfBaseUom) {
		this.issuedQtyOfBaseUom = issuedQtyOfBaseUom;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getManufacturerVendormstrId() {
		return this.manufacturerVendormstrId;
	}

	public void setManufacturerVendormstrId(BigDecimal manufacturerVendormstrId) {
		this.manufacturerVendormstrId = manufacturerVendormstrId;
	}

	public BigDecimal getMaterialitemmstrId() {
		return this.materialitemmstrId;
	}

	public void setMaterialitemmstrId(BigDecimal materialitemmstrId) {
		this.materialitemmstrId = materialitemmstrId;
	}

	public BigDecimal getMaterialtxnId() {
		return this.materialtxnId;
	}

	public void setMaterialtxnId(BigDecimal materialtxnId) {
		this.materialtxnId = materialtxnId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStockrequisitiondetailId() {
		return this.stockrequisitiondetailId;
	}

	public void setStockrequisitiondetailId(BigDecimal stockrequisitiondetailId) {
		this.stockrequisitiondetailId = stockrequisitiondetailId;
	}

	public BigDecimal getStorebinmstrId() {
		return this.storebinmstrId;
	}

	public void setStorebinmstrId(BigDecimal storebinmstrId) {
		this.storebinmstrId = storebinmstrId;
	}

	public BigDecimal getTotalReturnQtyOfBaseUom() {
		return this.totalReturnQtyOfBaseUom;
	}

	public void setTotalReturnQtyOfBaseUom(BigDecimal totalReturnQtyOfBaseUom) {
		this.totalReturnQtyOfBaseUom = totalReturnQtyOfBaseUom;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Stockissue getStockissue() {
		return this.stockissue;
	}

	public void setStockissue(Stockissue stockissue) {
		this.stockissue = stockissue;
	}

	public Stockissuedetail getStockissuedetail() {
		return this.stockissuedetail;
	}

	public void setStockissuedetail(Stockissuedetail stockissuedetail) {
		this.stockissuedetail = stockissuedetail;
	}

	public List<Stockissuedetail> getStockissuedetails() {
		return this.stockissuedetails;
	}

	public void setStockissuedetails(List<Stockissuedetail> stockissuedetails) {
		this.stockissuedetails = stockissuedetails;
	}

	public Stockissuedetail addStockissuedetail(Stockissuedetail stockissuedetail) {
		getStockissuedetails().add(stockissuedetail);
		stockissuedetail.setStockissuedetail(this);

		return stockissuedetail;
	}

	public Stockissuedetail removeStockissuedetail(Stockissuedetail stockissuedetail) {
		getStockissuedetails().remove(stockissuedetail);
		stockissuedetail.setStockissuedetail(null);

		return stockissuedetail;
	}

	public Stockreceiptdetail getStockreceiptdetail() {
		return this.stockreceiptdetail;
	}

	public void setStockreceiptdetail(Stockreceiptdetail stockreceiptdetail) {
		this.stockreceiptdetail = stockreceiptdetail;
	}

}