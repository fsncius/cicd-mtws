package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the COMMONSEARCH database table.
 * 
 */
@Entity
@NamedQuery(name="Commonsearch.findAll", query="SELECT c FROM Commonsearch c")
public class Commonsearch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COMMONSEARCH_ID")
	private long commonsearchId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PRE_SUBMIT")
	private String preSubmit;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SEARCH_CODE")
	private String searchCode;

	@Column(name="SEARCH_DESC")
	private String searchDesc;

	@Column(name="SEARCH_DESC_LANG1")
	private String searchDescLang1;

	@Column(name="SEARCH_DESC_LANG2")
	private String searchDescLang2;

	@Column(name="SEARCH_DESC_LANG3")
	private String searchDescLang3;

	@Column(name="SEARCH_TYPE")
	private String searchType;

	@Column(name="SEARCH_VALUE_REQ")
	private BigDecimal searchValueReq;

	@Column(name="SHORTCUT_KEY")
	private BigDecimal shortcutKey;

	public Commonsearch() {
	}

	public long getCommonsearchId() {
		return this.commonsearchId;
	}

	public void setCommonsearchId(long commonsearchId) {
		this.commonsearchId = commonsearchId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPreSubmit() {
		return this.preSubmit;
	}

	public void setPreSubmit(String preSubmit) {
		this.preSubmit = preSubmit;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getSearchCode() {
		return this.searchCode;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public String getSearchDesc() {
		return this.searchDesc;
	}

	public void setSearchDesc(String searchDesc) {
		this.searchDesc = searchDesc;
	}

	public String getSearchDescLang1() {
		return this.searchDescLang1;
	}

	public void setSearchDescLang1(String searchDescLang1) {
		this.searchDescLang1 = searchDescLang1;
	}

	public String getSearchDescLang2() {
		return this.searchDescLang2;
	}

	public void setSearchDescLang2(String searchDescLang2) {
		this.searchDescLang2 = searchDescLang2;
	}

	public String getSearchDescLang3() {
		return this.searchDescLang3;
	}

	public void setSearchDescLang3(String searchDescLang3) {
		this.searchDescLang3 = searchDescLang3;
	}

	public String getSearchType() {
		return this.searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public BigDecimal getSearchValueReq() {
		return this.searchValueReq;
	}

	public void setSearchValueReq(BigDecimal searchValueReq) {
		this.searchValueReq = searchValueReq;
	}

	public BigDecimal getShortcutKey() {
		return this.shortcutKey;
	}

	public void setShortcutKey(BigDecimal shortcutKey) {
		this.shortcutKey = shortcutKey;
	}

}