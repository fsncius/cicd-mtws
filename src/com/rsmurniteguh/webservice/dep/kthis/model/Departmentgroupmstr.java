package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the DEPARTMENTGROUPMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Departmentgroupmstr.findAll", query="SELECT d FROM Departmentgroupmstr d")
public class Departmentgroupmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DEPARTMENTGROUPMSTR_ID")
	private long departmentgroupmstrId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DEPARTMENTGROUP_CODE")
	private String departmentgroupCode;

	@Column(name="DEPARTMENTGROUP_NAME")
	private String departmentgroupName;

	@Column(name="DEPARTMENTGROUP_NAME_LANG1")
	private String departmentgroupNameLang1;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	private BigDecimal leader;

	private String remarks;

	@Column(name="SHORT_CODE")
	private String shortCode;

	//bi-directional many-to-one association to Departmentgroupmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PARENT_DEPARTMENTGROUPMSTR_ID")
	private Departmentgroupmstr departmentgroupmstr;

	//bi-directional many-to-one association to Departmentgroupmstr
	@OneToMany(mappedBy="departmentgroupmstr")
	private List<Departmentgroupmstr> departmentgroupmstrs;

	//bi-directional many-to-one association to Departmentmstr
	@OneToMany(mappedBy="departmentgroupmstr")
	private List<Departmentmstr> departmentmstrs;

	public Departmentgroupmstr() {
	}

	public long getDepartmentgroupmstrId() {
		return this.departmentgroupmstrId;
	}

	public void setDepartmentgroupmstrId(long departmentgroupmstrId) {
		this.departmentgroupmstrId = departmentgroupmstrId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDepartmentgroupCode() {
		return this.departmentgroupCode;
	}

	public void setDepartmentgroupCode(String departmentgroupCode) {
		this.departmentgroupCode = departmentgroupCode;
	}

	public String getDepartmentgroupName() {
		return this.departmentgroupName;
	}

	public void setDepartmentgroupName(String departmentgroupName) {
		this.departmentgroupName = departmentgroupName;
	}

	public String getDepartmentgroupNameLang1() {
		return this.departmentgroupNameLang1;
	}

	public void setDepartmentgroupNameLang1(String departmentgroupNameLang1) {
		this.departmentgroupNameLang1 = departmentgroupNameLang1;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getLeader() {
		return this.leader;
	}

	public void setLeader(BigDecimal leader) {
		this.leader = leader;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Departmentgroupmstr getDepartmentgroupmstr() {
		return this.departmentgroupmstr;
	}

	public void setDepartmentgroupmstr(Departmentgroupmstr departmentgroupmstr) {
		this.departmentgroupmstr = departmentgroupmstr;
	}

	public List<Departmentgroupmstr> getDepartmentgroupmstrs() {
		return this.departmentgroupmstrs;
	}

	public void setDepartmentgroupmstrs(List<Departmentgroupmstr> departmentgroupmstrs) {
		this.departmentgroupmstrs = departmentgroupmstrs;
	}

	public Departmentgroupmstr addDepartmentgroupmstr(Departmentgroupmstr departmentgroupmstr) {
		getDepartmentgroupmstrs().add(departmentgroupmstr);
		departmentgroupmstr.setDepartmentgroupmstr(this);

		return departmentgroupmstr;
	}

	public Departmentgroupmstr removeDepartmentgroupmstr(Departmentgroupmstr departmentgroupmstr) {
		getDepartmentgroupmstrs().remove(departmentgroupmstr);
		departmentgroupmstr.setDepartmentgroupmstr(null);

		return departmentgroupmstr;
	}

	public List<Departmentmstr> getDepartmentmstrs() {
		return this.departmentmstrs;
	}

	public void setDepartmentmstrs(List<Departmentmstr> departmentmstrs) {
		this.departmentmstrs = departmentmstrs;
	}

	public Departmentmstr addDepartmentmstr(Departmentmstr departmentmstr) {
		getDepartmentmstrs().add(departmentmstr);
		departmentmstr.setDepartmentgroupmstr(this);

		return departmentmstr;
	}

	public Departmentmstr removeDepartmentmstr(Departmentmstr departmentmstr) {
		getDepartmentmstrs().remove(departmentmstr);
		departmentmstr.setDepartmentgroupmstr(null);

		return departmentmstr;
	}

}