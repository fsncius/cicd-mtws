package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the STORECOUNTDEFINEDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Storecountdefinedetail.findAll", query="SELECT s FROM Storecountdefinedetail s")
public class Storecountdefinedetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STORECOUNTDEFINEDETAIL_ID")
	private long storecountdefinedetailId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	//bi-directional many-to-one association to Materialitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMMSTR_ID")
	private Materialitemmstr materialitemmstr;

	//bi-directional many-to-one association to Storebinmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREBINMSTR_ID")
	private Storebinmstr storebinmstr;

	//bi-directional many-to-one association to Storecountdefine
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STORECOUNTDEFINE_ID")
	private Storecountdefine storecountdefine;

	public Storecountdefinedetail() {
	}

	public long getStorecountdefinedetailId() {
		return this.storecountdefinedetailId;
	}

	public void setStorecountdefinedetailId(long storecountdefinedetailId) {
		this.storecountdefinedetailId = storecountdefinedetailId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public Materialitemmstr getMaterialitemmstr() {
		return this.materialitemmstr;
	}

	public void setMaterialitemmstr(Materialitemmstr materialitemmstr) {
		this.materialitemmstr = materialitemmstr;
	}

	public Storebinmstr getStorebinmstr() {
		return this.storebinmstr;
	}

	public void setStorebinmstr(Storebinmstr storebinmstr) {
		this.storebinmstr = storebinmstr;
	}

	public Storecountdefine getStorecountdefine() {
		return this.storecountdefine;
	}

	public void setStorecountdefine(Storecountdefine storecountdefine) {
		this.storecountdefine = storecountdefine;
	}

}