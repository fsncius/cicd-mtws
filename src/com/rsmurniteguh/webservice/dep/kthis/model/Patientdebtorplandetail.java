package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PATIENTDEBTORPLANDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Patientdebtorplandetail.findAll", query="SELECT p FROM Patientdebtorplandetail p")
public class Patientdebtorplandetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTDEBTORPLANDETAIL_ID")
	private long patientdebtorplandetailId;

	@Column(name="CN_APPOINTED_HOSPITAL_IND")
	private String cnAppointedHospitalInd;

	@Column(name="CN_HEALTHCARE_PLAN_IND")
	private String cnHealthcarePlanInd;

	@Column(name="CN_HEALTHCARE_PLAN_NO")
	private String cnHealthcarePlanNo;

	@Column(name="CN_INDUSTRIAL_SICKNESS")
	private String cnIndustrialSickness;

	@Column(name="CN_MAJOR_DISEASE_IND")
	private String cnMajorDiseaseInd;

	@Column(name="CN_MAJOR_DISEASE_TYPE")
	private String cnMajorDiseaseType;

	@Column(name="CN_MIO_METHOD")
	private String cnMioMethod;

	@Column(name="CN_MIO_PERSON_GROUP")
	private String cnMioPersonGroup;

	@Column(name="CONCESSIONPLAN_ID")
	private BigDecimal concessionplanId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="FINANCIAL_CLASS")
	private String financialClass;

	@Column(name="GUARANTEELETTER_ID")
	private BigDecimal guaranteeletterId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PAYER_SEQ")
	private BigDecimal payerSeq;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="STAKEHOLDER_ID")
	private BigDecimal stakeholderId;

	//bi-directional many-to-one association to Claimpolicy
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CLAIMPOLICY_ID")
	private Claimpolicy claimpolicy;

	//bi-directional many-to-one association to Patientdebtorplan
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTDEBTORPLAN_ID")
	private Patientdebtorplan patientdebtorplan;

	public Patientdebtorplandetail() {
	}

	public long getPatientdebtorplandetailId() {
		return this.patientdebtorplandetailId;
	}

	public void setPatientdebtorplandetailId(long patientdebtorplandetailId) {
		this.patientdebtorplandetailId = patientdebtorplandetailId;
	}

	public String getCnAppointedHospitalInd() {
		return this.cnAppointedHospitalInd;
	}

	public void setCnAppointedHospitalInd(String cnAppointedHospitalInd) {
		this.cnAppointedHospitalInd = cnAppointedHospitalInd;
	}

	public String getCnHealthcarePlanInd() {
		return this.cnHealthcarePlanInd;
	}

	public void setCnHealthcarePlanInd(String cnHealthcarePlanInd) {
		this.cnHealthcarePlanInd = cnHealthcarePlanInd;
	}

	public String getCnHealthcarePlanNo() {
		return this.cnHealthcarePlanNo;
	}

	public void setCnHealthcarePlanNo(String cnHealthcarePlanNo) {
		this.cnHealthcarePlanNo = cnHealthcarePlanNo;
	}

	public String getCnIndustrialSickness() {
		return this.cnIndustrialSickness;
	}

	public void setCnIndustrialSickness(String cnIndustrialSickness) {
		this.cnIndustrialSickness = cnIndustrialSickness;
	}

	public String getCnMajorDiseaseInd() {
		return this.cnMajorDiseaseInd;
	}

	public void setCnMajorDiseaseInd(String cnMajorDiseaseInd) {
		this.cnMajorDiseaseInd = cnMajorDiseaseInd;
	}

	public String getCnMajorDiseaseType() {
		return this.cnMajorDiseaseType;
	}

	public void setCnMajorDiseaseType(String cnMajorDiseaseType) {
		this.cnMajorDiseaseType = cnMajorDiseaseType;
	}

	public String getCnMioMethod() {
		return this.cnMioMethod;
	}

	public void setCnMioMethod(String cnMioMethod) {
		this.cnMioMethod = cnMioMethod;
	}

	public String getCnMioPersonGroup() {
		return this.cnMioPersonGroup;
	}

	public void setCnMioPersonGroup(String cnMioPersonGroup) {
		this.cnMioPersonGroup = cnMioPersonGroup;
	}

	public BigDecimal getConcessionplanId() {
		return this.concessionplanId;
	}

	public void setConcessionplanId(BigDecimal concessionplanId) {
		this.concessionplanId = concessionplanId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getFinancialClass() {
		return this.financialClass;
	}

	public void setFinancialClass(String financialClass) {
		this.financialClass = financialClass;
	}

	public BigDecimal getGuaranteeletterId() {
		return this.guaranteeletterId;
	}

	public void setGuaranteeletterId(BigDecimal guaranteeletterId) {
		this.guaranteeletterId = guaranteeletterId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPayerSeq() {
		return this.payerSeq;
	}

	public void setPayerSeq(BigDecimal payerSeq) {
		this.payerSeq = payerSeq;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getStakeholderId() {
		return this.stakeholderId;
	}

	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
	}

	public Claimpolicy getClaimpolicy() {
		return this.claimpolicy;
	}

	public void setClaimpolicy(Claimpolicy claimpolicy) {
		this.claimpolicy = claimpolicy;
	}

	public Patientdebtorplan getPatientdebtorplan() {
		return this.patientdebtorplan;
	}

	public void setPatientdebtorplan(Patientdebtorplan patientdebtorplan) {
		this.patientdebtorplan = patientdebtorplan;
	}

}