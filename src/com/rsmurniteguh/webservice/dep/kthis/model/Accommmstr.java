package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ACCOMMMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Accommmstr.findAll", query="SELECT a FROM Accommmstr a")
public class Accommmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ACCOMMMSTR_ID")
	private long accommmstrId;

	@Column(name="ACCOMM_CODE")
	private String accommCode;

	@Column(name="ACCOMM_DESC")
	private String accommDesc;

	@Column(name="ACCOMM_DESC_LANG1")
	private String accommDescLang1;

	@Column(name="ACCOMM_DESC_LANG2")
	private String accommDescLang2;

	@Column(name="ACCOMM_DESC_LANG3")
	private String accommDescLang3;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	//bi-directional many-to-one association to Accommcharge
	@OneToMany(mappedBy="accommmstr")
	private List<Accommcharge> accommcharges;

	//bi-directional many-to-one association to Bedmstr
	@OneToMany(mappedBy="accommmstr")
	private List<Bedmstr> bedmstrs;

	public Accommmstr() {
	}

	public long getAccommmstrId() {
		return this.accommmstrId;
	}

	public void setAccommmstrId(long accommmstrId) {
		this.accommmstrId = accommmstrId;
	}

	public String getAccommCode() {
		return this.accommCode;
	}

	public void setAccommCode(String accommCode) {
		this.accommCode = accommCode;
	}

	public String getAccommDesc() {
		return this.accommDesc;
	}

	public void setAccommDesc(String accommDesc) {
		this.accommDesc = accommDesc;
	}

	public String getAccommDescLang1() {
		return this.accommDescLang1;
	}

	public void setAccommDescLang1(String accommDescLang1) {
		this.accommDescLang1 = accommDescLang1;
	}

	public String getAccommDescLang2() {
		return this.accommDescLang2;
	}

	public void setAccommDescLang2(String accommDescLang2) {
		this.accommDescLang2 = accommDescLang2;
	}

	public String getAccommDescLang3() {
		return this.accommDescLang3;
	}

	public void setAccommDescLang3(String accommDescLang3) {
		this.accommDescLang3 = accommDescLang3;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public List<Accommcharge> getAccommcharges() {
		return this.accommcharges;
	}

	public void setAccommcharges(List<Accommcharge> accommcharges) {
		this.accommcharges = accommcharges;
	}

	public Accommcharge addAccommcharge(Accommcharge accommcharge) {
		getAccommcharges().add(accommcharge);
		accommcharge.setAccommmstr(this);

		return accommcharge;
	}

	public Accommcharge removeAccommcharge(Accommcharge accommcharge) {
		getAccommcharges().remove(accommcharge);
		accommcharge.setAccommmstr(null);

		return accommcharge;
	}

	public List<Bedmstr> getBedmstrs() {
		return this.bedmstrs;
	}

	public void setBedmstrs(List<Bedmstr> bedmstrs) {
		this.bedmstrs = bedmstrs;
	}

	public Bedmstr addBedmstr(Bedmstr bedmstr) {
		getBedmstrs().add(bedmstr);
		bedmstr.setAccommmstr(this);

		return bedmstr;
	}

	public Bedmstr removeBedmstr(Bedmstr bedmstr) {
		getBedmstrs().remove(bedmstr);
		bedmstr.setAccommmstr(null);

		return bedmstr;
	}

}