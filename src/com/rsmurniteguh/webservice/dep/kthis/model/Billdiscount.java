package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BILLDISCOUNT database table.
 * 
 */
@Entity
@NamedQuery(name="Billdiscount.findAll", query="SELECT b FROM Billdiscount b")
public class Billdiscount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BILLDISCOUNT_ID")
	private long billdiscountId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DISCOUNT_AMOUNT")
	private BigDecimal discountAmount;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATETIME")
	private Date enteredDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="ORGIN_DISCOUNT_AMOUNT")
	private BigDecimal orginDiscountAmount;

	//bi-directional many-to-one association to Bill
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BILL_ID")
	private Bill bill;

	//bi-directional many-to-one association to Discount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DISCOUNT_ID")
	private Discount discount;

	//bi-directional many-to-one association to Billdiscountdetail
	@OneToMany(mappedBy="billdiscount")
	private List<Billdiscountdetail> billdiscountdetails;

	public Billdiscount() {
	}

	public long getBilldiscountId() {
		return this.billdiscountId;
	}

	public void setBilldiscountId(long billdiscountId) {
		this.billdiscountId = billdiscountId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredDatetime() {
		return this.enteredDatetime;
	}

	public void setEnteredDatetime(Date enteredDatetime) {
		this.enteredDatetime = enteredDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getOrginDiscountAmount() {
		return this.orginDiscountAmount;
	}

	public void setOrginDiscountAmount(BigDecimal orginDiscountAmount) {
		this.orginDiscountAmount = orginDiscountAmount;
	}

	public Bill getBill() {
		return this.bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Discount getDiscount() {
		return this.discount;
	}

	public void setDiscount(Discount discount) {
		this.discount = discount;
	}

	public List<Billdiscountdetail> getBilldiscountdetails() {
		return this.billdiscountdetails;
	}

	public void setBilldiscountdetails(List<Billdiscountdetail> billdiscountdetails) {
		this.billdiscountdetails = billdiscountdetails;
	}

	public Billdiscountdetail addBilldiscountdetail(Billdiscountdetail billdiscountdetail) {
		getBilldiscountdetails().add(billdiscountdetail);
		billdiscountdetail.setBilldiscount(this);

		return billdiscountdetail;
	}

	public Billdiscountdetail removeBilldiscountdetail(Billdiscountdetail billdiscountdetail) {
		getBilldiscountdetails().remove(billdiscountdetail);
		billdiscountdetail.setBilldiscount(null);

		return billdiscountdetail;
	}

}