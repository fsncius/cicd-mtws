package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the COUNTERTOTAL database table.
 * 
 */
@Entity
@NamedQuery(name="Countertotal.findAll", query="SELECT c FROM Countertotal c")
public class Countertotal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COUNTERTOTAL_ID")
	private long countertotalId;

	@Column(name="CANCELLED_BY")
	private BigDecimal cancelledBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CANCELLED_DATETIME")
	private Date cancelledDatetime;

	@Column(name="CLOSE_IND")
	private String closeInd;

	@Column(name="COUNTERTOTAL_NO")
	private String countertotalNo;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="HANDOVER_DATETIME")
	private Date handoverDatetime;

	@Column(name="HANDOVERGROUPMSTR_ID")
	private BigDecimal handovergroupmstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_HANDOVER_DATETIME")
	private Date lastHandoverDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="TOTAL_AMOUNT")
	private BigDecimal totalAmount;

	@Column(name="USERMSTR_ID")
	private BigDecimal usermstrId;

	//bi-directional many-to-one association to Counter
	@OneToMany(mappedBy="countertotal")
	private List<Counter> counters;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to CounterIpcharge
	@OneToMany(mappedBy="countertotal")
	private List<CounterIpcharge> counterIpcharges;

	//bi-directional many-to-one association to CounterIpchargedetail
	@OneToMany(mappedBy="countertotal")
	private List<CounterIpchargedetail> counterIpchargedetails;

	//bi-directional many-to-one association to CounterIpdeposit
	@OneToMany(mappedBy="countertotal")
	private List<CounterIpdeposit> counterIpdeposits;

	public Countertotal() {
	}

	public long getCountertotalId() {
		return this.countertotalId;
	}

	public void setCountertotalId(long countertotalId) {
		this.countertotalId = countertotalId;
	}

	public BigDecimal getCancelledBy() {
		return this.cancelledBy;
	}

	public void setCancelledBy(BigDecimal cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledDatetime() {
		return this.cancelledDatetime;
	}

	public void setCancelledDatetime(Date cancelledDatetime) {
		this.cancelledDatetime = cancelledDatetime;
	}

	public String getCloseInd() {
		return this.closeInd;
	}

	public void setCloseInd(String closeInd) {
		this.closeInd = closeInd;
	}

	public String getCountertotalNo() {
		return this.countertotalNo;
	}

	public void setCountertotalNo(String countertotalNo) {
		this.countertotalNo = countertotalNo;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getHandoverDatetime() {
		return this.handoverDatetime;
	}

	public void setHandoverDatetime(Date handoverDatetime) {
		this.handoverDatetime = handoverDatetime;
	}

	public BigDecimal getHandovergroupmstrId() {
		return this.handovergroupmstrId;
	}

	public void setHandovergroupmstrId(BigDecimal handovergroupmstrId) {
		this.handovergroupmstrId = handovergroupmstrId;
	}

	public Date getLastHandoverDatetime() {
		return this.lastHandoverDatetime;
	}

	public void setLastHandoverDatetime(Date lastHandoverDatetime) {
		this.lastHandoverDatetime = lastHandoverDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getUsermstrId() {
		return this.usermstrId;
	}

	public void setUsermstrId(BigDecimal usermstrId) {
		this.usermstrId = usermstrId;
	}

	public List<Counter> getCounters() {
		return this.counters;
	}

	public void setCounters(List<Counter> counters) {
		this.counters = counters;
	}

	public Counter addCounter(Counter counter) {
		getCounters().add(counter);
		counter.setCountertotal(this);

		return counter;
	}

	public Counter removeCounter(Counter counter) {
		getCounters().remove(counter);
		counter.setCountertotal(null);

		return counter;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public List<CounterIpcharge> getCounterIpcharges() {
		return this.counterIpcharges;
	}

	public void setCounterIpcharges(List<CounterIpcharge> counterIpcharges) {
		this.counterIpcharges = counterIpcharges;
	}

	public CounterIpcharge addCounterIpcharge(CounterIpcharge counterIpcharge) {
		getCounterIpcharges().add(counterIpcharge);
		counterIpcharge.setCountertotal(this);

		return counterIpcharge;
	}

	public CounterIpcharge removeCounterIpcharge(CounterIpcharge counterIpcharge) {
		getCounterIpcharges().remove(counterIpcharge);
		counterIpcharge.setCountertotal(null);

		return counterIpcharge;
	}

	public List<CounterIpchargedetail> getCounterIpchargedetails() {
		return this.counterIpchargedetails;
	}

	public void setCounterIpchargedetails(List<CounterIpchargedetail> counterIpchargedetails) {
		this.counterIpchargedetails = counterIpchargedetails;
	}

	public CounterIpchargedetail addCounterIpchargedetail(CounterIpchargedetail counterIpchargedetail) {
		getCounterIpchargedetails().add(counterIpchargedetail);
		counterIpchargedetail.setCountertotal(this);

		return counterIpchargedetail;
	}

	public CounterIpchargedetail removeCounterIpchargedetail(CounterIpchargedetail counterIpchargedetail) {
		getCounterIpchargedetails().remove(counterIpchargedetail);
		counterIpchargedetail.setCountertotal(null);

		return counterIpchargedetail;
	}

	public List<CounterIpdeposit> getCounterIpdeposits() {
		return this.counterIpdeposits;
	}

	public void setCounterIpdeposits(List<CounterIpdeposit> counterIpdeposits) {
		this.counterIpdeposits = counterIpdeposits;
	}

	public CounterIpdeposit addCounterIpdeposit(CounterIpdeposit counterIpdeposit) {
		getCounterIpdeposits().add(counterIpdeposit);
		counterIpdeposit.setCountertotal(this);

		return counterIpdeposit;
	}

	public CounterIpdeposit removeCounterIpdeposit(CounterIpdeposit counterIpdeposit) {
		getCounterIpdeposits().remove(counterIpdeposit);
		counterIpdeposit.setCountertotal(null);

		return counterIpdeposit;
	}

}