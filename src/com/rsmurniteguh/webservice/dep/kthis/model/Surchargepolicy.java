package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the SURCHARGEPOLICY database table.
 * 
 */
@Entity
@NamedQuery(name="Surchargepolicy.findAll", query="SELECT s FROM Surchargepolicy s")
public class Surchargepolicy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SURCHARGEPOLICY_ID")
	private long surchargepolicyId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="ORDER_LEVEL")
	private String orderLevel;

	@Column(name="ORDER_LEVEL_CODE")
	private String orderLevelCode;

	@Column(name="ORDER_STATUS")
	private String orderStatus;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String roa;

	//bi-directional many-to-one association to Surchargepolicydetail
	@OneToMany(mappedBy="surchargepolicy")
	private List<Surchargepolicydetail> surchargepolicydetails;

	public Surchargepolicy() {
	}

	public long getSurchargepolicyId() {
		return this.surchargepolicyId;
	}

	public void setSurchargepolicyId(long surchargepolicyId) {
		this.surchargepolicyId = surchargepolicyId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getOrderLevel() {
		return this.orderLevel;
	}

	public void setOrderLevel(String orderLevel) {
		this.orderLevel = orderLevel;
	}

	public String getOrderLevelCode() {
		return this.orderLevelCode;
	}

	public void setOrderLevelCode(String orderLevelCode) {
		this.orderLevelCode = orderLevelCode;
	}

	public String getOrderStatus() {
		return this.orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRoa() {
		return this.roa;
	}

	public void setRoa(String roa) {
		this.roa = roa;
	}

	public List<Surchargepolicydetail> getSurchargepolicydetails() {
		return this.surchargepolicydetails;
	}

	public void setSurchargepolicydetails(List<Surchargepolicydetail> surchargepolicydetails) {
		this.surchargepolicydetails = surchargepolicydetails;
	}

	public Surchargepolicydetail addSurchargepolicydetail(Surchargepolicydetail surchargepolicydetail) {
		getSurchargepolicydetails().add(surchargepolicydetail);
		surchargepolicydetail.setSurchargepolicy(this);

		return surchargepolicydetail;
	}

	public Surchargepolicydetail removeSurchargepolicydetail(Surchargepolicydetail surchargepolicydetail) {
		getSurchargepolicydetails().remove(surchargepolicydetail);
		surchargepolicydetail.setSurchargepolicy(null);

		return surchargepolicydetail;
	}

}