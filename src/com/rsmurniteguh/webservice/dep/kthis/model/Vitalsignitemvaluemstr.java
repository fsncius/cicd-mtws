package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VITALSIGNITEMVALUEMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Vitalsignitemvaluemstr.findAll", query="SELECT v FROM Vitalsignitemvaluemstr v")
public class Vitalsignitemvaluemstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VITALSIGNITEMVALUEMSTR_ID")
	private long vitalsignitemvaluemstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="ITEM_VALUE")
	private String itemValue;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	//bi-directional many-to-one association to Vitalsignitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VITALSIGNITEMMSTR_ID")
	private Vitalsignitemmstr vitalsignitemmstr;

	public Vitalsignitemvaluemstr() {
	}

	public long getVitalsignitemvaluemstrId() {
		return this.vitalsignitemvaluemstrId;
	}

	public void setVitalsignitemvaluemstrId(long vitalsignitemvaluemstrId) {
		this.vitalsignitemvaluemstrId = vitalsignitemvaluemstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getItemValue() {
		return this.itemValue;
	}

	public void setItemValue(String itemValue) {
		this.itemValue = itemValue;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public Vitalsignitemmstr getVitalsignitemmstr() {
		return this.vitalsignitemmstr;
	}

	public void setVitalsignitemmstr(Vitalsignitemmstr vitalsignitemmstr) {
		this.vitalsignitemmstr = vitalsignitemmstr;
	}

}