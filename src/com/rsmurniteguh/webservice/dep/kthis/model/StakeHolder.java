package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity(name = "STAKEHOLDER")
public class StakeHolder implements Serializable {

    public StakeHolder() {}

    @Transient
    private Set<String> modifiedSet = new HashSet<String>();
    public Set<String> getModifiedSet() {
        return modifiedSet;
    }

    // FIELDS

    @Id
    @Column(name = "STAKEHOLDER_ID")
    private BigDecimal stakeHolderId;
    @Column(name = "STAKEHOLDER_TYPE")
    private String stakeHolderType;
    @Column(name = "ACCOUNT_STATUS")
    private String accountStatus;
    @Column(name = "DEPOSIT_BALANCE")
    private BigDecimal depositBalance;
    @Column(name = "DEPOSIT_AVAILABLE")
    private BigDecimal depositAvailable;
    @Column(name = "BILL_SIZE")
    private BigDecimal billSize;
    @Column(name = "ACCOUNT_BALANCE")
    private BigDecimal accountBalance;
    @Column(name = "CREDIT_RISK_MARKED_DATE")
    private Timestamp creditRiskMarkedDate;
    @Column(name = "CREDIT_RISK_PROMPT_IND")
    private String creditRiskPromptInd;
    @Column(name = "NOTE_INCLUSION_IND")
    private String noteInclusionInd;
    @Column(name = "CN_PERIOD_COVERAGE_IND")
    private String cnPeriodCoverageInd;
    @Column(name = "CREDIT_RISK_STATUS")
    private String creditRiskStatus;
    @Column(name = "CREDIT_TERM")
    private String creditTerm;
    @Column(name = "BILLING_MODE")
    private String billingMode;
    @Column(name = "BILLING_FREQUENCY")
    private String billingFrequency;
    @Column(name = "IP_FINAL_BILL_FORMAT")
    private String ipFinalBillFormat;
    @Column(name = "IP_REVISED_BILL_FORMAT")
    private String ipRevisedBillFormat;
    @Column(name = "OP_FINAL_BILL_FORMAT")
    private String opFinalBillFormat;
    @Column(name = "OP_REVISED_BILL_FORMAT")
    private String opRevisedBillFormat;
    @Column(name = "REMARKS")
    private String remarks;
    @Column(name = "ACCOUNT_POSTED_DATE")
    private Timestamp accountPostedDate;
    @Column(name = "CREDIT_RISK_REMARKS")
    private String creditRiskRemarks;
    @Column(name = "SHORT_CODE")
    private String shortCode;
    @Column(name = "PREV_UPDATED_BY")
    private BigDecimal prevUpdatedBy;
    @Column(name = "LAST_UPDATED_BY")
    private BigDecimal lastUpdatedBy;
    @Column(name = "PREV_UPDATED_DATETIME")
    private Timestamp prevUpdatedDateTime;
    @Column(name = "LAST_UPDATED_DATETIME")
    private Timestamp lastUpdatedDateTime;
    @Column(name = "OP_DEPOSIT_ACCOUNT_IND")
    private String opDepositAccountInd;
    @Column(name = "DEPOSIT_AMOUNT")
    private BigDecimal depositAmount;
    

    @Transient
    private boolean _stakeHolderIdFlag;
    @Transient
    private boolean _stakeHolderTypeFlag;
    @Transient
    private boolean _accountStatusFlag;
    @Transient
    private boolean _depositBalanceFlag;
    @Transient
    private boolean _depositAvailableFlag;
    @Transient
    private boolean _billSizeFlag;
    @Transient
    private boolean _accountBalanceFlag;
    @Transient
    private boolean _creditRiskMarkedDateFlag;
    @Transient
    private boolean _creditRiskPromptIndFlag;
    @Transient
    private boolean _noteInclusionIndFlag;
    @Transient
    private boolean _cnPeriodCoverageIndFlag;
    @Transient
    private boolean _creditRiskStatusFlag;
    @Transient
    private boolean _creditTermFlag;
    @Transient
    private boolean _billingModeFlag;
    @Transient
    private boolean _billingFrequencyFlag;
    @Transient
    private boolean _ipFinalBillFormatFlag;
    @Transient
    private boolean _ipRevisedBillFormatFlag;
    @Transient
    private boolean _opFinalBillFormatFlag;
    @Transient
    private boolean _opRevisedBillFormatFlag;
    @Transient
    private boolean _remarksFlag;
    @Transient
    private boolean _accountPostedDateFlag;
    @Transient
    private boolean _creditRiskRemarksFlag;
    @Transient
    private boolean _shortCodeFlag;
    @Transient
    private boolean _prevUpdatedByFlag;
    @Transient
    private boolean _lastUpdatedByFlag;
    @Transient
    private boolean _prevUpdatedDateTimeFlag;
    @Transient
    private boolean _lastUpdatedDateTimeFlag;
    @Transient
    private boolean _opDepositAccountIndFlag;
    @Transient
    private boolean _depositAmountFlag;

    // GETTERS

    public BigDecimal getStakeHolderId() {
        return stakeHolderId;
    }

    public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	public String getStakeHolderType() {
        return stakeHolderType;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public BigDecimal getDepositBalance() {
        return depositBalance;
    }

    public BigDecimal getDepositAvailable() {
        return depositAvailable;
    }

    public BigDecimal getBillSize() {
        return billSize;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public Timestamp getCreditRiskMarkedDate() {
        return creditRiskMarkedDate;
    }

    public String getCreditRiskPromptInd() {
        return creditRiskPromptInd;
    }

    public String getNoteInclusionInd() {
        return noteInclusionInd;
    }

    public String getCnPeriodCoverageInd() {
        return cnPeriodCoverageInd;
    }

    public String getCreditRiskStatus() {
        return creditRiskStatus;
    }

    public String getCreditTerm() {
        return creditTerm;
    }

    public String getBillingMode() {
        return billingMode;
    }

    public String getBillingFrequency() {
        return billingFrequency;
    }

    public String getIpFinalBillFormat() {
        return ipFinalBillFormat;
    }

    public String getIpRevisedBillFormat() {
        return ipRevisedBillFormat;
    }

    public String getOpFinalBillFormat() {
        return opFinalBillFormat;
    }

    public String getOpRevisedBillFormat() {
        return opRevisedBillFormat;
    }

    public String getRemarks() {
        return remarks;
    }

    public Timestamp getAccountPostedDate() {
        return accountPostedDate;
    }

    public String getCreditRiskRemarks() {
        return creditRiskRemarks;
    }

    public String getShortCode() {
        return shortCode;
    }

    public BigDecimal getPrevUpdatedBy() {
        return prevUpdatedBy;
    }

    public BigDecimal getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public Timestamp getPrevUpdatedDateTime() {
        return prevUpdatedDateTime;
    }

    public Timestamp getLastUpdatedDateTime() {
        return lastUpdatedDateTime;
    }

    public String getOpDepositAccountInd() {
        return opDepositAccountInd;
    }

    public boolean get_stakeHolderIdFlag() {
        return _stakeHolderIdFlag;
    }

    public boolean get_stakeHolderTypeFlag() {
        return _stakeHolderTypeFlag;
    }

    public boolean get_accountStatusFlag() {
        return _accountStatusFlag;
    }

    public boolean get_depositBalanceFlag() {
        return _depositBalanceFlag;
    }

    public boolean get_depositAvailableFlag() {
        return _depositAvailableFlag;
    }

    public boolean get_billSizeFlag() {
        return _billSizeFlag;
    }

    public boolean get_accountBalanceFlag() {
        return _accountBalanceFlag;
    }

    public boolean get_creditRiskMarkedDateFlag() {
        return _creditRiskMarkedDateFlag;
    }

    public boolean get_creditRiskPromptIndFlag() {
        return _creditRiskPromptIndFlag;
    }

    public boolean get_noteInclusionIndFlag() {
        return _noteInclusionIndFlag;
    }

    public boolean get_cnPeriodCoverageIndFlag() {
        return _cnPeriodCoverageIndFlag;
    }

    public boolean get_creditRiskStatusFlag() {
        return _creditRiskStatusFlag;
    }

    public boolean get_creditTermFlag() {
        return _creditTermFlag;
    }

    public boolean get_billingModeFlag() {
        return _billingModeFlag;
    }

    public boolean get_billingFrequencyFlag() {
        return _billingFrequencyFlag;
    }

    public boolean get_ipFinalBillFormatFlag() {
        return _ipFinalBillFormatFlag;
    }

    public boolean get_ipRevisedBillFormatFlag() {
        return _ipRevisedBillFormatFlag;
    }

    public boolean get_opFinalBillFormatFlag() {
        return _opFinalBillFormatFlag;
    }

    public boolean get_opRevisedBillFormatFlag() {
        return _opRevisedBillFormatFlag;
    }

    public boolean get_remarksFlag() {
        return _remarksFlag;
    }

    public boolean get_accountPostedDateFlag() {
        return _accountPostedDateFlag;
    }

    public boolean get_creditRiskRemarksFlag() {
        return _creditRiskRemarksFlag;
    }

    public boolean get_shortCodeFlag() {
        return _shortCodeFlag;
    }

    public boolean get_prevUpdatedByFlag() {
        return _prevUpdatedByFlag;
    }

    public boolean get_lastUpdatedByFlag() {
        return _lastUpdatedByFlag;
    }

    public boolean get_prevUpdatedDateTimeFlag() {
        return _prevUpdatedDateTimeFlag;
    }

    public boolean get_lastUpdatedDateTimeFlag() {
        return _lastUpdatedDateTimeFlag;
    }

    public boolean get_opDepositAccountIndFlag() {
        return _opDepositAccountIndFlag;
    }

    public boolean get_depositAmountFlag(){
    	return _depositAmountFlag;
    }
    
    // SETTERS

    public void setStakeHolderId(BigDecimal stakeHolderId) {
        this.stakeHolderId = stakeHolderId;
        this._stakeHolderIdFlag = true;
        this.modifiedSet.add("stakeHolderId");
    }
    
    public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
		this._depositAmountFlag = true;
		this.modifiedSet.add("depositAmount");
	}
    
    public void setStakeHolderType(String stakeHolderType) {
        this.stakeHolderType = stakeHolderType;
        this._stakeHolderTypeFlag = true;
        this.modifiedSet.add("stakeHolderType");
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
        this._accountStatusFlag = true;
        this.modifiedSet.add("accountStatus");
    }

    public void setDepositBalance(BigDecimal depositBalance) {
        this.depositBalance = depositBalance;
        this._depositBalanceFlag = true;
        this.modifiedSet.add("depositBalance");
    }

    public void setDepositAvailable(BigDecimal depositAvailable) {
        this.depositAvailable = depositAvailable;
        this._depositAvailableFlag = true;
        this.modifiedSet.add("depositAvailable");
    }

    public void setBillSize(BigDecimal billSize) {
        this.billSize = billSize;
        this._billSizeFlag = true;
        this.modifiedSet.add("billSize");
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
        this._accountBalanceFlag = true;
        this.modifiedSet.add("accountBalance");
    }

    public void setCreditRiskMarkedDate(Timestamp creditRiskMarkedDate) {
        this.creditRiskMarkedDate = creditRiskMarkedDate;
        this._creditRiskMarkedDateFlag = true;
        this.modifiedSet.add("creditRiskMarkedDate");
    }

    public void setCreditRiskPromptInd(String creditRiskPromptInd) {
        this.creditRiskPromptInd = creditRiskPromptInd;
        this._creditRiskPromptIndFlag = true;
        this.modifiedSet.add("creditRiskPromptInd");
    }

    public void setNoteInclusionInd(String noteInclusionInd) {
        this.noteInclusionInd = noteInclusionInd;
        this._noteInclusionIndFlag = true;
        this.modifiedSet.add("noteInclusionInd");
    }

    public void setCnPeriodCoverageInd(String cnPeriodCoverageInd) {
        this.cnPeriodCoverageInd = cnPeriodCoverageInd;
        this._cnPeriodCoverageIndFlag = true;
        this.modifiedSet.add("cnPeriodCoverageInd");
    }

    public void setCreditRiskStatus(String creditRiskStatus) {
        this.creditRiskStatus = creditRiskStatus;
        this._creditRiskStatusFlag = true;
        this.modifiedSet.add("creditRiskStatus");
    }

    public void setCreditTerm(String creditTerm) {
        this.creditTerm = creditTerm;
        this._creditTermFlag = true;
        this.modifiedSet.add("creditTerm");
    }

    public void setBillingMode(String billingMode) {
        this.billingMode = billingMode;
        this._billingModeFlag = true;
        this.modifiedSet.add("billingMode");
    }

    public void setBillingFrequency(String billingFrequency) {
        this.billingFrequency = billingFrequency;
        this._billingFrequencyFlag = true;
        this.modifiedSet.add("billingFrequency");
    }

    public void setIpFinalBillFormat(String ipFinalBillFormat) {
        this.ipFinalBillFormat = ipFinalBillFormat;
        this._ipFinalBillFormatFlag = true;
        this.modifiedSet.add("ipFinalBillFormat");
    }

    public void setIpRevisedBillFormat(String ipRevisedBillFormat) {
        this.ipRevisedBillFormat = ipRevisedBillFormat;
        this._ipRevisedBillFormatFlag = true;
        this.modifiedSet.add("ipRevisedBillFormat");
    }

    public void setOpFinalBillFormat(String opFinalBillFormat) {
        this.opFinalBillFormat = opFinalBillFormat;
        this._opFinalBillFormatFlag = true;
        this.modifiedSet.add("opFinalBillFormat");
    }

    public void setOpRevisedBillFormat(String opRevisedBillFormat) {
        this.opRevisedBillFormat = opRevisedBillFormat;
        this._opRevisedBillFormatFlag = true;
        this.modifiedSet.add("opRevisedBillFormat");
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
        this._remarksFlag = true;
        this.modifiedSet.add("remarks");
    }

    public void setAccountPostedDate(Timestamp accountPostedDate) {
        this.accountPostedDate = accountPostedDate;
        this._accountPostedDateFlag = true;
        this.modifiedSet.add("accountPostedDate");
    }

    public void setCreditRiskRemarks(String creditRiskRemarks) {
        this.creditRiskRemarks = creditRiskRemarks;
        this._creditRiskRemarksFlag = true;
        this.modifiedSet.add("creditRiskRemarks");
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
        this._shortCodeFlag = true;
        this.modifiedSet.add("shortCode");
    }

    public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
        this.prevUpdatedBy = prevUpdatedBy;
        this._prevUpdatedByFlag = true;
        this.modifiedSet.add("prevUpdatedBy");
    }

    public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
        this._lastUpdatedByFlag = true;
        this.modifiedSet.add("lastUpdatedBy");
    }

    public void setPrevUpdatedDateTime(Timestamp prevUpdatedDateTime) {
        this.prevUpdatedDateTime = prevUpdatedDateTime;
        this._prevUpdatedDateTimeFlag = true;
        this.modifiedSet.add("prevUpdatedDateTime");
    }

    public void setLastUpdatedDateTime(Timestamp lastUpdatedDateTime) {
        this.lastUpdatedDateTime = lastUpdatedDateTime;
        this._lastUpdatedDateTimeFlag = true;
        this.modifiedSet.add("lastUpdatedDateTime");
    }

    public void setOpDepositAccountInd(String opDepositAccountInd) {
        this.opDepositAccountInd = opDepositAccountInd;
        this._opDepositAccountIndFlag = true;
        this.modifiedSet.add("opDepositAccountInd");
    }

    // RESET

    public void resetModifiedFlag(boolean flag) {
        _stakeHolderIdFlag = flag;
        _stakeHolderTypeFlag = flag;
        _accountStatusFlag = flag;
        _depositBalanceFlag = flag;
        _depositAvailableFlag = flag;
        _billSizeFlag = flag;
        _accountBalanceFlag = flag;
        _creditRiskMarkedDateFlag = flag;
        _creditRiskPromptIndFlag = flag;
        _noteInclusionIndFlag = flag;
        _cnPeriodCoverageIndFlag = flag;
        _creditRiskStatusFlag = flag;
        _creditTermFlag = flag;
        _billingModeFlag = flag;
        _billingFrequencyFlag = flag;
        _ipFinalBillFormatFlag = flag;
        _ipRevisedBillFormatFlag = flag;
        _opFinalBillFormatFlag = flag;
        _opRevisedBillFormatFlag = flag;
        _remarksFlag = flag;
        _accountPostedDateFlag = flag;
        _creditRiskRemarksFlag = flag;
        _shortCodeFlag = flag;
        _prevUpdatedByFlag = flag;
        _lastUpdatedByFlag = flag;
        _prevUpdatedDateTimeFlag = flag;
        _lastUpdatedDateTimeFlag = flag;
        _opDepositAccountIndFlag = flag;
        _depositAmountFlag = flag;
        if (flag) {
            modifiedSet.add("stakeHolderId");
            modifiedSet.add("stakeHolderType");
            modifiedSet.add("accountStatus");
            modifiedSet.add("depositBalance");
            modifiedSet.add("depositAvailable");
            modifiedSet.add("billSize");
            modifiedSet.add("accountBalance");
            modifiedSet.add("creditRiskMarkedDate");
            modifiedSet.add("creditRiskPromptInd");
            modifiedSet.add("noteInclusionInd");
            modifiedSet.add("cnPeriodCoverageInd");
            modifiedSet.add("creditRiskStatus");
            modifiedSet.add("creditTerm");
            modifiedSet.add("billingMode");
            modifiedSet.add("billingFrequency");
            modifiedSet.add("ipFinalBillFormat");
            modifiedSet.add("ipRevisedBillFormat");
            modifiedSet.add("opFinalBillFormat");
            modifiedSet.add("opRevisedBillFormat");
            modifiedSet.add("remarks");
            modifiedSet.add("accountPostedDate");
            modifiedSet.add("creditRiskRemarks");
            modifiedSet.add("shortCode");
            modifiedSet.add("prevUpdatedBy");
            modifiedSet.add("lastUpdatedBy");
            modifiedSet.add("prevUpdatedDateTime");
            modifiedSet.add("lastUpdatedDateTime");
            modifiedSet.add("opDepositAccountInd");
            modifiedSet.add("depositAmount");
        } else {
            modifiedSet.clear();
        }
    }

    public void resetModifiedFlag() {
        resetModifiedFlag(false);
    }

    // TOSTRING

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        sb.append("StakeHolder: ");
        sb.append(stakeHolderId + ",");
        sb.append(stakeHolderType + ",");
        sb.append(accountStatus + ",");
        sb.append(depositBalance + ",");
        sb.append(depositAvailable + ",");
        sb.append(billSize + ",");
        sb.append(accountBalance + ",");
        sb.append(creditRiskMarkedDate + ",");
        sb.append(creditRiskPromptInd + ",");
        sb.append(noteInclusionInd + ",");
        sb.append(cnPeriodCoverageInd + ",");
        sb.append(creditRiskStatus + ",");
        sb.append(creditTerm + ",");
        sb.append(billingMode + ",");
        sb.append(billingFrequency + ",");
        sb.append(ipFinalBillFormat + ",");
        sb.append(ipRevisedBillFormat + ",");
        sb.append(opFinalBillFormat + ",");
        sb.append(opRevisedBillFormat + ",");
        sb.append(remarks + ",");
        sb.append(accountPostedDate + ",");
        sb.append(creditRiskRemarks + ",");
        sb.append(shortCode + ",");
        sb.append(prevUpdatedBy + ",");
        sb.append(lastUpdatedBy + ",");
        sb.append(prevUpdatedDateTime + ",");
        sb.append(lastUpdatedDateTime + ",");
        sb.append(opDepositAccountInd + ",");
        sb.append(depositAmount + ",");
        sb.append("]");
        return sb.toString();
    }

}