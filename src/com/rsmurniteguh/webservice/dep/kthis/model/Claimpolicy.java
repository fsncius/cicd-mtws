package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CLAIMPOLICY database table.
 * 
 */
@Entity
@NamedQuery(name="Claimpolicy.findAll", query="SELECT c FROM Claimpolicy c")
public class Claimpolicy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CLAIMPOLICY_ID")
	private long claimpolicyId;

	@Column(name="APPOINTED_HOSPITAL_IND")
	private String appointedHospitalInd;

	@Column(name="BRACKETCLAIM_ID")
	private BigDecimal bracketclaimId;

	@Column(name="CLAIMPOLICY_CODE")
	private String claimpolicyCode;

	@Column(name="CLAIMPOLICY_DESC")
	private String claimpolicyDesc;

	@Column(name="CLAIMPOLICY_DESC_LANG1")
	private String claimpolicyDescLang1;

	@Column(name="CLAIMPOLICY_DESC_LANG2")
	private String claimpolicyDescLang2;

	@Column(name="CLAIMPOLICY_DESC_LANG3")
	private String claimpolicyDescLang3;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATETIME")
	private Date effectiveDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATETIME")
	private Date expiryDatetime;

	@Column(name="FINANCIAL_CLASS")
	private String financialClass;

	@Column(name="HEALTHCARE_PLAN_IND")
	private String healthcarePlanInd;

	@Column(name="INDUSTRIAL_SICKNESS")
	private String industrialSickness;

	@Column(name="ITEMCLAIM_ID")
	private BigDecimal itemclaimId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MAJOR_DISEASE_IND")
	private String majorDiseaseInd;

	@Column(name="MAJOR_DISEASE_TYPE")
	private String majorDiseaseType;

	@Column(name="MIO_METHOD")
	private String mioMethod;

	@Column(name="MIO_PERSON_GROUP")
	private String mioPersonGroup;

	@Column(name="ONTHEJOB_RETIREMENT_IND")
	private String onthejobRetirementInd;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="RETIREMENT_STATUS")
	private String retirementStatus;

	//bi-directional many-to-one association to Patientdebtorplandetail
	@OneToMany(mappedBy="claimpolicy")
	private List<Patientdebtorplandetail> patientdebtorplandetails;

	public Claimpolicy() {
	}

	public long getClaimpolicyId() {
		return this.claimpolicyId;
	}

	public void setClaimpolicyId(long claimpolicyId) {
		this.claimpolicyId = claimpolicyId;
	}

	public String getAppointedHospitalInd() {
		return this.appointedHospitalInd;
	}

	public void setAppointedHospitalInd(String appointedHospitalInd) {
		this.appointedHospitalInd = appointedHospitalInd;
	}

	public BigDecimal getBracketclaimId() {
		return this.bracketclaimId;
	}

	public void setBracketclaimId(BigDecimal bracketclaimId) {
		this.bracketclaimId = bracketclaimId;
	}

	public String getClaimpolicyCode() {
		return this.claimpolicyCode;
	}

	public void setClaimpolicyCode(String claimpolicyCode) {
		this.claimpolicyCode = claimpolicyCode;
	}

	public String getClaimpolicyDesc() {
		return this.claimpolicyDesc;
	}

	public void setClaimpolicyDesc(String claimpolicyDesc) {
		this.claimpolicyDesc = claimpolicyDesc;
	}

	public String getClaimpolicyDescLang1() {
		return this.claimpolicyDescLang1;
	}

	public void setClaimpolicyDescLang1(String claimpolicyDescLang1) {
		this.claimpolicyDescLang1 = claimpolicyDescLang1;
	}

	public String getClaimpolicyDescLang2() {
		return this.claimpolicyDescLang2;
	}

	public void setClaimpolicyDescLang2(String claimpolicyDescLang2) {
		this.claimpolicyDescLang2 = claimpolicyDescLang2;
	}

	public String getClaimpolicyDescLang3() {
		return this.claimpolicyDescLang3;
	}

	public void setClaimpolicyDescLang3(String claimpolicyDescLang3) {
		this.claimpolicyDescLang3 = claimpolicyDescLang3;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffectiveDatetime() {
		return this.effectiveDatetime;
	}

	public void setEffectiveDatetime(Date effectiveDatetime) {
		this.effectiveDatetime = effectiveDatetime;
	}

	public Date getExpiryDatetime() {
		return this.expiryDatetime;
	}

	public void setExpiryDatetime(Date expiryDatetime) {
		this.expiryDatetime = expiryDatetime;
	}

	public String getFinancialClass() {
		return this.financialClass;
	}

	public void setFinancialClass(String financialClass) {
		this.financialClass = financialClass;
	}

	public String getHealthcarePlanInd() {
		return this.healthcarePlanInd;
	}

	public void setHealthcarePlanInd(String healthcarePlanInd) {
		this.healthcarePlanInd = healthcarePlanInd;
	}

	public String getIndustrialSickness() {
		return this.industrialSickness;
	}

	public void setIndustrialSickness(String industrialSickness) {
		this.industrialSickness = industrialSickness;
	}

	public BigDecimal getItemclaimId() {
		return this.itemclaimId;
	}

	public void setItemclaimId(BigDecimal itemclaimId) {
		this.itemclaimId = itemclaimId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMajorDiseaseInd() {
		return this.majorDiseaseInd;
	}

	public void setMajorDiseaseInd(String majorDiseaseInd) {
		this.majorDiseaseInd = majorDiseaseInd;
	}

	public String getMajorDiseaseType() {
		return this.majorDiseaseType;
	}

	public void setMajorDiseaseType(String majorDiseaseType) {
		this.majorDiseaseType = majorDiseaseType;
	}

	public String getMioMethod() {
		return this.mioMethod;
	}

	public void setMioMethod(String mioMethod) {
		this.mioMethod = mioMethod;
	}

	public String getMioPersonGroup() {
		return this.mioPersonGroup;
	}

	public void setMioPersonGroup(String mioPersonGroup) {
		this.mioPersonGroup = mioPersonGroup;
	}

	public String getOnthejobRetirementInd() {
		return this.onthejobRetirementInd;
	}

	public void setOnthejobRetirementInd(String onthejobRetirementInd) {
		this.onthejobRetirementInd = onthejobRetirementInd;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRetirementStatus() {
		return this.retirementStatus;
	}

	public void setRetirementStatus(String retirementStatus) {
		this.retirementStatus = retirementStatus;
	}

	public List<Patientdebtorplandetail> getPatientdebtorplandetails() {
		return this.patientdebtorplandetails;
	}

	public void setPatientdebtorplandetails(List<Patientdebtorplandetail> patientdebtorplandetails) {
		this.patientdebtorplandetails = patientdebtorplandetails;
	}

	public Patientdebtorplandetail addPatientdebtorplandetail(Patientdebtorplandetail patientdebtorplandetail) {
		getPatientdebtorplandetails().add(patientdebtorplandetail);
		patientdebtorplandetail.setClaimpolicy(this);

		return patientdebtorplandetail;
	}

	public Patientdebtorplandetail removePatientdebtorplandetail(Patientdebtorplandetail patientdebtorplandetail) {
		getPatientdebtorplandetails().remove(patientdebtorplandetail);
		patientdebtorplandetail.setClaimpolicy(null);

		return patientdebtorplandetail;
	}

}