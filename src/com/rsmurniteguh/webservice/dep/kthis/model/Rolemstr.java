package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the ROLEMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Rolemstr.findAll", query="SELECT r FROM Rolemstr r")
public class Rolemstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ROLEMSTR_ID")
	private long rolemstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="ROLE_CODE")
	private String roleCode;

	@Column(name="ROLE_DESC")
	private String roleDesc;

	@Column(name="ROLE_NAME")
	private String roleName;

	@Column(name="ROLE_NAME_LANG1")
	private String roleNameLang1;

	@Column(name="ROLE_NAME_LANG2")
	private String roleNameLang2;

	@Column(name="ROLE_NAME_LANG3")
	private String roleNameLang3;

	public Rolemstr() {
	}

	public long getRolemstrId() {
		return this.rolemstrId;
	}

	public void setRolemstrId(long rolemstrId) {
		this.rolemstrId = rolemstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRoleCode() {
		return this.roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleDesc() {
		return this.roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleNameLang1() {
		return this.roleNameLang1;
	}

	public void setRoleNameLang1(String roleNameLang1) {
		this.roleNameLang1 = roleNameLang1;
	}

	public String getRoleNameLang2() {
		return this.roleNameLang2;
	}

	public void setRoleNameLang2(String roleNameLang2) {
		this.roleNameLang2 = roleNameLang2;
	}

	public String getRoleNameLang3() {
		return this.roleNameLang3;
	}

	public void setRoleNameLang3(String roleNameLang3) {
		this.roleNameLang3 = roleNameLang3;
	}

}