package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DEPOSITTXNPOLICY database table.
 * 
 */
@Entity
@NamedQuery(name="Deposittxnpolicy.findAll", query="SELECT d FROM Deposittxnpolicy d")
public class Deposittxnpolicy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DEPOSITTXNPOLICY_ID")
	private long deposittxnpolicyId;

	@Column(name="AUTO_TXN_CODE")
	private String autoTxnCode;

	@Column(name="CANCEL_PAY_TXN_CODE")
	private String cancelPayTxnCode;

	@Column(name="CANCEL_REPORT_CODE")
	private String cancelReportCode;

	@Column(name="CANCEL_TXN_CODE")
	private String cancelTxnCode;

	@Column(name="COLLECT_REPORT_CODE")
	private String collectReportCode;

	@Column(name="COLLECT_TXN_CODE")
	private String collectTxnCode;

	@Column(name="COLLECTION_TYPE")
	private String collectionType;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="OP_DEPOSIT_IND")
	private String opDepositInd;

	@Column(name="PAY_TXN_CODE")
	private String payTxnCode;

	@Column(name="RECEIPT_CAT")
	private String receiptCat;

	@Column(name="REFUND_TXN_CODE")
	private String refundTxnCode;

	@Column(name="USE_TYPE")
	private String useType;

	public Deposittxnpolicy() {
	}

	public long getDeposittxnpolicyId() {
		return this.deposittxnpolicyId;
	}

	public void setDeposittxnpolicyId(long deposittxnpolicyId) {
		this.deposittxnpolicyId = deposittxnpolicyId;
	}

	public String getAutoTxnCode() {
		return this.autoTxnCode;
	}

	public void setAutoTxnCode(String autoTxnCode) {
		this.autoTxnCode = autoTxnCode;
	}

	public String getCancelPayTxnCode() {
		return this.cancelPayTxnCode;
	}

	public void setCancelPayTxnCode(String cancelPayTxnCode) {
		this.cancelPayTxnCode = cancelPayTxnCode;
	}

	public String getCancelReportCode() {
		return this.cancelReportCode;
	}

	public void setCancelReportCode(String cancelReportCode) {
		this.cancelReportCode = cancelReportCode;
	}

	public String getCancelTxnCode() {
		return this.cancelTxnCode;
	}

	public void setCancelTxnCode(String cancelTxnCode) {
		this.cancelTxnCode = cancelTxnCode;
	}

	public String getCollectReportCode() {
		return this.collectReportCode;
	}

	public void setCollectReportCode(String collectReportCode) {
		this.collectReportCode = collectReportCode;
	}

	public String getCollectTxnCode() {
		return this.collectTxnCode;
	}

	public void setCollectTxnCode(String collectTxnCode) {
		this.collectTxnCode = collectTxnCode;
	}

	public String getCollectionType() {
		return this.collectionType;
	}

	public void setCollectionType(String collectionType) {
		this.collectionType = collectionType;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getOpDepositInd() {
		return this.opDepositInd;
	}

	public void setOpDepositInd(String opDepositInd) {
		this.opDepositInd = opDepositInd;
	}

	public String getPayTxnCode() {
		return this.payTxnCode;
	}

	public void setPayTxnCode(String payTxnCode) {
		this.payTxnCode = payTxnCode;
	}

	public String getReceiptCat() {
		return this.receiptCat;
	}

	public void setReceiptCat(String receiptCat) {
		this.receiptCat = receiptCat;
	}

	public String getRefundTxnCode() {
		return this.refundTxnCode;
	}

	public void setRefundTxnCode(String refundTxnCode) {
		this.refundTxnCode = refundTxnCode;
	}

	public String getUseType() {
		return this.useType;
	}

	public void setUseType(String useType) {
		this.useType = useType;
	}

}