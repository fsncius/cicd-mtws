package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PATIENTTRACKINGINFOROLE database table.
 * 
 */
@Entity
@NamedQuery(name="Patienttrackinginforole.findAll", query="SELECT p FROM Patienttrackinginforole p")
public class Patienttrackinginforole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENTTRACKINGINFOROLE_ID")
	private long patienttrackinginforoleId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	//bi-directional many-to-one association to Patienttrackinginfo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTTRACKINGINFO_ID")
	private Patienttrackinginfo patienttrackinginfo;

	//bi-directional many-to-one association to Rolemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ROLEMSTR_ID")
	private Rolemstr rolemstr;

	public Patienttrackinginforole() {
	}

	public long getPatienttrackinginforoleId() {
		return this.patienttrackinginforoleId;
	}

	public void setPatienttrackinginforoleId(long patienttrackinginforoleId) {
		this.patienttrackinginforoleId = patienttrackinginforoleId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public Patienttrackinginfo getPatienttrackinginfo() {
		return this.patienttrackinginfo;
	}

	public void setPatienttrackinginfo(Patienttrackinginfo patienttrackinginfo) {
		this.patienttrackinginfo = patienttrackinginfo;
	}

	public Rolemstr getRolemstr() {
		return this.rolemstr;
	}

	public void setRolemstr(Rolemstr rolemstr) {
		this.rolemstr = rolemstr;
	}

}