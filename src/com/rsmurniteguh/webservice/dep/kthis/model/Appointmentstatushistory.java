package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the APPOINTMENTSTATUSHISTORY database table.
 * 
 */
@Entity
@NamedQuery(name="Appointmentstatushistory.findAll", query="SELECT a FROM Appointmentstatushistory a")
public class Appointmentstatushistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPOINTMENTSTATUSHISTORY_ID")
	private long appointmentstatushistoryId;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTER_DATETIME")
	private Date enterDatetime;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Column(name="NEW_STAUTS")
	private String newStauts;

	@Column(name="OLD_STATUS")
	private String oldStatus;

	private String remarks;

	//bi-directional many-to-one association to Appointment
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="APPOINTMENT_ID")
	private Appointment appointment;

	public Appointmentstatushistory() {
	}

	public long getAppointmentstatushistoryId() {
		return this.appointmentstatushistoryId;
	}

	public void setAppointmentstatushistoryId(long appointmentstatushistoryId) {
		this.appointmentstatushistoryId = appointmentstatushistoryId;
	}

	public Date getEnterDatetime() {
		return this.enterDatetime;
	}

	public void setEnterDatetime(Date enterDatetime) {
		this.enterDatetime = enterDatetime;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public String getNewStauts() {
		return this.newStauts;
	}

	public void setNewStauts(String newStauts) {
		this.newStauts = newStauts;
	}

	public String getOldStatus() {
		return this.oldStatus;
	}

	public void setOldStatus(String oldStatus) {
		this.oldStatus = oldStatus;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Appointment getAppointment() {
		return this.appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

}