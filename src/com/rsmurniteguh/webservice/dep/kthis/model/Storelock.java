package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the STORELOCK database table.
 * 
 */
@Entity
@NamedQuery(name="Storelock.findAll", query="SELECT s FROM Storelock s")
public class Storelock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOREMSTR_ID")
	private long storemstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="LOCK_TIMESTAMP")
	private Date lockTimestamp;

	public Storelock() {
	}

	public long getStoremstrId() {
		return this.storemstrId;
	}

	public void setStoremstrId(long storemstrId) {
		this.storemstrId = storemstrId;
	}

	public Date getLockTimestamp() {
		return this.lockTimestamp;
	}

	public void setLockTimestamp(Date lockTimestamp) {
		this.lockTimestamp = lockTimestamp;
	}

}