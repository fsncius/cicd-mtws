package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the LOCATIONMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Locationmstr.findAll", query="SELECT l FROM Locationmstr l")
public class Locationmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="LOCATIONMSTR_ID")
	private long locationmstrId;

	private String address;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="ENTITYMSTR_ID")
	private BigDecimal entitymstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="LOCATION_CODE")
	private String locationCode;

	@Column(name="LOCATION_DESC")
	private String locationDesc;

	@Column(name="LOCATION_NAME")
	private String locationName;

	@Column(name="LOCATION_NAME_LANG1")
	private String locationNameLang1;

	@Column(name="LOCATION_NAME_LANG2")
	private String locationNameLang2;

	@Column(name="LOCATION_NAME_LANG3")
	private String locationNameLang3;

	@Column(name="LOCATION_TYPE")
	private String locationType;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenuecentremstrId;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	@Column(name="SHORT_CODE")
	private String shortCode;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PARENT_LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to Locationmstr
	@OneToMany(mappedBy="locationmstr")
	private List<Locationmstr> locationmstrs;

	public Locationmstr() {
	}

	public long getLocationmstrId() {
		return this.locationmstrId;
	}

	public void setLocationmstrId(long locationmstrId) {
		this.locationmstrId = locationmstrId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getEntitymstrId() {
		return this.entitymstrId;
	}

	public void setEntitymstrId(BigDecimal entitymstrId) {
		this.entitymstrId = entitymstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getLocationDesc() {
		return this.locationDesc;
	}

	public void setLocationDesc(String locationDesc) {
		this.locationDesc = locationDesc;
	}

	public String getLocationName() {
		return this.locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getLocationNameLang1() {
		return this.locationNameLang1;
	}

	public void setLocationNameLang1(String locationNameLang1) {
		this.locationNameLang1 = locationNameLang1;
	}

	public String getLocationNameLang2() {
		return this.locationNameLang2;
	}

	public void setLocationNameLang2(String locationNameLang2) {
		this.locationNameLang2 = locationNameLang2;
	}

	public String getLocationNameLang3() {
		return this.locationNameLang3;
	}

	public void setLocationNameLang3(String locationNameLang3) {
		this.locationNameLang3 = locationNameLang3;
	}

	public String getLocationType() {
		return this.locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getRevenuecentremstrId() {
		return this.revenuecentremstrId;
	}

	public void setRevenuecentremstrId(BigDecimal revenuecentremstrId) {
		this.revenuecentremstrId = revenuecentremstrId;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public List<Locationmstr> getLocationmstrs() {
		return this.locationmstrs;
	}

	public void setLocationmstrs(List<Locationmstr> locationmstrs) {
		this.locationmstrs = locationmstrs;
	}

	public Locationmstr addLocationmstr(Locationmstr locationmstr) {
		getLocationmstrs().add(locationmstr);
		locationmstr.setLocationmstr(this);

		return locationmstr;
	}

	public Locationmstr removeLocationmstr(Locationmstr locationmstr) {
		getLocationmstrs().remove(locationmstr);
		locationmstr.setLocationmstr(null);

		return locationmstr;
	}

}