package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the STOREITEMBIN database table.
 * 
 */
@Entity
@NamedQuery(name="Storeitembin.findAll", query="SELECT s FROM Storeitembin s")
public class Storeitembin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOREITEMBIN_ID")
	private long storeitembinId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="STOREITEM_ID")
	private BigDecimal storeitemId;

	//bi-directional many-to-one association to Storebinmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREBINMSTR_ID")
	private Storebinmstr storebinmstr;

	public Storeitembin() {
	}

	public long getStoreitembinId() {
		return this.storeitembinId;
	}

	public void setStoreitembinId(long storeitembinId) {
		this.storeitembinId = storeitembinId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getStoreitemId() {
		return this.storeitemId;
	}

	public void setStoreitemId(BigDecimal storeitemId) {
		this.storeitemId = storeitemId;
	}

	public Storebinmstr getStorebinmstr() {
		return this.storebinmstr;
	}

	public void setStorebinmstr(Storebinmstr storebinmstr) {
		this.storebinmstr = storebinmstr;
	}

}