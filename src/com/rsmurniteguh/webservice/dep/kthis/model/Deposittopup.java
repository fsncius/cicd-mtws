package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DEPOSITTOPUP database table.
 * 
 */
@Entity
@NamedQuery(name="Deposittopup.findAll", query="SELECT d FROM Deposittopup d")
public class Deposittopup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DEPOSITTOPUP_ID")
	private long deposittopupId;

	@Column(name="ACCOUNT_BALANCE")
	private BigDecimal accountBalance;

	@Column(name="DEPOSIT_TOPUP")
	private BigDecimal depositTopup;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PRINTED_IND")
	private String printedInd;

	@Temporal(TemporalType.DATE)
	@Column(name="PROCESS_DATE")
	private Date processDate;

	@Column(name="TOTAL_DEPOSIT")
	private BigDecimal totalDeposit;

	//bi-directional many-to-one association to Patientaccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNT_ID")
	private Patientaccount patientaccount;

	public Deposittopup() {
	}

	public long getDeposittopupId() {
		return this.deposittopupId;
	}

	public void setDeposittopupId(long deposittopupId) {
		this.deposittopupId = deposittopupId;
	}

	public BigDecimal getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public BigDecimal getDepositTopup() {
		return this.depositTopup;
	}

	public void setDepositTopup(BigDecimal depositTopup) {
		this.depositTopup = depositTopup;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPrintedInd() {
		return this.printedInd;
	}

	public void setPrintedInd(String printedInd) {
		this.printedInd = printedInd;
	}

	public Date getProcessDate() {
		return this.processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public BigDecimal getTotalDeposit() {
		return this.totalDeposit;
	}

	public void setTotalDeposit(BigDecimal totalDeposit) {
		this.totalDeposit = totalDeposit;
	}

	public Patientaccount getPatientaccount() {
		return this.patientaccount;
	}

	public void setPatientaccount(Patientaccount patientaccount) {
		this.patientaccount = patientaccount;
	}

}