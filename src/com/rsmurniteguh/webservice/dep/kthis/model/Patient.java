package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PATIENT database table.
 * 
 */
@Entity
@NamedQuery(name="Patient.findAll", query="SELECT p FROM Patient p")
public class Patient implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PATIENT_ID")
	private BigDecimal patientId;
	
	private BigDecimal personId;

	@Column(name="ALT_PATIENT_ID")
	private BigDecimal altPatientId;

	@Temporal(TemporalType.DATE)
	@Column(name="ARCHIVAL_DATETIME")
	private Date archivalDatetime;

	private BigDecimal assistant;

	@Column(name="COST_TO_DATE")
	private BigDecimal costToDate;

	@Column(name="CREATED_ENTITYMSTR_ID")
	private BigDecimal createdEntitymstrId;

	@Temporal(TemporalType.DATE)
	@Column(name="CUSTOMER_EFFECTIVE_DATE")
	private Date customerEffectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="CUSTOMER_EXPIRY_DATE")
	private Date customerExpiryDate;

	@Column(name="CUSTOMERCLASSMSTR_ID")
	private BigDecimal customerclassmstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="HEAR_ABOUT_PATH")
	private String hearAboutPath;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_VISIT_DATE")
	private Date lastVisitDate;

	@Column(name="LAST_VISIT_NO")
	private String lastVisitNo;

	@Column(name="MC_TO_DATE")
	private BigDecimal mcToDate;

	@Column(name="MEDICAL_NO")
	private String medicalNo;

	@Column(name="MR_FILE_LOCATION")
	private String mrFileLocation;

	@Column(name="ORGANISATION_ID")
	private BigDecimal organisationId;

	@Column(name="PATIENT_CAT")
	private String patientCat;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_NO")
	private String patientNo;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="QUEUE_NO")
	private String queueNo;

	@Column(name="RECEIVEMESSAGE_IND")
	private String receivemessageInd;

	@Temporal(TemporalType.DATE)
	@Column(name="RECOMMEND_DATE")
	private Date recommendDate;

	private String remarks;

	@Column(name="SPECIAL_INSTRUCTIONS")
	private String specialInstructions;

	@Column(name="VISIT_TO_DATE")
	private BigDecimal visitToDate;

	//bi-directional many-to-one association to Materialconsumption
	@OneToMany(mappedBy="patient")
	private List<Materialconsumption> materialconsumptions;

	//bi-directional many-to-one association to Medicalrecordno
	@OneToMany(mappedBy="patient")
	private List<Medicalrecordno> medicalrecordnos;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FAMILYDOCTOR")
	private Careprovider careprovider1;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="HEALTHCAREDOCTOR")
	private Careprovider careprovider2;

	//bi-directional many-to-one association to Person
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="RECOMMEND_PERSON_ID")
	private Person person1;

	//bi-directional many-to-one association to Person
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PERSON_ID")
	private Person person2;

	//bi-directional many-to-one association to Patientdebtorplan
	@OneToMany(mappedBy="patient")
	private List<Patientdebtorplan> patientdebtorplans;

	//bi-directional many-to-one association to Patienttrackinginfo
	@OneToMany(mappedBy="patient")
	private List<Patienttrackinginfo> patienttrackinginfos;

	//bi-directional many-to-one association to Visit
	@OneToMany(mappedBy="patient")
	private List<Visit> visits;

	//bi-directional many-to-one association to Visitappointment
	@OneToMany(mappedBy="patient")
	private List<Visitappointment> visitappointments;
	
	private String bpjsNo;
	private String lastCondition;
	private String tingkatPendidikan;

	public Patient() {
	}

	public BigDecimal getPatientId() {
		return this.patientId;
	}

	public void setPatientId(BigDecimal patientId) {
		this.patientId = patientId;
	}

	public BigDecimal getAltPatientId() {
		return this.altPatientId;
	}

	public void setAltPatientId(BigDecimal altPatientId) {
		this.altPatientId = altPatientId;
	}

	public Date getArchivalDatetime() {
		return this.archivalDatetime;
	}

	public void setArchivalDatetime(Date archivalDatetime) {
		this.archivalDatetime = archivalDatetime;
	}

	public BigDecimal getAssistant() {
		return this.assistant;
	}

	public void setAssistant(BigDecimal assistant) {
		this.assistant = assistant;
	}

	public BigDecimal getCostToDate() {
		return this.costToDate;
	}

	public void setCostToDate(BigDecimal costToDate) {
		this.costToDate = costToDate;
	}

	public BigDecimal getCreatedEntitymstrId() {
		return this.createdEntitymstrId;
	}

	public void setCreatedEntitymstrId(BigDecimal createdEntitymstrId) {
		this.createdEntitymstrId = createdEntitymstrId;
	}

	public Date getCustomerEffectiveDate() {
		return this.customerEffectiveDate;
	}

	public void setCustomerEffectiveDate(Date customerEffectiveDate) {
		this.customerEffectiveDate = customerEffectiveDate;
	}

	public Date getCustomerExpiryDate() {
		return this.customerExpiryDate;
	}

	public void setCustomerExpiryDate(Date customerExpiryDate) {
		this.customerExpiryDate = customerExpiryDate;
	}

	public BigDecimal getCustomerclassmstrId() {
		return this.customerclassmstrId;
	}

	public void setCustomerclassmstrId(BigDecimal customerclassmstrId) {
		this.customerclassmstrId = customerclassmstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getHearAboutPath() {
		return this.hearAboutPath;
	}

	public void setHearAboutPath(String hearAboutPath) {
		this.hearAboutPath = hearAboutPath;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public Date getLastVisitDate() {
		return this.lastVisitDate;
	}

	public void setLastVisitDate(Date lastVisitDate) {
		this.lastVisitDate = lastVisitDate;
	}

	public String getLastVisitNo() {
		return this.lastVisitNo;
	}

	public void setLastVisitNo(String lastVisitNo) {
		this.lastVisitNo = lastVisitNo;
	}

	public BigDecimal getMcToDate() {
		return this.mcToDate;
	}

	public void setMcToDate(BigDecimal mcToDate) {
		this.mcToDate = mcToDate;
	}

	public String getMedicalNo() {
		return this.medicalNo;
	}

	public void setMedicalNo(String medicalNo) {
		this.medicalNo = medicalNo;
	}

	public String getMrFileLocation() {
		return this.mrFileLocation;
	}

	public void setMrFileLocation(String mrFileLocation) {
		this.mrFileLocation = mrFileLocation;
	}

	public BigDecimal getOrganisationId() {
		return this.organisationId;
	}

	public void setOrganisationId(BigDecimal organisationId) {
		this.organisationId = organisationId;
	}

	public String getPatientCat() {
		return this.patientCat;
	}

	public void setPatientCat(String patientCat) {
		this.patientCat = patientCat;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientNo() {
		return this.patientNo;
	}

	public void setPatientNo(String patientNo) {
		this.patientNo = patientNo;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getQueueNo() {
		return this.queueNo;
	}

	public void setQueueNo(String queueNo) {
		this.queueNo = queueNo;
	}

	public String getReceivemessageInd() {
		return this.receivemessageInd;
	}

	public void setReceivemessageInd(String receivemessageInd) {
		this.receivemessageInd = receivemessageInd;
	}

	public Date getRecommendDate() {
		return this.recommendDate;
	}

	public void setRecommendDate(Date recommendDate) {
		this.recommendDate = recommendDate;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSpecialInstructions() {
		return this.specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public BigDecimal getVisitToDate() {
		return this.visitToDate;
	}

	public void setVisitToDate(BigDecimal visitToDate) {
		this.visitToDate = visitToDate;
	}

	public List<Materialconsumption> getMaterialconsumptions() {
		return this.materialconsumptions;
	}

	public void setMaterialconsumptions(List<Materialconsumption> materialconsumptions) {
		this.materialconsumptions = materialconsumptions;
	}

	public Materialconsumption addMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().add(materialconsumption);
		materialconsumption.setPatient(this);

		return materialconsumption;
	}

	public Materialconsumption removeMaterialconsumption(Materialconsumption materialconsumption) {
		getMaterialconsumptions().remove(materialconsumption);
		materialconsumption.setPatient(null);

		return materialconsumption;
	}

	public List<Medicalrecordno> getMedicalrecordnos() {
		return this.medicalrecordnos;
	}

	public void setMedicalrecordnos(List<Medicalrecordno> medicalrecordnos) {
		this.medicalrecordnos = medicalrecordnos;
	}

	public Medicalrecordno addMedicalrecordno(Medicalrecordno medicalrecordno) {
		getMedicalrecordnos().add(medicalrecordno);
		medicalrecordno.setPatient(this);

		return medicalrecordno;
	}

	public Medicalrecordno removeMedicalrecordno(Medicalrecordno medicalrecordno) {
		getMedicalrecordnos().remove(medicalrecordno);
		medicalrecordno.setPatient(null);

		return medicalrecordno;
	}

	public Careprovider getCareprovider1() {
		return this.careprovider1;
	}

	public void setCareprovider1(Careprovider careprovider1) {
		this.careprovider1 = careprovider1;
	}

	public Careprovider getCareprovider2() {
		return this.careprovider2;
	}

	public void setCareprovider2(Careprovider careprovider2) {
		this.careprovider2 = careprovider2;
	}

	public Person getPerson1() {
		return this.person1;
	}

	public void setPerson1(Person person1) {
		this.person1 = person1;
	}

	public Person getPerson2() {
		return this.person2;
	}

	public void setPerson2(Person person2) {
		this.person2 = person2;
	}

	public List<Patientdebtorplan> getPatientdebtorplans() {
		return this.patientdebtorplans;
	}

	public void setPatientdebtorplans(List<Patientdebtorplan> patientdebtorplans) {
		this.patientdebtorplans = patientdebtorplans;
	}

	public Patientdebtorplan addPatientdebtorplan(Patientdebtorplan patientdebtorplan) {
		getPatientdebtorplans().add(patientdebtorplan);
		patientdebtorplan.setPatient(this);

		return patientdebtorplan;
	}

	public Patientdebtorplan removePatientdebtorplan(Patientdebtorplan patientdebtorplan) {
		getPatientdebtorplans().remove(patientdebtorplan);
		patientdebtorplan.setPatient(null);

		return patientdebtorplan;
	}

	public List<Patienttrackinginfo> getPatienttrackinginfos() {
		return this.patienttrackinginfos;
	}

	public void setPatienttrackinginfos(List<Patienttrackinginfo> patienttrackinginfos) {
		this.patienttrackinginfos = patienttrackinginfos;
	}

	public Patienttrackinginfo addPatienttrackinginfo(Patienttrackinginfo patienttrackinginfo) {
		getPatienttrackinginfos().add(patienttrackinginfo);
		patienttrackinginfo.setPatient(this);

		return patienttrackinginfo;
	}

	public Patienttrackinginfo removePatienttrackinginfo(Patienttrackinginfo patienttrackinginfo) {
		getPatienttrackinginfos().remove(patienttrackinginfo);
		patienttrackinginfo.setPatient(null);

		return patienttrackinginfo;
	}

	public List<Visit> getVisits() {
		return this.visits;
	}

	public void setVisits(List<Visit> visits) {
		this.visits = visits;
	}

	public Visit addVisit(Visit visit) {
		getVisits().add(visit);
		visit.setPatient(this);

		return visit;
	}

	public Visit removeVisit(Visit visit) {
		getVisits().remove(visit);
		visit.setPatient(null);

		return visit;
	}

	public List<Visitappointment> getVisitappointments() {
		return this.visitappointments;
	}

	public void setVisitappointments(List<Visitappointment> visitappointments) {
		this.visitappointments = visitappointments;
	}

	public Visitappointment addVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().add(visitappointment);
		visitappointment.setPatient(this);

		return visitappointment;
	}

	public Visitappointment removeVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().remove(visitappointment);
		visitappointment.setPatient(null);

		return visitappointment;
	}

	public BigDecimal getPersonId() {
		return personId;
	}

	public void setPersonId(BigDecimal personId) {
		this.personId = personId;
	}

	public String getLastCondition() {
		return lastCondition;
	}

	public void setLastCondition(String lastCondition) {
		this.lastCondition = lastCondition;
	}

	public String getTingkatPendidikan() {
		return tingkatPendidikan;
	}

	public void setTingkatPendidikan(String tingkatPendidikan) {
		this.tingkatPendidikan = tingkatPendidikan;
	}

	public String getBpjsNo() {
		return bpjsNo;
	}

	public void setBpjsNo(String bpjsNo) {
		this.bpjsNo = bpjsNo;
	}

}