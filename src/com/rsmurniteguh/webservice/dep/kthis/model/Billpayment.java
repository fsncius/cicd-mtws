package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the BILLPAYMENT database table.
 * 
 */
@Entity
@NamedQuery(name="Billpayment.findAll", query="SELECT b FROM Billpayment b")
public class Billpayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BILLPAYMENT_ID")
	private long billpaymentId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PAID_AMOUNT")
	private BigDecimal paidAmount;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	//bi-directional many-to-one association to Billaccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BILLACCOUNT_ID")
	private Billaccount billaccount;

	//bi-directional many-to-one association to Patientaccounttxn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNTTXN_ID")
	private Patientaccounttxn patientaccounttxn;

	public Billpayment() {
	}

	public long getBillpaymentId() {
		return this.billpaymentId;
	}

	public void setBillpaymentId(long billpaymentId) {
		this.billpaymentId = billpaymentId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Billaccount getBillaccount() {
		return this.billaccount;
	}

	public void setBillaccount(Billaccount billaccount) {
		this.billaccount = billaccount;
	}

	public Patientaccounttxn getPatientaccounttxn() {
		return this.patientaccounttxn;
	}

	public void setPatientaccounttxn(Patientaccounttxn patientaccounttxn) {
		this.patientaccounttxn = patientaccounttxn;
	}

}