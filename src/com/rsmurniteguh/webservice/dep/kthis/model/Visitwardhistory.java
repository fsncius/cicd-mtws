package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITWARDHISTORY database table.
 * 
 */
@Entity
@NamedQuery(name="Visitwardhistory.findAll", query="SELECT v FROM Visitwardhistory v")
public class Visitwardhistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITWARDHISTORY_ID")
	private long visitwardhistoryId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATETIME")
	private Date effectiveDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATETIME")
	private Date expiryDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="WARDMSTR_ID")
	private Wardmstr wardmstr;

	public Visitwardhistory() {
	}

	public long getVisitwardhistoryId() {
		return this.visitwardhistoryId;
	}

	public void setVisitwardhistoryId(long visitwardhistoryId) {
		this.visitwardhistoryId = visitwardhistoryId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEffectiveDatetime() {
		return this.effectiveDatetime;
	}

	public void setEffectiveDatetime(Date effectiveDatetime) {
		this.effectiveDatetime = effectiveDatetime;
	}

	public Date getExpiryDatetime() {
		return this.expiryDatetime;
	}

	public void setExpiryDatetime(Date expiryDatetime) {
		this.expiryDatetime = expiryDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

}