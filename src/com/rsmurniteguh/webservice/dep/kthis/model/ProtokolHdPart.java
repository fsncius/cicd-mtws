package com.rsmurniteguh.webservice.dep.kthis.model;
import java.math.BigDecimal;
import java.sql.Timestamp;


public final class ProtokolHdPart {
	public ProtokolHdPart() {}
	
	//private BigDecimal nokartu;
	private BigDecimal ProtokolHdPartId;
	private BigDecimal visitId;
	private Timestamp waktuPemeriksaan;
	private String qbPemeriksaan;
	private String qdPemeriksaan;
	private String ufrPemeriksaan;
	private String tdPemeriksaan;
	private String nadiPemeriksaan;
	private String nrsPemeriksaan;
	private String suhuPemeriksaan;
	private String rrPemeriksaan;
	private String vpPemeriksaan;
	private String apPemeriksaan;
	private String tmpPemeriksaan;
	private String keteranganPemeriksaan;
	private String parafPemeriksa;
	private BigDecimal createdBy;
	private Timestamp createdDatetime;
	private BigDecimal lastUpdatedBy;
	private Timestamp lastUpdatedDatetime;
	private String defunctInd;
	
	public BigDecimal getProtokolHdPartId() {
		return ProtokolHdPartId;
	}
	public void setProtokolHdPartId(BigDecimal protokolHdPartId) {
		ProtokolHdPartId = protokolHdPartId;
	}
	public BigDecimal getVisitId() {
		return visitId;
	}
	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}
	public Timestamp getWaktuPemeriksaan() {
		return waktuPemeriksaan;
	}
	public void setWaktuPemeriksaan(Timestamp waktuPemeriksaan) {
		this.waktuPemeriksaan = waktuPemeriksaan;
	}
	public String getQbPemeriksaan() {
		return qbPemeriksaan;
	}
	public void setQbPemeriksaan(String qbPemeriksaan) {
		this.qbPemeriksaan = qbPemeriksaan;
	}
	public String getQdPemeriksaan() {
		return qdPemeriksaan;
	}
	public void setQdPemeriksaan(String qdPemeriksaan) {
		this.qdPemeriksaan = qdPemeriksaan;
	}
	public String getUfrPemeriksaan() {
		return ufrPemeriksaan;
	}
	public void setUfrPemeriksaan(String ufrPemeriksaan) {
		this.ufrPemeriksaan = ufrPemeriksaan;
	}
	public String getTdPemeriksaan() {
		return tdPemeriksaan;
	}
	public void setTdPemeriksaan(String tdPemeriksaan) {
		this.tdPemeriksaan = tdPemeriksaan;
	}
	public String getNadiPemeriksaan() {
		return nadiPemeriksaan;
	}
	public void setNadiPemeriksaan(String nadiPemeriksaan) {
		this.nadiPemeriksaan = nadiPemeriksaan;
	}
	public String getNrsPemeriksaan() {
		return nrsPemeriksaan;
	}
	public void setNrsPemeriksaan(String nrsPemeriksaan) {
		this.nrsPemeriksaan = nrsPemeriksaan;
	}
	public String getSuhuPemeriksaan() {
		return suhuPemeriksaan;
	}
	public void setSuhuPemeriksaan(String suhuPemeriksaan) {
		this.suhuPemeriksaan = suhuPemeriksaan;
	}
	public String getRrPemeriksaan() {
		return rrPemeriksaan;
	}
	public void setRrPemeriksaan(String rrPemeriksaan) {
		this.rrPemeriksaan = rrPemeriksaan;
	}
	public String getVpPemeriksaan() {
		return vpPemeriksaan;
	}
	public void setVpPemeriksaan(String vpPemeriksaan) {
		this.vpPemeriksaan = vpPemeriksaan;
	}
	public String getApPemeriksaan() {
		return apPemeriksaan;
	}
	public void setApPemeriksaan(String apPemeriksaan) {
		this.apPemeriksaan = apPemeriksaan;
	}
	public String getTmpPemeriksaan() {
		return tmpPemeriksaan;
	}
	public void setTmpPemeriksaan(String tmpPemeriksaan) {
		this.tmpPemeriksaan = tmpPemeriksaan;
	}
	public String getKeteranganPemeriksaan() {
		return keteranganPemeriksaan;
	}
	public void setKeteranganPemeriksaan(String keteranganPemeriksaan) {
		this.keteranganPemeriksaan = keteranganPemeriksaan;
	}
	public String getParafPemeriksa() {
		return parafPemeriksa;
	}
	public void setParafPemeriksa(String parafPemeriksa) {
		this.parafPemeriksa = parafPemeriksa;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public BigDecimal getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Timestamp getLastUpdatedDatetime() {
		return lastUpdatedDatetime;
	}
	public void setLastUpdatedDatetime(Timestamp lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}
	public String getDefunctInd() {
		return defunctInd;
	}
	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}
	/*public BigDecimal getNokartu() {
		return nokartu;
	}
	public void setNokartu(BigDecimal nokartu) {
		this.nokartu = nokartu;
	}*/

	
}
