package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the APPOINTMENTRESOURCEMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Appointmentresourcemstr.findAll", query="SELECT a FROM Appointmentresourcemstr a")
public class Appointmentresourcemstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPOINTMENTRESOURCEMSTR_ID")
	private long appointmentresourcemstrId;

	@Column(name="APPOINTMENTRESOURCE_DESC")
	private String appointmentresourceDesc;

	@Column(name="APPOINTMENTRESOURCE_DESC_LANG1")
	private String appointmentresourceDescLang1;

	@Column(name="APPOINTMENTRESOURCE_TYPE")
	private String appointmentresourceType;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="OBJECT_ID")
	private BigDecimal objectId;

	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="appointmentresourcemstr")
	private List<Appointment> appointments;

	public Appointmentresourcemstr() {
	}

	public long getAppointmentresourcemstrId() {
		return this.appointmentresourcemstrId;
	}

	public void setAppointmentresourcemstrId(long appointmentresourcemstrId) {
		this.appointmentresourcemstrId = appointmentresourcemstrId;
	}

	public String getAppointmentresourceDesc() {
		return this.appointmentresourceDesc;
	}

	public void setAppointmentresourceDesc(String appointmentresourceDesc) {
		this.appointmentresourceDesc = appointmentresourceDesc;
	}

	public String getAppointmentresourceDescLang1() {
		return this.appointmentresourceDescLang1;
	}

	public void setAppointmentresourceDescLang1(String appointmentresourceDescLang1) {
		this.appointmentresourceDescLang1 = appointmentresourceDescLang1;
	}

	public String getAppointmentresourceType() {
		return this.appointmentresourceType;
	}

	public void setAppointmentresourceType(String appointmentresourceType) {
		this.appointmentresourceType = appointmentresourceType;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getObjectId() {
		return this.objectId;
	}

	public void setObjectId(BigDecimal objectId) {
		this.objectId = objectId;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setAppointmentresourcemstr(this);

		return appointment;
	}

	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setAppointmentresourcemstr(null);

		return appointment;
	}

}