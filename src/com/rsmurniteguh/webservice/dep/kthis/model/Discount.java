package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the DISCOUNT database table.
 * 
 */
@Entity
@NamedQuery(name="Discount.findAll", query="SELECT d FROM Discount d")
public class Discount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DISCOUNT_ID")
	private long discountId;

	@Column(name="AUDITED_BY")
	private BigDecimal auditedBy;

	@Column(name="BILLING_TYPE")
	private String billingType;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DISCOUNT_LOCATIONMSTR_ID")
	private BigDecimal discountLocationmstrId;

	@Column(name="DISCOUNT_REASON")
	private String discountReason;

	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATETIME")
	private Date enteredDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	private String remarks;

	@Column(name="STAKEHOLDER_ID")
	private BigDecimal stakeholderId;

	//bi-directional many-to-one association to Billdiscount
	@OneToMany(mappedBy="discount")
	private List<Billdiscount> billdiscounts;

	public Discount() {
	}

	public long getDiscountId() {
		return this.discountId;
	}

	public void setDiscountId(long discountId) {
		this.discountId = discountId;
	}

	public BigDecimal getAuditedBy() {
		return this.auditedBy;
	}

	public void setAuditedBy(BigDecimal auditedBy) {
		this.auditedBy = auditedBy;
	}

	public String getBillingType() {
		return this.billingType;
	}

	public void setBillingType(String billingType) {
		this.billingType = billingType;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getDiscountLocationmstrId() {
		return this.discountLocationmstrId;
	}

	public void setDiscountLocationmstrId(BigDecimal discountLocationmstrId) {
		this.discountLocationmstrId = discountLocationmstrId;
	}

	public String getDiscountReason() {
		return this.discountReason;
	}

	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}

	public BigDecimal getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredDatetime() {
		return this.enteredDatetime;
	}

	public void setEnteredDatetime(Date enteredDatetime) {
		this.enteredDatetime = enteredDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStakeholderId() {
		return this.stakeholderId;
	}

	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
	}

	public List<Billdiscount> getBilldiscounts() {
		return this.billdiscounts;
	}

	public void setBilldiscounts(List<Billdiscount> billdiscounts) {
		this.billdiscounts = billdiscounts;
	}

	public Billdiscount addBilldiscount(Billdiscount billdiscount) {
		getBilldiscounts().add(billdiscount);
		billdiscount.setDiscount(this);

		return billdiscount;
	}

	public Billdiscount removeBilldiscount(Billdiscount billdiscount) {
		getBilldiscounts().remove(billdiscount);
		billdiscount.setDiscount(null);

		return billdiscount;
	}

}