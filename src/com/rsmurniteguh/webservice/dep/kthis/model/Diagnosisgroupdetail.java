package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DIAGNOSISGROUPDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Diagnosisgroupdetail.findAll", query="SELECT d FROM Diagnosisgroupdetail d")
public class Diagnosisgroupdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DIAGNOSISGROUPDETAIL_ID")
	private long diagnosisgroupdetailId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	//bi-directional many-to-one association to Diagnosisgroupmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DIAGNOSISGROUPMSTR_ID")
	private Diagnosisgroupmstr diagnosisgroupmstr;

	//bi-directional many-to-one association to Diagnosismstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DIAGNOSISMSTR_ID")
	private Diagnosismstr diagnosismstr;

	public Diagnosisgroupdetail() {
	}

	public long getDiagnosisgroupdetailId() {
		return this.diagnosisgroupdetailId;
	}

	public void setDiagnosisgroupdetailId(long diagnosisgroupdetailId) {
		this.diagnosisgroupdetailId = diagnosisgroupdetailId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Diagnosisgroupmstr getDiagnosisgroupmstr() {
		return this.diagnosisgroupmstr;
	}

	public void setDiagnosisgroupmstr(Diagnosisgroupmstr diagnosisgroupmstr) {
		this.diagnosisgroupmstr = diagnosisgroupmstr;
	}

	public Diagnosismstr getDiagnosismstr() {
		return this.diagnosismstr;
	}

	public void setDiagnosismstr(Diagnosismstr diagnosismstr) {
		this.diagnosismstr = diagnosismstr;
	}

}