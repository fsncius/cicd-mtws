package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CHARGEROLLINGPLAN database table.
 * 
 */
@Entity
@NamedQuery(name="Chargerollingplan.findAll", query="SELECT c FROM Chargerollingplan c")
public class Chargerollingplan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGEROLLINGPLAN_ID")
	private long chargerollingplanId;

	@Column(name="ACTUAL_UNIT_PRICE")
	private BigDecimal actualUnitPrice;

	@Column(name="BASE_UNIT_PRICE")
	private BigDecimal baseUnitPrice;

	@Temporal(TemporalType.DATE)
	@Column(name="BEGIN_DATETIME")
	private Date beginDatetime;

	@Column(name="CHARGE_REVENUECENTREMSTR_ID")
	private BigDecimal chargeRevenuecentremstrId;

	@Column(name="CLAIM_AMOUNT")
	private BigDecimal claimAmount;

	@Column(name="CLAIM_PERCENT")
	private BigDecimal claimPercent;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DATETIME")
	private Date endDatetime;

	@Column(name="FREQUENCYMSTR_ID")
	private BigDecimal frequencymstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MIO_INDICATION")
	private String mioIndication;

	@Column(name="PLAN_STATUS")
	private String planStatus;

	@Column(name="PLAN_TYPE")
	private String planType;

	private BigDecimal qty;

	@Column(name="QTY_UOM")
	private String qtyUom;

	private String remarks;

	@Column(name="STOPPED_BY")
	private BigDecimal stoppedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="STOPPED_DATETIME")
	private Date stoppedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="TXN_DATETIME")
	private Date txnDatetime;

	@Column(name="TXN_DESC")
	private String txnDesc;

	//bi-directional many-to-one association to Chargeplanspecialtime
	@OneToMany(mappedBy="chargerollingplan")
	private List<Chargeplanspecialtime> chargeplanspecialtimes;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ORDERED_CAREPROVIDER_ID")
	private Careprovider careprovider1;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PERFORMED_CAREPROVIDER_ID")
	private Careprovider careprovider2;

	//bi-directional many-to-one association to Chargeitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHARGEITEMMSTR_ID")
	private Chargeitemmstr chargeitemmstr;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ENTERED_LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to Patientaccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PATIENTACCOUNT_ID")
	private Patientaccount patientaccount;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr1;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr2;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_WARDMSTR_ID")
	private Wardmstr wardmstr;

	public Chargerollingplan() {
	}

	public long getChargerollingplanId() {
		return this.chargerollingplanId;
	}

	public void setChargerollingplanId(long chargerollingplanId) {
		this.chargerollingplanId = chargerollingplanId;
	}

	public BigDecimal getActualUnitPrice() {
		return this.actualUnitPrice;
	}

	public void setActualUnitPrice(BigDecimal actualUnitPrice) {
		this.actualUnitPrice = actualUnitPrice;
	}

	public BigDecimal getBaseUnitPrice() {
		return this.baseUnitPrice;
	}

	public void setBaseUnitPrice(BigDecimal baseUnitPrice) {
		this.baseUnitPrice = baseUnitPrice;
	}

	public Date getBeginDatetime() {
		return this.beginDatetime;
	}

	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}

	public BigDecimal getChargeRevenuecentremstrId() {
		return this.chargeRevenuecentremstrId;
	}

	public void setChargeRevenuecentremstrId(BigDecimal chargeRevenuecentremstrId) {
		this.chargeRevenuecentremstrId = chargeRevenuecentremstrId;
	}

	public BigDecimal getClaimAmount() {
		return this.claimAmount;
	}

	public void setClaimAmount(BigDecimal claimAmount) {
		this.claimAmount = claimAmount;
	}

	public BigDecimal getClaimPercent() {
		return this.claimPercent;
	}

	public void setClaimPercent(BigDecimal claimPercent) {
		this.claimPercent = claimPercent;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public Date getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public BigDecimal getFrequencymstrId() {
		return this.frequencymstrId;
	}

	public void setFrequencymstrId(BigDecimal frequencymstrId) {
		this.frequencymstrId = frequencymstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getMioIndication() {
		return this.mioIndication;
	}

	public void setMioIndication(String mioIndication) {
		this.mioIndication = mioIndication;
	}

	public String getPlanStatus() {
		return this.planStatus;
	}

	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}

	public String getPlanType() {
		return this.planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getQtyUom() {
		return this.qtyUom;
	}

	public void setQtyUom(String qtyUom) {
		this.qtyUom = qtyUom;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getStoppedBy() {
		return this.stoppedBy;
	}

	public void setStoppedBy(BigDecimal stoppedBy) {
		this.stoppedBy = stoppedBy;
	}

	public Date getStoppedDatetime() {
		return this.stoppedDatetime;
	}

	public void setStoppedDatetime(Date stoppedDatetime) {
		this.stoppedDatetime = stoppedDatetime;
	}

	public Date getTxnDatetime() {
		return this.txnDatetime;
	}

	public void setTxnDatetime(Date txnDatetime) {
		this.txnDatetime = txnDatetime;
	}

	public String getTxnDesc() {
		return this.txnDesc;
	}

	public void setTxnDesc(String txnDesc) {
		this.txnDesc = txnDesc;
	}

	public List<Chargeplanspecialtime> getChargeplanspecialtimes() {
		return this.chargeplanspecialtimes;
	}

	public void setChargeplanspecialtimes(List<Chargeplanspecialtime> chargeplanspecialtimes) {
		this.chargeplanspecialtimes = chargeplanspecialtimes;
	}

	public Chargeplanspecialtime addChargeplanspecialtime(Chargeplanspecialtime chargeplanspecialtime) {
		getChargeplanspecialtimes().add(chargeplanspecialtime);
		chargeplanspecialtime.setChargerollingplan(this);

		return chargeplanspecialtime;
	}

	public Chargeplanspecialtime removeChargeplanspecialtime(Chargeplanspecialtime chargeplanspecialtime) {
		getChargeplanspecialtimes().remove(chargeplanspecialtime);
		chargeplanspecialtime.setChargerollingplan(null);

		return chargeplanspecialtime;
	}

	public Careprovider getCareprovider1() {
		return this.careprovider1;
	}

	public void setCareprovider1(Careprovider careprovider1) {
		this.careprovider1 = careprovider1;
	}

	public Careprovider getCareprovider2() {
		return this.careprovider2;
	}

	public void setCareprovider2(Careprovider careprovider2) {
		this.careprovider2 = careprovider2;
	}

	public Chargeitemmstr getChargeitemmstr() {
		return this.chargeitemmstr;
	}

	public void setChargeitemmstr(Chargeitemmstr chargeitemmstr) {
		this.chargeitemmstr = chargeitemmstr;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public Patientaccount getPatientaccount() {
		return this.patientaccount;
	}

	public void setPatientaccount(Patientaccount patientaccount) {
		this.patientaccount = patientaccount;
	}

	public Subspecialtymstr getSubspecialtymstr1() {
		return this.subspecialtymstr1;
	}

	public void setSubspecialtymstr1(Subspecialtymstr subspecialtymstr1) {
		this.subspecialtymstr1 = subspecialtymstr1;
	}

	public Subspecialtymstr getSubspecialtymstr2() {
		return this.subspecialtymstr2;
	}

	public void setSubspecialtymstr2(Subspecialtymstr subspecialtymstr2) {
		this.subspecialtymstr2 = subspecialtymstr2;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

}