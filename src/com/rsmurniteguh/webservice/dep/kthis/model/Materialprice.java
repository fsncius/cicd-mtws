package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the MATERIALPRICE database table.
 * 
 */
@Entity
@NamedQuery(name="Materialprice.findAll", query="SELECT m FROM Materialprice m")
public class Materialprice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MATERIALPRICE_ID")
	private long materialpriceId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PRICE_UOM")
	private String priceUom;

	private String remarks;

	@Column(name="SALES_PRICE")
	private BigDecimal salesPrice;

	@Column(name="WHOLESALE_PRICE")
	private BigDecimal wholesalePrice;

	//bi-directional many-to-one association to Materialitemmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MATERIALITEMMSTR_ID")
	private Materialitemmstr materialitemmstr;

	//bi-directional many-to-one association to Materialpricehistory
	@OneToMany(mappedBy="materialprice")
	private List<Materialpricehistory> materialpricehistories;

	public Materialprice() {
	}

	public long getMaterialpriceId() {
		return this.materialpriceId;
	}

	public void setMaterialpriceId(long materialpriceId) {
		this.materialpriceId = materialpriceId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPriceUom() {
		return this.priceUom;
	}

	public void setPriceUom(String priceUom) {
		this.priceUom = priceUom;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getSalesPrice() {
		return this.salesPrice;
	}

	public void setSalesPrice(BigDecimal salesPrice) {
		this.salesPrice = salesPrice;
	}

	public BigDecimal getWholesalePrice() {
		return this.wholesalePrice;
	}

	public void setWholesalePrice(BigDecimal wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}

	public Materialitemmstr getMaterialitemmstr() {
		return this.materialitemmstr;
	}

	public void setMaterialitemmstr(Materialitemmstr materialitemmstr) {
		this.materialitemmstr = materialitemmstr;
	}

	public List<Materialpricehistory> getMaterialpricehistories() {
		return this.materialpricehistories;
	}

	public void setMaterialpricehistories(List<Materialpricehistory> materialpricehistories) {
		this.materialpricehistories = materialpricehistories;
	}

	public Materialpricehistory addMaterialpricehistory(Materialpricehistory materialpricehistory) {
		getMaterialpricehistories().add(materialpricehistory);
		materialpricehistory.setMaterialprice(this);

		return materialpricehistory;
	}

	public Materialpricehistory removeMaterialpricehistory(Materialpricehistory materialpricehistory) {
		getMaterialpricehistories().remove(materialpricehistory);
		materialpricehistory.setMaterialprice(null);

		return materialpricehistory;
	}

}