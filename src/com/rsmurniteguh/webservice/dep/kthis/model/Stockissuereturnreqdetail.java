package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the STOCKISSUERETURNREQDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Stockissuereturnreqdetail.findAll", query="SELECT s FROM Stockissuereturnreqdetail s")
public class Stockissuereturnreqdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKISSUERETURNREQDETAIL_ID")
	private long stockissuereturnreqdetailId;

	@Column(name="BATCH_NO")
	private String batchNo;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MANUFACTURER_VENDORMSTR_ID")
	private BigDecimal manufacturerVendormstrId;

	@Column(name="MATERIALITEMMSTR_ID")
	private BigDecimal materialitemmstrId;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="REQUISITION_DETAIL_STATUS")
	private String requisitionDetailStatus;

	@Column(name="REQUISITION_QTY")
	private BigDecimal requisitionQty;

	@Column(name="REQUISITION_QTY_OF_BASE_UOM")
	private BigDecimal requisitionQtyOfBaseUom;

	@Column(name="REQUISITION_UOM")
	private String requisitionUom;

	@Column(name="STOREBINMSTR_ID")
	private BigDecimal storebinmstrId;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Stockissuereturnreq
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKISSUERETURNREQ_ID")
	private Stockissuereturnreq stockissuereturnreq;

	//bi-directional many-to-one association to Stockreceiptdetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKSET_ID")
	private Stockreceiptdetail stockreceiptdetail;

	public Stockissuereturnreqdetail() {
	}

	public long getStockissuereturnreqdetailId() {
		return this.stockissuereturnreqdetailId;
	}

	public void setStockissuereturnreqdetailId(long stockissuereturnreqdetailId) {
		this.stockissuereturnreqdetailId = stockissuereturnreqdetailId;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getManufacturerVendormstrId() {
		return this.manufacturerVendormstrId;
	}

	public void setManufacturerVendormstrId(BigDecimal manufacturerVendormstrId) {
		this.manufacturerVendormstrId = manufacturerVendormstrId;
	}

	public BigDecimal getMaterialitemmstrId() {
		return this.materialitemmstrId;
	}

	public void setMaterialitemmstrId(BigDecimal materialitemmstrId) {
		this.materialitemmstrId = materialitemmstrId;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRequisitionDetailStatus() {
		return this.requisitionDetailStatus;
	}

	public void setRequisitionDetailStatus(String requisitionDetailStatus) {
		this.requisitionDetailStatus = requisitionDetailStatus;
	}

	public BigDecimal getRequisitionQty() {
		return this.requisitionQty;
	}

	public void setRequisitionQty(BigDecimal requisitionQty) {
		this.requisitionQty = requisitionQty;
	}

	public BigDecimal getRequisitionQtyOfBaseUom() {
		return this.requisitionQtyOfBaseUom;
	}

	public void setRequisitionQtyOfBaseUom(BigDecimal requisitionQtyOfBaseUom) {
		this.requisitionQtyOfBaseUom = requisitionQtyOfBaseUom;
	}

	public String getRequisitionUom() {
		return this.requisitionUom;
	}

	public void setRequisitionUom(String requisitionUom) {
		this.requisitionUom = requisitionUom;
	}

	public BigDecimal getStorebinmstrId() {
		return this.storebinmstrId;
	}

	public void setStorebinmstrId(BigDecimal storebinmstrId) {
		this.storebinmstrId = storebinmstrId;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Stockissuereturnreq getStockissuereturnreq() {
		return this.stockissuereturnreq;
	}

	public void setStockissuereturnreq(Stockissuereturnreq stockissuereturnreq) {
		this.stockissuereturnreq = stockissuereturnreq;
	}

	public Stockreceiptdetail getStockreceiptdetail() {
		return this.stockreceiptdetail;
	}

	public void setStockreceiptdetail(Stockreceiptdetail stockreceiptdetail) {
		this.stockreceiptdetail = stockreceiptdetail;
	}

}