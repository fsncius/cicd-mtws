package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the ADTDAILYSUMMARY database table.
 * 
 */
@Entity
@NamedQuery(name="Adtdailysummary.findAll", query="SELECT a FROM Adtdailysummary a")
public class Adtdailysummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ADTDAILYSUMMARY_ID")
	private long adtdailysummaryId;

	@Column(name="ADDITIONAL_BED_CNT")
	private BigDecimal additionalBedCnt;

	@Column(name="ADMISSION_CNT")
	private BigDecimal admissionCnt;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DAY_ID")
	private String dayId;

	@Column(name="DISCHARGE_CNT")
	private BigDecimal dischargeCnt;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENT_CNT")
	private BigDecimal patientCnt;

	@Column(name="RATED_BED_CNT")
	private BigDecimal ratedBedCnt;

	@Column(name="SSPM_ACCOMPANY_CNT")
	private BigDecimal sspmAccompanyCnt;

	@Column(name="SUBSPECIALTY_IN")
	private BigDecimal subspecialtyIn;

	@Column(name="SUBSPECIALTY_OUT")
	private BigDecimal subspecialtyOut;

	@Temporal(TemporalType.DATE)
	@Column(name="TXN_DATE")
	private Date txnDate;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Wardmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="WARDMSTR_ID")
	private Wardmstr wardmstr;

	public Adtdailysummary() {
	}

	public long getAdtdailysummaryId() {
		return this.adtdailysummaryId;
	}

	public void setAdtdailysummaryId(long adtdailysummaryId) {
		this.adtdailysummaryId = adtdailysummaryId;
	}

	public BigDecimal getAdditionalBedCnt() {
		return this.additionalBedCnt;
	}

	public void setAdditionalBedCnt(BigDecimal additionalBedCnt) {
		this.additionalBedCnt = additionalBedCnt;
	}

	public BigDecimal getAdmissionCnt() {
		return this.admissionCnt;
	}

	public void setAdmissionCnt(BigDecimal admissionCnt) {
		this.admissionCnt = admissionCnt;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDayId() {
		return this.dayId;
	}

	public void setDayId(String dayId) {
		this.dayId = dayId;
	}

	public BigDecimal getDischargeCnt() {
		return this.dischargeCnt;
	}

	public void setDischargeCnt(BigDecimal dischargeCnt) {
		this.dischargeCnt = dischargeCnt;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPatientCnt() {
		return this.patientCnt;
	}

	public void setPatientCnt(BigDecimal patientCnt) {
		this.patientCnt = patientCnt;
	}

	public BigDecimal getRatedBedCnt() {
		return this.ratedBedCnt;
	}

	public void setRatedBedCnt(BigDecimal ratedBedCnt) {
		this.ratedBedCnt = ratedBedCnt;
	}

	public BigDecimal getSspmAccompanyCnt() {
		return this.sspmAccompanyCnt;
	}

	public void setSspmAccompanyCnt(BigDecimal sspmAccompanyCnt) {
		this.sspmAccompanyCnt = sspmAccompanyCnt;
	}

	public BigDecimal getSubspecialtyIn() {
		return this.subspecialtyIn;
	}

	public void setSubspecialtyIn(BigDecimal subspecialtyIn) {
		this.subspecialtyIn = subspecialtyIn;
	}

	public BigDecimal getSubspecialtyOut() {
		return this.subspecialtyOut;
	}

	public void setSubspecialtyOut(BigDecimal subspecialtyOut) {
		this.subspecialtyOut = subspecialtyOut;
	}

	public Date getTxnDate() {
		return this.txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public Wardmstr getWardmstr() {
		return this.wardmstr;
	}

	public void setWardmstr(Wardmstr wardmstr) {
		this.wardmstr = wardmstr;
	}

}