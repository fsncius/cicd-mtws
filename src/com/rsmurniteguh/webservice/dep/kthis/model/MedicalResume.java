package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MEDICAL_RESUME database table.
 * 
 */
@Entity
@Table(name="MEDICAL_RESUME")
@NamedQuery(name="MedicalResume.findAll", query="SELECT m FROM MedicalResume m")
public class MedicalResume implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MEDICAL_RESUME_ID")
	private long medicalResumeId;

	@Column(name="ANAMNESE_DESC")
	private String anamneseDesc;

	@Column(name="CAREPROVIDER_ID")
	private BigDecimal careproviderId;

	@Temporal(TemporalType.DATE)
	@Column(name="CONTROL_DATETIME")
	private Date controlDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="DISCHARGED_DATETIME")
	private Date dischargedDatetime;

	@Column(name="IN_DIAGNOSISMSTR_ID")
	private BigDecimal inDiagnosismstrId;

	@Column(name="IP_INDICATION_DESC")
	private String ipIndicationDesc;

	@Column(name="LAST_CONDITION")
	private String lastCondition;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MAIN_DIAGNOSISMSTR_ID")
	private BigDecimal mainDiagnosismstrId;

	@Column(name="PHYSIC_CHECK_DESC")
	private String physicCheckDesc;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="RESUME_DATETIME")
	private Date resumeDatetime;

	@Column(name="SEC_DIAGNOSIS_DESC")
	private String secDiagnosisDesc;

	@Column(name="SUPPORT_CHECK_DESC")
	private String supportCheckDesc;

	@Column(name="THERAPY_DESC")
	private String therapyDesc;

	@Column(name="THERAPY_DISCHARGED_DESC")
	private String therapyDischargedDesc;

	//bi-directional many-to-one association to Visit
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VISIT_ID")
	private Visit visit;

	public MedicalResume() {
	}

	public long getMedicalResumeId() {
		return this.medicalResumeId;
	}

	public void setMedicalResumeId(long medicalResumeId) {
		this.medicalResumeId = medicalResumeId;
	}

	public String getAnamneseDesc() {
		return this.anamneseDesc;
	}

	public void setAnamneseDesc(String anamneseDesc) {
		this.anamneseDesc = anamneseDesc;
	}

	public BigDecimal getCareproviderId() {
		return this.careproviderId;
	}

	public void setCareproviderId(BigDecimal careproviderId) {
		this.careproviderId = careproviderId;
	}

	public Date getControlDatetime() {
		return this.controlDatetime;
	}

	public void setControlDatetime(Date controlDatetime) {
		this.controlDatetime = controlDatetime;
	}

	public Date getDischargedDatetime() {
		return this.dischargedDatetime;
	}

	public void setDischargedDatetime(Date dischargedDatetime) {
		this.dischargedDatetime = dischargedDatetime;
	}

	public BigDecimal getInDiagnosismstrId() {
		return this.inDiagnosismstrId;
	}

	public void setInDiagnosismstrId(BigDecimal inDiagnosismstrId) {
		this.inDiagnosismstrId = inDiagnosismstrId;
	}

	public String getIpIndicationDesc() {
		return this.ipIndicationDesc;
	}

	public void setIpIndicationDesc(String ipIndicationDesc) {
		this.ipIndicationDesc = ipIndicationDesc;
	}

	public String getLastCondition() {
		return this.lastCondition;
	}

	public void setLastCondition(String lastCondition) {
		this.lastCondition = lastCondition;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMainDiagnosismstrId() {
		return this.mainDiagnosismstrId;
	}

	public void setMainDiagnosismstrId(BigDecimal mainDiagnosismstrId) {
		this.mainDiagnosismstrId = mainDiagnosismstrId;
	}

	public String getPhysicCheckDesc() {
		return this.physicCheckDesc;
	}

	public void setPhysicCheckDesc(String physicCheckDesc) {
		this.physicCheckDesc = physicCheckDesc;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public Date getResumeDatetime() {
		return this.resumeDatetime;
	}

	public void setResumeDatetime(Date resumeDatetime) {
		this.resumeDatetime = resumeDatetime;
	}

	public String getSecDiagnosisDesc() {
		return this.secDiagnosisDesc;
	}

	public void setSecDiagnosisDesc(String secDiagnosisDesc) {
		this.secDiagnosisDesc = secDiagnosisDesc;
	}

	public String getSupportCheckDesc() {
		return this.supportCheckDesc;
	}

	public void setSupportCheckDesc(String supportCheckDesc) {
		this.supportCheckDesc = supportCheckDesc;
	}

	public String getTherapyDesc() {
		return this.therapyDesc;
	}

	public void setTherapyDesc(String therapyDesc) {
		this.therapyDesc = therapyDesc;
	}

	public String getTherapyDischargedDesc() {
		return this.therapyDischargedDesc;
	}

	public void setTherapyDischargedDesc(String therapyDischargedDesc) {
		this.therapyDischargedDesc = therapyDischargedDesc;
	}

	public Visit getVisit() {
		return this.visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}