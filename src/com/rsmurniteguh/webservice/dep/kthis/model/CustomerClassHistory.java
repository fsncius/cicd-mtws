package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity(name="CUSTOMERCLASSHISTORY")
public final class CustomerClassHistory implements Serializable {

	public CustomerClassHistory() {}

	@Transient
	private Set<String> modifiedSet = new HashSet<String>();
	public Set<String> getModifiedSet() { return modifiedSet; }

	// FIELDS

	@Id
	@Column(name="CUSTOMERCLASSHISTORY_ID")
	private BigDecimal customerClassHistoryId;
	@Column(name="PATIENT_ID")
	private BigDecimal patientId;
	@Column(name="CUSTOMERSERVICEMSTR_ID")
	private BigDecimal customerServiceMstrId;
	@Column(name="OLD_CUSTOMERCLASSMSTR_ID")
	private BigDecimal oldCustomerClassMstrId;
	@Column(name="OLD_EFFECTIVE_DATE")
	private Timestamp oldEffectiveDate;
	@Column(name="OLD_EXPIRY_DATE")
	private Timestamp oldExpiryDate;
	@Column(name="NEW_CUSTOMERCLASSMSTR_ID")
	private BigDecimal newCustomerClassMstrId;
	@Column(name="NEW_EFFECTIVE_DATE")
	private Timestamp newEffectiveDate;
	@Column(name="NEW_EXPIRY_DATE")
	private Timestamp newExpiryDate;
	@Column(name="REASON")
	private String reason;
	@Column(name="REMARKS")
	private String remarks;
	@Column(name="RECEIPT_ID")
	private BigDecimal receiptId;
	@Column(name="ENTITYMSTR_ID")
	private BigDecimal entityMstrId;
	@Column(name="LOCATIONMSTR_ID")
	private BigDecimal locationMstrId;
	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;
	@Column(name="ENTERED_DATETIME")
	private Timestamp enteredDatetime;

	// GETTERS

	public BigDecimal getCustomerClassHistoryId() { return customerClassHistoryId; }
	public BigDecimal getPatientId() { return patientId; }
	public BigDecimal getCustomerServiceMstrId() { return customerServiceMstrId; }
	public BigDecimal getOldCustomerClassMstrId() { return oldCustomerClassMstrId; }
	public Timestamp getOldEffectiveDate() { return oldEffectiveDate; }
	public Timestamp getOldExpiryDate() { return oldExpiryDate; }
	public BigDecimal getNewCustomerClassMstrId() { return newCustomerClassMstrId; }
	public Timestamp getNewEffectiveDate() { return newEffectiveDate; }
	public Timestamp getNewExpiryDate() { return newExpiryDate; }
	public String getReason() { return reason; }
	public String getRemarks() { return remarks; }
	public BigDecimal getReceiptId() { return receiptId; }
	public BigDecimal getEntityMstrId() { return entityMstrId; }
	public BigDecimal getLocationMstrId() { return locationMstrId; }
	public BigDecimal getEnteredBy() { return enteredBy; }
	public Timestamp getEnteredDatetime() { return enteredDatetime; }

	// SETTERS

	public void setCustomerClassHistoryId(BigDecimal customerClassHistoryId) {
		this.customerClassHistoryId = customerClassHistoryId;
		this.modifiedSet.add("customerClassHistoryId");
	}
	public void setPatientId(BigDecimal patientId) {
		this.patientId = patientId;
		this.modifiedSet.add("patientId");
	}
	public void setCustomerServiceMstrId(BigDecimal customerServiceMstrId) {
		this.customerServiceMstrId = customerServiceMstrId;
		this.modifiedSet.add("customerServiceMstrId");
	}
	public void setOldCustomerClassMstrId(BigDecimal oldCustomerClassMstrId) {
		this.oldCustomerClassMstrId = oldCustomerClassMstrId;
		this.modifiedSet.add("oldCustomerClassMstrId");
	}
	public void setOldEffectiveDate(Timestamp oldEffectiveDate) {
		this.oldEffectiveDate = oldEffectiveDate;
		this.modifiedSet.add("oldEffectiveDate");
	}
	public void setOldExpiryDate(Timestamp oldExpiryDate) {
		this.oldExpiryDate = oldExpiryDate;
		this.modifiedSet.add("oldExpiryDate");
	}
	public void setNewCustomerClassMstrId(BigDecimal newCustomerClassMstrId) {
		this.newCustomerClassMstrId = newCustomerClassMstrId;
		this.modifiedSet.add("newCustomerClassMstrId");
	}
	public void setNewEffectiveDate(Timestamp newEffectiveDate) {
		this.newEffectiveDate = newEffectiveDate;
		this.modifiedSet.add("newEffectiveDate");
	}
	public void setNewExpiryDate(Timestamp newExpiryDate) {
		this.newExpiryDate = newExpiryDate;
		this.modifiedSet.add("newExpiryDate");
	}
	public void setReason(String reason) {
		this.reason = reason;
		this.modifiedSet.add("reason");
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
		this.modifiedSet.add("remarks");
	}
	public void setReceiptId(BigDecimal receiptId) {
		this.receiptId = receiptId;
		this.modifiedSet.add("receiptId");
	}
	public void setEntityMstrId(BigDecimal entityMstrId) {
		this.entityMstrId = entityMstrId;
		this.modifiedSet.add("entityMstrId");
	}
	public void setLocationMstrId(BigDecimal locationMstrId) {
		this.locationMstrId = locationMstrId;
		this.modifiedSet.add("locationMstrId");
	}
	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
		this.modifiedSet.add("enteredBy");
	}
	public void setEnteredDatetime(Timestamp enteredDatetime) {
		this.enteredDatetime = enteredDatetime;
		this.modifiedSet.add("enteredDatetime");
	}

	// RESET

	public void resetModifiedFlag(boolean flag) {
		if (flag) {
			modifiedSet.add("customerClassHistoryId");
			modifiedSet.add("patientId");
			modifiedSet.add("customerServiceMstrId");
			modifiedSet.add("oldCustomerClassMstrId");
			modifiedSet.add("oldEffectiveDate");
			modifiedSet.add("oldExpiryDate");
			modifiedSet.add("newCustomerClassMstrId");
			modifiedSet.add("newEffectiveDate");
			modifiedSet.add("newExpiryDate");
			modifiedSet.add("reason");
			modifiedSet.add("remarks");
			modifiedSet.add("receiptId");
			modifiedSet.add("entityMstrId");
			modifiedSet.add("locationMstrId");
			modifiedSet.add("enteredBy");
			modifiedSet.add("enteredDatetime");
		} else {
			modifiedSet.clear();
		}
	}

	public void resetModifiedFlag() {
		resetModifiedFlag(false);
	}

	// TOSTRING

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("CustomerClassHistory: ");
		sb.append(customerClassHistoryId + ",");
		sb.append(patientId + ",");
		sb.append(customerServiceMstrId + ",");
		sb.append(oldCustomerClassMstrId + ",");
		sb.append(oldEffectiveDate + ",");
		sb.append(oldExpiryDate + ",");
		sb.append(newCustomerClassMstrId + ",");
		sb.append(newEffectiveDate + ",");
		sb.append(newExpiryDate + ",");
		sb.append(reason + ",");
		sb.append(remarks + ",");
		sb.append(receiptId + ",");
		sb.append(entityMstrId + ",");
		sb.append(locationMstrId + ",");
		sb.append(enteredBy + ",");
		sb.append(enteredDatetime + ",");
		sb.append("]");
		return sb.toString();
	}

}