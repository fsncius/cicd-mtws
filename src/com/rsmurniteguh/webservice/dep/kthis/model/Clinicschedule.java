package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CLINICSCHEDULE database table.
 * 
 */
@Entity
@NamedQuery(name="Clinicschedule.findAll", query="SELECT c FROM Clinicschedule c")
public class Clinicschedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CLINICSCHEDULE_ID")
	private long clinicscheduleId;

	@Temporal(TemporalType.DATE)
	@Column(name="CLINIC_DATETIME")
	private Date clinicDatetime;

	@Column(name="CLINIC_DOCTOR_ID")
	private BigDecimal clinicDoctorId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="RESOURCEMSTR_ID")
	private BigDecimal resourcemstrId;

	public Clinicschedule() {
	}

	public long getClinicscheduleId() {
		return this.clinicscheduleId;
	}

	public void setClinicscheduleId(long clinicscheduleId) {
		this.clinicscheduleId = clinicscheduleId;
	}

	public Date getClinicDatetime() {
		return this.clinicDatetime;
	}

	public void setClinicDatetime(Date clinicDatetime) {
		this.clinicDatetime = clinicDatetime;
	}

	public BigDecimal getClinicDoctorId() {
		return this.clinicDoctorId;
	}

	public void setClinicDoctorId(BigDecimal clinicDoctorId) {
		this.clinicDoctorId = clinicDoctorId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getResourcemstrId() {
		return this.resourcemstrId;
	}

	public void setResourcemstrId(BigDecimal resourcemstrId) {
		this.resourcemstrId = resourcemstrId;
	}

}