package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the RECEIPTSEPARATEPOLICY database table.
 * 
 */
@Entity
@NamedQuery(name="Receiptseparatepolicy.findAll", query="SELECT r FROM Receiptseparatepolicy r")
public class Receiptseparatepolicy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RECEIPTSEPARATEPOLICY_ID")
	private long receiptseparatepolicyId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MAX_BILL_RECNUM")
	private BigDecimal maxBillRecnum;

	@Column(name="MAX_TXN_RECNUM")
	private BigDecimal maxTxnRecnum;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="RECEIPT_CAT")
	private String receiptCat;

	@Column(name="RECEIPT_ROUNDING_RULE")
	private String receiptRoundingRule;

	public Receiptseparatepolicy() {
	}

	public long getReceiptseparatepolicyId() {
		return this.receiptseparatepolicyId;
	}

	public void setReceiptseparatepolicyId(long receiptseparatepolicyId) {
		this.receiptseparatepolicyId = receiptseparatepolicyId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaxBillRecnum() {
		return this.maxBillRecnum;
	}

	public void setMaxBillRecnum(BigDecimal maxBillRecnum) {
		this.maxBillRecnum = maxBillRecnum;
	}

	public BigDecimal getMaxTxnRecnum() {
		return this.maxTxnRecnum;
	}

	public void setMaxTxnRecnum(BigDecimal maxTxnRecnum) {
		this.maxTxnRecnum = maxTxnRecnum;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public String getReceiptCat() {
		return this.receiptCat;
	}

	public void setReceiptCat(String receiptCat) {
		this.receiptCat = receiptCat;
	}

	public String getReceiptRoundingRule() {
		return this.receiptRoundingRule;
	}

	public void setReceiptRoundingRule(String receiptRoundingRule) {
		this.receiptRoundingRule = receiptRoundingRule;
	}

}