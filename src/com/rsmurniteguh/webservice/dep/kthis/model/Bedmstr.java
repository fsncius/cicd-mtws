package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BEDMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Bedmstr.findAll", query="SELECT b FROM Bedmstr b")
public class Bedmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BEDMSTR_ID")
	private long bedmstrId;

	@Column(name="BED_CAT")
	private String bedCat;

	@Column(name="BED_DESC")
	private String bedDesc;

	@Column(name="BED_DESC_LANG1")
	private String bedDescLang1;

	@Column(name="BED_DESC_LANG2")
	private String bedDescLang2;

	@Column(name="BED_DESC_LANG3")
	private String bedDescLang3;

	@Column(name="BED_NO")
	private String bedNo;

	@Temporal(TemporalType.DATE)
	@Column(name="BED_RESERVATION_TIME")
	private Date bedReservationTime;

	@Column(name="BED_STATUS")
	private String bedStatus;

	@Column(name="BED_TYPE")
	private String bedType;

	@Column(name="BORROWING_WARDMSTR_ID")
	private BigDecimal borrowingWardmstrId;

	@Column(name="CONTACT_NO")
	private String contactNo;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="WARNING_MSG")
	private String warningMsg;

	//bi-directional many-to-one association to Bedhistory
	@OneToMany(mappedBy="bedmstr")
	private List<Bedhistory> bedhistories;

	//bi-directional many-to-one association to Accommmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ACCOMMMSTR_ID")
	private Accommmstr accommmstr;

	//bi-directional many-to-one association to Roommstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ROOMMSTR_ID")
	private Roommstr roommstr;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	//bi-directional many-to-one association to Visit
	@OneToMany(mappedBy="bedmstr")
	private List<Visit> visits;

	//bi-directional many-to-one association to Visitappointment
	@OneToMany(mappedBy="bedmstr")
	private List<Visitappointment> visitappointments;

	public Bedmstr() {
	}

	public long getBedmstrId() {
		return this.bedmstrId;
	}

	public void setBedmstrId(long bedmstrId) {
		this.bedmstrId = bedmstrId;
	}

	public String getBedCat() {
		return this.bedCat;
	}

	public void setBedCat(String bedCat) {
		this.bedCat = bedCat;
	}

	public String getBedDesc() {
		return this.bedDesc;
	}

	public void setBedDesc(String bedDesc) {
		this.bedDesc = bedDesc;
	}

	public String getBedDescLang1() {
		return this.bedDescLang1;
	}

	public void setBedDescLang1(String bedDescLang1) {
		this.bedDescLang1 = bedDescLang1;
	}

	public String getBedDescLang2() {
		return this.bedDescLang2;
	}

	public void setBedDescLang2(String bedDescLang2) {
		this.bedDescLang2 = bedDescLang2;
	}

	public String getBedDescLang3() {
		return this.bedDescLang3;
	}

	public void setBedDescLang3(String bedDescLang3) {
		this.bedDescLang3 = bedDescLang3;
	}

	public String getBedNo() {
		return this.bedNo;
	}

	public void setBedNo(String bedNo) {
		this.bedNo = bedNo;
	}

	public Date getBedReservationTime() {
		return this.bedReservationTime;
	}

	public void setBedReservationTime(Date bedReservationTime) {
		this.bedReservationTime = bedReservationTime;
	}

	public String getBedStatus() {
		return this.bedStatus;
	}

	public void setBedStatus(String bedStatus) {
		this.bedStatus = bedStatus;
	}

	public String getBedType() {
		return this.bedType;
	}

	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	public BigDecimal getBorrowingWardmstrId() {
		return this.borrowingWardmstrId;
	}

	public void setBorrowingWardmstrId(BigDecimal borrowingWardmstrId) {
		this.borrowingWardmstrId = borrowingWardmstrId;
	}

	public String getContactNo() {
		return this.contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getWarningMsg() {
		return this.warningMsg;
	}

	public void setWarningMsg(String warningMsg) {
		this.warningMsg = warningMsg;
	}

	public List<Bedhistory> getBedhistories() {
		return this.bedhistories;
	}

	public void setBedhistories(List<Bedhistory> bedhistories) {
		this.bedhistories = bedhistories;
	}

	public Bedhistory addBedhistory(Bedhistory bedhistory) {
		getBedhistories().add(bedhistory);
		bedhistory.setBedmstr(this);

		return bedhistory;
	}

	public Bedhistory removeBedhistory(Bedhistory bedhistory) {
		getBedhistories().remove(bedhistory);
		bedhistory.setBedmstr(null);

		return bedhistory;
	}

	public Accommmstr getAccommmstr() {
		return this.accommmstr;
	}

	public void setAccommmstr(Accommmstr accommmstr) {
		this.accommmstr = accommmstr;
	}

	public Roommstr getRoommstr() {
		return this.roommstr;
	}

	public void setRoommstr(Roommstr roommstr) {
		this.roommstr = roommstr;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

	public List<Visit> getVisits() {
		return this.visits;
	}

	public void setVisits(List<Visit> visits) {
		this.visits = visits;
	}

	public Visit addVisit(Visit visit) {
		getVisits().add(visit);
		visit.setBedmstr(this);

		return visit;
	}

	public Visit removeVisit(Visit visit) {
		getVisits().remove(visit);
		visit.setBedmstr(null);

		return visit;
	}

	public List<Visitappointment> getVisitappointments() {
		return this.visitappointments;
	}

	public void setVisitappointments(List<Visitappointment> visitappointments) {
		this.visitappointments = visitappointments;
	}

	public Visitappointment addVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().add(visitappointment);
		visitappointment.setBedmstr(this);

		return visitappointment;
	}

	public Visitappointment removeVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().remove(visitappointment);
		visitappointment.setBedmstr(null);

		return visitappointment;
	}

}