package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the BABYPRELIMINARYASSESSMENT database table.
 * 
 */
@Entity
@Table(name = "Babypreliminaryassessment")
@NamedQuery(name="Babypreliminaryassessment.findAll", query="SELECT b FROM Babypreliminaryassessment b")
//@NamedQuery(name="Babypreliminaryassessment.findById", query="SELECT b FROM Babypreliminaryassessment b where b.babypreliminaryassessmentId = :id")
public class Babypreliminaryassessment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ABDOMEN_CONDITION_CODE")
	private String abdomenConditionCode;

	@Column(name="ABDOMEN_CONDITION_REMARK")
	private String abdomenConditionRemark;

	@Column(name="ADDBREASTFEED_APPROVAL")
	private String addbreastfeedApproval;

	@Temporal(TemporalType.DATE)
	@Column(name="ADDBREASTFEED_DATETIME")
	private Date addbreastfeedDatetime;

	@Lob
	@Column(name="ADDBREASTFEED_SIGN")
	private byte[] addbreastfeedSign;

	@Column(name="ANUS_CONDITION_CODE")
	private String anusConditionCode;

	@Column(name="ANUS_CONDITION_REMARK")
	private String anusConditionRemark;

	@Column(name="APGAR_ACTIVITY")
	private String apgarActivity;

	@Column(name="APGAR_APPEARANCE")
	private String apgarAppearance;

	@Column(name="APGAR_GRIMACE")
	private String apgarGrimace;

	@Column(name="APGAR_PULSE")
	private String apgarPulse;

	@Column(name="APGAR_RESPIRATION")
	private String apgarRespiration;

	@Column(name="APGAR_SCORE")
	private BigDecimal apgarScore;

	@Column(name="APGAR_SCORE_UNIT")
	private String apgarScoreUnit;

	@Temporal(TemporalType.DATE)
	@Column(name="ASSESSMENT_DATE")
	private Date assessmentDate;
	
	@Id
	@Column(name="BABYPRELIMINARYASSESSMENT_ID")
	private BigDecimal babypreliminaryassessmentId;

	@Column(name="BACK_CONDITION_CODE")
	private String backConditionCode;

	@Column(name="BACK_CONDITION_REMARK")
	private String backConditionRemark;

	@Column(name="BCG_APPROVAL")
	private String bcgApproval;

	@Temporal(TemporalType.DATE)
	@Column(name="BCG_DATETIME")
	private Date bcgDatetime;

	@Lob
	@Column(name="BCG_SIGN")
	private byte[] bcgSign;

	@Temporal(TemporalType.DATE)
	@Column(name="BIRTH_DATETIME")
	private Date birthDatetime;

	@Column(name="BIRTH_HEIGHT")
	private BigDecimal birthHeight;

	@Column(name="BIRTH_WEIGHT")
	private BigDecimal birthWeight;

	@Column(name="BREASTFEED_APPROVAL")
	private String breastfeedApproval;

	@Temporal(TemporalType.DATE)
	@Column(name="BREASTFEED_DATETIME")
	private Date breastfeedDatetime;

	@Lob
	@Column(name="BREASTFEED_SIGN")
	private byte[] breastfeedSign;

	private BigDecimal careprovider;

	@Column(name="CAREPROVIDER_ASSISTANT")
	private BigDecimal careproviderAssistant;

	@Column(name="CHEST_CONDITION_CODE")
	private String chestConditionCode;

	@Column(name="CHEST_CONDITION_REMARK")
	private String chestConditionRemark;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_AT")
	private Date createdAt;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Lob
	@Column(name="DOCTOR_SIGN")
	private byte[] doctorSign;

	@Column(name="EAR_CONDITION_CODE")
	private String earConditionCode;

	@Column(name="EAR_CONDITION_REMARK")
	private String earConditionRemark;

	@Column(name="EXTREMITY_CONDITION_CODE")
	private String extremityConditionCode;

	@Column(name="EXTREMITY_CONDITION_REMARK")
	private String extremityConditionRemark;

	@Column(name="EYE_CONDITION_CODE")
	private String eyeConditionCode;

	@Column(name="EYE_CONDITION_REMARK")
	private String eyeConditionRemark;

	@Column(name="GENITALIA_CONDITION_CODE")
	private String genitaliaConditionCode;

	@Column(name="GENITALIA_CONDITION_REMARK")
	private String genitaliaConditionRemark;

	@Column(name="HEAD_CONDITION_CODE")
	private String headConditionCode;

	@Column(name="HEAD_CONDITION_REMARK")
	private String headConditionRemark;

	@Column(name="HEART_CONDITION_CODE")
	private String heartConditionCode;

	@Column(name="HEART_CONDITION_REMARK")
	private String heartConditionRemark;

	@Column(name="HEPB_APPROVAL")
	private String hepbApproval;

	@Temporal(TemporalType.DATE)
	@Column(name="HEPB_DATETIME")
	private Date hepbDatetime;

	@Lob
	@Column(name="HEPB_SIGN")
	private byte[] hepbSign;

	private BigDecimal lk;

	@Column(name="LUNG_CONDITION_CODE")
	private String lungConditionCode;

	@Column(name="LUNG_CONDITION_REMARK")
	private String lungConditionRemark;

	@Column(name="MILK_APPROVAL")
	private String milkApproval;

	@Temporal(TemporalType.DATE)
	@Column(name="MILK_DATETIME")
	private Date milkDatetime;

	@Lob
	@Column(name="MILK_SIGN")
	private byte[] milkSign;

	@Column(name="NEO_K_PROVIDEDBY")
	private BigDecimal neoKProvidedby;

	@Column(name="NEO_K_TREATMENT")
	private String neoKTreatment;

	@Column(name="NEWBORN_ID")
	private BigDecimal newbornId;

	@Column(name="PARTUS_METHOD")
	private String partusMethod;

	@Column(name="POLIO_APPROVAL")
	private String polioApproval;

	@Temporal(TemporalType.DATE)
	@Column(name="POLIO_DATETIME")
	private Date polioDatetime;

	@Lob
	@Column(name="POLIO_SIGN")
	private byte[] polioSign;

	@Column(name="REFLEXES_CONDITION_CODE")
	private String reflexesConditionCode;

	@Column(name="REFLEXES_CONDITION_REMARK")
	private String reflexesConditionRemark;

	private String remark;

	private String sex;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_AT")
	private Date updatedAt;

	@Column(name="UPDATED_BY")
	private BigDecimal updatedBy;

	public Babypreliminaryassessment() {
	}

	public String getAbdomenConditionCode() {
		return this.abdomenConditionCode;
	}

	public void setAbdomenConditionCode(String abdomenConditionCode) {
		this.abdomenConditionCode = abdomenConditionCode;
	}

	public String getAbdomenConditionRemark() {
		return this.abdomenConditionRemark;
	}

	public void setAbdomenConditionRemark(String abdomenConditionRemark) {
		this.abdomenConditionRemark = abdomenConditionRemark;
	}

	public String getAddbreastfeedApproval() {
		return this.addbreastfeedApproval;
	}

	public void setAddbreastfeedApproval(String addbreastfeedApproval) {
		this.addbreastfeedApproval = addbreastfeedApproval;
	}

	public Date getAddbreastfeedDatetime() {
		return this.addbreastfeedDatetime;
	}

	public void setAddbreastfeedDatetime(Date addbreastfeedDatetime) {
		this.addbreastfeedDatetime = addbreastfeedDatetime;
	}

	public byte[] getAddbreastfeedSign() {
		return this.addbreastfeedSign;
	}

	public void setAddbreastfeedSign(byte[] addbreastfeedSign) {
		this.addbreastfeedSign = addbreastfeedSign;
	}

	public String getAnusConditionCode() {
		return this.anusConditionCode;
	}

	public void setAnusConditionCode(String anusConditionCode) {
		this.anusConditionCode = anusConditionCode;
	}

	public String getAnusConditionRemark() {
		return this.anusConditionRemark;
	}

	public void setAnusConditionRemark(String anusConditionRemark) {
		this.anusConditionRemark = anusConditionRemark;
	}

	public String getApgarActivity() {
		return this.apgarActivity;
	}

	public void setApgarActivity(String apgarActivity) {
		this.apgarActivity = apgarActivity;
	}

	public String getApgarAppearance() {
		return this.apgarAppearance;
	}

	public void setApgarAppearance(String apgarAppearance) {
		this.apgarAppearance = apgarAppearance;
	}

	public String getApgarGrimace() {
		return this.apgarGrimace;
	}

	public void setApgarGrimace(String apgarGrimace) {
		this.apgarGrimace = apgarGrimace;
	}

	public String getApgarPulse() {
		return this.apgarPulse;
	}

	public void setApgarPulse(String apgarPulse) {
		this.apgarPulse = apgarPulse;
	}

	public String getApgarRespiration() {
		return this.apgarRespiration;
	}

	public void setApgarRespiration(String apgarRespiration) {
		this.apgarRespiration = apgarRespiration;
	}

	public BigDecimal getApgarScore() {
		return this.apgarScore;
	}

	public void setApgarScore(BigDecimal apgarScore) {
		this.apgarScore = apgarScore;
	}

	public String getApgarScoreUnit() {
		return this.apgarScoreUnit;
	}

	public void setApgarScoreUnit(String apgarScoreUnit) {
		this.apgarScoreUnit = apgarScoreUnit;
	}

	public Date getAssessmentDate() {
		return this.assessmentDate;
	}

	public void setAssessmentDate(Date assessmentDate) {
		this.assessmentDate = assessmentDate;
	}

	public BigDecimal getBabypreliminaryassessmentId() {
		return this.babypreliminaryassessmentId;
	}

	public void setBabypreliminaryassessmentId(BigDecimal babypreliminaryassessmentId) {
		this.babypreliminaryassessmentId = babypreliminaryassessmentId;
	}

	public String getBackConditionCode() {
		return this.backConditionCode;
	}

	public void setBackConditionCode(String backConditionCode) {
		this.backConditionCode = backConditionCode;
	}

	public String getBackConditionRemark() {
		return this.backConditionRemark;
	}

	public void setBackConditionRemark(String backConditionRemark) {
		this.backConditionRemark = backConditionRemark;
	}

	public String getBcgApproval() {
		return this.bcgApproval;
	}

	public void setBcgApproval(String bcgApproval) {
		this.bcgApproval = bcgApproval;
	}

	public Date getBcgDatetime() {
		return this.bcgDatetime;
	}

	public void setBcgDatetime(Date bcgDatetime) {
		this.bcgDatetime = bcgDatetime;
	}

	public byte[] getBcgSign() {
		return this.bcgSign;
	}

	public void setBcgSign(byte[] bcgSign) {
		this.bcgSign = bcgSign;
	}

	public Date getBirthDatetime() {
		return this.birthDatetime;
	}

	public void setBirthDatetime(Date birthDatetime) {
		this.birthDatetime = birthDatetime;
	}

	public BigDecimal getBirthHeight() {
		return this.birthHeight;
	}

	public void setBirthHeight(BigDecimal birthHeight) {
		this.birthHeight = birthHeight;
	}

	public BigDecimal getBirthWeight() {
		return this.birthWeight;
	}

	public void setBirthWeight(BigDecimal birthWeight) {
		this.birthWeight = birthWeight;
	}

	public String getBreastfeedApproval() {
		return this.breastfeedApproval;
	}

	public void setBreastfeedApproval(String breastfeedApproval) {
		this.breastfeedApproval = breastfeedApproval;
	}

	public Date getBreastfeedDatetime() {
		return this.breastfeedDatetime;
	}

	public void setBreastfeedDatetime(Date breastfeedDatetime) {
		this.breastfeedDatetime = breastfeedDatetime;
	}

	public byte[] getBreastfeedSign() {
		return this.breastfeedSign;
	}

	public void setBreastfeedSign(byte[] breastfeedSign) {
		this.breastfeedSign = breastfeedSign;
	}

	public BigDecimal getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(BigDecimal careprovider) {
		this.careprovider = careprovider;
	}

	public BigDecimal getCareproviderAssistant() {
		return this.careproviderAssistant;
	}

	public void setCareproviderAssistant(BigDecimal careproviderAssistant) {
		this.careproviderAssistant = careproviderAssistant;
	}

	public String getChestConditionCode() {
		return this.chestConditionCode;
	}

	public void setChestConditionCode(String chestConditionCode) {
		this.chestConditionCode = chestConditionCode;
	}

	public String getChestConditionRemark() {
		return this.chestConditionRemark;
	}

	public void setChestConditionRemark(String chestConditionRemark) {
		this.chestConditionRemark = chestConditionRemark;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public byte[] getDoctorSign() {
		return this.doctorSign;
	}

	public void setDoctorSign(byte[] doctorSign) {
		this.doctorSign = doctorSign;
	}

	public String getEarConditionCode() {
		return this.earConditionCode;
	}

	public void setEarConditionCode(String earConditionCode) {
		this.earConditionCode = earConditionCode;
	}

	public String getEarConditionRemark() {
		return this.earConditionRemark;
	}

	public void setEarConditionRemark(String earConditionRemark) {
		this.earConditionRemark = earConditionRemark;
	}

	public String getExtremityConditionCode() {
		return this.extremityConditionCode;
	}

	public void setExtremityConditionCode(String extremityConditionCode) {
		this.extremityConditionCode = extremityConditionCode;
	}

	public String getExtremityConditionRemark() {
		return this.extremityConditionRemark;
	}

	public void setExtremityConditionRemark(String extremityConditionRemark) {
		this.extremityConditionRemark = extremityConditionRemark;
	}

	public String getEyeConditionCode() {
		return this.eyeConditionCode;
	}

	public void setEyeConditionCode(String eyeConditionCode) {
		this.eyeConditionCode = eyeConditionCode;
	}

	public String getEyeConditionRemark() {
		return this.eyeConditionRemark;
	}

	public void setEyeConditionRemark(String eyeConditionRemark) {
		this.eyeConditionRemark = eyeConditionRemark;
	}

	public String getGenitaliaConditionCode() {
		return this.genitaliaConditionCode;
	}

	public void setGenitaliaConditionCode(String genitaliaConditionCode) {
		this.genitaliaConditionCode = genitaliaConditionCode;
	}

	public String getGenitaliaConditionRemark() {
		return this.genitaliaConditionRemark;
	}

	public void setGenitaliaConditionRemark(String genitaliaConditionRemark) {
		this.genitaliaConditionRemark = genitaliaConditionRemark;
	}

	public String getHeadConditionCode() {
		return this.headConditionCode;
	}

	public void setHeadConditionCode(String headConditionCode) {
		this.headConditionCode = headConditionCode;
	}

	public String getHeadConditionRemark() {
		return this.headConditionRemark;
	}

	public void setHeadConditionRemark(String headConditionRemark) {
		this.headConditionRemark = headConditionRemark;
	}

	public String getHeartConditionCode() {
		return this.heartConditionCode;
	}

	public void setHeartConditionCode(String heartConditionCode) {
		this.heartConditionCode = heartConditionCode;
	}

	public String getHeartConditionRemark() {
		return this.heartConditionRemark;
	}

	public void setHeartConditionRemark(String heartConditionRemark) {
		this.heartConditionRemark = heartConditionRemark;
	}

	public String getHepbApproval() {
		return this.hepbApproval;
	}

	public void setHepbApproval(String hepbApproval) {
		this.hepbApproval = hepbApproval;
	}

	public Date getHepbDatetime() {
		return this.hepbDatetime;
	}

	public void setHepbDatetime(Date hepbDatetime) {
		this.hepbDatetime = hepbDatetime;
	}

	public byte[] getHepbSign() {
		return this.hepbSign;
	}

	public void setHepbSign(byte[] hepbSign) {
		this.hepbSign = hepbSign;
	}

	public BigDecimal getLk() {
		return this.lk;
	}

	public void setLk(BigDecimal lk) {
		this.lk = lk;
	}

	public String getLungConditionCode() {
		return this.lungConditionCode;
	}

	public void setLungConditionCode(String lungConditionCode) {
		this.lungConditionCode = lungConditionCode;
	}

	public String getLungConditionRemark() {
		return this.lungConditionRemark;
	}

	public void setLungConditionRemark(String lungConditionRemark) {
		this.lungConditionRemark = lungConditionRemark;
	}

	public String getMilkApproval() {
		return this.milkApproval;
	}

	public void setMilkApproval(String milkApproval) {
		this.milkApproval = milkApproval;
	}

	public Date getMilkDatetime() {
		return this.milkDatetime;
	}

	public void setMilkDatetime(Date milkDatetime) {
		this.milkDatetime = milkDatetime;
	}

	public byte[] getMilkSign() {
		return this.milkSign;
	}

	public void setMilkSign(byte[] milkSign) {
		this.milkSign = milkSign;
	}

	public BigDecimal getNeoKProvidedby() {
		return this.neoKProvidedby;
	}

	public void setNeoKProvidedby(BigDecimal neoKProvidedby) {
		this.neoKProvidedby = neoKProvidedby;
	}

	public String getNeoKTreatment() {
		return this.neoKTreatment;
	}

	public void setNeoKTreatment(String neoKTreatment) {
		this.neoKTreatment = neoKTreatment;
	}

	public BigDecimal getNewbornId() {
		return this.newbornId;
	}

	public void setNewbornId(BigDecimal newbornId) {
		this.newbornId = newbornId;
	}

	public String getPartusMethod() {
		return this.partusMethod;
	}

	public void setPartusMethod(String partusMethod) {
		this.partusMethod = partusMethod;
	}

	public String getPolioApproval() {
		return this.polioApproval;
	}

	public void setPolioApproval(String polioApproval) {
		this.polioApproval = polioApproval;
	}

	public Date getPolioDatetime() {
		return this.polioDatetime;
	}

	public void setPolioDatetime(Date polioDatetime) {
		this.polioDatetime = polioDatetime;
	}

	public byte[] getPolioSign() {
		return this.polioSign;
	}

	public void setPolioSign(byte[] polioSign) {
		this.polioSign = polioSign;
	}

	public String getReflexesConditionCode() {
		return this.reflexesConditionCode;
	}

	public void setReflexesConditionCode(String reflexesConditionCode) {
		this.reflexesConditionCode = reflexesConditionCode;
	}

	public String getReflexesConditionRemark() {
		return this.reflexesConditionRemark;
	}

	public void setReflexesConditionRemark(String reflexesConditionRemark) {
		this.reflexesConditionRemark = reflexesConditionRemark;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public BigDecimal getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

}