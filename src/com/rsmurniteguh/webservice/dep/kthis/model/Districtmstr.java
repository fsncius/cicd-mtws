package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DISTRICTMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Districtmstr.findAll", query="SELECT d FROM Districtmstr d")
public class Districtmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DISTRICTMSTR_ID")
	private long districtmstrId;

	@Column(name="CODE_SEQ")
	private BigDecimal codeSeq;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DISTRICT_CODE")
	private String districtCode;

	@Column(name="DISTRICT_DESC")
	private String districtDesc;

	@Column(name="DISTRICT_DESC_LANG1")
	private String districtDescLang1;

	@Column(name="DISTRICT_DESC_LANG2")
	private String districtDescLang2;

	@Column(name="DISTRICT_DESC_LANG3")
	private String districtDescLang3;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="SHORT_CODE")
	private String shortCode;

	//bi-directional many-to-one association to Citymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CITYMSTR_ID")
	private Citymstr citymstr;

	//bi-directional many-to-one association to Statemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATEMSTR_ID")
	private Statemstr statemstr;

	public Districtmstr() {
	}

	public long getDistrictmstrId() {
		return this.districtmstrId;
	}

	public void setDistrictmstrId(long districtmstrId) {
		this.districtmstrId = districtmstrId;
	}

	public BigDecimal getCodeSeq() {
		return this.codeSeq;
	}

	public void setCodeSeq(BigDecimal codeSeq) {
		this.codeSeq = codeSeq;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDistrictCode() {
		return this.districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getDistrictDesc() {
		return this.districtDesc;
	}

	public void setDistrictDesc(String districtDesc) {
		this.districtDesc = districtDesc;
	}

	public String getDistrictDescLang1() {
		return this.districtDescLang1;
	}

	public void setDistrictDescLang1(String districtDescLang1) {
		this.districtDescLang1 = districtDescLang1;
	}

	public String getDistrictDescLang2() {
		return this.districtDescLang2;
	}

	public void setDistrictDescLang2(String districtDescLang2) {
		this.districtDescLang2 = districtDescLang2;
	}

	public String getDistrictDescLang3() {
		return this.districtDescLang3;
	}

	public void setDistrictDescLang3(String districtDescLang3) {
		this.districtDescLang3 = districtDescLang3;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Citymstr getCitymstr() {
		return this.citymstr;
	}

	public void setCitymstr(Citymstr citymstr) {
		this.citymstr = citymstr;
	}

	public Statemstr getStatemstr() {
		return this.statemstr;
	}

	public void setStatemstr(Statemstr statemstr) {
		this.statemstr = statemstr;
	}

}