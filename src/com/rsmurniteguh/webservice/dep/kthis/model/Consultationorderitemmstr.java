package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CONSULTATIONORDERITEMMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Consultationorderitemmstr.findAll", query="SELECT c FROM Consultationorderitemmstr c")
public class Consultationorderitemmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CONSULTATIONORDERITEMMSTR_ID")
	private long consultationorderitemmstrId;

	@Column(name="CONSULTATION_SSPM_IND")
	private String consultationSspmInd;

	@Column(name="CONSULTATION_TYPE")
	private String consultationType;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="ORDERITEMMSTR_ID")
	private BigDecimal orderitemmstrId;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONSULTATION_SSPM_ID")
	private Subspecialtymstr subspecialtymstr;

	public Consultationorderitemmstr() {
	}

	public long getConsultationorderitemmstrId() {
		return this.consultationorderitemmstrId;
	}

	public void setConsultationorderitemmstrId(long consultationorderitemmstrId) {
		this.consultationorderitemmstrId = consultationorderitemmstrId;
	}

	public String getConsultationSspmInd() {
		return this.consultationSspmInd;
	}

	public void setConsultationSspmInd(String consultationSspmInd) {
		this.consultationSspmInd = consultationSspmInd;
	}

	public String getConsultationType() {
		return this.consultationType;
	}

	public void setConsultationType(String consultationType) {
		this.consultationType = consultationType;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getOrderitemmstrId() {
		return this.orderitemmstrId;
	}

	public void setOrderitemmstrId(BigDecimal orderitemmstrId) {
		this.orderitemmstrId = orderitemmstrId;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

}