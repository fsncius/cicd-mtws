package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the STOCKCOUNTBALANCE database table.
 * 
 */
@Entity
@NamedQuery(name="Stockcountbalance.findAll", query="SELECT s FROM Stockcountbalance s")
public class Stockcountbalance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOCKCOUNTBALANCE_ID")
	private long stockcountbalanceId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="QTY_ON_COUNTBALANCE")
	private BigDecimal qtyOnCountbalance;

	//bi-directional many-to-one association to Stockcountdetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOCKCOUNTDETAIL_ID")
	private Stockcountdetail stockcountdetail;

	//bi-directional many-to-one association to Storeitemstock
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREITEMSTOCK_ID")
	private Storeitemstock storeitemstock;

	public Stockcountbalance() {
	}

	public long getStockcountbalanceId() {
		return this.stockcountbalanceId;
	}

	public void setStockcountbalanceId(long stockcountbalanceId) {
		this.stockcountbalanceId = stockcountbalanceId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getQtyOnCountbalance() {
		return this.qtyOnCountbalance;
	}

	public void setQtyOnCountbalance(BigDecimal qtyOnCountbalance) {
		this.qtyOnCountbalance = qtyOnCountbalance;
	}

	public Stockcountdetail getStockcountdetail() {
		return this.stockcountdetail;
	}

	public void setStockcountdetail(Stockcountdetail stockcountdetail) {
		this.stockcountdetail = stockcountdetail;
	}

	public Storeitemstock getStoreitemstock() {
		return this.storeitemstock;
	}

	public void setStoreitemstock(Storeitemstock storeitemstock) {
		this.storeitemstock = storeitemstock;
	}

}