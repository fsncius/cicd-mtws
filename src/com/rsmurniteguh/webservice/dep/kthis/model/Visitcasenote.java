package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITCASENOTE database table.
 * 
 */
@Entity
@NamedQuery(name="Visitcasenote.findAll", query="SELECT v FROM Visitcasenote v")
public class Visitcasenote implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITCASENOTE_ID")
	private long visitcasenoteId;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFUNCT_BY")
	private BigDecimal defunctBy;

	@Temporal(TemporalType.DATE)
	@Column(name="DEFUNCT_DATETIME")
	private Date defunctDatetime;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="VISIT_ID")
	private BigDecimal visitId;

	@Column(name="VISITCASENOTE_DESC")
	private String visitcasenoteDesc;

	@Column(name="VISITCASENOTE_TYPE")
	private String visitcasenoteType;

	//bi-directional many-to-one association to Careprovider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CREATED_BY")
	private Careprovider careprovider;

	//bi-directional many-to-one association to Subspecialtymstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBSPECIALTYMSTR_ID")
	private Subspecialtymstr subspecialtymstr;

	public Visitcasenote() {
	}

	public long getVisitcasenoteId() {
		return this.visitcasenoteId;
	}

	public void setVisitcasenoteId(long visitcasenoteId) {
		this.visitcasenoteId = visitcasenoteId;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getDefunctBy() {
		return this.defunctBy;
	}

	public void setDefunctBy(BigDecimal defunctBy) {
		this.defunctBy = defunctBy;
	}

	public Date getDefunctDatetime() {
		return this.defunctDatetime;
	}

	public void setDefunctDatetime(Date defunctDatetime) {
		this.defunctDatetime = defunctDatetime;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getVisitId() {
		return this.visitId;
	}

	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}

	public String getVisitcasenoteDesc() {
		return this.visitcasenoteDesc;
	}

	public void setVisitcasenoteDesc(String visitcasenoteDesc) {
		this.visitcasenoteDesc = visitcasenoteDesc;
	}

	public String getVisitcasenoteType() {
		return this.visitcasenoteType;
	}

	public void setVisitcasenoteType(String visitcasenoteType) {
		this.visitcasenoteType = visitcasenoteType;
	}

	public Careprovider getCareprovider() {
		return this.careprovider;
	}

	public void setCareprovider(Careprovider careprovider) {
		this.careprovider = careprovider;
	}

	public Subspecialtymstr getSubspecialtymstr() {
		return this.subspecialtymstr;
	}

	public void setSubspecialtymstr(Subspecialtymstr subspecialtymstr) {
		this.subspecialtymstr = subspecialtymstr;
	}

}