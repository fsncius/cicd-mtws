package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the REFUNDPYMPOLICY database table.
 * 
 */
@Entity
@NamedQuery(name="Refundpympolicy.findAll", query="SELECT r FROM Refundpympolicy r")
public class Refundpympolicy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REFUNDPYMPOLICY_ID")
	private long refundpympolicyId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="DEFAULT_IND")
	private String defaultInd;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PATIENT_CLASS")
	private String patientClass;

	@Column(name="PATIENT_TYPE")
	private String patientType;

	@Column(name="PYM_CANCEL")
	private String pymCancel;

	@Column(name="PYM_ORIGIN")
	private String pymOrigin;

	@Column(name="RECEIPT_CAT")
	private String receiptCat;

	public Refundpympolicy() {
	}

	public long getRefundpympolicyId() {
		return this.refundpympolicyId;
	}

	public void setRefundpympolicyId(long refundpympolicyId) {
		this.refundpympolicyId = refundpympolicyId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDefaultInd() {
		return this.defaultInd;
	}

	public void setDefaultInd(String defaultInd) {
		this.defaultInd = defaultInd;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getPatientClass() {
		return this.patientClass;
	}

	public void setPatientClass(String patientClass) {
		this.patientClass = patientClass;
	}

	public String getPatientType() {
		return this.patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public String getPymCancel() {
		return this.pymCancel;
	}

	public void setPymCancel(String pymCancel) {
		this.pymCancel = pymCancel;
	}

	public String getPymOrigin() {
		return this.pymOrigin;
	}

	public void setPymOrigin(String pymOrigin) {
		this.pymOrigin = pymOrigin;
	}

	public String getReceiptCat() {
		return this.receiptCat;
	}

	public void setReceiptCat(String receiptCat) {
		this.receiptCat = receiptCat;
	}

}