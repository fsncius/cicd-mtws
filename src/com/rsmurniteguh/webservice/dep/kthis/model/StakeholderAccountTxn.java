package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;


@Entity(name="STAKEHOLDERACCOUNTTXN")
public final class StakeholderAccountTxn implements Serializable {

	public StakeholderAccountTxn() {}

	@Transient
	private Set<String> modifiedSet = new HashSet<String>();
	public Set<String> getModifiedSet() { return modifiedSet; }

	// FIELDS

	@Id
	@Column(name="STAKEHOLDERACCOUNTTXN_ID")
	private BigDecimal stakeholderAccountTxnId;
	@Column(name="STAKEHOLDER_ID")
	private BigDecimal stakeholderId;
	@Column(name="PARENT_SAT_ID")
	private BigDecimal parentSatId;
	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenueCentreMstrId;
	@Column(name="TXN_AMOUNT")
	private BigDecimal txnAmount;
	@Column(name="TXN_DESC")
	private String txnDesc;
	@Column(name="TXN_NO")
	private String txnNo;
	@Column(name="TXN_STATUS")
	private String txnStatus;
	@Column(name="TXNCODEMSTR_ID")
	private BigDecimal txnCodeMstrId;
	@Column(name="CANCEL_FOR_TXN_NO")
	private String cancelForTxnNo;
	@Column(name="ENTERED_BY")
	private BigDecimal enteredBy;
	@Column(name="ENTERED_DATETIME")
	private Timestamp enteredDateTime;
	@Column(name="REMARKS")
	private String remarks;
	@Column(name="PATIENTACCOUNTTXN_ID")
	private BigDecimal patientAccountTxnId;
	@Column(name="GLACCOUNTMSTR_ID")
	private BigDecimal glAccountMstrId;
	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;
	@Column(name="LAST_UPDATED_DATETIME")
	private Timestamp lastUpdatedDateTime;
	@Column(name="POSTED_DATETIME")
	private Timestamp postedDateTime;
	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;
	@Column(name="PREV_UPDATED_DATETIME")
	private Timestamp prevUpdatedDateTime;
	@Column(name="REFERENCE_NO")
	private String referenceNo;

	// GETTERS

	public BigDecimal getStakeholderAccountTxnId() { return stakeholderAccountTxnId; }
	public BigDecimal getStakeholderId() { return stakeholderId; }
	public BigDecimal getParentSatId() { return parentSatId; }
	public BigDecimal getRevenueCentreMstrId() { return revenueCentreMstrId; }
	public BigDecimal getTxnAmount() { return txnAmount; }
	public String getTxnDesc() { return txnDesc; }
	public String getTxnNo() { return txnNo; }
	public String getTxnStatus() { return txnStatus; }
	public BigDecimal getTxnCodeMstrId() { return txnCodeMstrId; }
	public String getCancelForTxnNo() { return cancelForTxnNo; }
	public BigDecimal getEnteredBy() { return enteredBy; }
	public Timestamp getEnteredDateTime() { return enteredDateTime; }
	public String getRemarks() { return remarks; }
	public BigDecimal getPatientAccountTxnId() { return patientAccountTxnId; }
	public BigDecimal getGlAccountMstrId() { return glAccountMstrId; }
	public BigDecimal getLastUpdatedBy() { return lastUpdatedBy; }
	public Timestamp getLastUpdatedDateTime() { return lastUpdatedDateTime; }
	public Timestamp getPostedDateTime() { return postedDateTime; }
	public BigDecimal getPrevUpdatedBy() { return prevUpdatedBy; }
	public Timestamp getPrevUpdatedDateTime() { return prevUpdatedDateTime; }
	public String getReferenceNo() { return referenceNo; }

	// SETTERS

	public void setStakeholderAccountTxnId(BigDecimal stakeholderAccountTxnId) {
		this.stakeholderAccountTxnId = stakeholderAccountTxnId;
		this.modifiedSet.add("stakeholderAccountTxnId");
	}
	public void setStakeholderId(BigDecimal stakeholderId) {
		this.stakeholderId = stakeholderId;
		this.modifiedSet.add("stakeholderId");
	}
	public void setParentSatId(BigDecimal parentSatId) {
		this.parentSatId = parentSatId;
		this.modifiedSet.add("parentSatId");
	}
	public void setRevenueCentreMstrId(BigDecimal revenueCentreMstrId) {
		this.revenueCentreMstrId = revenueCentreMstrId;
		this.modifiedSet.add("revenueCentreMstrId");
	}
	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
		this.modifiedSet.add("txnAmount");
	}
	public void setTxnDesc(String txnDesc) {
		this.txnDesc = txnDesc;
		this.modifiedSet.add("txnDesc");
	}
	public void setTxnNo(String txnNo) {
		this.txnNo = txnNo;
		this.modifiedSet.add("txnNo");
	}
	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
		this.modifiedSet.add("txnStatus");
	}
	public void setTxnCodeMstrId(BigDecimal txnCodeMstrId) {
		this.txnCodeMstrId = txnCodeMstrId;
		this.modifiedSet.add("txnCodeMstrId");
	}
	public void setCancelForTxnNo(String cancelForTxnNo) {
		this.cancelForTxnNo = cancelForTxnNo;
		this.modifiedSet.add("cancelForTxnNo");
	}
	public void setEnteredBy(BigDecimal enteredBy) {
		this.enteredBy = enteredBy;
		this.modifiedSet.add("enteredBy");
	}
	public void setEnteredDateTime(Timestamp enteredDateTime) {
		this.enteredDateTime = enteredDateTime;
		this.modifiedSet.add("enteredDateTime");
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
		this.modifiedSet.add("remarks");
	}
	public void setPatientAccountTxnId(BigDecimal patientAccountTxnId) {
		this.patientAccountTxnId = patientAccountTxnId;
		this.modifiedSet.add("patientAccountTxnId");
	}
	public void setGlAccountMstrId(BigDecimal glAccountMstrId) {
		this.glAccountMstrId = glAccountMstrId;
		this.modifiedSet.add("glAccountMstrId");
	}
	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
		this.modifiedSet.add("lastUpdatedBy");
	}
	public void setLastUpdatedDateTime(Timestamp lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
		this.modifiedSet.add("lastUpdatedDateTime");
	}
	public void setPostedDateTime(Timestamp postedDateTime) {
		this.postedDateTime = postedDateTime;
		this.modifiedSet.add("postedDateTime");
	}
	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
		this.modifiedSet.add("prevUpdatedBy");
	}
	public void setPrevUpdatedDateTime(Timestamp prevUpdatedDateTime) {
		this.prevUpdatedDateTime = prevUpdatedDateTime;
		this.modifiedSet.add("prevUpdatedDateTime");
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
		this.modifiedSet.add("referenceNo");
	}

	// RESET

	public void resetModifiedFlag(boolean flag) {
		if (flag) {
			modifiedSet.add("stakeholderAccountTxnId");
			modifiedSet.add("stakeholderId");
			modifiedSet.add("parentSatId");
			modifiedSet.add("revenueCentreMstrId");
			modifiedSet.add("txnAmount");
			modifiedSet.add("txnDesc");
			modifiedSet.add("txnNo");
			modifiedSet.add("txnStatus");
			modifiedSet.add("txnCodeMstrId");
			modifiedSet.add("cancelForTxnNo");
			modifiedSet.add("enteredBy");
			modifiedSet.add("enteredDateTime");
			modifiedSet.add("remarks");
			modifiedSet.add("patientAccountTxnId");
			modifiedSet.add("glAccountMstrId");
			modifiedSet.add("lastUpdatedBy");
			modifiedSet.add("lastUpdatedDateTime");
			modifiedSet.add("postedDateTime");
			modifiedSet.add("prevUpdatedBy");
			modifiedSet.add("prevUpdatedDateTime");
			modifiedSet.add("referenceNo");
		} else {
			modifiedSet.clear();
		}
	}

	public void resetModifiedFlag() {
		resetModifiedFlag(false);
	}

	// TOSTRING

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("StakeholderAccountTxn: ");
		sb.append(stakeholderAccountTxnId + ",");
		sb.append(stakeholderId + ",");
		sb.append(parentSatId + ",");
		sb.append(revenueCentreMstrId + ",");
		sb.append(txnAmount + ",");
		sb.append(txnDesc + ",");
		sb.append(txnNo + ",");
		sb.append(txnStatus + ",");
		sb.append(txnCodeMstrId + ",");
		sb.append(cancelForTxnNo + ",");
		sb.append(enteredBy + ",");
		sb.append(enteredDateTime + ",");
		sb.append(remarks + ",");
		sb.append(patientAccountTxnId + ",");
		sb.append(glAccountMstrId + ",");
		sb.append(lastUpdatedBy + ",");
		sb.append(lastUpdatedDateTime + ",");
		sb.append(postedDateTime + ",");
		sb.append(prevUpdatedBy + ",");
		sb.append(prevUpdatedDateTime + ",");
		sb.append(referenceNo + ",");
		sb.append("]");
		return sb.toString();
	}

}