package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VISITPRELIMINARYCAREINFO database table.
 * 
 */
@Entity
@NamedQuery(name="Visitpreliminarycareinfo.findAll", query="SELECT v FROM Visitpreliminarycareinfo v")
public class Visitpreliminarycareinfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VISITPRELIMINARYCAREINFO_ID")
	private long visitpreliminarycareinfoId;

	@Column(name="ALCOHOL_DAY_COUNT")
	private String alcoholDayCount;

	@Column(name="ALCOHOL_IND")
	private String alcoholInd;

	@Column(name="ALCOHOL_ISSUES_IND")
	private String alcoholIssuesInd;

	@Column(name="ALCOHOL_TYPE")
	private String alcoholType;

	@Column(name="ALLERGY_HISTORY_IND")
	private String allergyHistoryInd;

	@Column(name="ALLERGY_REACTION_IND")
	private String allergyReactionInd;

	@Column(name="ALLERGY_REACTION_TEXT")
	private String allergyReactionText;

	@Column(name="AMBULATORY_TOOL")
	private String ambulatoryTool;

	@Column(name="AMBULATORY_TOOL_DESC")
	private String ambulatoryToolDesc;

	@Temporal(TemporalType.DATE)
	@Column(name="ARRIVAL_DATETIME")
	private Date arrivalDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="ASSESSMENT_DATETIME")
	private Date assessmentDatetime;

	@Column(name="ASSESSMENT_GET_INFO")
	private String assessmentGetInfo;

	@Column(name="ASSESSMENT_RELATIONSHIP")
	private String assessmentRelationship;

	@Column(name="BATHROOM_FIRST_FLOOR")
	private String bathroomFirstFloor;

	@Column(name="BELL_REACH_DESC")
	private String bellReachDesc;

	@Column(name="BELL_REACH_IND")
	private String bellReachInd;

	@Column(name="BLOOD_PRESSURE_DIASTOLIC")
	private String bloodPressureDiastolic;

	@Column(name="BLOOD_PRESSURE_SYSTOLIC")
	private String bloodPressureSystolic;

	@Column(name="BLOOD_TRANSFUSION_IND")
	private String bloodTransfusionInd;

	@Column(name="BREAST_SELF_CHECK_IND")
	private String breastSelfCheckInd;

	@Column(name="BREATH_TOOL")
	private String breathTool;

	@Column(name="BREATH_TOOL_TEXT")
	private String breathToolText;

	@Column(name="CATH_SIZE_DESC")
	private String cathSizeDesc;

	@Column(name="CATH_TYPE_DESC")
	private String cathTypeDesc;

	@Column(name="CHECK_IN_METHOD")
	private String checkInMethod;

	@Column(name="CHECK_IN_TEXT1")
	private String checkInText1;

	private String chest;

	@Column(name="CHEST_TEXT")
	private String chestText;

	private String conscious;

	@Column(name="CONTRACEPTION_TYPE")
	private String contraceptionType;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="CURRENT_DIET")
	private String currentDiet;

	@Column(name="CURRENT_RISK_CONDITION")
	private String currentRiskCondition;

	@Column(name="DAILY_LANGUAGE")
	private String dailyLanguage;

	@Column(name="DAYS_CAPABILITY")
	private String daysCapability;

	private String defecate;

	@Column(name="DEFECATE_COLOSTOMY_DESC")
	private String defecateColostomyDesc;

	@Column(name="DEFECATE_INFO")
	private String defecateInfo;

	@Column(name="DISEASE_NUTRITION")
	private String diseaseNutrition;

	@Column(name="DISEASE_NUTRITION_DESC")
	private String diseaseNutritionDesc;

	@Column(name="DISORDERS_STATUS")
	private String disordersStatus;

	@Column(name="DOCTOR_DIAGNOSIS")
	private String doctorDiagnosis;

	private String ear;

	@Column(name="EAR_TEXT")
	private String earText;

	@Column(name="EAT_INFO")
	private String eatInfo;

	@Column(name="EDUCATION_LEVEL")
	private String educationLevel;

	@Column(name="EDUCATION_LEVEL_DESC")
	private String educationLevelDesc;

	@Column(name="ENVIRONMENT_SUPERVISED")
	private String environmentSupervised;

	@Column(name="ESCAPE_RISK_IND")
	private String escapeRiskInd;

	private String extremity;

	@Column(name="EXTREMITY_BOTTOM_LEFT")
	private String extremityBottomLeft;

	@Column(name="EXTREMITY_BOTTOM_RIGHT")
	private String extremityBottomRight;

	@Column(name="EXTREMITY_PARESE")
	private String extremityParese;

	@Column(name="EXTREMITY_PLEGI")
	private String extremityPlegi;

	@Column(name="EXTREMITY_TOP_LEFT")
	private String extremityTopLeft;

	@Column(name="EXTREMITY_TOP_RIGHT")
	private String extremityTopRight;

	private String eye;

	@Column(name="EYE_TEXT")
	private String eyeText;

	private String face;

	@Column(name="FALL_RISK_ACTIVITY")
	private String fallRiskActivity;

	@Column(name="FALL_RISK_AGE")
	private String fallRiskAge;

	@Column(name="FALL_RISK_DEFISIT")
	private String fallRiskDefisit;

	@Column(name="FALL_RISK_FALL_HISTORY")
	private String fallRiskFallHistory;

	@Column(name="FALL_RISK_HEALTH_TOOL")
	private String fallRiskHealthTool;

	@Column(name="FALL_RISK_KOGNISI")
	private String fallRiskKognisi;

	@Column(name="FALL_RISK_KOMORBIDITAS")
	private String fallRiskKomorbiditas;

	@Column(name="FALL_RISK_MOBILITY")
	private String fallRiskMobility;

	@Column(name="FALL_RISK_PREVENT")
	private String fallRiskPrevent;

	@Column(name="FALL_RISK_URINATE")
	private String fallRiskUrinate;

	@Column(name="FAMILY_DISEASES_HISTORY")
	private String familyDiseasesHistory;

	@Column(name="FAMILY_DISEASES_HISTORY_DESC")
	private String familyDiseasesHistoryDesc;

	@Column(name="FAVORITE_FOOD")
	private String favoriteFood;

	@Column(name="GCS_E")
	private String gcsE;

	@Column(name="GCS_M")
	private String gcsM;

	@Column(name="GCS_V")
	private String gcsV;

	private String genitalia;

	@Column(name="GENITALIA_TEXT")
	private String genitaliaText;

	@Column(name="GENITALS_DISORDERS_DESC")
	private String genitalsDisordersDesc;

	private String hair;

	@Column(name="HAVE_STAIRS")
	private String haveStairs;

	private String head;

	@Column(name="HEARING_ISSUE")
	private String hearingIssue;

	@Column(name="HEARING_LIQUID_DESC")
	private String hearingLiquidDesc;

	@Column(name="HEARING_LOST_DESC")
	private String hearingLostDesc;

	@Column(name="HEARING_TOOL_DESC")
	private String hearingToolDesc;

	private String heart;

	@Column(name="HEART_OTHERS")
	private String heartOthers;

	@Column(name="HEART_PACEMAKER")
	private String heartPacemaker;

	private String height;

	@Column(name="HOME_TREATMENT_HELP")
	private String homeTreatmentHelp;

	@Column(name="HOME_TREATMENT_ID")
	private String homeTreatmentId;

	@Column(name="HOME_TREATMENT_IND")
	private String homeTreatmentInd;

	@Column(name="HOME_TREATMENT_PEOPLE")
	private String homeTreatmentPeople;

	@Column(name="HOME_TREATMENT_TEXT")
	private String homeTreatmentText;

	@Column(name="HOSPITAL_FEAR_IND")
	private String hospitalFearInd;

	@Column(name="HOUSE_CONDITION")
	private String houseCondition;

	@Column(name="INFO_TO_FAMILY")
	private String infoToFamily;

	private String inhalation;

	@Column(name="INHALATION_IS_REGULAR")
	private String inhalationIsRegular;

	private String integumentary;

	@Column(name="INTEGUMENTARY_INJURY")
	private String integumentaryInjury;

	@Column(name="INTEGUMENTARY_SCORE")
	private String integumentaryScore;

	@Column(name="INTEGUMENTARY_STAGE")
	private String integumentaryStage;

	@Column(name="INTEGUMENTARY_TURGOR")
	private String integumentaryTurgor;

	@Column(name="ISSUE_LIST_NURSING")
	private String issueListNursing;

	@Column(name="ISSUE_LIST_OTHER")
	private String issueListOther;

	@Column(name="LAST_CERVIX_CHECK")
	private String lastCervixCheck;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MENSTRUATION_DATE")
	private Date lastMenstruationDate;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="LEARN_METHODS")
	private String learnMethods;

	@Column(name="LEARN_METHODS_DESC")
	private String learnMethodsDesc;

	@Column(name="LEARN_OBSTACLES")
	private String learnObstacles;

	@Column(name="LEARN_OBSTACLES_DESC")
	private String learnObstaclesDesc;

	@Column(name="LEARN_POTENTIAL")
	private String learnPotential;

	@Column(name="LEARN_POTENTIAL_DESC")
	private String learnPotentialDesc;

	@Column(name="LEFT_LIGHT_REACTION")
	private String leftLightReaction;

	@Column(name="LEFT_PUPIL")
	private String leftPupil;

	@Column(name="LIVING_WITH")
	private String livingWith;

	@Column(name="LIVING_WITH_DESC")
	private String livingWithDesc;

	@Column(name="MAIN_REACTION_ALLERGY")
	private String mainReactionAllergy;

	@Temporal(TemporalType.DATE)
	@Column(name="MAMOGRAFI_LAST_DATE")
	private Date mamografiLastDate;

	@Column(name="MEDICAL_HISTORY")
	private String medicalHistory;

	@Column(name="MEDICINE_ALLERGY_IND")
	private String medicineAllergyInd;

	@Column(name="MEDICINE_ALLERGY_TEXT")
	private String medicineAllergyText;

	@Column(name="MENTAL_STATUS")
	private String mentalStatus;

	@Column(name="MENTAL_STATUS_DESC")
	private String mentalStatusDesc;

	private String mouth;

	@Column(name="MOUTH_TEXT")
	private String mouthText;

	private String neck;

	@Column(name="NECK_TEXT")
	private String neckText;

	@Column(name="NEED_TRANSLATOR_DESC")
	private String needTranslatorDesc;

	@Column(name="NEED_TRANSLATOR_IND")
	private String needTranslatorInd;

	private String nose;

	@Column(name="NOSE_TEXT")
	private String noseText;

	@Column(name="NUTRITION_ISSUES")
	private String nutritionIssues;

	@Column(name="NUTRITION_ISSUES_DESC")
	private String nutritionIssuesDesc;

	@Column(name="NUTRITION_ISSUES_IND")
	private String nutritionIssuesInd;

	@Column(name="OBESITY_IND")
	private String obesityInd;

	@Column(name="OBESITY_INFO")
	private String obesityInfo;

	private String occupation;

	@Column(name="OCCUPATION_DESC")
	private String occupationDesc;

	@Column(name="OTHERS_ALLERGY")
	private String othersAllergy;

	@Column(name="OTHERS_ALLERGY_TEXT")
	private String othersAllergyText;

	@Column(name="OTHERS_LANGUAGE")
	private String othersLanguage;

	@Column(name="PAIN_DESC_ID")
	private BigDecimal painDescId;

	@Column(name="PAIN_EFFECTS")
	private String painEffects;

	@Column(name="PAIN_EFFECTS_DESC")
	private String painEffectsDesc;

	@Column(name="PAIN_IND")
	private String painInd;

	@Column(name="PAIN_INTENSITY_IMG")
	private String painIntensityImg;

	@Column(name="PAIN_INTENSITY_LEVEL")
	private String painIntensityLevel;

	@Column(name="PREGNANCY_IND")
	private String pregnancyInd;

	@Column(name="PREV_RELEASE_RESTRAINT")
	private String prevReleaseRestraint;

	@Column(name="PROSTATE_ISSUES_IND")
	private String prostateIssuesInd;

	private String pulse;

	@Column(name="PULSE_IS_REGULAR")
	private String pulseIsRegular;

	private String reason;

	@Column(name="REFRAIN_CAPABILITY_IND")
	private String refrainCapabilityInd;

	@Column(name="REPLACEMENT_NUTRITION")
	private String replacementNutrition;

	@Column(name="REPLACEMENT_NUTRITION_NO")
	private String replacementNutritionNo;

	private String respiration;

	@Column(name="RESPIRATION_TEXT")
	private String respirationText;

	@Column(name="RESTRAINT_DISCUSSION_POLICY")
	private String restraintDiscussionPolicy;

	@Column(name="RESTRAINT_IDENTIFICATION_IND")
	private String restraintIdentificationInd;

	@Column(name="RIGHT_LIGHT_REACTION")
	private String rightLightReaction;

	@Column(name="RIGHT_PUPIL")
	private String rightPupil;

	@Column(name="SECURITY_SAFETY_IND")
	private String securitySafetyInd;

	@Column(name="SEDATIVE_HISTORY_DESC")
	private String sedativeHistoryDesc;

	@Column(name="SEDATIVE_HISTORY_IND")
	private String sedativeHistoryInd;

	@Column(name="SIGHT_ISSUE")
	private String sightIssue;

	@Column(name="SIGHT_ISSUE_DESC")
	private String sightIssueDesc;

	@Column(name="SIGN_LANGUAGE_IND")
	private String signLanguageInd;

	@Column(name="SMOKE_DAY_COUNT")
	private String smokeDayCount;

	@Column(name="SMOKE_HISTORY")
	private String smokeHistory;

	@Column(name="SMOKE_IND")
	private String smokeInd;

	@Column(name="SMOKE_TYPE")
	private String smokeType;

	@Column(name="TALKING_DISORDERS_DESC")
	private String talkingDisordersDesc;

	@Column(name="TALKING_STATUS_IND")
	private String talkingStatusInd;

	private String teeth;

	@Column(name="TEETH_TEXT")
	private String teethText;

	private String temperature;

	private String throat;

	@Column(name="THROAT_TEXT")
	private String throatText;

	private String tongue;

	@Column(name="TONGUE_TEXT")
	private String tongueText;

	private String urinate;

	@Column(name="URINATE_INFO")
	private String urinateInfo;

	@Column(name="USED_CONTRACEPTION_IND")
	private String usedContraceptionInd;

	@Column(name="USED_RESTRAINT_IND")
	private String usedRestraintInd;

	@Column(name="USED_RESTRAINT_LOCATION")
	private String usedRestraintLocation;

	@Column(name="USED_RESTRAINT_TYPE")
	private String usedRestraintType;

	@Column(name="VERNACULAR_DESC")
	private String vernacularDesc;

	@Column(name="VISIT_ID")
	private BigDecimal visitId;

	@Column(name="WALKING_CAPABILITY")
	private String walkingCapability;

	@Column(name="WALKING_CAPABILITY_DESC")
	private String walkingCapabilityDesc;

	@Column(name="WALKING_ESCAPE_IND")
	private String walkingEscapeInd;

	private String weight;

	@Column(name="WEIGHT_LOSS_IND")
	private String weightLossInd;

	@Column(name="WORK_HISTORY")
	private String workHistory;

	public Visitpreliminarycareinfo() {
	}

	public long getVisitpreliminarycareinfoId() {
		return this.visitpreliminarycareinfoId;
	}

	public void setVisitpreliminarycareinfoId(long visitpreliminarycareinfoId) {
		this.visitpreliminarycareinfoId = visitpreliminarycareinfoId;
	}

	public String getAlcoholDayCount() {
		return this.alcoholDayCount;
	}

	public void setAlcoholDayCount(String alcoholDayCount) {
		this.alcoholDayCount = alcoholDayCount;
	}

	public String getAlcoholInd() {
		return this.alcoholInd;
	}

	public void setAlcoholInd(String alcoholInd) {
		this.alcoholInd = alcoholInd;
	}

	public String getAlcoholIssuesInd() {
		return this.alcoholIssuesInd;
	}

	public void setAlcoholIssuesInd(String alcoholIssuesInd) {
		this.alcoholIssuesInd = alcoholIssuesInd;
	}

	public String getAlcoholType() {
		return this.alcoholType;
	}

	public void setAlcoholType(String alcoholType) {
		this.alcoholType = alcoholType;
	}

	public String getAllergyHistoryInd() {
		return this.allergyHistoryInd;
	}

	public void setAllergyHistoryInd(String allergyHistoryInd) {
		this.allergyHistoryInd = allergyHistoryInd;
	}

	public String getAllergyReactionInd() {
		return this.allergyReactionInd;
	}

	public void setAllergyReactionInd(String allergyReactionInd) {
		this.allergyReactionInd = allergyReactionInd;
	}

	public String getAllergyReactionText() {
		return this.allergyReactionText;
	}

	public void setAllergyReactionText(String allergyReactionText) {
		this.allergyReactionText = allergyReactionText;
	}

	public String getAmbulatoryTool() {
		return this.ambulatoryTool;
	}

	public void setAmbulatoryTool(String ambulatoryTool) {
		this.ambulatoryTool = ambulatoryTool;
	}

	public String getAmbulatoryToolDesc() {
		return this.ambulatoryToolDesc;
	}

	public void setAmbulatoryToolDesc(String ambulatoryToolDesc) {
		this.ambulatoryToolDesc = ambulatoryToolDesc;
	}

	public Date getArrivalDatetime() {
		return this.arrivalDatetime;
	}

	public void setArrivalDatetime(Date arrivalDatetime) {
		this.arrivalDatetime = arrivalDatetime;
	}

	public Date getAssessmentDatetime() {
		return this.assessmentDatetime;
	}

	public void setAssessmentDatetime(Date assessmentDatetime) {
		this.assessmentDatetime = assessmentDatetime;
	}

	public String getAssessmentGetInfo() {
		return this.assessmentGetInfo;
	}

	public void setAssessmentGetInfo(String assessmentGetInfo) {
		this.assessmentGetInfo = assessmentGetInfo;
	}

	public String getAssessmentRelationship() {
		return this.assessmentRelationship;
	}

	public void setAssessmentRelationship(String assessmentRelationship) {
		this.assessmentRelationship = assessmentRelationship;
	}

	public String getBathroomFirstFloor() {
		return this.bathroomFirstFloor;
	}

	public void setBathroomFirstFloor(String bathroomFirstFloor) {
		this.bathroomFirstFloor = bathroomFirstFloor;
	}

	public String getBellReachDesc() {
		return this.bellReachDesc;
	}

	public void setBellReachDesc(String bellReachDesc) {
		this.bellReachDesc = bellReachDesc;
	}

	public String getBellReachInd() {
		return this.bellReachInd;
	}

	public void setBellReachInd(String bellReachInd) {
		this.bellReachInd = bellReachInd;
	}

	public String getBloodPressureDiastolic() {
		return this.bloodPressureDiastolic;
	}

	public void setBloodPressureDiastolic(String bloodPressureDiastolic) {
		this.bloodPressureDiastolic = bloodPressureDiastolic;
	}

	public String getBloodPressureSystolic() {
		return this.bloodPressureSystolic;
	}

	public void setBloodPressureSystolic(String bloodPressureSystolic) {
		this.bloodPressureSystolic = bloodPressureSystolic;
	}

	public String getBloodTransfusionInd() {
		return this.bloodTransfusionInd;
	}

	public void setBloodTransfusionInd(String bloodTransfusionInd) {
		this.bloodTransfusionInd = bloodTransfusionInd;
	}

	public String getBreastSelfCheckInd() {
		return this.breastSelfCheckInd;
	}

	public void setBreastSelfCheckInd(String breastSelfCheckInd) {
		this.breastSelfCheckInd = breastSelfCheckInd;
	}

	public String getBreathTool() {
		return this.breathTool;
	}

	public void setBreathTool(String breathTool) {
		this.breathTool = breathTool;
	}

	public String getBreathToolText() {
		return this.breathToolText;
	}

	public void setBreathToolText(String breathToolText) {
		this.breathToolText = breathToolText;
	}

	public String getCathSizeDesc() {
		return this.cathSizeDesc;
	}

	public void setCathSizeDesc(String cathSizeDesc) {
		this.cathSizeDesc = cathSizeDesc;
	}

	public String getCathTypeDesc() {
		return this.cathTypeDesc;
	}

	public void setCathTypeDesc(String cathTypeDesc) {
		this.cathTypeDesc = cathTypeDesc;
	}

	public String getCheckInMethod() {
		return this.checkInMethod;
	}

	public void setCheckInMethod(String checkInMethod) {
		this.checkInMethod = checkInMethod;
	}

	public String getCheckInText1() {
		return this.checkInText1;
	}

	public void setCheckInText1(String checkInText1) {
		this.checkInText1 = checkInText1;
	}

	public String getChest() {
		return this.chest;
	}

	public void setChest(String chest) {
		this.chest = chest;
	}

	public String getChestText() {
		return this.chestText;
	}

	public void setChestText(String chestText) {
		this.chestText = chestText;
	}

	public String getConscious() {
		return this.conscious;
	}

	public void setConscious(String conscious) {
		this.conscious = conscious;
	}

	public String getContraceptionType() {
		return this.contraceptionType;
	}

	public void setContraceptionType(String contraceptionType) {
		this.contraceptionType = contraceptionType;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getCurrentDiet() {
		return this.currentDiet;
	}

	public void setCurrentDiet(String currentDiet) {
		this.currentDiet = currentDiet;
	}

	public String getCurrentRiskCondition() {
		return this.currentRiskCondition;
	}

	public void setCurrentRiskCondition(String currentRiskCondition) {
		this.currentRiskCondition = currentRiskCondition;
	}

	public String getDailyLanguage() {
		return this.dailyLanguage;
	}

	public void setDailyLanguage(String dailyLanguage) {
		this.dailyLanguage = dailyLanguage;
	}

	public String getDaysCapability() {
		return this.daysCapability;
	}

	public void setDaysCapability(String daysCapability) {
		this.daysCapability = daysCapability;
	}

	public String getDefecate() {
		return this.defecate;
	}

	public void setDefecate(String defecate) {
		this.defecate = defecate;
	}

	public String getDefecateColostomyDesc() {
		return this.defecateColostomyDesc;
	}

	public void setDefecateColostomyDesc(String defecateColostomyDesc) {
		this.defecateColostomyDesc = defecateColostomyDesc;
	}

	public String getDefecateInfo() {
		return this.defecateInfo;
	}

	public void setDefecateInfo(String defecateInfo) {
		this.defecateInfo = defecateInfo;
	}

	public String getDiseaseNutrition() {
		return this.diseaseNutrition;
	}

	public void setDiseaseNutrition(String diseaseNutrition) {
		this.diseaseNutrition = diseaseNutrition;
	}

	public String getDiseaseNutritionDesc() {
		return this.diseaseNutritionDesc;
	}

	public void setDiseaseNutritionDesc(String diseaseNutritionDesc) {
		this.diseaseNutritionDesc = diseaseNutritionDesc;
	}

	public String getDisordersStatus() {
		return this.disordersStatus;
	}

	public void setDisordersStatus(String disordersStatus) {
		this.disordersStatus = disordersStatus;
	}

	public String getDoctorDiagnosis() {
		return this.doctorDiagnosis;
	}

	public void setDoctorDiagnosis(String doctorDiagnosis) {
		this.doctorDiagnosis = doctorDiagnosis;
	}

	public String getEar() {
		return this.ear;
	}

	public void setEar(String ear) {
		this.ear = ear;
	}

	public String getEarText() {
		return this.earText;
	}

	public void setEarText(String earText) {
		this.earText = earText;
	}

	public String getEatInfo() {
		return this.eatInfo;
	}

	public void setEatInfo(String eatInfo) {
		this.eatInfo = eatInfo;
	}

	public String getEducationLevel() {
		return this.educationLevel;
	}

	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}

	public String getEducationLevelDesc() {
		return this.educationLevelDesc;
	}

	public void setEducationLevelDesc(String educationLevelDesc) {
		this.educationLevelDesc = educationLevelDesc;
	}

	public String getEnvironmentSupervised() {
		return this.environmentSupervised;
	}

	public void setEnvironmentSupervised(String environmentSupervised) {
		this.environmentSupervised = environmentSupervised;
	}

	public String getEscapeRiskInd() {
		return this.escapeRiskInd;
	}

	public void setEscapeRiskInd(String escapeRiskInd) {
		this.escapeRiskInd = escapeRiskInd;
	}

	public String getExtremity() {
		return this.extremity;
	}

	public void setExtremity(String extremity) {
		this.extremity = extremity;
	}

	public String getExtremityBottomLeft() {
		return this.extremityBottomLeft;
	}

	public void setExtremityBottomLeft(String extremityBottomLeft) {
		this.extremityBottomLeft = extremityBottomLeft;
	}

	public String getExtremityBottomRight() {
		return this.extremityBottomRight;
	}

	public void setExtremityBottomRight(String extremityBottomRight) {
		this.extremityBottomRight = extremityBottomRight;
	}

	public String getExtremityParese() {
		return this.extremityParese;
	}

	public void setExtremityParese(String extremityParese) {
		this.extremityParese = extremityParese;
	}

	public String getExtremityPlegi() {
		return this.extremityPlegi;
	}

	public void setExtremityPlegi(String extremityPlegi) {
		this.extremityPlegi = extremityPlegi;
	}

	public String getExtremityTopLeft() {
		return this.extremityTopLeft;
	}

	public void setExtremityTopLeft(String extremityTopLeft) {
		this.extremityTopLeft = extremityTopLeft;
	}

	public String getExtremityTopRight() {
		return this.extremityTopRight;
	}

	public void setExtremityTopRight(String extremityTopRight) {
		this.extremityTopRight = extremityTopRight;
	}

	public String getEye() {
		return this.eye;
	}

	public void setEye(String eye) {
		this.eye = eye;
	}

	public String getEyeText() {
		return this.eyeText;
	}

	public void setEyeText(String eyeText) {
		this.eyeText = eyeText;
	}

	public String getFace() {
		return this.face;
	}

	public void setFace(String face) {
		this.face = face;
	}

	public String getFallRiskActivity() {
		return this.fallRiskActivity;
	}

	public void setFallRiskActivity(String fallRiskActivity) {
		this.fallRiskActivity = fallRiskActivity;
	}

	public String getFallRiskAge() {
		return this.fallRiskAge;
	}

	public void setFallRiskAge(String fallRiskAge) {
		this.fallRiskAge = fallRiskAge;
	}

	public String getFallRiskDefisit() {
		return this.fallRiskDefisit;
	}

	public void setFallRiskDefisit(String fallRiskDefisit) {
		this.fallRiskDefisit = fallRiskDefisit;
	}

	public String getFallRiskFallHistory() {
		return this.fallRiskFallHistory;
	}

	public void setFallRiskFallHistory(String fallRiskFallHistory) {
		this.fallRiskFallHistory = fallRiskFallHistory;
	}

	public String getFallRiskHealthTool() {
		return this.fallRiskHealthTool;
	}

	public void setFallRiskHealthTool(String fallRiskHealthTool) {
		this.fallRiskHealthTool = fallRiskHealthTool;
	}

	public String getFallRiskKognisi() {
		return this.fallRiskKognisi;
	}

	public void setFallRiskKognisi(String fallRiskKognisi) {
		this.fallRiskKognisi = fallRiskKognisi;
	}

	public String getFallRiskKomorbiditas() {
		return this.fallRiskKomorbiditas;
	}

	public void setFallRiskKomorbiditas(String fallRiskKomorbiditas) {
		this.fallRiskKomorbiditas = fallRiskKomorbiditas;
	}

	public String getFallRiskMobility() {
		return this.fallRiskMobility;
	}

	public void setFallRiskMobility(String fallRiskMobility) {
		this.fallRiskMobility = fallRiskMobility;
	}

	public String getFallRiskPrevent() {
		return this.fallRiskPrevent;
	}

	public void setFallRiskPrevent(String fallRiskPrevent) {
		this.fallRiskPrevent = fallRiskPrevent;
	}

	public String getFallRiskUrinate() {
		return this.fallRiskUrinate;
	}

	public void setFallRiskUrinate(String fallRiskUrinate) {
		this.fallRiskUrinate = fallRiskUrinate;
	}

	public String getFamilyDiseasesHistory() {
		return this.familyDiseasesHistory;
	}

	public void setFamilyDiseasesHistory(String familyDiseasesHistory) {
		this.familyDiseasesHistory = familyDiseasesHistory;
	}

	public String getFamilyDiseasesHistoryDesc() {
		return this.familyDiseasesHistoryDesc;
	}

	public void setFamilyDiseasesHistoryDesc(String familyDiseasesHistoryDesc) {
		this.familyDiseasesHistoryDesc = familyDiseasesHistoryDesc;
	}

	public String getFavoriteFood() {
		return this.favoriteFood;
	}

	public void setFavoriteFood(String favoriteFood) {
		this.favoriteFood = favoriteFood;
	}

	public String getGcsE() {
		return this.gcsE;
	}

	public void setGcsE(String gcsE) {
		this.gcsE = gcsE;
	}

	public String getGcsM() {
		return this.gcsM;
	}

	public void setGcsM(String gcsM) {
		this.gcsM = gcsM;
	}

	public String getGcsV() {
		return this.gcsV;
	}

	public void setGcsV(String gcsV) {
		this.gcsV = gcsV;
	}

	public String getGenitalia() {
		return this.genitalia;
	}

	public void setGenitalia(String genitalia) {
		this.genitalia = genitalia;
	}

	public String getGenitaliaText() {
		return this.genitaliaText;
	}

	public void setGenitaliaText(String genitaliaText) {
		this.genitaliaText = genitaliaText;
	}

	public String getGenitalsDisordersDesc() {
		return this.genitalsDisordersDesc;
	}

	public void setGenitalsDisordersDesc(String genitalsDisordersDesc) {
		this.genitalsDisordersDesc = genitalsDisordersDesc;
	}

	public String getHair() {
		return this.hair;
	}

	public void setHair(String hair) {
		this.hair = hair;
	}

	public String getHaveStairs() {
		return this.haveStairs;
	}

	public void setHaveStairs(String haveStairs) {
		this.haveStairs = haveStairs;
	}

	public String getHead() {
		return this.head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getHearingIssue() {
		return this.hearingIssue;
	}

	public void setHearingIssue(String hearingIssue) {
		this.hearingIssue = hearingIssue;
	}

	public String getHearingLiquidDesc() {
		return this.hearingLiquidDesc;
	}

	public void setHearingLiquidDesc(String hearingLiquidDesc) {
		this.hearingLiquidDesc = hearingLiquidDesc;
	}

	public String getHearingLostDesc() {
		return this.hearingLostDesc;
	}

	public void setHearingLostDesc(String hearingLostDesc) {
		this.hearingLostDesc = hearingLostDesc;
	}

	public String getHearingToolDesc() {
		return this.hearingToolDesc;
	}

	public void setHearingToolDesc(String hearingToolDesc) {
		this.hearingToolDesc = hearingToolDesc;
	}

	public String getHeart() {
		return this.heart;
	}

	public void setHeart(String heart) {
		this.heart = heart;
	}

	public String getHeartOthers() {
		return this.heartOthers;
	}

	public void setHeartOthers(String heartOthers) {
		this.heartOthers = heartOthers;
	}

	public String getHeartPacemaker() {
		return this.heartPacemaker;
	}

	public void setHeartPacemaker(String heartPacemaker) {
		this.heartPacemaker = heartPacemaker;
	}

	public String getHeight() {
		return this.height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getHomeTreatmentHelp() {
		return this.homeTreatmentHelp;
	}

	public void setHomeTreatmentHelp(String homeTreatmentHelp) {
		this.homeTreatmentHelp = homeTreatmentHelp;
	}

	public String getHomeTreatmentId() {
		return this.homeTreatmentId;
	}

	public void setHomeTreatmentId(String homeTreatmentId) {
		this.homeTreatmentId = homeTreatmentId;
	}

	public String getHomeTreatmentInd() {
		return this.homeTreatmentInd;
	}

	public void setHomeTreatmentInd(String homeTreatmentInd) {
		this.homeTreatmentInd = homeTreatmentInd;
	}

	public String getHomeTreatmentPeople() {
		return this.homeTreatmentPeople;
	}

	public void setHomeTreatmentPeople(String homeTreatmentPeople) {
		this.homeTreatmentPeople = homeTreatmentPeople;
	}

	public String getHomeTreatmentText() {
		return this.homeTreatmentText;
	}

	public void setHomeTreatmentText(String homeTreatmentText) {
		this.homeTreatmentText = homeTreatmentText;
	}

	public String getHospitalFearInd() {
		return this.hospitalFearInd;
	}

	public void setHospitalFearInd(String hospitalFearInd) {
		this.hospitalFearInd = hospitalFearInd;
	}

	public String getHouseCondition() {
		return this.houseCondition;
	}

	public void setHouseCondition(String houseCondition) {
		this.houseCondition = houseCondition;
	}

	public String getInfoToFamily() {
		return this.infoToFamily;
	}

	public void setInfoToFamily(String infoToFamily) {
		this.infoToFamily = infoToFamily;
	}

	public String getInhalation() {
		return this.inhalation;
	}

	public void setInhalation(String inhalation) {
		this.inhalation = inhalation;
	}

	public String getInhalationIsRegular() {
		return this.inhalationIsRegular;
	}

	public void setInhalationIsRegular(String inhalationIsRegular) {
		this.inhalationIsRegular = inhalationIsRegular;
	}

	public String getIntegumentary() {
		return this.integumentary;
	}

	public void setIntegumentary(String integumentary) {
		this.integumentary = integumentary;
	}

	public String getIntegumentaryInjury() {
		return this.integumentaryInjury;
	}

	public void setIntegumentaryInjury(String integumentaryInjury) {
		this.integumentaryInjury = integumentaryInjury;
	}

	public String getIntegumentaryScore() {
		return this.integumentaryScore;
	}

	public void setIntegumentaryScore(String integumentaryScore) {
		this.integumentaryScore = integumentaryScore;
	}

	public String getIntegumentaryStage() {
		return this.integumentaryStage;
	}

	public void setIntegumentaryStage(String integumentaryStage) {
		this.integumentaryStage = integumentaryStage;
	}

	public String getIntegumentaryTurgor() {
		return this.integumentaryTurgor;
	}

	public void setIntegumentaryTurgor(String integumentaryTurgor) {
		this.integumentaryTurgor = integumentaryTurgor;
	}

	public String getIssueListNursing() {
		return this.issueListNursing;
	}

	public void setIssueListNursing(String issueListNursing) {
		this.issueListNursing = issueListNursing;
	}

	public String getIssueListOther() {
		return this.issueListOther;
	}

	public void setIssueListOther(String issueListOther) {
		this.issueListOther = issueListOther;
	}

	public String getLastCervixCheck() {
		return this.lastCervixCheck;
	}

	public void setLastCervixCheck(String lastCervixCheck) {
		this.lastCervixCheck = lastCervixCheck;
	}

	public Date getLastMenstruationDate() {
		return this.lastMenstruationDate;
	}

	public void setLastMenstruationDate(Date lastMenstruationDate) {
		this.lastMenstruationDate = lastMenstruationDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getLearnMethods() {
		return this.learnMethods;
	}

	public void setLearnMethods(String learnMethods) {
		this.learnMethods = learnMethods;
	}

	public String getLearnMethodsDesc() {
		return this.learnMethodsDesc;
	}

	public void setLearnMethodsDesc(String learnMethodsDesc) {
		this.learnMethodsDesc = learnMethodsDesc;
	}

	public String getLearnObstacles() {
		return this.learnObstacles;
	}

	public void setLearnObstacles(String learnObstacles) {
		this.learnObstacles = learnObstacles;
	}

	public String getLearnObstaclesDesc() {
		return this.learnObstaclesDesc;
	}

	public void setLearnObstaclesDesc(String learnObstaclesDesc) {
		this.learnObstaclesDesc = learnObstaclesDesc;
	}

	public String getLearnPotential() {
		return this.learnPotential;
	}

	public void setLearnPotential(String learnPotential) {
		this.learnPotential = learnPotential;
	}

	public String getLearnPotentialDesc() {
		return this.learnPotentialDesc;
	}

	public void setLearnPotentialDesc(String learnPotentialDesc) {
		this.learnPotentialDesc = learnPotentialDesc;
	}

	public String getLeftLightReaction() {
		return this.leftLightReaction;
	}

	public void setLeftLightReaction(String leftLightReaction) {
		this.leftLightReaction = leftLightReaction;
	}

	public String getLeftPupil() {
		return this.leftPupil;
	}

	public void setLeftPupil(String leftPupil) {
		this.leftPupil = leftPupil;
	}

	public String getLivingWith() {
		return this.livingWith;
	}

	public void setLivingWith(String livingWith) {
		this.livingWith = livingWith;
	}

	public String getLivingWithDesc() {
		return this.livingWithDesc;
	}

	public void setLivingWithDesc(String livingWithDesc) {
		this.livingWithDesc = livingWithDesc;
	}

	public String getMainReactionAllergy() {
		return this.mainReactionAllergy;
	}

	public void setMainReactionAllergy(String mainReactionAllergy) {
		this.mainReactionAllergy = mainReactionAllergy;
	}

	public Date getMamografiLastDate() {
		return this.mamografiLastDate;
	}

	public void setMamografiLastDate(Date mamografiLastDate) {
		this.mamografiLastDate = mamografiLastDate;
	}

	public String getMedicalHistory() {
		return this.medicalHistory;
	}

	public void setMedicalHistory(String medicalHistory) {
		this.medicalHistory = medicalHistory;
	}

	public String getMedicineAllergyInd() {
		return this.medicineAllergyInd;
	}

	public void setMedicineAllergyInd(String medicineAllergyInd) {
		this.medicineAllergyInd = medicineAllergyInd;
	}

	public String getMedicineAllergyText() {
		return this.medicineAllergyText;
	}

	public void setMedicineAllergyText(String medicineAllergyText) {
		this.medicineAllergyText = medicineAllergyText;
	}

	public String getMentalStatus() {
		return this.mentalStatus;
	}

	public void setMentalStatus(String mentalStatus) {
		this.mentalStatus = mentalStatus;
	}

	public String getMentalStatusDesc() {
		return this.mentalStatusDesc;
	}

	public void setMentalStatusDesc(String mentalStatusDesc) {
		this.mentalStatusDesc = mentalStatusDesc;
	}

	public String getMouth() {
		return this.mouth;
	}

	public void setMouth(String mouth) {
		this.mouth = mouth;
	}

	public String getMouthText() {
		return this.mouthText;
	}

	public void setMouthText(String mouthText) {
		this.mouthText = mouthText;
	}

	public String getNeck() {
		return this.neck;
	}

	public void setNeck(String neck) {
		this.neck = neck;
	}

	public String getNeckText() {
		return this.neckText;
	}

	public void setNeckText(String neckText) {
		this.neckText = neckText;
	}

	public String getNeedTranslatorDesc() {
		return this.needTranslatorDesc;
	}

	public void setNeedTranslatorDesc(String needTranslatorDesc) {
		this.needTranslatorDesc = needTranslatorDesc;
	}

	public String getNeedTranslatorInd() {
		return this.needTranslatorInd;
	}

	public void setNeedTranslatorInd(String needTranslatorInd) {
		this.needTranslatorInd = needTranslatorInd;
	}

	public String getNose() {
		return this.nose;
	}

	public void setNose(String nose) {
		this.nose = nose;
	}

	public String getNoseText() {
		return this.noseText;
	}

	public void setNoseText(String noseText) {
		this.noseText = noseText;
	}

	public String getNutritionIssues() {
		return this.nutritionIssues;
	}

	public void setNutritionIssues(String nutritionIssues) {
		this.nutritionIssues = nutritionIssues;
	}

	public String getNutritionIssuesDesc() {
		return this.nutritionIssuesDesc;
	}

	public void setNutritionIssuesDesc(String nutritionIssuesDesc) {
		this.nutritionIssuesDesc = nutritionIssuesDesc;
	}

	public String getNutritionIssuesInd() {
		return this.nutritionIssuesInd;
	}

	public void setNutritionIssuesInd(String nutritionIssuesInd) {
		this.nutritionIssuesInd = nutritionIssuesInd;
	}

	public String getObesityInd() {
		return this.obesityInd;
	}

	public void setObesityInd(String obesityInd) {
		this.obesityInd = obesityInd;
	}

	public String getObesityInfo() {
		return this.obesityInfo;
	}

	public void setObesityInfo(String obesityInfo) {
		this.obesityInfo = obesityInfo;
	}

	public String getOccupation() {
		return this.occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getOccupationDesc() {
		return this.occupationDesc;
	}

	public void setOccupationDesc(String occupationDesc) {
		this.occupationDesc = occupationDesc;
	}

	public String getOthersAllergy() {
		return this.othersAllergy;
	}

	public void setOthersAllergy(String othersAllergy) {
		this.othersAllergy = othersAllergy;
	}

	public String getOthersAllergyText() {
		return this.othersAllergyText;
	}

	public void setOthersAllergyText(String othersAllergyText) {
		this.othersAllergyText = othersAllergyText;
	}

	public String getOthersLanguage() {
		return this.othersLanguage;
	}

	public void setOthersLanguage(String othersLanguage) {
		this.othersLanguage = othersLanguage;
	}

	public BigDecimal getPainDescId() {
		return this.painDescId;
	}

	public void setPainDescId(BigDecimal painDescId) {
		this.painDescId = painDescId;
	}

	public String getPainEffects() {
		return this.painEffects;
	}

	public void setPainEffects(String painEffects) {
		this.painEffects = painEffects;
	}

	public String getPainEffectsDesc() {
		return this.painEffectsDesc;
	}

	public void setPainEffectsDesc(String painEffectsDesc) {
		this.painEffectsDesc = painEffectsDesc;
	}

	public String getPainInd() {
		return this.painInd;
	}

	public void setPainInd(String painInd) {
		this.painInd = painInd;
	}

	public String getPainIntensityImg() {
		return this.painIntensityImg;
	}

	public void setPainIntensityImg(String painIntensityImg) {
		this.painIntensityImg = painIntensityImg;
	}

	public String getPainIntensityLevel() {
		return this.painIntensityLevel;
	}

	public void setPainIntensityLevel(String painIntensityLevel) {
		this.painIntensityLevel = painIntensityLevel;
	}

	public String getPregnancyInd() {
		return this.pregnancyInd;
	}

	public void setPregnancyInd(String pregnancyInd) {
		this.pregnancyInd = pregnancyInd;
	}

	public String getPrevReleaseRestraint() {
		return this.prevReleaseRestraint;
	}

	public void setPrevReleaseRestraint(String prevReleaseRestraint) {
		this.prevReleaseRestraint = prevReleaseRestraint;
	}

	public String getProstateIssuesInd() {
		return this.prostateIssuesInd;
	}

	public void setProstateIssuesInd(String prostateIssuesInd) {
		this.prostateIssuesInd = prostateIssuesInd;
	}

	public String getPulse() {
		return this.pulse;
	}

	public void setPulse(String pulse) {
		this.pulse = pulse;
	}

	public String getPulseIsRegular() {
		return this.pulseIsRegular;
	}

	public void setPulseIsRegular(String pulseIsRegular) {
		this.pulseIsRegular = pulseIsRegular;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRefrainCapabilityInd() {
		return this.refrainCapabilityInd;
	}

	public void setRefrainCapabilityInd(String refrainCapabilityInd) {
		this.refrainCapabilityInd = refrainCapabilityInd;
	}

	public String getReplacementNutrition() {
		return this.replacementNutrition;
	}

	public void setReplacementNutrition(String replacementNutrition) {
		this.replacementNutrition = replacementNutrition;
	}

	public String getReplacementNutritionNo() {
		return this.replacementNutritionNo;
	}

	public void setReplacementNutritionNo(String replacementNutritionNo) {
		this.replacementNutritionNo = replacementNutritionNo;
	}

	public String getRespiration() {
		return this.respiration;
	}

	public void setRespiration(String respiration) {
		this.respiration = respiration;
	}

	public String getRespirationText() {
		return this.respirationText;
	}

	public void setRespirationText(String respirationText) {
		this.respirationText = respirationText;
	}

	public String getRestraintDiscussionPolicy() {
		return this.restraintDiscussionPolicy;
	}

	public void setRestraintDiscussionPolicy(String restraintDiscussionPolicy) {
		this.restraintDiscussionPolicy = restraintDiscussionPolicy;
	}

	public String getRestraintIdentificationInd() {
		return this.restraintIdentificationInd;
	}

	public void setRestraintIdentificationInd(String restraintIdentificationInd) {
		this.restraintIdentificationInd = restraintIdentificationInd;
	}

	public String getRightLightReaction() {
		return this.rightLightReaction;
	}

	public void setRightLightReaction(String rightLightReaction) {
		this.rightLightReaction = rightLightReaction;
	}

	public String getRightPupil() {
		return this.rightPupil;
	}

	public void setRightPupil(String rightPupil) {
		this.rightPupil = rightPupil;
	}

	public String getSecuritySafetyInd() {
		return this.securitySafetyInd;
	}

	public void setSecuritySafetyInd(String securitySafetyInd) {
		this.securitySafetyInd = securitySafetyInd;
	}

	public String getSedativeHistoryDesc() {
		return this.sedativeHistoryDesc;
	}

	public void setSedativeHistoryDesc(String sedativeHistoryDesc) {
		this.sedativeHistoryDesc = sedativeHistoryDesc;
	}

	public String getSedativeHistoryInd() {
		return this.sedativeHistoryInd;
	}

	public void setSedativeHistoryInd(String sedativeHistoryInd) {
		this.sedativeHistoryInd = sedativeHistoryInd;
	}

	public String getSightIssue() {
		return this.sightIssue;
	}

	public void setSightIssue(String sightIssue) {
		this.sightIssue = sightIssue;
	}

	public String getSightIssueDesc() {
		return this.sightIssueDesc;
	}

	public void setSightIssueDesc(String sightIssueDesc) {
		this.sightIssueDesc = sightIssueDesc;
	}

	public String getSignLanguageInd() {
		return this.signLanguageInd;
	}

	public void setSignLanguageInd(String signLanguageInd) {
		this.signLanguageInd = signLanguageInd;
	}

	public String getSmokeDayCount() {
		return this.smokeDayCount;
	}

	public void setSmokeDayCount(String smokeDayCount) {
		this.smokeDayCount = smokeDayCount;
	}

	public String getSmokeHistory() {
		return this.smokeHistory;
	}

	public void setSmokeHistory(String smokeHistory) {
		this.smokeHistory = smokeHistory;
	}

	public String getSmokeInd() {
		return this.smokeInd;
	}

	public void setSmokeInd(String smokeInd) {
		this.smokeInd = smokeInd;
	}

	public String getSmokeType() {
		return this.smokeType;
	}

	public void setSmokeType(String smokeType) {
		this.smokeType = smokeType;
	}

	public String getTalkingDisordersDesc() {
		return this.talkingDisordersDesc;
	}

	public void setTalkingDisordersDesc(String talkingDisordersDesc) {
		this.talkingDisordersDesc = talkingDisordersDesc;
	}

	public String getTalkingStatusInd() {
		return this.talkingStatusInd;
	}

	public void setTalkingStatusInd(String talkingStatusInd) {
		this.talkingStatusInd = talkingStatusInd;
	}

	public String getTeeth() {
		return this.teeth;
	}

	public void setTeeth(String teeth) {
		this.teeth = teeth;
	}

	public String getTeethText() {
		return this.teethText;
	}

	public void setTeethText(String teethText) {
		this.teethText = teethText;
	}

	public String getTemperature() {
		return this.temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getThroat() {
		return this.throat;
	}

	public void setThroat(String throat) {
		this.throat = throat;
	}

	public String getThroatText() {
		return this.throatText;
	}

	public void setThroatText(String throatText) {
		this.throatText = throatText;
	}

	public String getTongue() {
		return this.tongue;
	}

	public void setTongue(String tongue) {
		this.tongue = tongue;
	}

	public String getTongueText() {
		return this.tongueText;
	}

	public void setTongueText(String tongueText) {
		this.tongueText = tongueText;
	}

	public String getUrinate() {
		return this.urinate;
	}

	public void setUrinate(String urinate) {
		this.urinate = urinate;
	}

	public String getUrinateInfo() {
		return this.urinateInfo;
	}

	public void setUrinateInfo(String urinateInfo) {
		this.urinateInfo = urinateInfo;
	}

	public String getUsedContraceptionInd() {
		return this.usedContraceptionInd;
	}

	public void setUsedContraceptionInd(String usedContraceptionInd) {
		this.usedContraceptionInd = usedContraceptionInd;
	}

	public String getUsedRestraintInd() {
		return this.usedRestraintInd;
	}

	public void setUsedRestraintInd(String usedRestraintInd) {
		this.usedRestraintInd = usedRestraintInd;
	}

	public String getUsedRestraintLocation() {
		return this.usedRestraintLocation;
	}

	public void setUsedRestraintLocation(String usedRestraintLocation) {
		this.usedRestraintLocation = usedRestraintLocation;
	}

	public String getUsedRestraintType() {
		return this.usedRestraintType;
	}

	public void setUsedRestraintType(String usedRestraintType) {
		this.usedRestraintType = usedRestraintType;
	}

	public String getVernacularDesc() {
		return this.vernacularDesc;
	}

	public void setVernacularDesc(String vernacularDesc) {
		this.vernacularDesc = vernacularDesc;
	}

	public BigDecimal getVisitId() {
		return this.visitId;
	}

	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}

	public String getWalkingCapability() {
		return this.walkingCapability;
	}

	public void setWalkingCapability(String walkingCapability) {
		this.walkingCapability = walkingCapability;
	}

	public String getWalkingCapabilityDesc() {
		return this.walkingCapabilityDesc;
	}

	public void setWalkingCapabilityDesc(String walkingCapabilityDesc) {
		this.walkingCapabilityDesc = walkingCapabilityDesc;
	}

	public String getWalkingEscapeInd() {
		return this.walkingEscapeInd;
	}

	public void setWalkingEscapeInd(String walkingEscapeInd) {
		this.walkingEscapeInd = walkingEscapeInd;
	}

	public String getWeight() {
		return this.weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getWeightLossInd() {
		return this.weightLossInd;
	}

	public void setWeightLossInd(String weightLossInd) {
		this.weightLossInd = weightLossInd;
	}

	public String getWorkHistory() {
		return this.workHistory;
	}

	public void setWorkHistory(String workHistory) {
		this.workHistory = workHistory;
	}

}