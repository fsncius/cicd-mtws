package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the WARDMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Wardmstr.findAll", query="SELECT w FROM Wardmstr w")
public class Wardmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WARDMSTR_ID")
	private long wardmstrId;

	@Column(name="CONTACT_NO")
	private String contactNo;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="INFANCY_IND")
	private String infancyInd;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MAX_VISITOR")
	private BigDecimal maxVisitor;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="RATED_BED_QTY")
	private BigDecimal ratedBedQty;

	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenuecentremstrId;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	@Column(name="SHORT_CODE")
	private String shortCode;

	@Column(name="WARD_CODE")
	private String wardCode;

	@Column(name="WARD_DESC")
	private String wardDesc;

	@Column(name="WARD_DESC_LANG1")
	private String wardDescLang1;

	@Column(name="WARD_DESC_LANG2")
	private String wardDescLang2;

	@Column(name="WARD_DESC_LANG3")
	private String wardDescLang3;

	@Column(name="WARD_TYPE")
	private String wardType;

	//bi-directional many-to-one association to Adtdailysummary
	@OneToMany(mappedBy="wardmstr")
	private List<Adtdailysummary> adtdailysummaries;

	//bi-directional many-to-one association to Bedhistory
	@OneToMany(mappedBy="wardmstr")
	private List<Bedhistory> bedhistories;

	//bi-directional many-to-one association to Careprovider
	@OneToMany(mappedBy="wardmstr")
	private List<Careprovider> careproviders;

	//bi-directional many-to-one association to Charge
	@OneToMany(mappedBy="wardmstr")
	private List<Charge> charges;

	//bi-directional many-to-one association to Chargematerialcomp
	@OneToMany(mappedBy="wardmstr")
	private List<Chargematerialcomp> chargematerialcomps;

	//bi-directional many-to-one association to Chargerollingplan
	@OneToMany(mappedBy="wardmstr")
	private List<Chargerollingplan> chargerollingplans;

	//bi-directional many-to-one association to Countedpatdailysummary
	@OneToMany(mappedBy="wardmstr")
	private List<Countedpatdailysummary> countedpatdailysummaries;

	//bi-directional many-to-one association to Diagnosisincommonusemstr
	@OneToMany(mappedBy="wardmstr")
	private List<Diagnosisincommonusemstr> diagnosisincommonusemstrs;

	//bi-directional many-to-one association to Roommstr
	@OneToMany(mappedBy="wardmstr")
	private List<Roommstr> roommstrs;

	//bi-directional many-to-one association to Subspecialtywardrequest
	@OneToMany(mappedBy="wardmstr1")
	private List<Subspecialtywardrequest> subspecialtywardrequests1;

	//bi-directional many-to-one association to Subspecialtywardrequest
	@OneToMany(mappedBy="wardmstr2")
	private List<Subspecialtywardrequest> subspecialtywardrequests2;

	//bi-directional many-to-one association to Visit
	@OneToMany(mappedBy="wardmstr")
	private List<Visit> visits;

	//bi-directional many-to-one association to Visitappointment
	@OneToMany(mappedBy="wardmstr")
	private List<Visitappointment> visitappointments;

	//bi-directional many-to-one association to Visitdailysummarydetail
	@OneToMany(mappedBy="wardmstr")
	private List<Visitdailysummarydetail> visitdailysummarydetails;

	//bi-directional many-to-one association to Visitdoctor
	@OneToMany(mappedBy="wardmstr")
	private List<Visitdoctor> visitdoctors;

	//bi-directional many-to-one association to Visitwardhistory
	@OneToMany(mappedBy="wardmstr")
	private List<Visitwardhistory> visitwardhistories;

	//bi-directional many-to-one association to Locationmstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOCATIONMSTR_ID")
	private Locationmstr locationmstr;

	//bi-directional many-to-one association to Wardsubspecialty
	@OneToMany(mappedBy="wardmstr")
	private List<Wardsubspecialty> wardsubspecialties;

	public Wardmstr() {
	}

	public long getWardmstrId() {
		return this.wardmstrId;
	}

	public void setWardmstrId(long wardmstrId) {
		this.wardmstrId = wardmstrId;
	}

	public String getContactNo() {
		return this.contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getInfancyInd() {
		return this.infancyInd;
	}

	public void setInfancyInd(String infancyInd) {
		this.infancyInd = infancyInd;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaxVisitor() {
		return this.maxVisitor;
	}

	public void setMaxVisitor(BigDecimal maxVisitor) {
		this.maxVisitor = maxVisitor;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public BigDecimal getRatedBedQty() {
		return this.ratedBedQty;
	}

	public void setRatedBedQty(BigDecimal ratedBedQty) {
		this.ratedBedQty = ratedBedQty;
	}

	public BigDecimal getRevenuecentremstrId() {
		return this.revenuecentremstrId;
	}

	public void setRevenuecentremstrId(BigDecimal revenuecentremstrId) {
		this.revenuecentremstrId = revenuecentremstrId;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getWardCode() {
		return this.wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getWardDesc() {
		return this.wardDesc;
	}

	public void setWardDesc(String wardDesc) {
		this.wardDesc = wardDesc;
	}

	public String getWardDescLang1() {
		return this.wardDescLang1;
	}

	public void setWardDescLang1(String wardDescLang1) {
		this.wardDescLang1 = wardDescLang1;
	}

	public String getWardDescLang2() {
		return this.wardDescLang2;
	}

	public void setWardDescLang2(String wardDescLang2) {
		this.wardDescLang2 = wardDescLang2;
	}

	public String getWardDescLang3() {
		return this.wardDescLang3;
	}

	public void setWardDescLang3(String wardDescLang3) {
		this.wardDescLang3 = wardDescLang3;
	}

	public String getWardType() {
		return this.wardType;
	}

	public void setWardType(String wardType) {
		this.wardType = wardType;
	}

	public List<Adtdailysummary> getAdtdailysummaries() {
		return this.adtdailysummaries;
	}

	public void setAdtdailysummaries(List<Adtdailysummary> adtdailysummaries) {
		this.adtdailysummaries = adtdailysummaries;
	}

	public Adtdailysummary addAdtdailysummary(Adtdailysummary adtdailysummary) {
		getAdtdailysummaries().add(adtdailysummary);
		adtdailysummary.setWardmstr(this);

		return adtdailysummary;
	}

	public Adtdailysummary removeAdtdailysummary(Adtdailysummary adtdailysummary) {
		getAdtdailysummaries().remove(adtdailysummary);
		adtdailysummary.setWardmstr(null);

		return adtdailysummary;
	}

	public List<Bedhistory> getBedhistories() {
		return this.bedhistories;
	}

	public void setBedhistories(List<Bedhistory> bedhistories) {
		this.bedhistories = bedhistories;
	}

	public Bedhistory addBedhistory(Bedhistory bedhistory) {
		getBedhistories().add(bedhistory);
		bedhistory.setWardmstr(this);

		return bedhistory;
	}

	public Bedhistory removeBedhistory(Bedhistory bedhistory) {
		getBedhistories().remove(bedhistory);
		bedhistory.setWardmstr(null);

		return bedhistory;
	}

	public List<Careprovider> getCareproviders() {
		return this.careproviders;
	}

	public void setCareproviders(List<Careprovider> careproviders) {
		this.careproviders = careproviders;
	}

	public Careprovider addCareprovider(Careprovider careprovider) {
		getCareproviders().add(careprovider);
		careprovider.setWardmstr(this);

		return careprovider;
	}

	public Careprovider removeCareprovider(Careprovider careprovider) {
		getCareproviders().remove(careprovider);
		careprovider.setWardmstr(null);

		return careprovider;
	}

	public List<Charge> getCharges() {
		return this.charges;
	}

	public void setCharges(List<Charge> charges) {
		this.charges = charges;
	}

	public Charge addCharge(Charge charge) {
		getCharges().add(charge);
		charge.setWardmstr(this);

		return charge;
	}

	public Charge removeCharge(Charge charge) {
		getCharges().remove(charge);
		charge.setWardmstr(null);

		return charge;
	}

	public List<Chargematerialcomp> getChargematerialcomps() {
		return this.chargematerialcomps;
	}

	public void setChargematerialcomps(List<Chargematerialcomp> chargematerialcomps) {
		this.chargematerialcomps = chargematerialcomps;
	}

	public Chargematerialcomp addChargematerialcomp(Chargematerialcomp chargematerialcomp) {
		getChargematerialcomps().add(chargematerialcomp);
		chargematerialcomp.setWardmstr(this);

		return chargematerialcomp;
	}

	public Chargematerialcomp removeChargematerialcomp(Chargematerialcomp chargematerialcomp) {
		getChargematerialcomps().remove(chargematerialcomp);
		chargematerialcomp.setWardmstr(null);

		return chargematerialcomp;
	}

	public List<Chargerollingplan> getChargerollingplans() {
		return this.chargerollingplans;
	}

	public void setChargerollingplans(List<Chargerollingplan> chargerollingplans) {
		this.chargerollingplans = chargerollingplans;
	}

	public Chargerollingplan addChargerollingplan(Chargerollingplan chargerollingplan) {
		getChargerollingplans().add(chargerollingplan);
		chargerollingplan.setWardmstr(this);

		return chargerollingplan;
	}

	public Chargerollingplan removeChargerollingplan(Chargerollingplan chargerollingplan) {
		getChargerollingplans().remove(chargerollingplan);
		chargerollingplan.setWardmstr(null);

		return chargerollingplan;
	}

	public List<Countedpatdailysummary> getCountedpatdailysummaries() {
		return this.countedpatdailysummaries;
	}

	public void setCountedpatdailysummaries(List<Countedpatdailysummary> countedpatdailysummaries) {
		this.countedpatdailysummaries = countedpatdailysummaries;
	}

	public Countedpatdailysummary addCountedpatdailysummary(Countedpatdailysummary countedpatdailysummary) {
		getCountedpatdailysummaries().add(countedpatdailysummary);
		countedpatdailysummary.setWardmstr(this);

		return countedpatdailysummary;
	}

	public Countedpatdailysummary removeCountedpatdailysummary(Countedpatdailysummary countedpatdailysummary) {
		getCountedpatdailysummaries().remove(countedpatdailysummary);
		countedpatdailysummary.setWardmstr(null);

		return countedpatdailysummary;
	}

	public List<Diagnosisincommonusemstr> getDiagnosisincommonusemstrs() {
		return this.diagnosisincommonusemstrs;
	}

	public void setDiagnosisincommonusemstrs(List<Diagnosisincommonusemstr> diagnosisincommonusemstrs) {
		this.diagnosisincommonusemstrs = diagnosisincommonusemstrs;
	}

	public Diagnosisincommonusemstr addDiagnosisincommonusemstr(Diagnosisincommonusemstr diagnosisincommonusemstr) {
		getDiagnosisincommonusemstrs().add(diagnosisincommonusemstr);
		diagnosisincommonusemstr.setWardmstr(this);

		return diagnosisincommonusemstr;
	}

	public Diagnosisincommonusemstr removeDiagnosisincommonusemstr(Diagnosisincommonusemstr diagnosisincommonusemstr) {
		getDiagnosisincommonusemstrs().remove(diagnosisincommonusemstr);
		diagnosisincommonusemstr.setWardmstr(null);

		return diagnosisincommonusemstr;
	}


	public List<Roommstr> getRoommstrs() {
		return this.roommstrs;
	}

	public void setRoommstrs(List<Roommstr> roommstrs) {
		this.roommstrs = roommstrs;
	}

	public Roommstr addRoommstr(Roommstr roommstr) {
		getRoommstrs().add(roommstr);
		roommstr.setWardmstr(this);

		return roommstr;
	}

	public Roommstr removeRoommstr(Roommstr roommstr) {
		getRoommstrs().remove(roommstr);
		roommstr.setWardmstr(null);

		return roommstr;
	}

	public List<Subspecialtywardrequest> getSubspecialtywardrequests1() {
		return this.subspecialtywardrequests1;
	}

	public void setSubspecialtywardrequests1(List<Subspecialtywardrequest> subspecialtywardrequests1) {
		this.subspecialtywardrequests1 = subspecialtywardrequests1;
	}

	public Subspecialtywardrequest addSubspecialtywardrequests1(Subspecialtywardrequest subspecialtywardrequests1) {
		getSubspecialtywardrequests1().add(subspecialtywardrequests1);
		subspecialtywardrequests1.setWardmstr1(this);

		return subspecialtywardrequests1;
	}

	public Subspecialtywardrequest removeSubspecialtywardrequests1(Subspecialtywardrequest subspecialtywardrequests1) {
		getSubspecialtywardrequests1().remove(subspecialtywardrequests1);
		subspecialtywardrequests1.setWardmstr1(null);

		return subspecialtywardrequests1;
	}

	public List<Subspecialtywardrequest> getSubspecialtywardrequests2() {
		return this.subspecialtywardrequests2;
	}

	public void setSubspecialtywardrequests2(List<Subspecialtywardrequest> subspecialtywardrequests2) {
		this.subspecialtywardrequests2 = subspecialtywardrequests2;
	}

	public Subspecialtywardrequest addSubspecialtywardrequests2(Subspecialtywardrequest subspecialtywardrequests2) {
		getSubspecialtywardrequests2().add(subspecialtywardrequests2);
		subspecialtywardrequests2.setWardmstr2(this);

		return subspecialtywardrequests2;
	}

	public Subspecialtywardrequest removeSubspecialtywardrequests2(Subspecialtywardrequest subspecialtywardrequests2) {
		getSubspecialtywardrequests2().remove(subspecialtywardrequests2);
		subspecialtywardrequests2.setWardmstr2(null);

		return subspecialtywardrequests2;
	}

	public List<Visit> getVisits() {
		return this.visits;
	}

	public void setVisits(List<Visit> visits) {
		this.visits = visits;
	}

	public Visit addVisit(Visit visit) {
		getVisits().add(visit);
		visit.setWardmstr(this);

		return visit;
	}

	public Visit removeVisit(Visit visit) {
		getVisits().remove(visit);
		visit.setWardmstr(null);

		return visit;
	}

	public List<Visitappointment> getVisitappointments() {
		return this.visitappointments;
	}

	public void setVisitappointments(List<Visitappointment> visitappointments) {
		this.visitappointments = visitappointments;
	}

	public Visitappointment addVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().add(visitappointment);
		visitappointment.setWardmstr(this);

		return visitappointment;
	}

	public Visitappointment removeVisitappointment(Visitappointment visitappointment) {
		getVisitappointments().remove(visitappointment);
		visitappointment.setWardmstr(null);

		return visitappointment;
	}

	public List<Visitdailysummarydetail> getVisitdailysummarydetails() {
		return this.visitdailysummarydetails;
	}

	public void setVisitdailysummarydetails(List<Visitdailysummarydetail> visitdailysummarydetails) {
		this.visitdailysummarydetails = visitdailysummarydetails;
	}

	public Visitdailysummarydetail addVisitdailysummarydetail(Visitdailysummarydetail visitdailysummarydetail) {
		getVisitdailysummarydetails().add(visitdailysummarydetail);
		visitdailysummarydetail.setWardmstr(this);

		return visitdailysummarydetail;
	}

	public Visitdailysummarydetail removeVisitdailysummarydetail(Visitdailysummarydetail visitdailysummarydetail) {
		getVisitdailysummarydetails().remove(visitdailysummarydetail);
		visitdailysummarydetail.setWardmstr(null);

		return visitdailysummarydetail;
	}

	public List<Visitdoctor> getVisitdoctors() {
		return this.visitdoctors;
	}

	public void setVisitdoctors(List<Visitdoctor> visitdoctors) {
		this.visitdoctors = visitdoctors;
	}

	public Visitdoctor addVisitdoctor(Visitdoctor visitdoctor) {
		getVisitdoctors().add(visitdoctor);
		visitdoctor.setWardmstr(this);

		return visitdoctor;
	}

	public Visitdoctor removeVisitdoctor(Visitdoctor visitdoctor) {
		getVisitdoctors().remove(visitdoctor);
		visitdoctor.setWardmstr(null);

		return visitdoctor;
	}

	public List<Visitwardhistory> getVisitwardhistories() {
		return this.visitwardhistories;
	}

	public void setVisitwardhistories(List<Visitwardhistory> visitwardhistories) {
		this.visitwardhistories = visitwardhistories;
	}

	public Visitwardhistory addVisitwardhistory(Visitwardhistory visitwardhistory) {
		getVisitwardhistories().add(visitwardhistory);
		visitwardhistory.setWardmstr(this);

		return visitwardhistory;
	}

	public Visitwardhistory removeVisitwardhistory(Visitwardhistory visitwardhistory) {
		getVisitwardhistories().remove(visitwardhistory);
		visitwardhistory.setWardmstr(null);

		return visitwardhistory;
	}

	public Locationmstr getLocationmstr() {
		return this.locationmstr;
	}

	public void setLocationmstr(Locationmstr locationmstr) {
		this.locationmstr = locationmstr;
	}

	public List<Wardsubspecialty> getWardsubspecialties() {
		return this.wardsubspecialties;
	}

	public void setWardsubspecialties(List<Wardsubspecialty> wardsubspecialties) {
		this.wardsubspecialties = wardsubspecialties;
	}

	public Wardsubspecialty addWardsubspecialty(Wardsubspecialty wardsubspecialty) {
		getWardsubspecialties().add(wardsubspecialty);
		wardsubspecialty.setWardmstr(this);

		return wardsubspecialty;
	}

	public Wardsubspecialty removeWardsubspecialty(Wardsubspecialty wardsubspecialty) {
		getWardsubspecialties().remove(wardsubspecialty);
		wardsubspecialty.setWardmstr(null);

		return wardsubspecialty;
	}

}