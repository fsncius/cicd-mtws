package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the DRUGDISPENSE database table.
 * 
 */
@Entity
@NamedQuery(name="Drugdispense.findAll", query="SELECT d FROM Drugdispense d")
public class Drugdispense implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DRUGDISPENSE_ID")
	private long drugdispenseId;

	@Column(name="ADDITIONAL_INSTRUCTIONS")
	private String additionalInstructions;

	@Column(name="AUTHORISED_BY")
	private BigDecimal authorisedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="AUTHORISED_DATETIME")
	private Date authorisedDatetime;

	@Column(name="CONSULTATION_IND")
	private String consultationInd;

	@Column(name="DISPENSE_BATCH_NO")
	private String dispenseBatchNo;

	@Column(name="DISPENSE_DURATION_IN_DAYS")
	private BigDecimal dispenseDurationInDays;

	@Column(name="DISPENSE_OPL_ID")
	private BigDecimal dispenseOplId;

	@Column(name="DISPENSE_QTY")
	private BigDecimal dispenseQty;

	@Column(name="DISPENSE_QTY_OF_BASE_UOM")
	private BigDecimal dispenseQtyOfBaseUom;

	@Column(name="DISPENSE_STATUS")
	private String dispenseStatus;

	@Column(name="DISPENSE_UOM")
	private String dispenseUom;

	@Column(name="DISPENSED_BY")
	private BigDecimal dispensedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="DISPENSED_DATETIME")
	private Date dispensedDatetime;

	@Column(name="DOSAGE_INSTRUCTIONS")
	private String dosageInstructions;

	@Column(name="DRUG_DISPENSING_MODE")
	private String drugDispensingMode;

	@Column(name="DRUG_LABEL")
	private String drugLabel;

	@Column(name="DRUG_LABEL_OVERRIDE_IND")
	private String drugLabelOverrideInd;

	@Column(name="DRUG_USAGE")
	private String drugUsage;

	@Column(name="ERROR_REMARKS")
	private String errorRemarks;

	@Column(name="ERROR_TYPE")
	private String errorType;

	@Column(name="IPDRUGDISPENSEFORMMSTR_ID")
	private BigDecimal ipdrugdispenseformmstrId;

	@Column(name="LABEL_UOM")
	private String labelUom;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="MATERIALITEMMSTR_ID")
	private BigDecimal materialitemmstrId;

	@Column(name="MATERIALTXN_ID")
	private BigDecimal materialtxnId;

	@Column(name="NO_OF_DRUG_LABELS")
	private BigDecimal noOfDrugLabels;

	@Column(name="ORDERCONFIRMATION_ID")
	private BigDecimal orderconfirmationId;

	@Temporal(TemporalType.DATE)
	@Column(name="PATIENT_ARRIVING_DATETIME")
	private Date patientArrivingDatetime;

	@Column(name="PATIENTACCOUNTTXN_ID")
	private BigDecimal patientaccounttxnId;

	@Column(name="PATIENTDRUGMANUFACTURE_ID")
	private BigDecimal patientdrugmanufactureId;

	@Column(name="PREPARATION_BATCH_NO")
	private String preparationBatchNo;

	@Temporal(TemporalType.DATE)
	@Column(name="PREPARATION_DATETIME")
	private Date preparationDatetime;

	@Column(name="PREPARE_BATCH_NO")
	private String prepareBatchNo;

	@Column(name="PREPARE_OPL_ID")
	private BigDecimal prepareOplId;

	@Column(name="PREPARED_BY")
	private BigDecimal preparedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREPARED_DATETIME")
	private Date preparedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String printer;

	@Temporal(TemporalType.DATE)
	@Column(name="READY_TO_COLLECTION_DATETIME")
	private Date readyToCollectionDatetime;

	@Column(name="STANDARD_UNIT_PRICE")
	private BigDecimal standardUnitPrice;

	@Column(name="STANDARD_UNIT_PRICE_UOM")
	private String standardUnitPriceUom;

	@Column(name="STOREDISPENSEWINDOWMSTR_ID")
	private BigDecimal storedispensewindowmstrId;

	@Column(name="STOREMSTR_ID")
	private BigDecimal storemstrId;

	@Column(name="TAG_PRINT_NUMBER")
	private BigDecimal tagPrintNumber;

	@Column(name="TOTAL_RETURN_QTY_OF_BASE_UOM")
	private BigDecimal totalReturnQtyOfBaseUom;

	@Column(name="VISIT_ID")
	private BigDecimal visitId;

	@Column(name="VISIT_SUBSPECIALTYMSTR_ID")
	private BigDecimal visitSubspecialtymstrId;

	@Column(name="VISIT_WARDMSTR_ID")
	private BigDecimal visitWardmstrId;

	//bi-directional many-to-one association to Drugreturn
	@OneToMany(mappedBy="drugdispense")
	private List<Drugreturn> drugreturns;

	public Drugdispense() {
	}

	public long getDrugdispenseId() {
		return this.drugdispenseId;
	}

	public void setDrugdispenseId(long drugdispenseId) {
		this.drugdispenseId = drugdispenseId;
	}

	public String getAdditionalInstructions() {
		return this.additionalInstructions;
	}

	public void setAdditionalInstructions(String additionalInstructions) {
		this.additionalInstructions = additionalInstructions;
	}

	public BigDecimal getAuthorisedBy() {
		return this.authorisedBy;
	}

	public void setAuthorisedBy(BigDecimal authorisedBy) {
		this.authorisedBy = authorisedBy;
	}

	public Date getAuthorisedDatetime() {
		return this.authorisedDatetime;
	}

	public void setAuthorisedDatetime(Date authorisedDatetime) {
		this.authorisedDatetime = authorisedDatetime;
	}

	public String getConsultationInd() {
		return this.consultationInd;
	}

	public void setConsultationInd(String consultationInd) {
		this.consultationInd = consultationInd;
	}

	public String getDispenseBatchNo() {
		return this.dispenseBatchNo;
	}

	public void setDispenseBatchNo(String dispenseBatchNo) {
		this.dispenseBatchNo = dispenseBatchNo;
	}

	public BigDecimal getDispenseDurationInDays() {
		return this.dispenseDurationInDays;
	}

	public void setDispenseDurationInDays(BigDecimal dispenseDurationInDays) {
		this.dispenseDurationInDays = dispenseDurationInDays;
	}

	public BigDecimal getDispenseOplId() {
		return this.dispenseOplId;
	}

	public void setDispenseOplId(BigDecimal dispenseOplId) {
		this.dispenseOplId = dispenseOplId;
	}

	public BigDecimal getDispenseQty() {
		return this.dispenseQty;
	}

	public void setDispenseQty(BigDecimal dispenseQty) {
		this.dispenseQty = dispenseQty;
	}

	public BigDecimal getDispenseQtyOfBaseUom() {
		return this.dispenseQtyOfBaseUom;
	}

	public void setDispenseQtyOfBaseUom(BigDecimal dispenseQtyOfBaseUom) {
		this.dispenseQtyOfBaseUom = dispenseQtyOfBaseUom;
	}

	public String getDispenseStatus() {
		return this.dispenseStatus;
	}

	public void setDispenseStatus(String dispenseStatus) {
		this.dispenseStatus = dispenseStatus;
	}

	public String getDispenseUom() {
		return this.dispenseUom;
	}

	public void setDispenseUom(String dispenseUom) {
		this.dispenseUom = dispenseUom;
	}

	public BigDecimal getDispensedBy() {
		return this.dispensedBy;
	}

	public void setDispensedBy(BigDecimal dispensedBy) {
		this.dispensedBy = dispensedBy;
	}

	public Date getDispensedDatetime() {
		return this.dispensedDatetime;
	}

	public void setDispensedDatetime(Date dispensedDatetime) {
		this.dispensedDatetime = dispensedDatetime;
	}

	public String getDosageInstructions() {
		return this.dosageInstructions;
	}

	public void setDosageInstructions(String dosageInstructions) {
		this.dosageInstructions = dosageInstructions;
	}

	public String getDrugDispensingMode() {
		return this.drugDispensingMode;
	}

	public void setDrugDispensingMode(String drugDispensingMode) {
		this.drugDispensingMode = drugDispensingMode;
	}

	public String getDrugLabel() {
		return this.drugLabel;
	}

	public void setDrugLabel(String drugLabel) {
		this.drugLabel = drugLabel;
	}

	public String getDrugLabelOverrideInd() {
		return this.drugLabelOverrideInd;
	}

	public void setDrugLabelOverrideInd(String drugLabelOverrideInd) {
		this.drugLabelOverrideInd = drugLabelOverrideInd;
	}

	public String getDrugUsage() {
		return this.drugUsage;
	}

	public void setDrugUsage(String drugUsage) {
		this.drugUsage = drugUsage;
	}

	public String getErrorRemarks() {
		return this.errorRemarks;
	}

	public void setErrorRemarks(String errorRemarks) {
		this.errorRemarks = errorRemarks;
	}

	public String getErrorType() {
		return this.errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public BigDecimal getIpdrugdispenseformmstrId() {
		return this.ipdrugdispenseformmstrId;
	}

	public void setIpdrugdispenseformmstrId(BigDecimal ipdrugdispenseformmstrId) {
		this.ipdrugdispenseformmstrId = ipdrugdispenseformmstrId;
	}

	public String getLabelUom() {
		return this.labelUom;
	}

	public void setLabelUom(String labelUom) {
		this.labelUom = labelUom;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getMaterialitemmstrId() {
		return this.materialitemmstrId;
	}

	public void setMaterialitemmstrId(BigDecimal materialitemmstrId) {
		this.materialitemmstrId = materialitemmstrId;
	}

	public BigDecimal getMaterialtxnId() {
		return this.materialtxnId;
	}

	public void setMaterialtxnId(BigDecimal materialtxnId) {
		this.materialtxnId = materialtxnId;
	}

	public BigDecimal getNoOfDrugLabels() {
		return this.noOfDrugLabels;
	}

	public void setNoOfDrugLabels(BigDecimal noOfDrugLabels) {
		this.noOfDrugLabels = noOfDrugLabels;
	}

	public BigDecimal getOrderconfirmationId() {
		return this.orderconfirmationId;
	}

	public void setOrderconfirmationId(BigDecimal orderconfirmationId) {
		this.orderconfirmationId = orderconfirmationId;
	}

	public Date getPatientArrivingDatetime() {
		return this.patientArrivingDatetime;
	}

	public void setPatientArrivingDatetime(Date patientArrivingDatetime) {
		this.patientArrivingDatetime = patientArrivingDatetime;
	}

	public BigDecimal getPatientaccounttxnId() {
		return this.patientaccounttxnId;
	}

	public void setPatientaccounttxnId(BigDecimal patientaccounttxnId) {
		this.patientaccounttxnId = patientaccounttxnId;
	}

	public BigDecimal getPatientdrugmanufactureId() {
		return this.patientdrugmanufactureId;
	}

	public void setPatientdrugmanufactureId(BigDecimal patientdrugmanufactureId) {
		this.patientdrugmanufactureId = patientdrugmanufactureId;
	}

	public String getPreparationBatchNo() {
		return this.preparationBatchNo;
	}

	public void setPreparationBatchNo(String preparationBatchNo) {
		this.preparationBatchNo = preparationBatchNo;
	}

	public Date getPreparationDatetime() {
		return this.preparationDatetime;
	}

	public void setPreparationDatetime(Date preparationDatetime) {
		this.preparationDatetime = preparationDatetime;
	}

	public String getPrepareBatchNo() {
		return this.prepareBatchNo;
	}

	public void setPrepareBatchNo(String prepareBatchNo) {
		this.prepareBatchNo = prepareBatchNo;
	}

	public BigDecimal getPrepareOplId() {
		return this.prepareOplId;
	}

	public void setPrepareOplId(BigDecimal prepareOplId) {
		this.prepareOplId = prepareOplId;
	}

	public BigDecimal getPreparedBy() {
		return this.preparedBy;
	}

	public void setPreparedBy(BigDecimal preparedBy) {
		this.preparedBy = preparedBy;
	}

	public Date getPreparedDatetime() {
		return this.preparedDatetime;
	}

	public void setPreparedDatetime(Date preparedDatetime) {
		this.preparedDatetime = preparedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPrinter() {
		return this.printer;
	}

	public void setPrinter(String printer) {
		this.printer = printer;
	}

	public Date getReadyToCollectionDatetime() {
		return this.readyToCollectionDatetime;
	}

	public void setReadyToCollectionDatetime(Date readyToCollectionDatetime) {
		this.readyToCollectionDatetime = readyToCollectionDatetime;
	}

	public BigDecimal getStandardUnitPrice() {
		return this.standardUnitPrice;
	}

	public void setStandardUnitPrice(BigDecimal standardUnitPrice) {
		this.standardUnitPrice = standardUnitPrice;
	}

	public String getStandardUnitPriceUom() {
		return this.standardUnitPriceUom;
	}

	public void setStandardUnitPriceUom(String standardUnitPriceUom) {
		this.standardUnitPriceUom = standardUnitPriceUom;
	}

	public BigDecimal getStoredispensewindowmstrId() {
		return this.storedispensewindowmstrId;
	}

	public void setStoredispensewindowmstrId(BigDecimal storedispensewindowmstrId) {
		this.storedispensewindowmstrId = storedispensewindowmstrId;
	}

	public BigDecimal getStoremstrId() {
		return this.storemstrId;
	}

	public void setStoremstrId(BigDecimal storemstrId) {
		this.storemstrId = storemstrId;
	}

	public BigDecimal getTagPrintNumber() {
		return this.tagPrintNumber;
	}

	public void setTagPrintNumber(BigDecimal tagPrintNumber) {
		this.tagPrintNumber = tagPrintNumber;
	}

	public BigDecimal getTotalReturnQtyOfBaseUom() {
		return this.totalReturnQtyOfBaseUom;
	}

	public void setTotalReturnQtyOfBaseUom(BigDecimal totalReturnQtyOfBaseUom) {
		this.totalReturnQtyOfBaseUom = totalReturnQtyOfBaseUom;
	}

	public BigDecimal getVisitId() {
		return this.visitId;
	}

	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}

	public BigDecimal getVisitSubspecialtymstrId() {
		return this.visitSubspecialtymstrId;
	}

	public void setVisitSubspecialtymstrId(BigDecimal visitSubspecialtymstrId) {
		this.visitSubspecialtymstrId = visitSubspecialtymstrId;
	}

	public BigDecimal getVisitWardmstrId() {
		return this.visitWardmstrId;
	}

	public void setVisitWardmstrId(BigDecimal visitWardmstrId) {
		this.visitWardmstrId = visitWardmstrId;
	}

	public List<Drugreturn> getDrugreturns() {
		return this.drugreturns;
	}

	public void setDrugreturns(List<Drugreturn> drugreturns) {
		this.drugreturns = drugreturns;
	}

	public Drugreturn addDrugreturn(Drugreturn drugreturn) {
		getDrugreturns().add(drugreturn);
		drugreturn.setDrugdispense(this);

		return drugreturn;
	}

	public Drugreturn removeDrugreturn(Drugreturn drugreturn) {
		getDrugreturns().remove(drugreturn);
		drugreturn.setDrugdispense(null);

		return drugreturn;
	}

}