package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the WINDOWOUTPUT database table.
 * 
 */
@Entity
@NamedQuery(name="Windowoutput.findAll", query="SELECT w FROM Windowoutput w")
public class Windowoutput implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WINDOWOUTPUT_ID")
	private long windowoutputId;

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATETIME")
	private Date createdDatetime;

	@Column(name="PATIENT_ID")
	private BigDecimal patientId;

	@Column(name="PATIENT_NAME")
	private String patientName;

	@Column(name="PATIENT_NO")
	private String patientNo;

	@Column(name="VISIT_ID")
	private BigDecimal visitId;

	@Column(name="WINDOW_ID")
	private BigDecimal windowId;

	@Column(name="WINDOW_NAME")
	private String windowName;

	@Column(name="WINDOW_NO")
	private String windowNo;

	public Windowoutput() {
	}

	public long getWindowoutputId() {
		return this.windowoutputId;
	}

	public void setWindowoutputId(long windowoutputId) {
		this.windowoutputId = windowoutputId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public BigDecimal getPatientId() {
		return this.patientId;
	}

	public void setPatientId(BigDecimal patientId) {
		this.patientId = patientId;
	}

	public String getPatientName() {
		return this.patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getPatientNo() {
		return this.patientNo;
	}

	public void setPatientNo(String patientNo) {
		this.patientNo = patientNo;
	}

	public BigDecimal getVisitId() {
		return this.visitId;
	}

	public void setVisitId(BigDecimal visitId) {
		this.visitId = visitId;
	}

	public BigDecimal getWindowId() {
		return this.windowId;
	}

	public void setWindowId(BigDecimal windowId) {
		this.windowId = windowId;
	}

	public String getWindowName() {
		return this.windowName;
	}

	public void setWindowName(String windowName) {
		this.windowName = windowName;
	}

	public String getWindowNo() {
		return this.windowNo;
	}

	public void setWindowNo(String windowNo) {
		this.windowNo = windowNo;
	}

}