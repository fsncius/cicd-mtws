package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the CHARGEITEMMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Chargeitemmstr.findAll", query="SELECT c FROM Chargeitemmstr c")
public class Chargeitemmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CHARGEITEMMSTR_ID")
	private long chargeitemmstrId;

	@Column(name="BASE_UNIT_TYPE")
	private String baseUnitType;

	@Column(name="CHARGE_CLASSIFICATION")
	private String chargeClassification;

	@Column(name="CHARGE_REVENUECENTREMSTR_ID")
	private BigDecimal chargeRevenuecentremstrId;

	@Column(name="CHARGE_TYPE")
	private String chargeType;

	@Column(name="CN_MOH_ITEM_CODE")
	private String cnMohItemCode;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DISCOUNT_ALLOWED_IND")
	private String discountAllowedInd;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="FIXED_PRICE_IND")
	private String fixedPriceInd;

	@Column(name="GENDER_ALLOWED")
	private String genderAllowed;

	@Column(name="GL_ACCOUNT")
	private String glAccount;

	@Column(name="GL_CATEGORIES")
	private String glCategories;

	@Column(name="ITEMSUBCATEGORYMSTR_ID")
	private BigDecimal itemsubcategorymstrId;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	@Column(name="PRICE_TYPE")
	private String priceType;

	@Column(name="PRICE_UOM")
	private String priceUom;

	private String remarks;

	@Column(name="REVENUECENTREMSTR_ID")
	private BigDecimal revenuecentremstrId;

	@Column(name="REVENUECTRLACCOUNTMSTR_ID")
	private BigDecimal revenuectrlaccountmstrId;

	private String spec;

	@Column(name="SPEC_LANG1")
	private String specLang1;

	@Column(name="TAX_INCLUSIVE_IND")
	private String taxInclusiveInd;

	@Column(name="TAXABLE_IND")
	private String taxableInd;

	@Column(name="TEMP_ID")
	private String tempId;

	@Column(name="TXNCODEMSTR_ID")
	private BigDecimal txncodemstrId;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Accommcharge
	@OneToMany(mappedBy="chargeitemmstr")
	private List<Accommcharge> accommcharges;

	//bi-directional many-to-one association to Baseunitprice
	@OneToMany(mappedBy="chargeitemmstr")
	private List<Baseunitprice> baseunitprices;

	//bi-directional many-to-one association to Baseunitpricehistory
	@OneToMany(mappedBy="chargeitemmstr")
	private List<Baseunitpricehistory> baseunitpricehistories;

	//bi-directional many-to-one association to Charge
	@OneToMany(mappedBy="chargeitemmstr")
	private List<Charge> charges;

	//bi-directional many-to-one association to Chargerollingplan
	@OneToMany(mappedBy="chargeitemmstr")
	private List<Chargerollingplan> chargerollingplans;

	//bi-directional many-to-one association to Chargetemplatedetail
	@OneToMany(mappedBy="chargeitemmstr")
	private List<Chargetemplatedetail> chargetemplatedetails;

	//bi-directional many-to-one association to Regntypecharge
	@OneToMany(mappedBy="chargeitemmstr")
	private List<Regntypecharge> regntypecharges;

	//bi-directional many-to-one association to Surchargedetail
	@OneToMany(mappedBy="chargeitemmstr")
	private List<Surchargedetail> surchargedetails;

	//bi-directional many-to-one association to Surchargepolicydetail
	@OneToMany(mappedBy="chargeitemmstr")
	private List<Surchargepolicydetail> surchargepolicydetails;

	public Chargeitemmstr() {
	}

	public long getChargeitemmstrId() {
		return this.chargeitemmstrId;
	}

	public void setChargeitemmstrId(long chargeitemmstrId) {
		this.chargeitemmstrId = chargeitemmstrId;
	}

	public String getBaseUnitType() {
		return this.baseUnitType;
	}

	public void setBaseUnitType(String baseUnitType) {
		this.baseUnitType = baseUnitType;
	}

	public String getChargeClassification() {
		return this.chargeClassification;
	}

	public void setChargeClassification(String chargeClassification) {
		this.chargeClassification = chargeClassification;
	}

	public BigDecimal getChargeRevenuecentremstrId() {
		return this.chargeRevenuecentremstrId;
	}

	public void setChargeRevenuecentremstrId(BigDecimal chargeRevenuecentremstrId) {
		this.chargeRevenuecentremstrId = chargeRevenuecentremstrId;
	}

	public String getChargeType() {
		return this.chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getCnMohItemCode() {
		return this.cnMohItemCode;
	}

	public void setCnMohItemCode(String cnMohItemCode) {
		this.cnMohItemCode = cnMohItemCode;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDiscountAllowedInd() {
		return this.discountAllowedInd;
	}

	public void setDiscountAllowedInd(String discountAllowedInd) {
		this.discountAllowedInd = discountAllowedInd;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getFixedPriceInd() {
		return this.fixedPriceInd;
	}

	public void setFixedPriceInd(String fixedPriceInd) {
		this.fixedPriceInd = fixedPriceInd;
	}

	public String getGenderAllowed() {
		return this.genderAllowed;
	}

	public void setGenderAllowed(String genderAllowed) {
		this.genderAllowed = genderAllowed;
	}

	public String getGlAccount() {
		return this.glAccount;
	}

	public void setGlAccount(String glAccount) {
		this.glAccount = glAccount;
	}

	public String getGlCategories() {
		return this.glCategories;
	}

	public void setGlCategories(String glCategories) {
		this.glCategories = glCategories;
	}

	public BigDecimal getItemsubcategorymstrId() {
		return this.itemsubcategorymstrId;
	}

	public void setItemsubcategorymstrId(BigDecimal itemsubcategorymstrId) {
		this.itemsubcategorymstrId = itemsubcategorymstrId;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getPriceType() {
		return this.priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public String getPriceUom() {
		return this.priceUom;
	}

	public void setPriceUom(String priceUom) {
		this.priceUom = priceUom;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getRevenuecentremstrId() {
		return this.revenuecentremstrId;
	}

	public void setRevenuecentremstrId(BigDecimal revenuecentremstrId) {
		this.revenuecentremstrId = revenuecentremstrId;
	}

	public BigDecimal getRevenuectrlaccountmstrId() {
		return this.revenuectrlaccountmstrId;
	}

	public void setRevenuectrlaccountmstrId(BigDecimal revenuectrlaccountmstrId) {
		this.revenuectrlaccountmstrId = revenuectrlaccountmstrId;
	}

	public String getSpec() {
		return this.spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public String getSpecLang1() {
		return this.specLang1;
	}

	public void setSpecLang1(String specLang1) {
		this.specLang1 = specLang1;
	}

	public String getTaxInclusiveInd() {
		return this.taxInclusiveInd;
	}

	public void setTaxInclusiveInd(String taxInclusiveInd) {
		this.taxInclusiveInd = taxInclusiveInd;
	}

	public String getTaxableInd() {
		return this.taxableInd;
	}

	public void setTaxableInd(String taxableInd) {
		this.taxableInd = taxableInd;
	}

	public String getTempId() {
		return this.tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public BigDecimal getTxncodemstrId() {
		return this.txncodemstrId;
	}

	public void setTxncodemstrId(BigDecimal txncodemstrId) {
		this.txncodemstrId = txncodemstrId;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public List<Accommcharge> getAccommcharges() {
		return this.accommcharges;
	}

	public void setAccommcharges(List<Accommcharge> accommcharges) {
		this.accommcharges = accommcharges;
	}

	public Accommcharge addAccommcharge(Accommcharge accommcharge) {
		getAccommcharges().add(accommcharge);
		accommcharge.setChargeitemmstr(this);

		return accommcharge;
	}

	public Accommcharge removeAccommcharge(Accommcharge accommcharge) {
		getAccommcharges().remove(accommcharge);
		accommcharge.setChargeitemmstr(null);

		return accommcharge;
	}

	public List<Baseunitprice> getBaseunitprices() {
		return this.baseunitprices;
	}

	public void setBaseunitprices(List<Baseunitprice> baseunitprices) {
		this.baseunitprices = baseunitprices;
	}

	public Baseunitprice addBaseunitprice(Baseunitprice baseunitprice) {
		getBaseunitprices().add(baseunitprice);
		baseunitprice.setChargeitemmstr(this);

		return baseunitprice;
	}

	public Baseunitprice removeBaseunitprice(Baseunitprice baseunitprice) {
		getBaseunitprices().remove(baseunitprice);
		baseunitprice.setChargeitemmstr(null);

		return baseunitprice;
	}

	public List<Baseunitpricehistory> getBaseunitpricehistories() {
		return this.baseunitpricehistories;
	}

	public void setBaseunitpricehistories(List<Baseunitpricehistory> baseunitpricehistories) {
		this.baseunitpricehistories = baseunitpricehistories;
	}

	public Baseunitpricehistory addBaseunitpricehistory(Baseunitpricehistory baseunitpricehistory) {
		getBaseunitpricehistories().add(baseunitpricehistory);
		baseunitpricehistory.setChargeitemmstr(this);

		return baseunitpricehistory;
	}

	public Baseunitpricehistory removeBaseunitpricehistory(Baseunitpricehistory baseunitpricehistory) {
		getBaseunitpricehistories().remove(baseunitpricehistory);
		baseunitpricehistory.setChargeitemmstr(null);

		return baseunitpricehistory;
	}

	public List<Charge> getCharges() {
		return this.charges;
	}

	public void setCharges(List<Charge> charges) {
		this.charges = charges;
	}

	public Charge addCharge(Charge charge) {
		getCharges().add(charge);
		charge.setChargeitemmstr(this);

		return charge;
	}

	public Charge removeCharge(Charge charge) {
		getCharges().remove(charge);
		charge.setChargeitemmstr(null);

		return charge;
	}

	public List<Chargerollingplan> getChargerollingplans() {
		return this.chargerollingplans;
	}

	public void setChargerollingplans(List<Chargerollingplan> chargerollingplans) {
		this.chargerollingplans = chargerollingplans;
	}

	public Chargerollingplan addChargerollingplan(Chargerollingplan chargerollingplan) {
		getChargerollingplans().add(chargerollingplan);
		chargerollingplan.setChargeitemmstr(this);

		return chargerollingplan;
	}

	public Chargerollingplan removeChargerollingplan(Chargerollingplan chargerollingplan) {
		getChargerollingplans().remove(chargerollingplan);
		chargerollingplan.setChargeitemmstr(null);

		return chargerollingplan;
	}

	public List<Chargetemplatedetail> getChargetemplatedetails() {
		return this.chargetemplatedetails;
	}

	public void setChargetemplatedetails(List<Chargetemplatedetail> chargetemplatedetails) {
		this.chargetemplatedetails = chargetemplatedetails;
	}

	public Chargetemplatedetail addChargetemplatedetail(Chargetemplatedetail chargetemplatedetail) {
		getChargetemplatedetails().add(chargetemplatedetail);
		chargetemplatedetail.setChargeitemmstr(this);

		return chargetemplatedetail;
	}

	public Chargetemplatedetail removeChargetemplatedetail(Chargetemplatedetail chargetemplatedetail) {
		getChargetemplatedetails().remove(chargetemplatedetail);
		chargetemplatedetail.setChargeitemmstr(null);

		return chargetemplatedetail;
	}

	public List<Regntypecharge> getRegntypecharges() {
		return this.regntypecharges;
	}

	public void setRegntypecharges(List<Regntypecharge> regntypecharges) {
		this.regntypecharges = regntypecharges;
	}

	public Regntypecharge addRegntypecharge(Regntypecharge regntypecharge) {
		getRegntypecharges().add(regntypecharge);
		regntypecharge.setChargeitemmstr(this);

		return regntypecharge;
	}

	public Regntypecharge removeRegntypecharge(Regntypecharge regntypecharge) {
		getRegntypecharges().remove(regntypecharge);
		regntypecharge.setChargeitemmstr(null);

		return regntypecharge;
	}

	public List<Surchargedetail> getSurchargedetails() {
		return this.surchargedetails;
	}

	public void setSurchargedetails(List<Surchargedetail> surchargedetails) {
		this.surchargedetails = surchargedetails;
	}

	public Surchargedetail addSurchargedetail(Surchargedetail surchargedetail) {
		getSurchargedetails().add(surchargedetail);
		surchargedetail.setChargeitemmstr(this);

		return surchargedetail;
	}

	public Surchargedetail removeSurchargedetail(Surchargedetail surchargedetail) {
		getSurchargedetails().remove(surchargedetail);
		surchargedetail.setChargeitemmstr(null);

		return surchargedetail;
	}

	public List<Surchargepolicydetail> getSurchargepolicydetails() {
		return this.surchargepolicydetails;
	}

	public void setSurchargepolicydetails(List<Surchargepolicydetail> surchargepolicydetails) {
		this.surchargepolicydetails = surchargepolicydetails;
	}

	public Surchargepolicydetail addSurchargepolicydetail(Surchargepolicydetail surchargepolicydetail) {
		getSurchargepolicydetails().add(surchargepolicydetail);
		surchargepolicydetail.setChargeitemmstr(this);

		return surchargepolicydetail;
	}

	public Surchargepolicydetail removeSurchargepolicydetail(Surchargepolicydetail surchargepolicydetail) {
		getSurchargepolicydetails().remove(surchargepolicydetail);
		surchargepolicydetail.setChargeitemmstr(null);

		return surchargepolicydetail;
	}

}