package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CONSUMERCARDTXNDETAIL database table.
 * 
 */
@Entity
@NamedQuery(name="Consumercardtxndetail.findAll", query="SELECT c FROM Consumercardtxndetail c")
public class Consumercardtxndetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CONSUMERCARDTXNDETAIL_ID")
	private long consumercardtxndetailId;

	@Column(name="CONSUMERCARDTXN_ID")
	private BigDecimal consumercardtxnId;

	@Column(name="COUNTERCOLLECTION_ID")
	private BigDecimal countercollectionId;

	public Consumercardtxndetail() {
	}

	public long getConsumercardtxndetailId() {
		return this.consumercardtxndetailId;
	}

	public void setConsumercardtxndetailId(long consumercardtxndetailId) {
		this.consumercardtxndetailId = consumercardtxndetailId;
	}

	public BigDecimal getConsumercardtxnId() {
		return this.consumercardtxnId;
	}

	public void setConsumercardtxnId(BigDecimal consumercardtxnId) {
		this.consumercardtxnId = consumercardtxnId;
	}

	public BigDecimal getCountercollectionId() {
		return this.countercollectionId;
	}

	public void setCountercollectionId(BigDecimal countercollectionId) {
		this.countercollectionId = countercollectionId;
	}

}