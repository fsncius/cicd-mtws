package com.rsmurniteguh.webservice.dep.kthis.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STOREDISPENSEWINDOWMSTR database table.
 * 
 */
@Entity
@NamedQuery(name="Storedispensewindowmstr.findAll", query="SELECT s FROM Storedispensewindowmstr s")
public class Storedispensewindowmstr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STOREDISPENSEWINDOWMSTR_ID")
	private long storedispensewindowmstrId;

	@Column(name="DEFUNCT_IND")
	private String defunctInd;

	@Column(name="DISPENSEWINDOW_CODE")
	private String dispensewindowCode;

	@Column(name="DISPENSEWINDOW_DESC")
	private String dispensewindowDesc;

	@Column(name="DISPENSEWINDOW_DESC_LANG1")
	private String dispensewindowDescLang1;

	@Column(name="DISPENSEWINDOW_STATUS")
	private String dispensewindowStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_PREPARATION_DATETIME")
	private Date lastPreparationDatetime;

	@Column(name="LAST_UPDATED_BY")
	private BigDecimal lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATETIME")
	private Date lastUpdatedDatetime;

	@Column(name="PREV_UPDATED_BY")
	private BigDecimal prevUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PREV_UPDATED_DATETIME")
	private Date prevUpdatedDatetime;

	private String remarks;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	//bi-directional many-to-one association to Charge
	@OneToMany(mappedBy="storedispensewindowmstr")
	private List<Charge> charges;


	//bi-directional many-to-one association to Storemstr
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STOREMSTR_ID")
	private Storemstr storemstr;

	public Storedispensewindowmstr() {
	}

	public long getStoredispensewindowmstrId() {
		return this.storedispensewindowmstrId;
	}

	public void setStoredispensewindowmstrId(long storedispensewindowmstrId) {
		this.storedispensewindowmstrId = storedispensewindowmstrId;
	}

	public String getDefunctInd() {
		return this.defunctInd;
	}

	public void setDefunctInd(String defunctInd) {
		this.defunctInd = defunctInd;
	}

	public String getDispensewindowCode() {
		return this.dispensewindowCode;
	}

	public void setDispensewindowCode(String dispensewindowCode) {
		this.dispensewindowCode = dispensewindowCode;
	}

	public String getDispensewindowDesc() {
		return this.dispensewindowDesc;
	}

	public void setDispensewindowDesc(String dispensewindowDesc) {
		this.dispensewindowDesc = dispensewindowDesc;
	}

	public String getDispensewindowDescLang1() {
		return this.dispensewindowDescLang1;
	}

	public void setDispensewindowDescLang1(String dispensewindowDescLang1) {
		this.dispensewindowDescLang1 = dispensewindowDescLang1;
	}

	public String getDispensewindowStatus() {
		return this.dispensewindowStatus;
	}

	public void setDispensewindowStatus(String dispensewindowStatus) {
		this.dispensewindowStatus = dispensewindowStatus;
	}

	public Date getLastPreparationDatetime() {
		return this.lastPreparationDatetime;
	}

	public void setLastPreparationDatetime(Date lastPreparationDatetime) {
		this.lastPreparationDatetime = lastPreparationDatetime;
	}

	public BigDecimal getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public BigDecimal getPrevUpdatedBy() {
		return this.prevUpdatedBy;
	}

	public void setPrevUpdatedBy(BigDecimal prevUpdatedBy) {
		this.prevUpdatedBy = prevUpdatedBy;
	}

	public Date getPrevUpdatedDatetime() {
		return this.prevUpdatedDatetime;
	}

	public void setPrevUpdatedDatetime(Date prevUpdatedDatetime) {
		this.prevUpdatedDatetime = prevUpdatedDatetime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public List<Charge> getCharges() {
		return this.charges;
	}

	public void setCharges(List<Charge> charges) {
		this.charges = charges;
	}

	public Charge addCharge(Charge charge) {
		getCharges().add(charge);
		charge.setStoredispensewindowmstr(this);

		return charge;
	}

	public Charge removeCharge(Charge charge) {
		getCharges().remove(charge);
		charge.setStoredispensewindowmstr(null);

		return charge;
	}


	public Storemstr getStoremstr() {
		return this.storemstr;
	}

	public void setStoremstr(Storemstr storemstr) {
		this.storemstr = storemstr;
	}

}