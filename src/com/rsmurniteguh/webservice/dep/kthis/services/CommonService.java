package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.omg.CORBA.portable.ApplicationException;

import com.rsmurniteguh.webservice.dep.all.model.mobile.Berita;
import com.rsmurniteguh.webservice.dep.base.ISysConstant;
import com.rsmurniteguh.webservice.dep.kthis.model.DropDownBo;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.CommonUtil;
import com.rsmurniteguh.webservice.dep.util.FileUploadUtil;
import com.rsmurniteguh.webservice.dep.util.LanguageUtil;
import com.rsmurniteguh.webservice.dep.util.WinRegistry;

public class CommonService {
	private final String COLUMN_PARAMETER_PRODUCTION = "PARAMETER.VALUE";
	private final String COLUMN_PARAMETER_DEVELOPMENT = "PARAMETER.DEV_VALUE";
	
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static Properties configFile = new Properties();
	private static String fileConfigPath = "/root/.webservice";
	
	public String getParameterValue(String parameterName){
		String result = null;
		Connection connection = DbConnection.getWebServicePooledConnection();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT "
				+ " " + (getProgramEnvironment().equals(ISysConstant.ENV_PRODUCTION) ? COLUMN_PARAMETER_PRODUCTION : COLUMN_PARAMETER_DEVELOPMENT) + " "
				+ " as PARVALUE FROM PARAMETER"
				+ " WHERE PARAMETER_NAME = ? "
				+ " AND DEFUNCT_IND = 'N' ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, parameterName);
			rs = ps.executeQuery();
			if (rs.next()){
				result = rs.getString("PARVALUE");
			}
		} catch (SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
		} catch (Exception e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
		} finally {
		 	 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}	
		return result;
	}
	
	public String getProgramEnvironment(){
		String result = "";
		
		try {
			if (isWindows()) {
				result = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE,//HKEY
					    ISysConstant.REGISTRY_LOCATION,				           //Key
					    ISysConstant.REGISTRY_NAME);
			} else if (isUnix()) {
				FileInputStream is = new FileInputStream(fileConfigPath);
				configFile.load(is);
				result   = configFile.getProperty("production");
			} else {
				System.out.println("Your OS is not support!!");
			}
			
		} catch (IllegalArgumentException e) {
			result = "";
		} catch (IllegalAccessException e) {
			result = "";
		} catch (InvocationTargetException e) {
			result = "";
		} catch (FileNotFoundException e) {
			result = "";
		} catch (IOException e) {
			result = "";
		}
		if (result == null) result = "";
		return result;
	}
	
	public static Object get_berita(String reader_role) {
		List<Berita> list = new ArrayList<Berita>();

		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getBpjsOnlineInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			String sql = "SELECT berita_id, judul, tgl, isi "
					+ "FROM tb_berita WHERE tgl BETWEEN ? AND ?  ";
			
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
	        Calendar cal = Calendar.getInstance();
	        String dateTo = sdf.format(cal.getTime());
	        cal.add(Calendar.DATE, -30);
	        String dateFrom = sdf.format(cal.getTime());
			
			ps = Connection.prepareStatement(sql);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, dateFrom);
			ps.setString(2, dateTo);
			rs = ps.executeQuery();
			
			while (rs.next()){
				Berita dl = new Berita();
				dl.setBerita_id(rs.getString("berita_id"));
				dl.setJudul(rs.getString("judul"));
				dl.setTanggal(rs.getString("tgl"));
				dl.setSimple_berita(rs.getString("isi"));
				
				list.add(dl);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		}
		return list;
	}

	public static List<DropDownBo> getListDropDownBoByCodeCat(String codeCat){
		List<DropDownBo> result = new ArrayList<DropDownBo>();
		try {
			String selectQuery = "SELECT CM.CODEMSTR_ID, CM.CODE_CAT, CM.CODE_ABBR, CM.CODE_DESC, CM.CODE_DESC_LANG1, CM.CODE_DESC_LANG2, CM.CODE_DESC_LANG3 FROM CODEMSTR CM "
					+ " WHERE CM.DEFUNCT_IND = 'N' "
					+ " AND CM.CODE_CAT = ? "
					+ " ORDER BY CM.CODE_SEQ ";
			List list = DbConnection.executeReader(DbConnection.KTHIS, selectQuery, new Object[] {codeCat});
			
			for (int a=0;a<list.size();a++){
				HashMap row = (HashMap)list.get(a);
				DropDownBo view = new DropDownBo();
				view.setPrimaryId((BigDecimal) row.get("CODEMSTR_ID"));
				view.setCode(row.get("CODE_CAT").toString() + row.get("CODE_ABBR").toString());
				view.setNormalDesc(row.get("CODE_DESC").toString());
				result.add(view);
			}
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		finally {
		}
		return result;
	}

	public String getSequenceNo(String sequenceNoType, BigDecimal userMstrId, String paramValue) {
		String language = "en_US";

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("piSequenceId", sequenceNoType); //$NON-NLS-1$
		map.put("piUserMstrId", userMstrId); //$NON-NLS-1$
		map.put("piExtPara", paramValue); //$NON-NLS-1$

//		map.put("poSeqNo", "");
//		map.put("poErrorCode", "");
//		map.put("poErrorCnMsg", "");
//		map.put("poErrorEnMsg", "");
		
		getSequenceNo(map);

		String seqNo = (String) map.get("poSeqNo"); //$NON-NLS-1$
		String errorCode = (String) map.get("poErrorCode"); //$NON-NLS-1$

		if (errorCode != null) {
//			String error = LanguageUtil.getInstance().getProcedureError(map);
//			ApplicationException appException = new ApplicationException(error);
//			throw appException;
		}
		return seqNo;
	}
	
	private void getSequenceNo(Map map) {
		Connection pooledConnection = null;
		CallableStatement pstmtGetReport = null;
		
		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null ) throw new Exception("Database failed to connect !");
			
			String selectQuery = "call pgSysFunc.prGenSeqNo(?,?,?,?,?,?,?) ";
			pstmtGetReport = pooledConnection.prepareCall(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			pstmtGetReport.setString(1, map.get("piSequenceId").toString());
			pstmtGetReport.setString(2, map.get("piUserMstrId").toString());
			pstmtGetReport.setString(3, map.get("piExtPara") == null ? "" : map.get("piExtPara").toString());
			pstmtGetReport.registerOutParameter(4, java.sql.Types.VARCHAR);
			pstmtGetReport.registerOutParameter(5, java.sql.Types.VARCHAR);
			pstmtGetReport.registerOutParameter(6, java.sql.Types.VARCHAR);
			pstmtGetReport.registerOutParameter(7, java.sql.Types.VARCHAR);
			
			pstmtGetReport.executeUpdate();
			
			map.put("poSeqNo", pstmtGetReport.getString(4));
			map.put("poErrorCode", pstmtGetReport.getString(5));
			map.put("poErrorCnMsg", pstmtGetReport.getString(6));
			map.put("poErrorEnMsg", pstmtGetReport.getString(7));
			
			pstmtGetReport.close();
			pooledConnection.close();
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
			if (pstmtGetReport != null) try { pstmtGetReport.close(); } catch (SQLException e) {}
		}
    }

	public BigDecimal getPrimaryKey(){
		String query = "SELECT PGSYSFUNC.FXGENPRIMARYKEY PRIMARYKEY FROM DUAL";
		BigDecimal result = BigDecimal.ZERO;
		try {
			result = (BigDecimal) DbConnection.executeScalar(DbConnection.KTHIS, query, new Object[] {});
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return result;
	}
	
	public Timestamp getCurrentTime(){
		String query = "SELECT SYSDATE FROM DUAL";
		Timestamp result = null;
		try {
			result = (Timestamp) DbConnection.executeScalar(DbConnection.KTHIS, query, new Object[] {});
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return result;
	}
	
	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}
	public static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 || OS.indexOf("bsd") > 0 );
	}
	
}