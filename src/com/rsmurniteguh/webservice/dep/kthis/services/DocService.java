package com.rsmurniteguh.webservice.dep.kthis.services;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.rsmurniteguh.webservice.dep.all.model.NamaMapBpjs;
import com.rsmurniteguh.webservice.dep.kthis.model.Locationmstr;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.EntityManagerUtil;
import com.rsmurniteguh.webservice.dep.kthis.view.CodeDescView;

public class DocService {
	private static EntityManager entityManager=EntityManagerUtil.getEntityManager();
	public static List<CodeDescView> getDataDoctor(String idDok) {
		
		entityManager.getTransaction().begin();
		Locationmstr doc= new Locationmstr();
		
		doc.setLocationmstrId(Long.parseLong(idDok));
		Query dadok=entityManager.createQuery("select t.careproviderId, p.personName from Careprovider t JOIN t.person p where "
				+ "t.careproviderId = :iddok and t.careproviderType = 'CPR1' and t.defunctInd='N' ");
		dadok.setParameter("iddok", Long.parseLong(idDok));
		
		List list = dadok.getResultList();
	    
	    List<CodeDescView> doctors = new ArrayList<CodeDescView>(); 
	    if(!list.isEmpty())
	    {
	    	for(Object o : list)
	    	{
	    		Object[] obj = (Object[])o;
	    		CodeDescView doctor  = new CodeDescView();
	    		doctor.setId(new BigDecimal(obj[0].toString()));
	    		doctor.setCode(obj[1].toString());
	    		doctors.add(doctor);
	    	}
	    }
		
		
		entityManager.getTransaction().commit();
		return doctors;
		
	}
	
	public static List<NamaMapBpjs> getDataPoly() {
		// TODO Auto-generated method stub
		List<NamaMapBpjs> all = new ArrayList<NamaMapBpjs>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet PolyId=null;
		if(connection == null)return null;
		Statement getDataPoly=null;
		String idPoly = "SELECT NULL RESOURCESCHEME_ID, "
				+ "RM.POLI_CODE, "
				+ "RM.RESOURCEMSTR_ID, "
				+ "RM.RESOURCE_CODE, "
				+ "RM.RESOURCE_SHORT_CODE, "
				+ "RM.RESOURCE_QUICK_CODE, "
				+ "RM.RESOURCE_NAME, "
				+ "RM.RESOURCE_NAME_LANG1, "
				+ "RM.REGISTRATION_TYPE, "
				+ "RM.SUBSPECIALTYMSTR_ID, "
				+ "SSPM.SUBSPECIALTY_DESC, "
				+ "SSPM.SUBSPECIALTY_DESC_LANG1, "
				+ "RM.CAREPROVIDER_ID, "
				+ "NULL SESSIONCODE, "
				+ "NULL SESSIONDESC, "
				+ "NULL SESSIONDESC1, "
				+ "NULL REGNLIMIT, "
				+ "NULL REGNCOUNT, "
				+ "NULL REGNAVAILABLECOUNT, "
				+ "(SELECT REGN_TYPE FROM (SELECT RPCL.REGN_TYPE "
				+ "FROM REGNPATIENTCLASSLIMIT RPCL "
				+ "WHERE RPCL.PATIENT_CLASS = 'PTC114' "
				+ "AND RPCL.DEFUNCT_IND = 'N' "
				+ "ORDER BY RPCL.REGN_TYPE) TEMP "
				+ "WHERE ROWNUM = 1) AS PATIENT_CLASS_REGN_TYPE "
				+ "FROM RESOURCEMSTR RM, SUBSPECIALTYMSTR SSPM "
				+ "WHERE RM.SUBSPECIALTYMSTR_ID = SSPM.SUBSPECIALTYMSTR_ID "
				+ "AND RM.RESOURCE_SCHEME_IND = 'N' "
				+ "AND RM.DEFUNCT_IND = 'N' "
				+ "AND SSPM.SUBSPECIALTYMSTR_ID = 336581902 "
				+ "AND EXISTS ( "
				+ "SELECT 0 FROM REGNPATIENTTYPELIMIT RPL, "
				+ "(SELECT PATIENT_TYPE, NVL(PATIENT_CLASS, 'NULL') PATIENT_CLASS "
				+ "FROM (SELECT RPL.PATIENT_TYPE, RPL.PATIENT_CLASS "
				+ "FROM REGNPATIENTTYPELIMIT RPL WHERE RPL.DEFUNCT_IND = 'N' "
				+ "AND (RPL.PATIENT_CLASS = 'PTC114' OR RPL.PATIENT_CLASS IS NULL) "
				+ "ORDER BY RPL.PATIENT_CLASS) "
				+ "WHERE ROWNUM = 1) MATCHED_RPL_POLICY "
				+ "WHERE RPL.DEFUNCT_IND = 'N' "
				+ "AND RPL.PATIENT_TYPE = MATCHED_RPL_POLICY.PATIENT_TYPE "
				+ "AND NVL(RPL.PATIENT_CLASS, 'NULL') = MATCHED_RPL_POLICY.PATIENT_CLASS "
				+ "AND RPL.REGN_TYPE = RM.REGISTRATION_TYPE)";
		try 
		{
			getDataPoly = connection.createStatement();
			PolyId= getDataPoly.executeQuery(idPoly);
			while (PolyId.next())
			{
				NamaMapBpjs nmb = new NamaMapBpjs();
				nmb.setRESOURCESCHEME_ID(PolyId.getString("RESOURCESCHEME_ID"));
				nmb.setPOLI_CODE(PolyId.getString("POLI_CODE"));
				nmb.setRESOURCEMSTR_ID(PolyId.getBigDecimal("RESOURCEMSTR_ID"));
				nmb.setRESOURCE_CODE(PolyId.getString("RESOURCE_CODE"));
				nmb.setRESOURCE_SHORT_CODE(PolyId.getString("RESOURCE_SHORT_CODE"));
				nmb.setRESOURCE_QUICK_CODE(PolyId.getString("RESOURCE_QUICK_CODE"));
				nmb.setRESOURCE_NAME(PolyId.getString("RESOURCE_NAME"));
				nmb.setRESOURCE_NAME_LANG1(PolyId.getString("RESOURCE_NAME_LANG1"));
				nmb.setREGISTRATION_TYPE(PolyId.getString("REGISTRATION_TYPE"));
				nmb.setSUBSPECIALTYMSTR_ID(PolyId.getBigDecimal("SUBSPECIALTYMSTR_ID"));
				nmb.setSUBSPECIALTY_DESC(PolyId.getString("SUBSPECIALTY_DESC"));
				nmb.setSUBSPECIALTY_DESC_LANG1(PolyId.getString("SUBSPECIALTY_DESC_LANG1"));
				nmb.setCAREPROVIDER_ID(PolyId.getBigDecimal("CAREPROVIDER_ID"));
				nmb.setSESSIONCODE(PolyId.getString("SESSIONCODE"));
				nmb.setSESSIONDESC(PolyId.getString("SESSIONDESC"));
				nmb.setSESSIONDESC1(PolyId.getString("SESSIONDESC1"));
				nmb.setREGNLIMIT(PolyId.getString("REGNLIMIT"));
				nmb.setREGNCOUNT(PolyId.getString("REGNCOUNT"));
				nmb.setREGNAVAILABLECOUNT(PolyId.getString("REGNAVAILABLECOUNT"));
				nmb.setPATIENT_CLASS_REGN_TYPE(PolyId.getString("PATIENT_CLASS_REGN_TYPE"));
				
				
				all.add(nmb);
			}
			PolyId.close();
			getDataPoly.close();
						
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (PolyId!=null) try  { PolyId.close(); } catch (Exception ignore){}
		     if (getDataPoly!=null) try  { getDataPoly.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

}
