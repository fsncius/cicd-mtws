package com.rsmurniteguh.webservice.dep.kthis.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.StoreItemMst;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class StoreItemSer extends DbConnection {

	public static List<StoreItemMst> getStoreItem(String storeMstrId, String materialItemMstrId) {
		List<StoreItemMst> all=new ArrayList<StoreItemMst>();
		ResultSet hasstore=null;
		Connection connection=DbConnection.getPooledConnection();
		if(connection == null)return null;
		Statement storedat=null;
		String abstor="select storeitem_id from storeitem si where si.storemstr_id = '"+storeMstrId+"' and si.materialitemmstr_id = '"+materialItemMstrId+"'"
				+ " and si.defunct_ind = 'N'";
		try {
			storedat=connection.createStatement();
			hasstore=storedat.executeQuery(abstor);
			while(hasstore.next())
			{
				StoreItemMst smmt=new StoreItemMst();
				smmt.setStoreItemId(hasstore.getLong("storeitem_id"));
				all.add(smmt);
			}
			hasstore.close();
			storedat.close();
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasstore!=null) try  { hasstore.close(); } catch (Exception ignore){}
		     if (storedat!=null) try  { storedat.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

}
