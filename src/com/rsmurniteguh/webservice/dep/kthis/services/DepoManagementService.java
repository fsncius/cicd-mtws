package com.rsmurniteguh.webservice.dep.kthis.services;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.util.TextUtils;

import com.rsmurniteguh.webservice.dep.all.model.EmoneyCardDatailInfo;
import com.rsmurniteguh.webservice.dep.all.model.PaymentModel;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.ReturnResult;
import com.rsmurniteguh.webservice.dep.all.model.UpdateCard;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.AppointmentStatus;
import com.rsmurniteguh.webservice.dep.biz.QueueServiceBiz;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.PaymentGatewayConnection;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.SoyalConnection;

public class DepoManagementService extends DbConnection {
	
	public static Boolean CheckCashierValid(String userid)
	{
		Boolean b = false;
		Connection connection=DbConnection.getPooledConnection();
		Connection connection2=DbConnection.getTrxInstance();
		ResultSet hascare=null , rs=null;
		if(connection == null || connection2 == null)return null;
		Statement Cashier=null;
		PreparedStatement ps= null;
		
		String abcarpro="select * from handovergroupdetail hgd where hgd.collection_type = 'CTY13' and hgd.defunct_ind = 'N' and hgd.usermstr_id= "+userid;
		String sql = "SELECT additional_access_id FROM t_additional_access WHERE additional_access_id = ? AND defunct_ind = 'N' ";
		
		try {
			Cashier=connection.createStatement();
			hascare=Cashier.executeQuery(abcarpro);
			if(hascare.next())
			{
				b = true;
			}
			else
			{
				ps = connection2.prepareStatement(sql);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				ps.setString(1, userid);
				rs=ps.executeQuery();
				
				if(rs.next())
				{
					b = true;
				}
				else
				{
					b = false;
				}
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (hascare!=null) try  { hascare.close(); } catch (Exception ignore){}
		     if (Cashier!=null) try  { Cashier.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}

		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (connection2!=null) try { connection2.close();} catch (Exception ignore){}
		   }
		return b;
	}
	
	public static ReturnResult RegisterAccount(PaymentModel model)
	{
		ReturnResult ret = new ReturnResult(); 
		String message = "OK";
		Boolean success = true;
		
		Connection connection=DbConnection.getTrxInstance();
		Connection SoyalConn = SoyalConnection.getInstance();
		if(connection==null || SoyalConn==null)return null;
		/*
		if(!CheckCashierValid(model.getUserId()))
		{
			message = "Operator Tidak Diizinkan Untuk Melakukan Registrasi";
			success = false;
			ret.setSuccess(success);
			ret.message = message;
			return ret;
		}
		*/
		String regSql = "exec RegisterCard ?,?,?";
		String SPsql = "EXEC registeraccount ?,?,?,?,?,?,?,?";   // for stored proc taking 2 parameters
		
		PreparedStatement ps1 = null , ps = null, psc = null;
		ResultSet rs = null, rsc = null;
		try {
			String sqlCek = "SELECT card_no FROM t_accountbalance WHERE card_no=? AND status = 1";
			psc = connection.prepareStatement(sqlCek);
			psc.setEscapeProcessing(true);
			psc.setQueryTimeout(60000);
			psc.setString(1, model.getCardNo());
			rsc =  psc.executeQuery();
			if(rsc.next())
			{
				ret.setSuccess(false);
				ret.message = "Data belum di Defunct";
			}
			else
			{
				ps1 = SoyalConn.prepareStatement(regSql);
				ps1.setEscapeProcessing(true);
				ps1.setQueryTimeout(60000);
				ps1.setString(1, model.getCardNo());
				ps1.setString(2, model.getPin());
				ps1.setString(3, model.getCardName());
				ps1.executeUpdate();
				
				ps = connection.prepareStatement(SPsql);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				ps.setString(1, model.getCardNo());
				ps.setString(2, model.getNfcNo());
				ps.setString(3, model.getCardName());
				ps.setString(4, model.getUserId());
				ps.setString(5, model.getNik());
				ps.setString(6, model.getStatuskr());
				ps.setString(7, model.getStatusperusahaan());
				ps.setString(8, model.getUserdetail());
				rs =  ps.executeQuery();
				
				while(rs.next())
				{
					message = rs.getString("mess");
					ret.setSuccess(success);
					ret.message = message;
				}
			}
			
		} catch (SQLException e) {
			success = false;
			System.out.println(e.getMessage());
			message ="Failed to Create Account, Error : "+e.getMessage(); 
			ret.setSuccess(success);
			ret.message = message;
		} finally {
            if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
            if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
            if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
            if (psc!=null) try  { psc.close(); } catch (Exception ignore){}
            if (rsc!=null) try  { rsc.close(); } catch (Exception ignore){}
            if (SoyalConn!=null) try { SoyalConn.close();} catch (Exception ignore){}
            if (connection != null) try { connection.close(); }catch(Exception e){}
        }
		return ret;
	}
	
	public static ReturnResult UpdateAccount(PaymentModel model)
	{
		/*
		 * UPDATE funct ini digunakan hanya untuk mengupdate nfc_no dan user_detail berdasarkan accountbalance
		 * Paymentmodel ditambahkan nfc_no, user_detail, accountbalance_id untuk memenuhi fungsi
		 * @author : benyaminginting
		 */
		ReturnResult ret = new ReturnResult(); 
		String message = "OK";
		Boolean success = true;
		
		Connection connection=DbConnection.getTrxInstance();
		if(connection==null)return null;
		
		if(!CheckCashierValid(model.getUserId()))
		{
			message = "Operator Tidak Diizinkan Untuk Melakukan Registrasi";
			success = false;
			ret.setSuccess(success);
			ret.message = message;
			return ret;
		}
		
		String SPsql = "UPDATE t_accountbalance SET nfc_no = ? , user_detail = ? WHERE accountbalance_id = ? ";   
		
		PreparedStatement ps = null;
		try {
			
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, model.getNfcNo());
			ps.setString(2, model.getUserdetail());
			ps.setString(3, model.getAccountbalance_id());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			success = false;
			System.out.println(e.getMessage());
			message ="Failed to Create Account, Error : "+e.getMessage(); 
		} finally {
            if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
            if (connection != null) try { connection.close(); }catch(Exception e){}
        }

		ret.setSuccess(success);
		ret.message = message;
		return ret;
	}

	public static ReturnResult GetBalance(String cardNo)
	{
		String message = "Kartu Tidak Ditemukan";
		Connection connection=DbConnection.getTrxInstance();
		if(connection==null)return null;
		ResultSet rs=null;
		Boolean success = true;
		
		String SPsql = "select balance from t_accountbalance where (card_no = ? or nfc_no= ? ) and status = 1 and deleted = 0;";
		
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, cardNo);
			ps.setString(2, cardNo);
			rs =  ps.executeQuery();
			
			while(rs.next())
			{
				message = rs.getString("balance");
			}
			
		} catch (SQLException e) {
			success = false;
			System.out.println(e.getMessage());
			message ="Failed to Perform Transaction, Error : "+e.getMessage(); 
		} finally {
            if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
            if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
            if (connection!=null) try { connection.close();} catch (Exception ignore){}
        }
		ReturnResult ret = new ReturnResult(); 
		ret.success = success;
		ret.message = message;
		return ret;
	}
	
	private static Date getDbDatetime()
	{
		Date date;
		Connection connection=DbConnection.getTrxInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select getdate() as tanggal;";
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			
			rs =  ps.executeQuery();
			if(rs.next())
			{
				date = rs.getDate("tanggal");
			}
			else
			{
				return null;
			}
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
            if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
            if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
            if (connection!=null) try { connection.close();} catch (Exception ignore){}
        }
		
		return date;
	}
	
	private static Boolean checkSeqExists(String seq)
	{
		Boolean result = false;
		Connection connection= DbConnection.getTrxInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select seqname from allsequences where seqname = ?";
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, seq);
			rs =  ps.executeQuery();
			if(rs.next())
			{
				result = true;
			}
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
            if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
            if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
            if (connection!=null) try { connection.close();} catch (Exception ignore){}
        }
		
		return result;
	}
	
	private static Boolean CreateNewSeq(String tipe)
	{
		Connection connection=DbConnection.getTrxInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean result = false;
		Date date = getDbDatetime();
		
		String seq = concatSeqDate(tipe, date);
		
		
		String SPsql = "EXEC usp_CreateNewSeq ?";   // for stored proc taking 2 parameters
		
		
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, seq);
			rs =  ps.executeQuery();
			
			if(rs.next())
			{
				result = true;
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			result =  false;
		} finally {
            if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
            if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
            if (connection!=null) try { connection.close();} catch (Exception ignore){}
        }
		
		return result;
	}
	
	private static String concatSeqDate(String seq, Date date)
	{
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String month = String.valueOf(cal.get(Calendar.MONTH)+1);
		String year = String.valueOf(cal.get(Calendar.YEAR));
		
		String tahun = year.length() > 2 ? year.substring(year.length() - 2) : year;
		
		String bulan = StringUtils.leftPad(month, 2, '0');
		String result = seq.concat(tahun).concat(bulan);
		return result;
	}

	private static String generateNumber(String seq)
	{
		String message = "OK";
		Connection connection=DbConnection.getTrxInstance();
		if(connection==null)return null;
		ResultSet rs = null;
		
		String SPsql = "EXEC usp_GetNewSeqVal ?";   // for stored proc taking 2 parameters
		
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, seq);
			rs =  ps.executeQuery();
			
			while(rs.next())
			{
				message = rs.getString("number");
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			message ="Failed to Perform Transaction, Error : "+e.getMessage(); 
		} finally {
            if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
            if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
            if (connection!=null) try { connection.close();} catch (Exception ignore){}
        }
		return message;
	}
	
	public static ReturnResult getGeneratedDepositInvoice()
	{
		ReturnResult res = new ReturnResult();
		String seq = concatSeqDate("DEP", getDbDatetime());
		if(!checkSeqExists(seq))
		{
			CreateNewSeq("DEP");
		}
		res.success=true;
		res.message = seq.concat(StringUtils.leftPad(generateNumber(seq), 6, '0'));
		return res;
	}
	
	public static ReturnResult getGeneratedDepositInvoiceCan()
	{
		ReturnResult res=new ReturnResult();
		String seq=concatSeqDate("CAN", getDbDatetime());
		if(!checkSeqExists(seq)){
			CreateNewSeq("CAN");			
		}
		res.success=true;
		res.message=seq.concat(StringUtils.leftPad(generateNumber(seq), 6,'0'));
		return res;
	}
	
	private static String getGeneratedPaymentNo()
	{
		String seq = concatSeqDate("PAY", getDbDatetime());
		if(!checkSeqExists(seq))
		{
			CreateNewSeq("PAY");
		}
		return seq.concat(StringUtils.leftPad(generateNumber(seq), 6, '0'));
	}
	
	public static ReturnResult postPayment(PaymentModel model)
	{
		String message = "OK";
		Connection connection=DbConnection.getTrxInstance();
		if(connection==null)return null;
		ResultSet rs = null;
		ReturnResult ret = new ReturnResult(); 
		Boolean success = true;
		
		if(model.getInvoiceNo().contains("DEP") || model.getInvoiceNo().contains("CAN") )//untuk deposit
		{
			if(!CheckCashierValid(model.getUserId()))
			{
				message = "Operator Tidak Diizinkan Untuk Melakukan Deposit";
				success = false;
				ret.message = message;
				return ret;
			}
			
		}
		else//untuk pembayaran
		{
			model.setPaymentNo(getGeneratedPaymentNo());
		}
		
		String SPsql = "EXEC paymentTransaction ?,?,?,?,?";   // for stored proc taking 2 parameters
		
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, model.getCardNo());
			ps.setDouble(2, model.getTrxAmount());
			ps.setString(3, model.getInvoiceNo());
			ps.setString(4, model.getUserId().toString());
			ps.setString(5, model.getPaymentNo());
			rs =  ps.executeQuery();
			
			while(rs.next())
			{
				message = rs.getString("mess");
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			success = false;
			message ="Failed to Perform Transaction, Error : "+e.getMessage(); 
		} finally {
            if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
            if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
            if (connection!=null) try { connection.close();} catch (Exception ignore){}
        }

		
		String nomorBayar = "";
		if(model.getInvoiceNo().contains("DEP"))//untuk deposit
		{
			nomorBayar = model.getInvoiceNo();
		}
		else
		{
			nomorBayar = model.getPaymentNo();
		}
		ret.success=success;	
		ret.message = nomorBayar;
		return ret;
	}

	public static ReturnResult postPaymentModified(PaymentModel model)
	{
		String message = "OK";
		Connection connection=DbConnection.getTrxInstance();
		if(connection==null)return null;
		ResultSet rs = null;
		
		ReturnResult ret = new ReturnResult(); 
		Boolean success = true;
		
		if(model.getInvoiceNo().contains("DEP"))//untuk deposit
		{
			if(!CheckCashierValid(model.getUserId()))
			{
				message = "Operator Tidak Diizinkan Untuk Melakukan Deposit";
				success = false;
				ret.message = message;
				return ret;
			}
			
		}
		
		String SPsql = "EXEC DoPaymentTransaction ?,?,?,?,?";   // for stored proc taking 2 parameters
		
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, model.getCardNo());
			ps.setDouble(2, model.getTrxAmount());
			ps.setString(3, model.getInvoiceNo());
			ps.setString(4, model.getUserId().toString());
			ps.setString(5, model.getPaymentNo());
			rs =  ps.executeQuery();
			
			while(rs.next())
			{
				message = rs.getString("mess");
			}
			
		} catch (SQLException e) {
			success = false;
			message ="Failed to Perform Transaction, Error : "+e.getMessage(); 
			System.out.println(e.getMessage());
		} finally {
            if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
            if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
            if (connection!=null) try { connection.close();} catch (Exception ignore){}
        }
		
		String nomorBayar = "";
		if(model.getInvoiceNo().contains("DEP"))//untuk deposit
		{
			nomorBayar = model.getInvoiceNo();
		}
		else
		{
			nomorBayar = model.getPaymentNo();
		}
		ret.success=success;	
		ret.message = message;
		return ret;
	}
	
	public static ReturnResult PrepareGetCardDataFromMachine(String machineNo) {
		Connection connection=SoyalConnection.getInstance();
		if(connection==null)return null;
		Boolean success = false;

		ReturnResult ret = new ReturnResult(); 
		String message = "Kartu Tidak Ditemukan";
		String SPsql = "select door_id from doors d where d.door_fullid = ?;";   // for stored proc taking 2 parameters
		
		PreparedStatement ps;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, machineNo);
			ResultSet rs =  ps.executeQuery();
			
			if(!rs.next()) 
			{
				message = "Data Mesin Tidak Ditemukan";
			}
			else				
			{
				String deleteSql = "update events set status='1' where door_fullid = ?";
				ps = connection.prepareStatement(deleteSql);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				ps.setString(1, machineNo);
				ps.executeUpdate();
				message = "OK";
				success = true;
			}
			
		} catch (SQLException e) {
			message ="Failed to Perform Transaction, Error : "+e.getMessage(); 
		}finally {
			if (connection != null) try { connection.close(); }catch(Exception e){}
		}
		ret.success = success;
		ret.message = message;
		return ret;
		
	}

	public static ReturnResult GetCardDataByMachine(String machineNo) {

		Connection connection=SoyalConnection.getInstance();
		if(connection==null)return null;
		Boolean success = false;
		ReturnResult ret = new ReturnResult(); 
		String message = "Data Tidak Ditemukan";
		String SPsql = "select card_num1+card_num2 as cardNo from events where door_fullid = ? and status='0' order by date_created desc";   // for stored proc taking 2 parameters
		PreparedStatement ps;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, machineNo);
			ResultSet rs =  ps.executeQuery();
			
			while(rs.next()) 
			{
				message = rs.getString("cardNo");
				success = true;
			}
			
		} catch (SQLException e) {
			message ="Failed to Perform Transaction, Error : "+e.getMessage(); 
		} finally {
			if (connection != null) try { connection.close(); }catch(Exception e){}
		}
		ret.setSuccess(success);
		ret.message = message;
		return ret;
	}

	public static ReturnResult GetValidCardDataByMachine(String machineNo) {

		Connection connection=SoyalConnection.getInstance();
		Connection payConn = DbConnection.getTrxInstance();
		if(connection==null)return null;
		ResultSet rs2 = null;
		ResultSet rs = null;
		if(payConn==null)return null;
		
		Boolean success = false;
		ReturnResult ret = new ReturnResult(); 
		String message = "Data Tidak Ditemukan";
		//String SPsql = "select card_num1+card_num2 as cardNo from events where door_fullid = ? and event_msg like '%Normal%' and status='0' order by date_created desc";   // for stored proc taking 2 parameters

		String SPsql = "select e.card_num1+e.card_num2 as cardNo from events e inner join profile_cards pc on pc.hw_num = e.hw_num and pc.user_num = e.user_num "
				+ " and pc.card_num1 = e.card_num1 and pc.card_num2 = e.card_num2 "
				+ "where e.door_fullid = ? and e.event_msg like '%Normal%' and e.status='0' order by e.date_created desc";
		
		
		
		String nama = "";
		String balance ="";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, machineNo);
			rs =  ps.executeQuery();
			
			while(rs.next()) 
			{
				message = rs.getString("cardNo");
				success = true;
			}
			
			
			
			String ceknameSql = "select user_name, balance from t_accountbalance where ( card_no = ? or nfc_no= ? ) "
					+ "and deleted = 0 and status = '1';";
			
			ps = payConn.prepareStatement(ceknameSql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, message);
			ps.setString(2, message);
			rs2 =  ps.executeQuery();
			
			while(rs2.next())
			{
				nama = rs2.getString("user_name");
				balance = rs2.getString("balance");
			}
			
			
			String insertSql = "EXEC insertlog ?,?";
			
			ps = payConn.prepareStatement(insertSql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, message);
			ps.setString(2, machineNo);
//			try
//			{
//				ps.executeQuery();	
//			}
//			catch(Exception e)
//			{
//				
//			}
			
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			message ="Failed to Perform Transaction, Error : "+e.getMessage(); 
		} finally {
            if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
            if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
            if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			if (connection != null) try { connection.close(); }catch(Exception e){}
			if (payConn != null) try {payConn.close(); }catch (Exception e){}
		}
		
		ret.setSuccess(success);
		ret.message = message +";"+nama+";"+balance;
		return ret;
	}
	
	private static String getCardType(String type){
		String result = "";
		if(TextUtils.isEmpty(type))return result;
		switch(type){
			case "1" :
				result = "card_no";
				break;
			case "2" :
				result = "nfc_no";
				break;
			case "3" :
				result = "card_no_first";
				break;
			case "4" :
				result = "nik";
				break;
			default:
				result = "";
				break;
		}
		return result;
	}
	
	private static int cardCount(String cardNo, String cardType){
		int result = -1;
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = DbConnection.getTrxInstance();
		try{
			String where_by = getCardType(cardType);
			if(TextUtils.isEmpty(where_by))return result;
			String ceknameSql = "SELECT count(accountbalance_id) as jumlah FROM t_accountbalance WHERE " +where_by+" = ? AND deleted = 0 AND STATUS = '1'";
			
			ps = connection.prepareStatement(ceknameSql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, cardNo);
			
			rs =  ps.executeQuery();
			if(rs.next())
			{
				result = rs.getInt("Jumlah");
			}
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}finally{
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
		}
		return result;
	}
	
	public static ResponseStatus fail(ResponseStatus status, String message, Connection connection){
		ResponseStatus result = status == null ? new ResponseStatus() : status;
		result.setSuccess(false);
		result.setMessage(message);
		try{
			if(connection != null)
			{
				if(!connection.getAutoCommit())connection.rollback();
			} 
		}catch(Exception ex){
			result.setMessage(message + "\n " + ex.getMessage());
		}
		return result;
	}
	
	public static ResponseStatus ChangeCardNumber(UpdateCard data) {
		ResponseStatus rst = new ResponseStatus();
		rst.setSuccess(false);
		PreparedStatement ps = null;
		Connection connection = DbConnection.getTrxInstance();
		
		try{
			String typeScan = data.getTypeScan();
			String typeSetIn = data.getTypeSetIn();
			String newCard = data.getNewCard();
			String oldCard = data.getOldCard();
			String where_by = getCardType(typeScan);
			String set_in = getCardType(typeSetIn);
			String message = "";
			int oldCount = cardCount(oldCard, typeScan);
			Boolean isOldOnlyOne = oldCount == 1;
			Boolean validated = !(TextUtils.isEmpty(where_by) || TextUtils.isEmpty(set_in)) && isOldOnlyOne;
			if(validated){
				int newCount = cardCount(newCard, typeSetIn);
				if(newCount >= 1){
					message = "Tidak dapat update, kartu baru sudah terdaftar.";
					rst = fail(rst, message, connection);
				}else if(newCount==0){
					String sql2 = "UPDATE t_accountbalance SET "+set_in+" = ?  WHERE "+where_by+" = ? AND deleted = 0 AND STATUS = '1'";
					ps = connection.prepareStatement(sql2);
					ps.setEscapeProcessing(true);
					ps.setQueryTimeout(5000);					
					ps.setString(1, newCard);
					ps.setString(2, oldCard);
					ps.execute();			
					
					rst.setSuccess(true);
					rst.setMessage("berhasil"); 
					
					connection.commit();
				}else{
					message = "Jumlah kartu baru tidak valid.";
					rst = fail(rst, message, connection);
				}
			}else{
				message = !isOldOnlyOne ? "Jumlah kartu lama tidak valid {"+oldCount+"}." : "Pastikan tipe scan/set kartu valid.";
				rst = fail(rst, message, connection);
			}
			
		}

		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
		}	
		return rst;
	}
	
	
	
	public static ResponseStatus GetCardDetailInfo(String cardNo, String cardType) {
		ResponseStatus rst = new ResponseStatus();
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = DbConnection.getTrxInstance();
		Boolean success = false;
		
		String where_by = getCardType(cardType);
		
		String nama = "";
		String balance = "";
		String message="";
		String cardNoFirst="";
		String nfcNo = "";
		Boolean isHaveNewCard = false;
		Boolean validated = !TextUtils.isEmpty(where_by);
		if(validated){
			try{
				String ceknameSql = "select COUNT(1) OVER (PARTITION BY "+where_by+") AS Jumlah, user_name, balance, card_no, card_no_first, case when card_no_first is null then convert(bit, 0) else convert(bit, 1) end as IsHaveNewCard, nfc_no from t_accountbalance where "
						+ "deleted = 0 and status = '1' and "+ where_by +" = ?;";
				
				ps = connection.prepareStatement(ceknameSql);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				ps.setString(1, cardNo);
				rs =  ps.executeQuery();
				
				message = "data tidak ada, pastikan kartu terdaftar atau hubungi IT Support.";	
				if(rs.next() && rs.getInt("Jumlah")==1){
					nama = rs.getString("user_name");
					balance = rs.getString("balance");
					cardNo = rs.getString("card_no");
					cardNoFirst = rs.getString("card_no_first");
					nfcNo = rs.getString("nfc_no");
					isHaveNewCard = rs.getBoolean("IsHaveNewCard");
					success = true;
					message = "Berhasil mendapatkan data.";
				}else{
					rst.setMessage(message);
				}
			}

			catch(SQLException e){
				e.fillInStackTrace();
				System.out.println(e.getMessage());
				if(connection != null){
					try{
						System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
						connection.rollback();}
					catch(SQLException excep){
						System.out.println(excep.getMessage());
					}
				}
			}
			catch (Exception e) {
				System.out.println(e.getMessage());
				message ="Failed to Perform Transaction, Error : "+e.getMessage(); 
				if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
			}
			finally {
				try {connection.setAutoCommit(true); } catch (SQLException e) {}
		 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
		 		if (rs != null)try{rs.close();}catch (SQLException e) {}
		 		if (ps != null)try{ps.close();}catch (SQLException e) {}
			}
		}else{
			success = false;
			message = "Tipe kartu tidak sesuai.";
		}

		EmoneyCardDatailInfo data = new EmoneyCardDatailInfo();
		data.setCardNo(cardNo);
		data.setCardNoFirst(cardNoFirst);
		data.setNfcNo(nfcNo);
		data.setBalance(balance);
		data.setName(nama);
		data.setIsHaveNewCard(isHaveNewCard);
		
		rst.setSuccess(success);
		rst.setMessage(message);
		rst.setData(data);
		return rst;
	}
}
