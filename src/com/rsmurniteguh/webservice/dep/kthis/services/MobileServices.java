package com.rsmurniteguh.webservice.dep.kthis.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.rsmurniteguh.webservice.dep.all.model.mobile.InfoPatient;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResourceList;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.mobile.UserList;
import com.rsmurniteguh.webservice.dep.all.model.mobile.listCardNoBpjs;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInfoResponse;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.EncryptPatient;
import com.rsmurniteguh.webservice.dep.util.Encryptor;

public class MobileServices extends DbConnection {
    
	public static List<UserList> UserGet(String username)
	{
		List<UserList> data = new ArrayList<UserList>();
		
		Connection connection = null;
		connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		ResultSet rs = null;
		String sql="select user_id, no_mrn, username, password from tb_user u where u.username = ?  "
				+ " order by u.user_id ";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, username);
			rs=ps.executeQuery();
			
			while(rs.next())
			{
				UserList dl=new UserList();
				dl.setMrn(rs.getString("mrn"));
				//dl.setTgl_lahir(rs.getString("tgl_lahir"));
				dl.setUsername(rs.getString("username"));
				dl.setPassword(rs.getString("password"));
				data.add(dl);
			}
		
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return data;
		
	}
	
	public static ResponseString UserCreate(String mrn, String username, String password,String tgl_lahir) {
		System.out.println("UserCreate...");
		
		ResponseString res = new ResponseString();
		
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		
		String sql = "INSERT tb_user (no_mrn,username,password,created,fullname,no_bpjs) values (?,?,?,SYSDATETIME(),?,?)";
		String encrypt = EncryptPatient.getInstance().encryptOnlineAppointment(Encryptor.getInstance().decrypt(password));
		
		PreparedStatement ps = null;
		try {
			listCardNoBpjs lsnb = MobileAppointment.getCardNo(mrn);
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, mrn);
			ps.setString(2, username);
			ps.setString(3, encrypt);
			ps.setString(4, lsnb.getPERSON_NAME());
			ps.setString(5, lsnb.getBPJS_NO());
			ps.execute();
			
			res.setResponse("OK");
		}
		
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
			res.setResponse("FAIL");
		}
		finally
		{
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return res;
	}
	
	public static ResponseString UserDelete(String username) {
		ResponseString res = new ResponseString();
		Connection connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		String SPsql = "update tb_user set deleted = 1 where username = ? and deleted = 0";
		PreparedStatement ps = null;;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, username);
			
			ps.executeUpdate();
			res.setResponse("OK");
		}
		
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
			res.setResponse("FAIL");
		}
		finally
		{
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return res;
	}
	
	public static ResponseString UserUpdate(String mrn,String username,String password,String tgl_lahir, String token) {
		System.out.println("UserUpdate...");
		ResponseString res = new ResponseString();
		Connection connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		String SPsql = "update tb_user set no_mrn = ? ,password = ?, token_forgotpass = ''  where username = ?";
		EncryptPatient.getInstance();
		String encrypt = EncryptPatient.encryptOnlineAppointment(Encryptor.getInstance().decrypt(password));
		PreparedStatement ps = null;;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, mrn);
			ps.setString(2, encrypt);
			ps.setString(3, username);
			
			ps.executeUpdate();
			res.setResponse("OK");
		}
		
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
			res.setResponse("FAIL");
		}
		finally
		{
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return res;
	}
	
	public static List<ResourceList> SearchDoctor(String searchtext, String lokasi) {
		System.out.println("SearchDoctor...");
		List<ResourceList> data = new ArrayList<ResourceList>();
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		connection = DbConnection.getPooledConnection();
		if(connection == null)return null;
		
		String filterDoctor = "";
		if(searchtext != "") filterDoctor = " AND LOWER(PER.PERSON_NAME) LIKE LOWER(?) ";
		
		/*
		 * CUSTOME HARD CODE FOR 'HEMODIALISA' AND 'REKAP MEDIK'
		 */
		String subspecialt_custom = MobileBiz.CUSTOM_SUBSPECIALTYMSTR_ID_BPJS ;
		if(lokasi.equals(MobileBiz.POLI.toString())) {
			subspecialt_custom = MobileBiz.CUSTOM_SUBSPECIALTYMSTR_ID_GENERAL;
		}
		
		String sql="SELECT NULL RESOURCESCHEME_ID, "
				+ " (SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR "
				+ " WHERE SUBSPECIALTY_CODE = RM.POLI_CODE AND DEFUNCT_IND='N' ) POLI_CODE, "
				+ "RM.POLI_CODE SUBSPECIALTY_CODE, "
				+ "RM.RESOURCEMSTR_ID, "
				+ "RM.RESOURCE_CODE, "
				+ "RM.RESOURCE_SHORT_CODE, "
				+ "RM.RESOURCE_QUICK_CODE, "
				+ "PER.PERSON_NAME RESOURCE_NAME, "
				+ "RM.RESOURCE_NAME_LANG1, "
				+ "RM.REGISTRATION_TYPE, "
				+ "RM.SUBSPECIALTYMSTR_ID, " 
				+ "SSPM.SUBSPECIALTY_DESC, "
				+ "SSPM.SUBSPECIALTY_DESC_LANG1, " 
				+ "RM.CAREPROVIDER_ID "
				+ "FROM RESOURCEMSTR RM, SUBSPECIALTYMSTR SSPM, PERSON PER, CAREPROVIDER CP " 
				+ "WHERE RM.SUBSPECIALTYMSTR_ID = SSPM.SUBSPECIALTYMSTR_ID "
				+ "AND CP.CAREPROVIDER_ID = RM.CAREPROVIDER_ID "
				+ "AND CP.PERSON_ID = PER.PERSON_ID "
				+ "AND exists(select 1 from RESOURCEPROFILE rp where RM.RESOURCEMSTR_ID = rp.resourcemstr_id AND (rp.EXPIRY_DATE > SYSDATE or rp.EXPIRY_DATE is null)) "
				+ "AND RM.RESOURCE_SCHEME_IND = 'N' "
				+ "AND (SSPM.SUBSPECIALTYMSTR_ID = ? OR SSPM.SUBSPECIALTYMSTR_ID IN("+subspecialt_custom+")) "
				+ "AND RM.DEFUNCT_IND = 'N' " + filterDoctor ;
		try {
			st=connection.prepareStatement(sql);
			st.setBigDecimal(1, new BigDecimal(lokasi));
			if(searchtext!="") st.setString(2, "%"+searchtext+"%");
			rs=st.executeQuery();
			while(rs.next())
				{	
					ResourceList dl=new ResourceList();
					dl.setResourcescheme_id(rs.getString("RESOURCESCHEME_ID"));
					dl.setResource_name(rs.getString("RESOURCE_NAME"));
					dl.setPoli_code(rs.getString("SUBSPECIALTY_CODE"));
					dl.setResourcemstr_id(rs.getBigDecimal("RESOURCEMSTR_ID"));
					dl.setCareprovider_id(rs.getBigDecimal("CAREPROVIDER_ID"));
					//dl.setPoli_name(rs.getString("POLI_NAME"));
					data.add(dl);
				}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (st!=null) try  { st.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return data;
		
		
	}

	public static InfoPatient getInfoPatient(String mrn, String tgl_lahir,String username) {
		System.out.println("getInfoPatient...");
		//System.out.println(mrn+"|"+tgl_lahir+"|"+username);
		InfoPatient data = new InfoPatient();
		Connection connection = null;
		   ResultSet rs = null;
		   ResultSet rs2 = null;
		   ResultSet rs3 = null;
		   ResultSet rs4 = null;
		connection = DbConnection.getPooledConnection();
		Connection connection2=DbConnection.getBpjsOnlineInstance();
		if(connection == null)return null;
		if(connection2 == null)return null;
		
		String sql="SELECT p.person_name, to_char(p.birth_date,'YYYY-MM-DD') as birth_date, p.sex, c.card_no " + 
				"FROM person p " + 
				"     INNER JOIN card c ON c.person_id = p.person_id "
				+ "WHERE c.card_no = ? AND p.birth_date = TO_DATE(?, 'YYYY-MM-DD')";
		
		String sql2 = "SELECT username FROM tb_user WHERE username = ? ";
		String sql3 = "SELECT count(no_mrn) count FROM tb_user WHERE no_mrn = ? ";
		String sql4 = "SELECT no_bpjs FROM tb_user WHERE no_mrn = ?";

		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		try {
			ps =connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, mrn);
			ps.setString(2, tgl_lahir);
			rs=ps.executeQuery();
			
			while(rs.next())
			{
				data.setMrn(rs.getString("CARD_NO"));
				data.setNamaPasien(rs.getString("PERSON_NAME"));
				data.setSex(rs.getString("SEX"));
				data.setTglLahir(rs.getString("BIRTH_DATE"));
			}
			
			listCardNoBpjs lsnb = MobileAppointment.getCardNo(mrn);
			data.setBpjs_no(lsnb.getBPJS_NO());

			ps2 = connection2.prepareStatement(sql2);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(60000);
			ps2.setString(1, username);
			rs2 = ps2.executeQuery();
			
			if(rs2.next())
			{
				data.setUsername(rs2.getString("username"));
			}
			
			
			ps3 = connection2.prepareStatement(sql3);
			ps3.setEscapeProcessing(true);
			ps3.setQueryTimeout(60000);
			ps3.setString(1, mrn);
			rs3 = ps3.executeQuery();
	
			if(rs3.next())
			{
				data.setCount(rs3.getInt("count"));
			}
			
			ps4 = connection2.prepareStatement(sql4);
			ps4.setEscapeProcessing(true);
			ps4.setQueryTimeout(60000);
			ps4.setString(1, mrn);
			rs4 = ps4.executeQuery();
			
			if(rs4.next()){
				data.setBpjs_no(rs4.getString("no_bpjs"));
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}

		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		     if (rs3!=null) try  { rs3.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		     if (connection2!=null) try { connection2.close();} catch (Exception ignore){}
		   }
		return data;
	}

	public static Object confirmPatient(String mrn, String tgl_lahir, String pass) {
		System.out.println("confirmPatient...");
		InfoPatient data = new InfoPatient();
		Connection connection = null;
		   ResultSet rs = null;
		   ResultSet rs2 = null;
		connection = DbConnection.getPooledConnection();
		Connection connection2=DbConnection.getBpjsOnlineInstance();
		if(connection == null)return null;
		if(connection2 == null)return null;
		
		String sql="SELECT p.person_name, to_char(p.birth_date,'YYYY-MM-DD') as birth_date, p.sex, c.card_no " + 
				"FROM person p " + 
				"     INNER JOIN card c ON c.person_id = p.person_id "
				+ "WHERE c.card_no = ? AND p.birth_date = TO_DATE(?, 'YYYY-MM-DD')";

		String sql2 = "SELECT username, password, SYSDATETIME() as waktu FROM tb_user WHERE no_mrn = ? ";
		String sql3 = "UPDATE tb_user SET token_forgotpass = ? WHERE username = ? ";

		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		try {
			ps =connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, mrn);
			ps.setString(2, tgl_lahir);
			rs=ps.executeQuery();
			
			if(rs.next())
			{
				data.setMrn(rs.getString("CARD_NO"));
				data.setNamaPasien(rs.getString("PERSON_NAME"));
				data.setSex(rs.getString("SEX"));
				data.setTglLahir(rs.getString("BIRTH_DATE"));
			}
			
			ps2 = connection2.prepareStatement(sql2);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(60000);
			ps2.setString(1, mrn);
			rs2 = ps2.executeQuery();
			
			if(rs2.next())
			{
				
				String stoken = rs2.getString("username").toString()+rs2.getString("waktu").toString();
				String token = EncryptPatient.encryptOnlineAppointment(stoken);
				
				data.setUsername(rs2.getString("username"));
				data.setToken(token);
				
				/*
				 * VALIDATION PASSWORD IS SAME?
				 */
				String newpass = EncryptPatient.encryptOnlineAppointment(Encryptor.getInstance().decrypt(pass));
				if (rs2.getString("password").equals(newpass)) data.setSamePass(true);
				
				ps3 = connection2.prepareStatement(sql3);
				ps3.setEscapeProcessing(true);
				ps3.setQueryTimeout(60000);
				ps3.setString(1, token);
				ps3.setString(2, rs2.getString("username"));
				ps3.executeUpdate();
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}

		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		     if (connection2!=null) try { connection2.close();} catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return data;
	}

	public static ResponseString notificationAppointment(String tanggal) {
		System.out.println("notificationAppointment...");
		ResponseString res = new ResponseString();
		Connection connection = null;
		connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		ResultSet rs = null;
		String sql="SELECT r.queue_date, r.user_id,r.doctor_name, token.firebase_token " + 
				"   FROM tb_registrasi r " + 
				"    INNER JOIN (select Row_number() OVER (partition by t.usermstr_id order by t.created_at desc) as num, t.usermstr_id, t.firebase_token,t.created_at from usertoken t where t.firebase_token is not null ) token ON r.user_id = token.usermstr_id and num = 1 " + 
				"	WHERE r.queue_date = ? AND r.status = 'Confirm' " + 
				"	GROUP BY r.queue_date,  r.user_id, r.doctor_name, token.firebase_token ";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tanggal);
			rs=ps.executeQuery();
			
			
			while(rs.next())
			{
				try {
					
					URL url = new URL(MobileBiz.FMCurl);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
					conn.setUseCaches(false);
					conn.setDoInput(true);
					conn.setDoOutput(true);
			
					conn.setRequestMethod("POST");
					conn.setRequestProperty("Authorization", "key=" + MobileBiz.FMKey);
					conn.setRequestProperty("Content-Type", "application/json");
			
					JSONObject data = new JSONObject();
					data.put("to", rs.getString("firebase_token").trim());
					JSONObject info = new JSONObject();
					info.put("title", "Appointment Reminder"); // Notification title
					info.put("body", "Dont forget to visit "+rs.getString("doctor_name")+" today"); // Notification body
					info.put("activity", "appointmenthistory");
					data.put("data", info);
			
					OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
					wr.write(data.toString());
					wr.flush();
					wr.close();
			
					int responseCode = conn.getResponseCode();
					//System.out.println("Response Code : " + responseCode);
			
					BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();
			
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
	
					res.setResponse(response.toString());
					//System.out.println(response.toString());
				} catch (Exception e) {
					res.setResponse(e.getMessage());
				}
				
			}
		
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return res;
	}

	public static BpjsInfoResponse getBPJSInfo(String noKTP){
		String ret = BpjsService2.GetBpjsInfobyNoka(noKTP);
        Gson gson = new Gson();
        BpjsInfoResponse memberInfo = gson.fromJson(ret, BpjsInfoResponse.class); 
        return memberInfo;
	}
}
