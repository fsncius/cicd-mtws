package com.rsmurniteguh.webservice.dep.kthis.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Convert;

import com.rsmurniteguh.webservice.dep.all.model.ListBedHistory;
import com.rsmurniteguh.webservice.dep.all.model.ListConDoc;
import com.rsmurniteguh.webservice.dep.all.model.ListDataFamily;
import com.rsmurniteguh.webservice.dep.all.model.ListDataRgPasien;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoData;
import com.rsmurniteguh.webservice.dep.all.model.PansienInfo;
import com.rsmurniteguh.webservice.dep.all.model.PasienDok;
import com.rsmurniteguh.webservice.dep.biz.ListVisPasDok;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class VisitDocService extends DbConnection {

	public static List<PasienDok> getVisitDoctor(String dadok) {
		List<PasienDok> all = new ArrayList<PasienDok>();
		Statement stmt = null;
		Statement ambi=null;
		ResultSet rs =null;
		ResultSet rso=null;
		
		Connection connection = DbConnection.getPooledConnection();
			try 
		    	{
					
					if(connection == null)return null;
				    String query = "select distinct v.visit_id, c.card_no, p.person_name from visit v, visitdoctor vd, patient pt, person p, card c "
			    		+ "where v.visit_id=vd.visit_id and v.patient_id=pt.patient_id and pt.person_id=p.person_id and c.person_id=p.person_id and "
			    		+ "vd.careprovider_id="+dadok+" and v.patient_type='PTY1' and v.admit_status='AST1'";

			    
 			        stmt = connection.createStatement();
			        rs = stmt.executeQuery(query);
			        while (rs.next())
			        {
			        	PasienDok pd = new PasienDok();
			        	pd.setVisitid(rs.getBigDecimal("visit_id"));
			        	BigDecimal vispas=rs.getBigDecimal("visit_id");
			        	pd.setCardno(rs.getString("card_no"));
			        	pd.setPersonname(rs.getString("person_name"));
			        	
			        	String ambque="select * from (select wm.ward_desc,bm.Bed_No,bh.visit_id from wardmstr wm, roommstr rm, bedmstr bm, bedhistory bh "
			        			+ "where wm.wardmstr_id=rm.wardmstr_id and rm.roommstr_id = bm.roommstr_id and bm.bedmstr_id=bh.bedmstr_id and "
			        			+ "bh.visit_id="+vispas+" order by bh.effective_start_datetime desc) where ROWNUM <= 1 ";
			        	ambi=connection.createStatement();
			        	rso=ambi.executeQuery(ambque);
			        	while(rso.next())
			        	{
				        	pd.setWardds(rso.getString("ward_desc"));
				        	pd.setBedno(rso.getString("Bed_No"));
				        	
			        	}
			        	rso.close();
			        	ambi.close();
                                    all.add(pd);
			        }
			        rs.close();
			        stmt.close();
		    	}
		    catch (SQLException e ) 
		    	{
		        	System.out.println(e.getMessage());
		    	} 
			finally {
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (ambi!=null) try  { ambi.close(); } catch (Exception ignore){}
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (rso!=null) try  { rso.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		return all;
	}

	public static List<PansienInfo> getpasieninfo(String mrn) {
		List<PansienInfo> all = new ArrayList<PansienInfo>();
		Statement stmt = null;
		Statement stmt2 = null;
		Statement stmt3 = null;
		Statement stmt5=null;
		ResultSet rs =null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet row5=null;
		Connection connection = DbConnection.getPooledConnection();
			try 
		    	{
					
					if(connection == null)return null;
				    String query = "select PS.PERSON_NAME, PS.SEX, PS.BIRTH_DATE,  PS.NATIONALITY ,PS.MOBILE_PHONE_NO, PS.MARITAL_STATUS, ADDR.ADDRESS_1, VS.VISIT_ID "
				    		+ "from MEDICALRECORDNO MRN, PATIENT PT, PERSON PS, ADDRESS ADDR, VISIT VS "
				    		+ "where SUBSTR(MRN.MEDICAL_RECORD_NO,3,12)='"+mrn+"' and SUBSTR(MRN.MEDICAL_RECORD_NO,1,2)='OP' and PT.PATIENT_ID=MRN.PATIENT_ID and PS.PERSON_ID=PT.PERSON_ID AND ADDR.STAKEHOLDER_ID=PS.STAKEHOLDER_ID "
				    		+ "AND VS.PATIENT_ID=PT.PATIENT_ID "
				    		+ "ORDER BY VS.VISIT_ID DESC";
			        
 			        stmt = connection.createStatement();
			        rs = stmt.executeQuery(query);
			        rs.next();	
			        String Vid = rs.getString("VISIT_ID");
			        PansienInfo ps = new PansienInfo();
		        	ps.setPerson_Name(rs.getString("PERSON_NAME"));
		        	ps.setSex(rs.getString("SEX"));
		        	ps.setBirthDate(rs.getString("BIRTH_DATE"));
		        	ps.setNationality(rs.getString("NATIONALITY"));
		        	ps.setMobilePhone(rs.getString("MOBILE_PHONE_NO"));
		        	ps.setMarital_Status(rs.getString("MARITAL_STATUS"));
		        	ps.setAddress(rs.getString("ADDRESS_1"));
		        	ps.setVisitId(Vid);
					
		        	String query2 = "select count(*) htg from (select wm.ward_desc,bm.Bed_No,bh.visit_id from wardmstr wm, roommstr rm, bedmstr bm, bedhistory bh "
							+ "where wm.wardmstr_id=rm.wardmstr_id "
							+ " and rm.roommstr_id = bm.roommstr_id and bm.bedmstr_id=bh.bedmstr_id "
							+ "and bh.visit_id='"+Vid+"' order by bh.effective_start_datetime desc)";
					
					stmt2 = connection.createStatement();
					rs2 = stmt2.executeQuery(query2);
					rs2.next();
					String tung = rs2.getString("htg");
					if(tung.equals("0"))
					{
			        	String query5="SELECT SBS.SUBSPECIALTY_DESC FROM VISIT V,SUBSPECIALTYMSTR SBS "
								+ "WHERE V.SUBSPECIALTYMSTR_ID=SBS.SUBSPECIALTYMSTR_ID AND V.VISIT_ID="+Vid+"";
						stmt5 = connection.createStatement();
						row5= stmt5.executeQuery(query5);
						row5.next();
		
						ps.setWard_Desc(row5.getString("SUBSPECIALTY_DESC"));
			        	ps.setBed_No("-");
			        	
					}
					else
					{
						String query3 = "select * from (select wm.ward_desc,bm.Bed_No,bh.visit_id from wardmstr wm, roommstr rm, bedmstr bm, bedhistory bh "
								+ "where wm.wardmstr_id=rm.wardmstr_id "
								+ " and rm.roommstr_id = bm.roommstr_id and bm.bedmstr_id=bh.bedmstr_id "
								+ "and bh.visit_id='"+Vid+"' order by bh.effective_start_datetime desc)";
						
						stmt3 = connection.createStatement();
						rs3 = stmt3.executeQuery(query3);		
						rs3.next();
						ps.setWard_Desc(rs3.getString("Ward_Desc"));
			        	ps.setBed_No(rs3.getString("Bed_No"));		
					
					}
					all.add(ps);			        
			        rs.close();
			        rs2.close();
			    	stmt.close();
			        stmt2.close();
		    	}
		    catch (SQLException e ) 
		    	{
			    	PansienInfo ps = new PansienInfo();
		        	ps.setPerson_Name("-");
		        	ps.setSex("-");
		        	ps.setBirthDate("-");
		        	ps.setNationality("-");
		        	ps.setMobilePhone("-");
		        	ps.setMarital_Status("-");
		        	ps.setAddress("-");
		        	ps.setVisitId("-");
		        	ps.setWard_Desc("-");
		        	ps.setBed_No("-");	
		        	all.add(ps);	
		        	//System.out.println(e.getMessage());
		    	} 
			finally {
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		return all;
	}

	public static List<ListVisPasDok> getinvispasdok(String mrnaja) {
		List<ListVisPasDok> all = new ArrayList<ListVisPasDok>();
		Statement stmt = null;
		ResultSet rs =null;
		Connection connection = DbConnection.getPooledConnection();
			try 
		    	{
					
					if(connection == null)return null;
				    String query = "select distinct cd.CARD_NO, ps.PERSON_NAME, ps.SEX, ps.BIRTH_DATE, vs.VISIT_ID, vs.ADMISSION_DATETIME,(SELECT WS.WARD_DESC FROM WARDMSTR WS WHERE WS.WARDMSTR_ID=vs.WARDMSTR_ID) WARD_DESC, "
				    		+ " vs.PATIENT_TYPE, (select person_name from person where person_id in "
				    		+ " (case  vs.patient_type when 'PTY1' then"
				    		+ " (select cp.person_id from careprovider cp inner join visitdoctor vd on vd.careprovider_id = cp.careprovider_id "
				    		+ " where vd.visit_id = vs.visit_id  "
				    		+ " and vd.doctor_type = 'DTYDR' and vd.defunct_ind = 'N' and rownum = 1) "
				    		+ " when 'PTY2' then "
				    		+ " (select cp.person_id from careprovider cp inner join visitregntype vd on vd.careprovider_id = cp.careprovider_id"
				    		+ " where vd.visit_id = vs.visit_id "
				    		+ "and vd.defunct_ind = 'N' and rownum = 1) "
				    		+ " else null end)) as Dok "
				    		+ "from CARD cd, PERSON ps, PATIENT pt, VISIT vs "
				    		+ "where cd.CARD_NO='"+mrnaja+"' and ps.PERSON_ID=cd.PERSON_ID  "
				    		+ "and pt.PERSON_ID=ps.PERSON_ID and vs.PATIENT_ID=pt.PATIENT_ID  "
				    		+ "order by vs.VISIT_ID desc";
			    
 			        stmt = connection.createStatement();
			        rs = stmt.executeQuery(query);
			        while(rs.next())
			        {
			        	ListVisPasDok lvp = new ListVisPasDok();
			        	lvp.setCardNo(rs.getString("CARD_NO"));
			        	lvp.setPersonName(rs.getString("PERSON_NAME"));
			        	lvp.setSex(rs.getString("SEX"));
			        	lvp.setBirthDate(rs.getDate("BIRTH_DATE"));
			        	lvp.setVisitId(rs.getLong("VISIT_ID"));
			        	lvp.setAdmisDate(rs.getDate("ADMISSION_DATETIME"));
			        	lvp.setPatientType(rs.getString("PATIENT_TYPE"));
			        	lvp.setPersonNameDokter(rs.getString("Dok"));
			        	lvp.setPatientWard(rs.getString("WARD_DESC"));
			        	all.add(lvp);
			        }
			        	rs.close();
			        	stmt.close();
		    	}
		    catch (SQLException e ) 
		    	{
		        	System.out.println(e.getMessage());
		    	} 
			finally {
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		return all;
	}

	public static List<ListConDoc> getConDoc(String tglcek) {
		List<ListConDoc> all = new ArrayList<ListConDoc>();
		Statement stmt = null;
		Statement stmbed=null;
		ResultSet rs=null;
		Connection connection = DbConnection.getPooledConnection();
			try 
		    	{					
					if(connection == null)return null;
				    String query = "SELECT PS.PERSON_NAME AS PATIENT_NAME,PT.PATIENT_ID, VT.VISIT_ID, to_char(MR.CONTROL_DATETIME,'dd-mm-yyyy') as tgcon, MR.CAREPROVIDER_ID, "
				    		+ "PSS.PERSON_NAME AS DOCTOR_NAME, RSM.RESOURCEMSTR_ID, PS.MOBILE_PHONE_NO, CD.CARD_NO "
				    		+ "FROM CARD CD, PERSON PS, PATIENT PT, VISIT VT, MEDICAL_RESUME MR, CAREPROVIDER CP, PERSON PSS, RESOURCEMSTR RSM "
				    		+ "WHERE to_char(VT.ADMISSION_DATETIME, 'ddmmyyyy') = '"+tglcek+"'  AND PS.PERSON_ID=CD.PERSON_ID AND "
				    		+ "PT.PERSON_ID=PS.PERSON_ID AND VT.PATIENT_ID=PT.PATIENT_ID AND MR.VISIT_ID=VT.VISIT_ID AND "
				    		+ "CP.CAREPROVIDER_ID=MR.CAREPROVIDER_ID AND PSS.PERSON_ID=CP.PERSON_ID and VT.PATIENT_TYPE = 'PTY2' and "
				    		+ "VT.PATIENT_CLASS ='PTC114' and RSM.CAREPROVIDER_ID=CP.CAREPROVIDER_ID AND RSM.SUBSPECIALTYMSTR_ID=336581902 and "
				    		+ "CD.PERSON_ID=PS.PERSON_ID AND MR.CONTROL_DATETIME IS NOT NULL";
			    
 			        stmt = connection.createStatement();
			        rs = stmt.executeQuery(query);
			        while(rs.next())
			        {
			        	ListConDoc lvp = new ListConDoc();
			        	lvp.setPATIENT_NAME(rs.getString("PATIENT_NAME"));
			        	lvp.setPATIENT_ID(rs.getString("PATIENT_ID"));
			        	lvp.setVISIT_ID(rs.getString("VISIT_ID"));
			        	lvp.setCONTROL_DATETIME(rs.getString("tgcon"));
			        	lvp.setCAREPROVIDER_ID(rs.getString("CAREPROVIDER_ID"));
			        	lvp.setDOCTOR_NAME(rs.getString("DOCTOR_NAME"));
			        	lvp.setRESOURCEMSTR_ID(rs.getString("RESOURCEMSTR_ID"));
			        	lvp.setMOBILE_PHONE_NO(rs.getString("MOBILE_PHONE_NO"));
			        	lvp.setCARD_NO(rs.getString("CARD_NO"));
			        	all.add(lvp);
			        }
			       
			        
			        	rs.close();
			        	stmt.close();
		    	}
		    catch (SQLException e ) 
		    	{
		    	
		        	System.out.println(e.getMessage());
		    	} 
			
			finally {
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		return all;
	}

	public static List<ListInfoData> getInfoData(String mrn) {
		List<ListInfoData> all = new ArrayList<ListInfoData>();
		Statement stmt = null;
				
		ResultSet rs=null;
		
		Connection connection = DbConnection.getPooledConnection();
			try 
		    	{					
					if(connection == null)return null;
				    String query = "SELECT * FROM (SELECT PRS.PERSON_NAME, PGCOMMON.FXGETCODEDESC(PRS.SEX) as SEX, PRS.BIRTH_DATE, PRS.ID_NO, "
				    		+ "PRS.MOBILE_PHONE_NO, PRS.COMPANY_NAME, PRS.NIK, PTN.BPJS_NO, PGCOMMON.FXGETCODEDESC(PTN.PATIENT_CLASS) AS PASIEN_CLASS, VST.VISIT_ID, "
				    		+ "VST.ADMISSION_DATETIME, VST.DISCHARGE_DATETIME, PRS.PERSON_ID "
				    		+ "FROM CARD CD, PERSON PRS, PATIENT PTN, VISIT VST "
				    		+ "WHERE CD.CARD_NO="+mrn+" AND PRS.PERSON_ID=CD.PERSON_ID AND PTN.PERSON_ID=PRS.PERSON_ID AND "
				    		+ "VST.PATIENT_ID=PTN.PATIENT_ID ORDER BY VST.VISIT_ID DESC) WHERE ROWNUM<=1";
			    
 			        stmt = connection.createStatement();
			        rs = stmt.executeQuery(query);
			        rs.next();
			        
			        	ListInfoData lvp = new ListInfoData();
			        	
			        	lvp.setPERSON_NAME(rs.getString("PERSON_NAME"));
			        	lvp.setSEX(rs.getString("SEX"));
			        	lvp.setBIRTH_DATE(rs.getString("BIRTH_DATE"));
			        	lvp.setID_NO(rs.getString("ID_NO"));
			        	lvp.setMOBILE_PHONE_NO(rs.getString("MOBILE_PHONE_NO"));
			        	lvp.setCOMPANY_NAME(rs.getString("COMPANY_NAME"));
			        	lvp.setNIK(rs.getString("NIK"));
			        	lvp.setBPJS_NO(rs.getString("BPJS_NO"));
			        	lvp.setPASIEN_CLASS(rs.getString("PASIEN_CLASS"));
			        	lvp.setVISIT_ID(rs.getString("VISIT_ID"));
			        	lvp.setADMISSION_DATETIME(rs.getString("ADMISSION_DATETIME"));
			        	lvp.setDISCHARGE_DATETIME(rs.getString("DISCHARGE_DATETIME"));
			        	lvp.setPERSON_ID(rs.getString("PERSON_ID"));
			        	all.add(lvp);
		        	rs.close();
		        	stmt.close();
		    	}
		    catch (SQLException e ) 
		    	{
		        	System.out.println(e.getMessage());
		    	} 
			
			finally {
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		return all;
	}

	public static List<ListBedHistory> getBedHistory(String visitid) {
		List<ListBedHistory> all = new ArrayList<ListBedHistory>();
		Statement stmt2=null;
		
		ResultSet rs2=null;
		Connection connection = DbConnection.getPooledConnection();
			try 
		    	{					
					if(connection == null)return null;
				    
			        String query2="SELECT * FROM (SELECT BM.BED_NO, RM.ROOM_DESC, WM.WARD_DESC "
			        		+ "FROM VISIT VST, BEDHISTORY BH, BEDMSTR BM, ROOMMSTR RM,  WARDMSTR WM "
			        		+ "WHERE BH.VISIT_ID='"+visitid+"' AND BM.BEDMSTR_ID=BH.BEDMSTR_ID AND "
			        		+ "RM.ROOMMSTR_ID=BM.ROOMMSTR_ID AND WM.WARDMSTR_ID=RM.WARDMSTR_ID  ORDER BY VST.VISIT_ID DESC) WHERE ROWNUM<=1";
			        stmt2=connection.createStatement();
			        rs2=stmt2.executeQuery(query2);
			        rs2.next();
			        	ListBedHistory lvp = new ListBedHistory();
			        	lvp.setBED_NO(rs2.getString("BED_NO"));
			        	lvp.setROOM_DESC(rs2.getString("ROOM_DESC"));
			        	lvp.setWARD_DESC(rs2.getString("WARD_DESC"));
			        	all.add(lvp);
		        	rs2.close();
		        	stmt2.close();	 
			        
		    	}
		    catch (SQLException e ) 
		    	{
		        	System.out.println(e.getMessage());
		    	} 
			
			finally {
			    if (stmt2!=null) try  { stmt2.close(); } catch (Exception ignore){}
			     
			     if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		return all;
	}

	public static List<ListDataFamily> getListFamily(String personid) {
		List<ListDataFamily> all = new ArrayList<ListDataFamily>();
		Statement stmt3=null;
		ResultSet rs3=null;
		Connection connection = DbConnection.getPooledConnection();
			try 
		    	{					
					if(connection == null)return null;
				     	
			        String query3="SELECT PS.PERSON_NAME AS FAMILY_NAME, PS.BIRTH_DATE AS BIRTH_DATE_FAMILY, PS.MOBILE_PHONE_NO AS MOBILE_PHONE_FAMILY, PGCOMMON.FXGETCODEDESC(NK.RELATIONSHIP) AS HUBUNGAN_KELUARGA "
			        		+ "FROM NOK NK, PERSON PS "
			        		+ "WHERE NK.EMERGENCY_CONTACT_IND='Y' AND NK.NOK_PERSON_ID='"+personid+"' AND PS.PERSON_ID=NK.PERSON_ID "
			        		+ "ORDER BY NK.PERSON_ID DESC";
			        stmt3=connection.createStatement();
			        rs3=stmt3.executeQuery(query3);
			        rs3.next();
			        	ListDataFamily lvp = new ListDataFamily();
		        	
			        	lvp.setFAMILY_NAME(rs3.getString("FAMILY_NAME"));
			        	lvp.setBIRTH_DATE_FAMILY(rs3.getString("BIRTH_DATE_FAMILY"));
			        	lvp.setMOBILE_PHONE_FAMILY(rs3.getString("MOBILE_PHONE_FAMILY"));
			        	lvp.setHUBUNGAN_KELUARGA(rs3.getString("HUBUNGAN_KELUARGA"));
			        	all.add(lvp);
			        	
			        rs3.close();
			        stmt3.close();
		    	}
		    catch (SQLException e ) 
		    	{
		        	System.out.println(e.getMessage());
		    	} 			
			finally {
			     if (stmt3!=null) try  { stmt3.close(); } catch (Exception ignore){}			     
			     if (rs3!=null) try  { rs3.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		return all;
	}

	public static List<ListDataRgPasien> getDataRgPasien(String mrn) {
		List<ListDataRgPasien> all = new ArrayList<ListDataRgPasien>();
		Statement stmt3=null;
		ResultSet rs3=null;
		Connection connection = DbConnection.getPooledConnection();
			try 
		    	{					
					if(connection == null)return null;
				     	
			        String query3="SELECT * FROM (SELECT SUBSTR(MRN.MEDICAL_RECORD_NO,3,12)AS card_NO, PS.PERSON_NAME, VS.VISIT_ID, wm.ward_desc, bm.Bed_No, "
			        		+ "PRS.PERSON_NAME AS NAMA_DOKTER, PGCOMMON.FXGETCODEDESC(PT.PATIENT_CLASS) as patient_class "
			        		+ "from MEDICALRECORDNO MRN, PATIENT PT, PERSON PS, VISIT VS, wardmstr wm, roommstr rm, bedmstr bm, bedhistory bh, "
			        		+ "VISITDOCTOR VD, CAREPROVIDER CP, PERSON PRS "
			        		+ "where SUBSTR(MRN.MEDICAL_RECORD_NO,3,12)='"+mrn+"' and SUBSTR(MRN.MEDICAL_RECORD_NO,1,2)='OP' and PT.PATIENT_ID=MRN.PATIENT_ID "
			        		+ "and PS.PERSON_ID=PT.PERSON_ID AND VS.PATIENT_ID=PT.PATIENT_ID AND wm.wardmstr_id=rm.wardmstr_id and "
			        		+ "rm.roommstr_id = bm.roommstr_id and bm.bedmstr_id=bh.bedmstr_id and bh.visit_id=VS.VISIT_ID AND VD.VISIT_ID=VS.VISIT_ID AND "
			        		+ "CP.CAREPROVIDER_ID=VD.CAREPROVIDER_ID AND PRS.PERSON_ID=CP.PERSON_ID AND PT.PATIENT_ID=VS.PATIENT_ID "
			        		+ "order by bh.effective_start_datetime desc) WHERE ROWNUM=1";
			        stmt3=connection.createStatement();
			        rs3=stmt3.executeQuery(query3);
			        rs3.next();
			        	ListDataRgPasien lvp = new ListDataRgPasien();
			        	lvp.setCard_NO(rs3.getString("card_NO"));
			        	lvp.setPERSON_NAME(rs3.getString("PERSON_NAME"));
			        	lvp.setVISIT_ID(rs3.getString("VISIT_ID"));
			        	lvp.setWard_desc(rs3.getString("ward_desc"));
			        	lvp.setBed_No(rs3.getString("Bed_No"));
			        	lvp.setNAMA_DOKTER(rs3.getString("NAMA_DOKTER"));
			        	lvp.setPatient_class(rs3.getString("patient_class"));
			        	all.add(lvp);			        	
			        rs3.close();
			        stmt3.close();
		    	}
		    catch (SQLException e ) 
		    	{
		        	System.out.println(e.getMessage());
		    	} 			
			finally {
			     if (stmt3!=null) try  { stmt3.close(); } catch (Exception ignore){}			     
			     if (rs3!=null) try  { rs3.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		return all;
	}
}