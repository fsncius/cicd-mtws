package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.apache.http.util.TextUtils;

import com.google.gson.Gson;
import com.rsmurniteguh.webservice.dep.all.model.AndroidUpdate;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.mobile.Berita;
import com.rsmurniteguh.webservice.dep.all.model.mobile.CardBpjsData;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.mobile.WhatsNewView;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInfoResponse;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.CommonUtil;

public class MobileCommon extends DbConnection {

	public static final String BASE_FOLDER_UPLOAD = "C:\\inetpub\\wwwroot\\MTMH\\antrian\\Upload";

	public static final String PROJECT_WHATSNEW_STATUS_BUG = "01";
	public static final String PROJECT_WHATSNEW_STATUS_FEATURE = "02";

	public static List<WhatsNewView> getWhatsNewList(String projectCode, String version) {
		System.out.println("whatsNewList...");
		List<WhatsNewView> data = new ArrayList<WhatsNewView>();

		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		PreparedStatement st = null;
		ResultSet rs = null;

		String sql = "SELECT new_id, wnew.version as [Version], feature, pj.name as [ProjectName], wnew.subject as [Subject], status, go_live_date  "
				+ "FROM tb_what_new wnew "
				+ "INNER JOIN tb_project pj on pj.project_id = wnew.project_id and pj.code = ? and pj.deleted = 0"
				+ "WHERE wnew.version = ? and wnew.deleted = 0 and (wnew.status = ? )"
				+ "ORDER by wnew.status ASC , wnew.update_at desc";

		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, projectCode);
			ps.setString(2, version);
			ps.setString(3, "02");
			rs = ps.executeQuery();

			while (rs.next()) {
				WhatsNewView dl = new WhatsNewView();
				dl.setProject(rs.getString("ProjectName"));
				dl.setSubject(rs.getString("Subject"));
				dl.setVersion(rs.getString("Version"));

				data.add(dl);
			}
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static AndroidUpdate getAndroidVersion(String projectCode) {
		AndroidUpdate result = new AndroidUpdate();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		ArrayList<String> releaseNotes = new ArrayList<String>();
		
		if (connection == null)
			return null;
		PreparedStatement st = null;
		ResultSet rs = null;

		String sql = "select w.version_code, w.version, w.feature, p.update_url from tb_what_new w "
				+ "inner join tb_project p on p.project_id = w.project_id and p.code = ? "
				+ "inner join "
				+ "( "
				+ "select top 1 w2.version_code from tb_what_new w2 "
				+ "inner join tb_project p2 on p2.project_id = w2.project_id and p2.code = ? "
				+ "where w2.deleted = 0 "
				+ "order by w2.version_code desc"
				+ ") as feature on feature.version_code = w.version_code "
				+ "where w.deleted = 0 "
				+ "order by w.feature asc ";

		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, projectCode);
			ps.setString(2, projectCode);
			rs = ps.executeQuery();

			while (rs.next()) {
				String releaseNote = rs.getString("feature");
				Long versionCode = rs.getLong("version_code");
				String versionName = rs.getString("version");
				String versionURL = rs.getString("update_url");
				releaseNotes.add(releaseNote);
				result.setURL(versionURL);
				result.setLatestVersion(versionName);
				result.setLatestVersionCode(versionCode);
			}
			
			result.setReleaseNotes(releaseNotes.toArray(new String[releaseNotes.size()]));
			
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}

		return result;
	}
	
	private static Object getFirstObject(ArrayList<String> target){
		Object result = new Object();
		Collections.sort(target);
		return result;
	}

	public static Object simple_get_poly() {
		String url = "http://192.168.222.114:18080/MTWS/Mobility/GetPoliclynicLists";

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			// SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static Object get_berita(String reader_role) {
		List<Berita> list = new ArrayList<Berita>();

		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getBpjsOnlineInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			String sql = "SELECT berita_id, judul, tgl, isi "
					+ "FROM tb_berita WHERE tgl BETWEEN ? AND ?  ";
			
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
	        Calendar cal = Calendar.getInstance();
	        String dateTo = sdf.format(cal.getTime());
	        cal.add(Calendar.DATE, -30);
	        String dateFrom = sdf.format(cal.getTime());
			
			ps = Connection.prepareStatement(sql);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, dateFrom);
			ps.setString(2, dateTo);
			rs = ps.executeQuery();
			
			while (rs.next()){
				Berita dl = new Berita();
				dl.setBerita_id(rs.getString("berita_id"));
				dl.setJudul(rs.getString("judul"));
				dl.setTanggal(rs.getString("tgl"));
				dl.setSimple_berita(rs.getString("isi"));
				
				list.add(dl);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		}
		return list;
	}

	public static Object add_kotak_saran(String user_id, String user_name, String project_code, String version,
			String saran, String kontak) {
		ResponseStatus data=new ResponseStatus();

		Connection connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql = "INSERT INTO tb_kotaksaran(user_id,user_name,project_code,version,saran,created_at, kontak) "
				+ "VALUES(?,?,?,?,?,SYSDATETIME(), ?) ";
		
		try {
				ps = connection.prepareStatement(sql);
				ps = CommonUtil.initialStatement(ps);
				ps.setString(1, user_id);
				ps.setString(2, user_name);
				ps.setString(3, project_code);
				ps.setString(4, version);
				ps.setString(5, saran);
				ps.setString(6, kontak);
				ps.executeUpdate();
				
				data.setSuccess(true);
				data.setMessage("Terimakasih atas saran yang diberikan, saran anda telah tersampaikan.");
		}
		
		catch (SQLException e)
		{
			data.setSuccess(false);
			data.setMessage("Terjadi kesalahan server, coba lagi nanti.");
			e.fillInStackTrace();
			System.out.println(e.getMessage());
		}
		finally
		{	
			 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}

		}
		return data;
	}

	public static Object getBpjsData(String noKartuBpjs){
		ResponseStatus rs = new ResponseStatus();
		CardBpjsData data = new CardBpjsData();
		
		String ret = BpjsService2.GetBpjsInfobyNoka(noKartuBpjs);
		
	       if(!ret.equals("")){
	    	   	Gson gson = new Gson();
	        	BpjsInfoResponse memberInfo = gson.fromJson(ret, BpjsInfoResponse.class); 
		        String provumumkd = memberInfo.getResponse().getPeserta().getProvUmum().getKdProvider();
		        String provumumnm = memberInfo.getResponse().getPeserta().getProvUmum().getNmProvider();
		        String tglahir = memberInfo.getResponse().getPeserta().getTglLahir();
		        data.setTglLahir(tglahir);
		        data.setProvumumnm(provumumnm);
		        data.setProvumumkd(provumumkd);
		        rs.setData(data);
		        rs.setSuccess(true);
				}
	       else{
	    	   rs.setSuccess(false);
	       }
	       return rs;
	}
}