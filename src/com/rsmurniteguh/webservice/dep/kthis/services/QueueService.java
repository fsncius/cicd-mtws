package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.util.TextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.rsmurniteguh.webservice.dep.all.model.ListGetQueue;
import com.rsmurniteguh.webservice.dep.all.model.QueueView;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.ServerPrintView;
import com.rsmurniteguh.webservice.dep.all.model.queue.CashierQueueVm;
import com.rsmurniteguh.webservice.dep.all.model.queue.Counter;
import com.rsmurniteguh.webservice.dep.all.model.queue.DataConfig;
import com.rsmurniteguh.webservice.dep.all.model.queue.DataQueueReg;
import com.rsmurniteguh.webservice.dep.all.model.queue.DataViewReg;
import com.rsmurniteguh.webservice.dep.all.model.queue.Location;
import com.rsmurniteguh.webservice.dep.all.model.queue.ListWaitingQueue;
import com.rsmurniteguh.webservice.dep.all.model.queue.QueueCall;
import com.rsmurniteguh.webservice.dep.all.model.queue.QueueConfig;
import com.rsmurniteguh.webservice.dep.all.model.queue.QueueDetails;
import com.rsmurniteguh.webservice.dep.base.BusinessException;
import com.rsmurniteguh.webservice.dep.base.ICodeMstrDetailConst;
import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.ISysConstant;
import com.rsmurniteguh.webservice.dep.biz.QueueServiceBiz;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.kthis.view.CodeDescView;
import com.rsmurniteguh.webservice.dep.kthis.view.UserLoginView;
import com.rsmurniteguh.webservice.dep.all.model.queue.RequestTicket;


public class QueueService { 
	private static String LOCATION_POLY_BPJS = "PBS1";
	private static String LOCATION_POLY_GENERAL = "PGL1";
	private static String validationMessage;
	
	@SuppressWarnings("finally")
	public static Boolean postDoctorWorkstationCall(String queueno, String subspeciality, String usermstr) {
		String location = "";
		
		if(subspeciality.equals("336581902"))location = "1"; //BPJS 
		else if(subspeciality.equals("311187819"))location = "2"; //POLY lt 2
		
		String message = "OK";
		Connection connection=DbConnection.getQueueInstance();
		if(connection==null)return false;
		String SPsql = "EXEC callDoctorWorkstationQueue ?,?,?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, queueno);
			ps.setString(2, location);
			ps.setString(3, usermstr);
			rs =  ps.executeQuery();
			
			while(rs.next())
			{
				message = rs.getString("mess");
			}
			
		} catch (SQLException e) {
			message ="Failed to Perform Transaction, Error : "+e.getMessage(); 
		}
		finally{
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			return true;
		}
	}	
		
	public static List<ListGetQueue> InsertQueue(String careProviderId, String patient_name, String card_no,
		String tgl_berobat, String PatientId, String location, String group_id, String source) {
		
		source = (source!=null) ? "QUE"+source : "";
		
		List<ListGetQueue> data=new ArrayList<ListGetQueue>();
		Connection connection=DbConnection.getQueueInstance();
		if(connection==null)return null;
		PreparedStatement ps = null; 
		
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		
		String sql = "";
		if(!careProviderId.isEmpty()) {
			sql = "Select queue_id, queue_no, resourcemstr_id, queue_date, card_no, patient_name, created_by, created_at, updated_at, updated_by, deleted "
					+ "from queue "
					+ "where card_no=? and queue_date=CONVERT(date, ?, 105) and group_id=? and location = ? and resourcemstr_id = ? and deleted=0 "
					+ "ORDER BY queue_no desc";
		}else {
			sql = "Select queue_id, queue_no, resourcemstr_id, queue_date, card_no, patient_name, created_by, created_at, updated_at, updated_by, deleted "
					+ "from queue "
					+ "where card_no=? and queue_date=CONVERT(date, ?, 105) and group_id=? and location = ? and deleted=0 "
					+ "ORDER BY queue_no desc";
		}		
		
		int nominator = 0;
		if(location.equals("1")) nominator = Integer.parseInt(group_id) * 1000;
		String sql2="INSERT INTO queue ( "
					+ "location ,queue_no ,doctor_code ,group_id ,resourcemstr_id ,patient_name ,card_no ,counter_id ,calling_ind ,endserve_ind ,status ,queue_date, "
					+ "created_at ,created_by ,updated_at,updated_by ,deleted,source_type) "
				+ "VALUES (?, "
						+ "(select coalesce(max(queue_no),?)+1 from queue q where q.location = ? and q.group_id = ? and convert(date, q.queue_date) = CONVERT(date, ?, 105) and q.deleted = 0),"
					+ "null,?,?,?,?, null, null, null, null, CONVERT(date,?, 105),CURRENT_TIMESTAMP,?, CURRENT_TIMESTAMP,?, 0,? )";
		
		try {
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(sql);
			ps2 = connection.prepareStatement(sql2);
			
			
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, card_no);
			ps.setString(2, tgl_berobat);
			ps.setString(3, group_id);
			ps.setBigDecimal(4, new BigDecimal(location));
			if(!careProviderId.isEmpty()) ps.setBigDecimal(5, new BigDecimal(careProviderId));
			PreparedStatement temp = ps;
			rs = ps.executeQuery();
			
			if(!rs.next()) {
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(60000);
				ps2.setBigDecimal(1, new BigDecimal(location));
				ps2.setBigDecimal(2, new BigDecimal(nominator));
				ps2.setBigDecimal(3, new BigDecimal(location));
				ps2.setBigDecimal(4, new BigDecimal(String.valueOf(Integer.parseInt(group_id))));
				ps2.setString(5, tgl_berobat);
				ps2.setBigDecimal(6, new BigDecimal(group_id));
				ps2.setString(7, careProviderId);
				ps2.setString(8, patient_name);
				ps2.setString(9, card_no);
				ps2.setString(10, tgl_berobat);
				ps2.setString(11, PatientId);
				ps2.setString(12, PatientId);
				ps2.setString(13, source);
				ps2.executeUpdate();
				
				connection.commit();
				
				if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
				rs = null;
				rs = temp.executeQuery();
				
				while(rs.next())
				{	
					ListGetQueue lp=new ListGetQueue();
					lp.setQueue_id(rs.getString("queue_id"));
					lp.setQueue_no(rs.getString("queue_no"));
					lp.setResourcemstr_id(rs.getString("resourcemstr_id"));
					lp.setQueue_date(rs.getString("queue_date"));
					lp.setCard_no(rs.getString("card_no"));
					lp.setPatient_name(rs.getString("patient_name"));
					lp.setCreated_by(rs.getString("created_by"));
					lp.setCreated_at(rs.getString("created_at"));
					lp.setUpdated_at(rs.getString("updated_at"));
					lp.setUpdated_by(rs.getString("updated_by"));
					lp.setDeleted(rs.getString("deleted"));
					
					lp.setResponse(true);
					data.add(lp);
				}
				addToPolyQueueOnline((ArrayList<ListGetQueue>) data, location, group_id);
			}else {
				
				do {
					ListGetQueue lp=new ListGetQueue();
					lp.setQueue_no(rs.getString("queue_no"));
					lp.setResourcemstr_id(rs.getString("resourcemstr_id"));
					lp.setQueue_date(rs.getString("queue_date"));
					lp.setCard_no(rs.getString("card_no"));
					lp.setPatient_name(rs.getString("patient_name"));
					lp.setCreated_by(rs.getString("created_by"));
					lp.setCreated_at(rs.getString("created_at"));
					lp.setUpdated_at(rs.getString("updated_at"));
					lp.setUpdated_by(rs.getString("updated_by"));
					lp.setDeleted(rs.getString("deleted"));
					
					lp.setResponse(false);
					lp.setDescription("Anda sudah pernah registrasi untuk hari ini");
					
					data.add(lp);
				} while (rs.next());		
			}
			connection.commit();
		}
		
		catch (SQLException e)
		{
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back in "+e.getStackTrace()[0].getMethodName());
	                connection.rollback();
	            } catch(SQLException excep) {
	            	System.out.println(excep.getMessage());
	            }
	        }
		}
		finally
		{	
		     try { connection.setAutoCommit(true); } catch (SQLException e) {}
		 	 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	private static void addToPolyQueueOnline(ArrayList<ListGetQueue> list, String location, String group_id){
		Connection connection = null;
		PreparedStatement pstmtInsertSql = null;
		
		try {
			connection = DbConnection.getQueueInstance();
			if (connection == null ) throw new Exception("Database failed to connect !");
			connection.setAutoCommit(false);
			
			for(ListGetQueue view : list){
				pstmtInsertSql = null;	
				
				String insertQuery="INSERT INTO polyqueue ( "
							+ "queue_id ,enterqueue_datetime ,startserve_datetime ,endserve_datetime ,endserve_ind,created_at ,created_by ,updated_at,updated_by ,deleted)"
							+ "VALUES (?,?,null,null, 0, ?, ?, ?, ?, 0)";
					pstmtInsertSql = connection.prepareStatement(insertQuery);
					pstmtInsertSql.setString(1, view.getQueue_id().toString());// rsReport.getString("queue_id"));
					pstmtInsertSql.setString(2, view.getCreated_at());
					pstmtInsertSql.setString(3, view.getCreated_at());
					pstmtInsertSql.setString(4, view.getCreated_by());
					pstmtInsertSql.setString(5, view.getCreated_at());
					pstmtInsertSql.setString(6, view.getCreated_by());
					pstmtInsertSql.execute();
					pstmtInsertSql.close();
			}
		}		
			
		catch (SQLException e){
			System.out.println(e.getMessage());
		    if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
			if (pstmtInsertSql != null) try { pstmtInsertSql.close(); } catch (SQLException e) {}
		}		
	}
	
	public static List<ListGetQueue> GetQueue(String cardNo, String patientId, String queueDate, String location, String group_id) {
		System.out.println("GetQueue...");//"+cardNo+"|"+patientId+"|"+queueDate+"|"+location+"|"+group_id);
		List<ListGetQueue> data=new ArrayList<ListGetQueue>();
		
		Connection connection=DbConnection.getQueueInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql = "SELECT queue_no, resourcemstr_id, queue_date, card_no, patient_name, created_by, created_at, updated_at, updated_by, deleted " + 
				"FROM queue "+
				"WHERE "+
					"card_no=? and created_by =? and queue_date=CONVERT(date, ?, 105) and deleted = 0 and group_id=? and location = ?";
		
		try {
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10000);
			ps.setString(1, cardNo);
			ps.setString(2, patientId);
			ps.setString(3, queueDate);
			ps.setString(4, group_id);
			ps.setBigDecimal(5, new BigDecimal(location));
			rs = ps.executeQuery();
			
			if(rs.next())
			{	
				ListGetQueue lp=new ListGetQueue();
				lp.setQueue_no(rs.getString("queue_no"));
				lp.setResourcemstr_id(rs.getString("resourcemstr_id"));
				lp.setQueue_date(rs.getString("queue_date"));
				lp.setCard_no(rs.getString("card_no"));
				lp.setPatient_name(rs.getString("patient_name"));
				lp.setCreated_by(rs.getString("created_by"));
				lp.setCreated_at(rs.getString("created_at"));
				lp.setUpdated_at(rs.getString("updated_at"));
				lp.setUpdated_by(rs.getString("updated_by"));
				lp.setDeleted(rs.getString("deleted"));
				data.add(lp);
			}
			connection.commit();
		}
		
		catch (SQLException e)
		{
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back in "+e.getStackTrace()[0].getMethodName());
	                connection.rollback();
	            } catch(SQLException excep) {
	            	System.out.println(excep.getMessage());
	            }
	        }
		}
		finally
		{	
		     try { connection.setAutoCommit(true); } catch (SQLException e) {}
		 	 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	private static class RequestIDTicketView{
		private String resourcemstr;
		private String location;
		private String tanggal;
		private String source;
		private String nama_patient;
		private String no_mrn;
		
		public String getResourcemstr() {
			return resourcemstr;
		}
		public void setResourcemstr(String resourcemstr) {
			this.resourcemstr = resourcemstr;
		}
		public String getLocation() {
			return location;
		}
		public void setLocation(String location) {
			this.location = location;
		}
		public String getTanggal() {
			return tanggal;
		}
		public void setTanggal(String tanggal) {
			this.tanggal = tanggal;
		}
		public String getSource() {
			return source;
		}
		public void setSource(String source) {
			this.source = source;
		}
		public String getNama_patient() {
			return nama_patient;
		}
		public void setNama_patient(String nama_patient) {
			this.nama_patient = nama_patient;
		}
		public String getNo_mrn() {
			return no_mrn;
		}
		public void setNo_mrn(String no_mrn) {
			this.no_mrn = no_mrn;
		}
	}
	
	private static Timestamp parseDate(String date) throws ParseException{
		Timestamp result = null;
		result= Timestamp.valueOf(date);
		return result;
	}
	
	private static Boolean validateRequest(RequestIDTicketView view){
		validationMessage = "";
		Boolean result = true;
		try{
			if(view == null)
			{
				validationMessage = "Request kosong!";
				return false;	
			}
			String source  = view.getSource();
			String resourceMstr = view.getResourcemstr();
			BigDecimal resourceMstrId = new BigDecimal(resourceMstr);
			String dateString = view.getTanggal();
			
			// Check stop resource
			ResponseStatus stopStatus = dataBpjsSer.checkStopResource(resourceMstrId, dateString);
			Boolean isStop = true;
			if(stopStatus != null){
				Boolean isGetStopStatusSuccess = stopStatus.isSuccess();
				if(isGetStopStatusSuccess){
					Object statusData = stopStatus.getData();
					isStop = statusData instanceof Boolean ? (Boolean) statusData : true;
					if(isStop){
						validationMessage = "Antrian dokter yang dipilih pada tanggal "+ dateString +" penuh, silahkan hubungi CS.";
						return false;
					}
					
					// Common validation
					if(TextUtils.isEmpty(source)){
						validationMessage = "Source antrian kosong";
						return false;
					}else{
						int count = source.length();
						if(count >2){
							validationMessage = "Source tidak valid (lebih dari 2 karakter)";
							return false;
						}
						if(count != 2){
							validationMessage = "Source tidak valid (kurang dari 2 karakter)";
							return false;
						}
					}
				}else{
					validationMessage = stopStatus.getMessage();
					return false;
				}
			}else{
				validationMessage = "Stop null.";
				return false;
			}

			
		}catch(Exception ex){
			String message = ex.getMessage();
			System.err.println(message);
			validationMessage = message;
			result = false;
		}
		
		return result;
	}
	
	private static RequestIDTicketView buildRequestIDView(String resourcemstr, String location, String tanggal, String source, String nama_patient, String no_mrn){
		RequestIDTicketView result = new RequestIDTicketView();
		result.resourcemstr = resourcemstr;
		result.location = location;
		result.tanggal = tanggal;
		result.source = source;
		result.nama_patient = nama_patient;
		result.no_mrn = no_mrn;
		return result;
	}
	
	//--------------------------------------------------------------------------------------------------------
	public static ResponseStatus RequestIDTicket(String resourcemstr, String location, String tanggal, String source, String nama_patient, String no_mrn, String queue_type){
		ResponseStatus ret  = new ResponseStatus();
		
		// Validasi Input
		RequestIDTicketView view = buildRequestIDView(resourcemstr, location, tanggal, source, nama_patient, no_mrn);
		if(!validateRequest(view))
		{
			ret.setSuccess(false);
			ret.setMessage(validationMessage);
			return ret;	
		}
		// Validasi Input End
		
		Connection Connection = null, ConnectionKthis = null;
		PreparedStatement ps = null, ps2 = null, ps3 = null, ps4 = null;
		ResultSet rs = null, rs2 = null, rs3 = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			ConnectionKthis = DbConnection.getPooledConnection();
			
			Connection.setAutoCommit(false);
			if (Connection == null || ConnectionKthis == null ) throw new Exception("Database failed to connect !");
			queue_type = StringUtils.isEmpty(queue_type) ? QueueServiceBiz.QUEUE_TYPE_REGULER : queue_type;
			String status = PrepareInitStatus(source);
			String sourceCat = QueueServiceBiz.SOURCE_CAT + source;
			String sql = "INSERT INTO tb_queue (resourcemstr,location,queue_date, deleted, created_at, status, patient_name, card_no, source, queue_type) "
					+ " OUTPUT Inserted.queue_id "
					+ " VALUES(?,?,?,0,SYSDATETIME(),?,?,?,?,?) ";
			
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10000);
			ps.setString(1, resourcemstr);
			ps.setString(2, location);
			ps.setString(3, tanggal);
			ps.setString(4, status);	
			ps.setString(5, nama_patient);
			ps.setString(6, no_mrn);
			ps.setString(7, sourceCat);
			ps.setString(8, queue_type);
			
			String sql2 = "SELECT careprovider_id, resource_name FROM tb_resourcemstr WHERE resourcemstr_id = ? ";
			ps2= Connection.prepareStatement(sql2);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(10000);
			ps2.setString(1, resourcemstr);
			rs2 = ps2.executeQuery();
			
			if(!rs2.next()){
				String sql3 = "select resource_name, rm.careprovider_id, usermstr_id from resourcemstr rm "+
							" inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = rm.CAREPROVIDER_ID "+
							" inner join USERPROFILE up on up.person_id = cp.person_id where rm.resourcemstr_id = ? ";
				
				ps3= ConnectionKthis.prepareStatement(sql3);
				ps3.setEscapeProcessing(true);
				ps3.setQueryTimeout(10000);
				ps3.setString(1, resourcemstr);
				rs3 = ps3.executeQuery();
				
				if (rs3.next()) {

					String usermstr_id = rs3.getString("usermstr_id");
					String careprovider = rs3.getString("careprovider_id");
					String resource_name = rs3.getString("resource_name");
					
					String sql4 = "INSERT INTO tb_resourcemstr (usermstr_id,resourcemstr_id,careprovider_id,resource_name) VALUES(?,?,?,?) ";
					ps4= Connection.prepareStatement(sql4);
					ps4.setEscapeProcessing(true);
					ps4.setQueryTimeout(10000);
					ps4.setBigDecimal(1, new BigDecimal(usermstr_id));
					ps4.setBigDecimal(2, new BigDecimal(resourcemstr));
					ps4.setBigDecimal(3, new BigDecimal(careprovider));
					ps4.setString(4, resource_name);
					ps4.execute();
				}else{
					throw new Exception("no data found !");
				}
				
			}
			rs = ps.executeQuery();
			
			if (rs.next()){
				ret.setSuccess(true);
				ret.setMessage(rs.getString("queue_id"));
			}			
			Connection.commit();			
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
 			if (ConnectionKthis != null) try { ConnectionKthis.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
			if (ps3 != null) try { ps3.close(); } catch (SQLException e) {}
			if (rs3 != null) try { rs3.close(); } catch (SQLException e) {}
			if (ps4 != null) try { ps4.close(); } catch (SQLException e) {}
		}
		
		return ret;
	}
	
	public static ResponseStatus RequestTicket(String location, String no_subspecialis, String no_doctor,String queue_id, String tanggal){
		return GenerateRegistrationBpjsTicket(location, no_subspecialis, no_doctor,queue_id, tanggal);
	}
	
	public static ResponseStatus GenerateRegistrationBpjsTicket(String location, String no_subspecialis, String no_doctor, String queue_id, String tanggal){
		ResponseStatus ret = new ResponseStatus();
		String Mqueue_no;
		Connection connection = DbConnection.getAntrianInstance();
		if(connection == null)return null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		
		try{			
			connection.setAutoCommit(false);
			
			String sql = "SELECT queue_no FROM tb_queue WHERE queue_date = ? AND queue_id = ? AND deleted = 0 AND location =?";
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, tanggal);
			ps.setString(2, queue_id);
			ps.setString(3, location);
			
			rs = ps.executeQuery();
			if(rs.next()){//queue_id terdaftar
				String Queue_no = rs.getString("queue_no");
				if(Queue_no != null){
					if(!Queue_no.equals("")){
						ret.setSuccess(true);
						ret.setMessage(Queue_no);
					}
				}
				else{
					String doc = no_doctor.replaceFirst("^0+(?!$)", "");
					String sub = no_subspecialis.replaceFirst("^0+(?!$)", "");
					String mNo_doctor = String.format("%02d", Integer.parseInt(doc));
					String mNo_subspecialis = String.format("%02d", Integer.parseInt(sub));
					String sql2 = "SELECT RIGHT(queue_no,LEN(queue_no)-CHARINDEX('-',queue_no)) as last_queue_no FROM tb_queue WHERE queue_date = ? AND location = ? AND LEFT(queue_no, 2) = ? AND SUBSTRING(queue_no, 3, 2) = ? ORDER BY RIGHT(queue_no, 2) DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY";
					ps2 = connection.prepareStatement(sql2);
					ps2.setEscapeProcessing(true);
					ps2.setQueryTimeout(5000);
					ps2.setString(1, tanggal);
					ps2.setString(2, location);
					ps2.setString(3, mNo_subspecialis);
					ps2.setString(4, mNo_doctor);
					rs2 = ps2.executeQuery();
					if(rs2.next()){
						String lastQueueNo = rs2.getString("last_queue_no");
						String queue = lastQueueNo.replaceFirst("^0+(?!$)", "");
						int queue_int = Integer.valueOf(queue);
						queue_int++;
						if(queue_int > 99){
							Mqueue_no = String.format("%03d", queue_int);
						}
						else{
							Mqueue_no = String.format("%02d", queue_int);
						}
						String mqueue_no = mNo_subspecialis + mNo_doctor + "-" + Mqueue_no;
						
						String sql3 = "UPDATE tb_queue SET queue_no = ? WHERE queue_date = ? AND queue_id = ? AND deleted = 0 AND location = ? ";
						ps3 = connection.prepareStatement(sql3);
						ps3.setEscapeProcessing(true);
						ps3.setQueryTimeout(5000);
						ps3.setString(1, mqueue_no);						
						ps3.setString(2, tanggal);
						ps3.setString(3, queue_id);
						ps3.setString(4, location);
						ps3.executeUpdate();
						
						ret.setSuccess(true);
						ret.setMessage(mqueue_no);
					}
					else{
						//gagal select queue_no		
						/*jika belum berbentuk nomor 1
						 * untuk pertama kalinya*/
						
						Mqueue_no = String.format("%02d", 1);
						String mqueue_no = mNo_subspecialis + mNo_doctor + "-" + Mqueue_no;
						
						String sql3 = "UPDATE tb_queue SET queue_no = ? WHERE queue_date = ? AND queue_id = ? AND deleted = 0 AND location = ?";
						ps3 = connection.prepareStatement(sql3);
						ps3.setEscapeProcessing(true);
						ps3.setQueryTimeout(5000);
						ps3.setString(1, mqueue_no);
						ps3.setString(2, tanggal); 
						ps3.setString(3, queue_id);
						ps3.setString(4, location);
						ps3.executeUpdate();

						ret.setSuccess(true);
						ret.setMessage(mqueue_no);
					}
				}
			}
			else{
				//queue_id tidak terdaftar
				ret.setSuccess(false);
				ret.setMessage("queue_id belum terdaftar");
			}
			connection.commit();
		}
		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally{
			try{connection.setAutoCommit(true);}
			catch (SQLException e) {}
		 	 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		 	 if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		 	 if (rs3!=null) try  { rs3.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return ret;
	}
	
	public static String PrepareInitStatus(String source){
		StringBuilder sb = new StringBuilder();
		if(StringUtils.isEmpty(source))return QueueServiceBiz.QUE_REG_IDLE;
		switch(source){
			case QueueServiceBiz.SOURCE_DESKTOP :
				sb.append(QueueServiceBiz.QUE_REG_FINISH);
			break;
			case QueueServiceBiz.SOURCE_MOBILE :
				sb.append(QueueServiceBiz.QUE_REG_FINISH);
			break;
			case QueueServiceBiz.SOURCE_WEB :
				sb.append(QueueServiceBiz.QUE_REG_FINISH);
			break;
			default :
				sb.append(QueueServiceBiz.QUE_REG_IDLE);
			break;
		}
		return sb.toString();
	}
 
	public static ResponseStatus ChangeQueueStatus(String queue_id, String userMstrID, String status, String counterId, String statusQueueTime, String queue_no){
		ResponseStatus rst = new ResponseStatus();
		if(status.equals(QueueServiceBiz.QUE_REG_IDLE)){
			rst = IdleQueue(queue_id, userMstrID, status, counterId, statusQueueTime);
		}
		else if(status.equals(QueueServiceBiz.QUE_REG_SKIP)){
			rst = SkipQueue(queue_id, status, userMstrID, statusQueueTime, counterId);
		}
		else if(status.equals(QueueServiceBiz.QUE_REG_CALL)){
			rst = CallQueue(status, userMstrID, queue_id,counterId);
		}
		else if(status.equals(QueueServiceBiz.QUE_REG_START)){
			rst = StartQueue(queue_id, counterId, status, userMstrID);
		}
		else if(status.equals(QueueServiceBiz.QUE_REG_FINISH)){
			rst = FinishQueue(status, userMstrID, queue_id, queue_no);
		}
		return rst;
	}
	
	public static ResponseStatus IdleQueue(String queue_id, String userMstrID, String status, String counterId, String statusQueueTime){
		ResponseStatus rst = new ResponseStatus();
		ResultSet rs = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		Connection connection = DbConnection.getAntrianInstance();
		
		try{
			connection.setAutoCommit(false);
			String sql = "SELECT status FROM tb_queue WHERE queue_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, queue_id);
			rs = ps.executeQuery();
			
			if(rs.next()){
				String mStatus = rs.getString("status");
				if(mStatus.equals("SQUE9")){
					String sql2 = "UPDATE tb_queue SET status = ?, updated_at = SYSDATETIME(), updated_by = ? WHERE queue_id = ? AND status = 'SQUE9'";
					ps2 = connection.prepareStatement(sql2);
					ps2.setEscapeProcessing(true);
					ps2.setQueryTimeout(5000);					
					ps2.setString(1, status);
					ps2.setString(2, userMstrID);
					ps2.setString(3, queue_id);
					ps2.execute();		
					rst.setSuccess(true);
					rst.setMessage("STATUS IDLE");
				}
				else{
					System.out.println("Belum di skip!!!");
				}
			}
			
			connection.commit();
		}

		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
	 		if (ps2 != null)try{ps2.close();}catch (SQLException e) {}
		}	
		return rst;
	}
	
	public static ResponseStatus SkipQueue(String queue_id, String status ,String userMstrID, String statusQueueTime, String counterId){
		ResponseStatus rst = new ResponseStatus();
		Connection connection = DbConnection.getAntrianInstance();
		if(connection == null)return null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		ResultSet rs = null;
		ResultSet rs4 = null;
		String serveTimeId = null;
		
		try{
			connection.setAutoCommit(false);
			
			String sql4 = "SELECT serve_time_id FROM tb_serve_time_reg WHERE queue_id = ? ORDER BY serve_time_id DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY";
			ps4 = connection.prepareStatement(sql4);
			ps4.setEscapeProcessing(true); 	
			ps4.setQueryTimeout(5000);
			ps4.setString(1, queue_id);
			rs4 = ps4.executeQuery();
			if(rs4.next()){
				serveTimeId = rs4.getString("serve_time_id");
			}
			
			String sql = "SELECT start_time FROM tb_serve_time_reg WHERE queue_id = ? AND deleted = 'false'";
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, queue_id);
			rs = ps.executeQuery();
			
			if(rs.next()){
				if(rs.next() && !rs.getString("start_time").isEmpty()){													
					String sql3 = "UPDATE tb_serve_time_reg SET updated_at  = SYSDATETIME(), updated_by = ?,"
							+ " status = ?, counter_id = ?, deleted = '1' WHERE queue_id = ? AND serve_time_id = ? AND end_time is null";
					ps3 = connection.prepareStatement(sql3);
					ps3.setEscapeProcessing(true);
					ps3.setQueryTimeout(5000);
					ps3.setString(1, userMstrID);
					ps3.setString(2, statusQueueTime);
					ps3.setString(3, counterId);
					ps3.setString(4, queue_id);
					ps3.setString(5, serveTimeId);
							
				}
				else{
	/*					String sql5 = "INSERT INTO tb_serve_time_reg (queue_id, end_time, counter_id, status, created_at, created_by, deleted) VALUES "
							+ "(?, SYSDATETIME() , ?, ?, SYSDATETIME() , ?, 1)";
					ps5 = connection.prepareStatement(sql5);
					ps5.setEscapeProcessing(true);
					ps5.setQueryTimeout(5000);
					ps5.setString(1, queue_id);
					ps5.setString(2, counterId);
					ps5.setString(3, statusQueueTime);
					ps5.setString(4, userMstrID);
					ps5.execute();
					rst.setMessage("Antrian berhasil di skip");
					rst.setSuccess(true);*/
					rst.setSuccess(false);
					rst.setMessage("START TIME TIDAK ADA");
				}
				

				String sql2 = "UPDATE tb_queue SET status = ?, updated_by = ?, updated_at = SYSDATETIME() WHERE queue_id = ? ";
				ps2 = connection.prepareStatement(sql2);
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(5000);
				ps2.setString(1, status);
				ps2.setString(2, userMstrID);
				ps2.setString(3, queue_id);
				ps2.executeUpdate();
				rst.setMessage("STATUS SKIP");
				rst.setSuccess(true);
			}		
			connection.commit();
		}
		
		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (rs4 != null)try{rs4.close();}catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
	 		if (ps2 != null)try{ps2.close();}catch (SQLException e) {}
	 		if (ps3 != null)try{ps3.close();}catch (SQLException e) {}
	 		if (ps4 != null)try{ps4.close();}catch (SQLException e) {}
		}	
		return rst;
	}

	public static ResponseStatus CallQueue(String status, String userMstrID, String queue_id, String counterId){
		ResponseStatus rst = new ResponseStatus();
		PreparedStatement ps = null, ps2 = null, ps3 = null, ps4 = null, ps5 = null, ps6 = null, ps7 = null;
		ResultSet rs = null, rs6 = null;
		Connection connection = null;
		connection = DbConnection.getAntrianInstance();
		
		try{		
			connection.setAutoCommit(false);
			

//			String sql6 = "UPDATE tb_caller SET counter_id = ? WHERE queue_id = ? AND counter_id != ? ";
			
			String sql6 = "SELECT queue_id, counter_id  FROM tb_caller WHERE queue_id = ?";
			ps6 = connection.prepareStatement(sql6);
			ps6.setEscapeProcessing(true);
			ps6.setQueryTimeout(5000);
			ps6.setString(1, queue_id);
			rs6 = ps6.executeQuery();
			
			if (!rs6.next()) {
				String sql2 = "INSERT INTO tb_caller (counter_id, queue_id, created_at, tipe ) VALUES (?, ?, SYSDATETIME(), ?)";
				ps2 = connection.prepareStatement(sql2);
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(5000);
				ps2.setString(1, counterId);
				ps2.setString(2, queue_id);
				ps2.setString(3, QueueServiceBiz.VOICE_ON);
				ps2.execute();
				

				String sql = "UPDATE tb_queue SET status = ?, updated_at = SYSDATETIME(), updated_by = ? WHERE queue_id = ? AND deleted = '0' AND queue_no IS NOT NULL";
				ps = connection.prepareStatement(sql);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(5000);
				ps.setString(1, status);
				ps.setString(2, userMstrID);
				ps.setString(3, queue_id);
				ps.executeUpdate();
				
				String sql4 = "SELECT queue_id FROM tb_serve_time_reg WHERE queue_id = ? AND deleted=0 ";
				ps4 = connection.prepareStatement(sql4);
				ps4.setEscapeProcessing(true);
				ps4.setQueryTimeout(5000);
				ps4.setString(1, queue_id);
				rs = ps4.executeQuery();
				
				if(rs.next()){
					String mQueue_id = rs.getString("queue_id");
					String sql5 = "UPDATE tb_serve_time_reg SET updated_at = SYSDATETIME(), updated_by = ? WHERE queue_id = ?";
					ps5 = connection.prepareStatement(sql5);
					ps5.setEscapeProcessing(true);
					ps5.setQueryTimeout(5000);
					ps5.setString(1, mQueue_id);
					ps5.setString(2, userMstrID);
					ps5.execute();
				}
				else{
					String sql3 = "INSERT INTO tb_serve_time_reg (queue_id, start_time, counter_id, status, created_at, created_by, updated_at, updated_by, deleted) VALUES (?, SYSDATETIME(), ?, ?, SYSDATETIME(), ?, SYSDATETIME(), ?, '0')";
					ps3 = connection.prepareStatement(sql3);
					ps3.setEscapeProcessing(true);
					ps3.setQueryTimeout(5000);
					ps3.setString(1, queue_id);
					ps3.setString(2, counterId);
					ps3.setString(3, "STM2");
					ps3.setString(4, userMstrID);
					ps3.setString(5, userMstrID);
					ps3.execute();
				}
				
			} else {
				if (!rs6.getString("counter_id").equals(counterId)) {
					String sql7 = "UPDATE tb_caller SET counter_id = ? WHERE queue_id = ? ";
					ps7 = connection.prepareStatement(sql7);
					ps7.setEscapeProcessing(true);
					ps7.setQueryTimeout(5000);
					ps7.setString(1, counterId);
					ps7.setString(2, queue_id);
					ps7.executeUpdate();
				}
			}
						
			
			rst.setMessage("success call");
			rst.setSuccess(true);			
			connection.commit();
		}
		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally{
			try{connection.setAutoCommit(true);}catch (SQLException e) {}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		    if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}
		    if (ps4!=null) try  { ps4.close(); } catch (Exception ignore){}
		    if (ps5!=null) try  { ps5.close(); } catch (Exception ignore){}
		    if (ps6!=null) try  { ps6.close(); } catch (Exception ignore){}
		    if (ps7!=null) try  { ps7.close(); } catch (Exception ignore){}
		    if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (rs6!=null) try  { rs6.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}	
		return rst;
	}
	
	private static void inserttbCallerDokter(String queueNo, String counterId)
	{
		PreparedStatement ps = null, ps2 = null, ps3= null;
		ResultSet rs = null, rs6 = null;
		Connection connection = null;
		connection = DbConnection.getAntrianInstance();
		
		try{		
			connection.setAutoCommit(false);
			String sql = "select top (1) q.queue_id from tb_queue q where q.queue_no = ? order by q.queue_id desc";
			String queueId = DbConnection.executeScalar(DbConnection.ANTRIAN, sql, new Object[] {queueNo}).toString(); 
			
			
			String sql6 = "SELECT queue_id FROM tb_caller WHERE queue_id = ?";
			ps3 = connection.prepareStatement(sql6);
			ps3.setEscapeProcessing(true);
			ps3.setQueryTimeout(5000);
			ps3.setString(1, queueId);
			rs6 = ps3.executeQuery();
			
			if (!rs6.next()) {
				String sql2 = "INSERT INTO tb_caller (counter_id, queue_id, created_at, tipe ) VALUES (?, ?, SYSDATETIME(), ?)";
				ps2 = connection.prepareStatement(sql2);
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(5000);
				ps2.setString(1, counterId);
				ps2.setString(2, queueId);
				ps2.setString(3, QueueServiceBiz.VOICE_OFF);
				ps2.execute();
			}
			connection.commit();
		}
		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally{
			try{connection.setAutoCommit(true);}catch (SQLException e) {}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}	
	}
	
	public static ResponseStatus StartQueue(String queue_id, String counterId, String status, String userMstrID){
		ResponseStatus rst = new ResponseStatus();
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		Connection connection = null;
		connection = DbConnection.getAntrianInstance();
		try{
			connection.setAutoCommit(false);	
			
			String sql= "UPDATE tb_serve_time_reg SET start_time = SYSDATETIME(), updated_at = SYSDATETIME(), updated_by = ? WHERE queue_id = ? AND deleted = 0";
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, userMstrID);
			ps.setString(2, queue_id);
			ps.execute();
			
			String sql2 = "UPDATE tb_queue SET status = ?, updated_at = SYSDATETIME(), updated_by = ? WHERE queue_id = ? AND deleted = '0' AND queue_no IS NOT NULL";
			ps2 = connection.prepareStatement(sql2);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(5000);
			ps2.setString(1, status);
			ps2.setString(2, userMstrID);
			ps2.setString(3, queue_id);
			ps2.execute();
			
			rst.setMessage("STATUS START");
			rst.setSuccess(true);
			
			connection.commit();
		}
		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally{
			try{connection.setAutoCommit(true);} catch (SQLException e) {}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return rst;
	}
	
	public static ResponseStatus FinishQueue(String status, String userMstrID, String queue_id, String queue_no){
		ResponseStatus rst = new ResponseStatus();
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
//		PreparedStatement ps3 = null;
//		PreparedStatement ps4 = null;
//		ResultSet rs3 = null;
//		ResultSet rs4 = null;
		Connection connection = null;
//		Connection connection2 = null;
		String personName = null, card_no = null;
		connection = DbConnection.getAntrianInstance();
//		connection2 = DbConnection.getPooledConnection();
		
		try{
			connection.setAutoCommit(false);
//			connection2.setAutoCommit(false);
					
			String sql3 = " SELECT PER.PERSON_NAME, C.CARD_NO FROM VISIT V"
					+ " INNER JOIN PATIENT PAT ON PAT.PATIENT_ID = V.PATIENT_ID"
					+ " INNER JOIN PERSON PER ON PER.PERSON_ID = PAT.PERSON_ID"
					+ " INNER JOIN CARD C ON C.PERSON_ID = PAT.PERSON_ID"
					+ " WHERE TRUNC(V.ADMISSION_DATETIME) = TRUNC(SYSDATE)"
					+ " AND V.PATIENT_TYPE = 'PTY2'"
					+ " AND V.QUEUE_NO = ?";
			List list = DbConnection.executeReader(DbConnection.KTHIS, sql3, new Object[] {queue_no});
			
			String checkQueue = "SELECT " +
					"  TOP 1 Q.*, " +
					"  CM.CODE_CAT AS location_group " +
					"FROM " +
					"  TB_QUEUE Q " +
					"  LEFT JOIN TB_CODEMSTR CM ON Q.LOCATION = CM.CODE_ABBR " +
					"  AND CM.DEFUNCT_IND = 'N' " +
					"WHERE " +
					"  Q.QUEUE_ID = ?";			
			
			List checkQueueList = DbConnection.executeReader(DbConnection.ANTRIAN, checkQueue, new Object[] {queue_id});
			boolean isCheckedExists = checkQueueList != null;
			if(checkQueueList == null) isCheckedExists = false;
			if(checkQueueList.isEmpty()) isCheckedExists = false;
			if(checkQueueList.size()!= 1) isCheckedExists = false;
			HashMap checkQueueData = (HashMap)checkQueueList.get(0);
			QueueView checkView = isCheckedExists ? getMappedClass(checkQueueData, QueueView.class) : null;
			Boolean specialQueue = checkView != null;
			specialQueue = specialQueue ? !TextUtils.isEmpty(checkView.getLocation_group()) : specialQueue;
			specialQueue = specialQueue ? checkView.getLocation_group().equals(QueueServiceBiz.LOCATION_GROUP_CASHIER) : specialQueue;
			
			
			if(list.size() > 0 || specialQueue){
				boolean validated = false;
				if(specialQueue) {
					personName = checkView.getPatient_name();
					card_no = checkView.getCard_no();
					validated = true;
				}else {
					HashMap row = (HashMap) list.get(0);
					if((row.get("PERSON_NAME") != null && !row.get("PERSON_NAME").toString().equals("")) || (row.get("CARD_NO") != null && !row.get("CARD_NO").toString().equals("CARD_NO"))){
						personName = row.get("PERSON_NAME").toString();
						card_no = row.get("CARD_NO").toString();
						validated = true;
					}
				}
				
				if(validated) {
					String sql = "UPDATE tb_queue SET status = ?, patient_name = ? ,card_no = ?  ,  updated_at = SYSDATETIME(), updated_by = ? WHERE queue_id = ? AND deleted = '0' AND queue_no IS NOT NULL";
					ps = connection.prepareStatement(sql);
					ps.setEscapeProcessing(true);
					ps.setQueryTimeout(5000);
					ps.setString(1, status);
					ps.setString(2, personName);
					ps.setString(3, card_no);
					ps.setString(4, userMstrID);
					ps.setString(5, queue_id);
					ps.execute();
					
					String sql2= "UPDATE tb_serve_time_reg SET end_time = SYSDATETIME(), updated_at = SYSDATETIME(), updated_by = ? WHERE queue_id = ? AND deleted = 0";
					ps2 = connection.prepareStatement(sql2);
					ps2.setEscapeProcessing(true);
					ps2.setQueryTimeout(5000);
					ps2.setString(1, userMstrID);
					ps2.setString(2, queue_id);
					ps2.execute();
				
					connection.commit();
					
					rst.setMessage("STATUS FINISH");
					rst.setSuccess(true);
					delQueueCall(queue_id);
				}		
			}
			else{
				rst.setMessage("Data belum terdaftar");
				rst.setSuccess(false);
			}
				
		}
		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally{
			try{connection.setAutoCommit(true);} catch (SQLException e) {}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return rst;
	}	
	
/*	public static ResponseStatus processQueueRegistration(String queue_id, String userMstrID, String status, String counterId, String statusQueueTime){
		ResponseStatus rst = new ResponseStatus();
		
		Connection connection = DbConnection.getAntrianInstance();
		if(connection == null)return null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		PreparedStatement ps6 = null;
		PreparedStatement ps7 = null;
		PreparedStatement ps8 = null;
		PreparedStatement ps9 = null;
		PreparedStatement ps10 = null;
		ResultSet rs = null;
		ResultSet rs6 = null;
		ResultSet rs7 = null;
		ResultSet rs8 = null;
		String serveTimeId = null;
		
		try{
			connection.setAutoCommit(false);
			
			String sql = "SELECT start_time FROM tb_serve_time_reg WHERE queue_id = ? AND deleted = 'false'";
			String sql6 = "SELECT status FROM tb_serve_time_reg WHERE queue_id =? AND deleted = 'true'";
			String sql7 = "SELECT serve_time_id FROM tb_serve_time_reg WHERE queue_id = ? ORDER BY serve_time_id DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY";
			String sql8 = "UPDATE tb_queue SET status = ?, created_by = ?, updated_at = SYSDATETIME() WHERE queue_id = ? ";
			String sql9 = "UPDATE tb_queue SET status = ?, updated_at = SYSDATETIME() WHERE queue_id = ? ";

			ps7 = connection.prepareStatement(sql7);
			ps7.setEscapeProcessing(true); 	
			ps7.setQueryTimeout(5000);
			ps7.setString(1, queue_id);
			rs7 = ps7.executeQuery();
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, queue_id);
			rs = ps.executeQuery();
			
			if(rs7.next()){
				serveTimeId = rs7.getString("serve_time_id");
			}
			
			if(status.equals(QueueServiceBiz.QUE_REG_IDLE)){
				ps9 = connection.prepareStatement(sql9);
				ps9.setEscapeProcessing(true);
				ps9.setQueryTimeout(5000);
				ps9.setString(1, status);
				ps9.setString(2, queue_id);
				ps9.executeUpdate();

				rst.setMessage("Set End Date Success");
				rst.setSuccess(true);
			}else if(status.equals(QueueServiceBiz.QUE_REG_CALL)){
				ps8 = connection.prepareStatement(sql8);
				ps8.setEscapeProcessing(true);
				ps8.setQueryTimeout(5000);
				ps8.setString(1, status);
				ps8.setString(2, userMstrID);
				ps8.setString(3, queue_id);
				ps8.executeUpdate();
				
				String sql0 = "INSERT INTO tb_caller (counter_id, queue_id, created_at, tipe ) VALUES (?, ?, SYSDATETIME(), ?)";
				ps10 = connection.prepareStatement(sql0);
				ps10.setEscapeProcessing(true);
				ps10.setQueryTimeout(5000);
				ps10.setString(1, counterId);
				ps10.setString(2, queue_id);
				ps10.setString(3, QueueServiceBiz.VOICE_ON);
				ps10.execute();
				
				rst.setMessage("Set End Date Success");
				rst.setSuccess(true);
			}
			else if(status.equals(QueueServiceBiz.QUE_REG_FINISH)){
				String sql2 = "UPDATE tb_serve_time_reg SET end_time = SYSDATETIME(), updated_at  = SYSDATETIME(), updated_by = ?, status = ?, counter_id = ? WHERE queue_id = ? AND serve_time_id = ? AND deleted = 'false'";
				ps2 = connection.prepareStatement(sql2);
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(5000);
				ps2.setString(1, userMstrID);
				ps2.setString(2, statusQueueTime);
				ps2.setString(3, counterId);
				ps2.setString(4, queue_id);
				ps2.setString(5, serveTimeId);
				ps2.execute();				
				
				ps8 = connection.prepareStatement(sql8);
				ps8.setEscapeProcessing(true);
				ps8.setQueryTimeout(5000);
				ps8.setString(1, status);
				ps8.setString(2, userMstrID);
				ps8.setString(3, queue_id);
				ps8.executeUpdate();
				
				rst.setMessage("Set End Date Success");
				rst.setSuccess(true);
			}else if(status.equals(QueueServiceBiz.QUE_REG_START)){
				ps6 = connection.prepareStatement(sql6);
				ps6.setString(1, queue_id);
				rs6 = ps6.executeQuery();
				if(rs6.next()){
					 if(rs6.getString("status").equals("SQUE9")){
						 String sql1 = "INSERT INTO tb_serve_time_reg (queue_id, start_time, counter_id, status, created_at, created_by, updated_at, updated_by, deleted) VALUES "
									+ "(?, SYSDATETIME() , ?, ?, SYSDATETIME(), ?, SYSDATETIME(), ?,0)";
							ps1 = connection.prepareStatement(sql1);
							ps1.setEscapeProcessing(true);
							ps1.setQueryTimeout(5000);
							ps1.setString(1, queue_id);
							ps1.setString(2, counterId);
							ps1.setString(3, statusQueueTime);
							ps1.setString(4, userMstrID);
							ps1.setString(5, userMstrID);
							ps1.execute();						
							rst.setMessage("Set Start Date Success");
							rst.setSuccess(true);							
							
							ps8 = connection.prepareStatement(sql8);
							ps8.setEscapeProcessing(true);
							ps8.setQueryTimeout(5000);
							ps8.setString(1, status);
							ps8.setString(2, userMstrID);
							ps8.setString(3, queue_id);
							ps8.executeUpdate();
					 }
				}else{
					 String sql1 = "INSERT INTO tb_serve_time_reg (queue_id, start_time, counter_id, status, created_at, created_by, updated_at, updated_by, deleted) VALUES "
								+ "(?, SYSDATETIME(), ?, ?, SYSDATETIME(), ?, SYSDATETIME(), ?,0)";
						ps1 = connection.prepareStatement(sql1);
						ps1.setEscapeProcessing(true);
						ps1.setQueryTimeout(5000);
						ps1.setString(1, queue_id);
						ps1.setString(2, counterId);
						ps1.setString(3, statusQueueTime);
						ps1.setString(4, userMstrID);
						ps1.setString(5, userMstrID);
						ps1.execute();
						rst.setMessage("Set Start Date Success");	
						rst.setSuccess(true);				
						
						ps8 = connection.prepareStatement(sql8);
						ps8.setEscapeProcessing(true);
						ps8.setQueryTimeout(5000);
						ps8.setString(1, status);
						ps8.setString(2, userMstrID);
						ps8.setString(3, queue_id);
						ps8.executeUpdate();
				 }
			}else if(status.equals(QueueServiceBiz.QUE_REG_SKIP)){
				if(rs.next() && !rs.getString("start_time").isEmpty()){
					String sql4 = "UPDATE tb_serve_time_reg SET updated_at  = SYSDATETIME(), updated_by = ?,"
							+ " status = ?, counter_id = ?, deleted = '1' WHERE queue_id = ? AND serve_time_id = ? AND end_time is null";	
					
					ps8 = connection.prepareStatement(sql8);
					ps8.setEscapeProcessing(true);
					ps8.setQueryTimeout(5000);
					ps8.setString(1, status);
					ps8.setString(2, userMstrID);
					ps8.setString(3, queue_id);
					ps8.executeUpdate();
					
					ps4 = connection.prepareStatement(sql4);
					ps4.setEscapeProcessing(true);
					ps4.setQueryTimeout(5000);
					ps4.setString(1, userMstrID);
					ps4.setString(2, statusQueueTime);
					ps4.setString(3, counterId);
					ps4.setString(4, queue_id);
					ps4.setString(5, serveTimeId);
					boolean isSuccess = ps4.execute();
					if(isSuccess){
						rst.setMessage("Antrian berhasil di skip");
						rst.setSuccess(true);
					}
					else{
						rst.setMessage("Antrian telah finish");
						rst.setSuccess(true);
					}
				}
				else{
					String sql5 = "INSERT INTO tb_serve_time_reg (queue_id, end_time, counter_id, status, created_at, created_by, deleted) VALUES "
							+ "(?, SYSDATETIME() , ?, ?, SYSDATETIME() , ?, 1)";
					ps5 = connection.prepareStatement(sql5);
					ps5.setEscapeProcessing(true);
					ps5.setQueryTimeout(5000);
					ps5.setString(1, queue_id);
					ps5.setString(2, counterId);
					ps5.setString(3, statusQueueTime);
					ps5.setString(4, userMstrID);
					ps5.execute();
					rst.setMessage("Antrian berhasil di skip");
					rst.setSuccess(true);
				}
			}
			connection.commit();
		}
		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		finally{
			try{connection.setAutoCommit(true);}
			catch (SQLException e) {}
		 	 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		 	 if (rs6!=null) try  { rs6.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}
		     if (ps4!=null) try  { ps4.close(); } catch (Exception ignore){}
		     if (ps5!=null) try  { ps5.close(); } catch (Exception ignore){}
		     if (ps6!=null) try  { ps5.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return rst;
	}*/

	public static List<DataViewReg> ListviewReg(String counters){
		
		List<DataViewReg> ret = new ArrayList<DataViewReg>();
		
		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			
			Connection.setAutoCommit(false);
			if (Connection == null) throw new Exception("Database failed to connect !");
			
			String sql = " SELECT t.status as status_time , q.queue_date, q.status as status_queue , r.resource_name, q.location, c.counter_no, c.counter_id, q.queue_no, q.location "+
					"FROM tb_serve_time_reg t  "+
					"INNER JOIN tb_queue q ON q.queue_id = t.queue_id  "+
					"INNER JOIN tb_counter c ON c.counter_id = t.counter_id "+
					"LEFT JOIN tb_resourcemstr r ON r.resourcemstr_id = q.resourcemstr "+
					"WHERE CONVERT(DATE,q.queue_date )= CONVERT(DATE,GETDATE()) AND (q.status = ? OR q.status = ?) AND q.deleted = 0  AND c.counter_id IN ("+counters+") ";
			
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10000);
			ps.setString(1, QueueServiceBiz.QUE_REG_CALL);
			ps.setString(2, QueueServiceBiz.QUE_REG_START);
			rs = ps.executeQuery();

			while(rs.next()){
				DataViewReg dl = new DataViewReg();
				dl.setQueue_no(rs.getString("queue_no"));
				dl.setCounter_id(rs.getLong("counter_id"));
				dl.setCounter_no(rs.getInt("counter_no"));
				dl.setQueue_code(rs.getString("status_queue"));
				dl.setResource_name(rs.getString("resource_name"));
				ret.add(dl);
			}
			
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		}
		
		return ret;
	}
	
	public static Integer getServingTime(Long queueId){
		Integer result = 0;
		try {
			String query = "SELECT DATEDIFF(SECOND,ST.START_TIME,SYSDATETIME()) AS TOTAL_SECONDS FROM TB_QUEUE Q" +
					"  		LEFT JOIN TB_SERVE_TIME_REG ST ON ST.QUEUE_ID = Q.QUEUE_ID AND ST.DELETED = 0" +
					"  		WHERE Q.STATUS = 'SQUE2' AND Q.DELETED = 0" +
					" 		AND Q.QUEUE_ID = ?";
			Object[] parameters = new Object[] {queueId};
			Object rawData = DbConnection.executeScalar(DbConnection.ANTRIAN, query, parameters);
			if(!(rawData instanceof Integer))return result;
			Integer data = (Integer) rawData;
			result = data < 0 ? result : data;
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return result;
	}
	
	public static List<DataQueueReg> ListQueue(String lokasi, String counter_id, String tanggal, String regType ){
		
		List<DataQueueReg> ret = new ArrayList<DataQueueReg>();
		
		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			
			Connection.setAutoCommit(false);
			if (Connection == null) throw new Exception("Database failed to connect !");
			
			String sql = " SELECT q.queue_id, q.queue_date, q.status as status_queue , r.resource_name, q.resourcemstr , q.location, q.queue_no "+
				"FROM  tb_queue q "+
				"LEFT JOIN tb_resourcemstr r ON r.resourcemstr_id = q.resourcemstr  "+
				"WHERE q.queue_no is not null AND q.deleted = 0 AND q.queue_date = ? AND q.status = ? AND q.location = ? "
				+ "AND q.queue_type = ? "
				+ "OR q.queue_id IN "
						+ "(SELECT q.queue_id FROM tb_queue q "
						+ "INNER JOIN tb_serve_time_reg t ON q.queue_id = t.queue_id "
						+ "WHERE q.queue_no is not null AND q.deleted = 0 AND t.deleted = 0 AND q.queue_date = ? "
							+ "AND q.status in(?,?) AND t.deleted = 0 AND t.counter_id = ? AND q.location = ? ) "
				+"ORDER BY status_queue desc,queue_id asc";
			
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10000);
			ps.setString(1, tanggal);
			ps.setString(2, QueueServiceBiz.QUE_REG_IDLE);
			ps.setString(3, lokasi);
			ps.setString(4, regType);
			ps.setString(5, tanggal);
			ps.setString(6, QueueServiceBiz.QUE_REG_CALL);
			ps.setString(7, QueueServiceBiz.QUE_REG_START);
			ps.setString(8, counter_id);
			ps.setString(9, lokasi);
			rs = ps.executeQuery();

			while(rs.next()){
				DataQueueReg dl = new DataQueueReg();
				dl.setQueue_id(rs.getBigDecimal("queue_id"));
				dl.setQueue_date(rs.getString("queue_date"));
				dl.setStatus_queue(rs.getString("status_queue"));
				dl.setResource_name(rs.getString("resource_name"));
				dl.setResourcemstr(rs.getBigDecimal("resourcemstr"));
				dl.setLocation(rs.getString("location"));
				dl.setQueue_no(rs.getString("queue_no"));
				ret.add(dl);
			}
			
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		}
		
		return ret;
	}
	
	public static List<DataQueueReg> ListQueue(String lokasi, String counter_id, String tanggal, String regType, String status ){
		
		List<DataQueueReg> ret = new ArrayList<DataQueueReg>();
		
		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			
			Connection.setAutoCommit(false);
			if (Connection == null) throw new Exception("Database failed to connect !");
			
			String sql = " SELECT q.queue_id, q.queue_date, q.status as status_queue , r.resource_name, q.resourcemstr , q.location, q.queue_no "+
				"			FROM  tb_queue q "+
				"			LEFT JOIN tb_resourcemstr r ON r.resourcemstr_id = q.resourcemstr  "
				+ " 		LEFT JOIN TB_SERVE_TIME_REG T ON T.QUEUE_ID = Q.QUEUE_ID AND T.COUNTER_ID = ? AND T.DELETED = 0 "+
				"			WHERE q.queue_no is not null AND q.deleted = 0 AND q.queue_date = ? AND q.status = ? AND q.location = ? "
				+ "			AND q.queue_type = ? "
				+"			ORDER BY status_queue desc,queue_id asc";
			
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10000);
			ps.setString(1, counter_id);
			ps.setString(2, tanggal);
			ps.setString(3, status);
			ps.setString(4, lokasi);
			ps.setString(5, regType);
			rs = ps.executeQuery();

			while(rs.next()){
				DataQueueReg dl = new DataQueueReg();
				dl.setQueue_id(rs.getBigDecimal("queue_id"));
				dl.setQueue_date(rs.getString("queue_date"));
				dl.setStatus_queue(rs.getString("status_queue"));
				dl.setResource_name(rs.getString("resource_name"));
				dl.setResourcemstr(rs.getBigDecimal("resourcemstr"));
				dl.setLocation(rs.getString("location"));
				dl.setQueue_no(rs.getString("queue_no"));
				ret.add(dl);
			}
			
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		}
		
		return ret;
	}
	
	public static QueueCall SoundQueue(String counters){
		QueueCall ret = new QueueCall();
		
		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			
			Connection.setAutoCommit(false);
			if (Connection == null) throw new Exception("Database failed to connect !");
			
			String sql = " select top 1 c.caller_id, c2.counter_no, c.queue_id, c.created_at, c.tipe, q.queue_no "
					+ "from tb_caller c "
						+ "INNER JOIN tb_counter c2 ON c2.counter_id = c.counter_id "
						+ "INNER JOIN tb_queue q ON q.queue_id = c.queue_id "
					+ "where CONVERT(DATE,q.queue_date )= CONVERT(DATE,GETDATE()) AND c.counter_id in ("+counters+") order by created_at ";
			
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10000);
			rs = ps.executeQuery();

			if(rs.next()){
				ret.setCaller_id(rs.getBigDecimal("caller_id"));
				ret.setCounter_no(rs.getBigDecimal("counter_no"));
				ret.setQueue_id(rs.getBigDecimal("queue_id"));
				ret.setQueue_no(rs.getString("queue_no"));
				ret.setCreated_at(rs.getString("created_at"));
				ret.setTipe(rs.getInt("tipe"));
			}
			
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		} 
		return ret;
	}
	
	public static ResponseStatus delQueueCall(String queue_id) {
		ResponseStatus ret = new ResponseStatus();
		
		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			
			Connection.setAutoCommit(false);
			if (Connection == null) throw new Exception("Database failed to connect !");
			
			String sql = "  DELETE FROM tb_caller WHERE queue_id = ? OR convert(varchar, created_at, 23) <> LEFT (SYSDATETIME(), 10)";
			
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10000);
			ps.setBigDecimal(1, new BigDecimal(queue_id));
			ps.execute();

			Connection.commit();
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		} 
		return ret;
	}

	public static DataConfig getConfigData(){
		DataConfig ret = new DataConfig();
		
		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			
			Connection.setAutoCommit(false);
			if (Connection == null) throw new Exception("Database failed to connect !");
			
			String sql = " SELECT counter_id, location, counter_no, user_name, usermstr_id, status FROM tb_counter ";
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10000);
			rs = ps.executeQuery();

			List<Counter> lcounter = new ArrayList<Counter>();
			
			while(rs.next()){
				Counter dl = new Counter();
				dl.setCounter_id(rs.getBigDecimal("counter_id"));
				dl.setLocation(rs.getString("location"));
				dl.setCounter_no(rs.getInt("counter_no"));
				dl.setUser_name(rs.getString("user_name"));
				dl.setUsermstr_id(rs.getBigDecimal("usermstr_id"));
				dl.setStatus(rs.getString("status"));
				lcounter.add(dl);
			}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			
			sql = " SELECT location_name, code FROM tb_location ";
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10000);
			rs = ps.executeQuery();
			
			List<Location> llocation = new ArrayList<Location>();
			while(rs.next()){
				Location dl2 = new Location();
				dl2.setLocation_name(rs.getString("location_name"));
				dl2.setCode(rs.getString("code"));
				llocation.add(dl2);
			}
			
			ret.setListCounter(lcounter);
			ret.setListLocation(llocation);
			
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		} 
		return ret;
	}

	public static QueueConfig getConfigDataByAddress(String mac_address){
		QueueConfig ret = new QueueConfig();
		
		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			
			Connection.setAutoCommit(false);
			if (Connection == null) throw new Exception("Database failed to connect !");
			
			byte[] decodedBytes = Base64.getDecoder().decode(mac_address);
			String passDecode = new String(decodedBytes);
			
			String sql = " SELECT DISTINCT c.config_id, c.computer_name, c.ip, c.items "
					+ " FROM tb_config c "
					+ " INNER JOIN tb_config_macaddress m ON c.config_id = m.config_id "
					+ " WHERE m.macaddress IN ("+passDecode+") AND c.deleted = 0 ";
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10);
			rs = ps.executeQuery();

			
			while(rs.next()){
				ret.setConfig_id(rs.getBigDecimal("config_id"));
				ret.setComputer_name(rs.getString("computer_name"));
				ret.setIp(rs.getString("ip"));
				ret.setItems(rs.getString("items"));
			}
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		}
		return ret;
	}
	
	public static QueueConfig retConfigDataByAddress(QueueConfig queueConfig){
		QueueConfig ret = new QueueConfig();
		
		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			
			Connection.setAutoCommit(false);
			if (Connection == null) throw new Exception("Database failed to connect !");
			
			String sql = " SELECT c.config_id, c.computer_name, c.ip, c.items, c.location, c.counter, c.mac "
					+ " FROM tb_config c "
					+ " WHERE c.mac = ? AND c.ip = ? AND c.deleted = 0 ";
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10);
			ps.setString(1, queueConfig.getMac());
			ps.setString(2, queueConfig.getIp());
			rs = ps.executeQuery();

			
			while(rs.next()){
				ret.setConfig_id(rs.getBigDecimal("config_id"));
				ret.setComputer_name(rs.getString("computer_name"));
				ret.setIp(rs.getString("ip"));
				ret.setMac(rs.getString("mac"));
				ret.setLocation(rs.getString("location"));
				ret.setCounter(rs.getBigDecimal("counter"));
				ret.setItems(rs.getString("items"));
			}
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		}
		return ret;
	}
	
	public static ResponseStatus setConfigDataByAddress(QueueConfig queueConfig){
		ResponseStatus ret = new ResponseStatus();
		
		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			
			Connection.setAutoCommit(false);
			if (Connection == null) throw new Exception("Database failed to connect !");
			
			QueueConfig isAvail = retConfigDataByAddress(queueConfig);
			
			String sql = " INSERT INTO tb_config (computer_name,ip,mac,items,created_at,created_by,updated_at,updated_by,deleted,location,counter) "
					+ " VALUES(?,?,?,?,SYSDATETIME(),1,SYSDATETIME(),1,0,?,?) ";
			if (isAvail.getConfig_id()!=null) { //update
				sql = " UPDATE tb_config SET "
						+ "computer_name = ?, ip = ?,mac=?,items=? ,updated_at = SYSDATETIME(), "
						+ "deleted = 0 ,location = ?,counter=? WHERE config_id =  "+isAvail.getConfig_id();
			}
			
			
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10);
			ps.setString(1, queueConfig.getComputer_name());
			ps.setString(2, queueConfig.getIp());
			ps.setString(3, queueConfig.getMac());
			ps.setString(4, queueConfig.getItems());
			ps.setString(5, queueConfig.getLocation());
			ps.setBigDecimal(6, queueConfig.getCounter());
			ps.execute();
			
			Connection.commit();
			ret.setSuccess(true);
			ret.setMessage("OK");
			
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			ret.setMessage(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		}
		return ret;
	}
	
	public static List<Counter> getActiveCounter(String counters) {
		List<Counter> ret = new ArrayList<Counter>();
		
		Connection Connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection = DbConnection.getAntrianInstance();
			
			Connection.setAutoCommit(false);
			if (Connection == null) throw new Exception("Database failed to connect !");
			
			String sql = " SELECT counter_id, location, counter_no , user_name, usermstr_id, status FROM tb_counter WHERE usermstr_id is not null AND user_name is not null AND counter_id IN("+counters+") ";
			ps = Connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(10000);
			rs = ps.executeQuery();

			while(rs.next()){
				Counter dl = new Counter();
				dl.setCounter_id(rs.getBigDecimal("counter_id"));
				dl.setLocation(rs.getString("location"));
				dl.setCounter_no(rs.getInt("counter_no"));
				dl.setUser_name(rs.getString("user_name"));
				dl.setUsermstr_id(rs.getBigDecimal("usermstr_id"));
				dl.setStatus(rs.getString("status"));
				ret.add(dl);
			}
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		} 
		return ret;
	}
	
	public static UserLoginView LoginCaller(String username, String password){
		UserLoginView view = new UserLoginView();
		ResultSet haspas = null;
		ResultSet locs=null;
		ResultSet rolResult=null;
		Statement carep=null;
		Connection connection = DbConnection.getPooledConnection();
		try {
			String query = "select USERMSTR_ID, PASSWORD_EXPIRY_DATE, "
					+ "USER_CODE, USER_NAME "
					+ "from usermstr "
					+ "where user_code = '"+username+"' "
							+ " and password = '"+password+"' ";
			
			if(connection == null)return null;
			carep = connection.createStatement();
			haspas = carep.executeQuery(query);
			
			if(haspas.next()){
				view.setUsermstrId(haspas.getLong("USERMSTR_ID"));
				view.setPasswordExpiryDate(haspas.getDate("PASSWORD_EXPIRY_DATE"));
				view.setUserCode(haspas.getString("USER_CODE"));
				view.setUserName(haspas.getString("USER_NAME"));
			}
			
			carep.close();
			/*if (view.getUserCode() != null) {
				query = "select lm.LOCATIONMSTR_ID, lm.LOCATION_NAME " 
						+ "from USER_LOCATION ul "
						+ "inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = ul.LOCATIONMSTR_ID "
						+ "where USERMSTR_ID = '"+view.getUsermstrId()+"' ";

				locs = carep.executeQuery(query);
				List<CodeDescView> locsview = new ArrayList<CodeDescView>();
				while (locs.next()) {
					CodeDescView location = new CodeDescView();
					location.setId(locs.getBigDecimal("LOCATIONMSTR_ID"));
					location.setCode(locs.getString("LOCATION_NAME"));
					locsview.add(location);
				}

//				stmt.close();
				locs.close();

				query = "select rm.ROLEMSTR_ID, rm.ROLE_NAME " + "from USER_ROLE ur "
						+ "inner join rolemstr rm on rm.ROLEMSTR_ID = ur.ROLEMSTR_ID "
						+ "WHERE ur.USERMSTR_ID = '"+view.getUsermstrId()+"' ";

				rolResult = carep.executeQuery(query);
				List<CodeDescView> roleview = new ArrayList<CodeDescView>();
				while (rolResult.next()) {
					CodeDescView v = new CodeDescView();
					v.setId(rolResult.getBigDecimal("ROLEMSTR_ID"));
					v.setCode(rolResult.getString("ROLE_NAME"));
					roleview.add(v);
				}

				rolResult.close();
				carep.close();

				view.setLocations(locsview);
				view.setRoles(roleview);
			}*/

		} catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}

		finally {
		     try {connection.setAutoCommit(true); } catch (SQLException e) {}
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (locs!=null) try  { locs.close(); } catch (Exception ignore){}
		     if (rolResult!=null) try  { rolResult.close(); } catch (Exception ignore){}
		     if (carep!=null) try  { carep.close(); } catch (Exception ignore){}
		     if (connection != null) try { connection.close(); } catch (SQLException e) {}
		   }
		
		return view;
	}
	
	public static ResponseStatus ChangeCountertoAktif(String counterid, String usermstrid, String resourcemstrId) {
		ResponseStatus rst = new ResponseStatus();
		ResultSet rs = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		Connection connection = DbConnection.getAntrianInstance();
		Connection connectionKthis = DbConnection.getPooledConnection();
		
		try{
			connection.setAutoCommit(false);
			
			String sql = "select PER.PERSON_NAME FROM USERPROFILE up "+
						"inner join PERSON PER on up.person_id = PER.person_id WHERE USERMSTR_ID = ?";
			ps = connectionKthis.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);					
			ps.setString(1, usermstrid);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				
				String sql2 = "UPDATE tb_counter SET status = ?, user_name = ?, usermstr_id = ?, resourcemstr_id = ? WHERE counter_id = ?";
				ps2 = connection.prepareStatement(sql2);
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(5000);					
				ps2.setString(1, QueueServiceBiz.COUNTER_ACTIVE);
				ps2.setString(2, rs.getString("PERSON_NAME"));
				ps2.setString(3, usermstrid);
				ps2.setString(4, resourcemstrId);
				ps2.setString(5, counterid);				
				ps2.executeUpdate();				
				rst.setSuccess(true);
				connection.commit();
			}else{
				rst.setSuccess(false);
			}
			
		}

		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (connectionKthis != null) try { connectionKthis.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
	 		if (ps2 != null)try{ps2.close();}catch (SQLException e) {}
		}	
		return rst;
	}
	
	public static ResponseStatus ChangeCountertoNonAktif(String counterid) {
		ResponseStatus rst = new ResponseStatus();
		ResultSet rs = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		Connection connection = DbConnection.getAntrianInstance();
		
		try{
			connection.setAutoCommit(false);
			String sql = "SELECT status FROM tb_counter WHERE counter_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, counterid);
			rs = ps.executeQuery();
			
			if(rs.next()){
				String mStatus = rs.getString("status");
				if(mStatus.equals("CTR1")){
					String sql2 = "UPDATE tb_counter SET status = ?, user_name = ?, usermstr_id = ?, resourcemstr_id = ? WHERE counter_id = ?";
					ps2 = connection.prepareStatement(sql2);
					ps2.setEscapeProcessing(true);
					ps2.setQueryTimeout(5000);					
					ps2.setString(1, QueueServiceBiz.COUNTER_NONACTIVE);
					ps2.setString(2, null);
					ps2.setString(3, null);
					ps2.setString(4, null);
					ps2.setString(5, counterid);				
					ps2.execute();
					rst.setSuccess(true);
				}
				else{
					System.out.println("Status Counter Sedang NonAktif!!!");
				}
			}
			connection.commit();
		}

		catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (rs != null)try{ps.close();}catch (SQLException e) {}
		}	
		return rst;
	}

	//------------------SPECIAL QUEUE----------------------
	public static ResponseStatus CallSpecialQueueByNumber(String location, String counterid, String queueNo, String userMstrId){
		ResponseStatus result = new ResponseStatus();
		
//		Connection antrianConnection = null;
//		PreparedStatement pstmtInsertAntrian = null;
		try {
//			antrianConnection = DbConnection.getAntrianInstance();
//			if (antrianConnection == null) throw new Exception("Database failed to connect !");
			
			String insertAntrian = "insert into tb_special_queue "
					+ " (location, counter_id, queue_no, status, start_time, end_time, created_at, created_by, updated_at, updated_by, deleted ) "
					+ " values (?, ?, ?, ?, current_timestamp, null, current_timestamp, ?, null, null, 0) ";
//			pstmtInsertAntrian = antrianConnection.prepareStatement(insertAntrian);
//			pstmtInsertAntrian.setString(1, location);
//			pstmtInsertAntrian.setString(2, counterid);
//			pstmtInsertAntrian.setString(3, queueNo);
//			pstmtInsertAntrian.setString(4, QueueServiceBiz.QUE_REG_CALL);
//			pstmtInsertAntrian.setString(5, userMstrId);
//			pstmtInsertAntrian.executeUpdate();
			
			List<Object> parameters = new ArrayList<Object>();
			parameters.add(location);
			parameters.add(counterid);
			parameters.add(queueNo);
			parameters.add(QueueServiceBiz.QUE_REG_CALL);
			parameters.add(userMstrId);
			
			DbConnection.executeQuery(DbConnection.ANTRIAN, insertAntrian, parameters.toArray());
			
			inserttbCallerDokter(queueNo,counterid);
			result.setSuccess(true);
			result.setMessage("Success");
		}
		catch (SQLException e){
			e.printStackTrace();
//		    if (antrianConnection != null) try { antrianConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			e.printStackTrace();
//			if (antrianConnection != null) try { antrianConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
// 			if (antrianConnection != null) try { antrianConnection.close(); } catch (SQLException e) {}
//			if (pstmtInsertAntrian != null) try { pstmtInsertAntrian.close(); } catch (SQLException e) {}
		}
		
		return result;
	}
	
	private static ResponseStatus UpdateQueueByNumber(String location, String counterid, String queueNo, String status, String userMstrId){
		ResponseStatus result = new ResponseStatus();
		
//		Connection antrianConnection = null;
//		PreparedStatement pstmtUpdateAntrian = null;
		try {
//			antrianConnection = DbConnection.getAntrianInstance();
//			if (antrianConnection == null) throw new Exception("Database failed to connect !");
			
			String updateAntrian = "update tb_special_queue "
					+ " set status = ? "
					+ " ,end_time = current_timestamp "
					+ " ,updated_by = ? "
					+ " ,updated_at = current_timestamp "
					+ " where location = ? "
					+ " and counter_id = ? "
					+ " and queue_no = ?";
//			pstmtUpdateAntrian = antrianConnection.prepareStatement(updateAntrian);
//			pstmtUpdateAntrian.setString(1, status);
//			pstmtUpdateAntrian.setString(2, userMstrId);
//			pstmtUpdateAntrian.setString(3, location);
//			pstmtUpdateAntrian.setString(4, counterid);
//			pstmtUpdateAntrian.setString(5, queueNo);
//			pstmtUpdateAntrian.executeUpdate();
			
			List<Object> parameters = new ArrayList<Object>();
			parameters.add(status);
			parameters.add(userMstrId);
			parameters.add(location);
			parameters.add(counterid);
			parameters.add(queueNo);
			
			DbConnection.executeQuery(DbConnection.ANTRIAN, updateAntrian, parameters.toArray());
			
			result.setSuccess(true);
			result.setMessage("Success");
		}
		catch (SQLException e){
			e.printStackTrace();
//		    if (antrianConnection != null) try { antrianConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			e.printStackTrace();
//			if (antrianConnection != null) try { antrianConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
// 			if (antrianConnection != null) try { antrianConnection.close(); } catch (SQLException e) {}
//			if (pstmtUpdateAntrian != null) try { pstmtUpdateAntrian.close(); } catch (SQLException e) {}
		}
		
		return result;
	}
	
	public static ResponseStatus SkipSpecialQueueByNumber(String location, String counterid, String queueNo, String userMstrId){
		return UpdateQueueByNumber(location, counterid, queueNo, QueueServiceBiz.QUE_REG_SKIP, userMstrId);
	}
	
	public static ResponseStatus FinishSpecialQueueByNumber(String location, String counterid, String queueNo, String userMstrId){
		return UpdateQueueByNumber(location, counterid, queueNo, QueueServiceBiz.QUE_REG_FINISH, userMstrId);
	}
	
	public static List<ListWaitingQueue> ListViewPolyclinicBPJS(String location, String counters){
		List<ListWaitingQueue> list = new ArrayList<ListWaitingQueue>();
 		HashMap<String, ListWaitingQueue> map = new HashMap<String, ListWaitingQueue>();
		
		List<String> listKey = new ArrayList<String>();
		
		try {
			String selectAntrian = "select tc.counter_id, tc.counter_no, t1.queue_no, tc.usermstr_id, tc.user_name from tb_counter tc "
					+ " left join ( "
					+ "		select top 1 tsq.counter_id, tsq.queue_no from tb_special_queue tsq "
					+ "		where tsq.status in ('SQUE1', 'SQUE2') "
					+ "		and CONVERT(date, tsq.created_at) = CONVERT(date, getdate()) "
					+ "		and tsq.deleted = 0 "
					+ "		order by tsq.created_at desc "
					+ " ) t1 on t1.counter_id = tc.counter_id "
					+ " where tc.location = ? "
					+ " and tc.usermstr_id is not null "
					+ " and tc.usermstr_id <> '' "
					+ " and tc.counter_id in (" + counters + ")"
					+ " order by tc.counter_id ";
			List listAntrian = DbConnection.executeReader(DbConnection.ANTRIAN, selectAntrian, new Object[] {location});
			for (int a=0;a<listAntrian.size();a++){
				HashMap row = (HashMap)listAntrian.get(a);
				ListWaitingQueue newQueue = new ListWaitingQueue();
				newQueue.setCounterId(row.get("counter_id").toString());
				newQueue.setCounterNo(row.get("counter_no").toString());
				String currentNumber = row.get("queue_no") == null ? "" : row.get("queue_no").toString();
				newQueue.setCurrentNumber(currentNumber);
				newQueue.setListNumber(new ArrayList<String>());
				newQueue.setPersonName("");
				if (!map.containsKey(row.get("usermstr_id").toString())){
					map.put(row.get("usermstr_id").toString(), newQueue);
					listKey.add(row.get("usermstr_id").toString());
				}
			}
			
			if (map.size() == 0) throw new BusinessException("Counter is empty");
			
			String patientClass = "";
			if (location != null){
				if (location.equals(LOCATION_POLY_BPJS)){
					patientClass = " AND VI.PATIENT_CLASS = '" + ICodeMstrDetailConst.PTC_114 + "' ";
				} else {
					patientClass = " AND VI.PATIENT_CLASS <> '" + ICodeMstrDetailConst.PTC_114 + "' ";
				}
			}
			
			String selectQuery = "SELECT UPR.USERMSTR_ID, VI.QUEUE_NO, VI.QUEUE_SEQUENCE, PER.PERSON_NAME DOCTOR_NAME "
			          + " ,VI.ADMIT_STATUS "
			          + " FROM VISITREGNTYPE VRT "
			          + " INNER JOIN VISIT VI ON VI.VISIT_ID = VRT.VISIT_ID "
			          + " INNER JOIN RESOURCEMSTR RM ON RM.RESOURCEMSTR_ID = VRT.RESOURCEMSTR_ID "
			          + " INNER JOIN CAREPROVIDER CP ON CP.CAREPROVIDER_ID = RM.CAREPROVIDER_ID "
			          + " INNER JOIN PERSON PER ON PER.PERSON_ID = CP.PERSON_ID "
			          + " INNER JOIN USERPROFILE UPR ON UPR.PERSON_ID = PER.PERSON_ID "
			          + " WHERE 1=1 "
			          + " AND VRT.CREATED_DATETIME >= TRUNC(SYSDATE) "
			          + " AND VRT.CREATED_DATETIME <= TRUNC(SYSDATE) + 1 "
//			          + " AND V.ADMISSION_DATETIME >=PGREGN.fxGetOPOMRegnValidityDateTime(VRT.REGISTRATION_TYPE) "
			          + " AND VI.CONSULT_STATUS IN ('CNT1') "
			          + " AND VI.ADMIT_STATUS <> 'AST5' "
			          + patientClass
			          + " AND UPR.USERMSTR_ID IN (" + StringUtils.join( map.keySet().toArray(),",") + ") "
			          + " ORDER BY VI.QUEUE_SEQUENCE DESC "
			          + " , CASE WHEN VI.QUEUE_SEQUENCE > 1 "
			          + "   THEN VI.QUEUE_NO "
			          + "   ELSE NULL "
			          + " END "
			          + " , VRT.EFFICTIVE_DATETIME , VI.CONSULT_START_TIME";// VI.QUEUE_NO ASC ";
			List listKthis = DbConnection.executeReader(DbConnection.KTHIS, selectQuery, new Object[] {});
			for (int a=0;a<listKthis.size();a++){
				HashMap row = (HashMap)listKthis.get(a);
				if (map.containsKey(row.get("USERMSTR_ID").toString())){
					ListWaitingQueue oldQueue = map.get(row.get("USERMSTR_ID").toString());
					oldQueue.setPersonName(row.get("DOCTOR_NAME").toString());
					if (!oldQueue.getCurrentNumber().equals(row.get("QUEUE_NO")) && !oldQueue.getListNumber().contains(row.get("QUEUE_NO").toString())){
						oldQueue.getListNumber().add(row.get("QUEUE_NO").toString());
					}
				}
			}
			for (String key : listKey){
				if (map.containsKey(key)){
					list.add(map.get(key));
				}
			}
		}
		catch (BusinessException e){
			System.out.println(e.getMessage());
		}
		catch (SQLException e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		finally {
		}
		return list;
	}
	//-----------------------------------------------------

	public static List<CashierQueueVm> ListQueueKasir(String location) {
		List<CashierQueueVm> rst = new ArrayList<CashierQueueVm>();
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = DbConnection.getAntrianInstance();
		
		try{
			connection.setAutoCommit(false);
			
			String sql = "select q.queue_id, q.queue_no, q.patient_name, q.created_at " +
					" from tb_queue q left join  tb_serve_time_reg st " +
					" on q.queue_id = st.queue_id where st.end_time is null " +
					" and q.location = ? " +
					" and CONVERT(date, q.queue_date) = CONVERT(date, CURRENT_TIMESTAMP) "
					+ "	and (q.status != 'SQUE3' and q.status != 'SQUE9')";

			List listQueueKasir = DbConnection.executeReader(DbConnection.ANTRIAN, sql, new Object[] {location});
			for (int a=0;a<listQueueKasir.size();a++){
				HashMap row = (HashMap)listQueueKasir.get(a);
				CashierQueueVm cashierQueueVm = new CashierQueueVm();
				cashierQueueVm.setQueue_id(row.get("queue_id").toString());
				cashierQueueVm.setQueue_no(row.get("queue_no").toString());
				cashierQueueVm.setPatient_name(row.get("patient_name").toString());;
				cashierQueueVm.setCreated_at(row.get("created_at").toString());
				rst.add(cashierQueueVm);
			}

			connection.commit();
			
		}catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
		}	
		return rst;
	}
	
	public static QueueDetails QueueDetails(String queue_no, String location, String tanggal) {
		QueueDetails rst = new QueueDetails();
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = DbConnection.getAntrianInstance();
		
		try{
			connection.setAutoCommit(false);
			
			String sql = "SELECT q.queue_id, q.queue_no, q.queue_date,q.card_no, q.patient_name, q.created_at, q.created_by, q.updated_at, q.updated_by, q.status as status_queue , q.resourcemstr, "
							+ "r.resource_name , q.location, t.counter_id, c.counter_no, c.user_name, c.status as status_counter, "
							+ "t.start_time, t.end_time FROM tb_queue q " 
						 +"LEFT JOIN tb_resourcemstr r ON r.resourcemstr_id = q.resourcemstr  "
						 +"LEFT JOIN tb_serve_time_reg t ON q.queue_id = t.queue_id AND t.status = 'STM2' AND t.deleted = 0 "
						 +"LEFT JOIN tb_counter c ON c.counter_id = t.counter_id "
						 +"WHERE q.queue_no is not null AND q.deleted = 0 AND q.queue_no=? AND q.location = ? AND q.queue_date = ? ";
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, queue_no);
			ps.setString(2, location);
			ps.setString(3, tanggal);
			rs = ps.executeQuery();
			
			if(rs.next()){
				rst.setQueue_id(rs.getString("queue_id"));
				rst.setQueue_no(rs.getString("queue_no"));
				rst.setQueue_date(rs.getString("queue_date"));
				rst.setCard_no(rs.getString("card_no"));
				rst.setPatient_name(rs.getString("patient_name"));
				rst.setCreated_at(rs.getString("created_at"));
				rst.setCreated_by(rs.getString("created_by"));
				rst.setUpdated_at(rs.getString("updated_at"));
				rst.setUpdated_by(rs.getString("updated_by"));
				rst.setStatus_queue(rs.getString("status_queue"));
				rst.setResourcemstr(rs.getString("resourcemstr"));
				rst.setResource_name(rs.getString("resource_name"));
				rst.setLocation(rs.getString("location"));
				rst.setCounter_id(rs.getString("counter_id"));
				rst.setCounter_no(rs.getString("counter_no"));
				rst.setUser_name(rs.getString("user_name"));
				rst.setStatus_counter(rs.getString("status_counter"));
				rst.setStart_time(rs.getString("start_time"));
				rst.setEnd_time(rs.getString("end_time"));
			}
			connection.commit();
			
		}catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
		}	
		return rst;
	}
	
	private static class SearchQueueParam {
		
		private String queue_type;
		private String tanggal;
		private String source;
		private String location;
		private String queue_no;

		public SearchQueueParam(String queue_no, String location, String source, String tanggal, String queue_type) {
			this.queue_no = queue_no;
			this.location = location;
			this.source = source;
			this.tanggal = tanggal;
			this.queue_type = queue_type;
		}
	}
	
	private static Boolean isQueueFinished(SearchQueueParam param) {
		Boolean result = false;
		try {
			String query = "IF EXISTS(SELECT 1 FROM TB_QUEUE Q " + 
					"					 		LEFT JOIN TB_SERVE_TIME_REG ST ON ST.QUEUE_ID = Q.QUEUE_ID AND ST.DELETED = 0 " + 
					"					 		WHERE Q.STATUS = 'SQUE3' AND Q.DELETED = 0 " + 
					"							AND Q.LOCATION = ? " + 
					"							AND CONVERT(DATE, Q.QUEUE_DATE) = ? " + 
					"							AND Q.QUEUE_NO = ? "
					+ ") " + 
					"		BEGIN " + 
					"			SELECT CONVERT(BIT, 1) " + 
					"		END " + 
					"		ELSE BEGIN SELECT CONVERT(BIT, 0) END";
			Object[] parameters = new Object[] {param.location, param.tanggal, param.queue_no};
			Object rawData = DbConnection.executeScalar(DbConnection.ANTRIAN, query, parameters);
			if(!(rawData instanceof Boolean))return result;
			Boolean data = (Boolean) rawData;
			result = data;
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		return result;
	}
	
	private static ResponseStatus fail(String message){
		ResponseStatus result = new ResponseStatus();
		result.setMessage(message);
		result.setSuccess(false);
		return result;
	}
	
	public static ResponseStatus InsertQueueKasir(String queue_no,String location, String source, String tanggal, String queue_type) {
		ResponseStatus rps = new ResponseStatus();
		if(isQueueFinished(new SearchQueueParam(queue_no, location, source, tanggal, queue_type))) return fail("Nomor antrian telah diselesaikan.");
		ResultSet rs = null;
		PreparedStatement ps = null;
		ResultSet rs2 = null;
		PreparedStatement ps2 = null;
//		ResultSet rs3 = null;
//		PreparedStatement ps3 = null;
		String tempLocation = "";
		String tempLocation2 = "";
		Connection connection = DbConnection.getAntrianInstance();
//		Connection connectionKthis = DbConnection.getPooledConnection();
		try{
			connection.setAutoCommit(false);
			if(location.equals(QueueServiceBiz.KASIR_GENERAL))
			{
				tempLocation = QueueServiceBiz.REGISTRASI_GENERAL;
				tempLocation2 = QueueServiceBiz.POLIKLINIK_GENERAL;
			}
			else if (location.equals(QueueServiceBiz.KASIR_BPJS))
			{
				tempLocation = QueueServiceBiz.REGISTRASI_BPJS;
				tempLocation2 = QueueServiceBiz.POLIKLINIK_BPJS;
			}
			String sql = "select q.queue_id, q.queue_no,q.resourcemstr,q.patient_name,q.card_no "+
					" from tb_queue q " +
					" where q.queue_no = ? and (q.location = ? OR q.location = ? )"+
					" and CONVERT(date, q.queue_date) = CONVERT(date, CURRENT_TIMESTAMP) ";
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, queue_no);
			ps.setString(2, tempLocation);
			ps.setString(3, tempLocation2);
			rs = ps.executeQuery();
			
			if(rs.next()){	
				
				String sql2 = "select q.queue_id, q.queue_no,q.resourcemstr,q.patient_name,q.card_no "+
						" from tb_queue q " +
						" where q.queue_no = ? and  q.location = ? "+
						" and CONVERT(date, q.queue_date) = CONVERT(date, CURRENT_TIMESTAMP) ";
				
				ps2 = connection.prepareStatement(sql2);
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(5000);
				ps2.setString(1, queue_no);
				ps2.setString(2, location);
				rs2 = ps2.executeQuery();
			}
//			else
//			{
//				String queryQueue = "select PE.PERSON_NAME, CA.CARD_NO, VRT.RESOURCEMSTR_ID from visit V INNER JOIN PATIENT PA ON PA.PATIENT_ID = V.PATIENT_ID"
//						+ " INNER JOIN PERSON PE ON PA.PERSON_ID = PE.PERSON_ID"
//						+ " INNER JOIN CARD CA ON CA.PERSON_ID = PE.PERSON_ID"
//						+ " INNER JOIN VISITREGNTYPE VRT ON VRT.VISIT_ID = V.VISIT_ID"
//						+ " WHERE V.ADMISSION_DATETIME >= TRUNC(SYSDATE) AND V.ADMISSION_DATETIME <= TRUNC(SYSDATE) + 1"
//						+ " AND V.QUEUE_NO = ?";
//				
//				ps3 = connectionKthis.prepareStatement(queryQueue);
//				ps3.setEscapeProcessing(true);
//				ps3.setQueryTimeout(5000);
//				ps3.setString(1, queue_no);
//				rs3 = ps3.executeQuery();
//			}
//			if(rs3.next()){
//				queue_type = StringUtils.isEmpty(queue_type) ? QueueServiceBiz.QUEUE_TYPE_REGULER : queue_type;
//				String status = PrepareInitStatus(source);
//				String sourceCat = QueueServiceBiz.SOURCE_CAT + source;
//				String sql3 = "INSERT INTO tb_queue (resourcemstr,queue_no,location,queue_date, deleted, created_at, status, patient_name, card_no, source, queue_type) "
//						+ " OUTPUT Inserted.queue_id "
//						+ " VALUES(?,?,?,?,0,SYSDATETIME(),?,?,?,?,?) ";
//				
//				ps = connection.prepareStatement(sql3);
//				ps.setEscapeProcessing(true);
//				ps.setQueryTimeout(10000);
//				ps.setString(1, rs3.getString("RESOURCEMSTR_ID"));
//				ps.setString(2, queue_no);
//				ps.setString(3, location);
//				ps.setString(4, tanggal);
//				ps.setString(5, status);	
//				ps.setString(6, rs3.getString("PERSON_NAME"));
//				ps.setString(7, rs3.getString("CARD_NO"));
//				ps.setString(8, sourceCat);
//				ps.setString(9, queue_type);			
//				ps.execute();
//				rps.setSuccess(true);
//			}
			
			if(!rs2.next()){		
				queue_type = StringUtils.isEmpty(queue_type) ? QueueServiceBiz.QUEUE_TYPE_REGULER : queue_type;
				String status = PrepareInitStatus(source);
				String sourceCat = QueueServiceBiz.SOURCE_CAT + source;
				String sql3 = "INSERT INTO tb_queue (resourcemstr,queue_no,location,queue_date, deleted, created_at, status, patient_name, card_no, source, queue_type) "
						+ " OUTPUT Inserted.queue_id "
						+ " VALUES(?,?,?,?,0,SYSDATETIME(),?,?,?,?,?) ";
				
				ps = connection.prepareStatement(sql3);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(10000);
				ps.setString(1, rs.getString("resourcemstr"));
				ps.setString(2, queue_no);
				ps.setString(3, location);
				ps.setString(4, tanggal);
				ps.setString(5, status);	
				ps.setString(6, rs.getString("patient_name"));
				ps.setString(7, rs.getString("card_no"));
				ps.setString(8, sourceCat);
				ps.setString(9, queue_type);			
				ps.execute();
				rps.setSuccess(true);
			}			

			connection.commit();
			
		}catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
	 		if (rs2 != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps2 != null)try{ps.close();}catch (SQLException e) {}
		}	
		return rps;
	}
	
	public static QueueDetails QueueCallKasir(String location,String counter_id) {
		QueueDetails qd =new QueueDetails();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ResultSet rs2 = null;
		PreparedStatement ps2 = null;
		Connection connection = DbConnection.getAntrianInstance();
		
		try{
			connection.setAutoCommit(false);
			String sql = "select top (1) q.queue_id, q.queue_no,q.queue_date,q.resourcemstr,q.patient_name,q.card_no,q.status, st.counter_id "+
					" from tb_queue q inner join tb_serve_time_reg st on st.queue_id = q.queue_id " +
					" where q.location = ? "+
					" and q.status = ? and st.counter_id = ? and CONVERT(date, q.queue_date) = CONVERT(date, CURRENT_TIMESTAMP) "+
					" order by q.queue_id asc";
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, location);
			ps.setString(2, QueueServiceBiz.QUE_REG_CALL);
			ps.setString(3, counter_id);
			rs = ps.executeQuery();
			
			if(rs.next()){
				qd.setQueue_id(rs.getString("queue_id"));
				qd.setQueue_no(rs.getString("queue_no"));
				qd.setQueue_date(rs.getString("queue_date"));
				qd.setCard_no(rs.getString("card_no"));
				qd.setPatient_name(rs.getString("patient_name"));
				qd.setStatus_queue(rs.getString("status"));
				qd.setResourcemstr(rs.getString("resourcemstr"));
				qd.setCounter_id(rs.getString("counter_id"));
			}
			else
			{
				String sql2 = "select top (1) q.queue_id, q.queue_no,q.queue_date,q.resourcemstr,q.patient_name,q.card_no,q.status "+
						" from tb_queue q " +
						" where q.location = ? "+
						" and q.status = ? and CONVERT(date, q.queue_date) = CONVERT(date, CURRENT_TIMESTAMP) "+
						" order by q.queue_id asc";
				
				ps2 = connection.prepareStatement(sql2);
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(5000);
				ps2.setString(1, location);
				ps2.setString(2, QueueServiceBiz.QUE_REG_IDLE);
				rs2 = ps2.executeQuery();
				
				if(rs2.next()){
					qd.setQueue_id(rs2.getString("queue_id"));
					qd.setQueue_no(rs2.getString("queue_no"));
					qd.setQueue_date(rs2.getString("queue_date"));
					qd.setCard_no(rs2.getString("card_no"));
					qd.setPatient_name(rs2.getString("patient_name"));
					qd.setStatus_queue(rs2.getString("status"));
					qd.setResourcemstr(rs2.getString("resourcemstr"));
				}	
			}

			connection.commit();
			
		}catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
	 		if (rs2 != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps2 != null)try{ps.close();}catch (SQLException e) {}
		}	
		return qd;
	}
	
	public static QueueDetails QueueNoByMrn(String noMrn) {
		QueueDetails qd =new QueueDetails();
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = DbConnection.getAntrianInstance();
		
		try{
			connection.setAutoCommit(false);
			String sql = "select q.queue_no, rm.resource_name from tb_queue q "
					+ " left join tb_resourcemstr rm on rm.resourcemstr_id = q.resourcemstr "
					+ " where q.card_no = ? and CONVERT(date, q.queue_date) = CONVERT(date, CURRENT_TIMESTAMP)";
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, noMrn);
			rs = ps.executeQuery();
			
			if(rs.next()){
				qd.setQueue_no(rs.getString("queue_no"));
				qd.setResource_name(rs.getString("resource_name"));
			}
			connection.commit();
			
		}catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
		}	
		return qd;
	}
	
	public static QueueDetails QueueCallSkipKasir(String queue_no, String location) {
		QueueDetails qd =new QueueDetails();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ResultSet rs2 = null;
		PreparedStatement ps2 = null;
		Connection connection = DbConnection.getAntrianInstance();
		
		try{
			connection.setAutoCommit(false);
			String sql = "select top (1) q.queue_id, q.queue_no,q.queue_date,q.resourcemstr,q.patient_name,q.card_no,q.status "
					+ " from tb_queue q where q.queue_no = ? and q.location = ? "
					+ " and (q.status = ? OR q.status = ?) and CONVERT(date, q.queue_date) = CONVERT(date, CURRENT_TIMESTAMP) "
					+ " order by q.queue_id asc";
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(5000);
			ps.setString(1, queue_no);
			ps.setString(2, location);
			ps.setString(3, QueueServiceBiz.QUE_REG_SKIP);
			ps.setString(4, QueueServiceBiz.QUE_REG_CALL);
			rs = ps.executeQuery();
			
			if(rs.next()){
				qd.setQueue_id(rs.getString("queue_id"));
				qd.setQueue_no(rs.getString("queue_no"));
				qd.setQueue_date(rs.getString("queue_date"));
				qd.setCard_no(rs.getString("card_no"));
				qd.setPatient_name(rs.getString("patient_name"));
				qd.setStatus_queue(rs.getString("status"));
				qd.setResourcemstr(rs.getString("resourcemstr"));
			}

			connection.commit();
			
		}catch(SQLException e){
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if(connection != null){
				try{
					System.err.print("Transaction is being rolled back in"+e.getStackTrace()[0].getMethodName());
					connection.rollback();}
				catch(SQLException excep){
					System.out.println(excep.getMessage());
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			try {connection.setAutoCommit(true); } catch (SQLException e) {}
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
	 		if (rs != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps != null)try{ps.close();}catch (SQLException e) {}
	 		if (rs2 != null)try{rs.close();}catch (SQLException e) {}
	 		if (ps2 != null)try{ps.close();}catch (SQLException e) {}
		}	
		return qd;
	}
	
	private static <T> T getMappedClass(HashMap rowInfo, Class<T> clazz) throws JsonProcessingException, IOException{
		T result = null;
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(rowInfo);
		result = mapClass(mapper, json, clazz);
		return result;
	}
	
	private static <T> T mapClass(ObjectMapper mapper, String json, Class<T> clazz) throws JsonProcessingException, IOException
	{
		T result = null;
		if(mapper == null)return result;
		result = mapper.readValue(json, clazz);
		return result;
	}
}
