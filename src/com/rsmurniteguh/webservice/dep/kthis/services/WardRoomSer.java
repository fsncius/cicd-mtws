package com.rsmurniteguh.webservice.dep.kthis.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.ListWardRoom;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class WardRoomSer extends DbConnection {

	public static List<ListWardRoom> getWardRoom() {
		// TODO Auto-generated method stub
		List<ListWardRoom> all=new ArrayList<ListWardRoom>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet haspas=null;
		if(connection==null)return null;
		Statement stmt=null;
		String abdataward="select wm.ward_desc, rm.room_desc, cd.card_no,bm.bed_no,v.ADMISSION_DATETIME,v.discharge_datetime,pgcommon.fxGetCodeDesc(bm.bed_status) AS bad_status, "
				+ "pgcommon.fxGetCodeDesc(v.patient_class) AS patient_clas, acm.accomm_desc, ps.person_name "
				+ "from wardmstr wm inner join roommstr rm on rm.wardmstr_id = wm.wardmstr_id inner join bedmstr bm on bm.roommstr_id = rm.roommstr_id "
				+ "inner join accommmstr acm on acm.accommmstr_id = bm.accommmstr_id left outer join visit v on v.bedmstr_id = bm.bedmstr_id "
				+ "left outer join patient pt on pt.patient_id = v.patient_id left outer join person ps on ps.person_id = pt.person_id left outer join card cd on cd.PERSON_ID = pt.person_id "
				+ "where wm.defunct_ind = 'N' and rm.defunct_ind = 'N' and bm.defunct_ind = 'N'";		
		try{
			stmt=connection.createStatement();
			haspas=stmt.executeQuery(abdataward);
			while(haspas.next())
			{
				ListWardRoom lw= new ListWardRoom();
				lw.setWard_desc(haspas.getString("ward_desc"));
				lw.setMrn(haspas.getString("card_no"));
				lw.setRoom_desc(haspas.getString("room_desc"));
				lw.setBed_no(haspas.getString("bed_no"));
				lw.setAdmission_datetime(haspas.getString("admission_datetime"));
				lw.setBad_status(haspas.getString("bad_status"));
				lw.setPatient_clas(haspas.getString("patient_clas"));
				lw.setAccomm_desc(haspas.getString("accomm_desc"));
				lw.setDischarge_datetime(haspas.getString("discharge_datetime"));
				lw.setPerson_name(haspas.getString("person_name"));
				all.add(lw);
			}
			haspas.close();
			stmt.close();
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

}
