package com.rsmurniteguh.webservice.dep.kthis.services.email;

import com.rsmurniteguh.webservice.dep.all.model.JobsRecruitmentEmail;
import com.rsmurniteguh.webservice.dep.all.model.RegistrasiNewPatient;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.util.EmailUtil;

public class NotificationRegistrationEmail extends EmailUtil {
	private String bodyEmail;
	

	public void sendEmail(RegistrasiNewPatient receipentTo){
		this.receipentTo = receipentTo.getEmail();
		bodyEmail = "<table style='height: 524px; width: 813px;' border='0'>";
		bodyEmail += "<tbody><tr><td style='width: 803px;' colspan='3'><p style='text-align: center;'><strong>REGISTRASI PASIEN BARU</strong></p><p style='text-align: center;'><strong>RUMAH SAKIT MURNI TEGUH</strong></p></td></tr>";
		bodyEmail += "<tr><td style='width: 803px;' colspan='3'><strong>1. Data Pasien</strong></td></tr>";
		bodyEmail += "<tr><td style='width: 263.667px;'>Nama Pasien</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
		bodyEmail += receipentTo.getFullName();
		bodyEmail += "</td></tr>";
		bodyEmail += "<tr><td style='width: 263.667px;'>Tanggal Lahir</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
		bodyEmail += receipentTo.getBirth();
		bodyEmail += "</td></tr>";
		bodyEmail += "<tr><td style='width: 263.667px;'>No. Identitas</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
		bodyEmail += receipentTo.getIdcard();
		bodyEmail += "</td></tr>";
		if (receipentTo.getPasienType().equals(MobileBiz.DEPARTMENT_POLI)) {
			bodyEmail += "<tr><td style='width: 263.667px;'>Tipe Pasien</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";		
			bodyEmail += "GENERAL";
			bodyEmail += "</td></tr>";
		} else if (receipentTo.getPasienType().equals(MobileBiz.DEPARTMENT_COORPORATE)) {
			bodyEmail += "<tr><td style='width: 263.667px;'>Tipe Pasien</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";		
			bodyEmail += "Coorporate";
			bodyEmail += "</td></tr>";
		} else if (receipentTo.getPasienType().equals(MobileBiz.DEPARTMENT_INSURANCE)) {
			bodyEmail += "<tr><td style='width: 263.667px;'>Tipe Pasien</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";		
			bodyEmail += "Insurance";
			bodyEmail += "</td></tr>";
		} else {
			String TipePasien = "BPJS KESEHATAN"; 
			if (receipentTo.getPasienType().equals(MobileBiz.DEPARTMENT_BPJSEX)) {
				TipePasien = "BPJS KESEHATAN";
			}
			bodyEmail += "<tr><td style='width: 263.667px;'>Tipe Pasien</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";		
			bodyEmail += TipePasien;
			bodyEmail += "</td></tr>";
			bodyEmail += "<tr><td style='width: 263.667px;'>No BPJS</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
			bodyEmail += receipentTo.getNoBpjs();
			bodyEmail += "</td></tr>";
			bodyEmail += "<tr><td style='width: 263.667px;'>No Surat Rujukan</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
			bodyEmail += receipentTo.getNoSurat();
			bodyEmail += "</td></tr>";		
			bodyEmail += "<tr><td style='width: 263.667px;'>Tanggal Surat Rujukan</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
			bodyEmail += receipentTo.getTglSurat();
			bodyEmail += "</td></tr>";
		}
		bodyEmail += "<tr><td style='width: 803px;' colspan='3'><strong>2. Data Appointment</strong></td></tr>";
		bodyEmail += "<tr><td style='width: 263.667px;'>Tanggal Registrasi</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
		bodyEmail += receipentTo.getCreatedDate();
		bodyEmail += "</td></tr>";
		bodyEmail += "<tr><td style='width: 263.667px;'>Nama Dokter</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
		bodyEmail += receipentTo.getDoctorName();
		bodyEmail += "</td></tr>";
		bodyEmail += "<tr><td style='width: 263.667px;'>Jadwal Dokter</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
		bodyEmail += receipentTo.getJadwal();
		bodyEmail += "</td></tr>";
		bodyEmail += "<tr><td style='width: 263.667px;'>Tanggal Berobat</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
		bodyEmail += receipentTo.getTglAppointment();
		bodyEmail += "</td></tr>";
		if(receipentTo.getNomrn() != null && !receipentTo.getNomrn().equals("")){
			bodyEmail += "<tr><td style='width: 263.667px;'>No MRN</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'><strong>";
			bodyEmail += receipentTo.getNomrn();
			bodyEmail += "</strong></td></tr>";
		}
		bodyEmail += "<tr><td style='width: 263.667px;'>No Registrasi</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'><strong>";
		bodyEmail += receipentTo.getBarcode();
		bodyEmail += "</strong></td></tr>";
		bodyEmail += "<tr><td style='width: 263.667px;'>Status</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'><strong>";
		bodyEmail += receipentTo.getStatus();
		bodyEmail += "</strong></td>";
		
		bodyEmail += "</tr><tr><td style='width: 263.667px;'>Keterangan</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
		if(receipentTo.getStatus().equals(MobileBiz.APPOINTMENT_STATUS_CONFIRM)){
			bodyEmail += "Registrasi anda telah di konfirmasi.Hadirlah sebelum jam PRAKTEK DOKTER pada tanggal berobat yang telah anda tentukan. ";
		} else if(receipentTo.getStatus().equals(MobileBiz.APPOINTMENT_STATUS_WAIT)){
			bodyEmail += "Menunggu konfirmasi dari pihak rumah sakit Murni Teguh.";
		} else {
			bodyEmail += "Registrasi anda ditolak, silahkan datang langsung ke rumah sakit Murni Teguh.";
		}
		
		bodyEmail += "</td></tr>";
		bodyEmail += "<tr><td style='width: 263.667px;'>Catatan </td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'>";
		bodyEmail += "<p style='text-align: justify;'>";
		if(receipentTo.getStatus().equals(MobileBiz.APPOINTMENT_STATUS_WAIT)){
			bodyEmail += "Mohon catat nomor registrasi dan simpan bukti ini. Silahkan periksa kembali apakah registrasi anda telah di konfirmasi atau belum dengan memasukan nomor registrasi di form <a href='www.rsmurniteguh.com/registrasi#track' target='_blank' style='text-decoration:none;'> TRACK REGISTRASI </a>. Terima kasih atas kepercayaan anda kepada kami.";
		} else if(receipentTo.getStatus().equals(MobileBiz.APPOINTMENT_STATUS_CONFIRM)){
			bodyEmail += "Untuk registrasi selanjutnya, anda dapat mendaftar kembali via <a href='www.rsmurniteguh.com/registrasi' target='_blank' style='text-decoration:none;'>website</a> / aplikasi mobile MURNI TEGUH dengan memasukan No.MRN atau melakukan self-registration dengan cara Scan Barcode dari kartu MRN anda di tempat yang telah disediakan di area registrasi Rumah Sakit.";
		} else{
			if(receipentTo.getAlasan() != null && !receipentTo.getAlasan().equals("")){			
				bodyEmail += receipentTo.getAlasan();
			} else {
				bodyEmail += "Mohon catat nomor registrasi dan simpan bukti ini. Dan silahkan periksa lagi apakah registrasi anda sudah diterima atau belum dengan memasukkan nomor registrasi di form tracking. Terima kasih atas kepercayaan anda kepada kami.";;
			}
		}
		bodyEmail += "</p></td></tr>";
		
		if (receipentTo.getPasienType().equals(MobileBiz.DEPARTMENT_COORPORATE)) {
			bodyEmail += "<tr><td style='width: 263.667px;'>NB :</td><td style='width: 15.7167px;'>&nbsp;</td><td style='width: 511.617px;'><strong>";
			bodyEmail += "Anda harus ke counter RS Murni Teguh untuk proses berkas selanjutnya";
			bodyEmail += "</strong></td></tr>";
		}
		bodyEmail += "<tr><td style='width: 803px;' colspan='3'>&nbsp;</td></tr><tr><td style='width: 803px;' colspan='3'>";
		bodyEmail += "<p style='text-align: center;'><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   Murni Teguh Memorial Hospital <br/>Make Tomorrow More Healthy</em></p>";
		bodyEmail += "</td></tr></tbody></table>";
		
		this.emailSubject = "Registrasi Antrian Rumah Sakit Murni Teguh";
		this.sendMessage(bodyEmail);
	}
	
	public void sendJobsEmail(JobsRecruitmentEmail receipentTo){
		this.receipentTo = receipentTo.getEmail();
		bodyEmail = " <html><body><p>Konfirmasi alamat email<p><p>Untuk menyelesaikan pendaftaran pada https://www.rsmurniteguh.com silakan klik tombol konfirmasi berikut</p><br><p><a href='https://www.rsmurniteguh.com/jobs/konfirm/" + receipentTo.getIdRegist() + "'><button>Konfirmasi Email</button></a></p><br><p>Jika tombol diatas tidak bekerja, silakan salin url berikut pada browser anda :<br><a href='https://www.rsmurniteguh.com/jobs/konfirm/" + receipentTo.getIdRegist() + "'>https://www.rsmurniteguh.com/jobs/konfirm/" + receipentTo.getIdRegist() + " </a></p><br><p>Jika anda merasa belum pernah melakukan pendaftaran pada https://www.rsmurniteguh.com silakan abaikan email ini dan jangan mengklik link konfirmasi diatas.</p><br><p>Terima kasih,<br><b>Rumah Sakit Murni Teguh Memorial Hospital</b><br>Jl. Veteran No.1C Komp. Centre Point</p><br><br><i>Mohon untuk tidak membalas email ini.</i></body></html>";
		this.emailSubject = "Konfirmasi Alamat Email";
		this.sendMessage(bodyEmail);
	}
}
