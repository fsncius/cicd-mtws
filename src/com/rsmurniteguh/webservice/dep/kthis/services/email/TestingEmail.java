package com.rsmurniteguh.webservice.dep.kthis.services.email;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;

import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.RegistrasiNewPatient;
import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;
import com.rsmurniteguh.webservice.dep.util.ColorUtil;
import com.rsmurniteguh.webservice.dep.util.DataUtils;
import com.rsmurniteguh.webservice.dep.util.EmailUtil;

public class TestingEmail extends EmailUtil {
	private static TestingEmail action = new TestingEmail();
	
	public static TestingEmail getInstance(){
		return action;
	}
	
	private String bodyEmail;
	

	public BaseResult sendEmail(){
		BaseResult result = new BaseResult();
		result.setResult(IConstant.IND_YES);
		result.setStatus(IConstant.STATUS_CONNECTED);
		
		try {
			CommonService cs = new CommonService();
			String emailTo = cs.getParameterValue(IParameterConstant.TESTING_EMAIL_TO);
			String emailCC = cs.getParameterValue(IParameterConstant.TESTING_EMAIL_CC);
			String emailBCC = cs.getParameterValue(IParameterConstant.TESTING_EMAIL_BCC);
			
			if (emailTo != null && !emailTo.trim().equals("")) this.receipentTo = emailTo;
			if (emailCC != null && !emailCC.trim().equals("")) this.receipentCC = emailCC;
			if (emailBCC != null && !emailBCC.trim().equals("")) this.receipentBCC = emailBCC;
			
			Message message = PhaseInterceptorChain.getCurrentMessage();
			HttpServletRequest request = (HttpServletRequest)message.get(AbstractHTTPDestination.HTTP_REQUEST);
			bodyEmail = "This email is generated by IP : " + request.getRemoteAddr();
			bodyEmail += "<br />" + "Date : " + DataUtils.getCurrentTimeLog();
		} catch (Exception e){
			System.out.println(e.getMessage());
		}				
		
		this.emailSubject = "Testing Email";
		this.sendMessage(bodyEmail);
		result.setResult(bodyEmail);
		return result;
	}
}
