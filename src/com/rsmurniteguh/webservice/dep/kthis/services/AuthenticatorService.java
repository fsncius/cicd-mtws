package com.rsmurniteguh.webservice.dep.kthis.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base32;
import com.rsmurniteguh.webservice.dep.all.model.ReturnResult;
import com.rsmurniteguh.webservice.dep.all.model.mobile.transaction_emoney;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorConfig.GoogleAuthenticatorConfigBuilder;

public class AuthenticatorService extends DbConnection {
	public static ReturnResult Authenticate(String user, String token) {
		GoogleAuthenticatorConfigBuilder gacb =
                new GoogleAuthenticatorConfigBuilder()
                        .setTimeStepSizeInMillis(TimeUnit.SECONDS.toMillis(30))
                        .setWindowSize(10);
		
		GoogleAuthenticator gAuth = new GoogleAuthenticator(gacb.build());
		 Base32 codec32 = new Base32();
		String a = codec32.encodeAsString("SuperSecretKeyGoesHere".getBytes());
		boolean isCodeValid = gAuth.authorize(a, Integer.parseInt(token));
		
		ReturnResult result = new ReturnResult();
		result.setSuccess(isCodeValid);
		
		return result;
	}
	
	public static Object get_trx_emoney(String nik, String startdate, String enddate) {
		List<transaction_emoney> list = new ArrayList<transaction_emoney>();

		Connection Connection = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		
		try {
			Connection = DbConnection.getTrxInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			String sql = "SELECT CONVERT(VARCHAR(10), t.trx_date, 105) as tgl, t.trx_type, t.trx_type, t.trx_amount, t.new_amount, c.code_desc as descr FROM t_accountbalance a "
					+"INNER JOIN t_accounttrx t ON a.accountbalance_id = t.accountbalance_id "
					+"LEFT JOIN t_code c ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(t.invoice_no,'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9',''),'0','') = c.code "
					+"WHERE nik = ? AND t.trx_date >= ?  AND t.trx_date <= ? " 
					+"ORDER BY t.trx_date DESC";
			
			ps = Connection.prepareStatement(sql);
			ps.setString(1, nik);
			ps.setString(2, startdate);
			ps.setString(3, enddate+" 23:59:59.999");
			rs = ps.executeQuery();
			
			while (rs.next()){
				transaction_emoney dl = new transaction_emoney();
				dl.setTrxtype(rs.getString("trx_type"));
				dl.setTrxdate(rs.getString("tgl"));
				dl.setTrx_amount(rs.getString("trx_amount"));
				dl.setNew_amount(rs.getString("new_amount"));
				dl.setDesc(rs.getString("descr"));
				list.add(dl);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
		}
		return list;
	}
}
