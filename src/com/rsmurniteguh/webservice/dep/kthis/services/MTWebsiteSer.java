package com.rsmurniteguh.webservice.dep.kthis.services;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.activation.DataHandler;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import com.google.gson.Gson;
import com.rsmurniteguh.webservice.dep.all.model.BaseInfo;
import com.rsmurniteguh.webservice.dep.all.model.DataPasien;
import com.rsmurniteguh.webservice.dep.all.model.JobsRecruitmentEmail;
import com.rsmurniteguh.webservice.dep.all.model.JobsRecruitmentRegistration;
import com.rsmurniteguh.webservice.dep.all.model.RegistrasiNewPatient;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.RujukanPcareOrRsByNoka;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteBerita;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteGallery;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteMegazine;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteTahunGallery;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteWhatNew;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataUser;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInfoResponse;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegBpjs;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Metadata;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.kthis.services.email.NotificationRegistrationEmail;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.CommonUtil;
import com.rsmurniteguh.webservice.dep.util.DataUtils;
import com.rsmurniteguh.webservice.dep.util.EncryptPatient;
import com.rsmurniteguh.webservice.dep.util.Encryptor;
import com.rsmurniteguh.webservice.dep.util.FileUploadUtil;

import oracle.sql.DATE;

public class MTWebsiteSer {

	public static final String BASE_FOLDER_UPLOAD = getBaseConfig(IParameterConstant.MTWEBSITE_BASE_FOLDER_UPLOAD);
	
    private static String BPJS_MEMBER_STATUS_NON_ACTIVE_PREMI = "9";
    
	private static String getBaseConfig(String parameter){
		CommonService cs = new CommonService();
		return cs.getParameterValue(parameter);
	}
	
	public static ResponseString upload(String type, String name, List<Attachment> attachments, HttpServletRequest request) {
		System.out.println("upload Image Berita atau Gallery");
		// local variables
		ResponseString res = new ResponseString();
		DataHandler dataHandler = null;
		InputStream inputStream = null;
		String extension = ".jpg";
		type = type.toLowerCase();

		for (Attachment attachment : attachments) {
			dataHandler = attachment.getDataHandler();
			try {
				String base_upload = BASE_FOLDER_UPLOAD;
				/*
				 * CREATE SUB FOLDER
				 */

				SimpleDateFormat tahun = new SimpleDateFormat("yyyy", Locale.US);
				SimpleDateFormat bulan = new SimpleDateFormat("MM", Locale.US);

				Calendar cal = Calendar.getInstance();
				Date date = cal.getTime();
				String subfolder = tahun.format(date)+ "_" + bulan.format(date);
				String createFileName = "MTMH_" + name;
				String folderLocation = base_upload + "\\" + type + "\\" + subfolder;
				folderLocation = DataUtils.ifUnixDir(folderLocation);
				
				File checkOrCreateDir = new File(folderLocation);
				// if the directory does not exist, create it
				if (!checkOrCreateDir.exists()) {
					boolean result = false;
					try {
						checkOrCreateDir.mkdir();
						result = true;
					} catch (SecurityException se) {
						res.setResponse("FAIL");
						se.printStackTrace();
					}
				}

				String CompletefileName = base_upload + "\\" + type +  "\\" + subfolder + "\\" + createFileName;
				int i = 1;
				String temp = createFileName;
				File f = new File(base_upload + "\\" + type +  "\\" + subfolder + "\\" + createFileName + extension);
				while (f.exists() && !f.isDirectory()) {
					i++;
					f = new File(base_upload + "\\" + type +  "\\" + subfolder + "\\" + createFileName + "_" + i + extension);
					temp = createFileName + "_" + i;
				}
				CompletefileName = base_upload + "\\" + type +  "\\" + subfolder + "\\" + temp + extension;
				CompletefileName = DataUtils.ifUnixDir(CompletefileName);
				
				String saveLocation = subfolder + "\\" + temp + extension;

				// write & upload file to server
				inputStream = dataHandler.getInputStream();
				BufferedImage image = ImageIO.read(inputStream);

				File output = new File(CompletefileName);
				OutputStream out = new FileOutputStream(output);

				ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
				ImageOutputStream ios = ImageIO.createImageOutputStream(out);
				writer.setOutput(ios);

				ImageWriteParam param = writer.getDefaultWriteParam();
				if (param.canWriteCompressed()) {
					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
					param.setCompressionQuality(0.7f);
				}

				writer.write(null, new IIOImage(image, null, null), param);

				out.close();
				ios.close();
				writer.dispose();

				res.setResponse(saveLocation);

			} catch (IOException ioex) {
				res.setResponse("FAIL");
				ioex.printStackTrace();
			} catch (Exception e) {
				res.setResponse("FAIL");
				e.printStackTrace();
			}
		}
		return res;
	}

	public static Response download(String gambar_id, String type, HttpServletRequest request) {
		System.out.println("download Image Berita atau Gallery");
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;
		String base_upload = BASE_FOLDER_UPLOAD;

		type = type.toLowerCase();
		
		String sql = "SELECT nama as images FROM gambar where id_gambar = ? ";
		if (type.equals(MobileBiz.mBerita)) {
			sql = "SELECT gambar as images FROM tb_berita where berita_id = ? ";
		}

		String file_loc = "";
		PreparedStatement ps = null;
		ResponseBuilder responseBuilder = Response.ok("");

		String ip_remote = request.getRemoteAddr();
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, gambar_id);
			rs = ps.executeQuery();

			file_loc = BASE_FOLDER_UPLOAD+"\\not_found.jpg";
			if (rs.next()) {
				file_loc = base_upload + "\\" + type + "\\" + rs.getString("images");
			}
			
			File file = FileUploadUtil.getInstance().getFileFromServer(file_loc);
			responseBuilder = Response.ok(file);
			if (!MobileBiz.REMOTE_IP_WHITELIST.contains(ip_remote)) {
				responseBuilder.header("Content-Disposition", "attachment; filename=\"image.jpg\"");
			}

		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)	try {	rs.close();	} catch (Exception ignore) {}
			if (ps != null)	try {		ps.close();		} catch (Exception ignore) {}
			if (connection != null)	try { connection.close();} catch (Exception ignore) {}
		}
		return responseBuilder.build();
	}
	
		
	public static List<WebsiteBerita> getBerita(String kategori, String type, String tags, String status) {
		System.out.println("get Berita Website MTMH...");
		List<WebsiteBerita> data = new ArrayList<WebsiteBerita>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;
		String sql = null;
		if(!status.equals("") && status != null){
			sql = "SELECT berita_id,judul,tgl,penulis,isi,gambar,link,tags,kategori,created_by,created_at,update_by,update_at,status,viewer FROM tb_berita brt "
					+ "WHERE brt.kategori = ? AND brt.tags != ? AND brt.status = 'FALSE' ";
			if(!tags.equals("") && tags != null){
				sql = "SELECT berita_id,judul,tgl,penulis,isi,gambar,link,tags,kategori,created_by,created_at,update_by,update_at,status,viewer FROM tb_berita brt "
						+ "WHERE brt.kategori = ? AND brt.tags like ? AND brt.status = 'FALSE' ";
			}
		} else{
			sql = "SELECT berita_id,judul,tgl,penulis,isi,gambar,link,tags,kategori,created_by,created_at,update_by,update_at,status,viewer FROM tb_berita brt "
					+ "WHERE brt.status = 'FALSE' ";
		}
		if (!type.equals("") && type != null) {
			sql = sql + " ORDER BY brt.viewer DESC";
		} else{
			sql = sql + " ORDER BY brt.berita_id DESC";
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			if(!status.equals("")){
				ps.setString(1, kategori);
				ps.setString(2, "%"+tags+"%");
			}
			rs = ps.executeQuery();

			while (rs.next()) {
				WebsiteBerita dl = new WebsiteBerita();
				dl.setBerita_id(rs.getString("berita_id"));
				dl.setJudul(rs.getString("judul"));
				dl.setTgl(rs.getString("tgl"));
				dl.setPenulis(rs.getString("penulis"));
				dl.setIsi(rs.getString("isi"));
				dl.setGambar(rs.getString("gambar"));
				dl.setLink(rs.getString("link"));
				dl.setTags(rs.getString("tags"));
				dl.setKategori(rs.getString("kategori"));
				dl.setViewer(rs.getString("viewer"));				
				data.add(dl);
			}
			rs.close();
			ps.close();
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static List<WebsiteBerita> getJudulBerita(String judul, String kategori) {
		System.out.println("get Berita Website MTMH...");
		List<WebsiteBerita> data = new ArrayList<WebsiteBerita>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;
		
		String sql = "SELECT berita_id,judul,tgl,penulis,isi,gambar,link,tags,kategori,status,viewer FROM tb_berita brt "
				+ "WHERE brt.judul = ? AND brt.kategori = ? AND brt.status = 'FALSE' ";
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, judul);
			ps.setString(2, kategori);
			rs = ps.executeQuery();

			while (rs.next()) {
				WebsiteBerita dl = new WebsiteBerita();
				dl.setBerita_id(rs.getString("berita_id"));
				dl.setJudul(rs.getString("judul"));
				dl.setTgl(rs.getString("tgl"));
				dl.setPenulis(rs.getString("penulis"));
				dl.setIsi(rs.getString("isi"));
				dl.setGambar(rs.getString("gambar"));
				dl.setLink(rs.getString("link"));
				dl.setTags(rs.getString("tags"));
				dl.setKategori(rs.getString("kategori"));
				dl.setStatus(rs.getString("status"));
				dl.setViewer(rs.getString("viewer"));				
				data.add(dl);
			}
		
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
		
	public static ResponseStatus getSaveViewerBerita(String ids, String jumlah) {
		System.out.println("Save Viewer Berita...");
		ResponseStatus data = new ResponseStatus();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;

		String sql = "UPDATE tb_berita SET viewer=? where berita_id = ? ";
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, jumlah);
			ps.setString(2, ids);
			ps.executeUpdate();
			data.setMessage("Data berhasil di simpan.");
			data.setSuccess(true);
			ps.close();
		
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static List<WebsiteMegazine> getMegazine() {
		System.out.println("get Megazine Website MTMH...");
		List<WebsiteMegazine> data = new ArrayList<WebsiteMegazine>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;
		String sql = "SELECT id_megazine,edisi,download,folder FROM megazine mgz "
					+ "ORDER by mgz.id_megazine DESC";
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			rs = ps.executeQuery();

			while (rs.next()) {
				WebsiteMegazine dl = new WebsiteMegazine();
				dl.setId_megazine(rs.getString("id_megazine"));
				dl.setEdisi(rs.getString("edisi"));
				dl.setDownload(rs.getString("download"));
				dl.setFolder(rs.getString("folder"));				
				data.add(dl);
			}
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static ResponseStatus SaveReadOnline(String firstname, String lastname, String email, String phone){
		
		System.out.println("Save Readonline Megazine...");
		ResponseStatus data = new ResponseStatus();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;

		String sql = "INSERT INTO readonline (firstname,lastname,email,phone,created_date) "
				+ "VALUES (?,?,?,?,SYSDATETIME())";
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, firstname);
			ps.setString(2, lastname);
			ps.setString(3, email);
			ps.setString(4, phone);
			ps.executeUpdate();
			data.setMessage("Data berhasil di simpan.");
			data.setSuccess(true);
			ps.close();		
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static ResponseStatus SaveKonsultasi(String fullname,String email, String phone, String gender, String blood, String height, String weight, String age, String jobs, String complaint, String spesialis){
		System.out.println("Save Konsulatsi Online...");
		ResponseStatus data = new ResponseStatus();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;

		String sql = "INSERT INTO konsultasi (spesialis,nama,email,telpon,jenis_kelamin,golongan_darah,tinggi_badan,berat_badan,usia,pekerjaan,keluhan,created_date) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,SYSDATETIME())";
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, spesialis);
			ps.setString(2, fullname);
			ps.setString(3, email);
			ps.setString(4, phone);
			ps.setString(5, gender);
			ps.setString(6, blood);
			ps.setString(7, height);
			ps.setString(8, weight);
			ps.setString(9, age);
			ps.setString(10, jobs);
			ps.setString(11, complaint);
			ps.executeUpdate();
			data.setMessage("Data berhasil di simpan.");
			data.setSuccess(true);
			ps.close();		
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static List<WebsiteWhatNew> getWhatNew(String code, String version, String type){
		System.out.println("get Berita Website MTMH...");
		List<WebsiteWhatNew> data = new ArrayList<WebsiteWhatNew>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;
		
		String sql = "SELECT w.subject,w.go_live_date FROM tb_project p INNER JOIN tb_what_new w ON p.project_id=w.project_id "
				+ "WHERE p.code = ? AND w.version = ? AND w.status = ? AND w.deleted = 'FALSE' ";
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, code);
			ps.setString(2, version);
			ps.setString(3, type);
			rs = ps.executeQuery();

			while (rs.next()) {
				WebsiteWhatNew dl = new WebsiteWhatNew();
				dl.setSubject(rs.getString("subject"));
				dl.setGo_live_date(rs.getString("go_live_date"));			
				data.add(dl);
			}		
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}

	public static List<WebsiteTahunGallery> getTahunGallery() {
		System.out.println("get Tahun Gallery Website MTMH...");
		List<WebsiteTahunGallery> data = new ArrayList<WebsiteTahunGallery>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;
		String sql = "SELECT DATEPART(year, g.tanggal_kegiatan) AS tahun FROM kegiatan g group by DATEPART(year, g.tanggal_kegiatan) ORDER BY DATEPART(year, g.tanggal_kegiatan) desc";
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			rs = ps.executeQuery();
			while (rs.next()) {
				WebsiteTahunGallery dl = new WebsiteTahunGallery();
				dl.setTahun(rs.getInt("tahun"));				
				data.add(dl);
			}
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}

	public static List<WebsiteTahunGallery> getJenisGallery() {
		System.out.println("get Jenis Gallery Website MTMH...");
		List<WebsiteTahunGallery> data = new ArrayList<WebsiteTahunGallery>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;
		String sql = "SELECT jenis_kegiatan AS jenis FROM kegiatan g group by g.jenis_kegiatan ORDER BY g.jenis_kegiatan asc";
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			rs = ps.executeQuery();
			while (rs.next()) {
				WebsiteTahunGallery dl = new WebsiteTahunGallery();
				dl.setJenis(rs.getString("jenis"));			
				data.add(dl);
			}
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}

	public static List<WebsiteGallery>getGallerybyTahun(String tahun){
		System.out.println("get Gallery by Tahun Website MTMH...");
		List<WebsiteGallery> data = new ArrayList<WebsiteGallery>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;
		String sql = "SELECT g.id_kegiatan,g.nama_kegiatan,g.jenis_kegiatan,g.tanggal_kegiatan,g.deskripsi,g.lokasi, (select top 1 id_gambar from gambar where id_kegiatan_fk=id_kegiatan) as gambar "
				+ "FROM kegiatan g WHERE DATEPART(year, g.tanggal_kegiatan) = ? order by id_kegiatan desc";
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tahun);
			rs = ps.executeQuery();

			while (rs.next()) {
				WebsiteGallery dl = new WebsiteGallery();
				dl.setId_kegiatan(rs.getString("id_kegiatan"));
				dl.setNama_kegiatan(rs.getString("nama_kegiatan"));
				dl.setJenis_kegiatan(rs.getString("jenis_kegiatan"));
				dl.setTanggal_kegiatan(rs.getString("tanggal_kegiatan"));
				dl.setDeskripsi(rs.getString("deskripsi"));
				dl.setLokasi(rs.getString("lokasi"));
				dl.setGambar(rs.getString("gambar"));
				data.add(dl);
			}
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static List<WebsiteGallery>getGallerybyJudul(String judul){
		System.out.println("get Gallery by Judul Website MTMH...");
		List<WebsiteGallery> data = new ArrayList<WebsiteGallery>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;		
		PreparedStatement ps = null;
		String sql = "SELECT g.id_kegiatan,g.nama_kegiatan,g.jenis_kegiatan,g.tanggal_kegiatan,g.deskripsi,g.lokasi, i.id_gambar "
				+ "FROM kegiatan g INNER JOIN gambar i ON g.id_kegiatan= i.id_kegiatan_fk "
				+ "WHERE g.nama_kegiatan = ? ORDER BY i.id_gambar asc";
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, judul);
			rs = ps.executeQuery();

			while (rs.next()) {
				WebsiteGallery dl = new WebsiteGallery();
				dl.setId_kegiatan(rs.getString("id_kegiatan"));
				dl.setNama_kegiatan(rs.getString("nama_kegiatan"));
				dl.setJenis_kegiatan(rs.getString("jenis_kegiatan"));
				dl.setTanggal_kegiatan(rs.getString("tanggal_kegiatan"));
				dl.setDeskripsi(rs.getString("deskripsi"));
				dl.setLokasi(rs.getString("lokasi"));
				dl.setGambar(rs.getString("id_gambar"));
				data.add(dl);
			}
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	
	public static ResponseStatus addPasienBaru(RegistrasiNewPatient registrasiNewPatient) {
		ResponseStatus data = new ResponseStatus();
		RegistrasiNewPatient dataemail = new RegistrasiNewPatient();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		ResultSet rs5 = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		data.setMessage("FAIL");
		data.setSuccess(false);
		Boolean ProsesType = false;
		String user_id = null;
		if(registrasiNewPatient.getPasienJenis().equals(MobileBiz.IND_Y) && registrasiNewPatient.getNomrn() != null )
		{
			String sqlCekUser = "SELECT user_id FROM tb_user "
					+ "WHERE no_mrn = ? AND no_ktp = ? AND no_bpjs = ?";
			try{
				ps5 = connection.prepareStatement(sqlCekUser);
				ps5.setEscapeProcessing(true);
				ps5.setQueryTimeout(60000);
				ps5.setString(1, registrasiNewPatient.getNomrn());
				ps5.setString(2, registrasiNewPatient.getIdcard());
				ps5.setString(3, registrasiNewPatient.getNoBpjs());
				rs5 = ps5.executeQuery();
				if(rs5.next()){
					ProsesType = true;
					user_id = rs5.getString("user_id");
				} 
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			
		}
		try {
			if(ProsesType == true){
				ps5 = null;
				ps4 = null;
				rs4 = null;
				rs5 = null;
				connection.setAutoCommit(false);
				String referenceid = null;
				if (registrasiNewPatient.getPasienType().equals(MobileBiz.DEPARTMENT_BPJS) || registrasiNewPatient.getPasienType().equals(MobileBiz.DEPARTMENT_BPJSEX)) {
					String sqlRujukan = "INSERT INTO tb_rujukan (no_mrn,images,reference_date,reference,created_at,created_by,source,reference_by) "
							+ "OUTPUT INSERTED.reference_id VALUES (?,?,?,?,SYSDATETIME(),?,?,?)";
					try{
						ps2 = connection.prepareStatement(sqlRujukan);
						ps2.setEscapeProcessing(true);
						ps2.setQueryTimeout(60000);
						ps2.setString(1, registrasiNewPatient.getNomrn());
						ps2.setString(2, registrasiNewPatient.getFileSurat());
						ps2.setString(3, registrasiNewPatient.getTglSurat());
						ps2.setString(4, registrasiNewPatient.getNoSurat());
						ps2.setString(5, user_id);
						ps2.setString(6, MobileBiz.WEB_SOURCE);
						ps2.setString(7, registrasiNewPatient.getTipeSurat());
						rs2 = ps2.executeQuery();
						if (rs2.next()) {
							referenceid = rs2.getString("reference_id");
						}
					}
					catch (SQLException e) {
						System.out.println(e.getMessage());
						connection.rollback();
					}
				}
				
				String sql = "INSERT INTO tb_registrasi (resourcemstr_id,careprovider_id,doctor_name,queue_date,no_mrn,user_id,created_at,deleted,sort,status,reference_id,reference_date,department,source) "
						+ "OUTPUT INSERTED.registrasi_id VALUES (?,?,?,?,?,?,SYSDATETIME(),0,0,?,?,?,?,?)";
					connection.setAutoCommit(false);
					ps = connection.prepareStatement(sql);
					ps.setEscapeProcessing(true);
					ps.setQueryTimeout(60000);
					ps.setString(1, registrasiNewPatient.getResourceMstrId());
					ps.setString(2, registrasiNewPatient.getCareProviderId());
					ps.setString(3, registrasiNewPatient.getDoctorName());
					ps.setString(4, registrasiNewPatient.getTglAppointment());
					ps.setString(5, registrasiNewPatient.getNomrn());
					ps.setString(6, user_id);
					ps.setString(7, MobileBiz.APPOINTMENT_STATUS_WAIT);
					ps.setString(8, referenceid);
					ps.setString(9, registrasiNewPatient.getTglSurat());
					ps.setString(10, registrasiNewPatient.getPasienType());
					ps.setString(11, MobileBiz.WEB_SOURCE);
					rs = ps.executeQuery();
					if (rs.next()) {
						String regid = rs.getString("registrasi_id");
						String status = MobileBiz.APPOINTMENT_STATUS_WAIT;
						if(registrasiNewPatient.getPasienType().equals(MobileBiz.DEPARTMENT_POLI)){							
							String SqlGenerate = "exec generateBarcode ?";
							try{
								ps1 = connection.prepareStatement(SqlGenerate);
								ps1.setEscapeProcessing(true);
								ps1.setQueryTimeout(60000);
								ps1.setString(1, regid);
								ps1.executeUpdate();
								status = MobileBiz.APPOINTMENT_STATUS_CONFIRM;							
							}
							catch (SQLException e) {
								System.out.println(e.getMessage());
								connection.rollback();
							}
						} else {
							String SqlGenerate = "exec genBarcodeNewPasien ?";
							try{
								ps1 = connection.prepareStatement(SqlGenerate);
								ps1.setEscapeProcessing(true);
								ps1.setQueryTimeout(60000);
								ps1.setString(1, regid);
								ps1.executeUpdate();
							}catch (SQLException e) {
								System.out.println(e.getMessage());
								connection.rollback();
							}
						}
						String sqlBarcode = "SELECT generate_barcode FROM tb_registrasi "
								+ "WHERE registrasi_id = ?";
						try{
							ps3 = connection.prepareStatement(sqlBarcode);
							ps3.setEscapeProcessing(true);
							ps3.setQueryTimeout(60000);
							ps3.setString(1, regid);
							rs3 = ps3.executeQuery();
							if(rs3.next()){
								String barcode = rs3.getString("generate_barcode");
								String currentTimeString = new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime()).toString();
								String jadwal = registrasiNewPatient.getJadwal().replace("\r \n ","<br/>");
								dataemail.setBarcode(barcode);
								dataemail.setFullName(registrasiNewPatient.getFullName());
								dataemail.setEmail(registrasiNewPatient.getEmail());
								dataemail.setBirth(registrasiNewPatient.getBirth());
								dataemail.setIdcard(registrasiNewPatient.getIdcard());
								dataemail.setPasienType(registrasiNewPatient.getPasienType());
								dataemail.setNoBpjs(registrasiNewPatient.getNoBpjs());
								dataemail.setNoSurat(registrasiNewPatient.getNoSurat());
								dataemail.setTglSurat(registrasiNewPatient.getTglSurat());
								dataemail.setCreatedDate(currentTimeString);
								dataemail.setDoctorName(registrasiNewPatient.getDoctorName());
								dataemail.setTglAppointment(registrasiNewPatient.getTglAppointment());
								dataemail.setNomrn(registrasiNewPatient.getNomrn());
								dataemail.setStatus(status);
								dataemail.setJadwal(jadwal);
								dataemail.setSuccess(true);
								if(referenceid != null){
									regid = referenceid;
								}
								data.setSuccess(true);
								data.setResponse(regid);
								data.setMessage(barcode);
								connection.commit();
								try{
									NotificationRegistrationEmail emailTask = new NotificationRegistrationEmail();
									emailTask.sendEmail(dataemail);
								}catch (Exception ignore) {}
							}
						}
						catch (SQLException e) {
							System.out.println(e.getMessage());
							connection.rollback();
						}
					}
			} else {
				String sqlUser = "INSERT INTO tb_person (tipeid,idcard,departement,no_mrn,no_bpjs,fullname,birth,contact,email,religion,jenis,suku,pendidikan,alamat,kota,provinsi,time_scheduler,created_date) "
						+ "OUTPUT INSERTED.person_id VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATETIME())";
				connection.setAutoCommit(false);
				ps = connection.prepareStatement(sqlUser);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				ps.setString(1, registrasiNewPatient.getTipecard());
				ps.setString(2, registrasiNewPatient.getIdcard());
				ps.setString(3, registrasiNewPatient.getPasienType());
				ps.setString(4, registrasiNewPatient.getNomrn());
				ps.setString(5, registrasiNewPatient.getNoBpjs());
				ps.setString(6, registrasiNewPatient.getFullName());
				ps.setString(7, registrasiNewPatient.getBirth());
				ps.setString(8, registrasiNewPatient.getPhone());
				ps.setString(9, registrasiNewPatient.getEmail());			
				ps.setString(10, registrasiNewPatient.getAgama());
				ps.setString(11, registrasiNewPatient.getJenisKelamin());
				ps.setString(12, registrasiNewPatient.getEtnis());
				ps.setString(13, registrasiNewPatient.getPendidikan());
				ps.setString(14, registrasiNewPatient.getAlamat());
				ps.setString(15, registrasiNewPatient.getKota());
				ps.setString(16, registrasiNewPatient.getProvinsi());
				ps.setString(17, registrasiNewPatient.getJadwal());
				rs = ps.executeQuery();
				String personid = null;
				String referenceid = null;
				if (rs.next()) {
					personid = rs.getString("person_id");	
					if (registrasiNewPatient.getPasienType().equals(MobileBiz.DEPARTMENT_BPJS) || registrasiNewPatient.getPasienType().equals(MobileBiz.DEPARTMENT_BPJSEX)) {
						String sqlRujukan = "INSERT INTO tb_rujukan (no_mrn,images,reference_date,reference,created_at,created_by,source,reference_by) "
								+ "OUTPUT INSERTED.reference_id VALUES (?,?,?,?,SYSDATETIME(),?,?,?)";
						try{
							ps2 = connection.prepareStatement(sqlRujukan);
							ps2.setEscapeProcessing(true);
							ps2.setQueryTimeout(60000);
							ps2.setString(1, registrasiNewPatient.getNomrn());
							ps2.setString(2, registrasiNewPatient.getFileSurat());
							ps2.setString(3, registrasiNewPatient.getTglSurat());
							ps2.setString(4, registrasiNewPatient.getNoSurat());
							ps2.setString(5, personid);
							ps2.setString(6, MobileBiz.WEB_SOURCE);
							ps2.setString(7, registrasiNewPatient.getTipeSurat());
							rs2 = ps2.executeQuery();
							if (rs2.next()) {
								referenceid = rs2.getString("reference_id");
							}
						}
						catch (SQLException e) {
							System.out.println(e.getMessage());
							connection.rollback();
						}
					}
					
					String sqlRegistrasi = "INSERT INTO tb_registrasi (queue_date,resourcemstr_id,careprovider_id,doctor_name,no_mrn,created_at,status,reference_id,reference_date,department,source,person_id) "
							+ "OUTPUT INSERTED.registrasi_id VALUES (?,?,?,?,?,SYSDATETIME(),?,?,?,?,?,?)";
					try{
						ps1 = connection.prepareStatement(sqlRegistrasi);
						ps1.setEscapeProcessing(true);
						ps1.setQueryTimeout(60000);
						ps1.setString(1, registrasiNewPatient.getTglAppointment());
						ps1.setString(2, registrasiNewPatient.getResourceMstrId());
						ps1.setString(3, registrasiNewPatient.getCareProviderId());
						ps1.setString(4, registrasiNewPatient.getDoctorName());	
						ps1.setString(5, registrasiNewPatient.getNomrn());
						ps1.setString(6, MobileBiz.APPOINTMENT_STATUS_WAIT);
						ps1.setString(7, referenceid);
						ps1.setString(8, registrasiNewPatient.getTglSurat());
						ps1.setString(9, registrasiNewPatient.getPasienType());
						ps1.setString(10, MobileBiz.WEB_SOURCE);
						ps1.setString(11, personid);
						rs1 = ps1.executeQuery();
						if (rs1.next()) {
							String regid = rs1.getString("registrasi_id");
							String status = MobileBiz.APPOINTMENT_STATUS_WAIT;
//							if(registrasiNewPatient.getPasienType().equals(MobileBiz.DEPARTMENT_POLI)){							
//								String SqlGenerate = "exec generateBarcode ?";
//								try{
//									ps3 = connection.prepareStatement(SqlGenerate);
//									ps3.setEscapeProcessing(true);
//									ps3.setQueryTimeout(60000);
//									ps3.setString(1, regid);
//									ps3.executeUpdate();
//									status = MobileBiz.APPOINTMENT_STATUS_CONFIRM;
//									ketstatus = MobileBiz.APPOINTMENT_KET_CONFIRM;								
//								}
//								catch (SQLException e) {
//									System.out.println(e.getMessage());
//									connection.rollback();
//								}
//							} else {
								String SqlGenerate = "exec genBarcodeNewPasien ?";
								try{
									ps3 = connection.prepareStatement(SqlGenerate);
									ps3.setEscapeProcessing(true);
									ps3.setQueryTimeout(60000);
									ps3.setString(1, regid);
									ps3.executeUpdate();
								}catch (SQLException e) {
									System.out.println(e.getMessage());
									connection.rollback();
								}
//							}
							String sqlBarcode = "SELECT generate_barcode FROM tb_registrasi "
									+ "WHERE registrasi_id = ?";
							try{
								ps4 = connection.prepareStatement(sqlBarcode);
								ps4.setEscapeProcessing(true);
								ps4.setQueryTimeout(60000);
								ps4.setString(1, regid);
								rs4 = ps4.executeQuery();
								if(rs4.next()){
									String barcode = rs4.getString("generate_barcode");
									String currentTimeString = new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime()).toString();
									String jadwal = registrasiNewPatient.getJadwal().replace("\r \n ","<br/>");
									dataemail.setBarcode(barcode);
									dataemail.setFullName(registrasiNewPatient.getFullName());
									dataemail.setEmail(registrasiNewPatient.getEmail());
									dataemail.setBirth(registrasiNewPatient.getBirth());
									dataemail.setIdcard(registrasiNewPatient.getIdcard());
									dataemail.setPasienType(registrasiNewPatient.getPasienType());
									dataemail.setNoBpjs(registrasiNewPatient.getNoBpjs());
									dataemail.setNoSurat(registrasiNewPatient.getNoSurat());
									dataemail.setTglSurat(registrasiNewPatient.getTglSurat());
									dataemail.setCreatedDate(currentTimeString);
									dataemail.setDoctorName(registrasiNewPatient.getDoctorName());
									dataemail.setTglAppointment(registrasiNewPatient.getTglAppointment());
									dataemail.setStatus(status);
									dataemail.setJadwal(jadwal);
									dataemail.setSuccess(true);
									if(referenceid != null){
										regid = referenceid;
									}
									data.setSuccess(true);
									data.setResponse(regid);
									data.setMessage(barcode);
									connection.commit();
									try{
										NotificationRegistrationEmail emailTask = new NotificationRegistrationEmail();
										emailTask.sendEmail(dataemail);
									}catch (Exception ignore) {}
								}
							}
							catch (SQLException e) {
								System.out.println(e.getMessage());
								connection.rollback();
							}
							
						}
					}
					catch (SQLException e) {
						System.out.println(e.getMessage());
						connection.rollback();
					}
				}
			}
		}	
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (rs1!=null) try  { rs1.close(); } catch (Exception ignore){}
		    if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		    if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		    if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		    if (rs3!=null) try  { rs3.close(); } catch (Exception ignore){}
		    if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}
		    if (rs4!=null) try  { rs4.close(); } catch (Exception ignore){}
		    if (ps4!=null) try  { ps4.close(); } catch (Exception ignore){}
		    if (rs5!=null) try  { rs5.close(); } catch (Exception ignore){}
		    if (ps5!=null) try  { ps5.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
		
	}
	
	public static RegistrasiNewPatient getPasienBaru(String barcode){
		System.out.println("get Barcode Pasien Baru ...");
		RegistrasiNewPatient data = new RegistrasiNewPatient();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		
		String sqlBarcode = "SELECT registrasi_id,queue_no,queue_date,resourcemstr_id,careprovider_id,doctor_name,no_mrn,created_at,status,generate_barcode,reference_id,reference_date,department,source,person_id,pasien_id FROM tb_registrasi "
				+ "WHERE generate_barcode = ?";		
		try {
			ps = connection.prepareStatement(sqlBarcode);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, barcode);
			rs = ps.executeQuery();
			if (rs.next()) {
				String personid = rs.getString("person_id");				
				String registrasi_id = rs.getString("registrasi_id");
				String reference_id = rs.getString("reference_id");
				String department = rs.getString("department");				
				String status = rs.getString("status");
				String resourcemstr = rs.getString("resourcemstr_id");
				String careprovider = rs.getString("careprovider_id");
				String alasan = null;
				String norujukan = null;
				if (department.equals(MobileBiz.DEPARTMENT_BPJS) || department.equals(MobileBiz.DEPARTMENT_BPJSEX)) {
					if(status.equals(MobileBiz.APPOINTMENT_STATUS_DELETE)){
						String sqlAlasan = "SELECT keterangan FROM tb_alasan_tolak "
							+ "WHERE registrasi_id = ?";
						try{
							ps2 = connection.prepareStatement(sqlAlasan);
							ps2.setEscapeProcessing(true);
							ps2.setQueryTimeout(60000);
							ps2.setString(1, registrasi_id);
							rs2 = ps2.executeQuery();
							if (rs2.next()) {
								alasan = rs2.getString("keterangan");
								status = MobileBiz.APPOINTMENT_STATUS_REJECT;
							}
							ps2.close();
						}
						catch (SQLException e) {
							System.out.println(e.getMessage());
						}
					}
					String sqlRujukan = "SELECT reference FROM tb_rujukan "
							+ "WHERE reference_id = ?";
					try{
						ps3 = connection.prepareStatement(sqlRujukan);
						ps3.setEscapeProcessing(true);
						ps3.setQueryTimeout(60000);
						ps3.setString(1, reference_id);
						rs3 = ps3.executeQuery();
						if (rs3.next()) {
							norujukan = rs3.getString("reference");
						}
						ps3.close();
					}
					catch (SQLException e) {
						System.out.println(e.getMessage());
					}
					
				}			
				String sqlUser = "SELECT idcard,no_bpjs,fullname,birth,email,time_scheduler FROM tb_person "
						+ "WHERE person_id = ? ";
				try{
					ps1 = connection.prepareStatement(sqlUser);
					ps1.setEscapeProcessing(true);
					ps1.setQueryTimeout(60000);
					ps1.setString(1, personid);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {

						String jadwal = rs1.getString("time_scheduler");
						jadwal = jadwal.replace("\r \n ","<br/>");
						data.setRegistrasiId(registrasi_id);
						data.setBarcode(barcode);
						data.setNoqueue(rs.getString("queue_no"));
						data.setTglAppointment(rs.getString("queue_date"));
						data.setResourceMstrId(resourcemstr);
						data.setCareProviderId(careprovider);
						data.setDoctorName(rs.getString("doctor_name"));
						data.setNomrn(rs.getString("no_mrn"));
						data.setCreatedDate(rs.getString("created_at"));
						data.setStatus(status);
						data.setTglSurat(rs.getString("reference_date"));
						data.setPasienType(rs.getString("department"));
						data.setSource(rs.getString("source"));
						data.setIdcard(rs1.getString("idcard"));
						data.setNoBpjs(rs1.getString("no_bpjs"));
						data.setFullName(rs1.getString("fullname"));
						data.setBirth(rs1.getString("birth"));
						data.setEmail(rs1.getString("email"));
						data.setJadwal(jadwal);
						data.setAlasan(alasan);
						data.setNoSurat(norujukan);
						data.setSuccess(true);
					} else {
						data.setAlasan("FAIL");
						data.setSuccess(false);
					}
					ps1.close();
				}
				catch (SQLException e) {
					System.out.println(e.getMessage());
				}
				
			} else {
				data.setAlasan("FAIL");
				data.setSuccess(false);
			}
			ps.close();
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (rs1!=null) try  { rs1.close(); } catch (Exception ignore){}
		    if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		    if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		    if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		    if (rs3!=null) try  { rs3.close(); } catch (Exception ignore){}
		    if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static BaseInfo getCheckPasienBpjsbyNoBpjs(String nobpjs) {
        BaseInfo data=new BaseInfo();
        Connection connection= null;
        ResultSet rs=null;
        try{
	        String bpjs = BpjsService2.GetBpjsInfobyNoka(nobpjs);
	        Gson gson = new Gson();
	        BpjsInfoResponse memberInfo = gson.fromJson(bpjs, BpjsInfoResponse.class); 
	        Metadata metadata = memberInfo.getMetadata();
	        if(metadata.getCode().equals("200") && metadata.getMessage().equals("OK")) {
	        	String kodeStatusPeserta = memberInfo.getResponse().getPeserta().getStatusPeserta().getKode();
	            Boolean isBpjsUserActive = !kodeStatusPeserta.equals(BPJS_MEMBER_STATUS_NON_ACTIVE_PREMI);
	            String bpjsUserStatusMessage = memberInfo.getResponse().getPeserta().getStatusPeserta().getKeterangan();
	            if(isBpjsUserActive){
	            	data.setPatientId(memberInfo.getResponse().getPeserta().getNik());
                    data.setPERSON_NAME(memberInfo.getResponse().getPeserta().getNama());
                    data.setSEX(memberInfo.getResponse().getPeserta().getSex());
                    data.setBIRTH_DATE(memberInfo.getResponse().getPeserta().getTglLahir());
                    data.setPATIENT_CLASS(memberInfo.getResponse().getPeserta().getPisa()); 
                    data.setNoHP(memberInfo.getResponse().getPeserta().getMr().getNoTelepon());
                    data.setAsalRujuk(memberInfo.getResponse().getPeserta().getProvUmum().getKdProvider());
                    data.setPpkrujukan(memberInfo.getResponse().getPeserta().getProvUmum().getNmProvider());
                    data.setKlsrawat(memberInfo.getResponse().getPeserta().getHakKelas().getKeterangan());
	                data.setResponse(true);
	                data.setDescription(bpjsUserStatusMessage);
	                String rujuk =BpjsService2.getRujukanbyNoka(nobpjs);
	                gson = new Gson();
	                RujukanPcareOrRsByNoka infomember = gson.fromJson(rujuk, RujukanPcareOrRsByNoka.class);
	                Metadata datameta = infomember.getMetaData();
	    	        if(datameta.getCode().equals("200") && datameta.getMessage().equals("OK")) {
	    	        	data.setRujukanresp(true);
	    	        	data.setNomorRujukan(infomember.getResponse().getRujukan().getNoKunjungan());
	    	        	data.setTanggalRujukan(infomember.getResponse().getRujukan().getTglKunjungan());
	    	        	data.setKeluhan(infomember.getResponse().getRujukan().getKeluhan());
	    	        } else {
	    	        	data.setRujukanresp(false);
	    	        }
	            }else{	              	
	              	data.setResponse(false);
	              	data.setRujukanresp(false);
	                data.setDescription(bpjsUserStatusMessage);
	            }
	        } else {
	        	data.setResponse(false);
	        	data.setRujukanresp(false);
	        }
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {           
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}             
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
        }
        return data;
    }
    
	
	public static DataPasien getInfoPasienByNIKBPJS(String nomor, String tipe){
		System.out.println("get Info Pasien by NIK or No BPJS ");
		DataPasien data = new DataPasien();
		data.setSuccess(false);
		Connection connection = null;
		connection = DbConnection.getPooledConnection();// getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		String sqlQUery = "SELECT C.CARD_NO,P.ID_TYPE,P.ID_NO,P.PERSON_NAME,P.BIRTH_DATE,P.MOBILE_PHONE_NO,P.EMAIL,P.NATIONALITY,PT.BPJS_NO"
				 +" ,P.SEX,P.RACE,P.ETHNIC_GROUP,P.ETHNIC,PT.TINGKAT_PENDIDIKAN"
				 +" ,AD.PHONE_NO,AD.ADDRESS_1"
				 +" ,AR.STAKEHOLDER_ID,AR.COUNTRY,AR.CITY,AR.STATE,AR.DISTRICT"
				 +" ,SM.STATEMSTR_ID,SM.STATE_DESC,CM.CITY_DESC" 
				 +" ,PGCOMMON.FXGETCODEDESC(P.SEX) as SEXY"         
		 		 +" ,PGCOMMON.FXGETCODEDESC(P.RACE) as RELIGION"
		 		 +" ,PGCOMMON.FXGETCODEDESC(P.ETHNIC) as SUKU"
		 		 +" ,PGCOMMON.FXGETCODEDESC(PT.TINGKAT_PENDIDIKAN) as PENDIDIKAN"
				 +" FROM CARD C INNER JOIN PERSON P ON C.PERSON_ID=P.PERSON_ID"
				 +" INNER JOIN PATIENT PT ON PT.PERSON_ID = P.PERSON_ID"
				 +" LEFT JOIN ADDRESS AD  ON AD.STAKEHOLDER_ID = P.STAKEHOLDER_ID AND AD.ADDRESS_TYPE = 'ADRRES'"
				 +" LEFT JOIN ADDRESS AR  ON AR.STAKEHOLDER_ID = P.STAKEHOLDER_ID AND AR.ADDRESS_TYPE = 'ADRPRE'"  
				 +" LEFT JOIN STATEMSTR SM  ON SM.STATE_CODE = AR.STATE" 
				 +" LEFT JOIN CITYMSTR CM  ON CM.CITY_CODE =AR.CITY"
				 +" WHERE C.PERSON_ID = PT.PERSON_ID AND P.ID_NO = ?";
		
		if(tipe.equals(MobileBiz.BPJS_TEXT)){
			sqlQUery = "SELECT C.CARD_NO,P.ID_TYPE,P.ID_NO,P.PERSON_NAME,P.BIRTH_DATE,P.MOBILE_PHONE_NO,P.EMAIL,P.NATIONALITY,PT.BPJS_NO"
					 +" ,P.SEX,P.RACE,P.ETHNIC_GROUP,P.ETHNIC,PT.TINGKAT_PENDIDIKAN"
					 +" ,AD.PHONE_NO,AD.ADDRESS_1"
					 +" ,AR.STAKEHOLDER_ID,AR.COUNTRY,AR.CITY,AR.STATE,AR.DISTRICT"
					 +" ,SM.STATEMSTR_ID,SM.STATE_DESC,CM.CITY_DESC"  
					 +" ,PGCOMMON.FXGETCODEDESC(P.SEX) as SEXY"         
			 		 +" ,PGCOMMON.FXGETCODEDESC(P.RACE) as RELIGION"
			 		 +" ,PGCOMMON.FXGETCODEDESC(P.ETHNIC) as SUKU"
			 		 +" ,PGCOMMON.FXGETCODEDESC(PT.TINGKAT_PENDIDIKAN) as PENDIDIKAN"
			 		 +" FROM CARD C INNER JOIN PERSON P ON C.PERSON_ID=P.PERSON_ID"
					 +" INNER JOIN PATIENT PT ON PT.PERSON_ID = P.PERSON_ID"
					 +" LEFT JOIN ADDRESS AD  ON AD.STAKEHOLDER_ID = P.STAKEHOLDER_ID AND AD.ADDRESS_TYPE = 'ADRRES'"  
					 +" LEFT JOIN ADDRESS AR  ON AR.STAKEHOLDER_ID = P.STAKEHOLDER_ID AND AR.ADDRESS_TYPE = 'ADRPRE'"  
					 +" LEFT JOIN STATEMSTR SM  ON SM.STATE_CODE = AR.STATE" 
					 +" LEFT JOIN CITYMSTR CM  ON CM.CITY_CODE =AR.CITY"
					 +" WHERE C.PERSON_ID = PT.PERSON_ID AND PT.BPJS_NO = ?";
		}
		if(tipe.equals(MobileBiz.MRN_TEXT)){
			sqlQUery = "SELECT C.CARD_NO,P.ID_TYPE,P.ID_NO,P.PERSON_NAME,P.BIRTH_DATE,P.MOBILE_PHONE_NO,P.EMAIL,P.NATIONALITY,PT.BPJS_NO"
					 +" ,P.SEX,P.RACE,P.ETHNIC_GROUP,P.ETHNIC,PT.TINGKAT_PENDIDIKAN"
					 +" ,AD.PHONE_NO,AD.ADDRESS_1"
					 +" ,AR.STAKEHOLDER_ID,AR.COUNTRY,AR.CITY,AR.STATE,AR.DISTRICT"
					 +" ,SM.STATEMSTR_ID,SM.STATE_DESC,CM.CITY_DESC"  
					 +" ,PGCOMMON.FXGETCODEDESC(P.SEX) as SEXY"         
			 		 +" ,PGCOMMON.FXGETCODEDESC(P.RACE) as RELIGION"
			 		 +" ,PGCOMMON.FXGETCODEDESC(P.ETHNIC) as SUKU"
			 		 +" ,PGCOMMON.FXGETCODEDESC(PT.TINGKAT_PENDIDIKAN) as PENDIDIKAN"
			 		 +" FROM CARD C INNER JOIN PERSON P ON C.PERSON_ID=P.PERSON_ID"
					 +" INNER JOIN PATIENT PT ON PT.PERSON_ID = P.PERSON_ID"
					 +" LEFT JOIN ADDRESS AD  ON AD.STAKEHOLDER_ID = P.STAKEHOLDER_ID AND AD.ADDRESS_TYPE = 'ADRRES'"   
					 +" LEFT JOIN ADDRESS AR  ON AR.STAKEHOLDER_ID = P.STAKEHOLDER_ID AND AR.ADDRESS_TYPE = 'ADRPRE'"  
					 +" LEFT JOIN STATEMSTR SM  ON SM.STATE_CODE = AR.STATE" 
					 +" LEFT JOIN CITYMSTR CM  ON CM.CITY_CODE =AR.CITY"
					 +" WHERE C.PERSON_ID = PT.PERSON_ID AND C.CARD_NO = ?";
		}
		
		try {
			ps = connection.prepareStatement(sqlQUery);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, nomor);
			rs = ps.executeQuery();
			if (rs.next()) {
				String agama = null;
				if(rs.getString("RACE") != null){
					agama = rs.getString("RACE")+"~"+rs.getString("RELIGION");
				}
				String suku = null;
				if(rs.getString("ETHNIC_GROUP") != null){
					suku = rs.getString("ETHNIC_GROUP")+"~"+rs.getString("SUKU");
				}
				if(rs.getString("ETHNIC") != null){
					suku = rs.getString("ETHNIC")+"~"+rs.getString("SUKU");
				}
				String pendidikan = null;
				if(rs.getString("TINGKAT_PENDIDIKAN") != null){
					pendidikan = rs.getString("TINGKAT_PENDIDIKAN")+"~"+rs.getString("PENDIDIKAN");
				}
				String prov = null;
				if(rs.getString("STATEMSTR_ID") != null){
					prov = rs.getString("STATEMSTR_ID")+"~"+rs.getString("STATE")+"~"+rs.getString("STATE_DESC");
				}
				String city = null;
				if(rs.getString("CITY") != null){
					city = rs.getString("CITY")+"~"+rs.getString("CITY_DESC");
				}
				
				data.setSuccess(true);
				data.setCARD_NO(rs.getString("CARD_NO"));
			 	data.setPERSON_NAME(CommonUtil.getInstance().convertToReadableName(rs.getString("PERSON_NAME")));
			 	data.setIdType(rs.getString("ID_TYPE"));
			 	data.setIdNo(rs.getString("ID_NO"));			 	
			 	data.setBIRTH_DATE(rs.getString("BIRTH_DATE"));
			 	data.setMOBILE_PHONE_NO(rs.getString("MOBILE_PHONE_NO"));
			 	data.setEmail(rs.getString("EMAIL"));
			 	data.setNationality(rs.getString("NATIONALITY"));
			 	data.setBpjsNo(rs.getString("BPJS_NO"));
			 	data.setSex(rs.getString("SEX"));
			 	data.setReligion(agama);
			 	data.setEthnic(suku);
			 	data.setPendidikan(pendidikan);		
			 	data.setCountry(rs.getString("COUNTRY"));
			 	data.setCity(city);
			 	data.setState(prov);
			 	data.setPhone(rs.getString("PHONE_NO"));
			 	data.setAddress(rs.getString("ADDRESS_1"));

			}
			ps.close();
		}
		
		catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (rs1!=null) try  { rs1.close(); } catch (Exception ignore){}
		    if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		    if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		    if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		    if (rs3!=null) try  { rs3.close(); } catch (Exception ignore){}
		    if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}

	
	public static ResponseStatus sendEmailPasienBaru(String regId){
		ResponseStatus data = new ResponseStatus();
		data.setMessage("FAIL");
		data.setSuccess(false);
		RegistrasiNewPatient dataemail = new RegistrasiNewPatient();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null) return null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		
		String sqlRegistrasi = "SELECT * FROM tb_registrasi WHERE registrasi_id=?";
		try {
			ps = connection.prepareStatement(sqlRegistrasi);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, regId);
			rs = ps.executeQuery();
			if (rs.next()) {				
				String personid = rs.getString("person_id");
				String department = rs.getString("department");
				String referenceid = rs.getString("reference_id");
				String status = rs.getString("status");
				String noRujuk = null;
				String tglRujuk = null;
				String alasan = null;
				if (department.equals(MobileBiz.DEPARTMENT_BPJS) || department.equals(MobileBiz.DEPARTMENT_BPJSEX)) {
					String sqlRujukan = "SELECT reference,reference_date FROM tb_rujukan WHERE reference_id=?";
					try{
						ps2 = connection.prepareStatement(sqlRujukan);
						ps2.setEscapeProcessing(true);
						ps2.setQueryTimeout(60000);
						ps2.setString(1, referenceid);
						rs2 = ps2.executeQuery();
						if (rs2.next()) {
							noRujuk = rs2.getString("reference");
							tglRujuk = rs2.getString("reference_date");
						}
						ps2.close();
					}
					catch (SQLException e) {
						System.out.println(e.getMessage());
					}
				}				
				if (status.equals(MobileBiz.APPOINTMENT_STATUS_DELETE)) {
					String sqlAlasan = "SELECT keterangan FROM tb_alasan_tolak WHERE registrasi_id=?";
					try{
						ps3 = connection.prepareStatement(sqlAlasan);
						ps3.setEscapeProcessing(true);
						ps3.setQueryTimeout(60000);
						ps3.setString(1, regId);
						rs3 = ps3.executeQuery();
						if (rs3.next()) {
							alasan = rs3.getString("keterangan");
							status = MobileBiz.APPOINTMENT_STATUS_REJECT;
						}
						ps3.close();
					}
					catch (SQLException e) {
						System.out.println(e.getMessage());
					}
				}
				String sqlUser = "SELECT idcard,no_bpjs,fullname,birth,email,time_scheduler FROM tb_person "
						+ "WHERE person_id = ? ";
				try{
					ps1 = connection.prepareStatement(sqlUser);
					ps1.setEscapeProcessing(true);
					ps1.setQueryTimeout(60000);
					ps1.setString(1, personid);
					rs1 = ps1.executeQuery();
					if(rs1.next()){

						String jadwal = rs1.getString("time_scheduler");
						jadwal = jadwal.replace("\r \n ","<br/>");
						dataemail.setNomrn(rs.getString("no_mrn"));
						dataemail.setBarcode(rs.getString("generate_barcode"));
						dataemail.setFullName(rs1.getString("fullname"));
						dataemail.setEmail(rs1.getString("email"));
						dataemail.setBirth(rs1.getString("birth"));
						dataemail.setIdcard(rs1.getString("idcard"));
						dataemail.setPasienType(department);
						dataemail.setNoBpjs(rs1.getString("no_bpjs"));
						dataemail.setNoSurat(noRujuk);
						dataemail.setTglSurat(tglRujuk);
						dataemail.setCreatedDate(rs.getString("created_at"));
						dataemail.setDoctorName(rs.getString("doctor_name"));
						dataemail.setTglAppointment(rs.getString("queue_date"));
						dataemail.setStatus(status);
						dataemail.setAlasan(alasan);
						dataemail.setJadwal(jadwal);
						dataemail.setSuccess(true);
						data.setSuccess(true);
						data.setResponse(regId);
						data.setMessage(rs.getString("generate_barcode"));
						try{
							NotificationRegistrationEmail emailTask = new NotificationRegistrationEmail();
							emailTask.sendEmail(dataemail);
						}catch(Exception ignore){}
					}
					ps1.close();
				}
				catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
			ps.close();
		}	
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (rs1!=null) try  { rs1.close(); } catch (Exception ignore){}
		    if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		    if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		    if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		    if (rs3!=null) try  { rs3.close(); } catch (Exception ignore){}
		    if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	
	}
	
	
	
	public static DataUser checkLoginPatientAntrian(String user, String pass) {
		System.out.println("Check Login Patient Website...");
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null) return null;
		DataUser du = new DataUser();
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		String sql = "SELECT user_id , no_mrn, no_ktp, username, fullname , password, no_bpjs, email,contact FROM tb_user u WHERE LOWER(u.username) = ? and u.password = ? ORDER BY u.user_id DESC";
		
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(pass);
			String passDecode = new String(decodedBytes);
			
			EncryptPatient.getInstance();
			String reEncryptAntrian = EncryptPatient.encryptOnlineAppointment(Encryptor.getInstance().decrypt(passDecode));			
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, user.toLowerCase());
			ps.setString(2, reEncryptAntrian);
			rs = ps.executeQuery();

			if (rs.next()) {				
				du.setResponse(true);
				du.setUser_id(rs.getBigDecimal("user_id"));
				du.setUsername(rs.getString("username"));
				du.setNo_mrn(rs.getString("no_mrn"));
				du.setFullname(rs.getString("fullname"));
				du.setNo_ktp(rs.getString("no_ktp"));
				du.setNo_bpjs(rs.getString("no_bpjs"));
				du.setPassword(pass);
				du.setContact(rs.getString("contact"));
				du.setEmail(rs.getString("email"));
			} else {
				du.setResponse(false);
			}
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return du;		
	}
	
	public static ResponseStatus checkUserPatientAntrian(String mrn, String ktp, String bpjs) {
		ResponseStatus data = new ResponseStatus();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)	return null;

		String sql = "SELECT * FROM tb_user WHERE no_mrn = ? OR no_bpjs = ? OR no_ktp = ?";
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1,mrn);
			ps.setString(2, bpjs);
			ps.setString(3, ktp);
			rs = ps.executeQuery();
			if(rs.next()){
				data.setMessage("Pasien Sudah Terdaftar");
				data.setSuccess(true);
			} else {
				data.setMessage("Pasien Belum Terdaftar");
				data.setSuccess(false);
			}
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static ResponseStatus saveUserPatientAntrian(String mrn, String ktp, String bpjs, String username, String fullname, String contact, String email, String pass) {
		// TODO Auto-generated method stub
		ResponseStatus data = new ResponseStatus();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)	return null;

		String sql = "SELECT * FROM tb_user WHERE username = ? ";
		ResultSet rs = null;
		PreparedStatement ps = null;
		ResultSet rs1 = null;
		PreparedStatement ps1 = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1,username);
			rs = ps.executeQuery();
			if(rs.next()){
				data.setMessage("Username Sudah Terdaftar");
				data.setSuccess(false);
			} else {
				byte[] decodedBytes = Base64.getDecoder().decode(pass);
				String passDecode = new String(decodedBytes);
				
				EncryptPatient.getInstance();
				String reEncryptAntrian = EncryptPatient.encryptOnlineAppointment(Encryptor.getInstance().decrypt(passDecode));
				String sqlUser = "INSERT INTO tb_user (no_ktp,no_mrn,no_bpjs,username,password,created,fullname,contact,email) "
						+ "OUTPUT INSERTED.user_id VALUES(?,?,?,?,?,SYSDATETIME(),?,?,?)";
				try{
					ps1 = connection.prepareStatement(sqlUser);
					ps1.setEscapeProcessing(true);
					ps1.setQueryTimeout(60000);
					ps1.setString(1, ktp);
					ps1.setString(2, mrn);
					ps1.setString(3, bpjs);
					ps1.setString(4, username);
					ps1.setString(5, reEncryptAntrian);
					ps1.setString(6, fullname);
					ps1.setString(7, contact);
					ps1.setString(8, email);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						data.setResponse(rs1.getString("user_id"));
						data.setMessage("Proses Simpan Berhasil");
						data.setSuccess(true);
					}
				}
				catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
			if (rs1!=null) try  { rs1.close(); } catch (Exception ignore){}
		    if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static DataUser getUserbyCard(String mrn, String ktp, String bpjs) {
		System.out.println("Get User Patient Website...");
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null) return null;
		DataUser du = new DataUser();
		ResultSet rs = null;
		PreparedStatement ps = null;		
		String sql = "";
		
		if(mrn != null && !mrn.equals("") && ktp != null && !ktp.equals("") && bpjs != null && !bpjs.equals("")){
			sql = "SELECT user_id , no_mrn, no_ktp, username, fullname , password, no_bpjs,email,contact FROM tb_user u WHERE u.no_mrn = ? AND u.no_bpjs = ? AND u.no_ktp = ? ORDER BY u.user_id DESC";
		} else if(mrn != null && !mrn.equals("") && ktp != null && !ktp.equals("") && (bpjs == null || bpjs.equals(""))){
			sql = "SELECT user_id , no_mrn, no_ktp, username, fullname , password, no_bpjs,email,contact FROM tb_user u WHERE u.no_mrn = ? AND u.no_ktp = ? ORDER BY u.user_id DESC";
		} else if(mrn != null && !mrn.equals("") && (ktp == null || ktp.equals("")) && bpjs != null && !bpjs.equals("")){
			sql = "SELECT user_id , no_mrn, no_ktp, username, fullname , password, no_bpjs,email,contact FROM tb_user u WHERE u.no_mrn = ? AND u.no_bpjs = ? ORDER BY u.user_id DESC";
		} else if((mrn == null|| mrn.equals("")) && ktp != null && !ktp.equals("") && bpjs != null && !bpjs.equals("")){
			sql = "SELECT user_id , no_mrn, no_ktp, username, fullname , password, no_bpjs,email,contact FROM tb_user u WHERE u.no_bpjs = ? AND u.no_ktp = ? ORDER BY u.user_id DESC";
		} else if(mrn != null && !mrn.equals("") && (ktp == null || ktp.equals("")) && (bpjs == null || bpjs.equals("")) ){
			sql = "SELECT user_id , no_mrn, no_ktp, username, fullname , password, no_bpjs,email,contact FROM tb_user u WHERE u.no_mrn = ? ORDER BY u.user_id DESC";
		} else if((mrn == null || mrn.equals("")) && ktp != null && !ktp.equals("") && (bpjs == null || bpjs.equals(""))){
			sql = "SELECT user_id , no_mrn, no_ktp, username, fullname , password, no_bpjs,email,contact FROM tb_user u WHERE u.no_ktp = ? ORDER BY u.user_id DESC";
		} else if((mrn == null || mrn.equals("")) && (ktp == null || ktp.equals("")) &&  bpjs != null && !bpjs.equals("")){
			sql = "SELECT user_id , no_mrn, no_ktp, username, fullname , password, no_bpjs,email,contact FROM tb_user u WHERE u.no_bpjs = ? ORDER BY u.user_id DESC";
		}
		try {
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			if(mrn != null && !mrn.equals("") && ktp != null && !ktp.equals("") && bpjs != null && !bpjs.equals("")){
				ps.setString(1,mrn);
				ps.setString(2, bpjs);
				ps.setString(3, ktp);
			} else if(mrn != null && !mrn.equals("") && ktp != null && !ktp.equals("") && (bpjs == null || bpjs.equals(""))){
				ps.setString(1,mrn);
				ps.setString(2, ktp);
			} else if(mrn != null && !mrn.equals("") && (ktp == null || ktp.equals("")) && bpjs != null && !bpjs.equals("")){
				ps.setString(1,mrn);
				ps.setString(2, bpjs);
			} else if((mrn == null || mrn.equals("")) && ktp != null && !ktp.equals("") && bpjs != null && !bpjs.equals("")){
				ps.setString(1, bpjs);
				ps.setString(2, ktp);
			} else if(mrn != null && !mrn.equals("") && (ktp == null || ktp.equals("")) && (bpjs == null || bpjs.equals(""))){
				ps.setString(1,mrn);
			} else if((mrn == null || mrn.equals("")) && ktp != null && !ktp.equals("") && (bpjs == null || bpjs.equals(""))){
				ps.setString(1, ktp);				
			} else if((mrn == null || mrn.equals("")) && (ktp == null || ktp.equals("")) &&  bpjs != null && !bpjs.equals("")){
				ps.setString(1, bpjs);
			}
			
			rs = ps.executeQuery();

			if (rs.next()) {
				String passcode = null;
				du.setResponse(true);
				du.setUser_id(rs.getBigDecimal("user_id"));
				du.setUsername(rs.getString("username"));
				du.setNo_mrn(rs.getString("no_mrn"));
				du.setFullname(rs.getString("fullname"));
				du.setNo_ktp(rs.getString("no_ktp"));
				du.setNo_bpjs(rs.getString("no_bpjs"));
				du.setContact(rs.getString("contact"));
				du.setEmail(rs.getString("email"));
				du.setPassword(passcode);
			} else {
				du.setResponse(false);
			}
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return du;		
	}
		
	public static ResponseStatus resetPasswordUser(String mrn, String userid, String pass, String username, String tipe) {
		// TODO Auto-generated method stub
		ResponseStatus data = new ResponseStatus();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)	return null;
		
		byte[] decodedBytes = Base64.getDecoder().decode(pass);
		String passDecode = new String(decodedBytes);
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		ResultSet rs1 = null;
		PreparedStatement ps1 = null;
		try {
			EncryptPatient.getInstance();
			String reEncryptAntrian = EncryptPatient.encryptOnlineAppointment(Encryptor.getInstance().decrypt(passDecode));			
			if(tipe.equals("Y")){
				String sql = "SELECT * FROM tb_user WHERE username = ?";
				ps = connection.prepareStatement(sql);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				ps.setString(1,username);
				rs = ps.executeQuery();
				if(rs.next()){
					data.setMessage("Username Sudah Digunakan");
					data.setSuccess(false);
				} else {
					String sql1 = "UPDATE tb_user SET username=?, password=? where user_id=? AND no_mrn=?";
					try {
						ps1 = connection.prepareStatement(sql1);
						ps1.setEscapeProcessing(true);
						ps1.setQueryTimeout(60000);
						ps1.setString(1, username);
						ps1.setString(2, reEncryptAntrian);
						ps1.setString(3, userid);
						ps1.setString(4, mrn);					
						ps1.executeUpdate();
						data.setMessage("Data berhasil di simpan.");
						data.setSuccess(true);					
					}
					catch (SQLException e) {
						System.out.println(e.getMessage());
					}
				}
			} else {				
				String sql1 = "UPDATE tb_user SET password=? where user_id=? AND no_mrn=?";
				try {
					ps1 = connection.prepareStatement(sql1);
					ps1.setEscapeProcessing(true);
					ps1.setQueryTimeout(60000);
					ps1.setString(1, reEncryptAntrian);
					ps1.setString(2, userid);
					ps1.setString(3, mrn);					
					ps1.executeUpdate();
					data.setMessage("Data berhasil di simpan.");
					data.setSuccess(true);					
				}
				catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (rs1!=null) try  { rs1.close(); } catch (Exception ignore){}
		    if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static ResponseStatus addJobsRekruitment(JobsRecruitmentRegistration jobsRecruitmentRegistration) {
		ResponseStatus data = new ResponseStatus();
		Connection connection = null;
		connection = DbConnection.getHrisInstance();
		if (connection == null) return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ResultSet rs1 = null;
		PreparedStatement ps1 = null;
		String idRecruitment = null;
		data.setMessage("FAIL");
		data.setSuccess(false);
		String sql = "SELECT IDRecruitment FROM TblRecruitmentProfile ORDER BY IDRecruitment DESC";
		try{
			ps1 = connection.prepareStatement(sql);
			ps1.setEscapeProcessing(true);
			ps1.setQueryTimeout(60000);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				idRecruitment = rs1.getString("IDRecruitment");
				String fTh= null;
				String fMh = null;
				String fNu = "0001";
				String th = idRecruitment.substring(0, 2);
				String bl = idRecruitment.substring(2, 4);
				String no = idRecruitment.substring(4);				
				Date now = new Date(); // java.util.Date, NOT java.sql.Date or java.sql.Timestamp!
				String format = new SimpleDateFormat("yyMM", Locale.ENGLISH).format(now);
				String yr = format.substring(0, 2);
				String mh = format.substring(2, 4);
				if(th.equals(yr)){
					fTh= th;
				} else {
					fTh = yr;
				}
				if(bl.equals(mh)){
					fMh = bl;
					int nomor = Integer.parseInt(no);
					int jl = nomor + 1;
					String str = Integer.toString(jl);
					int jlh = 4 - str.length();
					String nm = no.substring(0, jlh);
					fNu = nm+str;
				} else {
					fMh = mh;
				}
				
				String idNew = fTh+fMh+fNu;
				String sqlIns = "INSERT INTO TblRecruitmentProfile (IDRecruitment,Email,Password,NamaLengkap,TempatLahir,TglLahir,TinggiBadan,BeratBadan,Kewarganegaraan,"
						+ "PendidikanTerakhir,InstansiPendidikan,Jurusan,Nilai,Bahasa,StatusPerkawinan,JenisKelamin,Suku,Agama,Hobby,AlamatTetap,TeleponTetap,AlamatSekarang,TeleponSekarang,"
						+ "NoKTP,SIMA,SIMBI,SIMBII,SIMC,TglRegister,GajiDikehendaki,FreshGraduate,TglDapatMulaiBekerja,ProgramKomputerDikuasai,RegisterMelalui,Aktif,Dlt) "
						+ "OUTPUT INSERTED.IDRecruitment VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,SYSDATETIME(),?,?, ?,?,?,?,?)";
				try {
					ps = connection.prepareStatement(sqlIns);
					ps.setEscapeProcessing(true);
					ps.setQueryTimeout(60000);
					ps.setString(1, idNew);
					ps.setString(2, jobsRecruitmentRegistration.getEmail());
					ps.setString(3, jobsRecruitmentRegistration.getPassword());
					ps.setString(4, jobsRecruitmentRegistration.getNamalengkap());
					ps.setString(5, jobsRecruitmentRegistration.getTempatlahir());
					ps.setString(6, jobsRecruitmentRegistration.getTgllahir());
					ps.setString(7, jobsRecruitmentRegistration.getTinggibadan());
					ps.setString(8, jobsRecruitmentRegistration.getBeratbadan());
					ps.setString(9, jobsRecruitmentRegistration.getKewarganegaraan());
					ps.setString(10, jobsRecruitmentRegistration.getPendidikanterakhir());
					ps.setString(11, jobsRecruitmentRegistration.getInstansipendidikan());
					ps.setString(12, jobsRecruitmentRegistration.getJurusan());
					ps.setString(13, jobsRecruitmentRegistration.getNilai());
					ps.setString(14, jobsRecruitmentRegistration.getBahasa());
					ps.setString(15, jobsRecruitmentRegistration.getStatusperkawinan());
					ps.setString(16, jobsRecruitmentRegistration.getJeniskelamin());
					ps.setString(17, jobsRecruitmentRegistration.getSuku());
					ps.setString(18, jobsRecruitmentRegistration.getAgama());
					ps.setString(19, jobsRecruitmentRegistration.getHobby());
					ps.setString(20, jobsRecruitmentRegistration.getAlamattetap());
					ps.setString(21, jobsRecruitmentRegistration.getTelepontetap());
					ps.setString(22, jobsRecruitmentRegistration.getAlamatsekarang());
					ps.setString(23, jobsRecruitmentRegistration.getTeleponsekarang());
					ps.setString(24, jobsRecruitmentRegistration.getNoktp());
					ps.setString(25, jobsRecruitmentRegistration.getSima());
					ps.setString(26, jobsRecruitmentRegistration.getSimb1());
					ps.setString(27, jobsRecruitmentRegistration.getSimb2());
					ps.setString(28, jobsRecruitmentRegistration.getSimc());
					ps.setString(29, jobsRecruitmentRegistration.getGaji());
					ps.setString(30, jobsRecruitmentRegistration.getFreshgraduate());
					ps.setString(31, jobsRecruitmentRegistration.getTglkerja());
					ps.setString(32, jobsRecruitmentRegistration.getProgram());
					ps.setString(33, "Website MTMH");
					ps.setString(34, "1");
					ps.setString(35, "0");
					rs = ps.executeQuery();
					if (rs.next()) {						
						data.setSuccess(true);
						data.setMessage("OK");
						data.setResponse(rs.getString("IDRecruitment"));
					}
				}		
				catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (rs1!=null) try  { rs1.close(); } catch (Exception ignore){}
		    if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
		
	}
	
	public static ResponseStatus addKodeKonfirmRekruitment(String idRegist, String kodeKonfirm,String param){
		ResponseStatus data = new ResponseStatus();
		JobsRecruitmentEmail dataemail = new JobsRecruitmentEmail();
		Connection connection = null;
		connection = DbConnection.getHrisInstance();
		if (connection == null) return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ResultSet rs1 = null;
		PreparedStatement ps1 = null;
		data.setMessage("FAIL");
		data.setSuccess(false);
		String sql = "SELECT NamaLengkap,Email FROM TblRecruitmentProfile WHERE IDRecruitment = ?";
		try{
			ps1 = connection.prepareStatement(sql);
			ps1.setEscapeProcessing(true);
			ps1.setQueryTimeout(60000);
			ps1.setString(1,idRegist);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				String sqlUpd = "UPDATE TblRecruitmentProfile SET KodeKonfirmasi=? where IDRecruitment = ? ";
				try {
					ps = connection.prepareStatement(sqlUpd);
					ps.setEscapeProcessing(true);
					ps.setQueryTimeout(60000);
					ps.setString(1, idRegist);
					ps.setString(2, kodeKonfirm);
					ps.executeUpdate();
					dataemail.setFullName(rs1.getString("NamaLengkap"));
					dataemail.setEmail(rs1.getString("Email"));
					dataemail.setIdRegist(param);
					dataemail.setKodeKonfirm(kodeKonfirm);
					data.setSuccess(true);
					data.setMessage("OK");
					data.setResponse(idRegist);
					try{
						NotificationRegistrationEmail emailTask = new NotificationRegistrationEmail();
						emailTask.sendJobsEmail(dataemail);	
					}catch(Exception ignore){}
				}		
				catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (rs1!=null) try  { rs1.close(); } catch (Exception ignore){}
		    if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static ResponseStatus addKonfirmasiByEmail(String idRegist, String kodeKonfirm){
		ResponseStatus data = new ResponseStatus();
		Connection connection = null;
		connection = DbConnection.getHrisInstance();
		if (connection == null) return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ResultSet rs1 = null;
		PreparedStatement ps1 = null;
		data.setMessage("FAIL");
		data.setSuccess(false);
		String sql = "SELECT IDRecruitment FROM TblRecruitmentProfile WHERE IDRecruitment = ?";
		try{
			ps1 = connection.prepareStatement(sql);
			ps1.setEscapeProcessing(true);
			ps1.setQueryTimeout(60000);
			ps1.setString(1,idRegist);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				String sqlUpd = "UPDATE TblRecruitmentProfile SET aktif=1 where IDRecruitment = ? ";
				try {
					ps = connection.prepareStatement(sqlUpd);
					ps.setEscapeProcessing(true);
					ps.setQueryTimeout(60000);
					ps.setString(1, idRegist);
					ps.executeUpdate();
					data.setSuccess(true);
					data.setMessage("OK");
					data.setResponse(idRegist);
				}		
				catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (rs1!=null) try  { rs1.close(); } catch (Exception ignore){}
		    if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static ResponseStatus getKonfirmasiByIdRegist(String idRegist){
		ResponseStatus data = new ResponseStatus();
		Connection connection = null;
		connection = DbConnection.getHrisInstance();
		if (connection == null) return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		data.setMessage("FAIL");
		data.setSuccess(false);
		String sql = "SELECT KodeKonfirmasi FROM TblRecruitmentProfile WHERE IDRecruitment = ?";
		try{
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, idRegist);
			rs = ps.executeQuery();
			if (rs.next()) {
				data.setSuccess(true);
				data.setMessage("OK");
				data.setResponse(rs.getString("KodeKonfirmasi"));
			}
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
}
