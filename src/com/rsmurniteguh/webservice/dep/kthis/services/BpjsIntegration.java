package com.rsmurniteguh.webservice.dep.kthis.services;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;

import org.apache.http.util.TextUtils;

import com.rsmurniteguh.webservice.dep.all.model.Person;
import com.rsmurniteguh.webservice.dep.all.model.SepBpjs;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.Info;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.RegisterSepParam;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.RepeatTreatmentView;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.Response;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.ResponsePeserta.Peserta;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.ResponseRegisterSep;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.ResponseRujukan;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.ResponseRujukan.Rujukan;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.ResponseRujukanInsert;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.Response.MetaData;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsIntegration.ISep.PostRegisterData.RujukanData;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsIntegration.ISep.PostRegisterData.SepBpjsData;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsService2.BpjsInfoType;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.JsonUtil;

public class BpjsIntegration {
	private static final String HOSPITAL_NAME = "RS. Murni Teguh";
	private static final String DIAGNOSA_AWAL_DESC = "Other special examinations and investigations of persons without complaint or reported diagnosis";
	private static final String BASIC_PHONE_NUMBER = "006180501888";
	private static final String PPK_RS_MURNI_TEGUH = "0038R091";
	private static final String DIAGNOSA_AWAL = "Z04.8";
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static enum Version{
		V1_0("1.0"),
		V1_1("1.1");
		private String value;
		Version(String value) {
			this.value = value;
		}
		
		public static Version getVersion() {
			return Version.from("1.1");
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
		public static Version from(String type) {
			if(type ==null)return null;
			Version result  = null;
			List<Version> enums = new ArrayList(EnumSet.allOf(Version.class));
			if(enums == null)return null;
			for(Version target : enums) {
				if(target.getValue().equals(type)) 
				{
					result = target;
					break;
				} 
			}
			return result;
		}
	}
	
	public static interface ISep{
		public static enum Platform{
			KTHIS,
			SELF_REGISTRATION
		}
		
		public static class PostRegisterData{
			private SepBpjs sepBpjs;
			private BigDecimal careProviderId;
			private BigDecimal visitId;
			private BigDecimal userId;
			private String bpjsNo;
			private BigDecimal mrn;
			private String sepNo;
			private RujukanData rujukanData;
			public static class SepBpjsData{
				private String asuransiLakaLantas;
				private String sepLakaLantas;
				private String descriptLakaLantas;
				private String tmptLakaLantas;
				private String lakaLantas;
				private String letterNo;
				private String tipeRujukan;
				private String queueNo;
				private String sepNo;
				private BigDecimal careProviderId;
				private BigDecimal visitId;
				private Timestamp tglLakaLantas;

				public SepBpjsData(BigDecimal visitId, String sepNo,  String queueNo, BigDecimal careProviderId, String tipeRujukan, String letterNo,
				String lakaLantas, Timestamp tglLakaLantas, String tmptLakaLantas, String descriptLakaLantas, String sepLakaLantas,
				String asuransiLakaLantas) {
					this.visitId = visitId;
					this.sepNo = sepNo;
					this.queueNo = queueNo;
					this.careProviderId = careProviderId;
					this.tipeRujukan = tipeRujukan;
					this.letterNo = letterNo;
					this.lakaLantas = lakaLantas;
					this.tglLakaLantas = tglLakaLantas;
					this.tmptLakaLantas = tmptLakaLantas;
					this.descriptLakaLantas = descriptLakaLantas;
					this.sepLakaLantas = sepLakaLantas;
					this.asuransiLakaLantas = asuransiLakaLantas;
				}

				public String getAsuransiLakaLantas() {
					return asuransiLakaLantas;
				}

				public void setAsuransiLakaLantas(String asuransiLakaLantas) {
					this.asuransiLakaLantas = asuransiLakaLantas;
				}

				public String getSepLakaLantas() {
					return sepLakaLantas;
				}

				public void setSepLakaLantas(String sepLakaLantas) {
					this.sepLakaLantas = sepLakaLantas;
				}

				public String getDescriptLakaLantas() {
					return descriptLakaLantas;
				}

				public void setDescriptLakaLantas(String descriptLakaLantas) {
					this.descriptLakaLantas = descriptLakaLantas;
				}

				public String getTmptLakaLantas() {
					return tmptLakaLantas;
				}

				public void setTmptLakaLantas(String tmptLakaLantas) {
					this.tmptLakaLantas = tmptLakaLantas;
				}

				public String getLakaLantas() {
					return lakaLantas;
				}

				public void setLakaLantas(String lakaLantas) {
					this.lakaLantas = lakaLantas;
				}

				public String getLetterNo() {
					return letterNo;
				}

				public void setLetterNo(String letterNo) {
					this.letterNo = letterNo;
				}

				public String getTipeRujukan() {
					return tipeRujukan;
				}

				public void setTipeRujukan(String tipeRujukan) {
					this.tipeRujukan = tipeRujukan;
				}

				public String getQueueNo() {
					return queueNo;
				}

				public void setQueueNo(String queueNo) {
					this.queueNo = queueNo;
				}

				public String getSepNo() {
					return sepNo;
				}

				public void setSepNo(String sepNo) {
					this.sepNo = sepNo;
				}

				public BigDecimal getCareProviderId() {
					return careProviderId;
				}

				public void setCareProviderId(BigDecimal careProviderId) {
					this.careProviderId = careProviderId;
				}

				public BigDecimal getVisitId() {
					return visitId;
				}

				public void setVisitId(BigDecimal visitId) {
					this.visitId = visitId;
				}

				public Timestamp getTglLakaLantas() {
					return tglLakaLantas;
				}

				public void setTglLakaLantas(Timestamp tglLakaLantas) {
					this.tglLakaLantas = tglLakaLantas;
				}
			}
			
			public static class RujukanData{
				private String nmProviderRujuk;
				private Timestamp tglKunjungan;
				private String nomorRujukan;

				public RujukanData(String nmProviderRujuk, Timestamp tglRujuk, String nomorRujukan) {
					this.nmProviderRujuk = nmProviderRujuk;
					this.tglKunjungan = tglRujuk;
					this.nomorRujukan = nomorRujukan;
				}

				public String getNmProviderRujuk() {
					return nmProviderRujuk;
				}

				public void setNmProviderRujuk(String nmProviderRujuk) {
					this.nmProviderRujuk = nmProviderRujuk;
				}

				public Timestamp getTglKunjungan() {
					return tglKunjungan;
				}

				public void setTglKunjungan(Timestamp tglRujuk) {
					this.tglKunjungan = tglRujuk;
				}

				public String getNomorRujukan() {
					return nomorRujukan;
				}

				public void setNomorRujukan(String nomorRujukan) {
					this.nomorRujukan = nomorRujukan;
				}
			}
			public PostRegisterData(BigDecimal userId, BigDecimal mrn, 
				String bpjsNo, SepBpjsData sepBpjsData, RujukanData rujukanData) {
				this.mrn = mrn;
				this.bpjsNo = bpjsNo;
				this.userId = userId;
				this.userId = userId;
				this.careProviderId = sepBpjsData.getCareProviderId();
				this.visitId = sepBpjsData.getVisitId();
				this.sepNo = sepBpjsData.getSepNo();
				setSepBpjs(buildSepBpjs(sepBpjsData.getQueueNo(), sepBpjsData.getTipeRujukan(), sepBpjsData.getLetterNo(),
						sepBpjsData.getLakaLantas(), sepBpjsData.getTglLakaLantas(), sepBpjsData.getTmptLakaLantas(), sepBpjsData.getDescriptLakaLantas(),
						sepBpjsData.getSepLakaLantas(),
						sepBpjsData.getAsuransiLakaLantas()));
				this.rujukanData = rujukanData;
			}
			

			
			private SepBpjs buildSepBpjs( String queueNo, String tipeRujukan, String letterNo,
					String lakaLantas, Timestamp tglLakaLantas, String tempatLakaLantas, String descriptLakaLantas, String sepLakaLantas,
					String asuransiLakaLantas) {
				SepBpjs result = new SepBpjs();
				result.setDefunctInd("N");
				result.setModifiedAt(new Timestamp(Calendar.getInstance().getTimeInMillis()));
				result.setModifiedBy(userId);
				result.setCreatedAt(new Timestamp(Calendar.getInstance().getTimeInMillis()));
				result.setCreatedBy(userId);
				result.setQueueNo(queueNo);
				result.setCareproviderId(careProviderId);
				result.setVisitId(visitId);
				result.setSepNo(sepNo);
				result.setBpjsNo(bpjsNo);
				result.setCardNo(mrn);
				result.setLetterNo(letterNo);
				result.setLakaLantas(lakaLantas);
				result.setTglLantas(tglLakaLantas);
				result.setAsuransiLakaLantas(asuransiLakaLantas);
				result.setSepLakaLantas(sepLakaLantas);
				result.setPlaceLakaLantas(tempatLakaLantas);
				result.setDescriptLakaLantas(descriptLakaLantas);		
				result.setTipeRujukan(tipeRujukan);
				return result;
			}
			
			public SepBpjs getSepBpjs() {
				return sepBpjs;
			}
			public void setSepBpjs(SepBpjs sepBpjs) {
				this.sepBpjs = sepBpjs;
			}



			public BigDecimal getCareProviderId() {
				return careProviderId;
			}



			public void setCareProviderId(BigDecimal careProviderId) {
				this.careProviderId = careProviderId;
			}



			public BigDecimal getVisitId() {
				return visitId;
			}



			public void setVisitId(BigDecimal visitId) {
				this.visitId = visitId;
			}



			public BigDecimal getUserId() {
				return userId;
			}



			public void setUserId(BigDecimal userId) {
				this.userId = userId;
			}



			public String getBpjsNo() {
				return bpjsNo;
			}



			public void setBpjsNo(String bpjsNo) {
				this.bpjsNo = bpjsNo;
			}



			public BigDecimal getMrn() {
				return mrn;
			}



			public void setMrn(BigDecimal mrn) {
				this.mrn = mrn;
			}



			public String getSepNo() {
				return sepNo;
			}



			public void setSepNo(String sepNo) {
				this.sepNo = sepNo;
			}



			public RujukanData getRujukanData() {
				return rujukanData;
			}



			public void setRujukanData(RujukanData rujukanData) {
				this.rujukanData = rujukanData;
			}
		}
		
		public String Register(RegisterSepParam param);
		public String Register(RegisterSepParam param, Platform platform);
		public void PostRegister(PostRegisterData postSepBpjs) throws SQLException, Exception;
	}
	
	
	public static interface IRujukan{
		
		public interface IValidateResult{
			void onError(String code, String message);
		}
		
		public class RujukanInpatientParam {
			private String noKa;
			private String poli;
			public static RujukanInpatientParam build(String noKa, String poli) {
				return new RujukanInpatientParam(noKa, poli);
			}
			public RujukanInpatientParam(String noKa, String poli) {
				this.noKa = noKa;
				this.poli = poli;
			}
			public String getNoKa() {
				return noKa;
			}
			public void setNoKa(String noKa) {
				this.noKa = noKa;
			}
			public String getPoli() {
				return poli;
			}
			public void setPoli(String poli) {
				this.poli = poli;
			}
		}
		
		public enum JenisPelayanan{
			INPATIENT("1"),
			OUTPATIENT("2");
			private String value;
			
			public static JenisPelayanan from(String type) {
				if(type ==null)return null;
				JenisPelayanan result  = null;
				List<JenisPelayanan> enums = new ArrayList(EnumSet.allOf(JenisPelayanan.class));
				if(enums == null)return null;
				for(JenisPelayanan target : enums) {
					if(target.toString().equals(type)) 
					{
						result = target;
						break;
					} 
				}
				return result;
			}

			JenisPelayanan(String value){
				this.value = value;
			}

			public String getValue() {
				return value;
			}

			public void setValue(String value) {
				this.value = value;
			}
			
			@Override
			public String toString() {
				return this.value;
			}
		}
		
		public enum TipeRujukan{
			FASKES_I("1"),
			FASKES_II("2");
			private String value;
			
			public static TipeRujukan from(String type) {
				if(type ==null)return null;
				TipeRujukan result  = null;
				List<TipeRujukan> enums = new ArrayList(EnumSet.allOf(TipeRujukan.class));
				if(enums == null)return null;
				for(TipeRujukan target : enums) {
					if(target.toString().equals(type)) 
					{
						result = target;
						break;
					} 
				}
				return result;
			}
			
			TipeRujukan(String value){
				this.value = value;
			}
			
			public String getValue() {
				return value;
			}
			
			public void setValue(String value) {
				this.value = value;
			}
			
			@Override
			public String toString() {
				return this.value;
			}
		}

		ResponseRujukan getRujukanTerakhir(String noKa);

		boolean validateRujukan(ResponseRujukan rujukan, IValidateResult callback);

		boolean validateRujukan(ResponseRujukanInsert rujukan, IValidateResult callback);
		
		ResponseRujukan checkAndBuildRujukanInpatientByRujukan(ResponseRujukan rujukan, RujukanInpatientParam param, JenisPelayanan jenisPelayanan);

		ResponseRujukanInsert insertRujukan(String sepNo, String jenisPelayanan, String keterangan, String poliCode,
				String userId);

		ResponseRujukan getRujukanTerakhirFromList(String noKa);
		
		ResponseRujukan deleteLocalRujukanTerbaru(String mrn, String noKa);
		
	}
	
	private static abstract class RujukanProvider implements IRujukan{
		
		@Override
		public ResponseRujukan getRujukanTerakhir(String noKa) {
			ResponseRujukan result = null; //Get rujukan terbaru
			try {
				String rujukanResponse = BpjsService2.getRujukanPcareorRSbyNoka(noKa); 
				if(rujukanResponse == null)return result;
				result = JsonUtil.toObjectFromJson(rujukanResponse, ResponseRujukan.class);
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
				ex.printStackTrace();
			}
			return result;
		}
		
		private boolean isEmpty(String target) {
			return TextUtils.isEmpty(target) ? true : target.equals("-");
		}
		
		@Override
		public ResponseRujukan getRujukanTerakhirFromList(String noKa) {
			ResponseRujukan result = null; //Get rujukan terbaru
			try {
				String rujukanResponse = BpjsService2.getRujukanListPcareorRSbyNoka(noKa); 
				if(rujukanResponse == null)return result;
				result = JsonUtil.toObjectFromJson(rujukanResponse, ResponseRujukan.class);
//				if(result == null) return result;
//				if(result.getMetaData() == null) return result;
//				if(isEmpty(result.getMetaData().getCode())) return result;
//				if(!result.getMetaData().getCode().equals("200")) return result;
//				System.out.println("yyyy\n");
//				System.out.println(result.getResponse().getRujukan().getNoKunjungan());
//				System.out.println("yyyy\n");
//				result = JsonUtil.toObjectFromJson(BpjsService2.getRujukanbyNmrRujukan(result.getResponse().getRujukan().getNoKunjungan()), ResponseRujukan.class);
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
				ex.printStackTrace();
			}
			return result;
		}
		

		@Override
		public ResponseRujukanInsert insertRujukan(String sepNo, String jenisPelayanan, String keterangan, String poliCode, String userId) {
			ResponseRujukanInsert result = null;
			try {
				String insertRujukanResponse = BpjsService2.insertRujukan(sepNo, PPK_RS_MURNI_TEGUH, jenisPelayanan, keterangan, DIAGNOSA_AWAL, "0", poliCode, userId);
//				String insertRujukanResponse = "{\"metaData\":{\"code\":\"200\",\"message\":\"OK\"},\"response\":{\"rujukan\":{\"AsalRujukan\":{\"kode\":\"0038R091\",\"nama\":\"RSU MURNI TEGUH\"},\"diagnosa\":{\"kode\":\"Z04.8\",\"nama\":\"Z04.8 - Examination and observation for other specified reasons\"},\"noRujukan\":\"0038R0911218B000004\",\"peserta\":{\"asuransi\":\"-\",\"hakKelas\":null,\"jnsPeserta\":\"PNS DAERAH\",\"kelamin\":\"Perempuan\",\"nama\":\"EVI SUSANTI\",\"noKartu\":\"0000009518523\",\"noMr\":\"1711134269\",\"tglLahir\":\"1972-08-15\"},\"poliTujuan\":{\"kode\":\"BED\",\"nama\":\"BEDAH\"},\"tglRujukan\":\"2018-12-10\",\"tujuanRujukan\":{\"kode\":\"0038R091\",\"nama\":\"RSU MURNI TEGUH\"}}}}";
				result = JsonUtil.toObjectFromJson(insertRujukanResponse, ResponseRujukanInsert.class);
			} catch (Exception ex) {
				ex.printStackTrace();
				System.out.println(ex.getMessage());
			}
			return result;
		}
		

		@Override
		public boolean validateRujukan(ResponseRujukan rujukan, IValidateResult callback) {
			boolean result = true;
			if(callback==null)return false;
			if(rujukan == null) 
			{
				callback.onError("1002","Gagal mengambil rujukan, silahkan coba kembali, jika tetap berlajut silahkan hubungi kantor BPJS/FKTP anda (rujukan harus online).");
				return false;	
			}
			if(rujukan.getMetaData() == null) 
			{
				callback.onError("1002", "Metadata rujukan kosong");
				return false;	
			}
			if(rujukan.getMetaData().getCode() == null) 
			{
				callback.onError("1002", "Metadata rujukan kosong");
				return false;	
			}
			if(!rujukan.getMetaData().getCode().equals("200")) 
			{
				callback.onError(rujukan.getMetaData().getCode(), rujukan.getMetaData().getMessage());
				return false;	
			}
			return result;
		}
		
		@Override
		public boolean validateRujukan(ResponseRujukanInsert rujukan, IValidateResult callback) {
			boolean result = true;
			if(callback==null)return false;
			if(rujukan == null) 
			{
				callback.onError("1002","Gagal mengambil rujukan, silahkan coba kembali, jika tetap berlajut silahkan hubungi kantor BPJS/FKTP anda (rujukan harus online).");
				return false;	
			}
			if(rujukan.getMetaData() == null) 
			{
				callback.onError("1002", "Metadata rujukan kosong");
				return false;	
			}
			if(rujukan.getMetaData().getCode() == null) 
			{
				callback.onError("1002", "Metadata rujukan kosong");
				return false;	
			}
			if(!rujukan.getMetaData().getCode().equals("200")) 
			{
				callback.onError(rujukan.getMetaData().getCode(), rujukan.getMetaData().getMessage());
				return false;	
			}
			return result;
		}
		
		private ResponseRujukan rujukanFail(String message) {
			ResponseRujukan result = new ResponseRujukan();
			result.setMetaData(MetaData.fail(message));
			return result;
		}
		
		@Override
		public ResponseRujukan deleteLocalRujukanTerbaru(String mrn, String noKa) {
			ResponseRujukan result = new ResponseRujukan();
			result = rujukanFail("Fail.");
			try {
				String searchQuery =  
						"SELECT * " + 
						"                            FROM (SELECT NOMOR_RUJUKAN " + 
						"                                    FROM BPJS_RUJUKAN " + 
						"                                   WHERE CARD_NO = ? " + 
						"                                     AND BPJS_NO = ? " + 
						"                                     AND DEFUNCT_IND = 'N' " + 
						"                                   ORDER BY CREATED_AT DESC) " + 
						"                           WHERE ROWNUM = 1";
				Object dataNorujuk = DbConnection.executeScalar(DbConnection.KTHIS, searchQuery, new Object[] {mrn, noKa});
				if(dataNorujuk ==null)return rujukanFail("Data tidak ditemukan.");
				if(!(dataNorujuk instanceof String)) return rujukanFail("Data tidak valid");
				String noRujuk = (String) dataNorujuk;
				if(!noRujuk.contains(PPK_RS_MURNI_TEGUH)) {
					String queryDelete = "UPDATE BPJS_RUJUKAN " + 
							"   SET DEFUNCT_IND = 'Y' " + 
							" WHERE BPJS_RUJUKAN_ID = (SELECT * " + 
							"                            FROM (SELECT BPJS_RUJUKAN_ID " + 
							"                                    FROM BPJS_RUJUKAN " + 
							"                                   WHERE CARD_NO = ? " + 
							"                                     AND BPJS_NO = ? " + 
							"                                     AND DEFUNCT_IND = 'N' " + 
							"									  AND NOMOR_RUJUKAN = ?	 " +
							"                                   ORDER BY CREATED_AT DESC) " + 
							"                           WHERE ROWNUM = 1) " + 
							"";
					DbConnection.executeQuery(DbConnection.KTHIS, queryDelete, new Object[] {mrn, noKa, noRujuk});
					result.setMetaData(MetaData.oK());
				}else return rujukanFail("Rujukan terbaru bukan PCARE.");
			}catch(Exception ex) {
				System.out.println(ex.getMessage());
				result.setMetaData(MetaData.fail(ex.getMessage()));
			}
			return result;
		}
		
		@Override
		public ResponseRujukan checkAndBuildRujukanInpatientByRujukan(ResponseRujukan rujukan, RujukanInpatientParam param, JenisPelayanan jenisPelayanan) {
			ResponseRujukan result = rujukan;
			try {
				if(jenisPelayanan == null)return result;
				if(jenisPelayanan == JenisPelayanan.OUTPATIENT)return result;
				if(TextUtils.isEmpty(param.getPoli()))return result;
				if(!param.getPoli().equals("IGD"))param.setPoli("IGD");
				if(rujukan == null)return generateRujukanInpatient(param.getNoKa(), param.getPoli());
				if(rujukan.getMetaData() == null)return generateRujukanInpatient(param.getNoKa(), param.getPoli());
				if(TextUtils.isEmpty(rujukan.getMetaData().getCode()))return generateRujukanInpatient(param.getNoKa(), param.getPoli());
				if(rujukan.getMetaData().getCode().equals("200"))return result;
				else return generateRujukanInpatient(param.getNoKa(), param.getPoli());	
			}catch(Exception ex) {
				System.out.println(ex.getMessage());
			}
			
			return result;
		}
		
		private ResponseRujukan generateRujukanInpatient(String noKa, String poliCode) throws Exception {
			return generateRujukan(noKa, poliCode, TipeRujukan.FASKES_II);
		}
		
		private ResponseRujukan generateRujukan(String noKa, String poliCode, TipeRujukan faskes) throws Exception {
			ResponseRujukan result = null;
			if(faskes == null)return result;
			result = JsonUtil.toObjectFromJson(JsonUtil.toJsonString(BpjsService2.generateRujukan(noKa, faskes.getValue())), ResponseRujukan.class);
			result.getResponse().getRujukan().setPoliRujukan(Info.from(poliCode, ""));
			return result;
		}
	}
	
	
	private static abstract class SepProvider implements ISep, IRujukan.IValidateResult{
		private Version version;
		private String code;
		private String message;
		public SepProvider(Version version) {
			this.version = version;
		}
		
		public Version getVersion() {
			return version;
		}
		
		@Override
		public void onError(String code, String message) {
			setCode(code);
			setMessage(message);
		}
		
		public Person getPerson(String mrn) {
			Person result = null;
			try {
				String query = "SELECT * FROM PERSON P " + 
						"JOIN CARD C ON C.PERSON_ID = P.PERSON_ID AND C.CARD_NO = ? AND C.DEFUNCT_IND = 'N' " + 
						"WHERE P.DEFUNCT_IND = 'N'";
				List rawData = DbConnection.executeReader(DbConnection.KTHIS, query, new Object[] {mrn}, 60);
				if(rawData == null)return result;
				if(rawData.size() !=1) return result;
				result = JsonUtil.getMappedClass((HashMap)rawData.get(0), Person.class, true);
			}catch(Exception ex) {
				System.out.println(ex.getMessage());
				ex.printStackTrace();
			}
			return result;
		}
		
		public RepeatTreatmentView getTreatmentView(String noKa, String noRujukan, String tipeRujukan) {
			RepeatTreatmentView result = null;
			try {
				String query = "SELECT RJ.CARD_NO, " + 
						"       RJ.BPJS_NO, " + 
						"       RJ.NOMOR_RUJUKAN, " + 
						"       RJ.TGL_RUJUKAN, " + 
						"       V.VISIT_ID, " + 
						"       SB.CAREPROVIDER_ID, " + 
						"       SB.SEP_NO, " + 
						"       SB.VISIT_ID, " + 
						"       HB.HFIS_ID, " + 
						"       POLY.CODE_ABBR AS POLI_CODE " + 
						"  FROM (SELECT * " + 
						"          FROM (SELECT CARD_NO, " + 
						"                       BPJS_NO, " + 
						"                       NOMOR_RUJUKAN, " + 
						"                       TGL_RUJUKAN, " + 
						"                       CREATED_AT " + 
						"                  FROM BPJS_RUJUKAN " + 
						"                 WHERE NOMOR_RUJUKAN = ? " + 
						"                   AND BPJS_NO = ? " + 
						"                   AND DEFUNCT_IND = 'N' " + 
						"                 ORDER BY CREATED_AT DESC) " + 
						"         WHERE ROWNUM = 1) RJ " + 
						" INNER JOIN (SELECT SEPBPJS.*, " + 
						"                    ROW_NUMBER() OVER(PARTITION BY CARD_NO ORDER BY CREATED_AT DESC) AS RN " + 
						"               FROM SEPBPJS " + 
						"              WHERE DEFUNCT_IND = 'N' " + 
						"                AND TIPE_RUJUKAN = ?) SB " + 
						"    ON SB.BPJS_NO = RJ.BPJS_NO " + 
						"   AND SB.CARD_NO = RJ.CARD_NO " + 
						"   AND SB.RN = 1 " + 
						"   AND SB.CREATED_AT BETWEEN TRUNC(RJ.TGL_RUJUKAN) AND " + 
						"       ADD_MONTHS(RJ.TGL_RUJUKAN, 3) " + 
						" INNER JOIN VISIT V " + 
						"    ON V.VISIT_ID = SB.VISIT_ID " + 
						"  LEFT JOIN (SELECT HFISBPJS.*, " + 
						"                    ROW_NUMBER() OVER(PARTITION BY CAREPROVIDER_ID ORDER BY CREATED_DATETIME DESC) " + 
						"               FROM HFISBPJS " + 
						"              WHERE DEFUNCT_IND = 'N' " + 
						"                AND HFIS_ID IS NOT NULL) HB " + 
						"    ON HB.CAREPROVIDER_ID = SB.CAREPROVIDER_ID " + 
						"  LEFT JOIN (SELECT CM.CODE_ABBR, RM.SUBSPECIALTYMSTR_ID, RM.CAREPROVIDER_ID " + 
						"               FROM RESOURCEMSTR RM " + 
						"              INNER JOIN DOCTORSPECIALTYMSTR SM " + 
						"                 ON SM.SUBSPECIALTY_CODE = RM.POLI_CODE " + 
						"                AND SM.DEFUNCT_IND = 'N' " + 
						"              INNER JOIN CODEMSTR CM " + 
						"                 ON CM.CODE_CAT || '' || CM.CODE_ABBR = SM.SPECIALTY_CODE " + 
						"                AND CM.DEFUNCT_IND = 'N' " + 
						"              WHERE RM.DEFUNCT_IND = 'N' " + 
						"                AND RM.SUBSPECIALTYMSTR_ID = 336581902) POLY " + 
						"    ON POLY.CAREPROVIDER_ID = SB.CAREPROVIDER_ID ";
				List rawData = DbConnection.executeReader(DbConnection.KTHIS, query, new Object[] {noRujukan, noKa, tipeRujukan}, 60);
				if(rawData == null)return result;
				if(rawData.size() !=1) return result;
				result = JsonUtil.getMappedClass((HashMap)rawData.get(0), RepeatTreatmentView.class, true);
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
				ex.printStackTrace();
			}
			return result;
		}
		
		public ResponseRegisterSep insertSep(Platform platform, RegisterSepParam param, Rujukan rujukan) {
			return insertSep(platform, param, rujukan, null);
		}
		
		private boolean isEmpty(String target) {
			return TextUtils.isEmpty(target) ? true : target.equals("-");
		}
		
		protected String getRelatedPoli(String poliPilihan, String poliRujukan) {
			String result = poliPilihan;
			try {
				String query = "SELECT * " + 
						"  FROM (SELECT BRP.POLI_CODE_RELATED " + 
						"          FROM BPJS_RELATED_POLI BRP " + 
						"         INNER JOIN CODEMSTR CM " + 
						"            ON CM.CODE_CAT = 'POL' " + 
						"           AND CM.CODE_ABBR = BRP.POLI_CODE_RELATED " + 
						"         WHERE BRP.POLI_CODE = ? " + 
						"           AND BRP.POLI_CODE_RELATED = ? " + 
						"			AND BRP.DEFUNCT_IND = 'N' " +
						"         ORDER BY BRP.CREATED_DATETIME DESC) " + 
						" WHERE ROWNUM = 1";
				Object rawData = DbConnection.executeScalar(DbConnection.KTHIS, query, new Object[] {poliPilihan, poliRujukan}, 60);
				if(rawData == null)return result;
				if(!(rawData instanceof String))return result;
				result = (String)rawData;
			}catch (Exception ex) {
				ex.printStackTrace();
				System.out.println(ex.getMessage());
			}
			return result;
		}
		
		public ResponseRegisterSep insertSep(Platform platform, RegisterSepParam param, Rujukan rujukan, RepeatTreatmentView treatment) {
			ResponseRegisterSep result = new ResponseRegisterSep();
			String user = param.getUserId() == null ? "327759870" : param.getUserId().toString();
			String userName = TextUtils.isEmpty(param.getUserName()) ? "SELFREGISTRATION" : param.getUserName();
			String noSep = null;
			try {
				Peserta peserta = rujukan.getPeserta();
				Person person = getPerson(param.getMrn());
				if(person == null) return ResponseRegisterSep.fail("Person KTHIS tidak ditemukan, silahkan hubungi CS/IT RS. Murni Teguh.");
				String noKa = peserta.getNoKartu();
				String jnsPelayanan = param.getJnsPelayanan();
				String klsRawat = peserta.getHakKelas().getKode();
				String mrn = param.getMrn();
				String asalRujukan = rujukan.getFaskes();
				String tglRujukan = rujukan.getTglKunjungan();
				String noRujukan = rujukan.getNoKunjungan();
				String asalPerujuk = rujukan.getProvPerujuk().getKode();
				String catatan = "-";
				String diagAwal = rujukan.getDiagnosa().getKode();
				String poli = param.getPoli();
				System.out.println(JsonUtil.toJsonString(rujukan));
				System.out.println("\nKode poli rujukan : " + rujukan.getPoliRujukan().getKode());
				poli = param.getPoli().equals(rujukan.getPoliRujukan().getKode()) ? param.getPoli() : getRelatedPoli(poli, rujukan.getPoliRujukan().getKode());
				System.out.println("\nKode poli akhir : " + rujukan.getPoliRujukan().getKode());
				String Eks = TextUtils.isEmpty(param.getEks()) ? "0" : param.getEks();
				String lakaLantas = TextUtils.isEmpty(param.getLakaLantas()) ? "0" : param.getLakaLantas();
				String penjamin = param.getPenjamin();
				String tglKejadian = param.getTglKejadian();
				String ket = param.getKet();
				String suplesi = TextUtils.isEmpty(param.getSuplesi()) ? "0" : param.getSuplesi();
				String noSuplesi = param.getNoSuplesi();
				String kdPro = param.getKdPro();
				String kdKab = param.getKdKab();
				String kdKec = param.getKdKec();
				String noSurat = TextUtils.isEmpty(param.getNoSurat()) ? BpjsService2.getLastControlletter(mrn) : BpjsService2.max6DigitNumeric(param.getNoSurat());
				String dpjp = treatment == null ? BpjsService2.getMapHFISId(param.getCareProviderId()) : treatment.getHfis_id();
				String noHp = getValidPatientMobilePhoneNoFrom(person.getMOBILE_PHONE_NO(), peserta.getMr().getNoTelepon());
				String insertSepResponse = BpjsService2.insertSep3(noKa, jnsPelayanan, klsRawat, mrn, asalRujukan, tglRujukan, noRujukan, asalPerujuk, catatan, diagAwal
						, poli, Eks, lakaLantas, penjamin, tglKejadian, ket, suplesi, noSuplesi, kdPro, kdKab, kdKec, noSurat, dpjp, noHp, userName);
				result = JsonUtil.toObjectFromJson(insertSepResponse, ResponseRegisterSep.class);
				if(result == null)return result;
				if(result.getMetaData().getCode().equals("200")) {
					//Masukkan ke database sepbpjs, bpjs_rujukan dll sebagai history
					noSep = result.getResponse().getSep().getNoSep();
					if(isEmpty(result.getResponse().getSep().getPpkPerujuk())) result.getResponse().getSep().setPpkPerujuk(peserta.getProvUmum().getNmProvider());
					if(isEmpty(result.getResponse().getSep().getDiagnosa())) result.getResponse().getSep().setDiagnosa(rujukan.getDiagnosa().getNama());
					if(isEmpty(result.getResponse().getSep().getKelasRawat()))result.getResponse().getSep().setKelasRawat(peserta.getHakKelas().getKeterangan());
					if(platform == Platform.KTHIS) {
						SepBpjsData sepBpjsData = new SepBpjsData(param.getVisitId(),  result.getResponse().getSep().getNoSep(),
								param.getQueueNo(),BigDecimal.valueOf(param.getCareProviderId()), 
								asalRujukan, noSurat, lakaLantas,
								tglKejadian == null ? null : new Timestamp(dateFormat.parse(tglKejadian).getTime()),
								param.getTempatLakaLantas(), ket, "",param.getAsuransi());
						RujukanData rujukanData = new RujukanData(rujukan.getProvPerujuk().getNama(),
								TextUtils.isEmpty(rujukan.getTglKunjungan()) ? null : new Timestamp(dateFormat.parse(rujukan.getTglKunjungan()).getTime()),
								rujukan.getNoKunjungan());

						PostRegisterData postSepBpjs = new PostRegisterData(
								BigDecimal.valueOf(Long.valueOf(user)),
								BigDecimal.valueOf(Long.valueOf(param.getMrn())), 
								noKa, 
								sepBpjsData, 
								rujukanData);
						PostRegister(postSepBpjs);
					}
				}
			} catch (Exception ex) {
				if(result !=null) {
					if(result.getMetaData() !=null) {
						if(result.getMetaData().getCode().equals("200")) {
							BpjsService2.hapusSep(noSep, user);
						}
					}
					
				}
				System.out.println(ex.getMessage());
				ex.printStackTrace();
				result = ResponseRegisterSep.error(ex);
			}
			return result;
		}
		
		@Override
		public void PostRegister(PostRegisterData postRegisterData) throws SQLException, Exception {
			SepBpjs sepBpjs = postRegisterData.getSepBpjs();
			insertSepBpjs(sepBpjs);
			insertRujukanBpjs(postRegisterData);
		}
		
		private void insertRujukanBpjs(PostRegisterData postRegisterData) throws SQLException, Exception {
//		String nmProviderRujukan, Timestamp tglKunjungan, String, String mrn, String noKa, BigDecimal userId) throws SQLException, Exception {
			String query = "INSERT INTO BPJS_RUJUKAN(BPJS_RUJUKAN_ID, " + 
					"CARD_NO, " + 
					"BPJS_NO , " + 
					"ASAL_FKTP,  " + 
					"TGL_RUJUKAN, " + 
					"NOMOR_RUJUKAN,  " + 
					"CREATED_BY, " + 
					"CREATED_AT,   " + 
					"MODIFIED_BY, " + 
					"MODIFIED_AT, " + 
					"DEFUNCT_IND) VALUES(pgSYSFUNC.fxGenPrimaryKey, ?,?,?,?,?,?,SYSDATE,?,SYSDATE,'N')";
					DbConnection.executeQuery(DbConnection.KTHIS, query, new Object[] {
							postRegisterData.getMrn(), postRegisterData.getBpjsNo(),
							postRegisterData.getRujukanData().getNmProviderRujuk(),
							postRegisterData.getRujukanData().getTglKunjungan(),
							postRegisterData.getRujukanData().getNomorRujukan(),
							postRegisterData.getUserId(),
							postRegisterData.getUserId()}, 60);
		}

		private void insertSepBpjs(SepBpjs sepBpjs) throws SQLException, Exception {
			String query = "INSERT INTO SEPBPJS( " + 
					"SEPBPJS_ID,  " + 
					"CARD_NO, " + 
					"BPJS_NO, " + 
					"SEP_NO,      " + 
					"VISIT_ID,   " + 
					"CAREPROVIDER_ID, " + 
					"QUEUE_NO,   " + 
					"CREATED_BY,      " + 
					"CREATED_AT,   " + 
					"MODIFIED_BY,  " + 
					"MODIFIED_AT,  " + 
					"DEFUNCT_IND,   " + 
					"LAKALANTAS,      " + 
					"DATE_LAKALANTAS, " + 
					"SEP_LAKALANTAS,   " + 
					"ASURANSI_LAKALANTAS,  " + 
					"LETTER_NO,    " + 
					"PLACE_LAKALANTAS, " + 
					"DESCRIPTION_LAKALANTAS,      " + 
					"TIPE_RUJUKAN ) " + 
					"VALUES ( PGSYSFUNC.FXGENPRIMARYKEY, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "'N', "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?, "
					+ "?) ";
			DbConnection.executeQuery(DbConnection.KTHIS, query, new Object[] {
					sepBpjs.getCardNo(), sepBpjs.getBpjsNo(), sepBpjs.getSepNo(), sepBpjs.getVisitId(), sepBpjs.getCareproviderId(), sepBpjs.getQueueNo(),
					sepBpjs.getCreatedBy(), sepBpjs.getCreatedAt(), sepBpjs.getModifiedBy(), sepBpjs.getModifiedAt(), sepBpjs.getLakaLantas(), 
					sepBpjs.getTglLantas(), sepBpjs.getSepLakaLantas(), sepBpjs.getAsuransiLakaLantas(), sepBpjs.getLetterNo(), 
					"", sepBpjs.getDescriptLakaLantas(), sepBpjs.getTipeRujukan()}, 60);
		}

		private Boolean validationMobilePhoneNo(String mobilePhoneNo) {
			Boolean result = true;
			if (TextUtils.isEmpty(mobilePhoneNo))
				return false;
			int count = mobilePhoneNo.length();
			if ((count != 11 && count != 12))
				return false;
			String defaultNumber11 = "00000000000";
			String defaultNumber12 = "000000000000";
			if (mobilePhoneNo.equals(defaultNumber11) || mobilePhoneNo.equals(defaultNumber12))
				return false;
			return result;
		}
		
		private String getValidPatientMobilePhoneNoFrom(String kthisPatientMobilePhoneNo,
				String bpjsPatientMobilePhoneNo) {
			Boolean isKTHISValid = validationMobilePhoneNo(kthisPatientMobilePhoneNo);
			Boolean isBPJSValid = validationMobilePhoneNo(bpjsPatientMobilePhoneNo);
			if (isKTHISValid)
				return kthisPatientMobilePhoneNo;
			else if (!isKTHISValid && isBPJSValid)
				return bpjsPatientMobilePhoneNo;
			else
				return BASIC_PHONE_NUMBER;
		}
		
		public boolean isRegistrationDataValidated(Platform platform, RegisterSepParam param) {
			boolean result = true;
			if(TextUtils.isEmpty(param.getMrn())) {
				return notValidated("MRN tidak boleh kosong.");
			}
			
			if(TextUtils.isEmpty(param.getJnsPelayanan())) {
				return notValidated("Jenis pelayanan harus dipilih.");
			}
			
			if(TextUtils.isEmpty(param.getNoKa())) {
				return notValidated("Nomor kartu BPJS tidak boleh kosong.");
			}
			
			if(TextUtils.isEmpty(param.getPoli())) {
				return notValidated("Poli harus dipilih.");
			}
			
			if(param.getCareProviderId() == null) {
				return notValidated("Dokter harus dipilih.");
			}
			
			if(TextUtils.isEmpty(param.getFaskes())) {
				return notValidated("Faskes harus dipilih.");
			}
			
			if(platform == Platform.KTHIS) {
				if(param.getVisitId() == null)return notValidated("Visit ID harus ada.");
				if(!param.getJnsPelayanan().equals("1")) { //Pelayanan selain rawat inap
					if(TextUtils.isEmpty(param.getQueueNo()))return notValidated("Nomor antrian harus ada.");
				}
				
			}
			return result;
		}
		
		private boolean notValidated(String message) {
			setMessage(message);
			return false;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
	}
	
	public static IRujukan Rujukan = new RujukanProvider() {
		
	};
	
	
	public static ISep SEP = new SepProvider(Version.getVersion()) {

		@Override
		public String Register(RegisterSepParam param) {
			return Register(param, Platform.KTHIS);
		}

		@Override
		public String Register(RegisterSepParam param, Platform platform) {
			String result = null;
			if(!isRegistrationDataValidated(platform, param)) return JsonUtil.toJsonString(Response.fail(getMessage()));
			if(getVersion().equals(Version.V1_1)) {
				ResponseRujukan rujukan = Rujukan.getRujukanTerakhirFromList(param.getNoKa());
				rujukan = Rujukan.checkAndBuildRujukanInpatientByRujukan(rujukan, 
						IRujukan.RujukanInpatientParam.build(param.getNoKa(), param.getPoli()), 
						IRujukan.JenisPelayanan.from(param.getJnsPelayanan()));
//				try {
//					rujukan = JsonUtil.toObjectFromJson(JsonUtil.toJsonString(BpjsService2.generateRujukan(param.getNoKa(), "1")), ResponseRujukan.class);
//					rujukan.getResponse().getRujukan().setPoliRujukan(Info.from("URO", "UROLOGI"));
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				if(!Rujukan.validateRujukan(rujukan, this))return JsonUtil.toJsonString(Response.fail(getCode(), getMessage()));
				String noRujukan = rujukan.getResponse().getRujukan().getNoKunjungan();
				String tipeRujukan = rujukan.getResponse().getRujukan().getFaskes();
				RepeatTreatmentView treatment = getTreatmentView(param.getNoKa(), noRujukan, tipeRujukan);
				boolean isRujukanRegistered = treatment != null;
				if(isRujukanRegistered) { // Berobat berulang
					String treatmentPoly = treatment.getPoli_code();
					String paramPoly = param.getPoli();
					boolean isSamePoly = treatmentPoly.equals(paramPoly);
					if(!isSamePoly && param.getFaskes().equals("2")) {
						String user = param.getUserId() == null ? "327759870" : param.getUserId().toString();
						ResponseRujukanInsert responseRujukanInsert = Rujukan.insertRujukan(treatment.getSep_no(), param.getJnsPelayanan(), param.getKet(), param.getPoli(), user);
						if(!Rujukan.validateRujukan(responseRujukanInsert, this))return JsonUtil.toJsonString(Response.fail(getCode(), getMessage()));
						rujukan = Rujukan.getRujukanTerakhirFromList(param.getNoKa());
						if(!Rujukan.validateRujukan(rujukan, this))return JsonUtil.toJsonString(Response.fail(getCode(), getMessage()));
					}
				}else { // Berobat pertama kali
					
				}
				
				ResponseRegisterSep registerSep = insertSep(platform, param, rujukan.getResponse().getRujukan());
				if(registerSep == null) return JsonUtil.toJsonString(Response.fail("Gagal insert sep, silahkan coba kemabali, jika tetap berlanjut hubungi CS/IT RS. Murni Teguh."));
				return JsonUtil.toJsonString(registerSep);
			}else if(getVersion().equals(Version.V1_0)) {
				
			}else {
				
			}
			return result;
		}
		
	};
}