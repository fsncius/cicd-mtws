package com.rsmurniteguh.webservice.dep.kthis.services;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Base64;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.rsmurniteguh.webservice.dep.all.model.mobile.LoginExternal;
import com.rsmurniteguh.webservice.dep.all.model.mobile.LoginForm;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInfoResponse;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.AES;
import com.rsmurniteguh.webservice.dep.util.EncryptPatient;
import com.rsmurniteguh.webservice.dep.util.EncryptUtil;
import com.rsmurniteguh.webservice.dep.util.Encryptor;

public class MobileAuth {
	private static Boolean complete = false;

	public static ResponseString getChangePasswordPatient(String usermstrId, String password, String new_password) {
		ResponseString data = new ResponseString();
		Connection Connection = null;
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null, rs2 = null;

		String pass = EncryptPatient.getInstance().encryptOnlineAppointment(Encryptor.getInstance().decrypt(password));

		try {
			Connection = DbConnection.getBpjsOnlineInstance();
			if (Connection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT user_id FROM tb_user WHERE user_id = ? AND password = ?";

			ps = Connection.prepareStatement(selectQuery);
			ps.setString(1, usermstrId);
			ps.setString(2, pass);
			rs = ps.executeQuery();

			if (rs.next()) {
				String Ids = rs.getString("user_id");
				String encrypt = EncryptPatient.getInstance()
						.encryptOnlineAppointment(Encryptor.getInstance().decrypt(new_password));
				String updatequery = "UPDATE tb_user SET password = ? WHERE user_id = ? ";
				ps2 = Connection.prepareStatement(updatequery);
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(60000);
				ps2.setString(1, encrypt);
				ps2.setString(2, Ids);
				data.setResponse("Ganti password berhasil");
				rs2 = ps2.executeQuery();
			} else {
				data.setResponse("Password salah");
			}
		} catch (SQLException e) {
			if (Connection != null)
				try {
					Connection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			if (Connection != null)
				try {
					Connection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (Connection != null)
				try {
					Connection.close();
				} catch (SQLException e) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (SQLException e) {
				}
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
				}
			if (ps2 != null)
				try {
					ps2.close();
				} catch (SQLException e) {
				}
			if (rs2 != null)
				try {
					rs2.close();
				} catch (SQLException e) {
				}
		}
		return data;
	}

	public static void insertAuthorisation(String usermstr_id, String key, String salt) {
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return;
		String SPsql = "EXEC logUserToken ?,?,?";
		PreparedStatement ps = null;
		;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setBigDecimal(1, new BigDecimal(usermstr_id));
			ps.setString(2, key);
			ps.setString(3, salt);

			ps.executeUpdate();
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
	}

	public static ResponseString SaveRegistrationToken(String user, String token) {
		System.out.println("Save token of User = " + user);
		ResponseString res = new ResponseString();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		String SPsql = "update usertoken set firebase_token = ? where usermstr_id = ? and deleted = 0";
		PreparedStatement ps = null;
		;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, token);
			ps.setString(2, user);

			ps.executeUpdate();
			res.setResponse("OK");
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
			res.setResponse("FAIL");
		} finally {
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return res;
	}

	public static String getTokenByUsermstrId(BigDecimal usermstrId) {
		String token = null;
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		PreparedStatement st = null;
		ResultSet rs = null;
		String sql = "SELECT firebase_token from usertoken where usermstr_id = ? and deleted = 0";
		try {
			st = connection.prepareStatement(sql);
			st.setBigDecimal(1, usermstrId);

			rs = st.executeQuery();
			while (rs.next()) {
				token = rs.getString("TOKEN");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return token;

	}

	public static void insertBarcode(String registration_id) {
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return;
		String SPsql = "EXEC generateBarcode ?";
		PreparedStatement ps = null;
		;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, registration_id);

			ps.executeUpdate();
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
	}

	public static LoginExternal login_external(LoginForm loginform) {
		System.out.println("login_external...");

		LoginExternal res = new LoginExternal();

		String user = loginform.getUsername();
		String pass = loginform.getPassword();

		Connection connection = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;

		PreparedStatement st = null;
		ResultSet rs = null;

		try {

			String reEncryptpass = EncryptUtil.getInstance()
					.nonReversibleEncode(Encryptor.getInstance().decrypt(pass));
			String salt = AES.getInstance().generateSalt();

			String sql = "select um.USERMSTR_ID, um.PASSWORD_EXPIRY_DATE, " + "um.USER_CODE, um.USER_NAME "
					+ ", cp.CAREPROVIDER_TYPE, PGCOMMON.FXGETCODEDESC(cp.CAREPROVIDER_TYPE) as TYPE_DESC,  ep.DESIGNATION "
					+ "from usermstr um " + "inner join USERPROFILE up on up.USERMSTR_ID = um.USERMSTR_ID "
					+ "left outer join CAREPROVIDER cp on cp.PERSON_ID = up.PERSON_ID and cp.defunct_ind = 'N' "
					+ "left outer join employee ep on ep.PERSON_ID = up.PERSON_ID and ep.defunct_ind = 'N'"
					+ " where user_code = ?  and password = ?  and um.defunct_ind = 'N' "
					+ " and up.defunct_ind = 'N' ";

			st = connection.prepareStatement(sql);
			st.setEscapeProcessing(true);
			st.setQueryTimeout(60000);
			st.setString(1, user);
			st.setString(2, reEncryptpass);
			rs = st.executeQuery();

			res.setDescription("FAIL");
			if (rs.next()) {
				res.setResponse(true);
				res.setDescription("OK");
				res.setUsermstr_id(rs.getString("USERMSTR_ID"));
				res.setUser_name(rs.getString("USER_NAME"));
				res.setUsertype(rs.getString("CAREPROVIDER_TYPE"));
				res.setTypedesc(rs.getString("TYPE_DESC"));
				res.setDesignation(rs.getString("DESIGNATION"));
				res.setLogintype("staff");
				res.setSalt(salt);

				boolean isAuthPR = MobileDoctor.isUserAuthorizationPR(rs.getString("USERMSTR_ID"));
				if (isAuthPR)
					res.setIsAuthPR("1");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			res.setDescription(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}

		return res;
	}

	@SuppressWarnings("finally")
	public static ResponseString CheckLogin(String user, String pass) {
		System.out.println("CheckLogin...");

		ResponseString str = new ResponseString();

		Connection connection = null, connection2 = null;
		connection = DbConnection.getPooledConnection();
		connection2 = DbConnection.getBpjsOnlineInstance();
		if (connection == null || connection2 == null)
			return null;

		PreparedStatement st = null, st2 = null;
		ResultSet rs = null, rs2 = null;

		complete = false;
		try {

			String decrypted = Encryptor.getInstance().decrypt(pass);
			String reEncryptpass = EncryptUtil.getInstance()
					.nonReversibleEncode(Encryptor.getInstance().decrypt(pass));
			String reEncryptAntrian = EncryptPatient.getInstance()
					.encryptOnlineAppointment(Encryptor.getInstance().decrypt(pass));
			String salt = AES.getInstance().generateSalt();
			String encodedSalt = Base64.getEncoder().encodeToString(salt.getBytes());

			String sql = "select um.USERMSTR_ID, um.PASSWORD_EXPIRY_DATE, " + "um.USER_CODE, um.USER_NAME "
					+ ", cp.CAREPROVIDER_TYPE, PGCOMMON.FXGETCODEDESC(cp.CAREPROVIDER_TYPE) as TYPE_DESC,  ep.DESIGNATION "
					+ "from usermstr um " + "inner join USERPROFILE up on up.USERMSTR_ID = um.USERMSTR_ID "
					+ "left outer join CAREPROVIDER cp on cp.PERSON_ID = up.PERSON_ID and cp.defunct_ind = 'N' "
					+ "left outer join employee ep on ep.PERSON_ID = up.PERSON_ID and ep.defunct_ind = 'N'"
					+ " where user_code = ?  and password = ?  and um.defunct_ind = 'N' "
					+ " and up.defunct_ind = 'N' ";

			String sql2 = "select user_id , no_mrn, no_ktp, username, password from tb_user u where LOWER(u.username) = ? and u.password = ? "
					+ " order by u.user_id ";

			st = connection.prepareStatement(sql);
			st.setEscapeProcessing(true);
			st.setQueryTimeout(60000);
			st.setString(1, user);
			st.setString(2, reEncryptpass);
			rs = st.executeQuery();

			if (rs.next()) {
				String uid = rs.getString("USERMSTR_ID");
				HashMap<String, Object> additionalClaims = new HashMap<String, Object>();
				additionalClaims.put("usermstr_id", rs.getString("USERMSTR_ID"));
				additionalClaims.put("name", rs.getString("USER_NAME"));
				additionalClaims.put("usertype", rs.getString("CAREPROVIDER_TYPE"));
				additionalClaims.put("typedesc", rs.getString("TYPE_DESC"));
				additionalClaims.put("designation", rs.getString("DESIGNATION"));
				additionalClaims.put("logintype", "staff");
				additionalClaims.put("mrn", null);
				additionalClaims.put("salt", salt);
				additionalClaims.put("username", user);
				additionalClaims.put("encrypt_pass", pass);

				boolean isAuthPR = MobileDoctor.isUserAuthorizationPR(uid);
				if (isAuthPR)
					additionalClaims.put("isAuthPR", "1");

				FirebaseAuth.getInstance().createCustomToken(uid, additionalClaims)
						.addOnSuccessListener(new OnSuccessListener<String>() {
							@Override
							public void onSuccess(String customToken) {
								str.setResponse(customToken);
								System.out.println(customToken);
								AuthMobileFunctions.insertAuthorisation(uid, customToken, encodedSalt);
								complete = true;
							}
						});
			} else {

				st2 = connection2.prepareStatement(sql2);
				st2.setEscapeProcessing(true);
				st2.setQueryTimeout(60000);
				st2.setString(1, user.toLowerCase());
				st2.setString(2, reEncryptAntrian);
				rs2 = st2.executeQuery();

				if (rs2.next()) {
					String uid2 = rs2.getString("user_id");
					HashMap<String, Object> additionalClaims2 = new HashMap<String, Object>();
					additionalClaims2.put("usermstr_id", rs2.getString("user_id"));
					additionalClaims2.put("name", rs2.getString("username"));
					additionalClaims2.put("logintype", "patient");
					additionalClaims2.put("mrn", rs2.getString("no_mrn"));
					additionalClaims2.put("salt", salt);
					FirebaseAuth.getInstance().createCustomToken(uid2, additionalClaims2)
							.addOnSuccessListener(new OnSuccessListener<String>() {
								@Override
								public void onSuccess(String customToken) {
									str.setResponse(customToken);
									System.out.println(customToken);
									AuthMobileFunctions.insertAuthorisation(uid2, customToken, encodedSalt);
									complete = true;
								}
							});
				} else {
					complete = true;
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			complete = true;
		}

		finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}

			if (rs2 != null)
				try {
					rs2.close();
				} catch (Exception ignore) {
				}
			if (st2 != null)
				try {
					st2.close();
				} catch (Exception ignore) {
				}
			if (connection2 != null)
				try {
					connection2.close();
				} catch (Exception ignore) {
				}

			int a = 0;

			while (!complete) {
				try {
					
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (a > 60)
					break;
				else {
					a += 10;
				}
			}
			return str;

		}

	}

	@SuppressWarnings("finally")
	public static ResponseString CheckLoginStaff(String user, String pass) {
		System.out.println("CheckLoginStaff...");

		ResponseString str = new ResponseString();

		Connection connection = DbConnection.getPooledConnection();
		Connection connection2 = DbConnection.getHrisInstance();
		if (connection == null)
			return null;

		PreparedStatement st = null, st2 = null, st3 = null;
		ResultSet rs = null, rs2 = null, rs3 = null;

		complete = false;
		try {

			String reEncryptpass = EncryptUtil.getInstance()
					.nonReversibleEncode(Encryptor.getInstance().decrypt(pass));
			String reEncryptpass_hris = EncryptUtil.getInstance().StringtoMD5(Encryptor.getInstance().decrypt(pass));
			String salt = AES.getInstance().generateSalt();

			String encodedSalt = Base64.getEncoder().encodeToString(salt.getBytes());
			
			String sql = "select um.USERMSTR_ID, um.PASSWORD_EXPIRY_DATE, " + "um.USER_CODE, um.USER_NAME "
					+ ", cp.CAREPROVIDER_TYPE, PGCOMMON.FXGETCODEDESC(cp.CAREPROVIDER_TYPE) as TYPE_DESC,  ep.DESIGNATION "
					+ "from usermstr um " + "inner join USERPROFILE up on up.USERMSTR_ID = um.USERMSTR_ID "
					+ "left outer join CAREPROVIDER cp on cp.PERSON_ID = up.PERSON_ID and cp.defunct_ind = 'N' "
					+ "left outer join employee ep on ep.PERSON_ID = up.PERSON_ID and ep.defunct_ind = 'N'"
					+ " where user_code = ?  and password = ?  and um.defunct_ind = 'N' "
					+ " and up.defunct_ind = 'N' ";

			String sql2 = " SELECT usr.Username, usr.Password, kar.Nama, usr.IDKaryawan , kar.KodeJabatan, kar.KodeBagian, jab.Jabatan, bag.bagian "
					+ "FROM TblUsers usr  " + "left join TblKaryawan kar ON usr.IDKaryawan = kar.IDKaryawan "
					+ "left join TblJabatan jab ON kar.KodeJabatan = jab.KodeJabatan "
					+ "left join TblBagian bag ON kar.KodeBagian = bag.Kodebagian "
					+ "where usr.Username = ?  and usr.Password = ?  and usr.Aktif = 1 ";

			st = connection.prepareStatement(sql);
			st.setEscapeProcessing(true);
			st.setQueryTimeout(60000);
			st.setString(1, user);
			st.setString(2, reEncryptpass);
			rs = st.executeQuery();

			if (rs.next()) {
				String uid = rs.getString("USERMSTR_ID");
				HashMap<String, Object> ct_kthis = new HashMap<String, Object>();
				ct_kthis.put("usermstr_id", rs.getString("USERMSTR_ID"));
				ct_kthis.put("username", user);
				ct_kthis.put("name", rs.getString("USER_NAME"));
				ct_kthis.put("usertype", rs.getString("CAREPROVIDER_TYPE"));
				ct_kthis.put("typedesc", rs.getString("TYPE_DESC"));
				ct_kthis.put("designation", rs.getString("DESIGNATION"));
				ct_kthis.put("logintype", "kthis");
				ct_kthis.put("salt", encodedSalt);
				ct_kthis.put("encrypt_pass", pass);

				boolean isAuthPR = MobileDoctor.isUserAuthorizationPR(uid);
				if (isAuthPR)
					ct_kthis.put("isAuthPR", "1");

				FirebaseApp instanceStaff = FirebaseApp.getInstance("staff");

				FirebaseAuth.getInstance(instanceStaff).createCustomToken(uid, ct_kthis)
						.addOnSuccessListener(new OnSuccessListener<String>() {
							@Override
							public void onSuccess(String customToken) {
								str.setResponse(customToken);
								System.out.println(customToken);
								AuthMobileFunctions.insertAuthorisation(uid, customToken, encodedSalt);
								complete = true;
							}
						});
			} else {

				st2 = connection2.prepareStatement(sql2);
				st2.setEscapeProcessing(true);
				st2.setQueryTimeout(60000);
				st2.setString(1, user);
				st2.setString(2, reEncryptpass_hris);
				rs2 = st2.executeQuery();

				if (rs2.next()) {
					String Ids = rs2.getString("Username");
					String updatequery = "UPDATE TblUsers SET LastLogin = sysdatetime(), Online = 1 WHERE Username = ? ";
					st3 = connection2.prepareStatement(updatequery);
					st3.setEscapeProcessing(true);
					st3.setQueryTimeout(60000);
					st3.setString(1, Ids);
					st3.executeUpdate();

					String uid = rs2.getString("IDKaryawan");
					HashMap<String, Object> ct_hris = new HashMap<String, Object>();
					ct_hris.put("usermstr_id", uid);
					ct_hris.put("username", user);
					ct_hris.put("name", rs2.getString("Nama"));
					ct_hris.put("usertype", rs2.getString("KodeBagian"));
					ct_hris.put("typedesc", rs2.getString("Jabatan") + " - " + rs2.getString("bagian"));
					ct_hris.put("designation", rs2.getString("KodeJabatan"));
					ct_hris.put("logintype", "hris");
					ct_hris.put("salt", encodedSalt);

					FirebaseApp instanceStaff = FirebaseApp.getInstance("staff");

					FirebaseAuth.getInstance(instanceStaff).createCustomToken(uid, ct_hris)
							.addOnSuccessListener(new OnSuccessListener<String>() {
								@Override
								public void onSuccess(String customToken) {
									str.setResponse(customToken);
									System.out.println(customToken);
									AuthMobileFunctions.insertAuthorisation(uid, customToken, encodedSalt);
									complete = true;
								}
							});
				} else {
					complete = true;
				}

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			complete = true;
		}

		finally {
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
			if (connection2 != null)
				try {
					connection2.close();
				} catch (Exception ignore) {
				}

			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}

			if (rs2 != null)
				try {
					rs2.close();
				} catch (Exception ignore) {
				}
			if (st2 != null)
				try {
					st2.close();
				} catch (Exception ignore) {
				}

			if (rs3 != null)
				try {
					rs3.close();
				} catch (Exception ignore) {
				}
			if (st3 != null)
				try {
					st3.close();
				} catch (Exception ignore) {
				}

			int a = 0;

			while (!complete) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (a > 60)
					break;
				else {
					a += 10;
				}
			}
			return str;
		}
	}

	
	@SuppressWarnings("finally")
	public static ResponseString CheckLoginPatient(String user, String pass) {
		System.out.println("CheckLoginPatient...");

		ResponseString str = new ResponseString();

		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;

		PreparedStatement st = null;
		ResultSet rs = null;

		complete = false;
		try {

			EncryptPatient.getInstance();
			String reEncryptAntrian = EncryptPatient.encryptOnlineAppointment(Encryptor.getInstance().decrypt(pass));
			String salt = AES.getInstance().generateSalt();

			String encodedSalt = Base64.getEncoder().encodeToString(salt.getBytes("UTF-8"));
			
			String sql = "select user_id , no_mrn, no_ktp, username, fullname as name, password, no_bpjs from tb_user u where LOWER(u.username) = ? and u.password = ? "
					+ " order by u.user_id ";

			st = connection.prepareStatement(sql);
			st.setEscapeProcessing(true);
			st.setQueryTimeout(60000);
			st.setString(1, user.toLowerCase());
			st.setString(2, reEncryptAntrian);
			rs = st.executeQuery();
			
			if (rs.next()) {
				HashMap<String, Object> additionalClaims = new HashMap<String, Object>();
				/*
						String ret = BpjsService2.GetBpjsInfobyNoka(rs.getString("no_bpjs"));
				        Gson gson = new Gson();
				        BpjsInfoResponse memberInfo = gson.fromJson(ret, BpjsInfoResponse.class); 
				        if(memberInfo.getResponse() != null){
				        String provumumkd = memberInfo.getResponse().getPeserta().getProvUmum().getKdProvider();
				        String provumumnm = memberInfo.getResponse().getPeserta().getProvUmum().getNmProvider();
				        String tglahir = memberInfo.getResponse().getPeserta().getTglLahir();
				    	additionalClaims.put("tgllahir", tglahir);
						additionalClaims.put("prvumumkd", provumumkd);
						additionalClaims.put("prvumumnm", provumumnm);}
				*/
				String uid2 = rs.getString("user_id");
			
				additionalClaims.put("usermstr_id", rs.getString("user_id"));
				additionalClaims.put("username", rs.getString("username"));
				additionalClaims.put("name", rs.getString("name"));
				additionalClaims.put("logintype", "patient");
				additionalClaims.put("mrn", rs.getString("no_mrn"));
				additionalClaims.put("salt", encodedSalt);
				additionalClaims.put("nobpjs", rs.getString("no_bpjs"));
			
				additionalClaims.put("ktp", rs.getString("no_ktp"));
				FirebaseApp instancePatient = FirebaseApp.getInstance("patient");
				
				FirebaseAuth.getInstance(instancePatient).createCustomToken(uid2, additionalClaims)
						.addOnSuccessListener(new OnSuccessListener<String>() {
							@Override
							public void onSuccess(String customToken) {
								str.setResponse(customToken);
								AuthMobileFunctions.insertAuthorisation(uid2, customToken, encodedSalt);
								System.out.println(customToken);
							}
						});				

			} else {
				complete = true;
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			complete = true;
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}

			int a = 0;

			while (!complete) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (a > 60)
					break;
				else {
					a += 10;
				}
			}
			return str;
		}
	}

}