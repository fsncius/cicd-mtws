package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.util.TextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.ServerPrintView;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.Encryptor;
import com.securecontext.secure.EncryptorConnection;

public class ServerPrintSer {
	private final static String BASE_FTP_URL = getBaseConfig(IParameterConstant.SERVER_PRINT_FTP_BASE);
	private final static String BASE_FTP_USER = EncryptorConnection.getInstance().decrypt(getBaseConfig(IParameterConstant.SERVER_PRINT_FTP_BASE_USER));
//	private final static String BASE_FTP_USER = "";
	private final static String BASE_FTP_PASSWORD = EncryptorConnection.getInstance().decrypt(getBaseConfig(IParameterConstant.SERVER_PRINT_FTP_BASE_PASSWORD));
//	private final static String BASE_FTP_PASSWORD = "";
	private final static Integer BASE_FTP_PORT = Integer.valueOf(getBaseConfig(IParameterConstant.SERVER_PRINT_FTP_BASE_PORT));
//	private final static Integer BASE_FTP_PORT = 21;
	
	private static String getBaseConfig(String parameter){
		CommonService cs = new CommonService();
		return cs.getParameterValue(parameter);
	}
	
	public static ResponseStatus getPrintList(String listString) {
		ResponseStatus result = newResponseStatus();
		if(TextUtils.isEmpty(listString))return fail("Report code kosong.");
		ArrayList<ServerPrintView> datas = new ArrayList();
		try{
			listString = URLDecoder.decode(listString, "UTF-8");
			ObjectMapper mapper = new ObjectMapper();
			List<String> reportCodes = mapper.readValue(listString, new TypeReference<List<String>>(){});
			String whereIn = buildWhereIn(reportCodes, "REPORT_CODE", true);
			
			String query = "SELECT " +
					"	SERVERPRINT_ID," +
					"	REPORT_CODE," +
					"	FTP_FULL_PATH," +
					"	PRINT_COUNT," +
					"	CREATED_BY," +
					"	CREATED_DATETIME," +
					"	LAST_UPDATED_BY," +
					"	LAST_UPDATED_DATETIME," +
					"	DEFUNCT_IND" +
					"	FROM SERVERPRINT WHERE PRINT_COUNT =0 AND DEFUNCT_IND = 0 "+whereIn+" ORDER BY CREATED_DATETIME ASC";

			Object[] params = new Object[]{};
			List rawData = DbConnection.executeReader(DbConnection.KTHIS, query, params);
			if(rawData == null)return fail("Data kosong");
			if(rawData.size() <=0)return fail("Data kosong");
			for(Object dataRow : rawData){
				try{
					HashMap row = (HashMap) dataRow;
					ServerPrintView target = getMappedClass(row, ServerPrintView.class);
					try{
						target.setPdfFile(getFTPFile(target.getFtpFullPath()));
					}catch(Exception ex){
						ex.fillInStackTrace();
						System.out.println("Get FTP file("+target.getFtpFullPath()+") : " + ex.getMessage());
						target.setPdfFile(null);
					}
					datas.add(target);					
				}catch(Exception ex){
					ex.fillInStackTrace();
					System.out.println(ex.getMessage());
					continue;
				}				
			}
			result = success("Berhasil mendapatkan data.", datas);
		}catch(Exception ex){
			ex.fillInStackTrace();
			System.out.println(ex.getMessage());
		}
		return result;
	}
	
	private static byte[] getFTPFile(String filePath) throws Exception{
		byte[] result = null;
		FTPClient ftpClient = new FTPClient();
        try {
 
            ftpClient.connect(BASE_FTP_URL, BASE_FTP_PORT);
            ftpClient.login(BASE_FTP_USER, BASE_FTP_PASSWORD);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
 
            // APPROACH #1: using retrieveFile(String, OutputStream)
            InputStream inputStream = ftpClient.retrieveFileStream(filePath);
            byte[] bytes = IOUtils.toByteArray(inputStream);
//            result = ArrayUtils.toObject(bytes); // in Bytes
            result = bytes; // in bytes
            inputStream.close();
 
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
		return result;
	}
	
	private static String buildWhereIn(List<String> reportCodes, String columnName, boolean andCondition) {
		String result = "";
		if(reportCodes==null)return result;
		if(reportCodes.size() <=0)return result;
		if(andCondition) result = result.concat("AND ");
		result = result.concat(columnName + " IN ");
		int index =0;
		for(String target : reportCodes){
			if(index==0) result = result.concat(" (" + target);
			else 
			{
				if(index == reportCodes.size() - 1){
					result = result.concat(", "+ target + ")");
				}else{
					result = result.concat(", "+target);
				}
			}
			if(reportCodes.size() == 1)result = result.concat(" )");
			index++;
		}
		return result;
	}

	private static <T> T getMappedClass(HashMap rowInfo, Class<T> clazz) throws JsonProcessingException, IOException{
		T result = null;
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(rowInfo);
		result = mapClass(mapper, json, clazz);
		return result;
	}
	
	private static <T> T mapClass(ObjectMapper mapper, String json, Class<T> clazz) throws JsonProcessingException, IOException
	{
		T result = null;
		if(mapper == null)return result;
		result = mapper.readValue(json, clazz);
		return result;
	}
	
	private static ResponseStatus newResponseStatus(){
		ResponseStatus result = new ResponseStatus();
		result.setMessage("Inisialisasi data.");
		result.setSuccess(false);
		return result;
	}
	
	private static ResponseStatus fail(String message){
		ResponseStatus result = new ResponseStatus();
		result.setMessage(message);
		result.setSuccess(false);
		return result;
	}
	
	private static ResponseStatus fail(String message, Object data){
		ResponseStatus result = new ResponseStatus();
		result.setMessage(message);
		result.setSuccess(false);
		result.setData(data);
		return result;
	}
	
	private static ResponseStatus success(String message){
		ResponseStatus result = new ResponseStatus();
		result.setMessage(message);
		result.setSuccess(true);
		return result;
	}
	
	private static ResponseStatus success(String message, Object data){
		ResponseStatus result = new ResponseStatus();
		result.setMessage(message);
		result.setSuccess(true);
		result.setData(data);
		return result;
	}

	public static ResponseStatus addPrintCount(Long serverPrintId) {
		ResponseStatus result = newResponseStatus();
		ArrayList<ServerPrintView> datas = new ArrayList();
		try{
			String query = "UPDATE SERVERPRINT SET PRINT_COUNT = PRINT_COUNT +1 WHERE SERVERPRINT_ID = ?";

			Object[] params = new Object[]{serverPrintId};
			int success = DbConnection.executeQuery(DbConnection.KTHIS, query, params);
			if(success == 1)result = success("Berhasil mendapatkan data.", datas);
			else result = fail("Tidak dapat update data.");
		}catch(Exception ex){
			ex.fillInStackTrace();
			System.out.println(ex.getMessage());
		}
		return result;
	}
	

}
