package com.rsmurniteguh.webservice.dep.kthis.services;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.rsmurniteguh.webservice.dep.all.model.PurchaseStatus;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.ResumeMedis;
import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.ISysConstant;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.FileUploadUtil;
import com.sun.org.apache.bcel.internal.generic.ICONST;

public class PurchaseService extends DbConnection {
	
	public static List<PurchaseStatus> AddPurchase(String purchaseNo)
	{
		List<PurchaseStatus> view = new ArrayList<PurchaseStatus>();
		PurchaseStatus purchaseStatus = new PurchaseStatus();
		Connection pooledConnection = null;
		Connection purchaseConnection = null;
		
		PreparedStatement pstmtStockPurchasePlan = null;
		PreparedStatement pstmtPrice = null;
		PreparedStatement pstmtInsertPoMast = null;
		PreparedStatement pstmtPuchasePlanDetail = null;
		PreparedStatement pstmtInsertPoDetail = null;
		PreparedStatement pstmtUpdateStatus = null;
		
		ResultSet rsStockPurchasePlan = null;
		ResultSet rsPrice = null;
		ResultSet rsPurchasePlanDetail = null;
		
		try {
			pooledConnection = DbConnection.getPooledConnection();
			purchaseConnection = DbConnection.getPurchaseConnection();
			if (pooledConnection == null || purchaseConnection == null) throw new Exception("Database failed to connect !");
			purchaseConnection.setAutoCommit(false);
			
			String selectStockPurchasePlanView = "SELECT SPP.STOCKPURCHASEPLAN_ID, SPP.CREATED_DATETIME, SPP.LAST_UPDATED_DATETIME, SPP.REMARKS, SPP.PURCHASE_STATUS, "
					+ "VM.GL_ACCOUNT, VM.VENDOR_NAME, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SPP.CREATED_BY) CREATED_BY, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SPP.LAST_UPDATED_BY) LAST_UPDATED_BY "
					+ "FROM STOCKPURCHASEPLAN SPP "
					+ "INNER JOIN VENDORMSTR VM ON SPP.VENDORMSTR_ID = VM.VENDORMSTR_ID "
					+ "WHERE SPP.PURCHASE_PLAN_NO = '" + purchaseNo + "' AND SPP.PURCHASE_STATUS IN ('SPS3', 'SPS4', 'SPS6', 'SPS7')";
			pstmtStockPurchasePlan = pooledConnection.prepareStatement(selectStockPurchasePlanView);
			rsStockPurchasePlan = pstmtStockPurchasePlan.executeQuery();
			
			if (!rsStockPurchasePlan.next()) throw new Exception("Purchase Plan No '" + purchaseNo + "' does not valid ! ");
			BigDecimal stockPurchasePlanId = rsStockPurchasePlan.getBigDecimal("STOCKPURCHASEPLAN_ID");
			Timestamp createdDateTime = rsStockPurchasePlan.getTimestamp("CREATED_DATETIME");
			Timestamp lastUpdatedDateTime = rsStockPurchasePlan.getTimestamp("LAST_UPDATED_DATETIME");
			String remarks = rsStockPurchasePlan.getString("REMARKS");
			String glAccount = rsStockPurchasePlan.getString("GL_ACCOUNT");
			String vendorName = rsStockPurchasePlan.getString("VENDOR_NAME");
			String createdBy = rsStockPurchasePlan.getString("CREATED_BY");
			String lastUpdatedBy = rsStockPurchasePlan.getString("LAST_UPDATED_BY");
			String periode = Integer.toString(createdDateTime.getYear() + 1900) + "123456789ABC".charAt(createdDateTime.getMonth());
			if (remarks == null) remarks = "";
			Map<String, String> supplier = getSupplier(purchaseConnection, glAccount, vendorName);
			
			String selectPrice = "SELECT SUM(UNIT_PRICE * PLAN_QTY * (100 - COALESCE(DISCOUNT_RATE, 0)) / 100) TOTAL_PRICE, "
					+ "COALESCE(PPN_IND, 0) PPN_IND "
					+ "FROM PURCHASEPLANDETAIL WHERE STOCKPURCHASEPLAN_ID = '" + stockPurchasePlanId + "' AND PLAN_DETAIL_STATUS = 'PDS2'"
					+ "GROUP BY COALESCE(PPN_IND, 0)";
			pstmtPrice = pooledConnection.prepareStatement(selectPrice);
			rsPrice = pstmtPrice.executeQuery();
			
			rsPrice.next();
			BigDecimal totalPrice = rsPrice.getBigDecimal("TOTAL_PRICE").setScale(2, RoundingMode.HALF_UP);
			BigDecimal ppnPercent = rsPrice.getBigDecimal("PPN_IND").setScale(2, RoundingMode.HALF_UP);
			BigDecimal ppn = totalPrice.multiply(ppnPercent).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
			BigDecimal netto = totalPrice.add(ppn).setScale(2, RoundingMode.HALF_UP);
			if (rsPrice.next()) throw new Exception("Cannot posting multiple distinct ppn type !");
			
			String insertPoMast = "insert into dbo.Tr_POMast "
					+ "(NoBukti, TglTransaksi, KodeSupp, NamaSupp, KodeLokasi, NamaLokasi, Terms, TglJthTempo, NoRef, "
					+ "TglRef, KodeMataUang, Rate, Keterangan, TotalDetail, DiscPersen, DiscRp, PPNPersen, PPNRp, "
					+ "Netto, Periode, Jenis, Posting, Receive, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime)"
					+ "values "
					+ "(?, ?, ?, ?, 'FARMASI', 'INSTALASI FARMASI', 0, ?, '', "
					+ "?, 'IDR', 1, ?, ?, 0.00, 0.00, ?, ?, "
					+ "?, ?, 'Stock', 0, 0, ?, ?, ?, ?)";
			pstmtInsertPoMast = purchaseConnection.prepareStatement(insertPoMast);
			
			pstmtInsertPoMast.setString(1, purchaseNo);
			pstmtInsertPoMast.setTimestamp(2, createdDateTime);
			pstmtInsertPoMast.setString(3, supplier.get("kodeSupp"));
			pstmtInsertPoMast.setString(4, supplier.get("namaSupp"));
			pstmtInsertPoMast.setTimestamp(5, createdDateTime);
			pstmtInsertPoMast.setTimestamp(6, createdDateTime);
			pstmtInsertPoMast.setString(7, remarks);
			pstmtInsertPoMast.setBigDecimal(8, totalPrice);
			pstmtInsertPoMast.setBigDecimal(9, ppnPercent);
			pstmtInsertPoMast.setBigDecimal(10, ppn);
			pstmtInsertPoMast.setBigDecimal(11, netto);
			pstmtInsertPoMast.setString(12, periode);
			pstmtInsertPoMast.setString(13, createdBy);
			pstmtInsertPoMast.setTimestamp(14, createdDateTime);
			pstmtInsertPoMast.setString(15, lastUpdatedBy);
			pstmtInsertPoMast.setTimestamp(16, lastUpdatedDateTime);
			
			pstmtInsertPoMast.executeUpdate();
			pstmtInsertPoMast.close();
			
			String selectPurchasePlanDetail = "SELECT PPD.PLAN_QTY, PPD.UNIT_PRICE, PPD.RECEIVED_QTY, PPD.REMARKS, PPD.CREATED_DATETIME, PPD.LAST_UPDATED_DATETIME, PPD.COMPLETE_IND, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = PPD.CREATED_BY) CREATED_BY, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = PPD.LAST_UPDATED_BY) LAST_UPDATED_BY, "
					+ "PGCOMMON.FXGETCODEDESC(PPD.PLAN_UOM) SATUAN, MIM.MATERIAL_ITEM_CODE, COALESCE(PPD.DISCOUNT_RATE, 0) DISCOUNT_RATE "
					+ "FROM PURCHASEPLANDETAIL PPD "
					+ "INNER JOIN MATERIALITEMMSTR MIM ON MIM.MATERIALITEMMSTR_ID = PPD.MATERIALITEMMSTR_ID "
					+ "WHERE PPD.STOCKPURCHASEPLAN_ID = '" + stockPurchasePlanId + "' AND PPD.PLAN_DETAIL_STATUS = 'PDS2'";
			pstmtPuchasePlanDetail = pooledConnection.prepareStatement(selectPurchasePlanDetail);
			rsPurchasePlanDetail = pstmtPuchasePlanDetail.executeQuery();
			
			BigDecimal totalDetail = BigDecimal.ZERO;
			
			int sequence = 1;
			while (rsPurchasePlanDetail.next()) {
				String kodeStock = rsPurchasePlanDetail.getString("MATERIAL_ITEM_CODE");
				BigDecimal planQty = rsPurchasePlanDetail.getBigDecimal("PLAN_QTY");
				BigDecimal unitPrice = rsPurchasePlanDetail.getBigDecimal("UNIT_PRICE").setScale(2, RoundingMode.HALF_UP);
				BigDecimal discountRate = rsPurchasePlanDetail.getBigDecimal("DISCOUNT_RATE");
				String detailRemarks = rsPurchasePlanDetail.getString("REMARKS");
				String detailCreatedBy = rsPurchasePlanDetail.getString("CREATED_BY");
				String detailLastUpdatedBy = rsPurchasePlanDetail.getString("LAST_UPDATED_BY");
				Timestamp detailCreatedDateTime = rsStockPurchasePlan.getTimestamp("CREATED_DATETIME");
				Timestamp detailLastUpdatedDateTime = rsStockPurchasePlan.getTimestamp("LAST_UPDATED_DATETIME");
				String satuan = rsPurchasePlanDetail.getString("SATUAN");
				BigDecimal detailTotalPrice = planQty.multiply(unitPrice).setScale(2, RoundingMode.HALF_UP);
				BigDecimal discount = detailTotalPrice.multiply(discountRate).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
				BigDecimal detailNetto = detailTotalPrice.subtract(discount).setScale(2, RoundingMode.HALF_UP);
				if (detailRemarks == null) detailRemarks = "";
				String namaStock = getStockName(purchaseConnection, kodeStock);
				
				totalDetail = totalDetail.add(detailNetto);
				
				String insertPoDetail = "insert into dbo.Tr_PODetail "
						+ "(NoBukti, NomorUrut, KodeStock, NamaStock, KodeSupp, NamaSupp, KodeLokasi, NamaLokasi, "
						+ "Qty, Satuan, HargaUnit, TotalHarga, DiscPersen, DiscRp, Netto, Keterangan, "
						+ "Periode, JlhTerima, Receive, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime) "
						+ "values "
						+ "('" + purchaseNo + "', '" + sequence + "', ?, ?, ?, ?, 'FARMASI', 'INSTALASI FARMASI', " 
						+ planQty + ", ?, " + unitPrice + ", " + detailTotalPrice + ", " + discountRate + ", " + discount + ", " + detailNetto + ", ?, '" 
						+ periode + "', 0, 0, ?, '" + detailCreatedDateTime + "', ?, '" + detailLastUpdatedDateTime + "')";
				pstmtInsertPoDetail = purchaseConnection.prepareStatement(insertPoDetail);
				
				pstmtInsertPoDetail.setString(1, kodeStock);
				pstmtInsertPoDetail.setString(2, namaStock);
				pstmtInsertPoDetail.setString(3, supplier.get("kodeSupp"));
				pstmtInsertPoDetail.setString(4, supplier.get("namaSupp"));
				pstmtInsertPoDetail.setString(5, satuan);
				pstmtInsertPoDetail.setString(6, detailRemarks);
				pstmtInsertPoDetail.setString(7, detailCreatedBy);
				pstmtInsertPoDetail.setString(8, detailLastUpdatedBy);
				
				pstmtInsertPoDetail.executeUpdate();
				
				sequence++;
			}
			
			if (totalDetail.compareTo(totalPrice) != 0) {
				ppn = totalDetail.multiply(ppnPercent).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
				netto = totalDetail.add(ppn);
				String updatePoMast = "update dbo.Tr_POMast set TotalDetail = '" + totalDetail + "', PPNRp = '" + ppn + "', Netto = '" + netto + "' where NoBukti = '" + purchaseNo + "'";
				pstmtInsertPoMast = purchaseConnection.prepareStatement(updatePoMast);
				pstmtInsertPoMast.executeUpdate();
			}
			
			purchaseStatus.setStatus1("OK");
			purchaseStatus.setStatus2("OK");
			purchaseStatus.setStatus3("OK");
			view.add(purchaseStatus);
			
			purchaseConnection.commit();
			
//			String updateStatus = "UPDATE STOCKPURCHASEPLAN SET POSTING_IND = 'Y' WHERE PURCHASE_PLAN_NO = '" + purchaseNo + "'";
//			pstmtUpdateStatus = pooledConnection.prepareStatement(updateStatus);
//			pstmtUpdateStatus.executeUpdate();
			updatePostIndPO(purchaseNo, IConstant.IND_YES);
		}
		catch (SQLException e){
			PurchaseStatus status = new PurchaseStatus();
		    if (e.getErrorCode() == 2627) { 
		    	status.setStatus1("Posting failed : Purchase Plan has been posting before !"); 
		    	updatePostIndPO(purchaseNo, IConstant.IND_YES);
		    }
		    else status.setStatus1("Posting failed : " + e.getMessage());
		    view.add(status);
		    if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			PurchaseStatus status = new PurchaseStatus();
			status.setStatus1("Posting failed : " + e.getMessage());
			view.add(status);
			if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			if (purchaseConnection != null) try { purchaseConnection.setAutoCommit(true); purchaseConnection.close(); } catch (SQLException e) {}
			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
			if (pstmtStockPurchasePlan != null) try { pstmtStockPurchasePlan.close(); } catch (SQLException e) {}
			if (pstmtPrice != null) try { pstmtPrice.close(); } catch (SQLException e) {}
			if (pstmtInsertPoMast != null) try { pstmtInsertPoMast.close(); } catch (SQLException e) {}
			if (pstmtPuchasePlanDetail != null) try { pstmtPuchasePlanDetail.close(); } catch (SQLException e) {}
			if (pstmtInsertPoDetail != null) try { pstmtInsertPoDetail.close(); } catch (SQLException e) {}
			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
			if (rsStockPurchasePlan != null) try { rsStockPurchasePlan.close(); } catch (SQLException e) {}
			if (rsPrice != null) try { rsPrice.close(); } catch (SQLException e) {}
			if (rsPurchasePlanDetail != null) try { rsPurchasePlanDetail.close(); } catch (SQLException e) {}
		}
		return view;
	}
	
	public static List<PurchaseStatus> AddReceive(String receiptNo)
	{
		List<PurchaseStatus> view = new ArrayList<PurchaseStatus>();
		Connection pooledConnection = null;
		Connection purchaseConnection = null;
		
		PreparedStatement pstmtStockReceipt = null;
		PreparedStatement pstmtPrice = null;
		PreparedStatement pstmtInsertStockReceipt = null;
		PreparedStatement pstmtStockReceiptDetail = null;
		PreparedStatement pstmtInsertStockReceiptDetail = null;
		PreparedStatement pstmtInsertJurnal = null;
		PreparedStatement pstmtInsertJurnalDetail = null;
		PreparedStatement pstmtJurnalPrice = null;
		PreparedStatement pstmtUpdateStatus = null;
		
		ResultSet rsStockReceipt = null;
		ResultSet rsPrice = null;
		ResultSet rsStockReceiptDetail = null;
		ResultSet rsJurnalPrice = null;
		
		try {
			pooledConnection = DbConnection.getPooledConnection();
			purchaseConnection = DbConnection.getPurchaseConnection();
			if (pooledConnection == null || purchaseConnection == null) throw new Exception("Database failed to connect !");
			purchaseConnection.setAutoCommit(false);
			
			String selectStockReceipt = "SELECT SR.STOCKRECEIPT_ID, SR.RECEIPT_DATETIME, SR.PURCHASE_PLAN_NO, SR.REMARKS, SR.CREATED_DATETIME, SR.LAST_UPDATED_DATETIME, "
					+ "VM.GL_ACCOUNT, VM.VENDOR_NAME, SPP.CREATED_DATETIME PURCHASE_DATETIME, SPP.PURCHASE_STATUS, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SR.CREATED_BY) CREATED_BY, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SR.LAST_UPDATED_BY) LAST_UPDATED_BY "
					+ "FROM STOCKRECEIPT SR "
					+ "INNER JOIN VENDORMSTR VM ON SR.VENDORMSTR_ID = VM.VENDORMSTR_ID "
					+ "INNER JOIN STOCKPURCHASEPLAN SPP ON SPP.PURCHASE_PLAN_NO = SR.PURCHASE_PLAN_NO "
					+ "WHERE SR.RECEIPT_NO = '" + receiptNo + "' AND SR.RECEIPT_STATUS = 'SRSCSR' AND SR.RECEIPT_TYPE = 'SRT1'";
			pstmtStockReceipt = pooledConnection.prepareStatement(selectStockReceipt);
			rsStockReceipt = pstmtStockReceipt.executeQuery();
			
			if (!rsStockReceipt.next()) throw new Exception("Stock Receipt No '" + receiptNo + "' does not valid !");
			BigDecimal stockReceiptId = rsStockReceipt.getBigDecimal("STOCKRECEIPT_ID");
			Timestamp receiptDateTime = rsStockReceipt.getTimestamp("RECEIPT_DATETIME");
			Timestamp createdDateTime = rsStockReceipt.getTimestamp("CREATED_DATETIME");
			Timestamp lastUpdatedDateTime = rsStockReceipt.getTimestamp("LAST_UPDATED_DATETIME");
			Timestamp purchaseDateTime = rsStockReceipt.getTimestamp("PURCHASE_DATETIME");
			String purchasePlanNo = rsStockReceipt.getString("PURCHASE_PLAN_NO");
			String remarks = rsStockReceipt.getString("REMARKS");
			String createdBy = rsStockReceipt.getString("CREATED_BY");
			String lastUpdateBy = rsStockReceipt.getString("LAST_UPDATED_BY");
			String glAccount = rsStockReceipt.getString("GL_ACCOUNT");
			String vendorName = rsStockReceipt.getString("VENDOR_NAME");
			String periode = Integer.toString(receiptDateTime.getYear() + 1900) + "123456789ABC".charAt(receiptDateTime.getMonth());
			if (remarks == null) remarks = "";
			Map<String, String> supplier = getSupplier(purchaseConnection, glAccount, vendorName);
			
			String selectPrice = "SELECT SUM(UNIT_PRICE * RECEIPT_QTY / (100 + COALESCE(PPN_RATE, 0)) * 100) TOTAL_PRICE, "
					+ "COALESCE(PPN_RATE, 0) PPN_IND "
					+ "FROM STOCKRECEIPTDETAIL WHERE STOCKRECEIPT_ID = '" + stockReceiptId + "' AND RECEIPT_DETAIL_STATUS = 'SRDSCRI' "
					+ "GROUP BY COALESCE(PPN_RATE, 0)";
			pstmtPrice = pooledConnection.prepareStatement(selectPrice);
			rsPrice = pstmtPrice.executeQuery();
			
			rsPrice.next();
			BigDecimal totalPrice = rsPrice.getBigDecimal("TOTAL_PRICE").setScale(2, RoundingMode.HALF_UP);
			BigDecimal ppnPercent = rsPrice.getBigDecimal("PPN_IND").setScale(2, RoundingMode.HALF_UP);
			BigDecimal ppn = totalPrice.multiply(ppnPercent).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
			BigDecimal netto = totalPrice.add(ppn).setScale(2, RoundingMode.HALF_UP);
			if (rsPrice.next()) throw new Exception("Cannot posting multiple distinct ppn type !");
			
			String insertStockReceipt = "insert into dbo.Tr_ReceivingGoodsMast "
					+ "(NoBukti, TglTransaksi, TglTerima, Supplier, KodeSupp, "
					+ "NoPO, TglPO, "
					+ "NoDO, TglDO, Keterangan, Jenis, Receive, Periode, CreatedBy, CreatedTime, ModifiedBy, "
					+ "ModifiedTime, Pilih, TotalDetail, Discount, PPN, Netto, GrandTotal, BayarDimuka) "
					+ "values "
					+ "('" + receiptNo + "', '" + receiptDateTime + "', '" + receiptDateTime + "', ?, ?, "
					+ ((purchasePlanNo == null) ? "NULL, NULL" : "'" + purchasePlanNo + "', '" + purchaseDateTime + "'")
					+ ", '', '" + receiptDateTime + "', ?, 'Stock', 0, '" + periode + "', ?, '" + createdDateTime + "', ?, '"
					+ lastUpdatedDateTime + "', 0, " + totalPrice + ", 0.00, " + ppn + ", " + netto + ", " + netto + ", 0.00)";
			pstmtInsertStockReceipt = purchaseConnection.prepareStatement(insertStockReceipt);
			
			pstmtInsertStockReceipt.setString(1, supplier.get("namaSupp"));
			pstmtInsertStockReceipt.setString(2, supplier.get("kodeSupp"));
			pstmtInsertStockReceipt.setString(3, remarks);
			pstmtInsertStockReceipt.setString(4, createdBy);
			pstmtInsertStockReceipt.setString(5, lastUpdateBy);
			
			pstmtInsertStockReceipt.executeUpdate();
			pstmtInsertStockReceipt.close();
			
			String selectStockReceiptDetail = "SELECT SRD.RECEIPT_QTY, SRD.REMARKS, (SRD.UNIT_PRICE / (100 + COALESCE(PPN_RATE, 0)) * 100) UNIT_PRICE, SRD.LAST_UPDATED_DATETIME, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SRD.LAST_UPDATED_BY) LAST_UPDATED_BY, "
					+ "PGCOMMON.FXGETCODEDESC(SRD.RECEIPT_UOM) SATUAN, MIM.MATERIAL_ITEM_CODE, PPD.COMPLETE_IND "
					+ "FROM STOCKRECEIPTDETAIL SRD "
					+ "INNER JOIN MATERIALITEMMSTR MIM ON MIM.MATERIALITEMMSTR_ID = SRD.MATERIALITEMMSTR_ID "
					+ "LEFT OUTER JOIN PURCHASEPLANDETAIL PPD ON PPD.PURCHASEPLANDETAIL_ID = SRD.PURCHASEPLANDETAIL_ID "
					+ "WHERE SRD.STOCKRECEIPT_ID = '" + stockReceiptId + "' AND SRD.RECEIPT_DETAIL_STATUS = 'SRDSCRI'";
			pstmtStockReceiptDetail = pooledConnection.prepareStatement(selectStockReceiptDetail);
			rsStockReceiptDetail = pstmtStockReceiptDetail.executeQuery();
			
			BigDecimal totalDetail = BigDecimal.ZERO;
			int sequence = 1;
			while (rsStockReceiptDetail.next()) {
				String kodeStock = rsStockReceiptDetail.getString("MATERIAL_ITEM_CODE");
				BigDecimal receiptQty = rsStockReceiptDetail.getBigDecimal("RECEIPT_QTY");
				String detailRemarks = rsStockReceiptDetail.getString("REMARKS");
				BigDecimal unitPrice = rsStockReceiptDetail.getBigDecimal("UNIT_PRICE").setScale(2, RoundingMode.HALF_UP);
				Timestamp detailLastUpdatedDateTime = rsStockReceiptDetail.getTimestamp("LAST_UPDATED_DATETIME");
				String detailLastUpdatedBy = rsStockReceiptDetail.getString("LAST_UPDATED_BY");
				String satuan = rsStockReceiptDetail.getString("SATUAN");
				if (detailRemarks == null) detailRemarks = "";
				String namaStock = getStockName(purchaseConnection, kodeStock);
				
				String insertStockReceiptDetail = "insert into dbo.Tr_ReceivingGoods "
						+ "(NoBukti, NomorUrut, KodeStock, NamaStock, Qty, Satuan, Remark, "
						+ "Periode, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, QtyTerima, "
						+ "NoPo, ContUnit, HargaUnitNet, QtyRetur) "
						+ "values "
						+ "('" + receiptNo + "', '" + sequence + "', ?, ?, '" + receiptQty + "', ?, ?, '" 
						+ periode + "', ?, '" + createdDateTime + "', ?, '" + detailLastUpdatedDateTime + "', 0, " 
						+ (purchasePlanNo == null ? "NULL" : "'" + purchasePlanNo + "'") + ", 0, '" + unitPrice + "', 0)";
				pstmtInsertStockReceiptDetail = purchaseConnection.prepareStatement(insertStockReceiptDetail);
				
				pstmtInsertStockReceiptDetail.setString(1, kodeStock);
				pstmtInsertStockReceiptDetail.setString(2, namaStock);
				pstmtInsertStockReceiptDetail.setString(3, satuan);
				pstmtInsertStockReceiptDetail.setString(4, detailRemarks);
				pstmtInsertStockReceiptDetail.setString(5, createdBy);
				pstmtInsertStockReceiptDetail.setString(6, detailLastUpdatedBy);
				
				pstmtInsertStockReceiptDetail.executeUpdate();
				
				totalDetail = totalDetail.add(receiptQty.multiply(unitPrice)).setScale(2, RoundingMode.HALF_UP);
				
				sequence++;
			}
			
			if (totalDetail.compareTo(totalPrice) != 0) {
				ppn = totalDetail.multiply(ppnPercent).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
				netto = totalDetail.add(ppn);
				String updateStockReceiptMast = "update dbo.Tr_ReceivingGoodsMast set TotalDetail = '" + totalDetail + "', PPN = '" + ppn + "', Netto = '" + netto + "', GrandTotal = '" + netto + "' where NoBukti = '" + receiptNo + "'";
				pstmtInsertStockReceipt = purchaseConnection.prepareStatement(updateStockReceiptMast);
				pstmtInsertStockReceipt.executeUpdate();
			}
			
			String insertJurnal = "insert into dbo.GL_JurnalMast "
					+ "(NoBukti, Debet, Kredit, TglTrans, Periode, Posted, ProsesAkhir, Tipe, "
					+ "Keterangan, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NoRef, TEMPNUMBER, "
					+ "NAMAREF, SANDIREF, Checked, ADMISSION_DATETIME, NOKAS, CheckedBy) "
					+ "values "
					+ "('" + receiptNo + "', '" + netto + "', '" + netto + "', '" + receiptDateTime + "', '" + periode + "', 0, 0, '2. Jurnal Pembelian', " 
					+ "'', ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, "
					+ "NULL, NULL, 0, NULL, NULL, NULL)";
			pstmtInsertJurnal = purchaseConnection.prepareStatement(insertJurnal);
			
			pstmtInsertJurnal.setString(1, lastUpdateBy);
			pstmtInsertJurnal.setString(2, lastUpdateBy);
			
			pstmtInsertJurnal.executeUpdate();
			pstmtInsertJurnal.close();
			
			String insertJurnalDetail = "insert into dbo.GL_JurnalDetail "
					+ "(NoBukti, NoUrut, NomorUrut, Sandi, Keterangan, Debet, Kredit, TglTrans, TglTransKTHIS, Posted, "
					+ "ProsesAkhir, Unit, Periode, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NAMAREF, TEMPNUMBER, NOKAS) "
					+ "values "
					+ "('" + receiptNo + "', ?, ?, ?, '', ?, ?, '" + receiptDateTime + "', '" + receiptDateTime + "', 0, "
					+ "0, 0.00, '" + periode + "', ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, NULL)";
			pstmtInsertJurnalDetail = purchaseConnection.prepareStatement(insertJurnalDetail);
			
			sequence = 1;
			pstmtInsertJurnalDetail.setInt(1, sequence);
			pstmtInsertJurnalDetail.setInt(2, sequence);
			pstmtInsertJurnalDetail.setString(3, glAccount);
			pstmtInsertJurnalDetail.setBigDecimal(4, BigDecimal.ZERO);
			pstmtInsertJurnalDetail.setBigDecimal(5, netto);
			pstmtInsertJurnalDetail.setString(6, lastUpdateBy);
			pstmtInsertJurnalDetail.setString(7, lastUpdateBy);
			
			pstmtInsertJurnalDetail.executeUpdate();
			
			String selectJurnalPrice = "select SUM(rg.Qty * rg.HargaUnitNet * (1 + rgm.PPN / rgm.TotalDetail)) debet, ms.SandiBeli "
					+ "from dbo.Tr_ReceivingGoods rg "
					+ "inner join dbo.Tr_ReceivingGoodsMast rgm on rgm.NoBukti = rg.NoBukti "
					+ "inner join dbo.Ms_Stock ms on ms.KodeStock = rg.KodeStock "
					+ "where rg.NoBukti = '" + receiptNo + "' group by ms.SandiBeli";
			pstmtJurnalPrice = purchaseConnection.prepareStatement(selectJurnalPrice);
			rsJurnalPrice = pstmtJurnalPrice.executeQuery();
			
			totalDetail = BigDecimal.ZERO;
			
			while(rsJurnalPrice.next()) {
				BigDecimal debet = rsJurnalPrice.getBigDecimal("debet").setScale(2, RoundingMode.HALF_UP);
				String sandiBeli = rsJurnalPrice.getString("SandiBeli");
				
				totalDetail = totalDetail.add(debet);
				
				sequence++;
				pstmtInsertJurnalDetail.setInt(1, sequence);
				pstmtInsertJurnalDetail.setInt(2, sequence);
				pstmtInsertJurnalDetail.setString(3, sandiBeli);
				pstmtInsertJurnalDetail.setBigDecimal(4, debet);
				pstmtInsertJurnalDetail.setBigDecimal(5, BigDecimal.ZERO);
				pstmtInsertJurnalDetail.setString(6, lastUpdateBy);
				pstmtInsertJurnalDetail.setString(7, lastUpdateBy);
				
				pstmtInsertJurnalDetail.executeUpdate();
			}
			
			if (totalDetail.compareTo(netto) != 0) {
				String updateJurnal = "update dbo.GL_JurnalMast set Debet = '" + totalDetail + "', Kredit = '" + totalDetail + "' where NoBukti = '" + receiptNo + "'";
				pstmtInsertJurnal = purchaseConnection.prepareStatement(updateJurnal);
				pstmtInsertJurnal.executeUpdate();
				pstmtInsertJurnal.close();
				
				String updateJurnalDetail = "update dbo.GL_JurnalDetail set Kredit = '" + totalDetail + "' where NoBukti = '" + receiptNo + "' and NoUrut = 1";
				pstmtInsertJurnal = purchaseConnection.prepareStatement(updateJurnalDetail);
				pstmtInsertJurnal.executeUpdate();
			}
			
			PurchaseStatus status = new PurchaseStatus();
			status.setStatus1("OK");
			status.setStatus2("OK");
			status.setStatus3("OK");
			view.add(status);
			
			purchaseConnection.commit();
			
//			String updateStatus = "UPDATE STOCKRECEIPT SET POSTING_IND = 'Y' WHERE RECEIPT_NO = '" + receiptNo + "'";
//			pstmtUpdateStatus = pooledConnection.prepareStatement(updateStatus);
//			pstmtUpdateStatus.executeUpdate();
			updatePostIndGR(receiptNo, IConstant.IND_YES);
		}
		catch (SQLException e){
			PurchaseStatus status = new PurchaseStatus();
		    if (e.getErrorCode() == 2627) { 
		    	status.setStatus1("Posting failed : Stock Receipt has been posting before !");
		    	updatePostIndGR(receiptNo, IConstant.IND_YES);
		    }
		    else status.setStatus1("Posting failed : " + e.getMessage());
		    view.add(status);
		    if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			PurchaseStatus status = new PurchaseStatus();
			status.setStatus1("Posting failed : " + e.getMessage());
			view.add(status);
			if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			if (purchaseConnection != null) try { purchaseConnection.setAutoCommit(true); purchaseConnection.close(); } catch (SQLException e) {}
 			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
			if (pstmtStockReceipt != null) try { pstmtStockReceipt.close(); } catch (SQLException e) {}
			if (pstmtPrice != null) try { pstmtPrice.close(); } catch (SQLException e) {}
			if (pstmtInsertStockReceipt != null) try { pstmtInsertStockReceipt.close(); } catch (SQLException e) {}
			if (pstmtStockReceiptDetail != null) try { pstmtStockReceiptDetail.close(); } catch (SQLException e) {}
			if (pstmtInsertStockReceiptDetail != null) try { pstmtInsertStockReceiptDetail.close(); } catch (SQLException e) {}
			if (pstmtInsertJurnal != null) try { pstmtInsertJurnal.close(); } catch (SQLException e) {}
			if (pstmtInsertJurnalDetail != null) try { pstmtInsertJurnalDetail.close(); } catch (SQLException e) {}
			if (pstmtJurnalPrice != null) try { pstmtJurnalPrice.close(); } catch (SQLException e) {}
			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
			if (rsStockReceipt != null) try { rsStockReceipt.close(); } catch (SQLException e) {}
			if (rsPrice != null) try { rsPrice.close(); } catch (SQLException e) {}
			if (rsStockReceiptDetail != null) try { rsStockReceiptDetail.close(); } catch (SQLException e) {}
			if (rsJurnalPrice != null) try { rsJurnalPrice.close(); } catch (SQLException e) {}
		}
		return view;
	}
	
	public static List<PurchaseStatus> AddReturn(String receiptNo)
	{
		List<PurchaseStatus> view = new ArrayList<PurchaseStatus>();
		Connection pooledConnection = null;
		Connection purchaseConnection = null;
		
		PreparedStatement pstmtStockReceiptReturn = null;
		PreparedStatement pstmtCheckStockReceiptReturn = null;
		PreparedStatement pstmtInsertStockReceiptReturn = null;
		PreparedStatement pstmtReturnId = null;
		PreparedStatement pstmtStockReceiptReturnDetail = null;
		PreparedStatement pstmtInsertStockReceiptReturnDetail = null;
		PreparedStatement pstmtInsertJurnal = null;
		PreparedStatement pstmtInsertJurnalDetail = null;
		PreparedStatement pstmtJurnalPrice = null;
		PreparedStatement pstmtUpdateStatus = null;
		
		ResultSet rsStockReceiptReturn = null;
		ResultSet rsCheckStockReceiptReturn = null;
		ResultSet rsReturnId = null;
		ResultSet rsStockReceiptReturnDetail = null;
		ResultSet rsJurnalPrice = null;
		
		try {
			pooledConnection = DbConnection.getPooledConnection();
			purchaseConnection = DbConnection.getPurchaseConnection();
			if (pooledConnection == null || purchaseConnection == null) throw new Exception("Database failed to connect !");
			purchaseConnection.setAutoCommit(false);
			
			String selectStockReceiptReturn = "SELECT SR.STOCKRECEIPT_ID, SR.RECEIPT_DATETIME, SR.STOCK_IN_CODE_NO, SR.REMARKS, SR.CREATED_DATETIME, SR.LAST_UPDATED_DATETIME, "
					+ "VM.GL_ACCOUNT, VM.VENDOR_NAME,"
					+ "(SELECT CREATED_DATETIME FROM STOCKRECEIPT WHERE RECEIPT_NO = SR.STOCK_IN_CODE_NO) STOCK_IN_DATETIME, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SR.CREATED_BY) CREATED_BY, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SR.LAST_UPDATED_BY) LAST_UPDATED_BY "
					+ "FROM STOCKRECEIPT SR "
					+ "INNER JOIN VENDORMSTR VM ON SR.VENDORMSTR_ID = VM.VENDORMSTR_ID "
					+ "WHERE SR.RECEIPT_NO = '" + receiptNo + "' AND SR.RECEIPT_STATUS = 'SRSCSR' AND SR.RECEIPT_TYPE = 'SRT4' AND SR.STOCK_IN_CODE_NO IS NOT NULL";
			pstmtStockReceiptReturn = pooledConnection.prepareStatement(selectStockReceiptReturn);
			rsStockReceiptReturn = pstmtStockReceiptReturn.executeQuery();
			
			if (!rsStockReceiptReturn.next()) throw new Exception("Stock Receipt Return No '" + receiptNo + "' does not valid !");
			BigDecimal stockReceiptReturnId = rsStockReceiptReturn.getBigDecimal("STOCKRECEIPT_ID");
			Timestamp receiptDateTime = rsStockReceiptReturn.getTimestamp("RECEIPT_DATETIME");
			Timestamp createdDateTime = rsStockReceiptReturn.getTimestamp("CREATED_DATETIME");
			Timestamp lastUpdatedDateTime = rsStockReceiptReturn.getTimestamp("LAST_UPDATED_DATETIME");
			Timestamp stockInDateTime = rsStockReceiptReturn.getTimestamp("STOCK_IN_DATETIME");
			String stockInCodeNo = rsStockReceiptReturn.getString("STOCK_IN_CODE_NO");
			String remarks = rsStockReceiptReturn.getString("REMARKS");
			String createdBy = rsStockReceiptReturn.getString("CREATED_BY");
			String lastUpdateBy = rsStockReceiptReturn.getString("LAST_UPDATED_BY");
			String glAccount = rsStockReceiptReturn.getString("GL_ACCOUNT");
			String vendorName = rsStockReceiptReturn.getString("VENDOR_NAME");
			String periode = Integer.toString(receiptDateTime.getYear() + 1900) + "123456789ABC".charAt(receiptDateTime.getMonth());
			String stockInPeriode = null;
			if (stockInCodeNo != null) stockInPeriode = Integer.toString(stockInDateTime.getYear() + 1900) + "123456789ABC".charAt(stockInDateTime.getMonth());
			if (remarks == null) remarks = "";
			Map<String, String> supplier = getSupplier(purchaseConnection, glAccount, vendorName);
			
			String checkStockReceiptReturn = "select * from dbo.Tr_ReturBeliMast where RETURBELIMAST_NO = '" + receiptNo + "'";
			pstmtCheckStockReceiptReturn = purchaseConnection.prepareStatement(checkStockReceiptReturn);
			rsCheckStockReceiptReturn = pstmtCheckStockReceiptReturn.executeQuery();
			if (rsCheckStockReceiptReturn.next()) throw new Exception("Stock Receipt Return has been posting before !");
			
			String insertStockReceiptReturn = "insert into dbo.Tr_ReturBeliMast "
					+ "(RETURBELIMAST_NO, TglTransaksi, NoSUJ, "
					+ "KodeSupp, NAMASUPP, Keterangan, Periode, CreatedBy, CreatedTime, ModifiedBy, "
					+ "ModifiedTime, PeriodeSUJ, NOMORJURNAL) "
					+ "values "
					+ "('" + receiptNo + "', '" + receiptDateTime + "', " + (stockInCodeNo == null ? "NULL, '" : "'" + stockInCodeNo + "', ")
					+ "?, ?, ?, '" + periode + "', ?, '" + createdDateTime + "', ?, '"
					+ lastUpdatedDateTime + "', " + (stockInPeriode == null ? "NULL" : "'" + stockInPeriode + "'") + ", '" + receiptNo + "')";
			pstmtInsertStockReceiptReturn = purchaseConnection.prepareStatement(insertStockReceiptReturn);
			
			pstmtInsertStockReceiptReturn.setString(1, supplier.get("kodeSupp"));
			pstmtInsertStockReceiptReturn.setString(2, supplier.get("namaSupp"));
			pstmtInsertStockReceiptReturn.setString(3, remarks);
			pstmtInsertStockReceiptReturn.setString(4, createdBy);
			pstmtInsertStockReceiptReturn.setString(5, lastUpdateBy);
			
			pstmtInsertStockReceiptReturn.executeUpdate();
			
			String selectStockReceiptReturnDetail = "SELECT SRD.RECEIPT_QTY, SRD.REMARKS, (SRD.UNIT_PRICE / (100 + COALESCE(SRD.PPN_RATE, 0)) * 100) UNIT_PRICE, COALESCE(SRD.PPN_RATE, 0) PPN_RATE, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SRD.LAST_UPDATED_BY) LAST_UPDATED_BY, SRD.LAST_UPDATED_DATETIME, "
					+ "PGCOMMON.FXGETCODEDESC(SRD.RECEIPT_UOM) SATUAN, MIM.MATERIAL_ITEM_CODE "
					+ "FROM STOCKRECEIPTDETAIL SRD "
					+ "INNER JOIN MATERIALITEMMSTR MIM ON MIM.MATERIALITEMMSTR_ID = SRD.MATERIALITEMMSTR_ID "
					+ "WHERE SRD.STOCKRECEIPT_ID = '" + stockReceiptReturnId + "' AND SRD.RECEIPT_DETAIL_STATUS = 'SRDSCRI'";
			pstmtStockReceiptReturnDetail = pooledConnection.prepareStatement(selectStockReceiptReturnDetail);
			rsStockReceiptReturnDetail = pstmtStockReceiptReturnDetail.executeQuery();

			BigDecimal netto = BigDecimal.ZERO;
			
			String selectReturnId = "select RETURBELIMAST_ID from dbo.Tr_ReturBeliMast where RETURBELIMAST_NO = '" + receiptNo + "'";
			pstmtReturnId = purchaseConnection.prepareStatement(selectReturnId);
			rsReturnId = pstmtReturnId.executeQuery();
			
			rsReturnId.next();
			String returnId = rsReturnId.getString("RETURBELIMAST_ID");
			
			while(rsStockReceiptReturnDetail.next()) {
				String kodeStock = rsStockReceiptReturnDetail.getString("MATERIAL_ITEM_CODE");
				BigDecimal receiptQty = rsStockReceiptReturnDetail.getBigDecimal("RECEIPT_QTY");
				String detailRemarks = rsStockReceiptReturnDetail.getString("REMARKS");
				BigDecimal unitPrice = rsStockReceiptReturnDetail.getBigDecimal("UNIT_PRICE").setScale(2, RoundingMode.HALF_UP);
				BigDecimal ppnRate = rsStockReceiptReturnDetail.getBigDecimal("PPN_RATE");
				Timestamp detailLastUpdatedDateTime = rsStockReceiptReturnDetail.getTimestamp("LAST_UPDATED_DATETIME");
				String detailLastUpdatedBy = rsStockReceiptReturnDetail.getString("LAST_UPDATED_BY");
				String satuan = rsStockReceiptReturnDetail.getString("SATUAN");
				if (detailRemarks == null) detailRemarks = "";
				getStockName(purchaseConnection, kodeStock);
				BigDecimal ppn = ppnRate.multiply(unitPrice).multiply(receiptQty).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
				
				String insertStockReceiptReturnDetail = "insert into dbo.Tr_ReturBeliDetail "
						+ "(RETURBELIMAST_ID, KodeStock, Qty, Satuan, HargaUnit, "
						+ "DiscPersen, DiscRp, PPNPErsen, PPNRp, Keterangan, Periode, "
						+ "CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, DelStatus) "
						+ "values "
						+ "('" + returnId + "', ?, '" + receiptQty + "', ?, '" + unitPrice 
						+ "', 0.00, 0.00, '" + ppnRate + "', '" + ppn + "', ?, '" + periode 
						+ "', ?, '" + createdDateTime + "', ?, '" + detailLastUpdatedDateTime + "', 0)";
				pstmtInsertStockReceiptReturnDetail = purchaseConnection.prepareStatement(insertStockReceiptReturnDetail);
				
				pstmtInsertStockReceiptReturnDetail.setString(1, kodeStock);
				pstmtInsertStockReceiptReturnDetail.setString(2, satuan);
				pstmtInsertStockReceiptReturnDetail.setString(3, detailRemarks);
				pstmtInsertStockReceiptReturnDetail.setString(4, createdBy);
				pstmtInsertStockReceiptReturnDetail.setString(5, detailLastUpdatedBy);
				
				pstmtInsertStockReceiptReturnDetail.executeUpdate();
				
				netto = netto.add(receiptQty.multiply(unitPrice).add(ppn)).setScale(2, RoundingMode.HALF_UP);
			}
			
			String insertJurnal = "insert into dbo.GL_JurnalMast "
					+ "(NoBukti, Debet, Kredit, TglTrans, Periode, Posted, ProsesAkhir, Tipe, "
					+ "Keterangan, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NoRef, TEMPNUMBER, "
					+ "NAMAREF, SANDIREF, Checked, ADMISSION_DATETIME, NOKAS, CheckedBy) "
					+ "values "
					+ "('" + receiptNo + "', '" + netto + "', '" + netto + "', '" + receiptDateTime + "', '" + periode + "', 0, 0, '2. Jurnal Pembelian', " 
					+ "'Auto Jurnal Retur Pembelian NoBukti " + stockInCodeNo + "', ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, "
					+ "NULL, NULL, 0, NULL, NULL, NULL)";
			pstmtInsertJurnal = purchaseConnection.prepareStatement(insertJurnal);
			
			pstmtInsertJurnal.setString(1, lastUpdateBy);
			pstmtInsertJurnal.setString(2, lastUpdateBy);
			
			pstmtInsertJurnal.executeUpdate();
			pstmtInsertJurnal.close();
			
			String insertJurnalDetail = "insert into dbo.GL_JurnalDetail "
					+ "(NoBukti, NoUrut, NomorUrut, Sandi, Keterangan, Debet, Kredit, TglTrans, TglTransKTHIS, Posted, "
					+ "ProsesAkhir, Unit, Periode, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NAMAREF, TEMPNUMBER, NOKAS) "
					+ "values "
					+ "('" + receiptNo + "', ?, ?, ?, 'Auto Jurnal Retur Pembelian NoBukti " + stockInCodeNo + "', ?, ?, '" + receiptDateTime + "', '" + receiptDateTime + "', 0, "
					+ "0, 0.00, '" + periode + "', ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, NULL)";
			pstmtInsertJurnalDetail = purchaseConnection.prepareStatement(insertJurnalDetail);
			
			int sequence = 1;
			pstmtInsertJurnalDetail.setInt(1, sequence);
			pstmtInsertJurnalDetail.setInt(2, sequence);
			pstmtInsertJurnalDetail.setString(3, glAccount);
			pstmtInsertJurnalDetail.setBigDecimal(4, netto);
			pstmtInsertJurnalDetail.setBigDecimal(5, BigDecimal.ZERO);
			pstmtInsertJurnalDetail.setString(6, lastUpdateBy);
			pstmtInsertJurnalDetail.setString(7, lastUpdateBy);
			pstmtInsertJurnalDetail.executeUpdate();
			
			String selectJurnalPrice = "select SUM(rbd.Qty * rbd.HargaUnit + rbd.PPNRp) debet, ms.SandiBeli "
					+ "from dbo.Tr_ReturBeliDetail rbd "
					+ "inner join dbo.Tr_ReturBeliMast rbm on rbm.RETURBELIMAST_ID = rbd.RETURBELIMAST_ID "
					+ "inner join dbo.Ms_Stock ms on ms.KodeStock = rbd.KodeStock "
					+ "where rbm.RETURBELIMAST_NO = '" + receiptNo + "' group by ms.SandiBeli";
			pstmtJurnalPrice = purchaseConnection.prepareStatement(selectJurnalPrice);
			rsJurnalPrice = pstmtJurnalPrice.executeQuery();
			
			BigDecimal totalDetail = BigDecimal.ZERO;
			
			while(rsJurnalPrice.next()) {
				BigDecimal debet = rsJurnalPrice.getBigDecimal("debet").setScale(2, RoundingMode.HALF_UP);
				String sandiBeli = rsJurnalPrice.getString("SandiBeli");
				
				totalDetail = totalDetail.add(debet);
				
				sequence++;
				pstmtInsertJurnalDetail.setInt(1, sequence);
				pstmtInsertJurnalDetail.setInt(2, sequence);
				pstmtInsertJurnalDetail.setString(3, sandiBeli);
				pstmtInsertJurnalDetail.setBigDecimal(4, BigDecimal.ZERO);
				pstmtInsertJurnalDetail.setBigDecimal(5, debet);
				pstmtInsertJurnalDetail.setString(6, lastUpdateBy);
				pstmtInsertJurnalDetail.setString(7, lastUpdateBy);
				pstmtInsertJurnalDetail.executeUpdate();
			}
			
			if (totalDetail.compareTo(netto) != 0) {
				String updateJurnal = "update dbo.GL_JurnalMast set Debet = '" + totalDetail + "', Kredit = '" + totalDetail + "' where NoBukti = '" + receiptNo + "'";
				pstmtInsertJurnal = purchaseConnection.prepareStatement(updateJurnal);
				pstmtInsertJurnal.executeUpdate();
				pstmtInsertJurnal.close();
				
				String updateJurnalDetail = "update dbo.GL_JurnalDetail set Debet = '" + totalDetail + "' where NoBukti = '" + receiptNo + "' and NoUrut = 1";
				pstmtInsertJurnal = purchaseConnection.prepareStatement(updateJurnalDetail);
				pstmtInsertJurnal.executeUpdate();
			}
			
			PurchaseStatus status = new PurchaseStatus();
			status.setStatus1("OK");
			status.setStatus2("OK");
			status.setStatus3("OK");
			view.add(status);
			
			purchaseConnection.commit();
			
//			String updateStatus = "UPDATE STOCKRECEIPT SET POSTING_IND = 'Y' WHERE RECEIPT_NO = '" + receiptNo + "'";
//			pstmtUpdateStatus = pooledConnection.prepareStatement(updateStatus);
//			pstmtUpdateStatus.executeUpdate();
			updatePostIndGR(receiptNo, IConstant.IND_YES);
		}
		catch (SQLException e){
			PurchaseStatus status = new PurchaseStatus();
		    if (e.getErrorCode() == 2627) { 
		    	status.setStatus1("Posting failed : Stock Receipt Return has been posting before !");
		    	updatePostIndGR(receiptNo, IConstant.IND_YES);
		    }
		    else status.setStatus1("Posting failed : " + e.getMessage());
		    view.add(status);
		    if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			PurchaseStatus status = new PurchaseStatus();
			status.setStatus1("Posting failed : " + e.getMessage());
			view.add(status);
			if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			if (purchaseConnection != null) try { purchaseConnection.setAutoCommit(true); purchaseConnection.close(); } catch (SQLException e) {}
 			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
 			if (pstmtStockReceiptReturn != null) try { pstmtStockReceiptReturn.close(); } catch (SQLException e) {}
 			if (pstmtCheckStockReceiptReturn != null) try { pstmtCheckStockReceiptReturn.close(); } catch (SQLException e) {}
 			if (pstmtInsertStockReceiptReturn != null) try { pstmtInsertStockReceiptReturn.close(); } catch (SQLException e) {}
 			if (pstmtReturnId != null) try { pstmtReturnId.close(); } catch (SQLException e) {}
 			if (pstmtStockReceiptReturnDetail != null) try { pstmtStockReceiptReturnDetail.close(); } catch (SQLException e) {}
 			if (pstmtInsertStockReceiptReturnDetail != null) try { pstmtInsertStockReceiptReturnDetail.close(); } catch (SQLException e) {}
 			if (pstmtInsertJurnal != null) try { pstmtInsertJurnal.close(); } catch (SQLException e) {}
 			if (pstmtInsertJurnalDetail != null) try { pstmtInsertJurnalDetail.close(); } catch (SQLException e) {}
 			if (pstmtJurnalPrice != null) try { pstmtJurnalPrice.close(); } catch (SQLException e) {}
 			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
 			if (rsStockReceiptReturn != null) try { rsStockReceiptReturn.close(); } catch (SQLException e) {}
 			if (rsCheckStockReceiptReturn != null) try { rsCheckStockReceiptReturn.close(); } catch (SQLException e) {}
 			if (rsReturnId != null) try { rsReturnId.close(); } catch (SQLException e) {}
 			if (rsStockReceiptReturnDetail != null) try { rsStockReceiptReturnDetail.close(); } catch (SQLException e) {}
 			if (rsJurnalPrice != null) try { rsJurnalPrice.close(); } catch (SQLException e) {}
		}
		return view;
	}
	 
	public static List<PurchaseStatus> AddConsignment(String purchaseNo) {
		List<PurchaseStatus> view = new ArrayList<PurchaseStatus>();
		Connection pooledConnection = null;
		Connection purchaseConnection = null;
		
		PreparedStatement pstmtStockPurchasePlan = null;
		PreparedStatement pstmtPrice = null;
		PreparedStatement pstmtInsertPoMast = null;
		PreparedStatement pstmtPuchasePlanDetail = null;
		PreparedStatement pstmtInsertPoDetail = null;
		PreparedStatement pstmtUpdateStatus = null;
		PreparedStatement pstmtInsertStockReceipt = null;
		PreparedStatement pstmtInsertStockReceiptDetail = null;
		PreparedStatement pstmtInsertJurnal = null;
		PreparedStatement pstmtInsertJurnalDetail = null;
		PreparedStatement pstmtJurnalPrice = null;
		
		ResultSet rsStockPurchasePlan = null;
		ResultSet rsPrice = null;
		ResultSet rsPurchasePlanDetail = null;
		ResultSet rsJurnalPrice = null;
		
		try {
			pooledConnection = DbConnection.getPooledConnection();
			purchaseConnection = DbConnection.getPurchaseConnection();
			if (pooledConnection == null || purchaseConnection == null) throw new Exception("Database failed to connect !");
			purchaseConnection.setAutoCommit(false);
			
			String selectStockPurchasePlanView = "SELECT SPP.STOCKPURCHASEPLAN_ID, SPP.CREATED_DATETIME, SPP.LAST_UPDATED_DATETIME, SPP.REMARKS, SPP.PURCHASE_STATUS, "
					+ "VM.GL_ACCOUNT, VM.VENDOR_NAME, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SPP.CREATED_BY) CREATED_BY, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SPP.LAST_UPDATED_BY) LAST_UPDATED_BY "
					+ "FROM STOCKPURCHASEPLAN SPP "
					+ "INNER JOIN VENDORMSTR VM ON SPP.VENDORMSTR_ID = VM.VENDORMSTR_ID "
					+ "WHERE SPP.PURCHASE_PLAN_NO = '" + purchaseNo + "' AND SPP.PURCHASE_STATUS = 'CSPS2'";
			pstmtStockPurchasePlan = pooledConnection.prepareStatement(selectStockPurchasePlanView);
			rsStockPurchasePlan = pstmtStockPurchasePlan.executeQuery();
			
			String receiptNo = purchaseNo.concat("/GR");
			
			if (!rsStockPurchasePlan.next()) throw new Exception("Purchase Plan No '" + purchaseNo + "' does not valid ! ");
			BigDecimal stockPurchasePlanId = rsStockPurchasePlan.getBigDecimal("STOCKPURCHASEPLAN_ID");
			Timestamp createdDateTime = rsStockPurchasePlan.getTimestamp("CREATED_DATETIME");
			Timestamp lastUpdatedDateTime = rsStockPurchasePlan.getTimestamp("LAST_UPDATED_DATETIME");
			String remarks = rsStockPurchasePlan.getString("REMARKS");
			String glAccount = rsStockPurchasePlan.getString("GL_ACCOUNT");
			String vendorName = rsStockPurchasePlan.getString("VENDOR_NAME");
			String createdBy = rsStockPurchasePlan.getString("CREATED_BY");
			String lastUpdatedBy = rsStockPurchasePlan.getString("LAST_UPDATED_BY");
			String periode = Integer.toString(createdDateTime.getYear() + 1900) + "123456789ABC".charAt(createdDateTime.getMonth());
			if (remarks == null) remarks = "";
			Map<String, String> supplier = getSupplier(purchaseConnection, glAccount, vendorName);
			
			String selectPrice = "SELECT SUM(UNIT_PRICE * PLAN_QTY * (100 - COALESCE(DISCOUNT_RATE, 0)) / 100) TOTAL_PRICE, "
					+ "COALESCE(PPN_IND, 0) PPN_IND "
					+ "FROM PURCHASEPLANDETAIL WHERE STOCKPURCHASEPLAN_ID = '" + stockPurchasePlanId + "' AND PLAN_DETAIL_STATUS = 'PDS2'"
					+ "GROUP BY COALESCE(PPN_IND, 0)";
			pstmtPrice = pooledConnection.prepareStatement(selectPrice);
			rsPrice = pstmtPrice.executeQuery();
			
			rsPrice.next();
			BigDecimal totalPrice = rsPrice.getBigDecimal("TOTAL_PRICE").setScale(2, RoundingMode.HALF_UP);
			BigDecimal ppnPercent = rsPrice.getBigDecimal("PPN_IND").setScale(2, RoundingMode.HALF_UP);
			BigDecimal ppn = totalPrice.multiply(ppnPercent).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
			BigDecimal netto = totalPrice.add(ppn).setScale(2, RoundingMode.HALF_UP);
			if (rsPrice.next()) throw new Exception("Cannot posting multiple distinct ppn type !");
			
			String insertPoMast = "insert into dbo.Tr_POMast "
					+ "(NoBukti, TglTransaksi, KodeSupp, NamaSupp, KodeLokasi, NamaLokasi, Terms, TglJthTempo, NoRef, "
					+ "TglRef, KodeMataUang, Rate, Keterangan, TotalDetail, DiscPersen, DiscRp, PPNPersen, PPNRp, "
					+ "Netto, Periode, Jenis, Posting, Receive, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime)"
					+ "values "
					+ "(?, ?, ?, ?, 'FARMASI', 'INSTALASI FARMASI', 0, ?, '', "
					+ "?, 'IDR', 1, ?, ?, 0.00, 0.00, ?, ?, "
					+ "?, ?, 'Stock', 0, 0, ?, ?, ?, ?)";
			pstmtInsertPoMast = purchaseConnection.prepareStatement(insertPoMast);
			
			pstmtInsertPoMast.setString(1, purchaseNo);
			pstmtInsertPoMast.setTimestamp(2, createdDateTime);
			pstmtInsertPoMast.setString(3, supplier.get("kodeSupp"));
			pstmtInsertPoMast.setString(4, supplier.get("namaSupp"));
			pstmtInsertPoMast.setTimestamp(5, createdDateTime);
			pstmtInsertPoMast.setTimestamp(6, createdDateTime);
			pstmtInsertPoMast.setString(7, remarks);
			pstmtInsertPoMast.setBigDecimal(8, totalPrice);
			pstmtInsertPoMast.setBigDecimal(9, ppnPercent);
			pstmtInsertPoMast.setBigDecimal(10, ppn);
			pstmtInsertPoMast.setBigDecimal(11, netto);
			pstmtInsertPoMast.setString(12, periode);
			pstmtInsertPoMast.setString(13, createdBy);
			pstmtInsertPoMast.setTimestamp(14, createdDateTime);
			pstmtInsertPoMast.setString(15, lastUpdatedBy);
			pstmtInsertPoMast.setTimestamp(16, lastUpdatedDateTime);
			
			pstmtInsertPoMast.executeUpdate();
			pstmtInsertPoMast.close();
			
			String insertStockReceipt = "insert into dbo.Tr_ReceivingGoodsMast "
					+ "(NoBukti, TglTransaksi, TglTerima, Supplier, KodeSupp, "
					+ "NoPO, TglPO, "
					+ "NoDO, TglDO, Keterangan, Jenis, Receive, Periode, CreatedBy, CreatedTime, ModifiedBy, "
					+ "ModifiedTime, Pilih, TotalDetail, Discount, PPN, Netto, GrandTotal, BayarDimuka) "
					+ "values "
					+ "('" + receiptNo + "', '" + createdDateTime + "', '" + createdDateTime + "', ?, ?, "
					+ ((purchaseNo == null) ? "NULL, NULL" : "'" + purchaseNo + "', '" + createdDateTime + "'")
					+ ", '', '" + createdDateTime + "', ?, 'Stock', 0, '" + periode + "', ?, '" + createdDateTime + "', ?, '"
					+ lastUpdatedDateTime + "', 0, " + totalPrice + ", 0.00, " + ppn + ", " + netto + ", " + netto + ", 0.00)";
			pstmtInsertStockReceipt = purchaseConnection.prepareStatement(insertStockReceipt);
			
			pstmtInsertStockReceipt.setString(1, supplier.get("namaSupp"));
			pstmtInsertStockReceipt.setString(2, supplier.get("kodeSupp"));
			pstmtInsertStockReceipt.setString(3, remarks);
			pstmtInsertStockReceipt.setString(4, createdBy);
			pstmtInsertStockReceipt.setString(5, lastUpdatedBy);
			
			pstmtInsertStockReceipt.executeUpdate();
			pstmtInsertStockReceipt.close();
			
			String selectPurchasePlanDetail = "SELECT PPD.PLAN_QTY, PPD.UNIT_PRICE, PPD.RECEIVED_QTY, PPD.REMARKS, PPD.CREATED_DATETIME, PPD.LAST_UPDATED_DATETIME, PPD.COMPLETE_IND, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = PPD.CREATED_BY) CREATED_BY, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = PPD.LAST_UPDATED_BY) LAST_UPDATED_BY, "
					+ "PGCOMMON.FXGETCODEDESC(PPD.PLAN_UOM) SATUAN, MIM.MATERIAL_ITEM_CODE, COALESCE(PPD.DISCOUNT_RATE, 0) DISCOUNT_RATE "
					+ "FROM PURCHASEPLANDETAIL PPD "
					+ "INNER JOIN MATERIALITEMMSTR MIM ON MIM.MATERIALITEMMSTR_ID = PPD.MATERIALITEMMSTR_ID "
					+ "WHERE PPD.STOCKPURCHASEPLAN_ID = '" + stockPurchasePlanId + "' AND PPD.PLAN_DETAIL_STATUS = 'PDS2'";
			pstmtPuchasePlanDetail = pooledConnection.prepareStatement(selectPurchasePlanDetail);
			rsPurchasePlanDetail = pstmtPuchasePlanDetail.executeQuery();
			
			BigDecimal totalDetail = BigDecimal.ZERO;
			BigDecimal totalDetailGR = BigDecimal.ZERO;
			
			int sequence = 1;
			while (rsPurchasePlanDetail.next()) {
				String kodeStock = rsPurchasePlanDetail.getString("MATERIAL_ITEM_CODE");
				BigDecimal planQty = rsPurchasePlanDetail.getBigDecimal("PLAN_QTY");
				BigDecimal unitPrice = rsPurchasePlanDetail.getBigDecimal("UNIT_PRICE").setScale(2, RoundingMode.HALF_UP);
				BigDecimal discountRate = rsPurchasePlanDetail.getBigDecimal("DISCOUNT_RATE");
				String detailRemarks = rsPurchasePlanDetail.getString("REMARKS");
				String detailCreatedBy = rsPurchasePlanDetail.getString("CREATED_BY");
				String detailLastUpdatedBy = rsPurchasePlanDetail.getString("LAST_UPDATED_BY");
				Timestamp detailCreatedDateTime = rsStockPurchasePlan.getTimestamp("CREATED_DATETIME");
				Timestamp detailLastUpdatedDateTime = rsStockPurchasePlan.getTimestamp("LAST_UPDATED_DATETIME");
				String satuan = rsPurchasePlanDetail.getString("SATUAN");
				BigDecimal detailTotalPrice = planQty.multiply(unitPrice).setScale(2, RoundingMode.HALF_UP);
				BigDecimal discount = detailTotalPrice.multiply(discountRate).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
				BigDecimal detailNetto = detailTotalPrice.subtract(discount).setScale(2, RoundingMode.HALF_UP);
				if (detailRemarks == null) detailRemarks = "";
				String namaStock = getStockName(purchaseConnection, kodeStock);
				
				totalDetail = totalDetail.add(detailNetto);
				
				String insertPoDetail = "insert into dbo.Tr_PODetail "
						+ "(NoBukti, NomorUrut, KodeStock, NamaStock, KodeSupp, NamaSupp, KodeLokasi, NamaLokasi, "
						+ "Qty, Satuan, HargaUnit, TotalHarga, DiscPersen, DiscRp, Netto, Keterangan, "
						+ "Periode, JlhTerima, Receive, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime) "
						+ "values "
						+ "('" + purchaseNo + "', '" + sequence + "', ?, ?, ?, ?, 'FARMASI', 'INSTALASI FARMASI', " 
						+ planQty + ", ?, " + unitPrice + ", " + detailTotalPrice + ", " + discountRate + ", " + discount + ", " + detailNetto + ", ?, '" 
						+ periode + "', 0, 0, ?, '" + detailCreatedDateTime + "', ?, '" + detailLastUpdatedDateTime + "')";
				pstmtInsertPoDetail = purchaseConnection.prepareStatement(insertPoDetail);
				
				pstmtInsertPoDetail.setString(1, kodeStock);
				pstmtInsertPoDetail.setString(2, namaStock);
				pstmtInsertPoDetail.setString(3, supplier.get("kodeSupp"));
				pstmtInsertPoDetail.setString(4, supplier.get("namaSupp"));
				pstmtInsertPoDetail.setString(5, satuan);
				pstmtInsertPoDetail.setString(6, detailRemarks);
				pstmtInsertPoDetail.setString(7, detailCreatedBy);
				pstmtInsertPoDetail.setString(8, detailLastUpdatedBy);
				
				pstmtInsertPoDetail.executeUpdate();
				
				BigDecimal unitPriceGR = unitPrice.multiply(new BigDecimal(100).subtract(discountRate)).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
				totalDetailGR = totalDetailGR.add(planQty.multiply(unitPriceGR)).setScale(2, RoundingMode.HALF_UP);
				
				String insertStockReceiptDetail = "insert into dbo.Tr_ReceivingGoods "
						+ "(NoBukti, NomorUrut, KodeStock, NamaStock, Qty, Satuan, Remark, "
						+ "Periode, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, QtyTerima, "
						+ "NoPo, ContUnit, HargaUnitNet, QtyRetur) "
						+ "values "
						+ "('" + receiptNo + "', '" + sequence + "', ?, ?, '" + planQty + "', ?, ?, '" 
						+ periode + "', ?, '" + createdDateTime + "', ?, '" + detailLastUpdatedDateTime + "', 0, " 
						+ (purchaseNo == null ? "NULL" : "'" + purchaseNo + "'") + ", 0, '" + unitPriceGR + "', 0)";
				pstmtInsertStockReceiptDetail = purchaseConnection.prepareStatement(insertStockReceiptDetail);
				
				pstmtInsertStockReceiptDetail.setString(1, kodeStock);
				pstmtInsertStockReceiptDetail.setString(2, namaStock);
				pstmtInsertStockReceiptDetail.setString(3, satuan);
				pstmtInsertStockReceiptDetail.setString(4, detailRemarks);
				pstmtInsertStockReceiptDetail.setString(5, createdBy);
				pstmtInsertStockReceiptDetail.setString(6, detailLastUpdatedBy);
				
				pstmtInsertStockReceiptDetail.executeUpdate();
				
				sequence++;
			}
			
			if (totalDetail.compareTo(totalPrice) != 0) {
				ppn = totalDetail.multiply(ppnPercent).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
				netto = totalDetail.add(ppn);
				String updatePoMast = "update dbo.Tr_POMast set TotalDetail = '" + totalDetail + "', PPNRp = '" + ppn + "', Netto = '" + netto + "' where NoBukti = '" + purchaseNo + "'";
				pstmtInsertPoMast = purchaseConnection.prepareStatement(updatePoMast);
				pstmtInsertPoMast.executeUpdate();
			}
			
			if (totalDetailGR.compareTo(totalPrice) != 0) {
				ppn = totalDetailGR.multiply(ppnPercent).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
				netto = totalDetailGR.add(ppn);
				String updateStockReceiptMast = "update dbo.Tr_ReceivingGoodsMast set TotalDetail = '" + totalDetailGR + "', PPN = '" + ppn + "', Netto = '" + netto + "', GrandTotal = '" + netto + "' where NoBukti = '" + receiptNo + "'";
				pstmtInsertStockReceipt = purchaseConnection.prepareStatement(updateStockReceiptMast);
				pstmtInsertStockReceipt.executeUpdate();
			}
			
			String insertJurnal = "insert into dbo.GL_JurnalMast "
					+ "(NoBukti, Debet, Kredit, TglTrans, Periode, Posted, ProsesAkhir, Tipe, "
					+ "Keterangan, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NoRef, TEMPNUMBER, "
					+ "NAMAREF, SANDIREF, Checked, ADMISSION_DATETIME, NOKAS, CheckedBy) "
					+ "values "
					+ "('" + receiptNo + "', '" + netto + "', '" + netto + "', '" + createdDateTime + "', '" + periode + "', 0, 0, '2. Jurnal Pembelian', " 
					+ "'', ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, "
					+ "NULL, NULL, 0, NULL, NULL, NULL)";
			pstmtInsertJurnal = purchaseConnection.prepareStatement(insertJurnal);
			
			pstmtInsertJurnal.setString(1, lastUpdatedBy);
			pstmtInsertJurnal.setString(2, lastUpdatedBy);
			
			pstmtInsertJurnal.executeUpdate();
			pstmtInsertJurnal.close();
			
			String insertJurnalDetail = "insert into dbo.GL_JurnalDetail "
					+ "(NoBukti, NoUrut, NomorUrut, Sandi, Keterangan, Debet, Kredit, TglTrans, TglTransKTHIS, Posted, "
					+ "ProsesAkhir, Unit, Periode, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NAMAREF, TEMPNUMBER, NOKAS) "
					+ "values "
					+ "('" + receiptNo + "', ?, ?, ?, '', ?, ?, '" + createdDateTime + "', '" + createdDateTime + "', 0, "
					+ "0, 0.00, '" + periode + "', ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, NULL)";
			pstmtInsertJurnalDetail = purchaseConnection.prepareStatement(insertJurnalDetail);
			
			sequence = 1;
			pstmtInsertJurnalDetail.setInt(1, sequence);
			pstmtInsertJurnalDetail.setInt(2, sequence);
			pstmtInsertJurnalDetail.setString(3, glAccount);
			pstmtInsertJurnalDetail.setBigDecimal(4, BigDecimal.ZERO);
			pstmtInsertJurnalDetail.setBigDecimal(5, netto);
			pstmtInsertJurnalDetail.setString(6, lastUpdatedBy);
			pstmtInsertJurnalDetail.setString(7, lastUpdatedBy);
			pstmtInsertJurnalDetail.executeUpdate();
			
			String selectJurnalPrice = "select SUM(rg.Qty * rg.HargaUnitNet * (1 + rgm.PPN / rgm.TotalDetail)) debet, ms.SandiBeli "
					+ "from dbo.Tr_ReceivingGoods rg "
					+ "inner join dbo.Tr_ReceivingGoodsMast rgm on rgm.NoBukti = rg.NoBukti "
					+ "inner join dbo.Ms_Stock ms on ms.KodeStock = rg.KodeStock "
					+ "where rg.NoBukti = '" + receiptNo + "' group by ms.SandiBeli";
			pstmtJurnalPrice = purchaseConnection.prepareStatement(selectJurnalPrice);
			
			rsJurnalPrice = pstmtJurnalPrice.executeQuery();
			
			totalDetail = BigDecimal.ZERO;
			
			while(rsJurnalPrice.next()) {
				BigDecimal debet = rsJurnalPrice.getBigDecimal("debet").setScale(2, RoundingMode.HALF_UP);
				String sandiBeli = rsJurnalPrice.getString("SandiBeli");
				
				totalDetail = totalDetail.add(debet);
				
				sequence++;
				pstmtInsertJurnalDetail.setInt(1, sequence);
				pstmtInsertJurnalDetail.setInt(2, sequence);
				pstmtInsertJurnalDetail.setString(3, sandiBeli);
				pstmtInsertJurnalDetail.setBigDecimal(4, debet);
				pstmtInsertJurnalDetail.setBigDecimal(5, BigDecimal.ZERO);
				pstmtInsertJurnalDetail.setString(6, lastUpdatedBy);
				pstmtInsertJurnalDetail.setString(7, lastUpdatedBy);
				pstmtInsertJurnalDetail.executeUpdate();
			}
			
			if (totalDetail.compareTo(netto) != 0) {
				String updateJurnal = "update dbo.GL_JurnalMast set Debet = '" + totalDetail + "', Kredit = '" + totalDetail + "' where NoBukti = '" + receiptNo + "'";
				pstmtInsertJurnal = purchaseConnection.prepareStatement(updateJurnal);
				pstmtInsertJurnal.executeUpdate();
				pstmtInsertJurnal.close();
				
				String updateJurnalDetail = "update dbo.GL_JurnalDetail set Kredit = '" + totalDetail + "' where NoBukti = '" + receiptNo + "' and NoUrut = 1";
				pstmtInsertJurnal = purchaseConnection.prepareStatement(updateJurnalDetail);
				pstmtInsertJurnal.executeUpdate();
			}
			
			PurchaseStatus status = new PurchaseStatus();
			status.setStatus1("OK");
			status.setStatus2("OK");
			status.setStatus3("OK");
			view.add(status);
			
			purchaseConnection.commit();
			
//			String updateStatus = "UPDATE STOCKPURCHASEPLAN SET POSTING_IND = 'Y' WHERE PURCHASE_PLAN_NO = '" + purchaseNo + "'";
//			pstmtUpdateStatus = pooledConnection.prepareStatement(updateStatus);
//			pstmtUpdateStatus.executeUpdate();
			updatePostIndPO(purchaseNo, IConstant.IND_YES);
		}
		catch (SQLException e){
			PurchaseStatus status = new PurchaseStatus();
		    if (e.getErrorCode() == 2627) { 
		    	status.setStatus1("Posting failed : Purchase Plan has been posting before !"); 
		    	updatePostIndPO(purchaseNo, IConstant.IND_YES);
		    }
		    else status.setStatus1("Posting failed : " + e.getMessage());
		    view.add(status);
		    if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			PurchaseStatus status = new PurchaseStatus();
			status.setStatus1("Posting failed : " + e.getMessage());
			view.add(status);
			if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			if (purchaseConnection != null) try { purchaseConnection.setAutoCommit(true); purchaseConnection.close(); } catch (SQLException e) {}
			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
			if (pstmtStockPurchasePlan != null) try { pstmtStockPurchasePlan.close(); } catch (SQLException e) {}
			if (pstmtPrice != null) try { pstmtPrice.close(); } catch (SQLException e) {}
			if (pstmtInsertPoMast != null) try { pstmtInsertPoMast.close(); } catch (SQLException e) {}
			if (pstmtPuchasePlanDetail != null) try { pstmtPuchasePlanDetail.close(); } catch (SQLException e) {}
			if (pstmtInsertPoDetail != null) try { pstmtInsertPoDetail.close(); } catch (SQLException e) {}
			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
			if (pstmtInsertStockReceipt != null) try { pstmtInsertStockReceipt.close(); } catch (SQLException e) {}
			if (pstmtInsertStockReceiptDetail != null) try { pstmtInsertStockReceiptDetail.close(); } catch (SQLException e) {}
			if (pstmtInsertJurnal != null) try { pstmtInsertJurnal.close(); } catch (SQLException e) {}
			if (pstmtInsertJurnalDetail != null) try { pstmtInsertJurnalDetail.close(); } catch (SQLException e) {}
			if (pstmtJurnalPrice != null) try { pstmtJurnalPrice.close(); } catch (SQLException e) {}
			if (rsStockPurchasePlan != null) try { rsStockPurchasePlan.close(); } catch (SQLException e) {}
			if (rsPrice != null) try { rsPrice.close(); } catch (SQLException e) {}
			if (rsPurchasePlanDetail != null) try { rsPurchasePlanDetail.close(); } catch (SQLException e) {}
			if (rsJurnalPrice != null) try { rsJurnalPrice.close(); } catch (SQLException e) {}
		}
		return view;
	}
	
	private static Map<String, String> getSupplier(Connection connection, String glAccount, String vendorName) throws Exception {
		PreparedStatement pstmtSupplier = null;
		ResultSet rsSupplier = null;
		Map<String, String> map = new HashMap<String, String>();
		try {
			if (glAccount == null) throw new Exception("Please set GL Account for Supplier '" + vendorName + "' !");
			String selectSupplier = "select KodeSupp, NamaSupp from dbo.Ms_Supplier where NoSandi = '" + glAccount + "' and KodeSupp not like '%DELETE%'";
			pstmtSupplier = connection.prepareStatement(selectSupplier);
			rsSupplier = pstmtSupplier.executeQuery();
			
			if (!rsSupplier.next()) throw new Exception("Supplier with GL Account '" + glAccount + "' not found !");
			map.put("kodeSupp", rsSupplier.getString("KodeSupp"));
			map.put("namaSupp", rsSupplier.getString("NamaSupp"));
			if (rsSupplier.next()) throw new Exception("Duplicate Supplier with GL Account '" + glAccount + "' !");
		}
		catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		finally {
			if (pstmtSupplier != null) try { pstmtSupplier.close(); } catch (SQLException sqle) {}
			if (rsSupplier != null) try { rsSupplier.close(); } catch (SQLException sqle) {}
		}
		return map;
	}
	
	private static String getStockName(Connection connection, String stockCode) throws Exception {
		PreparedStatement pstmtStock = null;
		ResultSet rsStock = null;
		String stockName = null;
		try {
			String selectStock = "select NamaStock from dbo.Ms_Stock where KodeStock = '" + stockCode + "'";
			pstmtStock = connection.prepareStatement(selectStock);
			rsStock = pstmtStock.executeQuery();
			
			if (!rsStock.next()) throw new Exception("Material Code '" + stockCode + "' not found");
			stockName = rsStock.getString("NamaStock");
			if (rsStock.next()) throw new Exception("Duplicate Material Code '" + stockCode + "' !");
		}
		catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		finally {
			if (pstmtStock != null) try { pstmtStock.close(); } catch (SQLException sqle) {}
			if (rsStock != null) try { rsStock.close(); } catch (SQLException sqle) {}
		}
		return stockName;
	}
	
	public static List<PurchaseStatus> UnpostPurchase(String purchaseNo) {
		List<PurchaseStatus> view = new ArrayList<PurchaseStatus>();
		PurchaseStatus purchaseStatus = new PurchaseStatus();
		Connection pooledConnection = null;
		Connection purchaseConnection = null;
		
		PreparedStatement pstmtStockPurchasePlan = null;
		PreparedStatement pstmtCheckPoMast = null;
//		PreparedStatement pstmtUpdateStatus = null;
		
		ResultSet rsStockPurchasePlan = null;
		ResultSet rsCheckPoMast = null;
		
		try {
			pooledConnection = DbConnection.getPooledConnection();
			purchaseConnection = DbConnection.getPurchaseConnection();
			if (pooledConnection == null || purchaseConnection == null) throw new Exception("Database failed to connect !");
			
			String selectStockPurchasePlanView = "SELECT SPP.STOCKPURCHASEPLAN_ID, SPP.CREATED_DATETIME, SPP.LAST_UPDATED_DATETIME, SPP.REMARKS, SPP.PURCHASE_STATUS, "
					+ "VM.GL_ACCOUNT, VM.VENDOR_NAME, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SPP.CREATED_BY) CREATED_BY, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SPP.LAST_UPDATED_BY) LAST_UPDATED_BY "
					+ "FROM STOCKPURCHASEPLAN SPP "
					+ "INNER JOIN VENDORMSTR VM ON SPP.VENDORMSTR_ID = VM.VENDORMSTR_ID "
					+ "WHERE SPP.PURCHASE_PLAN_NO = '" + purchaseNo + "' AND SPP.PURCHASE_STATUS IN ('SPS3', 'SPS4', 'SPS6', 'SPS7')";
			pstmtStockPurchasePlan = pooledConnection.prepareStatement(selectStockPurchasePlanView);
			rsStockPurchasePlan = pstmtStockPurchasePlan.executeQuery();
			
			if (!rsStockPurchasePlan.next()) throw new Exception("Purchase Plan No '" + purchaseNo + "' does not valid ! ");
			
			String checkPoMast = "SELECT count(*) as 'countPo' FROM dbo.Tr_POMast "
					+ " where NoBukti = ? ";
			pstmtCheckPoMast = purchaseConnection.prepareStatement(checkPoMast);
			pstmtCheckPoMast.setString(1, purchaseNo);
			
			rsCheckPoMast = pstmtCheckPoMast.executeQuery();
			
			if (rsCheckPoMast.next()){
				BigDecimal totalPo = rsCheckPoMast.getBigDecimal("countPo");
				if (totalPo.compareTo(BigDecimal.ZERO) == 0){
//					String updateStockPurchasePlan = "UPDATE STOCKPURCHASEPLAN SPP"
//							+ " SET SPP.POSTING_IND = 'N' "
//							+ " WHERE SPP.PURCHASE_PLAN_NO = '" + purchaseNo + "'";
//					
//					pstmtUpdateStatus = pooledConnection.prepareStatement(updateStockPurchasePlan);
//					pstmtUpdateStatus.executeUpdate();
					updatePostIndPO(purchaseNo, IConstant.IND_NO);
				} else {
					throw new Exception("Purchase Plan No '" + purchaseNo + "' have been posted");
				}
			}
			
			purchaseStatus.setStatus1("OK");
			purchaseStatus.setStatus2("OK");
			purchaseStatus.setStatus3("OK");
			view.add(purchaseStatus);
		}
		catch (SQLException e){
			PurchaseStatus status = new PurchaseStatus();
		    if (e.getErrorCode() == 2627) { status.setStatus1("Posting failed : Purchase Plan has been posting before !"); }
		    else status.setStatus1("Posting failed : " + e.getMessage());
		    view.add(status);
		    if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			PurchaseStatus status = new PurchaseStatus();
			status.setStatus1("Posting failed : " + e.getMessage());
			view.add(status);
			if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			if (purchaseConnection != null) try {  purchaseConnection.close(); } catch (SQLException e) {}
			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
			if (pstmtStockPurchasePlan != null) try { pstmtStockPurchasePlan.close(); } catch (SQLException e) {}
//			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
			if (rsStockPurchasePlan != null) try { rsStockPurchasePlan.close(); } catch (SQLException e) {}
			if (rsCheckPoMast != null) try { rsCheckPoMast.close(); } catch (SQLException e) {}
		}
		return view;
	}
	
	public static List<PurchaseStatus> UnpostReceive(String receiptNo) {
		List<PurchaseStatus> view = new ArrayList<PurchaseStatus>();
		Connection pooledConnection = null;
		Connection purchaseConnection = null;
		
		PreparedStatement pstmtStockReceipt = null;
		PreparedStatement pstmtCheckGrMast = null;
		PreparedStatement pstmtUpdateStatus = null;
		
		ResultSet rsStockReceipt = null;
		ResultSet rsCheckGr = null;
		
		try {
			pooledConnection = DbConnection.getPooledConnection();
			purchaseConnection = DbConnection.getPurchaseConnection();
			if (pooledConnection == null || purchaseConnection == null) throw new Exception("Database failed to connect !");
			
			String selectStockReceipt = "SELECT SR.STOCKRECEIPT_ID, SR.RECEIPT_DATETIME, SR.PURCHASE_PLAN_NO, SR.REMARKS, SR.CREATED_DATETIME, SR.LAST_UPDATED_DATETIME, "
					+ "VM.GL_ACCOUNT, VM.VENDOR_NAME, SPP.CREATED_DATETIME PURCHASE_DATETIME, SPP.PURCHASE_STATUS, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SR.CREATED_BY) CREATED_BY, "
					+ "(SELECT USER_NAME FROM USERMSTR WHERE USERMSTR_ID = SR.LAST_UPDATED_BY) LAST_UPDATED_BY "
					+ "FROM STOCKRECEIPT SR "
					+ "INNER JOIN VENDORMSTR VM ON SR.VENDORMSTR_ID = VM.VENDORMSTR_ID "
					+ "INNER JOIN STOCKPURCHASEPLAN SPP ON SPP.PURCHASE_PLAN_NO = SR.PURCHASE_PLAN_NO "
					+ "WHERE SR.RECEIPT_NO = '" + receiptNo + "' AND SR.RECEIPT_STATUS = 'SRSCSR' AND SR.RECEIPT_TYPE = 'SRT1'";
			pstmtStockReceipt = pooledConnection.prepareStatement(selectStockReceipt);
			rsStockReceipt = pstmtStockReceipt.executeQuery();
			
			if (!rsStockReceipt.next()) throw new Exception("Stock Receipt No '" + receiptNo + "' does not valid !");
			
			String checkGrMast = "SELECT count(*) as 'countGr' FROM dbo.Tr_ReceivingGoodsMast "
					+ " where NoBukti = ? ";
			pstmtCheckGrMast = purchaseConnection.prepareStatement(checkGrMast);
			pstmtCheckGrMast.setString(1, receiptNo);
			
			rsCheckGr = pstmtCheckGrMast.executeQuery();
			
			if (rsCheckGr.next()){
				BigDecimal totalPo = rsCheckGr.getBigDecimal("countGr");
				if (totalPo.compareTo(BigDecimal.ZERO) == 0){
					String updateStockPurchasePlan = "UPDATE STOCKRECEIPT SR"
							+ " SET SR.POSTING_IND = 'N' "
							+ " WHERE SR.RECEIPT_NO = '" + receiptNo + "'";
					
					pstmtUpdateStatus = pooledConnection.prepareStatement(updateStockPurchasePlan);
					pstmtUpdateStatus.executeUpdate();
				}else {
					throw new Exception("Stock Receipt No '" + receiptNo + "' have been posted !");
				}
			}
			
			PurchaseStatus status = new PurchaseStatus();
			status.setStatus1("OK");
			status.setStatus2("OK");
			status.setStatus3("OK");
			view.add(status);			
		}
		catch (SQLException e){
			PurchaseStatus status = new PurchaseStatus();
		    if (e.getErrorCode() == 2627) { status.setStatus1("Posting failed : Stock Receipt has been posting before !"); }
		    else status.setStatus1("Posting failed : " + e.getMessage());
		    view.add(status);
		    if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			PurchaseStatus status = new PurchaseStatus();
			status.setStatus1("Posting failed : " + e.getMessage());
			view.add(status);
			if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
			if (purchaseConnection != null) try { purchaseConnection.close(); } catch (SQLException e) {}
 			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
			if (pstmtStockReceipt != null) try { pstmtStockReceipt.close(); } catch (SQLException e) {}
			if (pstmtCheckGrMast != null) try { pstmtCheckGrMast.close(); } catch (SQLException e) {}
			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
			if (rsStockReceipt != null) try { rsStockReceipt.close(); } catch (SQLException e) {}
			if (rsCheckGr != null) try { rsCheckGr.close(); } catch (SQLException e) {}
		}
		return view;
	}

	public static void updatePostIndPO(String purchaseNo, String postingInd){
		Connection pooledConnection = null;
		PreparedStatement pstmtUpdateStatus = null;
		
		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null ) throw new Exception("Database failed to connect !");
			
			String updateStatus = "UPDATE STOCKPURCHASEPLAN SET POSTING_IND = '" + postingInd + "' WHERE PURCHASE_PLAN_NO = '" + purchaseNo + "'";
			pstmtUpdateStatus = pooledConnection.prepareStatement(updateStatus);
			pstmtUpdateStatus.executeUpdate();
			
			pstmtUpdateStatus.close();
			pooledConnection.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		    if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
		}
	}
	
	public static void updatePostIndGR(String receiptNo, String postingInd){
		Connection pooledConnection = null;
		PreparedStatement pstmtUpdateStatus = null;
		
		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null ) throw new Exception("Database failed to connect !");
			
			String updateStatus = "UPDATE STOCKRECEIPT SET POSTING_IND = '" + postingInd + "' WHERE RECEIPT_NO = '" + receiptNo + "'";
			pstmtUpdateStatus = pooledConnection.prepareStatement(updateStatus);
			pstmtUpdateStatus.executeUpdate();
			
			pstmtUpdateStatus.close();
			pooledConnection.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		    if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
		}
	}
}
