package com.rsmurniteguh.webservice.dep.kthis.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.ws.rs.FormParam;

import org.apache.http.util.TextUtils;

import com.ctc.wstx.util.StringUtil;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.StringUtils;
import com.google.gson.Gson;
import com.rsmurniteguh.webservice.dep.all.model.BaseInfo;
import com.rsmurniteguh.webservice.dep.all.model.CurrentDatetim;
import com.rsmurniteguh.webservice.dep.all.model.InsertSepMtregParam;
import com.rsmurniteguh.webservice.dep.all.model.IntegratedKthis;
import com.rsmurniteguh.webservice.dep.all.model.ListGetQueue;
import com.rsmurniteguh.webservice.dep.all.model.ListSelfReg;
import com.rsmurniteguh.webservice.dep.all.model.RegData;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.ResponseRujukan;
import com.rsmurniteguh.webservice.dep.all.model.PDFUpload;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInfoResponse;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInsertSepResponse;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsRujukanResponse;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsSepDetailResponse;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsRujukanResponse.Rujukan;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.CancelReg;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.CreateSep;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.DataRujukan;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.DetailSep;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.GetSep;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegBpjs;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.InsertRegGeneral;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.RegKthis;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.SepList;
import com.rsmurniteguh.webservice.dep.all.model.queue.QueueDetails;
import com.rsmurniteguh.webservice.dep.all.model.queue.RequestTicket;
import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.biz.QueueServiceBiz;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Item;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.MemberInfo;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Metadata;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsIntegration.ISep.PostRegisterData;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsIntegration.ISep.PostRegisterData.RujukanData;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsIntegration.ISep.PostRegisterData.SepBpjsData;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsService2.BPJSVersion;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class MtRegistrationService {
	private static String mPpkpelayanan = "0038R091";
	private static String mJp = "2";
	private static String mDiagawal = "Z04.8";
	private static String mKla = "2";
	private static String mBpjs = "BPJS";
	private static String mGeneral = "GENERAL";
	private static String mBasicPhoneNumber = "006180501888";
	private static String BPJS_MEMBER_STATUS_NON_ACTIVE_PREMI = "9";
	private static String mUser = "SELFREGISTRATION";
	private static final String paramBpjsVersion = getBaseConfig(IParameterConstant.BPJS_VERSION);
	
	private static String getBaseConfig(String parameter){
		CommonService cs = new CommonService();
		return cs.getParameterValue(parameter);
	}

	private static class InsertQueueRequest {
		private String resourceMstrId;
		private String location;
		private String date;
		private RequestTicket data;
		private Boolean success = false;
		private String message;

		public Boolean getSuccess() {
			return success;
		}

		public void setSuccess(Boolean success) {
			this.success = success;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public RequestTicket getData() {
			return data;
		}

		public void setData(RequestTicket data) {
			this.data = data;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public String getResourceMstrId() {
			return resourceMstrId;
		}

		public void setResourceMstrId(String resourceMstrId) {
			this.resourceMstrId = resourceMstrId;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

	}

	public static BaseInfo getBaseInfo(String baseno) {
		BaseInfo data = new BaseInfo();
		Connection connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		ResultSet rs = null;

		PreparedStatement ps = null;

		String sql = "SELECT * FROM (SELECT CD.CARD_NO, PT.BPJS_NO, PS.PERSON_NAME,PS.SEX,PS.BIRTH_DATE,PT.PATIENT_CLASS,PS.MARITAL_STATUS,AD.ADDRESS_1,PT.PATIENT_ID, PS.MOBILE_PHONE_NO "
				+ "FROM CARD CD " + "INNER JOIN PERSON PS ON PS.PERSON_ID=CD.PERSON_ID "
				+ "INNER JOIN PATIENT PT ON PS.PERSON_ID=PT.PERSON_ID "
				+ "INNER JOIN STAKEHOLDER SH ON PS.STAKEHOLDER_ID = SH.STAKEHOLDER_ID "
				+ "INNER JOIN ADDRESS AD ON SH.STAKEHOLDER_ID= AD.STAKEHOLDER_ID "
				+ "WHERE [FILTER] ORDER BY SH.STAKEHOLDER_ID DESC )WHERE ROWNUM=1 ";

		try {
			connection.setAutoCommit(false);
			baseno = baseno.replaceAll("[^\\d.]", "");
			data.setDescription("data tidak ditemukan");

			String ret = BpjsService2.GetBpjsInfobyNoka(baseno);
			Gson gson = new Gson();
			BpjsInfoResponse memberInfo = gson.fromJson(ret, BpjsInfoResponse.class);

			Metadata metadata = memberInfo == null ? null : memberInfo.getMetadata();
			Boolean bpjsFound = false;
			if (metadata != null) {
				if (!TextUtils.isEmpty(metadata.getCode()) || !TextUtils.isEmpty(metadata.getMessage())) {
					if (metadata.getCode().equals("200") && metadata.getMessage().equals("OK")) {
						String kodeStatusPeserta = memberInfo.getResponse().getPeserta().getStatusPeserta().getKode();
						Boolean isBpjsUserActive = !kodeStatusPeserta.equals(BPJS_MEMBER_STATUS_NON_ACTIVE_PREMI);

						if (isBpjsUserActive) {
							ps = connection.prepareStatement(sql.replace("[FILTER]", "PT.BPJS_NO = ?"));
							ps.setString(1, baseno);
							rs = ps.executeQuery();

							if (rs.next()) {
								data.setPatientId(rs.getString("PATIENT_ID"));
								data.setPERSON_NAME(rs.getString("PERSON_NAME"));
								data.setSEX(rs.getString("SEX"));
								data.setBIRTH_DATE(rs.getString("BIRTH_DATE"));
								data.setPATIENT_CLASS(rs.getString("PATIENT_CLASS"));
								data.setMARITAL_STATUS(rs.getString("MARITAL_STATUS"));
								data.setADDRESS_1(rs.getString("ADDRESS_1"));
								data.setMrn(rs.getString("CARD_NO"));
								String kthisPatientMobilePhoneNo = rs.getString("MOBILE_PHONE_NO");
								String bpjsPatientMobilePhoneNo = memberInfo.getResponse().getPeserta().getMr()
										.getNoTelepon();
								String validPatientMobilePhoneNo = getValidPatientMobilePhoneNoFrom(
										kthisPatientMobilePhoneNo, bpjsPatientMobilePhoneNo);
								data.setNoHP(validPatientMobilePhoneNo);
								data.setScanCode(baseno);
								data.setDepartment("BPJS");
								data.setResponse(true);
								data.setDescription("Data Berhasil ditemukan di BPJS");

								BaseInfo getrujukan = MtRegistrationService.getRujukan(baseno, data, memberInfo);
								if (getrujukan.isResponse()) {
									data.setNomorRujukan(getrujukan.getNomorRujukan());
									data.setTanggalRujukan(getrujukan.getTanggalRujukan());
									data.setPpkrujukan(
											memberInfo.getResponse().getPeserta().getProvUmum().getNmProvider());
									data.setKlsrawat(getrujukan.getKlsrawat());
									data.setLastSep(getrujukan.getLastSep());
									data.setDiagAwal(getrujukan.getDiagAwal());
									data.setJnsPelayanan(getrujukan.getJnsPelayanan());
									data.setAsalRujuk(
											memberInfo.getResponse().getPeserta().getProvUmum().getKdProvider());
									bpjsFound = true;
								} else {
									data.setResponse(false);
									data.setDescription(getrujukan.getDescription());
								}

								connection.commit();
							} else {
								data.setResponse(false);
								data.setDescription("Data di BPJS ada , namun di KTHIS tidak ada");
							}
						} else {
							String bpjsUserStatusMessage = memberInfo.getResponse().getPeserta().getStatusPeserta()
									.getKeterangan();
							data.setResponse(false);
							data.setDescription(bpjsUserStatusMessage);
						}
					}
				}
			}

			if (!bpjsFound) {
				ps = connection.prepareStatement(sql.replace("[FILTER]", "CD.CARD_NO = ?"));
				ps.setString(1, baseno);
				rs = ps.executeQuery();

				if (rs.next()) {
					data.setPatientId(rs.getString("PATIENT_ID"));
					data.setPERSON_NAME(rs.getString("PERSON_NAME"));
					data.setSEX(rs.getString("SEX"));
					data.setBIRTH_DATE(rs.getString("BIRTH_DATE"));
					data.setPATIENT_CLASS(rs.getString("PATIENT_CLASS"));
					data.setMARITAL_STATUS(rs.getString("MARITAL_STATUS"));
					data.setADDRESS_1(rs.getString("ADDRESS_1"));
					data.setNoHP(rs.getString("MOBILE_PHONE_NO"));
					data.setScanCode(baseno);
					data.setMrn(baseno);
					data.setDepartment("GENERAL");
					data.setResponse(true);
					data.setDescription("Data Berhasil ditemukan di GENERAL");

					connection.commit();
				}
			}
		} catch (Exception e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if (connection != null) {
				try {
					System.err.println("Transaction is being rolled back in " + e.getStackTrace()[0].getMethodName());
					connection.rollback();
				} catch (SQLException excep) {
					System.out.println(excep.getMessage());
				}
			}
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
			}

			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}

			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	private static String getValidPatientMobilePhoneNoFrom(String kthisPatientMobilePhoneNo,
			String bpjsPatientMobilePhoneNo) {
		Boolean isKTHISValid = validationMobilePhoneNo(kthisPatientMobilePhoneNo);
		Boolean isBPJSValid = validationMobilePhoneNo(bpjsPatientMobilePhoneNo);
		if (isKTHISValid)
			return kthisPatientMobilePhoneNo;
		else if (!isKTHISValid && isBPJSValid)
			return bpjsPatientMobilePhoneNo;
		else
			return mBasicPhoneNumber;
	}

	private static Boolean validationMobilePhoneNo(String mobilePhoneNo) {
		Boolean result = true;
		if (TextUtils.isEmpty(mobilePhoneNo))
			return false;
		int count = mobilePhoneNo.length();
		if ((count != 11 && count != 12))
			return false;
		String defaultNumber11 = "00000000000";
		String defaultNumber12 = "000000000000";
		if (mobilePhoneNo.equals(defaultNumber11) || mobilePhoneNo.equals(defaultNumber12))
			return false;
		return result;
	}

	public static BaseInfo getRujukan(String bpjsNo, BaseInfo baseInfo, MemberInfo memberInfo) {

		BaseInfo data = new BaseInfo();
		List<CurrentDatetim> getCurrentDatetime = StoreDispanceSer.getCurrentDatetime();
		String norujuk = null;
		String tglkun;
		String diag;
		String tglrujuk;
		String tglrujukan;
		String ftgl;
		Date dateTglRujuk;
		boolean validrujukan = false;
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			DateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
			DateFormat df3 = new SimpleDateFormat("MMyy", Locale.ENGLISH);
			DateFormat df4 = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);

			String currenttime = getCurrentDatetime.get(0).getTanggal();
			Date serverDatetime = df2.parse(currenttime);
			tglrujuk = df.format(serverDatetime);
			dateTglRujuk = df.parse(tglrujuk);

			String ret = BpjsService.GetBpjsMemberRujukanNomorKartu(bpjsNo);
			Gson gson = new Gson();
			DataRujukan dataRujukan = gson.fromJson(ret, DataRujukan.class);
			Metadata metadata = dataRujukan.getMetadata();

			String ret2 = BpjsService.GetBpjsMemberSep(bpjsNo);
			GetSep getSep = gson.fromJson(ret2, GetSep.class);
			Metadata metadataSep = getSep.getMetadata();
			if (metadataSep.getCode().equals("200")) {
				data.setLastSep(getSep.getResponse().getList()[0]);
			}

			if (metadata.getCode().equals("200") && metadata.getMessage().equals("OK")) {

				Item item = dataRujukan.getResponse().getItem();
				norujuk = item.getNoKunjungan();
				tglrujuk = item.getTglKunjungan();

				dateTglRujuk = df.parse(tglrujuk);
				Calendar calTglexpired = Calendar.getInstance();
				calTglexpired.setTime(dateTglRujuk);
				// Validasi rujukan dalam waktu 3 bulan
				calTglexpired.add(Calendar.MONTH, 3);
				Date tglexpired = calTglexpired.getTime();

				tglkun = df.format(dateTglRujuk);
				diag = item.getDiagnosa().getKdDiag();
				if (serverDatetime.before(tglexpired)) {
					validrujukan = true;
				}
			}

			if (!validrujukan || !metadata.getCode().equals("200")) {
				tglrujuk = df.format(serverDatetime);
				norujuk = "6661";
				tglkun = df.format(serverDatetime);
				diag = "Z04.8";
			}

			String nor = norujuk;
			int tung = nor.length();
			String nru = "";
			if (tung == 4) {
				nru = norujuk;
			} else if (tung == 3) {
				nru = "0" + norujuk;
			} else if (tung == 2) {
				nru = "00" + norujuk;
			} else if (tung == 1) {
				nru = "000" + norujuk;
			} else {
				nru = "0000";
			}
			tglrujukan = tglrujuk + " " + df4.format(dateTglRujuk);
			ftgl = df3.format(dateTglRujuk);
			MemberInfo mbi = memberInfo;
			String ppkrujukan = mbi.getResponse().getPeserta().getProvUmum().getKdProvider();
			String klsrawat = mbi.getResponse().getPeserta().getKelasTanggungan().getKdKelas();
			String nr = "";
			if (tung == 19) {
				nr = nor;
			} else {
				nr = ppkrujukan + ftgl + "Y00" + nru;
			}
			data.setNomorRujukan(nr);
			data.setTanggalRujukan(tglrujukan);
			data.setPpkrujukan(ppkrujukan);
			data.setKlsrawat(klsrawat);

			data.setResponse(true);
			data.setDescription("Berhasil get rujukan");
		} catch (Exception e) {
			data.setResponse(false);
			data.setDescription("Gagal get rujukan | error : " + e.getMessage());
			e.fillInStackTrace();
			System.out.println(e.getMessage() + " | function : " + e.getStackTrace()[0].getMethodName());
		}

		return data;

	}

	public static BaseInfo getRujukan(String bpjsNo, BaseInfo baseInfo, BpjsInfoResponse memberInfo) {

		BaseInfo data = new BaseInfo();
		List<CurrentDatetim> getCurrentDatetime = StoreDispanceSer.getCurrentDatetime();
		String norujuk = null;
		String tglkun;
		String diag = null;
		String tglrujuk;
		String tglrujukan;
		String ftgl;
		String jnsPelayanan = null;
		String asalRujuk = null;
		Date dateTglRujuk;
		boolean validrujukan = false;
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			DateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
			DateFormat df3 = new SimpleDateFormat("MMyy", Locale.ENGLISH);
			DateFormat df4 = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);

			String currenttime = getCurrentDatetime.get(0).getTanggal();
			Date serverDatetime = df2.parse(currenttime);
			tglrujuk = df.format(serverDatetime);
			dateTglRujuk = df.parse(tglrujuk);

			String ret = BpjsService2.getRujukanbyNoka(bpjsNo);
			Gson gson = new Gson();
			BpjsRujukanResponse dataRujukan = gson.fromJson(ret, BpjsRujukanResponse.class);
			Metadata metadata = dataRujukan.getMetaData();

			// cari last sep (untuk fitur cetak ulang)
			if (false) {
				String ret2 = BpjsService.GetBpjsMemberSep(bpjsNo);
				GetSep getSep = gson.fromJson(ret2, GetSep.class);
				Metadata metadataSep = getSep.getMetadata();
				if (metadataSep.getCode().equals("200")) {
					data.setLastSep(getSep.getResponse().getList()[0]);
				}
			}

			if (metadata.getCode().equals("200") && metadata.getMessage().equals("OK")) {

				Rujukan item = dataRujukan.getResponse().getRujukan();
				norujuk = item.getNoKunjungan();
				tglrujuk = item.getTglKunjungan();

				dateTglRujuk = df.parse(tglrujuk);
				Calendar calTglexpired = Calendar.getInstance();
				calTglexpired.setTime(dateTglRujuk);
				// Validasi rujukan dalam waktu 1 bulan
				calTglexpired.add(Calendar.MONTH, 1);
				Date tglexpired = calTglexpired.getTime();

				tglkun = df.format(dateTglRujuk);
				diag = item.getDiagnosa().getKode();
				jnsPelayanan = item.getPelayanan().getKode();
				asalRujuk = item.getProvPerujuk().getKode();
				if (serverDatetime.before(tglexpired)) {
					validrujukan = true;
				}
			}

			if (!validrujukan || !metadata.getCode().equals("200")) {
				tglrujuk = df.format(serverDatetime);
				norujuk = "6661";
				tglkun = df.format(serverDatetime);
				diag = "Z04.8";
				jnsPelayanan = mJp;
				asalRujuk = mPpkpelayanan;
			}

			String nor = norujuk;
			int tung = nor.length();
			String nru = "";
			if (tung == 4) {
				nru = norujuk;
			} else if (tung == 3) {
				nru = "0" + norujuk;
			} else if (tung == 2) {
				nru = "00" + norujuk;
			} else if (tung == 1) {
				nru = "000" + norujuk;
			} else {
				nru = "0000";
			}
			tglrujukan = tglrujuk + " " + df4.format(dateTglRujuk);
			ftgl = df3.format(dateTglRujuk);
			BpjsInfoResponse mbi = memberInfo;
			String ppkrujukan = mbi.getResponse().getPeserta().getProvUmum().getKdProvider();
			String klsrawat = mbi.getResponse().getPeserta().getHakKelas().getKode();
			String nr = "";
			if (tung == 19) {
				nr = nor;
			} else {
				nr = ppkrujukan + ftgl + "Y00" + nru;
			}

			data.setAsalRujuk(asalRujuk);
			data.setJnsPelayanan(jnsPelayanan);
			data.setDiagAwal(diag);
			data.setNomorRujukan(nr);
			data.setTanggalRujukan(tglrujukan);
			data.setPpkrujukan(ppkrujukan);
			data.setKlsrawat(klsrawat);

			data.setResponse(true);
			data.setDescription("Berhasil get rujukan");
		} catch (Exception e) {
			data.setResponse(false);
			data.setDescription("Gagal get rujukan | error : " + e.getMessage());
			e.fillInStackTrace();
			System.out.println(e.getMessage() + " | function : " + e.getStackTrace()[0].getMethodName());
		}

		return data;

	}

	public static String cancelReg(CancelReg cancelReg) {
		try {

			String mrn = cancelReg.getMrn();
			String tgl_berobat = cancelReg.getTgl_berobat();
			String userid = cancelReg.getUserid();
			String visitid = cancelReg.getVisitid();
			String remarks = cancelReg.getRemarks();
			String cancelreason = cancelReg.getCancelreason();
			String noSep = cancelReg.getNoSep();

			if (!noSep.isEmpty()) {
				BpjsService2.hapusSep(noSep, mUser);
			}
			dataBpjsSer.getcancelQueue(mrn, tgl_berobat);
			dataBpjsSer.getcancelreg(userid, visitid, remarks, cancelreason);

		} catch (Exception e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage() + " | function : " + e.getStackTrace()[0].getMethodName());
		}
		return "OK";
	}

	public static DetailSep getSep(String baseno) {
		DetailSep data = new DetailSep();
		try {
			BaseInfo baseInfo = getBaseInfo(baseno);

			if (baseInfo.getDepartment().equals(mBpjs)) {
				SepList sepList = baseInfo.getLastSep();
				String ret2 = BpjsService.GetBpjsMemberDetailNoSep(sepList.getNoSEP());
				Gson gson = new Gson();
				data = gson.fromJson(ret2, DetailSep.class);
			}

		} catch (Exception e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage() + " | function : " + e.getStackTrace()[0].getMethodName());
		}
		return data;
	}

	public static RegKthis insertRegKthisBpjs(InsertRegBpjs insertRegBpjs) {
		RegKthis data = new RegKthis();
		try {

			String noBpjs = insertRegBpjs.getNoBpjs();
			String tglSep = insertRegBpjs.getTglSep();
			String tglRujukan = insertRegBpjs.getTglRujukan();
			String noRujukan = insertRegBpjs.getNoRujukan();
			String ppkRujukan = insertRegBpjs.getPpkRujukan();
			String catatan = insertRegBpjs.getCatatan();
			String poliTujuan = insertRegBpjs.getPoliTujuan();
			String klsRawat = insertRegBpjs.getKlsRawat();
			String mrn = insertRegBpjs.getMrn();
			String resourceMstrId = insertRegBpjs.getCareProviderId();
			String patient_name = insertRegBpjs.getPatientName();
			String tgl_berobat = insertRegBpjs.getTglBerobat();
			String PatientId = insertRegBpjs.getPatientId();
			String location = insertRegBpjs.getLocation();
			String group_id = insertRegBpjs.getGroupId();
			String userid = insertRegBpjs.getUserid();
			String source = insertRegBpjs.getSource();
			String lakaLantas = insertRegBpjs.getLakaLantas();
			String noHp = insertRegBpjs.getNoHP();
			String user = insertRegBpjs.getUser();
			String diagAwal = insertRegBpjs.getDiagAwal();
			String jnsPelayanan = insertRegBpjs.getJnsPelayanan();
			String asalRujuk = insertRegBpjs.getAsalRujuk();
			Boolean fakses = insertRegBpjs.getFakses();
			String message = "";

			// insert SEP
			data.setLevel(0);
			// String ret = BpjsService.CreateBpjsLaporanSep(noBpjs, tglSep, tglRujukan,
			// noRujukan, ppkRujukan, mPpkpelayanan, mJp, catatan, mDiagawal, poliTujuan,
			// klsRawat, mKla, mrn);
			String ret = "";
			if (fakses) {
				ret = BpjsService2.insertSep(noBpjs, jnsPelayanan, klsRawat, mrn, asalRujuk, tglRujukan, noRujukan,
						catatan, diagAwal, poliTujuan, noHp, user);
			} else {
				ret = BpjsService2.insertSep2(noBpjs, jnsPelayanan, klsRawat, mrn, asalRujuk, tglRujukan, noRujukan,
						catatan, diagAwal, poliTujuan, noHp, user);
			}

			Gson gson = new Gson();
			BpjsInsertSepResponse createSep = gson.fromJson(ret, BpjsInsertSepResponse.class);
			Metadata metadata = createSep.getMetaData();
			data.setDescription(metadata.getMessage());

			if (metadata.getCode().equals("200")) {
				data.setLevel(1);
				data.setSepResponse(createSep.getResponse().getSep());
				/*
				 * RETRIVE SEP ----- LEVEL 0
				 */

				String noSep = null;
				noSep = createSep.getResponse().getSep().getNoSep();

				System.out.println("No SEP : " + noSep);
				// get detail sep from bpjs (not used)
				if (false) {
					noSep = createSep.getResponse().getSep().getNoSep();
					String ret2 = BpjsService2.detailSep(noSep);
					BpjsSepDetailResponse detailResponse = gson.fromJson(ret2, BpjsSepDetailResponse.class);
					data.setRetSep(detailResponse);
				}

				/*
				 * INSERT QUEUE ----- LEVEL 1
				 */
				List<ListGetQueue> listQueue = QueueService.InsertQueue(resourceMstrId, patient_name, mrn, tgl_berobat,
						PatientId, location, group_id, source);

				if (listQueue.get(0) != null && listQueue.get(0).getResponse()) {
					data.setLevel(2);
					data.setListQueue(listQueue.get(0));
					String noantri = data.getListQueue().getQueue_no();

					data.setResponse(false);
					data.setDescription("Gagal insert kthis");

					/*
					 * INSERT KE KTHIS ----- LEVEL 2
					 */

					List<ListSelfReg> selfReg = insertKthisBpjs(mrn, resourceMstrId, noantri, userid, source);

					if (selfReg.get(0) != null) {
						ListSelfReg reg = selfReg.get(0);

						if (reg.isResponse()) {

							data.setResponse(true);
							data.setDescription("Berhasil");
							data.setListSelfReg(reg);

							RegData regdata = dataBpjsSer.GetRegData(reg.getREGN_NO());
							dataBpjsSer.getInsBrid2(mrn, noBpjs, noSep, reg.getVISITID(), resourceMstrId, noantri,
									userid, lakaLantas);
							data.setRegdata(regdata);

						} else {
							data.setResponse(false);
							data.setDescription("Gagal insert kthis , rollback ");
							dataBpjsSer.getcancelQueue(mrn, tgl_berobat);
							data.setListQueue(null);
							BpjsService2.hapusSep(noSep, mUser);
							data.setRetSep(null);
						}
					}
				} else {
					// CANCEL SEP
					data.setResponse(false);
					data.setDescription("Gagal insert queue atau Queue sudah ada");
					BpjsService2.hapusSep(noSep, mUser);
					data.setRetSep(null);
				}
			} else {
				message = "Gagal create SEP dari BPJS service, code = " + metadata.getCode() + ", message = "
						+ metadata.getMessage();
				data.setResponse(false);
				data.setDescription(message);
			}

		} catch (Exception e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage() + " | function : " + e.getStackTrace()[0].getMethodName());
		}

		return data;

	}

	private static RegKthis throwError(RegKthis currentView, String description) {
		RegKthis result = currentView;
		result.setResponse(false);
		result.setDescription(description);
		return result;
	}

	public static RegKthis insertRegKthisBpjs2(InsertRegBpjs insertRegBpjs) {
		RegKthis data = new RegKthis();
		try {

			String noBpjs = insertRegBpjs.getNoBpjs();
			String tglSep = insertRegBpjs.getTglSep();
			String tglRujukan = insertRegBpjs.getTglRujukan();
			String noRujukan = insertRegBpjs.getNoRujukan();
			String ppkRujukan = insertRegBpjs.getPpkRujukan();
			String catatan = insertRegBpjs.getCatatan();
			String poliTujuan = insertRegBpjs.getPoliTujuan();
			String klsRawat = insertRegBpjs.getKlsRawat();
			String mrn = insertRegBpjs.getMrn();
			String resourceMstrId = insertRegBpjs.getCareProviderId();
			String patient_name = insertRegBpjs.getPatientName();
			String tgl_berobat = insertRegBpjs.getTglBerobat();
			String PatientId = insertRegBpjs.getPatientId();
			String location = insertRegBpjs.getLocation();
			String group_id = insertRegBpjs.getGroupId();
			String userid = insertRegBpjs.getUserid();
			String source = insertRegBpjs.getSource();
			String lakaLantas = insertRegBpjs.getLakaLantas();
			String noHp = insertRegBpjs.getNoHP();
			String user = insertRegBpjs.getUser();
			String diagAwal = insertRegBpjs.getDiagAwal();
			String jnsPelayanan = insertRegBpjs.getJnsPelayanan();
			String asalRujuk = insertRegBpjs.getAsalRujuk();
			Boolean fakses = insertRegBpjs.getFakses();
			Boolean online = insertRegBpjs.getOnline();
			String onlineQueueNo = insertRegBpjs.getOnlineQueueNo();
			String message = "";
			String careProviderId = String.valueOf(CareProviderSer.getCareProviderIdByResourceMstrId(Long.valueOf(resourceMstrId)));

			// insert SEP
			data.setLevel(0);
			String ret = "";
			ret = "{ \"response\": { \"sep\": { \"noSep\": \"0902R00101140000004\", \"peserta\": { \"noKartu\": \"0000047622846\", \"nik\": null, \"nama\": \"HJ.UMRAH-HERRIYANRI\", \"pisa\": \"3\", \"sex\": \"P\", \"tglLahir\": \"1951-10-16 00:00:00\", \"tglCetakKartu\": \"2009-10-30 00:00:00\", \"provUmum\": { \"kdProvider\": \"10050201\", \"nmProvider\": \"CIPUTAT\", \"kdCabang\": null, \"nmCabang\": null }, \"jenisPeserta\": { \"kdJenisPeserta\": \"15\", \"nmJenisPeserta\": \"BUKAN PEKERJA LAIN-LAIN\" }, \"kelasTanggungan\": { \"kdKelas\": \"1\", \"nmKelas\": \"Kelas I\" } }, \"tglSep\": \"2014-01-02 00:00:00\", \"tglRujukan\": \"2014-01-02 00:00:00\", \"tglPulang\": \"2014-01-02 00:00:00\", \"noRujukan\": \"\", \"provRujukan\": { \"kdProvider\": \"09020100\", \"nmProvider\": \"KEC TEBET\", \"kdCabang\": null, \"nmCabang\": null }, \"provPelayanan\": { \"kdProvider\": \"0902R001\", \"nmProvider\": \"RSUP FATMAWATI\", \"kdCabang\": null, \"nmCabang\": null }, \"jnsPelayanan\": \"Rawat Jalan\", \"catatan\": \"\", \"diagAwal\": { \"kdDiag\": \"T843\", \"nmDiag\": \"Mechanical comp other bone devices implants & grafts\" }, \"poliTujuan\": { \"kdPoli\": \"BED\", \"nmPoli\": \"Poli Bedah\" }, \"klsRawat\": { \"kdKelas\": \"3\", \"nmKelas\": \"Kelas III\" }, \"statSep\": { \"kdStatSep\": \"40\", \"nmStatSep\": \"40_Proses_Cabang\" }, \"byTagihan\": 378878 } }, \"metaData\": { \"message\": \"OK\", \"code\": 200 } } ";

			InsertSepMtregParam param = new InsertSepMtregParam(noBpjs, jnsPelayanan, klsRawat, mrn, asalRujuk,
					tglRujukan, noRujukan, catatan, diagAwal, poliTujuan, noHp, user,careProviderId);
			
			int tryInsertSepCount = 9;
			int tryIndex = 1;
			ret = BpjsService2.insertSepMtreg(param, fakses);
			System.out.println("Request SEP : " + (tryIndex));
			for(tryIndex = 1; tryIndex<=tryInsertSepCount; tryIndex++) {
				if(ret == null) ret = BpjsService2.insertSepMtreg(param, fakses);
				else if(ret.equals("")) ret = BpjsService2.insertSepMtreg(param, fakses);
				else if(ret.equals("null"))ret = BpjsService2.insertSepMtreg(param, fakses);
				else break;
				System.out.println("Request SEP : " + tryIndex);
			}
			if(tryIndex > tryInsertSepCount)return throwError(data, "Gagal mencoba create SEP sebanyak "+ (tryInsertSepCount + 1) +"x, silahkan coba lagi atau hubungi counter registrasi.");

			Gson gson = new Gson();
			BpjsInsertSepResponse createSep = gson.fromJson(ret, BpjsInsertSepResponse.class);
			if(createSep == null) return throwError(data, "Gagal create SEP dari BPJS service, silahkan coba lagi atau hubungi counter registrasi.");
			Metadata metadata = createSep.getMetaData();
			data.setDescription(metadata.getMessage());
			
			if(metadata == null) return throwError(data, "Gagal create SEP dari BPJS service, silahkan coba lagi atau hubungi counter registrasi.");
			if (metadata.getCode().equals("200")) {
				data.setLevel(1);
				data.setSepResponse(createSep.getResponse().getSep());
				/*
				 * RETRIVE SEP ----- LEVEL 0
				 */

				String noSep = null;
				noSep = createSep.getResponse().getSep().getNoSep();

				System.out.println("No SEP : " + noSep);
				/*
				 * INSERT QUEUE ----- LEVEL 1
				 */
				List<ListGetQueue> listQueue = null;
				if (online) {
					GetQueueParam getQueueParam = new GetQueueParam();
					getQueueParam.setMrn(mrn);
					getQueueParam.setOnlineQueueNo(onlineQueueNo);
					getQueueParam.setResourceMstrId(resourceMstrId);
					String date = formatedSqlDate(tgl_berobat);
					if (TextUtils.isEmpty(date))
						return throwError(data, "Gagal format date queue.");
					getQueueParam.setTgl_berobat(date);
					getQueueParam.setUserId(user);
					getQueueParam.setLocation(location);
					getQueueParam.setPatientName(patient_name);
					listQueue = GetQueue(getQueueParam);
				} else {
					InsertQueueRequest insertQueueRequest = buildInsertQueueRequest(insertRegBpjs);
					if (insertQueueRequest == null)
						return throwError(data, "Gagal insert queue (Generate View)");
					else if (!insertQueueRequest.getSuccess()) {
						return throwError(data, insertQueueRequest.getMessage());
					}
					if (!validateInsertQueueData(insertQueueRequest))
						return throwError(data, queueMessage);

					listQueue = InsertQueue(insertQueueRequest, mrn, user, patient_name, source);
				}
				// List<ListGetQueue> listQueue = null;

				if (listQueue != null) {
					if (!listQueue.isEmpty()) {
						if (listQueue.get(0) != null && listQueue.get(0).getResponse()) {
							data.setLevel(2);
							data.setListQueue(listQueue.get(0));
							String noantri = data.getListQueue().getQueue_no();

							data.setResponse(false);
							data.setDescription("Gagal insert kthis");

							/*
							 * INSERT KE KTHIS ----- LEVEL 2
							 */

							List<ListSelfReg> selfReg = insertKthisBpjs(mrn, resourceMstrId, noantri, userid, source);

							if (selfReg.get(0) != null) {
								ListSelfReg reg = selfReg.get(0);

								if (reg.isResponse()) {

									data.setResponse(true);
									data.setDescription("Berhasil");
									data.setListSelfReg(reg);
									RegData regdata = dataBpjsSer.GetRegData(reg.getREGN_NO());
									data.setRegdata(regdata);
									
									if(BPJSVersion.isVersion(BPJSVersion.Version1_0, paramBpjsVersion)) {
										dataBpjsSer.getInsBrid2(mrn, noBpjs, noSep, reg.getVISITID(), resourceMstrId,
												noantri, userid, lakaLantas);
										String nmProviderRujukan = TextUtils.isEmpty(createSep.getResponse().getSep().getPpkPerujuk()) ?  "Murni Teguh" : createSep.getResponse().getSep().getPpkPerujuk();
										insertRujukanBpjs( mrn, noBpjs, nmProviderRujukan, new Timestamp(BpjsIntegration.dateFormat.parse(tglRujukan).getTime()), noRujukan, BigDecimal.valueOf(Long.valueOf(userid)));
									}
									else if(BPJSVersion.isVersion(BPJSVersion.Version1_1, paramBpjsVersion)){
										
										ResponseRujukan rujukan = BpjsIntegration.Rujukan.getRujukanTerakhirFromList(noBpjs);
										if(BpjsIntegration.Rujukan.validateRujukan
												(rujukan,
												 new BpjsIntegration.IRujukan.IValidateResult() {
											
													@Override
													public void onError(String code, String message) {
														System.out.println("Self Registration PostRegistrationSEP Failed : " + code + " " + message);
													}
												 })
										  ) {
											SepBpjsData sepBpjsData = new SepBpjsData(
													BigDecimal.valueOf(Long.valueOf(reg.getVISITID())),
													noSep,
													noantri, 
													BigDecimal.valueOf(Long.valueOf(careProviderId)), 
													fakses ? "1" : "2", 
													BpjsService2.getLastControlletter(mrn), lakaLantas,
													null,
													null, null, "", null);
											String tglKunjungan = rujukan.getResponse().getRujukan().getTglKunjungan();
											RujukanData rujukanData = new RujukanData(
													rujukan.getResponse().getRujukan().getProvPerujuk().getNama(),
													TextUtils.isEmpty(tglKunjungan) ? null : new Timestamp(BpjsIntegration.dateFormat.parse(tglKunjungan).getTime()),
													rujukan.getResponse().getRujukan().getNoKunjungan());

											PostRegisterData postSepBpjs = new PostRegisterData(
													BigDecimal.valueOf(Long.valueOf(user)),
													BigDecimal.valueOf(Long.valueOf(param.getMrn())), 
													noBpjs, 
													sepBpjsData, 
													rujukanData);
											BpjsIntegration.SEP.PostRegister(postSepBpjs);
										}
									}

								} else {
									data.setResponse(false);
									data.setDescription("Gagal insert kthis , rollback ");
									dataBpjsSer.getcancelQueue(mrn, tgl_berobat);
									data.setListQueue(null);
									BpjsService2.hapusSep(noSep, mUser);
									data.setRetSep(null);
								}
							}
						} else {
							// CANCEL SEP
							data.setResponse(false);
							data.setDescription(queueMessage);
							BpjsService2.hapusSep(noSep, mUser);
							data.setRetSep(null);
						}
					} else {
						// CANCEL SEP
						data.setResponse(false);
						data.setDescription(queueMessage);
						BpjsService2.hapusSep(noSep, mUser);
						data.setRetSep(null);
					}
				} else {
					// CANCEL SEP
					data.setResponse(false);
					data.setDescription("Gagal insert queue atau Queue sudah ada");
					BpjsService2.hapusSep(noSep, mUser);
					data.setRetSep(null);
				}

			} else {
				message = metadata.getMessage();
				data.setResponse(false);
				data.setDescription(message);
			}

		} catch (Exception e) {
			String fullTrack = e.getMessage() + " | function : " + e.getStackTrace()[0].getMethodName();
			e.fillInStackTrace();
			System.out.println(fullTrack);
			data.setDescription(fullTrack);
		}

		return data;

	}
	
	private static void insertRujukanBpjs(String mrn, String bpjsNo, String nmProviderRujukan, Timestamp tglRujukan, String nmrRujukan, BigDecimal userId) throws SQLException, Exception {
//		String nmProviderRujukan, Timestamp tglKunjungan, String, String mrn, String noKa, BigDecimal userId) throws SQLException, Exception {
			String query = "INSERT INTO BPJS_RUJUKAN(BPJS_RUJUKAN_ID, " + 
					"CARD_NO, " + 
					"BPJS_NO , " + 
					"ASAL_FKTP,  " + 
					"TGL_RUJUKAN, " + 
					"NOMOR_RUJUKAN,  " + 
					"CREATED_BY, " + 
					"CREATED_AT,   " + 
					"MODIFIED_BY, " + 
					"MODIFIED_AT, " + 
					"DEFUNCT_IND) VALUES(pgSYSFUNC.fxGenPrimaryKey, ?,?,?,?,?,?,SYSDATE,?,SYSDATE,'N')";
					DbConnection.executeQuery(DbConnection.KTHIS, query, new Object[] {
							mrn, bpjsNo,
							nmProviderRujukan,
							tglRujukan,
							nmrRujukan,
							userId,
							userId}, 60);
		}

	private static String formatedSqlDate(String dateString) {
		String result = "";
		try {
			DateFormat inFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			DateFormat outFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
			Date date = inFormat.parse(dateString);
			// result = outFormat.format(inFormat.parse(dateString));
			result = outFormat.format(date);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			result = "";
		}
		return result;
	}

	private static InsertQueueRequest buildInsertQueueRequest(InsertRegBpjs insertRegBpjs) {
		InsertQueueRequest result = new InsertQueueRequest();
		RequestTicket data = new RequestTicket();
		String date = formatedSqlDate(insertRegBpjs.getTglBerobat());
		if (TextUtils.isEmpty(date)) {
			result.success = false;
			result.message = "Format tanggal Queue tidak sesuai.";
		} else {
			String no_doctor = insertRegBpjs.getNomorDokter();
			String no_subspecialis = insertRegBpjs.getNomorSpesialis();
			String resourceMstrId = insertRegBpjs.getCareProviderId();
			String location = insertRegBpjs.getLocation();
			data.setDate(date);
			data.setNo_doctor(no_doctor);
			data.setNo_subspecialis(no_subspecialis);
			result.date = date;
			result.data = data;
			result.resourceMstrId = resourceMstrId;
			result.location = location;
			result.success = true;
			result.message = "OK";
		}
		return result;
	}

	private static InsertQueueRequest buildInsertQueueRequest(InsertRegGeneral insertRegGeneral) {
		InsertQueueRequest result = new InsertQueueRequest();
		RequestTicket data = new RequestTicket();
		String date = formatedSqlDate(insertRegGeneral.getTgl_berobat());
		if (TextUtils.isEmpty(date)) {
			result.success = false;
			result.message = "Format tanggal Queue tidak sesuai.";
		} else {
			String no_doctor = insertRegGeneral.getNomorDokter();
			String no_subspecialis = insertRegGeneral.getNomorSpesialis();
			String resourceMstrId = insertRegGeneral.getCareProviderId();
			String location = insertRegGeneral.getLocation();
			data.setDate(date);
			data.setNo_doctor(no_doctor);
			data.setNo_subspecialis(no_subspecialis);
			result.date = date;
			result.data = data;
			result.resourceMstrId = resourceMstrId;
			result.location = location;
			result.success = true;
		}
		return result;
	}

	private static String queueMessage;

	private static Boolean validateInsertQueueData(InsertQueueRequest param) {
		if (param == null) {
			queueMessage = "Parameter InsertQueueRequest antrian kosong.";
			return false;
		}
		String resourceMstrId = param.getResourceMstrId();
		String location = param.getLocation();
		String tanggal = param.getDate();
		RequestTicket data = param.getData();
		String no_subspecialis = data.getNo_subspecialis();
		String no_doctor = data.getNo_doctor();

		Boolean result = true;
		if (TextUtils.isEmpty(no_doctor)) {
			queueMessage = "Nomor antrian dokter kosong.";
			return false;
		}
		if (TextUtils.isEmpty(no_subspecialis)) {
			queueMessage = "Nomor antrian spesialis kosong.";
			return false;
		}
		if (TextUtils.isEmpty(tanggal)) {
			queueMessage = "Tanggal antrian tidak valid.";
			return false;
		}
		if (TextUtils.isEmpty(location)) {
			queueMessage = "Lokasi antrian tidak valid.";
			return false;
		}
		if (TextUtils.isEmpty(resourceMstrId)) {
			queueMessage = "Dokter tidak dipilih.";
			return false;
		}
		return result;
	}

	private static List<ListGetQueue> InsertQueue(InsertQueueRequest param, String cardNo, String createdBy,
			String patientName, String source) {
		queueMessage = "";
		List<ListGetQueue> result = new ArrayList<ListGetQueue>();
		String resourceMstrId = param.getResourceMstrId();
		String location = param.getLocation();
		String tanggal = param.getDate();
		RequestTicket data = param.getData();
		String no_subspecialis = data.getNo_subspecialis();
		String no_doctor = data.getNo_doctor();

		ResponseStatus ticketId = QueueService.RequestIDTicket(resourceMstrId, location, tanggal, source, patientName,
				cardNo, QueueServiceBiz.QUEUE_TYPE_REGULER);
		if (ticketId == null)
			return result;
		queueMessage = ticketId.getMessage();
		if (!ticketId.isSuccess())
			return result;
		String queueId = ticketId.getMessage();
		// ResponseStatus responseRequestTicket = null;
		ResponseStatus ticketData = QueueService.RequestTicket(location, no_subspecialis, no_doctor, queueId, tanggal);
		if (ticketData == null)
			return result;
		queueMessage = ticketId.getMessage();
		if (!ticketData.isSuccess())
			return result;
		String queueNo = ticketData.getMessage();
		ListGetQueue item = new ListGetQueue();
		// 1.
		item.setCard_no(cardNo);
		// 2.
		item.setCreated_at(tanggal);
		// 3.
		item.setCreated_by(createdBy);
		item.setDeleted("N");
		// 4.
		item.setDescription("");
		// 5.
		item.setPatient_name(patientName);
		// 6.
		item.setQueue_date(tanggal);
		// 7.
		item.setQueue_no(queueNo);
		item.setQueue_id(queueId);
		// 8.
		item.setResponse(true);
		// 9.
		item.setUpdated_at(tanggal);
		item.setResourcemstr_id(resourceMstrId);
		// item.setUpdated_at(updated_at);
		// item.setUpdated_by(updated_by);
		result.add(item);
		queueMessage = "Berhasil membuat antrian.";
		return result;
	}

	private static class GetQueueParam {
		private String location;
		private String tgl_berobat;
		private String onlineQueueNo;
		private String mrn;
		private String userId;
		private String resourceMstrId;
		private String patientName;

		public String getPatientName() {
			return patientName;
		}

		public void setPatientName(String patientName) {
			this.patientName = patientName;
		}

		public String getResourceMstrId() {
			return resourceMstrId;
		}

		public void setResourceMstrId(String resourceMstrId) {
			this.resourceMstrId = resourceMstrId;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public String getTgl_berobat() {
			return tgl_berobat;
		}

		public void setTgl_berobat(String tgl_berobat) {
			this.tgl_berobat = tgl_berobat;
		}

		public String getOnlineQueueNo() {
			return onlineQueueNo;
		}

		public void setOnlineQueueNo(String onlineQueueNo) {
			this.onlineQueueNo = onlineQueueNo;
		}

		public String getMrn() {
			return mrn;
		}

		public void setMrn(String mrn) {
			this.mrn = mrn;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

	}

	public static RegKthis insertRegKthisGeneral(InsertRegGeneral insertRegGeneral) {
		RegKthis data = new RegKthis();

		try {

			String careProviderId = insertRegGeneral.getCareProviderId();
			String patient_name = insertRegGeneral.getPatient_name();
			String tgl_berobat = insertRegGeneral.getTgl_berobat();
			String PatientId = insertRegGeneral.getPatientId();
			String location = insertRegGeneral.getLocation();
			String group_id = insertRegGeneral.getGroup_id();
			String mrn = insertRegGeneral.getMrn();
			String userid = insertRegGeneral.getUserid();
			String source = insertRegGeneral.getSource();

			/*
			 * INSERT QUEUE ----- LEVEL 1
			 */
			data.setLevel(1);
			List<ListGetQueue> listQueue = QueueService.InsertQueue(careProviderId, patient_name, mrn, tgl_berobat,
					PatientId, location, group_id, source);
			if (listQueue.get(0) != null && listQueue.get(0).getResponse()) {
				data.setLevel(2);
				data.setListQueue(listQueue.get(0));
				String noantri = data.getListQueue().getQueue_no();
				data.setResponse(false);
				data.setDescription("Gagal insert kthis");
				/*
				 * INSERT KE KTHIS ----- LEVEL 2
				 */
				List<ListSelfReg> selfReg = insertKthisGeneral(mrn, careProviderId, noantri, userid, source);

				if (selfReg.get(0) != null) {
					ListSelfReg reg = selfReg.get(0);

					if (reg.isResponse()) {
						data.setResponse(true);
						data.setDescription("Berhasil");
						data.setListSelfReg(reg);

						RegData regdata = dataBpjsSer.GetRegData(reg.getREGN_NO());
						data.setRegdata(regdata);
					} else {
						data.setResponse(false);
						data.setDescription("Gagal insert kthis , rollback ");
						dataBpjsSer.getcancelQueue(mrn, tgl_berobat);
						data.setListQueue(null);
					}
				}
			} else {
				data.setResponse(false);
				data.setDescription("Gagal insert queue atau Queue sudah ada");
			}

		} catch (Exception e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage() + " | function : " + e.getStackTrace()[0].getMethodName());
		}
		return data;
	}

	public static RegKthis insertRegKthisGeneral2(InsertRegGeneral insertRegGeneral) {
		RegKthis data = new RegKthis();
		try {
			String resourceMstrId = insertRegGeneral.getCareProviderId();
			String patient_name = insertRegGeneral.getPatient_name();
			String tgl_berobat = insertRegGeneral.getTgl_berobat();
			String PatientId = insertRegGeneral.getPatientId();
			String location = insertRegGeneral.getLocation();
			String group_id = insertRegGeneral.getGroup_id();
			String mrn = insertRegGeneral.getMrn();
			String userid = insertRegGeneral.getUserid();
			String source = insertRegGeneral.getSource();
			Boolean online = insertRegGeneral.getOnline();
			String onlineQueueNo = insertRegGeneral.getOnlineQueueNo();

			/*
			 * INSERT QUEUE ----- LEVEL 1
			 */
			data.setLevel(1);
			List<ListGetQueue> listQueue = null;
			if (online) {
				GetQueueParam getQueueParam = new GetQueueParam();
				getQueueParam.setOnlineQueueNo(onlineQueueNo);
				getQueueParam.setResourceMstrId(resourceMstrId);
				String date = formatedSqlDate(tgl_berobat);
				if (TextUtils.isEmpty(date))
					return throwError(data, "Gagal format date queue.");
				getQueueParam.setTgl_berobat(date);
				getQueueParam.setUserId(userid);
				getQueueParam.setLocation(location);
				getQueueParam.setPatientName(patient_name);
				listQueue = GetQueue(getQueueParam);
			} else {
				InsertQueueRequest insertQueueRequest = buildInsertQueueRequest(insertRegGeneral);
				if (insertQueueRequest == null)
					return throwError(data, "Gagal insert queue (Generate View)");
				else if (!insertQueueRequest.getSuccess()) {
					return throwError(data, insertQueueRequest.getMessage());
				}
				if (!validateInsertQueueData(insertQueueRequest))
					return throwError(data, queueMessage);
				listQueue = InsertQueue(insertQueueRequest, mrn, userid, patient_name, source);
			}

			if (listQueue != null) {
				if (!listQueue.isEmpty()) {
					if (listQueue.get(0) != null && listQueue.get(0).getResponse()) {
						data.setLevel(2);
						data.setListQueue(listQueue.get(0));
						String noantri = data.getListQueue().getQueue_no();
						data.setResponse(false);
						data.setDescription("Gagal insert kthis");
						/*
						 * INSERT KE KTHIS ----- LEVEL 2
						 */
						List<ListSelfReg> selfReg = insertKthisGeneral(mrn, resourceMstrId, noantri, userid, source);

						if (selfReg.get(0) != null) {
							ListSelfReg reg = selfReg.get(0);

							if (reg.isResponse()) {
								data.setResponse(true);
								data.setDescription("Berhasil");
								data.setListSelfReg(reg);

								RegData regdata = dataBpjsSer.GetRegData(reg.getREGN_NO());
								data.setRegdata(regdata);
							} else {
								data.setResponse(false);
								data.setDescription("Gagal insert kthis , rollback ");
								dataBpjsSer.getcancelQueue(mrn, tgl_berobat);
								data.setListQueue(null);
							}
						}
					} else {
						data.setResponse(false);
						data.setDescription(queueMessage);
					}
				} else {
					data.setResponse(false);
					data.setDescription(queueMessage);
				}
			} else {
				data.setResponse(false);
				data.setDescription("Gagal insert queue atau Queue sudah ada");
			}

		} catch (Exception e) {
			String fullTrack = e.getMessage() + " | function : " + e.getStackTrace()[0].getMethodName();
			e.fillInStackTrace();
			System.out.println(fullTrack);
			data.setDescription(fullTrack);
		}
		return data;
	}

	private static List<ListGetQueue> GetQueue(GetQueueParam param) {
		List<ListGetQueue> result = new ArrayList<ListGetQueue>();
		;
		String location = param.getLocation();
		String tanggal = param.getTgl_berobat();
		String cardNo = param.getMrn();
		String queueNo = param.getOnlineQueueNo();
		String patientName = param.getPatientName();
		ListGetQueue item = new ListGetQueue();
		QueueDetails ticketData = QueueService.QueueDetails(queueNo, location, tanggal);
		if (ticketData == null)
			return result;
		String createdBy = ticketData.getCreated_by();
		String queueId = ticketData.getQueue_id();
		String resourceMstrId = ticketData.getResourcemstr();
		// 1.
		item.setCard_no(cardNo);
		// 2.
		item.setCreated_at(tanggal);
		// 3.
		item.setCreated_by(createdBy);
		item.setDeleted("N");
		// 4.
		item.setDescription("");
		// 5.
		item.setPatient_name(patientName);
		// 6.
		item.setQueue_date(tanggal);
		// 7.
		item.setQueue_no(queueNo);
		item.setQueue_id(queueId);
		// 8.
		item.setResponse(true);
		// 9.
		item.setUpdated_at(tanggal);
		item.setResourcemstr_id(resourceMstrId);
		// item.setUpdated_at(updated_at);
		// item.setUpdated_by(updated_by);
		result.add(item);
		return result;
	}

	public static List<ListSelfReg> insertKthisBpjs(String cardno, String careproviderid, String noantri, String userid,
			String source) {
		// TODO Auto-generated method stub
		List<ListSelfReg> all = new ArrayList<ListSelfReg>();
		String sequenceStatus = null;
		String patid = "";
		String stkid = "";
		String patcal = "";
		String lokasi = "336581436";
		String prk = "";
		String gen = "";
		String prk2 = "";
		String prk3 = "";
		String prk4 = "";
		String prk5 = "";
		String prk6 = "";
		String prk7 = "";
		String prk8 = "";
		String ed = "";
		String qn = "";
		String qn2 = "";

		String qr1 = "SELECT RM.RESOURCEMSTR_ID, RM.CAREPROVIDER_ID, RM.SUBSPECIALTYMSTR_ID, RM.RESOURCE_NAME, RM.REGISTRATION_TYPE, RM.RESOURCE_QUEUE_IND, "
				+ "RM.RESOURCE_CHECKOUT_IND FROM RESOURCEMSTR RM WHERE RM.RESOURCEMSTR_ID= ? AND RM.DEFUNCT_IND = 'N'";
		try {
			List list = DbConnection.executeReader(DbConnection.KTHIS, qr1, new Object[] { careproviderid });

			for (int a = 0; a < list.size(); a++) {
				HashMap row = (HashMap) list.get(a);
				ListSelfReg lsg = new ListSelfReg();
				lsg.setRESOURCEMSTR_ID(row.get("RESOURCEMSTR_ID").toString());
				String resourceid = row.get("RESOURCEMSTR_ID").toString();
				lsg.setCAREPROVIDER_ID(row.get("CAREPROVIDER_ID").toString());
				String careid = row.get("CAREPROVIDER_ID").toString();
				lsg.setSUBSPECIALTYMSTR_ID(row.get("SUBSPECIALTYMSTR_ID").toString());
				String subid = row.get("SUBSPECIALTYMSTR_ID").toString();
				lsg.setRESOURCE_NAME(row.get("RESOURCE_NAME").toString());
				lsg.setREGISTRATION_TYPE(row.get("REGISTRATION_TYPE").toString());
				String regtyp = row.get("REGISTRATION_TYPE").toString();
				lsg.setRESOURCE_QUEUE_IND(row.get("RESOURCE_QUEUE_IND").toString());
				String rqi = row.get("RESOURCE_QUEUE_IND").toString();
				lsg.setRESOURCE_CHECKOUT_IND(row.get("RESOURCE_CHECKOUT_IND").toString());
				String rci = row.get("RESOURCE_CHECKOUT_IND").toString();

				String qr2 = "SELECT PS.PERSON_ID, PT.PATIENT_ID, CD.CARD_NO, PS.PERSON_NAME, pgCOMMON.fxGetCodeDesc(PS.SEX) as sex, "
						+ "TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)||' Tahun ' || "
						+ "TRUNC(months_between(sysdate,PS.BIRTH_DATE)-12*trunc(months_between(sysdate,PS.BIRTH_DATE)/12)) || ' Bulan' as AGE, "
						+ "PS.BIRTH_DATE, PS.STAKEHOLDER_ID, PT.PATIENT_CLASS FROM PERSON PS, PATIENT PT, CARD CD WHERE PT.PERSON_ID = PS.PERSON_ID AND "
						+ "CD.PERSON_ID = PS.PERSON_ID AND CD.CARD_NO = ?";
				List list2 = DbConnection.executeReader(DbConnection.KTHIS, qr2, new Object[] { cardno });
				for (int b = 0; b < list2.size(); b++) {
					HashMap row2 = (HashMap) list2.get(b);
					lsg.setPERSON_ID(row2.get("PERSON_ID").toString());
					lsg.setPATIENT_ID(row2.get("PATIENT_ID").toString());
					patid = row2.get("PATIENT_ID").toString();
					lsg.setCARD_NO(row2.get("CARD_NO").toString());
					lsg.setPERSON_NAME(row2.get("PERSON_NAME").toString());
					lsg.setSex(row2.get("SEX").toString());
					lsg.setAGE(row2.get("AGE").toString());
					lsg.setBIRTH_DATE(row2.get("BIRTH_DATE").toString());
					;
					lsg.setSTAKEHOLDER_ID(row2.get("STAKEHOLDER_ID").toString());
					stkid = row2.get("STAKEHOLDER_ID").toString();
					lsg.setPATIENT_CLASS(row2.get("PATIENT_CLASS").toString());
					patcal = "PTC114";
				}
				String user = userid;

				String qr3 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY FROM DUAL";
				String primarykey = DbConnection.executeScalar(DbConnection.KTHIS, qr3, new Object[] {}).toString();
				lsg.setVISITID(primarykey);
				prk = primarykey;

				if (source.equals(MobileBiz.MOBILE_SOURCE) || source.equals(MobileBiz.WEB_SOURCE)) {
					sequenceStatus = "2";
				} else {
					sequenceStatus = "1";
				}

				String qr4 = "INSERT INTO VISIT (VISIT_ID, PATIENT_ID, SUBSPECIALTYMSTR_ID, ADMITTED_AT, ADMITTED_BY, LAST_UPDATED_BY, ADMISSION_DATETIME, "
						+ "LAST_UPDATED_DATETIME, SPECIAL_CARE_IND, HEALTH_BOOK_FEE_IND, CARD_FEE_IND, PATIENT_TYPE, PATIENT_CLASS, VISIT_TYPE, ADMIT_STATUS, ADMIT_CLASS, "
						+ "ADMITTING_SOURCE, VISIT_NO, LAST_VISIT_NO, IPT_VISIT_TO_DATE, OPT_VISIT_TO_DATE, CONSULT_STATUS, MA_TYPE, ADMISSION_TIMES, ENTITYMSTR_ID, "
						+ "IN_PATH_IND, QUEUE_NO, QUEUE_SEQUENCE, SOURCE_TYPE) VALUES (?, ?, ?, ?, ?, ?, (SELECT sysdate from DUAL), "
						+ "(SELECT sysdate from DUAL), 'N', 'N', 'N', 'PTY2', ?, 'VST1', 'AST1', 'ACL1', 'ADSNORM', "
						+ "(SELECT pgSYSFUNC.fxGenSeqNo('VISIT_NO_OP',?,'PTY2') FROM DUAL), "
						+ "(SELECT MAX(V.VISIT_NO)FROM VISIT V WHERE V.PATIENT_ID = ?),0,0,'CNT1','MATN',1,1,'N',?, ?,?)";
				Object[] input = { prk, patid, subid, lokasi, user, user, patcal, user, patid, noantri, sequenceStatus,
						source };
				DbConnection.executeQuery(DbConnection.KTHIS, qr4, input);

				String qr5 = "SELECT VISIT_NO FROM VISIT WHERE VISIT_ID = ?";
				List list5 = DbConnection.executeReader(DbConnection.KTHIS, qr5, new Object[] { prk });
				for (int d = 0; d < list5.size(); d++) {
					HashMap row5 = (HashMap) list5.get(d);
					lsg.setLAST_VISIT_NO(row5.get("VISIT_NO").toString());
					gen = row5.get("VISIT_NO").toString();
				}

				String qr6 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY2 FROM DUAL";
				String primarykey2 = DbConnection.executeScalar(DbConnection.KTHIS, qr6, new Object[] {}).toString();
				lsg.setPRIMARYKEY2(primarykey2);
				prk2 = primarykey2;

				String qr7 = "INSERT INTO PATIENTTYPECLASSHISTORY(PATIENTTYPECLASSHISTORY_ID, VISIT_ID, NEW_PATIENT_TYPE, NEW_PATIENT_CLASS, START_DATETIME, CREATED_BY, "
						+ "CREATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME ) VALUES(?, ?, 'PTY2', ?, (SELECT sysdate from DUAL), "
						+ "?, (SELECT sysdate from DUAL), ?, (SELECT sysdate from DUAL) )";
				Object[] input2 = { prk2, prk, patcal, user, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr7, input2);

				String qr8 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY3 FROM DUAL";
				String primarykey3 = DbConnection.executeScalar(DbConnection.KTHIS, qr8, new Object[] {}).toString();
				lsg.setPRIMARYKEY3(primarykey3);
				prk3 = primarykey3;

				String qr9 = "INSERT INTO VISITREGNTYPE (VISITREGNTYPE_ID, VISIT_ID, REGISTRATION_TYPE, RESOURCEMSTR_ID, CREATED_BY, CREATED_DATETIME, DEFUNCT_IND,  "
						+ "LAST_UPDATED_BY, LAST_UPDATED_DATETIME, CAREPROVIDER_ID, SUBSPECIALTYMSTR_ID, CREATED_AT, SESSIONMSTR_ID, REGN_NO, REGISTRATION_METHOD, "
						+ "EFFICTIVE_DATETIME ) VALUES (?, ?, ?, ?, ?, (SELECT sysdate from DUAL), 'N', ?, "
						+ "(SELECT sysdate from DUAL), ?, ?, ?, '1', (SELECT pgSYSFUNC.fxGenSeqNo('REGN_NO',?,NULL) FROM DUAL), "
						+ "'RGM1', (SELECT sysdate from DUAL))";
				Object[] input3 = { prk3, prk, regtyp, resourceid, user, user, careid, subid, lokasi, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr9, input3);

				String qr10 = "select REGN_NO from VISITREGNTYPE where VISITREGNTYPE_ID= ?";
				String regn_no = DbConnection.executeScalar(DbConnection.KTHIS, qr10, new Object[] { prk3 }).toString();
				lsg.setREGN_NO(regn_no);

				String qr11 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY4 FROM DUAL";
				String primarykey4 = DbConnection.executeScalar(DbConnection.KTHIS, qr11, new Object[] {}).toString();
				lsg.setPRIMARYKEY4(primarykey4);
				prk4 = primarykey4;

				String qr12 = "INSERT INTO REGNVISITACTIVATION (REGNVISITACTIVATION_ID, VISITREGNTYPE_ID, VISIT_ID, CREATED_BY, CREATED_DATETIME, "
						+ "CREATED_LOCATIONMSTR_ID ) VALUES (?, ?, ?, ?, (SELECT sysdate from DUAL), ?)";

				Object[] input4 = { prk4, prk3, prk, user, lokasi };
				DbConnection.executeQuery(DbConnection.KTHIS, qr12, input4);

				String qr13 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY5 FROM DUAL";
				String primarykey5 = DbConnection.executeScalar(DbConnection.KTHIS, qr13, new Object[] {}).toString();
				lsg.setPRIMARYKEY5(primarykey5);
				prk5 = primarykey5;

				String qr14 = "INSERT INTO VISITSUBSPECIALTYHISTORY (VISITSUBSPECIALTYHISTORY_ID, SUBSPECIALTYMSTR_ID, VISIT_ID, LAST_UPDATED_BY, EFFECTIVE_DATETIME, "
						+ "LAST_UPDATED_DATETIME, DEFUNCT_IND) VALUES (?, ?, ?, ?, (SELECT sysdate from DUAL), (SELECT sysdate from DUAL), 'N')";
				Object[] input5 = { prk5, subid, prk, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr14, input5);

				String qr15 = "SELECT TO_CHAR(EFFECTIVE_DATETIME,'ddmmyyyy') as efek_date FROM VISITSUBSPECIALTYHISTORY WHERE VISITSUBSPECIALTYHISTORY_ID=?";
				List list15 = DbConnection.executeReader(DbConnection.KTHIS, qr15, new Object[] { prk5 });
				for (int f = 0; f < list15.size(); f++) {
					HashMap row15 = (HashMap) list15.get(f);
					lsg.setEfek_date(row15.get("EFEK_DATE").toString());
					ed = row15.get("EFEK_DATE").toString();
				}

				String qr16 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY6 FROM DUAL";
				String primarykey6 = DbConnection.executeScalar(DbConnection.KTHIS, qr16, new Object[] {}).toString();
				lsg.setPRIMARYKEY6(primarykey6);
				prk6 = primarykey6;

				String qr17 = "INSERT INTO PATIENTACCOUNT (PATIENTACCOUNT_ID, VISIT_ID, LAST_UPDATED_BY, ACCOUNT_BALANCE, BILL_SIZE, FROZEN_ACCOUNT_BALANCE, "
						+ "FROZEN_BILL_SIZE, LAST_UPDATED_DATETIME, OPEN_CARD_IND, ACCOUNT_CHANGED_IND, ACCOUNT_STATUS, "
						+ "PATIENT_ACCOUNT_NO, DEPOSIT_BALANCE) VALUES (?, ?,  ?,0,0,0,0,(SELECT sysdate from DUAL),'N','N','ACS1', ?,0)";
				Object[] input6 = { prk6, prk, user, gen };
				DbConnection.executeQuery(DbConnection.KTHIS, qr17, input6);

				String qr18 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY7 FROM DUAL";
				String primarykey7 = DbConnection.executeScalar(DbConnection.KTHIS, qr18, new Object[] {}).toString();
				lsg.setPRIMARYKEY7(primarykey7);
				prk7 = primarykey7;

				String qr19 = "INSERT INTO ACCOUNTDEBTOR (ACCOUNTDEBTOR_ID, STAKEHOLDER_ID, PATIENTACCOUNT_ID, PAYER_SEQ, CONTRACT_BALANCE, PAID_AMOUNT, LAST_BILL_AMOUNT, "
						+ "FROZEN_CONTRACT_BALANCE, FROZEN_CONTRACT_SIZE, CN_YTD_SELFPAID_AMOUNT, CN_LAST_CLAIM_AMOUNT, CN_SELF_PAYABLE_AMOUNT, DEBTOR_CHANGED_IND, "
						+ "FINANCIAL_CLASS, CLAIM_POLICY_CODE, CN_HEALTHCARE_PLAN_IND, CN_MAJOR_DISEASE_IND, CN_APPOINTED_HOSPITAL_IND, DEFUNCT_IND, LAST_UPDATED_BY, "
						+ "LAST_UPDATED_DATETIME) VALUES (?, ?, ?,10,0,0,0,0,0,0,0,0,'N','FCC1','SELF','N','N','N','N', ?, "
						+ "(SELECT sysdate from DUAL))";
				Object[] input7 = { prk7, stkid, prk6, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr19, input7);

				if (rqi.equals("Y") && rci.equals("N")) {

					String qr20 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY8 FROM DUAL";
					String primarykey8 = DbConnection.executeScalar(DbConnection.KTHIS, qr20, new Object[] {})
							.toString();
					lsg.setPRIMARYKEY8(primarykey8);
					prk8 = primarykey8;

					String qr21 = "SELECT COUNT(1) as qn FROM VISITREGNTYPE VRT WHERE VRT.EFFICTIVE_DATETIME >= to_date(?,'ddmmyyyy') AND VRT.EFFICTIVE_DATETIME < "
							+ "to_date(?,'ddmmyyyy') + 1 AND VRT.VISITREGNTYPE_ID <= ?";
					Object[] input8 = { ed, ed, prk3 };
					String qnTemp = DbConnection.executeScalar(DbConnection.KTHIS, qr21, input8).toString();
					lsg.setQn(qnTemp);
					qn = qnTemp;

					String qr22 = "SELECT COUNT(1) as qn2 FROM OPWAITINGQUEUE WQ WHERE WQ.RESOURCEMSTR_ID = ? AND WQ.ENQUEUE_DATETIME <= SYSDATE";
					String qn2Temp = DbConnection.executeScalar(DbConnection.KTHIS, qr22, new Object[] { resourceid })
							.toString();
					lsg.setQn2(qn2Temp);
					qn2 = qn2Temp;

					String qr23 = "INSERT INTO OPWAITINGQUEUE (OPWAITINGQUEUE_ID, RESOURCEMSTR_ID, ENQUEUE_DATETIME, VISIT_ID, QUEUE_NO, QUEUE_SEQ_NO, STATUS, CAREPROVIDER_ID, "
							+ "CREATED_BY,   CREATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME ) VALUES "
							+ "(?, ?, SYSDATE, ?, ?, ?,'OQS1', ?, ?, SYSDATE, ?,SYSDATE)";
					Object[] input9 = { prk8, resourceid, prk, qn, qn2, careid, user, user };
					DbConnection.executeQuery(DbConnection.KTHIS, qr23, input9);
				}

				String qr24 = "SELECT to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS') as tgreg FROM DUAL";
				String tgreg = DbConnection.executeScalar(DbConnection.KTHIS, qr24, new Object[] {}).toString();
				lsg.setTgreg(tgreg);

				lsg.setResponse(true);
				lsg.setDescription("Berhasil");

				all.add(lsg);
			}
		} catch (SQLException e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage());

			ListSelfReg lsg = new ListSelfReg();
			lsg.setResponse(false);
			lsg.setDescription("Gagal | " + e.getMessage());
			all.add(lsg);
		} catch (Exception e) {

			e.fillInStackTrace();
			System.out.println(e.getMessage());

			ListSelfReg lsg = new ListSelfReg();
			lsg.setResponse(false);
			lsg.setDescription("Gagal | " + e.getMessage());
		}

		finally {
		}
		return all;
	}

	public static List<ListSelfReg> insertKthisGeneral(String cardno, String careproviderid, String noantri,
			String userid, String source) {
		// TODO Auto-generated method stub
		List<ListSelfReg> all = new ArrayList<ListSelfReg>();
		String sequenceStatus = null;
		String patid = "";
		String stkid = "";
		String patcal = "";
		String lokasi = "311177558";
		String prk = "";
		String gen = "";
		String prk2 = "";
		String prk3 = "";
		String prk4 = "";
		String prk5 = "";
		String prk6 = "";
		String prk7 = "";
		String prk8 = "";
		String ed = "";
		String qn = "";
		String qn2 = "";

		String qr1 = "SELECT RM.RESOURCEMSTR_ID, RM.CAREPROVIDER_ID, RM.SUBSPECIALTYMSTR_ID, RM.RESOURCE_NAME, RM.REGISTRATION_TYPE, RM.RESOURCE_QUEUE_IND, "
				+ "RM.RESOURCE_CHECKOUT_IND FROM RESOURCEMSTR RM WHERE RM.RESOURCEMSTR_ID=? AND RM.DEFUNCT_IND = 'N'";
		try {
			List list = DbConnection.executeReader(DbConnection.KTHIS, qr1, new Object[] { careproviderid });

			for (int a = 0; a < list.size(); a++) {
				HashMap row = (HashMap) list.get(a);
				ListSelfReg lsg = new ListSelfReg();
				lsg.setRESOURCEMSTR_ID(row.get("RESOURCEMSTR_ID").toString());
				String resourceid = row.get("RESOURCEMSTR_ID").toString();
				lsg.setCAREPROVIDER_ID(row.get("CAREPROVIDER_ID").toString());
				String careid = row.get("CAREPROVIDER_ID").toString();
				lsg.setSUBSPECIALTYMSTR_ID(row.get("SUBSPECIALTYMSTR_ID").toString());
				String subid = row.get("SUBSPECIALTYMSTR_ID").toString();
				lsg.setRESOURCE_NAME(row.get("RESOURCE_NAME").toString());
				lsg.setREGISTRATION_TYPE(row.get("REGISTRATION_TYPE").toString());
				String regtyp = row.get("REGISTRATION_TYPE").toString();
				lsg.setRESOURCE_QUEUE_IND(row.get("RESOURCE_QUEUE_IND").toString());
				String rqi = row.get("RESOURCE_QUEUE_IND").toString();
				lsg.setRESOURCE_CHECKOUT_IND(row.get("RESOURCE_CHECKOUT_IND").toString());
				String rci = row.get("RESOURCE_CHECKOUT_IND").toString();

				String qr2 = "SELECT PS.PERSON_ID, PT.PATIENT_ID, CD.CARD_NO, PS.PERSON_NAME, pgCOMMON.fxGetCodeDesc(PS.SEX) as sex, "
						+ "TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)||' Tahun ' || "
						+ "TRUNC(months_between(sysdate,PS.BIRTH_DATE)-12*trunc(months_between(sysdate,PS.BIRTH_DATE)/12)) || ' Bulan' as AGE, "
						+ "PS.BIRTH_DATE, PS.STAKEHOLDER_ID, PT.PATIENT_CLASS FROM PERSON PS, PATIENT PT, CARD CD WHERE PT.PERSON_ID = PS.PERSON_ID AND "
						+ "CD.PERSON_ID = PS.PERSON_ID AND CD.CARD_NO = ?";
				List list2 = DbConnection.executeReader(DbConnection.KTHIS, qr2, new Object[] { cardno });
				for (int b = 0; b < list2.size(); b++) {
					HashMap row2 = (HashMap) list2.get(b);
					lsg.setPERSON_ID(row2.get("PERSON_ID").toString());
					lsg.setPATIENT_ID(row2.get("PATIENT_ID").toString());
					patid = row2.get("PATIENT_ID").toString();
					lsg.setCARD_NO(row2.get("CARD_NO").toString());
					lsg.setPERSON_NAME(row2.get("PERSON_NAME").toString());
					lsg.setSex(row2.get("SEX").toString());
					lsg.setAGE(row2.get("AGE").toString());
					lsg.setBIRTH_DATE(row2.get("BIRTH_DATE").toString());
					;
					lsg.setSTAKEHOLDER_ID(row2.get("STAKEHOLDER_ID").toString());
					stkid = row2.get("STAKEHOLDER_ID").toString();
					lsg.setPATIENT_CLASS(row2.get("PATIENT_CLASS").toString());
					patcal = row2.get("PATIENT_CLASS").toString();
				}
				String user = userid;

				String qr3 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY FROM DUAL";
				String primarykey = DbConnection.executeScalar(DbConnection.KTHIS, qr3, new Object[] {}).toString();
				lsg.setVISITID(primarykey);
				prk = primarykey;

				if (source.equals(MobileBiz.MOBILE_SOURCE) || source.equals(MobileBiz.WEB_SOURCE)) {
					sequenceStatus = "2";
				} else {
					sequenceStatus = "1";
				}

				String qr4 = "INSERT INTO VISIT (VISIT_ID, PATIENT_ID, SUBSPECIALTYMSTR_ID, ADMITTED_AT, ADMITTED_BY, LAST_UPDATED_BY, ADMISSION_DATETIME, "
						+ "LAST_UPDATED_DATETIME, SPECIAL_CARE_IND, HEALTH_BOOK_FEE_IND, CARD_FEE_IND, PATIENT_TYPE, PATIENT_CLASS, VISIT_TYPE, ADMIT_STATUS, ADMIT_CLASS, "
						+ "ADMITTING_SOURCE, VISIT_NO, LAST_VISIT_NO, IPT_VISIT_TO_DATE, OPT_VISIT_TO_DATE, CONSULT_STATUS, MA_TYPE, ADMISSION_TIMES, ENTITYMSTR_ID, "
						+ "IN_PATH_IND, QUEUE_NO, QUEUE_SEQUENCE,SOURCE_TYPE) VALUES (?, ?, ?, ?, ?, ?, (SELECT sysdate from DUAL), "
						+ "(SELECT sysdate from DUAL), 'N', 'N', 'N', 'PTY2', ?, 'VST1', 'AST1', 'ACL1', 'ADSNORM', "
						+ "(SELECT pgSYSFUNC.fxGenSeqNo('VISIT_NO_OP',?,'PTY2') FROM DUAL), "
						+ "(SELECT MAX(V.VISIT_NO)FROM VISIT V WHERE V.PATIENT_ID = ?),0,0,'CNT1','MATN',1,1,'N',?,?, ?)";
				Object[] input = { prk, patid, subid, lokasi, user, user, patcal, user, patid, noantri, sequenceStatus,
						source };
				DbConnection.executeQuery(DbConnection.KTHIS, qr4, input);

				String qr5 = "SELECT VISIT_NO FROM VISIT WHERE VISIT_ID = ?";
				String visit_no = DbConnection.executeScalar(DbConnection.KTHIS, qr5, new Object[] { prk }).toString();
				lsg.setLAST_VISIT_NO(visit_no);
				gen = visit_no;

				String qr6 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY2 FROM DUAL";
				String primarykey2 = DbConnection.executeScalar(DbConnection.KTHIS, qr6, new Object[] {}).toString();
				lsg.setPRIMARYKEY2(primarykey2);
				prk2 = primarykey2;

				String qr7 = "INSERT INTO PATIENTTYPECLASSHISTORY(PATIENTTYPECLASSHISTORY_ID, VISIT_ID, NEW_PATIENT_TYPE, NEW_PATIENT_CLASS, START_DATETIME, CREATED_BY, "
						+ "CREATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME ) VALUES(?, ?, 'PTY2', ?, (SELECT sysdate from DUAL), "
						+ "?, (SELECT sysdate from DUAL), ?, (SELECT sysdate from DUAL) )";
				Object[] input2 = { prk2, prk, patcal, user, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr7, input2);

				String qr8 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY3 FROM DUAL";
				String primarykey3 = DbConnection.executeScalar(DbConnection.KTHIS, qr8, new Object[] {}).toString();
				lsg.setPRIMARYKEY3(primarykey3);
				prk3 = primarykey3;

				String qr9 = "INSERT INTO VISITREGNTYPE (VISITREGNTYPE_ID, VISIT_ID, REGISTRATION_TYPE, RESOURCEMSTR_ID, CREATED_BY, CREATED_DATETIME, DEFUNCT_IND,  "
						+ "LAST_UPDATED_BY, LAST_UPDATED_DATETIME, CAREPROVIDER_ID, SUBSPECIALTYMSTR_ID, CREATED_AT, SESSIONMSTR_ID, REGN_NO, REGISTRATION_METHOD, "
						+ "EFFICTIVE_DATETIME ) VALUES (?, ?, ?, ?, ?, (SELECT sysdate from DUAL), 'N', ?, "
						+ "(SELECT sysdate from DUAL), ?, ?, ?, '1', (SELECT pgSYSFUNC.fxGenSeqNo('REGN_NO',?,NULL) FROM DUAL), "
						+ "'RGM1', (SELECT sysdate from DUAL))";
				Object[] input3 = { prk3, prk, regtyp, resourceid, user, user, careid, subid, lokasi, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr9, input3);

				String qr10 = "select REGN_NO from VISITREGNTYPE where VISITREGNTYPE_ID= ?";
				String regn_no = DbConnection.executeScalar(DbConnection.KTHIS, qr10, new Object[] { prk3 }).toString();
				lsg.setREGN_NO(regn_no);

				String qr11 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY4 FROM DUAL";
				String primarykey4 = DbConnection.executeScalar(DbConnection.KTHIS, qr11, new Object[] {}).toString();
				lsg.setPRIMARYKEY4(primarykey4);
				prk4 = primarykey4;

				String qr12 = "INSERT INTO REGNVISITACTIVATION (REGNVISITACTIVATION_ID, VISITREGNTYPE_ID, VISIT_ID, CREATED_BY, CREATED_DATETIME, "
						+ "CREATED_LOCATIONMSTR_ID ) VALUES (?, ?, ?, ?, (SELECT sysdate from DUAL), ?)";

				Object[] input4 = { prk4, prk3, prk, user, lokasi };
				DbConnection.executeQuery(DbConnection.KTHIS, qr12, input4);

				String qr13 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY5 FROM DUAL";
				String primarykey5 = DbConnection.executeScalar(DbConnection.KTHIS, qr13, new Object[] {}).toString();
				lsg.setPRIMARYKEY5(primarykey5);
				prk5 = primarykey5;

				String qr14 = "INSERT INTO VISITSUBSPECIALTYHISTORY (VISITSUBSPECIALTYHISTORY_ID, SUBSPECIALTYMSTR_ID, VISIT_ID, LAST_UPDATED_BY, EFFECTIVE_DATETIME, "
						+ "LAST_UPDATED_DATETIME, DEFUNCT_IND) VALUES (?, ?, ?, ?, (SELECT sysdate from DUAL), (SELECT sysdate from DUAL), 'N')";
				Object[] input5 = { prk5, subid, prk, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr14, input5);

				String qr15 = "SELECT TO_CHAR(EFFECTIVE_DATETIME,'ddmmyyyy') as efek_date FROM VISITSUBSPECIALTYHISTORY WHERE VISITSUBSPECIALTYHISTORY_ID= ?";
				String efek_date = DbConnection.executeScalar(DbConnection.KTHIS, qr15, new Object[] { prk5 })
						.toString();
				lsg.setEfek_date(efek_date);
				ed = efek_date;

				String qr16 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY6 FROM DUAL";
				String primarykey6 = DbConnection.executeScalar(DbConnection.KTHIS, qr16, new Object[] {}).toString();
				lsg.setPRIMARYKEY6(primarykey6);
				prk6 = primarykey6;

				String qr17 = "INSERT INTO PATIENTACCOUNT (PATIENTACCOUNT_ID, VISIT_ID, LAST_UPDATED_BY, ACCOUNT_BALANCE, BILL_SIZE, FROZEN_ACCOUNT_BALANCE, "
						+ "FROZEN_BILL_SIZE, LAST_UPDATED_DATETIME, OPEN_CARD_IND, ACCOUNT_CHANGED_IND, ACCOUNT_STATUS, "
						+ "PATIENT_ACCOUNT_NO, DEPOSIT_BALANCE) VALUES (?, ?, ?,0,0,0,0,(SELECT sysdate from DUAL),'N','N','ACS1',?,0)";
				Object[] input6 = { prk6, prk, user, gen };
				DbConnection.executeQuery(DbConnection.KTHIS, qr17, input6);

				String qr18 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY7 FROM DUAL";
				String primarykey7 = DbConnection.executeScalar(DbConnection.KTHIS, qr18, new Object[] {}).toString();
				lsg.setPRIMARYKEY7(primarykey7);
				prk7 = primarykey7;

				String qr19 = "INSERT INTO ACCOUNTDEBTOR (ACCOUNTDEBTOR_ID, STAKEHOLDER_ID, PATIENTACCOUNT_ID, PAYER_SEQ, CONTRACT_BALANCE, PAID_AMOUNT, LAST_BILL_AMOUNT, "
						+ "FROZEN_CONTRACT_BALANCE, FROZEN_CONTRACT_SIZE, CN_YTD_SELFPAID_AMOUNT, CN_LAST_CLAIM_AMOUNT, CN_SELF_PAYABLE_AMOUNT, DEBTOR_CHANGED_IND, "
						+ "FINANCIAL_CLASS, CLAIM_POLICY_CODE, CN_HEALTHCARE_PLAN_IND, CN_MAJOR_DISEASE_IND, CN_APPOINTED_HOSPITAL_IND, DEFUNCT_IND, LAST_UPDATED_BY, "
						+ "LAST_UPDATED_DATETIME) VALUES (?, ?, ?,10,0,0,0,0,0,0,0,0,'N','FCC1','SELF','N','N','N','N', ?, "
						+ "(SELECT sysdate from DUAL))";
				Object[] input7 = { prk7, stkid, prk6, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr19, input7);

				if (rqi.equals("Y") && rci.equals("N")) {

					String qr20 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY8 FROM DUAL";
					String primarykey8 = DbConnection.executeScalar(DbConnection.KTHIS, qr20, new Object[] {})
							.toString();
					lsg.setPRIMARYKEY8(primarykey8);
					prk8 = primarykey8;

					String qr21 = "SELECT COUNT(1) as qn FROM VISITREGNTYPE VRT WHERE VRT.EFFICTIVE_DATETIME >= to_date(?,'ddmmyyyy') AND VRT.EFFICTIVE_DATETIME < "
							+ "to_date(?,'ddmmyyyy') + 1 AND VRT.VISITREGNTYPE_ID <= ?";
					Object[] input8 = { ed, ed, prk3 };
					String qn_query = DbConnection.executeScalar(DbConnection.KTHIS, qr21, input8).toString();
					lsg.setQn(qn_query);
					qn = qn_query;

					String qr22 = "SELECT COUNT(1) as qn2 FROM OPWAITINGQUEUE WQ WHERE WQ.RESOURCEMSTR_ID = ? AND WQ.ENQUEUE_DATETIME <= SYSDATE";
					String qn2_query = DbConnection.executeScalar(DbConnection.KTHIS, qr22, new Object[] { resourceid })
							.toString();
					lsg.setQn2(qn2_query);
					qn2 = qn2_query;

					String qr23 = "INSERT INTO OPWAITINGQUEUE (OPWAITINGQUEUE_ID, RESOURCEMSTR_ID, ENQUEUE_DATETIME, VISIT_ID, QUEUE_NO, QUEUE_SEQ_NO, STATUS, CAREPROVIDER_ID, "
							+ "CREATED_BY,   CREATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME ) VALUES "
							+ "(?, ?, SYSDATE, ?, ?, ?,'OQS1', ?, ?, SYSDATE, ?,SYSDATE)";
					Object[] input9 = { prk8, resourceid, prk, qn, qn2, careid, user, user };
					DbConnection.executeQuery(DbConnection.KTHIS, qr23, input9);

				}

				String qr24 = "SELECT to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS') as tgreg FROM DUAL";
				String tgreg = DbConnection.executeScalar(DbConnection.KTHIS, qr24, new Object[] {}).toString();
				lsg.setTgreg(tgreg);

				lsg.setResponse(true);
				lsg.setDescription("Berhasil");
				all.add(lsg);
			}
		} catch (SQLException e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage());

			ListSelfReg lsg = new ListSelfReg();
			lsg.setResponse(false);
			lsg.setDescription("Gagal | " + e.getMessage());
			all.add(lsg);
		} catch (Exception e) {

			e.fillInStackTrace();
			System.out.println(e.fillInStackTrace());

			ListSelfReg lsg = new ListSelfReg();
			lsg.setResponse(false);
			lsg.setDescription("Gagal | " + e.getMessage());
		}

		finally {
		}
		return all;
	}

	private static void clearSendSep(BigDecimal visitId, String fileType) {
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		ResultSet haspas2 = null;

		if (connection == null)
			return;
		Statement stmt = null;
		Statement stmt2 = null;
		PreparedStatement ps = null;
		PreparedStatement statementInsertPdfUpload = null;

		String qry = "update pdfupload set defunct_ind = 'Y' where visit_id =? and file_type=? and file_cat=?";
		try {
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(qry);
			ps.setBigDecimal(1, visitId);
			ps.setString(2, fileType);
			ps.setString(3, "SEP");
			ps.executeUpdate();
			ps.close();
			connection.commit();
		} catch (Exception ex) {
			ex.fillInStackTrace();
			System.out.println(ex.getMessage());

			if (connection != null) {
				try {
					System.err.println("Transaction is being rolled back in " + ex.getStackTrace()[0].getMethodName());
					connection.rollback();
				} catch (SQLException excep) {
					System.out.println(excep.getMessage());
				}
			}
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (haspas2 != null)
				try {
					haspas2.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (SQLException e) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (statementInsertPdfUpload != null)
				try {
					statementInsertPdfUpload.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
	}

	public static IntegratedKthis sendSep(PDFUpload sep) {
		IntegratedKthis result = new IntegratedKthis();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		ResultSet haspas2 = null;

		if (connection == null)
			return null;
		Statement stmt = null;
		Statement stmt2 = null;
		PreparedStatement ps = null;
		PreparedStatement statementInsertPdfUpload = null;

		String qr1 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY FROM DUAL";
		try {
			connection.setAutoCommit(false);

			stmt = connection.createStatement();
			haspas = stmt.executeQuery(qr1);
			haspas.next();
			String prk = haspas.getString("PRIMARYKEY");
			haspas.close();
			stmt.close();

			String queryGetVisitIdSelfreg = "select sb.visit_id from sepbpjs sb where sb.sep_no=? and sb.created_by=327759870";
			stmt2 = connection.createStatement();
			ps = connection.prepareStatement(queryGetVisitIdSelfreg);
			ps.setString(1, sep.getSepNo());
			haspas2 = ps.executeQuery();
			while (haspas2.next()) {
				BigDecimal visitId = haspas2.getBigDecimal("VISIT_ID");
				clearSendSep(visitId, sep.getFileType());
				String insertSEP = "INSERT INTO PDFUPLOAD " + "(PDFUPLOAD_ID,VISIT_ID,FILE_TYPE,"
						+ "FILE_NAME,REMARKS,CREATED_BY," + "CREATED_AT,MODIFIED_BY,MODIFIED_AT,"
						+ "DEFUNCT_IND, FILE_UPLOAD, FILE_CAT) " + " VALUES (?,?,?,?,?,?,SYSDATE,?,SYSDATE,'N',?,?)";
				statementInsertPdfUpload = connection.prepareStatement(insertSEP);
				statementInsertPdfUpload.setString(1, prk);
				statementInsertPdfUpload.setBigDecimal(2, visitId);
				statementInsertPdfUpload.setString(3, sep.getFileType());
				statementInsertPdfUpload.setString(4, sep.getFileName());
				statementInsertPdfUpload.setString(5, sep.getRemarks());
				statementInsertPdfUpload.setBigDecimal(6, sep.getUserid());
				statementInsertPdfUpload.setBigDecimal(7, sep.getUserid());
				statementInsertPdfUpload.setBytes(8, sep.getFileUpload());
				statementInsertPdfUpload.setString(9, "SEP");
				statementInsertPdfUpload.executeUpdate();
				result.setStatus("OK");
				statementInsertPdfUpload.close();
			}
			haspas2.close();
			ps.close();
			connection.commit();
		} catch (Exception ex) {
			ex.fillInStackTrace();
			System.out.println(ex.getMessage());

			result.setStatus("Gagal | " + ex.getMessage());

			if (connection != null) {
				try {
					System.err.println("Transaction is being rolled back in " + ex.getStackTrace()[0].getMethodName());
					connection.rollback();
				} catch (SQLException excep) {
					System.out.println(excep.getMessage());
				}
			}
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (haspas2 != null)
				try {
					haspas2.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (SQLException e) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (statementInsertPdfUpload != null)
				try {
					statementInsertPdfUpload.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return result;

	}
}