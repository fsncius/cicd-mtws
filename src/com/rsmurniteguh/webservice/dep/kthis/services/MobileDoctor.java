package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import com.google.gson.Gson;
import com.rsmurniteguh.webservice.dep.all.model.mobile.CodeDescView;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DoctorListSchedule;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DoctorVisitRegnList;
import com.rsmurniteguh.webservice.dep.all.model.mobile.IpDoctorVisitList;
import com.rsmurniteguh.webservice.dep.all.model.mobile.PurchaseRequisition;
import com.rsmurniteguh.webservice.dep.all.model.mobile.PurchaseRequisitionDetail;
import com.rsmurniteguh.webservice.dep.all.model.mobile.RadiologyScheduleViewModel;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResourceList;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResourceScheme;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.mobile.RisReportTime;
import com.rsmurniteguh.webservice.dep.all.model.mobile.WeeklySchedule;
import com.rsmurniteguh.webservice.dep.all.model.mobile.WeeklyScheduleDoctor;
import com.rsmurniteguh.webservice.dep.all.model.mobile.firebasenotif.NotifModel;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.securecontext.secure.EncryptorConnection;

public class MobileDoctor extends DbConnection {
	private static final String API_KEY = "key=AAAAAokv-T4:APA91bE7EcrfNzadqfZvyg8e2t7cLZs7CUF66DUnsb2wMRhxgjl1NvugQVwkewss2CBAbbwxvk5ILFmE9dwUBoFXLNXzqoAn_6w6yuFRtob9_CV7xELIPt1GqJgECxhvS1Sw7MgBnxeu";
	private static final String url = "https://fcm.googleapis.com/fcm/send";

	public static Boolean postNotification(NotifModel model) {
		Boolean success = false;
		System.out.println("Send Notification");
		Gson gson = new Gson();
		String body = gson.toJson(model);
		HttpClient httpClient = new DefaultHttpClient();
		URL obj;
		try {
			HttpPost request = new HttpPost(url);
			StringEntity params = new StringEntity(body);
			request.addHeader("Accept-Language", "en-US,en;q=0.5");
			request.addHeader("Content-Type", "application/json");
			request.addHeader("Authorization", API_KEY);

			request.setEntity(params);
			HttpResponse response = httpClient.execute(request);

			if (response != null) {
				InputStream in = response.getEntity().getContent(); // Get the
																	// data in
																	// the
																	// entity
			}

			// print result
			System.out.println(response.toString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		finally {
		}

		return success;
	}

	public static List<CodeDescView> GetPoliclynicListsByLocation(String location) {
		System.out.println("GetPoliclynicListsByLocation...");
		List<CodeDescView> data = new ArrayList<CodeDescView>();
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;

		/*
		 * CUSTOME HARD CODE FOR 'HEMODIALISA'
		 */
		String resourcemstr_custom = MobileBiz.CUSTOM_RESOURCEMSTR_ID_GENERAL;
		if (!location.equals(MobileBiz.POLI.toString())) {
			resourcemstr_custom = MobileBiz.CUSTOM_RESOURCEMSTR_ID_BPJS;
		}

		String query_filter = "";
		if (!resourcemstr_custom.equals("")) {
			query_filter = "RM.RESOURCEMSTR_ID IN (" + resourcemstr_custom + ") OR  ";
		}

		String sql = "select cm.code_cat||cm.code_abbr as code, CODE_DESC, CODE_SEQ, code_cat "
				+ " from CODEMSTR cm where code_cat = 'PSB' AND DEFUNCT_IND = 'N' " + " and exists(SELECT 1  "
				+ " FROM RESOURCEMSTR RM, SUBSPECIALTYMSTR SSPM  "
				+ " WHERE ( RM.SUBSPECIALTYMSTR_ID = SSPM.SUBSPECIALTYMSTR_ID OR RM.RESOURCEMSTR_ID = '311241317')  "
				+ " AND RM.RESOURCE_SCHEME_IND = 'N'  " + " AND RM.DEFUNCT_IND = 'N'  "
				+ " and RM.POLI_CODE = cm.code_cat||cm.code_abbr AND ( " + query_filter
				+ " (exists(select 1 from RESOURCEPROFILE rp where RM.RESOURCEMSTR_ID = rp.resourcemstr_id AND (rp.EXPIRY_DATE > SYSDATE or rp.EXPIRY_DATE is null) ) AND SSPM.SUBSPECIALTYMSTR_ID = ? ))  "
				+ " ) order by cm.CODE_SEQ";
		try {
			st = connection.prepareStatement(sql);
			st.setBigDecimal(1, new BigDecimal(location));
			rs = st.executeQuery();
			while (rs.next()) {
				CodeDescView dl = new CodeDescView();
				dl.setCode(rs.getString("CODE"));
				dl.setCodeDesc(rs.getString("CODE_DESC"));
				dl.setSeq(new BigDecimal(rs.getString("CODE_SEQ")));
				data.add(dl);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static List<CodeDescView> GetPoliclynicLists() {		
		List<CodeDescView> data = new ArrayList<CodeDescView>();
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		String sql = "select cm.code_cat||cm.code_abbr as code, CODE_DESC, CODE_SEQ, code_cat "
				+ " from CODEMSTR cm where code_cat = 'PSB' AND DEFUNCT_IND = 'N' " + " and exists(SELECT 1  "
				+ " FROM RESOURCEMSTR RM, SUBSPECIALTYMSTR SSPM  "
				+ " WHERE ( RM.SUBSPECIALTYMSTR_ID = SSPM.SUBSPECIALTYMSTR_ID OR RM.RESOURCEMSTR_ID = '311241317')  "
				+ " AND RM.RESOURCE_SCHEME_IND = 'N'  " + " AND RM.DEFUNCT_IND = 'N'  "
				+ " and RM.POLI_CODE = cm.code_cat||cm.code_abbr AND ( "
				+ " (exists(select 1 from RESOURCEPROFILE rp where RM.RESOURCEMSTR_ID = rp.resourcemstr_id AND (rp.EXPIRY_DATE > SYSDATE or rp.EXPIRY_DATE is null) ) AND SSPM.SUBSPECIALTYMSTR_ID IN (336581902,311187819) ))  "
				+ " ) order by cm.CODE_SEQ";
		try {
			st = connection.createStatement();
			rs = st.executeQuery(sql);
			while (rs.next()) {
				CodeDescView dl = new CodeDescView();
				dl.setCode(rs.getString("CODE"));
				dl.setCodeDesc(rs.getString("CODE_DESC"));
				dl.setSeq(new BigDecimal(rs.getString("CODE_SEQ")));
				data.add(dl);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static List<ResourceList> GetDoctorsList(String poli, String location) {
		System.out.println("GetDoctorsList...");
		List<ResourceList> data = new ArrayList<ResourceList>();
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;

		/*
		 * CUSTOME HARD CODE FOR 'HEMODIALISA' AND 'REKAP MEDIK'
		 */
		String subspecialt_custom = MobileBiz.CUSTOM_SUBSPECIALTYMSTR_ID_BPJS;
		if (location.equals(MobileBiz.POLI.toString())) {
			subspecialt_custom = MobileBiz.CUSTOM_SUBSPECIALTYMSTR_ID_GENERAL;
		}

		String resourcemstr_custom = MobileBiz.CUSTOM_RESOURCEMSTR_ID_BPJS;
		if (location.equals(MobileBiz.POLI.toString())) {
			resourcemstr_custom = MobileBiz.CUSTOM_RESOURCEMSTR_ID_GENERAL;
		}

		String query_filter = "";
		if (!resourcemstr_custom.equals("")) {
			query_filter = "RM.RESOURCEMSTR_ID IN (" + resourcemstr_custom + ") OR  ";
		}
		String sql = "SELECT NULL RESOURCESCHEME_ID, " 
				+ " (SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR WHERE SUBSPECIALTY_CODE = RM.POLI_CODE AND DEFUNCT_IND='N') POLI_CODE, " 
				+ "RM.POLI_CODE SUBSPECIALTY_CODE, "
				+ "RM.RESOURCEMSTR_ID, " 
				+ "RM.RESOURCE_CODE, " 
				+ "RM.RESOURCE_SHORT_CODE, "
				+ "RM.RESOURCE_QUICK_CODE, " 
				+ "PER.PERSON_NAME RESOURCE_NAME, " 
				+ "RM.RESOURCE_NAME_LANG1, "
				+ "RM.REGISTRATION_TYPE, " 
				+ "RM.SUBSPECIALTYMSTR_ID, " 
				+ "SSPM.SUBSPECIALTY_DESC, "
				+ "SSPM.SUBSPECIALTY_DESC_LANG1, " 
				+ "RM.CAREPROVIDER_ID, " 
				+ "CM.CODE_DESC "
				+ "FROM RESOURCEMSTR RM, SUBSPECIALTYMSTR SSPM, CODEMSTR CM, PERSON PER , CAREPROVIDER CP "
				+ "WHERE RM.POLI_CODE = CM.CODE_CAT||CM.CODE_ABBR "
				+ "AND RM.SUBSPECIALTYMSTR_ID = SSPM.SUBSPECIALTYMSTR_ID " 
				+ "AND CP.CAREPROVIDER_ID = RM.CAREPROVIDER_ID "
				+ "AND CP.PERSON_ID = PER.PERSON_ID "
				+ "AND ( " + query_filter + " exists(select 1 from RESOURCEPROFILE rp where RM.RESOURCEMSTR_ID = rp.resourcemstr_id AND (rp.EXPIRY_DATE > SYSDATE or rp.EXPIRY_DATE is null) )) "
				+ "AND RM.RESOURCE_SCHEME_IND = 'N' " + "AND RM.DEFUNCT_IND = 'N' " + "and RM.POLI_CODE = ? "
				+ "AND (SSPM.SUBSPECIALTYMSTR_ID = ? OR SSPM.SUBSPECIALTYMSTR_ID IN(" + subspecialt_custom + "))";

		try {
			st = connection.prepareStatement(sql);
			st.setString(1, poli);
			st.setBigDecimal(2, new BigDecimal(location));

			rs = st.executeQuery();
			while (rs.next()) {
				ResourceList nmb = new ResourceList();
				nmb.setResourcescheme_id(rs.getString("RESOURCESCHEME_ID"));
				nmb.setPoli_code(rs.getString("POLI_CODE"));
				nmb.setResourcemstr_id(rs.getBigDecimal("RESOURCEMSTR_ID"));
				nmb.setResource_code(rs.getString("RESOURCE_CODE"));
				nmb.setResource_short_code(rs.getString("RESOURCE_SHORT_CODE"));
				nmb.setResource_quick_code(rs.getString("RESOURCE_QUICK_CODE"));
				nmb.setResource_name(rs.getString("RESOURCE_NAME"));
				nmb.setResource_name_lang1(rs.getString("RESOURCE_NAME_LANG1"));
				nmb.setRegistration_type(rs.getString("REGISTRATION_TYPE"));
				nmb.setSubspecialtymstr_id(rs.getBigDecimal("SUBSPECIALTYMSTR_ID"));
				nmb.setSubspecialty_desc(rs.getString("SUBSPECIALTY_DESC"));
				nmb.setSubspecialty_desc_lang1(rs.getString("SUBSPECIALTY_DESC_LANG1"));
				nmb.setCareprovider_id(rs.getBigDecimal("CAREPROVIDER_ID"));
				nmb.setCareprovider_id(rs.getBigDecimal("CAREPROVIDER_ID"));
				nmb.setSpecialist(rs.getString("CODE_DESC"));
				data.add(nmb);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static boolean isUserAuthorizationPR(String usermstr_id) {
		boolean res = false;

		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return false;

		String sql = "SELECT 1 FROM USERAUTHORIZATIONMSTR UAM "
				+ "        INNER JOIN AUTHORIZATIONMSTR AM ON AM.AUTHORIZATIONMSTR_ID = UAM.AUTHORIZATIONMSTR_ID "
				+ "        WHERE UAM.DEFUNCT_IND = 'N' " + "        AND UAM.USERMSTR_ID = ? "
				+ "        AND AM.AUTHORIZATIONMSTR_CODE = 'AUT_MTMG_APPPR' ";

		try {
			st = connection.prepareStatement(sql);
			st.setEscapeProcessing(true);
			st.setQueryTimeout(60000);
			st.setBigDecimal(1, new BigDecimal(usermstr_id));
			rs = st.executeQuery();

			if (rs.next()) {
				res = true;
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}

		return res;
	}

	public static WeeklySchedule GetDoctorSchedule(String resourcemstr) {
		System.out.println("GetDoctorSchedule...");
		WeeklySchedule data = new WeeklySchedule();
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		String sql = "select rp.RESOURCEMSTR_ID, rp.MON_IND, rp.TUE_IND, rp.WED_IND, rp.THU_IND, rp.FRI_IND, rp.SAT_IND, rp.SUN_IND, sm.SESSION_DESC,cm.code_desc,"
				+ " RM.RESOURCE_NAME, RM.CAREPROVIDER_ID, CP.CAREPROVIDER_URL_IMAGE, "
				+ " RM.sequence as seq_doctor, cm.code_seq as seq_poli "
				+ " from RESOURCEPROFILE rp " + "inner join SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID "
				+ "left outer join RESOURCEMSTR RM on rp.Resourcemstr_Id = RM.RESOURCEMSTR_ID "
				+ "left outer join CAREPROVIDER CP ON RM.CAREPROVIDER_ID = CP.CAREPROVIDER_ID "
				+ "left outer join CODEMSTR cm ON RM.POLI_CODE = cm.code_cat||cm.code_abbr " + "where "
				+ "(rp.EXPIRY_DATE > SYSDATE or rp.EXPIRY_DATE is null) and " + "rp.defunct_ind = 'N' and "
				+ "rp.RESOURCEMSTR_ID = ? order by TO_CHAR(sm.start_time, 'hh24:mi:ss')";
		try {
			st = connection.prepareStatement(sql);
			st.setBigDecimal(1, new BigDecimal(resourcemstr));
			rs = st.executeQuery();

			ArrayList<String> senin = new ArrayList<String>();
			ArrayList<String> selasa = new ArrayList<String>();
			ArrayList<String> rabu = new ArrayList<String>();
			ArrayList<String> kamis = new ArrayList<String>();
			ArrayList<String> jumat = new ArrayList<String>();
			ArrayList<String> sabtu = new ArrayList<String>();
			ArrayList<String> minggu = new ArrayList<String>();

			while (rs.next()) {

				data.setDoctorname(rs.getString("RESOURCE_NAME"));
				data.setSpecialist(rs.getString("code_desc"));
				data.setCareprovider(rs.getString("CAREPROVIDER_ID"));
				data.setDoctor_url_image(rs.getString("CAREPROVIDER_URL_IMAGE"));
				data.setSeq_doctor(rs.getString("seq_doctor"));
				data.setSeq_poli(rs.getString("seq_poli"));
				if (rs.getString("MON_IND").equals("Y"))
					senin.add(rs.getString("SESSION_DESC"));
				if (rs.getString("TUE_IND").equals("Y"))
					selasa.add(rs.getString("SESSION_DESC"));
				if (rs.getString("WED_IND").equals("Y"))
					rabu.add(rs.getString("SESSION_DESC"));
				if (rs.getString("THU_IND").equals("Y"))
					kamis.add(rs.getString("SESSION_DESC"));
				if (rs.getString("FRI_IND").equals("Y"))
					jumat.add(rs.getString("SESSION_DESC"));
				if (rs.getString("SAT_IND").equals("Y"))
					sabtu.add(rs.getString("SESSION_DESC"));
				if (rs.getString("SUN_IND").equals("Y"))
					minggu.add(rs.getString("SESSION_DESC"));

				if (!senin.isEmpty())
					data.setMonday(senin);
				if (!selasa.isEmpty())
					data.setTuesday(selasa);
				if (!rabu.isEmpty())
					data.setWednesday(rabu);
				if (!kamis.isEmpty())
					data.setThursday(kamis);
				if (!jumat.isEmpty())
					data.setFriday(jumat);
				if (!sabtu.isEmpty())
					data.setSaturday(sabtu);
				if (!minggu.isEmpty())
					data.setSunday(minggu);

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static List<DoctorVisitRegnList> GetDoctorsAvailableRegistration(BigDecimal usermstrId) {
		System.out.println("GetDoctorsAvailableRegistration...");
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		List<DoctorVisitRegnList> all = new ArrayList<DoctorVisitRegnList>();
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		String abcarpro = "select * from (select sp.subspecialtymstr_id, v.VISIT_ID, vd.CAREPROVIDER_ID, sp.SUBSPECIALTY_DESC, "
				+ "ps1.PERSON_NAME as pasien, ps2.PERSON_NAME as dokter, um.usermstr_id "
				// + "(select count(*) from ORDERENTRY oe where "
				// + "oe.VISIT_ID = v.VISIT_ID) + (select count(*) from
				// VISITDIAGNOSIS vd where vd.visit_id = v.VISIT_ID) as ind "
				+ "from visit v " + "inner join VISITREGNTYPE vd on vd.VISIT_ID = v.VISIT_ID "
				+ "inner join patient pt on pt.PATIENT_ID = v.PATIENT_ID "
				+ "inner join person ps1 on ps1.PERSON_ID = pt.PERSON_ID "
				+ "inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = vd.CAREPROVIDER_ID "
				+ "inner join person ps2 on ps2.PERSON_ID =cp.PERSON_ID "
				+ "inner join SUBSPECIALTYMSTR sp on sp.SUBSPECIALTYMSTR_ID = v.SUBSPECIALTYMSTR_ID "
				+ "inner join userprofile up on up.person_id = ps2.person_id "
				+ "inner join usermstr um on um.usermstr_id = up.usermstr_id "
				+ "where v.PATIENT_TYPE = 'PTY2' and V.CONSULT_STATUS IN ('CNT1') "
				+ "and to_char(v.ADMISSION_DATETIME, 'ddmmyyyy') = to_char(sysdate, 'ddmmyyyy') "
				+ "and v.ADMIT_STATUS in('AST1', 'AST2', 'AST3', 'AST7') [WITH_USERMSTR] )";

		if (usermstrId != null) {
			abcarpro = abcarpro.replace("[WITH_USERMSTR]", " and um.usermstr_id = ? ");
		} else {
			abcarpro = abcarpro.replace("[WITH_USERMSTR]", "");
		}

		try {
			st = connection.prepareStatement(abcarpro);
			if (usermstrId != null) {
				st.setBigDecimal(1, usermstrId);
			}

			rs = st.executeQuery();
			while (rs.next()) {
				DoctorVisitRegnList dl = new DoctorVisitRegnList();
				dl.setCareproviderId(rs.getBigDecimal("CAREPROVIDER_ID"));
				dl.setPasien(rs.getString("PASIEN"));
				dl.setSubspecialitymstr_id(rs.getString("SUBSPECIALTYMSTR_ID"));
				dl.setSubspeciality_desc(rs.getString("SUBSPECIALTY_DESC"));
				dl.setDokter(rs.getString("DOKTER"));
				dl.setUsermstrId(rs.getBigDecimal("USERMSTR_ID"));
				all.add(dl);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<IpDoctorVisitList> GetDoctorIPVisitList(BigDecimal usermstrId) {
		System.out.println("GetDoctorIPVisitList...");
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		List<IpDoctorVisitList> all = new ArrayList<IpDoctorVisitList>();
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		String abcarpro = "select distinct  c.card_no, p.person_name, bed.ward_desc, bed.bed_no" + " from visit v "
				+ " inner join visitdoctor vd on v.VISIT_ID = vd.VISIT_ID "
				+ " inner join patient pt on pt.patient_id = v.patient_id "
				+ " inner join person p on p.person_id = pt.person_id "
				+ " inner join card c on c.PERSON_ID = p.person_id "
				+ " inner join ( select *  from ( select bh.visit_id, wm.ward_desc,bm.Bed_No, "
				+ " ROW_NUMBER() OVER (PARTITION BY bh.visit_id ORDER BY bh.effective_start_datetime DESC) as rn "
				+ " from BEDHISTORY bh " + " inner join BEDMSTR bm on bm.bedmstr_id = bh.bedmstr_id "
				+ " inner join ROOMMSTR rm on rm.ROOMMSTR_id = bm.ROOMMSTR_id "
				+ " inner join WARDMSTR wm on wm.wardmstr_id = rm.WARDMSTR_id )  where rn = 1) "
				+ " bed on bed.visit_id = v.visit_id " + " where vd.careprovider_id=(select cp0.CAREPROVIDER_ID "
				+ " from CAREPROVIDER cp0 inner join USERPROFILE up0 on up0.PERSON_ID = cp0.PERSON_ID "
				+ " and up0.USERMSTR_ID = ?) and v.patient_type='PTY1' and v.admit_status='AST1' order by ward_desc";

		try {
			st = connection.prepareStatement(abcarpro);
			if (usermstrId != null) {
				st.setBigDecimal(1, usermstrId);
			}

			rs = st.executeQuery();
			while (rs.next()) {
				IpDoctorVisitList dl = new IpDoctorVisitList();
				dl.setCardNo(rs.getString("CARD_NO"));
				dl.setPatientName(rs.getString("PERSON_NAME"));
				dl.setWard(rs.getString("WARD_DESC"));
				dl.setBed(rs.getString("BED_NO"));

				all.add(dl);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<RadiologyScheduleViewModel> GetRadiologySchedule(String date, String type) {
		System.out.println("GetRadiologySchedule...");

		Connection connection = null;
		connection = DbConnection.getRisInstance();
		PreparedStatement st = null;
		ResultSet rs = null;

		List<RadiologyScheduleViewModel> all = new ArrayList<RadiologyScheduleViewModel>();

		if (connection == null)
			return null;
		String sql = "select  to_char(dt.tanggal, 'HH24:MI') as JAM, s.EXAM_DESC from "
				+ " (SELECT to_date('@date@ 00:00:00', 'ddmmyyyy HH24:MI:SS') + ((ROWNUM-1)* 15/1440) tanggal"
				+ " FROM DUAL CONNECT BY ROWNUM <= 96) dt"
				+ " left outer join SCHEDULE s on s.ARRANGE = dt.tanggal and to_char(arrange, 'ddmmyyyy') = ?  "
				+ " and modality = ? " + " union all "
				+ " select to_char(s2.arrange, 'HH24:MI') as JAM, s2.EXAM_DESC from "
				+ " SCHEDULE s2 where to_char(s2.arrange, 'ddmmyyyy') = ?  and s2.modality = ? order by jam ";
		try {

			st = connection.prepareStatement(sql.replace("@date@", date));
			st.setString(1, date);
			st.setString(2, type);
			st.setString(3, date);
			st.setString(4, type);
			rs = st.executeQuery();

			while (rs.next()) {
				RadiologyScheduleViewModel dl = new RadiologyScheduleViewModel();
				dl.setHour(rs.getString("JAM"));
				dl.setExamDesc(rs.getString("EXAM_DESC"));
				all.add(dl);
			}

			// hapus data yang null
			RadiologyScheduleViewModel temp = new RadiologyScheduleViewModel();
			for (Iterator<RadiologyScheduleViewModel> iter = all.iterator(); iter.hasNext();) {
				RadiologyScheduleViewModel dl = iter.next();
				if (temp.getExamDesc() != null) {
					if (temp.getHour().equals(dl.getHour()) && temp.getExamDesc().equals(dl.getExamDesc())) {
						iter.remove();
					}
				}

				temp = dl;
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ResourceScheme> getResourceScheme(String resourcemstr, String tanggal) {
		System.out.println("getResourceScheme...");
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		List<ResourceScheme> data = new ArrayList<ResourceScheme>();
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		String sql = "select RM.RESOURCESCHEME_ID, RM.RESOURCEMSTR_ID, TO_CHAR(RM.REGN_DATE,'YYYY-MM-DD') AS DATE_SCHEDULE, "
				+ "SM.SESSION_DESC from RESOURCESCHEME RM, SESSIONMSTR SM where TO_CHAR(REGN_DATE, 'YYYY-MM-DD') = ? AND "
				+ "RESOURCEMSTR_ID=? AND SM.SESSIONMSTR_ID=RM.SESSIONMSTR_ID ";

		try {
			st = connection.prepareStatement(sql);
			st.setString(1, tanggal);
			st.setBigDecimal(2, new BigDecimal(resourcemstr));

			rs = st.executeQuery();
			while (rs.next()) {
				ResourceScheme dl = new ResourceScheme();
				dl.setResourceSchemeId(rs.getBigDecimal("RESOURCESCHEME_ID"));
				dl.setDateSchedule(rs.getString("DATE_SCHEDULE"));
				dl.setResourcemstr(rs.getBigDecimal("RESOURCEMSTR_ID"));
				dl.setSessionDesc(rs.getString("SESSION_DESC"));
				data.add(dl);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static List<RisReportTime> getRisReportTime(String tanggal, String jenis) {
		List<RisReportTime> data = new ArrayList<RisReportTime>();

		Connection connection = null;
		Connection connection2 = null;
		connection = DbConnection.getRisReportInstance();
		connection2 = DbConnection.getPooledConnection();
		if (connection == null || connection2 == null)
			return null;

		ResultSet rs = null;
		ResultSet rs2 = null;

		/*
		 * KONDISI KETIKA MEMILIKI TIPE RADIOLOGY
		 */
		String filter = "";
		if (!jenis.equals("All") && jenis != null) {
			filter = " AND iscm.item_subcat_desc = '" + jenis + "' ";
		}

		String sql = "SELECT R.ID_Report, R.request_date, R.patient_name,R.admission_id, R.no_rad, R.patient_ID, R.patient_sex, R.patient_age, R.request_physician, R.modality, R.create_by, "
				+ "  CONCAT(replace(convert(char(10),R.time_photo,102),'.','-'),convert(char(5), R.time_photo, 108)) as difoto, "
				+ "  CONCAT(replace(convert(char(10),D.Create_time,102),'.','-'),convert(char(5), D.Create_time, 108)) as dibaca "
				+ "  FROM Tbl_report R " + "  	INNER JOIN "
				+ "		(select Row_number() OVER (partition by t.ID_Report order by t.Create_time desc) as num, t.Create_time, ID_Report from Tbl_Report_Details t where t.Create_time is not null) D ON D.ID_Report = R.ID_Report and num = 1 "
				+ "  WHERE R.admission_id IN [WHERE_IN] "
				+ "  GROUP BY request_date,R.ID_Report, R.patient_name, admission_id, R.no_rad, R.patient_ID, R.patient_sex, R.patient_age, R.request_date, R.request_physician, R.modality, R.create_by, R.create_time, R.time_photo, D.Create_time ";

		String sql2 = "select oe.entered_datetime daftar, oe.VISIT_ID, c.card_no as patient_id, oe.ORDERENTRY_ID, oei.ORDERENTRYITEM_ID admission_id, lm.LOCATION_DESC,  "
				+ "				 p2.PERSON_NAME, iscm.item_subcat_desc modality, tcm.txn_code, tcm.TXN_DESC,  "
				+ "				 oe.REMARKS, (case v.patient_type when 'PTY1' then 'IP' when 'PTY2' then 'OP' end) as patient_source, oe.order_status, pgcommon.fxGetCodeDesc(v.PATIENT_CLASS) as PATIENT_CLASS,  "
				+ "				 pgcommon.fxGetCodeDesc(p2.SEX) as SEX, p2.BIRTH_DATE, p.person_name as REQUEST_PHYSICIAN  "
				+ "				 from ORDERENTRY oe  "
				+ "				 inner join VISIT v on v.visit_id = oe.visit_id  "
				+ "				 inner join ORDERENTRYITEM oei on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID  "
				+ "				 inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = oei.ORDERITEMMSTR_ID  "
				+ "				 inner join ITEMSUBCATEGORYMSTR iscm on iscm.ITEMSUBCATEGORYMSTR_ID = oim.ITEMSUBCATEGORYMSTR_ID  "
				+ "				 inner join ITEMCATEGORYMSTR icm on icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID  "
				+ "				 inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = oe.LOCATIONMSTR_ID  "
				+ "				 inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID  "
				+ "				 inner join patient pt on pt.patient_id = v.patient_id  "
				+ "				 inner join person p2 on p2.person_id = pt.person_id  "
				+ "				 inner join card c on c.person_id =  pt.person_id  "
				+ "				 inner join person p on p.PERSON_ID = cp.PERSON_ID  "
				+ "				 inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID  "
				+ "				 where icm.ITEM_TYPE = 'ITY7'  "
				+ "				 and oe.entered_datetime between to_date(?, 'yyyy-mm-dd HH24:mi:ss') and  "
				+ "				 to_date(?, 'yyyy-mm-dd HH24:mi:ss')  " + filter
				+ "				 order by oe.ENTERED_DATETIME";

		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		try {

			ps2 = connection2.prepareStatement(sql2, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(60000);
			ps2.setString(1, tanggal);
			ps2.setString(2, tanggal + " 23:59:59");
			rs2 = ps2.executeQuery();

			String where_in = "(";
			while (rs2.next()) {
				RisReportTime dl = new RisReportTime();
				dl.setDaftar(rs2.getString("daftar"));
				dl.setAdmission_id(rs2.getString("admission_id"));
				dl.setPatient_name(rs2.getString("PERSON_NAME"));
				dl.setRequest_physician(rs2.getString("REQUEST_PHYSICIAN"));
				dl.setModality(rs2.getString("modality"));
				data.add(dl);

				if (rs2.isLast()) {
					where_in = where_in + rs2.getString("admission_id") + ")";
				} else {
					where_in = where_in + rs2.getString("admission_id") + ",";
				}
			}

			if (!where_in.equals("(")) {
				ps = connection.prepareStatement(sql.replace("[WHERE_IN]", where_in));
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				rs = ps.executeQuery();

				while (rs.next()) {

					for (RisReportTime risReportTime : data) {
						if (risReportTime.getAdmission_id().equals(rs.getString("admission_id"))) {
							risReportTime.setId_report(rs.getString("ID_Report"));
							risReportTime.setRequest_date(rs.getString("request_date"));
							risReportTime.setNo_rad(rs.getString("no_rad"));
							risReportTime.setPatient_id(rs.getString("patient_ID"));
							risReportTime.setPatient_sex(rs.getString("patient_sex"));
							risReportTime.setPatient_age(rs.getString("patient_age"));
							risReportTime.setCreate_by(rs.getString("create_by"));
							risReportTime.setDipoto(rs.getString("difoto"));
							risReportTime.setDibaca(rs.getString("dibaca"));
						}
					}
				}
			}

			// hapus data yang null
			for (Iterator<RisReportTime> iter = data.iterator(); iter.hasNext();) {
				RisReportTime dl = iter.next();
				if (dl.getPatient_name() == null) {
					iter.remove();
				}
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}

			if (rs2 != null)
				try {
					rs2.close();
				} catch (Exception ignore) {
				}
			if (ps2 != null)
				try {
					ps2.close();
				} catch (Exception ignore) {
				}
			if (connection2 != null)
				try {
					connection2.close();
				} catch (Exception ignore) {
				}
		}

		return data;
	}

	public static List<PurchaseRequisition> getPurchaseRequisition() {
		List<PurchaseRequisition> data = new ArrayList<PurchaseRequisition>();

		Connection connection = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		ResultSet rs = null;

		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		ResultSet rs5 = null;
		Statement st = null;

		String sql = "SELECT DISTINCT SPR.STOCKPURCHASEREQUISITION_ID   " + ",SPR.STOREMSTR_ID "
				+ ",SPR.PURCHASE_REQUISITION_NO " + ",SPR.PURCHASE_STATUS   " + ",SPR.CREATED_BY "
				+ ",SPR.CREATED_DATETIME " + ",SPR.REQUESTED_BY " + ",SPR.REQUESTED_DATETIME" + ",SPR.PLANNED_BY   "
				+ ",SPR.PLANNED_DATETIME   " + ",SPR.CANCELLED_BY   " + ",SPR.CANCELLED_DATETIME   " + ",SPR.REMARKS   "
				+ ",SPR.PREV_UPDATED_BY   " + ",SPR.LAST_UPDATED_BY   " + ",SPR.PREV_UPDATED_DATETIME   "
				+ ",SPR.LAST_UPDATED_DATETIME   " + ",SM.STORE_DESC   " + ",SM.STORE_DESC_LANG1   "
				+ ",UMC.USER_NAME CREATED_BY_DESC   " + ",UMA.USER_NAME CONFIRMED_BY_DESC      " + ",CM.CODE_DESC "
				+ "FROM STOCKPURCHASEREQUISITION SPR   " + ",STOREMSTR SM   " + ",USERMSTR UMC   " + ",USERMSTR UMA "
				+ ",CODEMSTR CM " + "WHERE SPR.STOREMSTR_ID = SM.STOREMSTR_ID   "
				+ "AND SPR.CREATED_BY = UMC.USERMSTR_ID   " + "AND SPR.REQUESTED_BY = UMA.USERMSTR_ID(+) "
				+ "AND SPR.PURCHASE_STATUS = CM.CODE_CAT||CM.CODE_ABBR " + "AND CM.CODE_DESC = 'CONFIRMED' "
				+ "ORDER BY SPR.PURCHASE_REQUISITION_NO,SPR.PURCHASE_STATUS,SPR.CREATED_DATETIME,SPR.REMARKS ";

		String sql2 = "SELECT PRD.PURCHASEREQUISITIONDETAIL_ID,  " + "PRD.STOCKPURCHASEREQUISITION_ID,  "
				+ "PRD.REQUISITION_DETAIL_STATUS,  " + "PRD.MATERIALITEMMSTR_ID,  " + "SPR.STOREMSTR_ID,  "
				+ "PRD.REQUISITION_QTY,  " + "PRD.REQUISITION_UOM, " + "PRD.UNIT_PRICE,  " + "PRD.UNIT_PRICE,  "
				+ "PRD.VENDORMSTR_ID, PRD. " + "CREATED_BY, PRD. " + "CREATED_DATETIME, PRD.REMARKS,  "
				+ "PRD.PREV_UPDATED_BY,  " + "PRD.LAST_UPDATED_BY,  " + "PRD.PREV_UPDATED_DATETIME,  "
				+ "PRD.LAST_UPDATED_DATETIME,  " + "PRD.MANUFACTURER_VENDORMSTR_ID, " + "PRD.DISCOUNT_RATE,  "
				+ "PRD.TOTAL_PURCHASE_PRICE,  " + "PRD.PPN_IND,  " + "PRD.REQUISITION_QTY_FOR_SHOW,  "
				+ "PRD.REQUISITION_UOM_FOR_SHOW,  " + "PRD.COMPLETE_IND,  " + "PRD.COMPLETE_REMARKS,  "
				+ "PRD.TOTAL_RS_AMT,  " + "PRD.MAX_USAGE,  " + "PRD.AVERAGE_USAGE,  "
				+ "PRD.TOTAL_STORAGE, PRD.PR_QTY, PRD.PO_QTY, " + "MM.Material_Item_Name BRAND_NAME "
				+ "FROM PURCHASEREQUISITIONDETAIL PRD, " + "MATERIALITEMMSTR MM, " + "STOCKPURCHASEREQUISITION SPR "
				+ "WHERE  " + "PRD.stockpurchaserequisition_id IN [WHERE_IN] "
				+ "AND PRD.requisition_detail_status = 'PDS2' "
				+ "AND SPR.STOCKPURCHASEREQUISITION_ID = PRD.STOCKPURCHASEREQUISITION_ID "
				+ "AND PRD.MATERIALITEMMSTR_ID = MM.MATERIALITEMMSTR_ID "
				+ "ORDER BY PURCHASEREQUISITIONDETAIL_id DESC ";

		String sql3 = "SELECT BALANCE_QTY " + "as asTotalStorage " + "FROM STOREITEM " + "WHERE DEFUNCT_IND = 'N' "
				+ "AND MATERIALITEMMSTR_ID = ? " + "AND STOREITEM.STOREMSTR_ID = ? ";

		String sql4 = "SELECT SUM(BALANCE_QTY) " + "as asTotRSAmt " + "FROM STOREITEM " + "WHERE DEFUNCT_IND = 'N' "
				+ "AND MATERIALITEMMSTR_ID = ? ";

		String sql5 = "SELECT NVL(SUM(ISSUED_QTY),0) " + "as asStockTransferGantung " + "FROM STOCKISSUEDETAIL SID "
				+ "LEFT JOIN STOCKISSUE SI ON SI.STOCKISSUE_ID = SID.STOCKISSUE_ID " + "WHERE SI.ISSUE_STATUS = 'SIS2' "
				+ "AND SID.ISSUE_DETAIL_STATUS = 'SID2' " + "AND SI.STOCKRECEIPT_ID IS NULL "
				+ "AND SI.ISSUE_TYPE = 'SIT1' " + "AND SID.MATERIALITEMMSTR_ID = ? " + "AND SI.STOREMSTR_ID = ? "
				+ "GROUP BY SID.MATERIALITEMMSTR_ID";

		PreparedStatement ps = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		try {
			ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			rs = ps.executeQuery();

			String where_in = "(";
			while (rs.next()) {
				PurchaseRequisition dl = new PurchaseRequisition();
				dl.setStockpurchase_requisitionid(rs.getString("STOCKPURCHASEREQUISITION_ID"));
				dl.setStoremstr_id(rs.getString("STOREMSTR_ID"));
				dl.setPurchase_requisition_no(rs.getString("PURCHASE_REQUISITION_NO"));
				dl.setPurchase_status(rs.getString("PURCHASE_STATUS"));
				dl.setCreated_by(rs.getString("CREATED_BY"));
				dl.setCreated_datetime(rs.getString("CREATED_DATETIME"));
				dl.setRequested_by(rs.getString("requested_by"));
				dl.setRequested_datetime(rs.getString("requested_datetime"));
				dl.setPlanned_by(rs.getString("planned_by"));
				dl.setPlanned_datetime(rs.getString("planned_datetime"));
				dl.setCancelled_by(rs.getString("cancelled_by"));
				dl.setCancelled_datetime(rs.getString("cancelled_datetime"));
				dl.setRemarks(rs.getString("remarks"));
				dl.setPrev_updated_by(rs.getString("prev_updated_by"));
				dl.setLast_updated_by(rs.getString("last_updated_by"));
				dl.setPrev_updated_datetime(rs.getString("prev_updated_datetime"));
				dl.setLast_updated_datetime(rs.getString("last_updated_datetime"));
				dl.setStore_desc(rs.getString("store_desc"));
				dl.setStore_desc_lang1(rs.getString("store_desc_lang1"));
				dl.setCreated_by_desc(rs.getString("created_by_desc"));
				dl.setConfirmed_by_desc(rs.getString("confirmed_by_desc"));
				dl.setCode_desc(rs.getString("code_desc"));

				data.add(dl);

				if (rs.isLast()) {
					where_in = where_in + rs.getString("STOCKPURCHASEREQUISITION_ID") + ")";
				} else {
					where_in = where_in + rs.getString("STOCKPURCHASEREQUISITION_ID") + ",";
				}

			}

			if (!where_in.equals("(")) {
				st = connection.createStatement();
				rs2 = st.executeQuery(sql2.replace("[WHERE_IN]", where_in));

				BigDecimal TotalStorage = new BigDecimal(0);
				BigDecimal TotRSAmt = new BigDecimal(0);
				BigDecimal StockTransferGantung = new BigDecimal(0);

				while (rs2.next()) {

					PurchaseRequisitionDetail dl = new PurchaseRequisitionDetail();
					dl.setPurchaseRequisition_detai_id(rs2.getString("PURCHASEREQUISITIONDETAIL_ID"));
					dl.setRequisition_detail_status(rs2.getString("REQUISITION_DETAIL_STATUS"));
					dl.setMaterialitemmstr_id(rs2.getString("MATERIALITEMMSTR_ID"));
					dl.setRequisition_qty(rs2.getString("REQUISITION_QTY"));
					dl.setRequisition_uom(rs2.getString("REQUISITION_UOM"));
					dl.setUnit_price(rs2.getString("UNIT_PRICE"));
					dl.setVendormstr_id(rs2.getString("VENDORMSTR_ID"));
					dl.setCreated_by(rs2.getString("CREATED_BY"));
					dl.setCreated_datetime(rs2.getString("CREATED_DATETIME"));
					dl.setRemarks(rs2.getString("PURCHASEREQUISITIONDETAIL_ID"));
					dl.setPurchaseRequisition_detai_id(rs2.getString("REMARKS"));
					dl.setPrev_update_datetime(rs2.getString("PREV_UPDATED_DATETIME"));
					dl.setLast_update_datetime(rs2.getString("LAST_UPDATED_DATETIME"));
					dl.setManufacturer_vendormstr_id(rs2.getString("MANUFACTURER_VENDORMSTR_ID"));
					dl.setDiscount_rate(rs2.getString("DISCOUNT_RATE"));
					dl.setTotal_puchase_price(rs2.getString("TOTAL_PURCHASE_PRICE"));
					dl.setPpn_ind(rs2.getString("PPN_IND"));
					dl.setRequisition_qty_for_show(rs2.getString("REQUISITION_QTY_FOR_SHOW"));
					dl.setRequisition_uom_for_show(rs2.getString("REQUISITION_UOM_FOR_SHOW"));
					dl.setComplete_ind(rs2.getString("COMPLETE_IND"));
					dl.setComplete_remarks(rs2.getString("COMPLETE_REMARKS"));
					dl.setTotal_rs_amt(rs2.getString("TOTAL_RS_AMT"));
					dl.setMax_usage(rs2.getString("MAX_USAGE"));
					dl.setAverage_usage(rs2.getString("AVERAGE_USAGE"));
					dl.setTotal_storage(rs2.getString("TOTAL_STORAGE"));
					dl.setPr_qty(rs2.getString("PR_QTY"));
					dl.setPo_qty(rs2.getString("PO_QTY"));
					dl.setBrand_name(rs2.getString("BRAND_NAME"));

					ps3 = connection.prepareStatement(sql3);
					ps3.setEscapeProcessing(true);
					ps3.setQueryTimeout(60000);
					ps3.setBigDecimal(1, new BigDecimal(rs2.getString("MATERIALITEMMSTR_ID")));
					ps3.setBigDecimal(2, new BigDecimal(rs2.getString("STOREMSTR_ID")));
					rs3 = ps3.executeQuery();
					if (rs3.next())
						TotalStorage = rs3.getBigDecimal("asTotalStorage");

					ps4 = connection.prepareStatement(sql4);
					ps4.setEscapeProcessing(true);
					ps4.setQueryTimeout(60000);
					ps4.setBigDecimal(1, new BigDecimal(rs2.getString("MATERIALITEMMSTR_ID")));
					rs4 = ps4.executeQuery();
					if (rs4.next())
						TotRSAmt = rs4.getBigDecimal("asTotRSAmt");

					ps5 = connection.prepareStatement(sql5);
					ps5.setEscapeProcessing(true);
					ps5.setQueryTimeout(60000);
					ps5.setBigDecimal(1, new BigDecimal(rs2.getString("MATERIALITEMMSTR_ID")));
					ps5.setBigDecimal(2, new BigDecimal(rs2.getString("STOREMSTR_ID")));
					rs5 = ps5.executeQuery();
					if (rs5.next())
						StockTransferGantung = rs5.getBigDecimal("asStockTransferGantung");

					if (rs3 != null)try {rs3.close();} catch (Exception ignore) {}
					if (rs4 != null)try {rs4.close();} catch (Exception ignore) {}
					if (rs5 != null)try {rs5.close();} catch (Exception ignore) {}
					
					if (ps3 != null)try {ps3.close();} catch (Exception ignore) {}
					if (ps4 != null)try {ps4.close();} catch (Exception ignore) {}
					if (ps5 != null)try {ps5.close();} catch (Exception ignore) {}

					TotalStorage.add(StockTransferGantung);
					TotRSAmt.add(StockTransferGantung);

					dl.setTotal_storage(TotalStorage.toString());
					dl.setTotal_rs_amt(TotRSAmt.toString());

					for (PurchaseRequisition hd : data) {
						if (hd.getStockpurchase_requisitionid().equals(rs2.getString("STOCKPURCHASEREQUISITION_ID"))) {
							List<PurchaseRequisitionDetail> detail = hd.getPurchaseRequisitionDetail();
							if (detail == null)
								detail = new ArrayList<PurchaseRequisitionDetail>();
							detail.add(dl);
							hd.setPurchaseRequisitionDetail(detail);
						}
					}
				}
			}

		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)try {rs.close();} catch (Exception ignore) {}
			if (rs2 != null)try {rs2.close();} catch (Exception ignore) {}
			if (rs3 != null)try {rs3.close();} catch (Exception ignore) {}
			if (rs4 != null)try {rs4.close();} catch (Exception ignore) {}
			if (rs5 != null)try {rs5.close();} catch (Exception ignore) {}
			
			if (ps != null)try {ps.close();} catch (Exception ignore) {}
			if (st != null)try {st.close();} catch (Exception ignore) {}
			if (ps3 != null)try {ps3.close();} catch (Exception ignore) {}
			if (ps4 != null)try {ps4.close();} catch (Exception ignore) {}
			if (ps5 != null)try {ps5.close();} catch (Exception ignore) {}
			
			if (connection != null)try {connection.close();} catch (Exception ignore) {}
		}
		return data;
	}

	public static ResponseString setPurchaseRequisition(String stockpurchase_requisitionid, String isConfirm, String usermstrId) {
		ResponseString res = new ResponseString();
		String status = null;
		
		Connection connection = DbConnection.getPooledConnection();
		if (connection == null)return null;
		PreparedStatement ps = null;
		String sql = "UPDATE STOCKPURCHASEREQUISITION SET PURCHASE_STATUS = ? ";
		
		try {
			String sps_code_cancel = "SPS5";
			String sps_code_approve = "SPS2"; // SPS11 for partial approve

			if (isConfirm.equals("1") || isConfirm.equals("true")) {
				status = sps_code_approve;
				sql += ", APPROVED_DATETIME = SYSDATE , APPROVED_BY = ? ";
			}else{
				status = sps_code_cancel;
				sql += ", CANCELLED_DATETIME = SYSDATE , CANCELLED_BY = ? ";
			}
			sql += "WHERE STOCKPURCHASEREQUISITION_ID = ? ";

			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60);
			ps.setString(1, status);
			ps.setBigDecimal(2, new BigDecimal(usermstrId));
			ps.setBigDecimal(3, new BigDecimal(stockpurchase_requisitionid));
			ps.executeUpdate();
			
			res.setResponse("OK");
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
			res.setResponse("FAIL");
		} finally {
			if (ps != null)try {ps.close();} catch (Exception ignore) {}
			if (connection != null)try {connection.close();} catch (Exception ignore) {}
		}
		return res;
	}

	public static Object get_doctorlist_schedule(String poli) {

		List<DoctorListSchedule> data = new ArrayList<DoctorListSchedule>();

		Connection connection = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)return null;

		ResultSet rs = null;
		ResultSet rs2 = null;

		String sql = "SELECT NULL RESOURCESCHEME_ID, " + " (SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR "
				+ " WHERE SUBSPECIALTY_CODE = RM.POLI_CODE AND DEFUNCT_IND='N' ) POLI_CODE, " + "RM.POLI_CODE SUBSPECIALTY_CODE, "
				+ "RM.RESOURCEMSTR_ID, " + "RM.RESOURCE_CODE, " + "RM.RESOURCE_SHORT_CODE, "
				+ "RM.RESOURCE_QUICK_CODE, " + "RM.RESOURCE_NAME, " + "RM.RESOURCE_NAME_LANG1, "
				+ "RM.REGISTRATION_TYPE, " + "RM.SUBSPECIALTYMSTR_ID, " + "SSPM.SUBSPECIALTY_DESC, "
				+ "SSPM.SUBSPECIALTY_DESC_LANG1, " + "RM.CAREPROVIDER_ID, " + "cm.code_desc, "
				+ " RM.sequence as seq_doctor, cm.code_seq as seq_poli "
				+ "FROM RESOURCEMSTR RM, SUBSPECIALTYMSTR SSPM, CODEMSTR cm "
				+ "WHERE RM.POLI_CODE = cm.code_cat||cm.code_abbr AND "
				+ "RM.SUBSPECIALTYMSTR_ID = SSPM.SUBSPECIALTYMSTR_ID "
				+ "and ( exists(select 1 from RESOURCEPROFILE rp where RM.RESOURCEMSTR_ID = rp.resourcemstr_id AND (rp.EXPIRY_DATE > SYSDATE or rp.EXPIRY_DATE is null) )) "
				+ "AND RM.RESOURCE_SCHEME_IND = 'N' " + "AND RM.DEFUNCT_IND = 'N' " + "and RM.POLI_CODE = ? "
				+ "AND SSPM.SUBSPECIALTYMSTR_ID IN (336581902,311187819) ";

		String sql2 = "select rp.RESOURCEMSTR_ID, rp.MON_IND, rp.TUE_IND, rp.WED_IND, rp.THU_IND, rp.FRI_IND, rp.SAT_IND, rp.SUN_IND, sm.SESSION_DESC,cm.code_desc, RM.RESOURCE_NAME, RM.CAREPROVIDER_ID, CP.CAREPROVIDER_URL_IMAGE "
				+ " from RESOURCEPROFILE rp " + "inner join SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID "
				+ "left outer join RESOURCEMSTR RM on rp.Resourcemstr_Id = RM.RESOURCEMSTR_ID "
				+ "left outer join CAREPROVIDER CP ON RM.CAREPROVIDER_ID = CP.CAREPROVIDER_ID "
				+ "left outer join CODEMSTR cm ON RM.POLI_CODE = cm.code_cat||cm.code_abbr " + "where "
				+ "(rp.EXPIRY_DATE > SYSDATE or rp.EXPIRY_DATE is null) and " + "rp.defunct_ind = 'N' and "
				+ "rp.RESOURCEMSTR_ID IN [WHERE_IN] order by TO_CHAR(sm.start_time, 'hh24:mi:ss')";

		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		try {

			ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, poli);
			rs = ps.executeQuery();

			String where_in = "(";
			while (rs.next()) {
				DoctorListSchedule nmb = new DoctorListSchedule();
				nmb.setResourcescheme_id(rs.getString("RESOURCESCHEME_ID"));
				nmb.setPoli_code(rs.getString("POLI_CODE"));
				nmb.setResourcemstr_id(rs.getBigDecimal("RESOURCEMSTR_ID"));
				nmb.setResource_code(rs.getString("RESOURCE_CODE"));
				nmb.setResource_short_code(rs.getString("RESOURCE_SHORT_CODE"));
				nmb.setResource_quick_code(rs.getString("RESOURCE_QUICK_CODE"));
				nmb.setResource_name(rs.getString("RESOURCE_NAME"));
				nmb.setResource_name_lang1(rs.getString("RESOURCE_NAME_LANG1"));
				nmb.setRegistration_type(rs.getString("REGISTRATION_TYPE"));
				nmb.setSubspecialtymstr_id(rs.getBigDecimal("SUBSPECIALTYMSTR_ID"));
				nmb.setSubspecialty_desc(rs.getString("SUBSPECIALTY_DESC"));
				nmb.setSubspecialty_desc_lang1(rs.getString("SUBSPECIALTY_DESC_LANG1"));
				nmb.setCareprovider_id(rs.getBigDecimal("CAREPROVIDER_ID"));
				nmb.setCareprovider_id(rs.getBigDecimal("CAREPROVIDER_ID"));
				nmb.setSpecialist(rs.getString("CODE_DESC"));
				nmb.setSeq_doctor(rs.getString("seq_doctor"));
				nmb.setSeq_poli(rs.getString("seq_poli"));
				data.add(nmb);

				if (rs.isLast()) {
					where_in = where_in + nmb.getResourcemstr_id() + ")";
				} else {
					where_in = where_in + nmb.getResourcemstr_id() + ",";
				}
			}

			if (!where_in.equals("(")) {
				ps2 = connection.prepareStatement(sql2.replace("[WHERE_IN]", where_in));
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(60000);
				rs2 = ps2.executeQuery();

				while (rs2.next()) {
					HashMap<Integer, List<String>> schedule = new HashMap<Integer, List<String>>();
					schedule.put(1, new ArrayList<String>());
					schedule.put(2, new ArrayList<String>());
					schedule.put(3, new ArrayList<String>());
					schedule.put(4, new ArrayList<String>());
					schedule.put(5, new ArrayList<String>());
					schedule.put(6, new ArrayList<String>());
					schedule.put(7, new ArrayList<String>());

					for (DoctorListSchedule dl : data) {
						if (dl.getResourcemstr_id().compareTo(new BigDecimal(rs2.getString("RESOURCEMSTR_ID"))) == 0) {
							if (dl.getWeeklySchedule() != null) {
								schedule = dl.getWeeklySchedule().getSchedule();
							}

							WeeklyScheduleDoctor dl2 = new WeeklyScheduleDoctor();
							dl2.setDoctorname(rs2.getString("RESOURCE_NAME"));
							dl2.setSpecialist(rs2.getString("code_desc"));
							dl2.setCareprovider(rs2.getString("CAREPROVIDER_ID"));
							dl2.setDoctor_url_image(rs2.getString("CAREPROVIDER_URL_IMAGE"));

							List<String> jam = new ArrayList<String>();
							for (int i = 1; i <= schedule.size(); i++) {
								jam = schedule.get(i);

								if (rs2.getString("MON_IND").equals("Y") && i == 1)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("TUE_IND").equals("Y") && i == 2)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("WED_IND").equals("Y") && i == 3)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("THU_IND").equals("Y") && i == 4)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("FRI_IND").equals("Y") && i == 5)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("SAT_IND").equals("Y") && i == 6)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("SUN_IND").equals("Y") && i == 7)
									jam.add(rs2.getString("SESSION_DESC"));

								schedule.put(i, jam);

							}

							dl2.setSchedule(schedule);
							dl.setWeeklySchedule(dl2);
						}
					}
				}
			}

			// hapus data yang null
			for (Iterator<DoctorListSchedule> iter = data.iterator(); iter.hasNext();) {
				DoctorListSchedule dl = iter.next();
				if (dl.getWeeklySchedule() == null) {
					iter.remove();
				}
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)try {rs.close();} catch (Exception ignore) {}
			if (ps != null)try {ps.close();} catch (Exception ignore) {}
			if (connection != null)try {connection.close();} catch (Exception ignore) {}

			if (rs2 != null)try {rs2.close();} catch (Exception ignore) {}
			if (ps2 != null)try {ps2.close();} catch (Exception ignore) {}
		}

		return data;

	}

	public static Object getDoctorListScheduleByName(String name) {

		List<DoctorListSchedule> data = new ArrayList<DoctorListSchedule>();

		Connection connection = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;

		ResultSet rs = null;
		ResultSet rs2 = null;

		// validasi string input
		if (!StringUtils.isEmpty(name)) {
			name = name.toLowerCase();
			name.replace("!", "!!").replace("%", "!%").replace("_", "!_").replace("[", "![");
		}

		String sql = "SELECT NULL RESOURCESCHEME_ID, " + " (SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR "
				+ " WHERE SUBSPECIALTY_CODE = RM.POLI_CODE AND DEFUNCT_IND='N' ) POLI_CODE, " + "RM.POLI_CODE SUBSPECIALTY_CODE, "
				+ "RM.RESOURCEMSTR_ID, " + "RM.RESOURCE_CODE, " + "RM.RESOURCE_SHORT_CODE, "
				+ "RM.RESOURCE_QUICK_CODE, " + "RM.RESOURCE_NAME, " + "RM.RESOURCE_NAME_LANG1, "
				+ "RM.REGISTRATION_TYPE, " + "RM.SUBSPECIALTYMSTR_ID, " + "SSPM.SUBSPECIALTY_DESC, "
				+ "SSPM.SUBSPECIALTY_DESC_LANG1, " + "RM.CAREPROVIDER_ID, " + "cm.code_desc "
				+ "FROM RESOURCEMSTR RM, SUBSPECIALTYMSTR SSPM, CODEMSTR cm "
				+ "WHERE RM.POLI_CODE = cm.code_cat||cm.code_abbr AND "
				+ "RM.SUBSPECIALTYMSTR_ID = SSPM.SUBSPECIALTYMSTR_ID "
				+ "and ( exists(select 1 from RESOURCEPROFILE rp where RM.RESOURCEMSTR_ID = rp.resourcemstr_id AND (rp.EXPIRY_DATE > SYSDATE or rp.EXPIRY_DATE is null) )) "
				+ "AND RM.RESOURCE_SCHEME_IND = 'N' " + "AND RM.DEFUNCT_IND = 'N' "
				+ "and LOWER(RM.RESOURCE_NAME) like ? " + "AND SSPM.SUBSPECIALTYMSTR_ID IN (336581902,311187819) ";

		String sql2 = "select rp.RESOURCEMSTR_ID, rp.MON_IND, rp.TUE_IND, rp.WED_IND, rp.THU_IND, rp.FRI_IND, rp.SAT_IND, rp.SUN_IND, sm.SESSION_DESC,cm.code_desc, RM.RESOURCE_NAME, "
				+ " RM.CAREPROVIDER_ID, CP.CAREPROVIDER_URL_IMAGE, "
				+ " RM.sequence as seq_doctor, cm.code_seq as seq_poli "
				+ " from RESOURCEPROFILE rp " + "inner join SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID "
				+ "left outer join RESOURCEMSTR RM on rp.Resourcemstr_Id = RM.RESOURCEMSTR_ID "
				+ "left outer join CAREPROVIDER CP ON RM.CAREPROVIDER_ID = CP.CAREPROVIDER_ID "
				+ "left outer join CODEMSTR cm ON RM.POLI_CODE = cm.code_cat||cm.code_abbr " + "where "
				+ "(rp.EXPIRY_DATE > SYSDATE or rp.EXPIRY_DATE is null) and " + "rp.defunct_ind = 'N' and "
				+ "rp.RESOURCEMSTR_ID IN [WHERE_IN] order by TO_CHAR(sm.start_time, 'hh24:mi:ss')";

		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		try {

			ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, "%" + name + "%");
			rs = ps.executeQuery();

			String where_in = "(";
			while (rs.next()) {
				DoctorListSchedule nmb = new DoctorListSchedule();
				nmb.setResourcescheme_id(rs.getString("RESOURCESCHEME_ID"));
				nmb.setPoli_code(rs.getString("POLI_CODE"));
				nmb.setResourcemstr_id(rs.getBigDecimal("RESOURCEMSTR_ID"));
				nmb.setResource_code(rs.getString("RESOURCE_CODE"));
				nmb.setResource_short_code(rs.getString("RESOURCE_SHORT_CODE"));
				nmb.setResource_quick_code(rs.getString("RESOURCE_QUICK_CODE"));
				nmb.setResource_name(rs.getString("RESOURCE_NAME"));
				nmb.setResource_name_lang1(rs.getString("RESOURCE_NAME_LANG1"));
				nmb.setRegistration_type(rs.getString("REGISTRATION_TYPE"));
				nmb.setSubspecialtymstr_id(rs.getBigDecimal("SUBSPECIALTYMSTR_ID"));
				nmb.setSubspecialty_desc(rs.getString("SUBSPECIALTY_DESC"));
				nmb.setSubspecialty_desc_lang1(rs.getString("SUBSPECIALTY_DESC_LANG1"));
				nmb.setCareprovider_id(rs.getBigDecimal("CAREPROVIDER_ID"));
				nmb.setCareprovider_id(rs.getBigDecimal("CAREPROVIDER_ID"));
				nmb.setSpecialist(rs.getString("CODE_DESC"));
				data.add(nmb);

				if (rs.isLast()) {
					where_in = where_in + nmb.getResourcemstr_id() + ")";
				} else {
					where_in = where_in + nmb.getResourcemstr_id() + ",";
				}
			}

			if (!where_in.equals("(")) {
				ps2 = connection.prepareStatement(sql2.replace("[WHERE_IN]", where_in));
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(60000);
				rs2 = ps2.executeQuery();

				while (rs2.next()) {
					HashMap<Integer, List<String>> schedule = new HashMap<Integer, List<String>>();
					schedule.put(1, new ArrayList<String>());
					schedule.put(2, new ArrayList<String>());
					schedule.put(3, new ArrayList<String>());
					schedule.put(4, new ArrayList<String>());
					schedule.put(5, new ArrayList<String>());
					schedule.put(6, new ArrayList<String>());
					schedule.put(7, new ArrayList<String>());

					for (DoctorListSchedule dl : data) {
						if (dl.getResourcemstr_id().compareTo(new BigDecimal(rs2.getString("RESOURCEMSTR_ID"))) == 0) {
							if (dl.getWeeklySchedule() != null) {
								schedule = dl.getWeeklySchedule().getSchedule();
							}

							WeeklyScheduleDoctor dl2 = new WeeklyScheduleDoctor();
							dl2.setDoctorname(rs2.getString("RESOURCE_NAME"));
							dl2.setSpecialist(rs2.getString("code_desc"));
							dl2.setCareprovider(rs2.getString("CAREPROVIDER_ID"));
							dl2.setDoctor_url_image(rs2.getString("CAREPROVIDER_URL_IMAGE"));
							dl2.setSeq_doctor(rs2.getString("seq_doctor"));
							dl2.setSeq_poli(rs2.getString("seq_poli"));

							List<String> jam = new ArrayList<String>();
							for (int i = 1; i <= schedule.size(); i++) {
								jam = schedule.get(i);

								if (rs2.getString("MON_IND").equals("Y") && i == 1)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("TUE_IND").equals("Y") && i == 2)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("WED_IND").equals("Y") && i == 3)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("THU_IND").equals("Y") && i == 4)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("FRI_IND").equals("Y") && i == 5)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("SAT_IND").equals("Y") && i == 6)
									jam.add(rs2.getString("SESSION_DESC"));
								if (rs2.getString("SUN_IND").equals("Y") && i == 7)
									jam.add(rs2.getString("SESSION_DESC"));

								schedule.put(i, jam);

							}

							dl2.setSchedule(schedule);
							dl.setWeeklySchedule(dl2);
						}
					}
				}
			}

			// hapus data yang null
			for (Iterator<DoctorListSchedule> iter = data.iterator(); iter.hasNext();) {
				DoctorListSchedule dl = iter.next();
				if (dl.getWeeklySchedule() == null) {
					iter.remove();
				}
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}

			if (rs2 != null)
				try {
					rs2.close();
				} catch (Exception ignore) {
				}
			if (ps2 != null)
				try {
					ps2.close();
				} catch (Exception ignore) {
				}
		}

		return data;

	}

	public static List<ResourceList> GetDoctorsPolyName(String poli, String location) {
		System.out.println("GetDoctorsList...");
		List<ResourceList> data = new ArrayList<ResourceList>();
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;

		/*
		 * CUSTOME HARD CODE FOR 'HEMODIALISA' AND 'REKAP MEDIK'
		 */
		String subspecialt_custom = MobileBiz.CUSTOM_SUBSPECIALTYMSTR_ID_BPJS;
		if (location.equals(MobileBiz.POLI.toString())) {
			subspecialt_custom = MobileBiz.CUSTOM_SUBSPECIALTYMSTR_ID_GENERAL;
		}

		String resourcemstr_custom = MobileBiz.CUSTOM_RESOURCEMSTR_ID_BPJS;
		if (location.equals(MobileBiz.POLI.toString())) {
			resourcemstr_custom = MobileBiz.CUSTOM_RESOURCEMSTR_ID_GENERAL;
		}

		String query_filter = ""; 
		if (!resourcemstr_custom.equals("")) {
			query_filter = "RM.RESOURCEMSTR_ID IN (" + resourcemstr_custom + ") OR  ";
		}

		String sql = "SELECT NULL RESOURCESCHEME_ID, "
				+ "(SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR WHERE SUBSPECIALTY_CODE = RM.POLI_CODE AND DEFUNCT_IND='N' ) POLI_CODE, " 
				+ "RM.POLI_CODE SUBSPECIALTY_CODE, "
				+ "RM.RESOURCEMSTR_ID, "
				+ "RM.RESOURCE_CODE, " 
				+ "RM.RESOURCE_SHORT_CODE, "
				+ "RM.RESOURCE_QUICK_CODE, "
				+ "PER.PERSON_NAME RESOURCE_NAME, "
				+ "RM.RESOURCE_NAME_LANG1, "
				+ "RM.REGISTRATION_TYPE, " 
				+ "RM.SUBSPECIALTYMSTR_ID, " 
				+ "SSPM.SUBSPECIALTY_DESC, "
				+ "SSPM.SUBSPECIALTY_DESC_LANG1, " 
				+ "RM.CAREPROVIDER_ID, " 
				+ "CM.CODE_DESC "
				+ "FROM RESOURCEMSTR RM, SUBSPECIALTYMSTR SSPM, CODEMSTR CM, PERSON PER , CAREPROVIDER CP "
				+ "WHERE RM.POLI_CODE = CM.CODE_CAT||CM.CODE_ABBR "
				+ "AND RM.SUBSPECIALTYMSTR_ID = SSPM.SUBSPECIALTYMSTR_ID "
				+ "AND CP.CAREPROVIDER_ID = RM.CAREPROVIDER_ID "
				+ "AND CP.PERSON_ID = PER.PERSON_ID "
				+ "AND ( " +query_filter+ " exists(select 1 from RESOURCEPROFILE RP where RM.RESOURCEMSTR_ID = RP.RESOURCEMSTR_ID AND (RP.EXPIRY_DATE > SYSDATE or RP.EXPIRY_DATE is null) )) "
				+ "AND RM.RESOURCE_SCHEME_IND = 'N' " 
				+ "AND RM.DEFUNCT_IND = 'N' " 
				+ "AND CM.CODE_DESC = ? "
				+ "AND (SSPM.SUBSPECIALTYMSTR_ID = ? OR SSPM.SUBSPECIALTYMSTR_ID IN(" + subspecialt_custom + "))";

		try {
			st = connection.prepareStatement(sql);
			st.setString(1, poli);
			st.setBigDecimal(2, new BigDecimal(location));

			rs = st.executeQuery();
			while (rs.next()) {
				ResourceList nmb = new ResourceList();
				nmb.setResourcescheme_id(rs.getString("RESOURCESCHEME_ID"));
				nmb.setPoli_code(rs.getString("POLI_CODE"));
				nmb.setResourcemstr_id(rs.getBigDecimal("RESOURCEMSTR_ID"));
				nmb.setResource_code(rs.getString("RESOURCE_CODE"));
				nmb.setResource_short_code(rs.getString("RESOURCE_SHORT_CODE"));
				nmb.setResource_quick_code(rs.getString("RESOURCE_QUICK_CODE"));
				nmb.setResource_name(rs.getString("RESOURCE_NAME"));
				nmb.setResource_name_lang1(rs.getString("RESOURCE_NAME_LANG1"));
				nmb.setRegistration_type(rs.getString("REGISTRATION_TYPE"));
				nmb.setSubspecialtymstr_id(rs.getBigDecimal("SUBSPECIALTYMSTR_ID"));
				nmb.setSubspecialty_desc(rs.getString("SUBSPECIALTY_DESC"));
				nmb.setSubspecialty_desc_lang1(rs.getString("SUBSPECIALTY_DESC_LANG1"));
				nmb.setCareprovider_id(rs.getBigDecimal("CAREPROVIDER_ID"));
				nmb.setCareprovider_id(rs.getBigDecimal("CAREPROVIDER_ID"));
				nmb.setSpecialist(rs.getString("CODE_DESC"));
				data.add(nmb);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

} 