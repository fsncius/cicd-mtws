package com.rsmurniteguh.webservice.dep.kthis.services;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.activation.DataHandler;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.bouncycastle.mail.smime.examples.SendSignedAndEncryptedMail;
import org.json.JSONObject;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.tasks.OnSuccessListener;
import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.WebsiteBerita;
import com.rsmurniteguh.webservice.dep.all.model.mobile.AppointmentSource;
import com.rsmurniteguh.webservice.dep.all.model.mobile.AppointmentStatus;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataAppointment;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataRujukan;
import com.rsmurniteguh.webservice.dep.all.model.mobile.DataUser;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ListQueNum;
import com.rsmurniteguh.webservice.dep.all.model.mobile.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.mobile.WeeklySchedule;
import com.rsmurniteguh.webservice.dep.all.model.mobile.listCardNoBpjs;
import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.all.model.mobile.appointmentHistory;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.biz.QueueServiceBiz;
import com.rsmurniteguh.webservice.dep.kthis.services.email.TestingEmail;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.AES;
import com.rsmurniteguh.webservice.dep.util.DataUtils;
import com.rsmurniteguh.webservice.dep.util.EmailUtil;
import com.rsmurniteguh.webservice.dep.util.EncryptPatient;
import com.rsmurniteguh.webservice.dep.util.Encryptor;
import com.rsmurniteguh.webservice.dep.util.FileUploadUtil;

public class MobileAppointment extends EmailUtil {
	public static final String BASE_FOLDER_UPLOAD = getBaseConfig(IParameterConstant.MTWEBSITE_BASE_FOLDER_UPLOAD)+"\\antrian";
	private String bodyEmail;
	
	private static MobileAppointment action = new MobileAppointment();
	
	public static MobileAppointment getInstance(){
		return action;
	}
	
	private static String getBaseConfig(String parameter){
		CommonService cs = new CommonService();
		return cs.getParameterValue(parameter);
	}
	
	public static List<appointmentHistory> appointmentHistory(String user_id, String mrn, String lokasi) {
		System.out.println("appointmentHistory...");
		List<appointmentHistory> data = new ArrayList<appointmentHistory>();

		Connection connection = null;
		Connection connection2 = null;
		connection = DbConnection.getBpjsOnlineInstance();
		connection2 = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		if (connection2 == null)
			return null;
		PreparedStatement st = null;
		ResultSet rs = null;
		PreparedStatement st2 = null;
		ResultSet rs2 = null;

		String department = MobileBiz.DEPARTMENT_POLI;
		if (lokasi.equals(MobileBiz.BPJS.toString()))
			department = MobileBiz.DEPARTMENT_BPJS;

		String sql = "SELECT registrasi_id, queue_no, queue_date, resourcemstr_id, careprovider_id, doctor_name, no_mrn, user_id, generate_barcode, status, department,  "
				+ "priority = CASE" + "  WHEN queue_date = CONVERT(date,SYSDATETIME()) THEN 1"
				+ "  WHEN queue_date > CONVERT(date,SYSDATETIME())  THEN 2" + "  END " + "FROM tb_registrasi reg "
				+ "WHERE reg.user_id = ? and reg.no_mrn = ? and reg.deleted = 0 and (reg.status = ? OR reg.status = ? OR reg.status = ?)  AND reg.queue_date >= CONVERT(date,SYSDATETIME()) AND reg.department = ? "
				+ "ORDER by priority ASC , reg.queue_date asc";

		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, user_id);
			ps.setString(2, mrn);
			ps.setString(3, MobileBiz.APPOINTMENT_STATUS_CONFIRM);
			ps.setString(4, MobileBiz.APPOINTMENT_STATUS_REGISTERED);
			ps.setString(5, MobileBiz.APPOINTMENT_STATUS_WAIT);
			ps.setString(6, department);
			rs = ps.executeQuery();

			while (rs.next()) {
				appointmentHistory dl = new appointmentHistory();

				dl.setRegistrationCode(rs.getString("registrasi_id"));
				dl.setResourcemstr(rs.getString("resourcemstr_id"));
				dl.setCareprovide_id(rs.getString("careprovider_id"));
				dl.setDoctorName(rs.getString("doctor_name"));
				dl.setMrn(rs.getString("no_mrn"));
				dl.setUser_id(rs.getString("user_id"));
				dl.setRegistrationStatus(rs.getString("status"));
				dl.setBarcode(rs.getString("generate_barcode"));
				dl.setDateTreatment(rs.getString("queue_date"));
				dl.setDepartment(rs.getString("department"));
				dl.setQueue_no(rs.getString("queue_no"));
				dl.setReg_id(rs.getString("registrasi_id"));
				
				if (rs.getString("status").equals(MobileBiz.APPOINTMENT_STATUS_REGISTERED) && !rs.getString("generate_barcode").isEmpty() ) {
					BigDecimal subspecialtymstr = MobileBiz.POLI;
					if(dl.getDepartment().equals(MobileBiz.DEPARTMENT_BPJS)) subspecialtymstr = MobileBiz.BPJS;
					
					boolean isDone = isDoneAppointment(subspecialtymstr, rs.getString("careprovider_id"), rs.getString("queue_no"));
					if(isDone) dl.setRegistrationStatus(MobileBiz.APPOINTMENT_STATUS_DONE);
				}
				data.add(dl);
			}	
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		     
		     if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		     if (st2!=null) try  { st2.close(); } catch (Exception ignore){}
		     if (connection2!=null) try { connection2.close();} catch (Exception ignore){}
		   }
		return data;
	}
	
	public static List<appointmentHistory> appointmentList(String user_id, String mrn) {
		List<appointmentHistory> data = new ArrayList<appointmentHistory>();

		Connection connection = null;
		connection=DbConnection.getBpjsOnlineInstance();
		
		if(connection==null)return null;
		
		ResultSet rs = null;
		
		String sql="SELECT reg.registrasi_id, reg.queue_no, reg.queue_date, reg.resourcemstr_id, reg.careprovider_id, reg.doctor_name, reg.no_mrn, reg.user_id, reg.generate_barcode, reg.status, reg.department, doc.url_photo, "
				+ "priority = CASE" + 
					"  WHEN reg.queue_date = CONVERT(date,SYSDATETIME()) THEN 1" + 
					"  WHEN reg.queue_date > CONVERT(date,SYSDATETIME())  THEN 2" +
					"  END "
				+ "FROM tb_registrasi reg left join tb_doctor doc on doc.careprovider_id = reg.careprovider_id "
				+ "WHERE reg.user_id = ? and reg.no_mrn = ? and reg.deleted = 0 and (reg.status = ? OR reg.status = ? OR reg.status = ?)  AND reg.queue_date >= CONVERT(date,SYSDATETIME()) "
				+ "ORDER by priority ASC , reg.queue_date asc";
		
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, user_id);
			ps.setString(2, mrn);
			ps.setString(3, MobileBiz.APPOINTMENT_STATUS_CONFIRM);
			ps.setString(4, MobileBiz.APPOINTMENT_STATUS_REGISTERED);
			ps.setString(5, MobileBiz.APPOINTMENT_STATUS_WAIT);
			rs=ps.executeQuery();
			
			while(rs.next())
			{
				appointmentHistory dl=new appointmentHistory();
				
				dl.setRegistrationCode(rs.getString("registrasi_id"));
				dl.setResourcemstr(rs.getString("resourcemstr_id"));
				dl.setCareprovide_id(rs.getString("careprovider_id"));
				dl.setDoctorName(rs.getString("doctor_name"));
				dl.setMrn(rs.getString("no_mrn"));
				dl.setUser_id(rs.getString("user_id"));
				dl.setRegistrationStatus(rs.getString("status"));
				dl.setBarcode(rs.getString("generate_barcode"));
				dl.setDateTreatment(rs.getString("queue_date"));
				dl.setDepartment(rs.getString("department"));
				dl.setQueue_no(rs.getString("queue_no"));
				dl.setReg_id(rs.getString("registrasi_id"));
				dl.setUrl_photo(rs.getString("url_photo"));
				dl.setDoctorSchedule(MobileDoctor.GetDoctorSchedule(dl.getResourcemstr()));
				
				
				if (rs.getString("status").equals(MobileBiz.APPOINTMENT_STATUS_REGISTERED) && !rs.getString("generate_barcode").isEmpty() ) {
					BigDecimal subspecialtymstr = MobileBiz.POLI;
					if(dl.getDepartment().equals(MobileBiz.DEPARTMENT_BPJS)) subspecialtymstr = MobileBiz.BPJS;
					
					if (!dl.getQueue_no().isEmpty()) {
						dl.setQueues(getDoctorQueueList(dl.getCareprovide_id(), subspecialtymstr.toString()));
					}
					
					boolean isDone = isDoneAppointment(subspecialtymstr, rs.getString("careprovider_id"), rs.getString("queue_no"));
					if(isDone) dl.setRegistrationStatus(MobileBiz.APPOINTMENT_STATUS_DONE);
				}
				
				data.add(dl);
			}

		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static boolean isDoneAppointment(BigDecimal subspecialtymstr,String careprovider, String queueno){
		
		boolean ret = false;
		
		Connection connection=DbConnection.getPooledConnection();
		if(connection==null)return false;
		PreparedStatement st  = null;
		ResultSet rs  = null;
		
		
		try {
			String sql="SELECT VRT.CAREPROVIDER_ID, V.QUEUE_NO, V.VISIT_ID "
	                + " FROM VISITREGNTYPE VRT,VISIT V,SUBSPECIALTYMSTR SSM,RESOURCEMSTR RM,SESSIONMSTR SM, REGNVISITACTIVATION RVA, "
	                + " PATIENT PT,PERSON P1 WHERE V.VISIT_ID = RVA.VISIT_ID "
	                + " AND VRT.VISITREGNTYPE_ID =RVA.VISITREGNTYPE_ID AND V.ADMIT_STATUS <> 'AST5' "
	                + " AND SSM.SUBSPECIALTYMSTR_ID(+) = VRT.SUBSPECIALTYMSTR_ID "
	                + " AND RM.RESOURCEMSTR_ID(+)  = VRT.RESOURCEMSTR_ID "
	                + " AND SM.SESSIONMSTR_ID(+)  = VRT.SESSIONMSTR_ID "
	                + " AND (VRT.SUBSPECIALTYMSTR_ID =? OR VRT.SUBSPECIALTYMSTR_ID IS NULL ) "
	                + " AND V.ADMISSION_DATETIME >=pgregn.fxGetOPOMRegnValidityDateTime(VRT.REGISTRATION_TYPE) "
	                + " AND ( V.CONSULT_CAREPROVIDER_ID  in (?) OR ( V.CONSULT_CAREPROVIDER_ID IS NULL AND   "
	                + " (VRT.CAREPROVIDER_ID in (?) OR VRT.CAREPROVIDER_ID IS NULL))) "
	                + " AND VRT.CREATED_DATETIME >= TRUNC(SYSDATE) "
	                + " AND    VRT.CREATED_DATETIME <= TRUNC(SYSDATE) + 1 "
	                + " AND V.CONSULT_STATUS IN ('CNT3')  AND V.QUEUE_NO =?  "
	                + " AND PT.PATIENT_ID = V.PATIENT_ID AND P1.PERSON_ID = PT.PERSON_ID AND V.ADMISSION_DATETIME < TRUNC(SYSDATE) + 1 "
	                + " ORDER BY EFFICTIVE_DATETIME, CONSULT_START_TIME";
			
				st=connection.prepareStatement(sql);
				st.setBigDecimal(1, subspecialtymstr);
				st.setBigDecimal(2, new BigDecimal(careprovider));
				st.setBigDecimal(3, new BigDecimal(careprovider));
				st.setBigDecimal(4, new BigDecimal(queueno));
				rs=st.executeQuery();
				if(rs.next())
				{
					ret = true;
				}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (st!=null) try  { st.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		
		return ret;
	}

	public static ResponseString UserCreateApointment(String mrn, String resourcemstr, String user_id, String tanggal,
			String lokasi, String careprovider_id, String doctor_name, String no_subspecialis, String no_doctor, String patient_name, String no_mrn) {

		System.out.println("UserCreateApointment...");
		ResponseString res = new ResponseString();

		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;

		String cekuser = "SELECT username, iif(doctor_name IS NULL,'0','1') as isEver " 
				+ "FROM tb_user "
				+ "LEFT OUTER JOIN tb_registrasi ON tb_user.user_id = tb_registrasi.user_id AND tb_registrasi.queue_date = ? AND tb_registrasi.careprovider_id = ? AND tb_registrasi.deleted = 0 AND (tb_registrasi.status = ? OR tb_registrasi.status = ?) "
				+ "WHERE tb_user.user_id = ? AND tb_user.no_mrn = ? ";
		String sql = "INSERT tb_registrasi "
				+ "(queue_date,resourcemstr_id,careprovider_id,doctor_name,no_mrn,user_id,status,department,source,deleted,created_at,reference_date,queue_no) "
				+ "values (?,?,?,?,?,?,?,?,?,0,SYSDATETIME(),SYSDATETIME(),?)";
		String sql2 = "SELECT 1 FROM tb_rujukan WHERE DATEADD(MONTH,1,reference_date)>= CONVERT(DATE, GETDATE(), 120) AND no_mrn = ? ";
		String sql4 = "SELECT 1 FROM tb_registrasi WHERE queue_date = ? AND no_mrn = ? AND department = ? AND status IN(?,?,?) ";
		String sql5 = "select CD.CARD_NO, PS.PERSON_NAME, PT.BPJS_NO from CARD CD, PATIENT PT, PERSON PS where "
				+ "CD.CARD_NO=? AND " + "PT.PERSON_ID = CD.PERSON_ID AND PS.PERSON_ID=PT.PERSON_ID";

		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		ResultSet rs4 = null;
		ResultSet generatedKeys = null;

		try {
			ps = connection.prepareStatement(cekuser);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tanggal);
			ps.setString(2, careprovider_id);
			ps.setString(3, MobileBiz.APPOINTMENT_STATUS_CONFIRM);
			ps.setString(4, MobileBiz.APPOINTMENT_STATUS_WAIT);
			ps.setString(5, user_id);
			ps.setString(6, mrn);
			rs = ps.executeQuery();
			boolean isUser = rs.next(); // dipakai untuk validasi

			// get available data
			WeeklySchedule data = MobileDoctor.GetDoctorSchedule(resourcemstr);

			// return day of date
			Date date_appointment = new SimpleDateFormat("yyyy-M-d").parse(tanggal);
			String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date_appointment);
			String available = "N";
			if (dayOfWeek.equals("Sunday") && data.getSunday() != null)
				available = "Y";
			if (dayOfWeek.equals("Monday") && data.getMonday() != null)
				available = "Y";
			if (dayOfWeek.equals("Tuesday") && data.getTuesday() != null)
				available = "Y";
			if (dayOfWeek.equals("Wednesday") && data.getWednesday() != null)
				available = "Y";
			if (dayOfWeek.equals("Thursday") && data.getThursday() != null)
				available = "Y";
			if (dayOfWeek.equals("Friday") && data.getFriday() != null)
				available = "Y";
			if (dayOfWeek.equals("Saturday") && data.getSaturday() != null)
				available = "Y";

			// assign department var
			String department = MobileBiz.DEPARTMENT_POLI;
			boolean isRujukan = true;
			boolean isAppointment = true;
			boolean isAppointmentDepartment = true;
			boolean isNotEverAppointmentSameDay = true;
			if (lokasi.equals(MobileBiz.BPJS.toString())) {
				department = MobileBiz.DEPARTMENT_BPJS;

				// jika BPJS maka cek apakah sudah ada rujukan yang valid nya
				ps2 = connection.prepareStatement(sql2);
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(60000);
				ps2.setString(1, mrn);
				rs2 = ps2.executeQuery();
				isRujukan = rs2.next(); // dipakai untuk validasi

				// hanya boleh sekali appointment / hari (BPJS)
				ps4 = connection.prepareStatement(sql4);
				ps4.setEscapeProcessing(true);
				ps4.setQueryTimeout(60000);
				ps4.setString(1, tanggal);
				ps4.setString(2, mrn);
				ps4.setString(3, MobileBiz.DEPARTMENT_BPJS);
				ps4.setString(4, MobileBiz.APPOINTMENT_STATUS_CONFIRM);
				ps4.setString(5, MobileBiz.APPOINTMENT_STATUS_REGISTERED);
				ps4.setString(6, MobileBiz.APPOINTMENT_STATUS_WAIT);
				rs4 = ps4.executeQuery();
				if (rs4.next())
					isNotEverAppointmentSameDay = false; // validasi apakah
															// ditanggal
															// appointment itu
															// sudah pernah
															// daftar sebelumnya

				// Apakah no BPJS ada?
				listCardNoBpjs lsnb = getCardNo(mrn);
				if (lsnb.getBPJS_NO() == null) {
					isAppointmentDepartment = false;
				}

				Calendar lusa = Calendar.getInstance(); // current date and time
				lusa.add(Calendar.DAY_OF_MONTH, 1);
				lusa.set(Calendar.HOUR, 0);
				lusa.set(Calendar.MINUTE, 0);
				lusa.set(Calendar.SECOND, 0);
				lusa.set(Calendar.MILLISECOND, 0);

				// apakah tanggal diperbolehkan
				if (date_appointment.before(lusa.getTime())) {
					isAppointment = false;
				}
			}

			// pengecekan seluruhnya
			if (isUser && available.equals("Y") && rs.getString("isEver").equals("0") && isRujukan && isAppointment
					&& isNotEverAppointmentSameDay && isAppointmentDepartment) {				
					
				ps3 = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps3.setEscapeProcessing(true);
				ps3.setQueryTimeout(60000);
				ps3.setString(1, tanggal);
				ps3.setString(2, resourcemstr);
				ps3.setString(3, careprovider_id);
				ps3.setString(4, doctor_name);
				ps3.setString(5, mrn);
				ps3.setString(6, user_id);
				ps3.setString(7, MobileBiz.APPOINTMENT_STATUS_WAIT);
				ps3.setString(8, department);
				ps3.setString(9, MobileBiz.MOBILE_SOURCE);
				ps3.setString(10, "");
				
				if(lokasi.equals(MobileBiz.POLI.toString())){
					ResponseStatus queue_id = QueueService.RequestIDTicket(resourcemstr, QueueServiceBiz.REGISTRASI_GENERAL, tanggal, QueueServiceBiz.SOURCE_MOBILE, patient_name, no_mrn, QueueServiceBiz.QUEUE_TYPE_REGULER);				
					ResponseStatus queue_no = QueueService.RequestTicket(QueueServiceBiz.REGISTRASI_GENERAL, no_subspecialis, no_doctor, queue_id.getMessage(), tanggal);
					ps3.setString(10, queue_no.getMessage());	
				}
						
				
				// masukan queue_no
				ps3.executeUpdate();

				if (department.equals(MobileBiz.DEPARTMENT_POLI)) {
					generatedKeys = ps3.getGeneratedKeys();
					if (generatedKeys.next()) {
						MobileAuth.insertBarcode(generatedKeys.getString(1));
					} else {
						throw new SQLException("Creating generate barcode failed, no generated key obtained.");
					}
				}

				res.setResponse("OK");

				ps.close();
				rs.close();
			} else {
				if (!isUser) {
					res.setResponse("Anda harus masuk sebagai pasien");
				} else if (!isRujukan) {
					res.setResponse("Rujukan belum ada atau tidak valid");
				} else if (!isAppointment) {
					res.setResponse("Tanggal perjanjian salah");
				} else if (!isNotEverAppointmentSameDay) {
					res.setResponse("Anda sudah membuat perjanjian ditanggal tersebut");
				} else if (!isAppointmentDepartment) {
					res.setResponse("Anda tidak terdaftar di departemen ini");
				} else if (!available.equals("Y")) {
					res.setResponse("Tanggal tersebut tidak tersedia untuk dokter ini");
				} else if (rs.getString("isEver").equals("1")) {
					res.setResponse("Anda sudah membuat perjanjian dengan dokter ini");
				}
			}
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());

		} catch (ParseException e) {
			System.out.print(e.getMessage());
		} finally {
			if (rs != null)	try {	rs.close();	} catch (Exception ignore) {}
			if (ps != null)	try {	ps.close();	} catch (Exception ignore) {}
			if (rs2 != null)try {	rs2.close();} catch (Exception ignore) {}
			if (ps2 != null)try {	ps2.close();} catch (Exception ignore) {}
			if (rs4 != null)try {	rs4.close();} catch (Exception ignore) {}
			if (ps3 != null)try {	ps3.close();} catch (Exception ignore) {}
			if (ps4 != null)try { 	ps4.close();} catch (Exception ignore) {}
			if (generatedKeys != null)try {	generatedKeys.close();} catch (Exception ignore) {}
			if (connection != null)	try {connection.close();} catch (Exception ignore) {}
		}
		return res;
	}

	public static listCardNoBpjs getCardNo(String mrn) {
		listCardNoBpjs res = new listCardNoBpjs();

		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;

		String sql = "select PS.PERSON_NAME, PT.BPJS_NO  from CARD CD, PATIENT PT, PERSON PS where CD.CARD_NO='" + mrn
				+ "'" + " AND PT.PERSON_ID=CD.PERSON_ID AND PS.PERSON_ID=PT.PERSON_ID";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(sql);
			stmt.setEscapeProcessing(true);
			stmt.setQueryTimeout(60000);
			if (haspas.next()) {
				res.setPERSON_NAME(haspas.getString("PERSON_NAME"));
				res.setBPJS_NO(haspas.getString("BPJS_NO"));
			}

			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)	try { haspas.close();		} catch (Exception ignore) {}
			if (stmt != null)try {	stmt.close();	} catch (Exception ignore) {}
			if (connection != null)	try {	connection.close();	} catch (Exception ignore) {}
		}
		return res;
	}

	public static Object cancelAppointment(String reg_id) {
		System.out.println("cancelAppointment...");
		ResponseString res = new ResponseString();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		String SPsql = "update tb_registrasi set status = ? where registrasi_id = ? and deleted = 0";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, MobileBiz.APPOINTMENT_STATUS_CANCEL);
			ps.setString(2, reg_id);

			ps.executeUpdate();
			res.setResponse("OK");
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
			res.setResponse("FAIL");
		} finally {
			if (ps != null)	try {	ps.close();	} catch (Exception ignore) {}
			if (connection != null)	try {	connection.close();	} catch (Exception ignore) {}
		}
		return res;
	}

	public static ResponseString checkAvailable(String tanggal, String user_id, String careprovider_id, String resourcemstr) {
		System.out.println("checkAvailable...");
		ResponseString res = new ResponseString();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;

		String cekuser = "SELECT username, iif(doctor_name IS NULL,'0','1') as isEver " + "FROM tb_user "
				+ "LEFT OUTER JOIN tb_registrasi ON tb_user.user_id = tb_registrasi.user_id AND tb_registrasi.queue_date = ? AND tb_registrasi.careprovider_id = ? AND tb_registrasi.deleted = 0 AND (tb_registrasi.status = 'Confirm' OR tb_registrasi.status = 'Wait') "
				+ "WHERE tb_user.user_id = ? ";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = connection.prepareStatement(cekuser);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tanggal);
			ps.setString(2, careprovider_id);
			ps.setString(3, user_id);
			rs = ps.executeQuery();

			// get available data
			WeeklySchedule data = MobileDoctor.GetDoctorSchedule(resourcemstr);

			// return day of date
			Date date = new SimpleDateFormat("yyyy-M-d").parse(tanggal);
			String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date);
			String available = "N";

			if (dayOfWeek.equals("Sunday") && data.getSunday() != null)
				available = "Y";
			if (dayOfWeek.equals("Monday") && data.getMonday() != null)
				available = "Y";
			if (dayOfWeek.equals("Tuesday") && data.getTuesday() != null)
				available = "Y";
			if (dayOfWeek.equals("Wednesday") && data.getWednesday() != null)
				available = "Y";
			if (dayOfWeek.equals("Thursday") && data.getThursday() != null)
				available = "Y";
			if (dayOfWeek.equals("Friday") && data.getFriday() != null)
				available = "Y";
			if (dayOfWeek.equals("Saturday") && data.getSaturday() != null)
				available = "Y";

			boolean rs_data = rs.next();
			if (!rs_data) {
				res.setResponse("Anda harus login sebagai pasien");
			} else if (!available.equals("Y")) {
				res.setResponse("Tanggal tersebut tidak tersedia untuk dokter ini");
			} else if (rs.getString("isEver").equals("1")) {
				res.setResponse("Anda sudah membuat perjanjian ditanggal tersebut");
			} else {
				res.setResponse("OK");
			}

		}

		catch (SQLException e) {
			System.out.println(e.getMessage());

		} catch (ParseException e) {
			System.out.print(e.getMessage());
		} finally {
			if (rs != null)	try {	rs.close();} catch (Exception ignore) {}
			if (ps != null)	try {ps.close();} catch (Exception ignore) {}
			if (connection != null)	try {connection.close();} catch (Exception ignore) {}
		}
		return res;
	}

	public static appointmentHistory InfoRegistration(String registrasi_id) {
		System.out.println("InfoRegistration...");
		appointmentHistory data = new appointmentHistory();

		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;
		String sql = "SELECT * FROM tb_registrasi WHERE registrasi_id = ? ";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, registrasi_id);
			rs = ps.executeQuery();

			while (rs.next()) {
				data.setRegistrationCode(rs.getString("registrasi_id"));
				data.setResourcemstr(rs.getString("resourcemstr_id"));
				data.setCareprovide_id(rs.getString("careprovider_id"));
				data.setDoctorName(rs.getString("doctor_name"));
				data.setMrn(rs.getString("no_mrn"));
				data.setUser_id(rs.getString("user_id"));
				data.setRegistrationStatus(rs.getString("status"));
				data.setBarcode(rs.getString("generate_barcode"));
				data.setDateTreatment(rs.getString("queue_date"));
				data.setQueue_no(rs.getString("queue_no"));
				data.setDepartment(rs.getString("department"));
			}

		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static List<ListQueNum> getDoctorQueueList(String careproviderId, String subspecialtymstr) {

		System.out.println("getDoctorQueueList...");
		List<ListQueNum> data = new ArrayList<ListQueNum>();
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		String sql = "SELECT VRT.CAREPROVIDER_ID, V.QUEUE_NO, V.VISIT_ID "
				+ " FROM VISITREGNTYPE VRT,VISIT V,SUBSPECIALTYMSTR SSM,RESOURCEMSTR RM,SESSIONMSTR SM, REGNVISITACTIVATION RVA, "
				+ " PATIENT PT,PERSON P1 WHERE V.VISIT_ID = RVA.VISIT_ID "
				+ " AND VRT.VISITREGNTYPE_ID =RVA.VISITREGNTYPE_ID AND V.ADMIT_STATUS <> 'AST5' "
				+ " AND SSM.SUBSPECIALTYMSTR_ID(+) = VRT.SUBSPECIALTYMSTR_ID "
				+ " AND RM.RESOURCEMSTR_ID(+)  = VRT.RESOURCEMSTR_ID "
				+ " AND SM.SESSIONMSTR_ID(+)  = VRT.SESSIONMSTR_ID "
				+ " AND (VRT.SUBSPECIALTYMSTR_ID = ? OR VRT.SUBSPECIALTYMSTR_ID IS NULL ) "
				+ " AND V.ADMISSION_DATETIME >=pgregn.fxGetOPOMRegnValidityDateTime(VRT.REGISTRATION_TYPE) "
				+ " AND ( V.CONSULT_CAREPROVIDER_ID  in (?) OR ( V.CONSULT_CAREPROVIDER_ID IS NULL AND   "
				+ " (VRT.CAREPROVIDER_ID in (?) OR VRT.CAREPROVIDER_ID IS NULL))) "
				+ " AND VRT.CREATED_DATETIME >= TRUNC(SYSDATE) " + " AND    VRT.CREATED_DATETIME <= TRUNC(SYSDATE) + 1 "
				+ " AND V.CONSULT_STATUS IN ('CNT1') "
				+ " AND PT.PATIENT_ID = V.PATIENT_ID AND P1.PERSON_ID = PT.PERSON_ID AND V.ADMISSION_DATETIME < TRUNC(SYSDATE) + 1 "
				+ " ORDER BY EFFICTIVE_DATETIME, CONSULT_START_TIME";
		try {
			st = connection.prepareStatement(sql);
			st.setBigDecimal(1, new BigDecimal(subspecialtymstr));
			st.setBigDecimal(2, new BigDecimal(careproviderId));
			st.setBigDecimal(3, new BigDecimal(careproviderId));
			rs = st.executeQuery();
			while (rs.next()) {
				ListQueNum lq = new ListQueNum();
				lq.setCareproviderid(rs.getString("careprovider_id"));
				lq.setVisitid(rs.getString("visit_id"));
				lq.setQueueno(rs.getString("queue_no"));
				data.add(lq);

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static List<AppointmentSource> getAllAppointmentByStatus(String tanggal, String status) {

		System.out.println("getAllAppointmentByStatus...");

		List<AppointmentStatus> temp = new ArrayList<AppointmentStatus>();
		List<AppointmentSource> data = new ArrayList<AppointmentSource>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;

		String filter_source = "";
		if (status.equals("mobile"))
			status = MobileBiz.MOBILE_SOURCE;
		if (status.equals("desktop"))
			status = MobileBiz.DESKTOP_SOURCE;
		if (status.equals("web"))
			status = MobileBiz.WEB_SOURCE;

		if (status != null && !status.equals("all"))
			filter_source = "AND source = '" + status + "' ";

		String sql = "SELECT distinct convert(varchar,queue_date,111) as tanggal, count(registrasi_id) as count_patient, careprovider_id, doctor_name, status , department,source "
				+ "FROM tb_registrasi " + "WHERE queue_date = ? " + filter_source
				+ "group by convert(varchar,queue_date,111), careprovider_id, doctor_name, status, department,source "
				+ "order by count_patient DESC";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tanggal);
			rs = ps.executeQuery();

			while (rs.next()) {
				AppointmentStatus dl = new AppointmentStatus();
				dl.setTanggal(rs.getString("tanggal"));
				dl.setCount_patient(rs.getString("count_patient"));
				dl.setDoctor_name(rs.getString("doctor_name"));
				dl.setStatus(rs.getString("status"));
				dl.setDepartment(rs.getString("department"));
				dl.setSource(rs.getString("source"));
				dl.setCareprovider_id(rs.getString("careprovider_id"));
				temp.add(dl);

				// System.out.println(dl.getTanggal()+"|"+dl.getDoctor_name()+"|"+dl.getDepartment()+"|"+rs.getString("count_patient"));
			}

			Map<String, AppointmentSource> all = new HashMap<String, AppointmentSource>();
			for (AppointmentStatus as : temp) {
				if (all.get(as.getCareprovider_id() + as.getDepartment()) == null) {
					AppointmentSource dl = new AppointmentSource();
					dl.setTanggal(as.getTanggal());
					dl.setDoctor_name(as.getDoctor_name());
					dl.setDepartment(as.getDepartment());
					dl.setSource(as.getSource());
					dl.setRegistered(new BigDecimal(0));
					dl.setConfirm(new BigDecimal(0));
					dl.setWait(new BigDecimal(0));
					dl.setCancel(new BigDecimal(0));
					all.put(as.getCareprovider_id() + as.getDepartment(), dl);
				}

				AppointmentSource update = all.get(as.getCareprovider_id() + as.getDepartment());
				if (as.getStatus().equals(MobileBiz.APPOINTMENT_STATUS_REGISTERED)) {
					update.setRegistered(update.getRegistered().add(new BigDecimal(as.getCount_patient())));
				} else if (as.getStatus().equals(MobileBiz.APPOINTMENT_STATUS_CONFIRM)) {
					update.setConfirm(update.getConfirm().add(new BigDecimal(as.getCount_patient())));
				} else if (as.getStatus().equals(MobileBiz.APPOINTMENT_STATUS_WAIT)) {
					update.setWait(update.getWait().add(new BigDecimal(as.getCount_patient())));
				} else if (as.getStatus().equals(MobileBiz.APPOINTMENT_STATUS_CANCEL)
						|| as.getStatus().equals("Delete")) {
					update.setCancel(update.getCancel().add(new BigDecimal(as.getCount_patient())));
				}
				all.put(as.getCareprovider_id() + as.getDepartment(), update);

			}

			for (Map.Entry<String, AppointmentSource> as : all.entrySet()) {
				data.add(as.getValue());
			}

		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static ResponseString notificationAppointment(String tanggal) {
		System.out.println("notificationAppointment...");
		ResponseString res = new ResponseString();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;
		String sql = "SELECT r.queue_date, r.user_id,r.doctor_name, token.firebase_token " + "   FROM tb_registrasi r "
				+ "    INNER JOIN (select Row_number() OVER (partition by t.usermstr_id order by t.created_at desc) as num, t.usermstr_id, t.firebase_token,t.created_at from usertoken t where t.firebase_token is not null ) token ON r.user_id = token.usermstr_id and num = 1 "
				+ "	WHERE r.queue_date = ? AND r.status = 'Confirm' "
				+ "	GROUP BY r.queue_date,  r.user_id, r.doctor_name, token.firebase_token ";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tanggal);
			rs = ps.executeQuery();

			while (rs.next()) {
				try {

					URL url = new URL(MobileBiz.FMCurl);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();

					conn.setUseCaches(false);
					conn.setDoInput(true);
					conn.setDoOutput(true);

					conn.setRequestMethod("POST");
					conn.setRequestProperty("Authorization", "key=" + MobileBiz.FMKey);
					conn.setRequestProperty("Content-Type", "application/json");

					JSONObject data = new JSONObject();
					data.put("to", rs.getString("firebase_token").trim());
					JSONObject info = new JSONObject();
					info.put("title", "Appointment Reminder"); // Notification
																// title
					info.put("body", "Dont forget to visit " + rs.getString("doctor_name") + " today"); // Notification
																										// body
					info.put("activity", "appointmenthistory");
					data.put("data", info);

					OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
					wr.write(data.toString());
					wr.flush();
					wr.close();

					int responseCode = conn.getResponseCode();
					// System.out.println("Response Code : " + responseCode);

					BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();

					res.setResponse(response.toString());
					// System.out.println(response.toString());
				} catch (Exception e) {
					res.setResponse(e.getMessage());
				}

			}

		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return res;
	}

	public static ResponseString notificationDirect(String message, String user_id) {
		System.out.println("notificationAppointment...");
		ResponseString res = new ResponseString();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;
		String sql = "SELECT TOP(1) firebase_token  FROM usertoken where usermstr_id = ? order by usertoken_id desc";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, user_id);
			rs = ps.executeQuery();

			while (rs.next()) {
				try {

					URL url = new URL(MobileBiz.FMCurl);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();

					conn.setUseCaches(false);
					conn.setDoInput(true);
					conn.setDoOutput(true);

					conn.setRequestMethod("POST");
					conn.setRequestProperty("Authorization", "key=" + MobileBiz.FMKey);
					conn.setRequestProperty("Content-Type", "application/json");

					JSONObject data = new JSONObject();
					data.put("to", rs.getString("firebase_token").trim());
					JSONObject info = new JSONObject();
					info.put("title", "Murni Teguh Memorial Hospital"); // Notification
																		// title
					info.put("body", message); // Notification body
					data.put("data", info);

					OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
					wr.write(data.toString());
					wr.flush();
					wr.close();

					int responseCode = conn.getResponseCode();
					// System.out.println("Response Code : " + responseCode);

					BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();

					res.setResponse(response.toString());
				} catch (Exception e) {
					e.getMessage();
				}

			}

		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return res;
	}

	public static List<AppointmentStatus> topRegisteredAppointment(String tanggal) {
		System.out.println("topRegisteredAppointment...");

		List<AppointmentStatus> data = new ArrayList<AppointmentStatus>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;
		ResultSet rs2 = null;

		String sql = "  SELECT TOP(5)  convert(varchar,queue_date,111) as tanggal, count(careprovider_id) as count_patient, doctor_name, department "
				+ "  FROM tb_registrasi " + "  WHERE queue_date = ? AND status =? AND department='"
				+ MobileBiz.DEPARTMENT_BPJS + "' "
				+ "  group by convert(varchar,queue_date,111), doctor_name, department, status "
				+ "  HAVING count(careprovider_id)>0  " + "  order by count(careprovider_id) DESC ";

		String sql2 = "  SELECT TOP(5)  convert(varchar,queue_date,111) as tanggal, count(careprovider_id) as count_patient, doctor_name, department "
				+ "  FROM tb_registrasi " + "  WHERE queue_date = ? AND status =? AND department='"
				+ MobileBiz.DEPARTMENT_POLI + "' "
				+ "  group by convert(varchar,queue_date,111), doctor_name, department, status "
				+ "  HAVING count(careprovider_id)>0  " + "  order by count(careprovider_id) DESC ";

		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(30000);
			ps.setString(1, tanggal);
			ps.setString(2, MobileBiz.APPOINTMENT_STATUS_REGISTERED);
			rs = ps.executeQuery();

			while (rs.next()) {
				AppointmentStatus dl = new AppointmentStatus();
				dl.setTanggal(rs.getString("tanggal"));
				dl.setCount_patient(rs.getString("count_patient"));
				dl.setDoctor_name(rs.getString("doctor_name"));
				dl.setDepartment(rs.getString("department"));
				data.add(dl);
			}

			ps2 = connection.prepareStatement(sql2);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(30000);
			ps2.setString(1, tanggal);
			ps2.setString(2, MobileBiz.APPOINTMENT_STATUS_REGISTERED);
			rs2 = ps2.executeQuery();

			while (rs2.next()) {
				AppointmentStatus dl2 = new AppointmentStatus();
				dl2.setTanggal(rs2.getString("tanggal"));
				dl2.setCount_patient(rs2.getString("count_patient"));
				dl2.setDoctor_name(rs2.getString("doctor_name"));
				dl2.setDepartment(rs2.getString("department"));
				data.add(dl2);
			}

		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}

			if (rs2 != null)
				try {
					rs2.close();
				} catch (Exception ignore) {
				}
			if (ps2 != null)
				try {
					ps2.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static List<DataRujukan> getDataRujukan(String mrn) {
		List<DataRujukan> data = new ArrayList<DataRujukan>();

		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		String sql = "select reference_id, no_mrn, images, reference_date, reference, created_at,"
				+ "DATEADD(month, 1, reference_date) as expired_date,"
				// validasi status
				+ "CAST(" + "case "
				+ "when dateadd(month, 1, reference_date) < sysdatetime() then 'Expired' " + "else 'Valid'"
				+ "end as varchar(12)) as status" + " from tb_rujukan "

				+ "where "
				// + "DATEADD(month, 1, reference_date) > SYSDATETIME() AND
				+ "no_mrn = ? " + "AND" + " images IS NOT NULL " + " order by reference_id desc ";

		String sql2 = "select reference_add_id,no_mrn, gambar, created_date from tb_rujukan_tambahan "
				+ "where no_mrn = ? AND DATEADD(month, 1, created_date) > CONVERT(DATE, GETDATE(), 120) "
				+ "AND gambar IS NOT NULL " + " order by reference_add_id desc ";

		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, mrn);
			rs = ps.executeQuery();

			while (rs.next()) {
				DataRujukan dl = new DataRujukan();
				dl.setReference_id(rs.getString("reference_id"));
				dl.setMrn(rs.getString("no_mrn"));
				dl.setImages(rs.getString("images"));
				dl.setReference_date(rs.getString("reference_date"));
				dl.setExpired_date(rs.getString("expired_date"));
				dl.setReference(rs.getString("reference"));
				dl.setCreatedate(rs.getString("created_at"));
				dl.setType(MobileBiz.mRujukan);
				dl.setStatus(rs.getString("status"));
				data.add(dl);
			}

			ps2 = connection.prepareStatement(sql2);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(60000);
			ps2.setString(1, mrn);
			rs2 = ps2.executeQuery();

			while (rs2.next()) {
				DataRujukan dl = new DataRujukan();
				dl.setReference_id(rs2.getString("reference_add_id"));
				dl.setMrn(rs2.getString("no_mrn"));
				dl.setImages(rs2.getString("gambar"));
				dl.setCreatedate(rs2.getString("created_date"));
				dl.setType(MobileBiz.mRujukanTambahan);
				data.add(dl);
			}

		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (rs2 != null)
				try {
					rs2.close();
				} catch (Exception ignore) {
				}
			if (ps2 != null)
				try {
					ps2.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;
	}

	public static ResponseString addRujukan(String mrn, String userid, String no_rujukan, String tanggal_rujukan,
			String gambar) {
		ResponseString res = new ResponseString();
		// DataHandler dataHandler = attachment.getDataHandler();

		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;

		String sql = "INSERT tb_rujukan_tambahan (no_mrn,gambar,created_date) values (?,?,SYSDATETIME())";
		if (!no_rujukan.isEmpty() && !tanggal_rujukan.isEmpty()) {
			sql = "INSERT tb_rujukan (no_mrn,images,reference_date,reference,created_at,created_by) values (?,?,?,?,SYSDATETIME(),?)";
		}

		PreparedStatement ps = null;
		System.out.println(mrn + "|" + userid + "|" + no_rujukan + "|" + tanggal_rujukan + "|" + gambar);
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);

			ps.setString(1, mrn);
			ps.setString(2, gambar);
			if (!no_rujukan.isEmpty() && !tanggal_rujukan.isEmpty()) {
				ps.setString(3, tanggal_rujukan);
				ps.setString(4, no_rujukan);
				ps.setBigDecimal(5, new BigDecimal(userid));
			}

			ps.execute();
			res.setResponse("OK");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return res;
	}

	public static ResponseString uploadRujukan(List<Attachment> attachments, HttpServletRequest request) {
		System.out.println("uploadRujukan...");
		// local variables
		ResponseString res = new ResponseString();
		DataHandler dataHandler = null;
		MultivaluedMap<String, String> multivaluedMap = null;
		InputStream inputStream = null;
		String extension = ".jpg";

		for (Attachment attachment : attachments) {
			dataHandler = attachment.getDataHandler();
			try {
				String base_upload = BASE_FOLDER_UPLOAD;
				/*
				 * CREATE SUB FOLDER
				 */

				SimpleDateFormat tahun = new SimpleDateFormat("yyyy", Locale.US);
				SimpleDateFormat bulan = new SimpleDateFormat("MM", Locale.US);
				SimpleDateFormat hari = new SimpleDateFormat("dd", Locale.US);

				Calendar cal = Calendar.getInstance();
				Date date = cal.getTime();
				String subfolder = tahun.format(date) + "_" + bulan.format(date);
				String createFileName = tahun.format(date) + "_" + bulan.format(date) + "_" + hari.format(date)
						+ "_MOBILE";
				String folderLocation = base_upload + "\\" + subfolder;
				folderLocation = DataUtils.ifUnixDir(folderLocation);
				
				File checkOrCreateDir = new File(folderLocation);
				// if the directory does not exist, create it
				if (!checkOrCreateDir.exists()) {
					boolean result = false;
					try {
						checkOrCreateDir.mkdir();
						result = true;
					} catch (SecurityException se) {
						res.setResponse("FAIL");
						se.printStackTrace();
					}
				}

				String CompletefileName = base_upload + "\\" + subfolder + "\\" + createFileName;
				int i = 1;
				String temp = createFileName;
				String curPos = base_upload + "\\" + subfolder + "\\" + createFileName + extension;
				curPos = DataUtils.ifUnixDir(curPos);
				
				File f = new File(curPos);
				while (f.exists() && !f.isDirectory()) {
					i++;
					curPos = base_upload + "\\" + subfolder + "\\" + createFileName + "_" + i + extension;
					curPos = DataUtils.ifUnixDir(curPos);
					f = new File(curPos);
					temp = createFileName + "_" + i;
				}
				CompletefileName = base_upload + "\\" + subfolder + "\\" + temp + extension;
				CompletefileName = DataUtils.ifUnixDir(CompletefileName);
				
				String saveLocation = subfolder + "\\" + temp + extension;

				// write & upload file to server
				inputStream = dataHandler.getInputStream();
				BufferedImage image = ImageIO.read(inputStream);

				File output = new File(CompletefileName);
				OutputStream out = new FileOutputStream(output);

				ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
				ImageOutputStream ios = ImageIO.createImageOutputStream(out);
				writer.setOutput(ios);

				ImageWriteParam param = writer.getDefaultWriteParam();
				if (param.canWriteCompressed()) {
					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
					param.setCompressionQuality(0.7f);
				}

				writer.write(null, new IIOImage(image, null, null), param);

				out.close();
				ios.close();
				writer.dispose();

				res.setResponse(saveLocation);

			} catch (IOException ioex) {
				res.setResponse("FAIL");
				ioex.printStackTrace();
				MobileAppointment.getInstance().sendEmail(ioex.getMessage());
			} catch (Exception e) {
				res.setResponse("FAIL");
				MobileAppointment.getInstance().sendEmail(e.getMessage());
				e.printStackTrace();
			}
		}
		return res;
	}

	public static Response downloadRujukan(String rujukan_id, String jenis, HttpServletRequest request) {
		System.out.println("downloadRujukan...");

		List<AppointmentStatus> data = new ArrayList<AppointmentStatus>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		ResultSet rs = null;
		String base_upload = BASE_FOLDER_UPLOAD;

		String sql = "SELECT gambar as images FROM tb_rujukan_tambahan where reference_add_id = ? ";
		if (jenis.equals(MobileBiz.mRujukan)) {
			sql = "SELECT images FROM tb_rujukan where reference_id = ? ";
		}

		String file_loc = "";
		PreparedStatement ps = null;
		ResponseBuilder responseBuilder = Response.ok("");

		String ip_remote = request.getRemoteAddr();
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, rujukan_id);
			rs = ps.executeQuery();

			file_loc = BASE_FOLDER_UPLOAD+"\\not_found.jpg";
			if (rs.next()) {
				file_loc = base_upload + "\\" + rs.getString("images");
			}

			// set file (and path) to be download
			file_loc = DataUtils.ifUnixDir(file_loc);
			File file = FileUploadUtil.getInstance().getFileFromServer(file_loc);

			responseBuilder = Response.ok(file);
			if (!MobileBiz.REMOTE_IP_WHITELIST.contains(ip_remote)) {
				responseBuilder.header("Content-Disposition", "attachment; filename=\"image_rujukan.jpg\"");
			}			
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
			MobileAppointment.getInstance().sendEmail(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			MobileAppointment.getInstance().sendEmail(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return responseBuilder.build();
	}
	
	public BaseResult sendEmail(String errorMessage){
		BaseResult result = new BaseResult();
		result.setResult(IConstant.IND_YES);
		result.setStatus(IConstant.STATUS_CONNECTED);
		
		try {
			CommonService cs = new CommonService();
			String emailTo = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_FAILED_UPDOWN_IMAGE_TO);
			String emailCC = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_FAILED_UPDOWN_IMAGE_CC);
			String emailBCC = cs.getParameterValue(IParameterConstant.NOTIF_EMAIL_FAILED_UPDOWN_IMAGE_BCC);
			
			if (emailTo != null && !emailTo.trim().equals("")) this.receipentTo = emailTo;
			if (emailCC != null && !emailCC.trim().equals("")) this.receipentCC = emailCC;
			if (emailBCC != null && !emailBCC.trim().equals("")) this.receipentBCC = emailBCC;
			
			Message message = PhaseInterceptorChain.getCurrentMessage();
			HttpServletRequest request = (HttpServletRequest)message.get(AbstractHTTPDestination.HTTP_REQUEST);
			bodyEmail = "This email is generated by IP : " + request.getRemoteAddr();
			bodyEmail += "<br />Upload/download image failed caused by this error </br>";
			bodyEmail += "<br />" + "Date : " + DataUtils.getCurrentTimeLog()+"</br>";
			bodyEmail += errorMessage;
		} catch (Exception e){
			System.out.println(e.getMessage());
		}				
		
		this.emailSubject = "Automatic send email caused error upload / download image";
		this.sendMessage(bodyEmail);
		result.setResult(bodyEmail);
		return result;
	}
	
	
	/*--- Appointment Webstie Antrian Online  : Andy Three Saputra --*/
	
	public static ResponseStatus updateDataUser(String fullname, String idcard,String bpjs,String contact, String email, String userid) {
		ResponseStatus data = new ResponseStatus();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)	return null;
		
		String sql = "UPDATE tb_user SET no_ktp = ?, fullname=?, no_bpjs=?,contact=?, email=? WHERE user_id = ?";

		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, idcard);
			ps.setString(2, fullname);
			ps.setString(3, bpjs);
			ps.setString(4, contact);
			ps.setString(5, email);			
			ps.setString(6, userid);	
			ps.executeUpdate();
			data.setMessage("Data berhasil di update.");
			data.setSuccess(true);
			ps.close();
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static ResponseStatus checkAvalibaleRujukan(String mrn) {
		System.out.println("Check Avalibale Rujukan Website...");
		ResponseStatus data = new ResponseStatus();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)return null;
		ResultSet rs = null;
		PreparedStatement ps = null;		
		String sql = "SELECT TOP (1) R.reference_id, R.reference_date FROM tb_rujukan R WHERE R.no_mrn = ? AND R.reference IS NOT NULL AND R.reference != '' ORDER BY R.reference_id DESC ";
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, mrn);
			rs = ps.executeQuery();
			if (rs.next()) {
				String tglrujukan = rs.getString("reference_date");
				Date tglNow = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date tglRujuk = null;
				try {
					tglRujuk = df.parse(tglrujukan);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Calendar calTglExpired = Calendar.getInstance();				
				calTglExpired.setTime(tglRujuk);
				calTglExpired.add(Calendar.MONTH, 3);
				Date tglExpired = calTglExpired.getTime();
				if(tglNow.before(tglExpired)){
					data.setMessage(rs.getString("reference_date"));
					data.setResponse(rs.getString("reference_id"));
					data.setSuccess(true);
				} else {
					data.setMessage("Rujukan Sudah Expired.");
					data.setSuccess(false);
				}
			}
			rs.close();
			ps.close();
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static ResponseStatus checkAvalibaleAppointment(String nomrn, String tglappointment, String resourcemstrid) {
		System.out.println("Check Avalibale Appointment Website...");
		//String sql = "SELECT COUNT(R.registrasi_id) as JLH FROM tb_registrasi R WHERE R.no_mrn = ? AND R.queue_date = ? AND R.resourcemstr_id = ? AND R.status != ? AND R.status != ? AND R.status != ? ORDER BY R.registrasi_id DESC";
		//ResponseString result = new ResponseString();
		//result.setResponse(DbConnection.executeScalar(DbConnection.BPJSONLINE, sql, new Object[] {nomrn, tglappointment, resourcemstrid, MobileBiz.APPOINTMENT_STATUS_DELETE, MobileBiz.APPOINTMENT_STATUS_CANCEL, MobileBiz.APPOINTMENT_STATUS_REJECT }).toString());
		
		ResponseStatus data = new ResponseStatus();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		String sql = "SELECT * FROM tb_registrasi R WHERE R.no_mrn = ? AND R.queue_date = ? AND R.resourcemstr_id = ? AND R.status != ? AND R.status != ? AND R.status != ? ORDER BY R.registrasi_id DESC";
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, nomrn);
			ps.setString(2, tglappointment);
			ps.setString(3, resourcemstrid);
			ps.setString(4, MobileBiz.APPOINTMENT_STATUS_DELETE);
			ps.setString(5, MobileBiz.APPOINTMENT_STATUS_CANCEL);
			ps.setString(6, MobileBiz.APPOINTMENT_STATUS_REJECT);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				data.setMessage("Sudah Appointment di Tanggal tersebut.");
				data.setSuccess(false);
			} else {
				data.setMessage("Bulam ada Appointment di Tanggal tersebut");
				data.setSuccess(true);
			}
			rs.close();
			ps.close();
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static List<DataRujukan> getRujukan(String mrn) {
		System.out.println("Check Avalibale Rujukan Website...");
		List<DataRujukan> data = new ArrayList<DataRujukan>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		String sql = "select reference_id, no_mrn, images, reference_date, reference, created_at,"
				+ "DATEADD(month, 3, reference_date) as expired_date,"
				+ "CAST(" + "case "
				+ "when dateadd(month, 3, reference_date) < sysdatetime() then 'Expired' " + "else 'Valid'"
				+ "end as varchar(12)) as status" + " from tb_rujukan "
				+ "where "
				+ "no_mrn = ? " + "AND " 
				+ "images IS NOT NULL AND "
				+ "images !='' AND " 
				+ "reference IS NOT NULL AND " 
				+ "reference != '' " 
				+ " order by reference_id desc ";	
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, mrn);
			rs = ps.executeQuery();
			while (rs.next()) {
				DataRujukan dl = new DataRujukan();
				dl.setReference_id(rs.getString("reference_id"));
				dl.setMrn(rs.getString("no_mrn"));
				dl.setImages(rs.getString("images"));
				dl.setReference_date(rs.getString("reference_date"));
				dl.setExpired_date(rs.getString("expired_date"));
				dl.setReference(rs.getString("reference"));
				dl.setCreatedate(rs.getString("created_at"));
				dl.setType(MobileBiz.mRujukan);
				dl.setStatus(rs.getString("status"));
				data.add(dl);
			}
			rs.close();
			ps.close();
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static List<DataRujukan> getRujukanTambahan(String mrn) {
		System.out.println("Check Avalibale Rujukan Website...");
		List<DataRujukan> data = new ArrayList<DataRujukan>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)return null;
		ResultSet rs = null;
		PreparedStatement ps = null;		
		String sql = "SELECT R.reference_add_id, R.gambar,R.no_mrn,R.created_date FROM tb_rujukan_tambahan R WHERE R.no_mrn = ? AND R.gambar IS NOT NULL AND R.gambar != '' ORDER BY R.reference_add_id DESC";
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, mrn);
			rs = ps.executeQuery();
			while (rs.next()) {
				DataRujukan dl = new DataRujukan();
				dl.setReference_id(rs.getString("reference_add_id"));
				dl.setMrn(rs.getString("no_mrn"));
				dl.setImages(rs.getString("gambar"));
				dl.setCreatedate(rs.getString("created_date"));
				dl.setType(MobileBiz.mRujukanTambahan);
				data.add(dl);
			}
			rs.close();
			ps.close();
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static ResponseStatus createAppointment(DataAppointment dataform) {
		ResponseStatus data = new ResponseStatus();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)	return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		String sql = "INSERT INTO tb_registrasi (queue_no,resourcemstr_id,careprovider_id,doctor_name,queue_date,no_mrn,user_id,created_at,deleted,sort,status,reference_id,reference_date,department,source) "
				+ "OUTPUT INSERTED.registrasi_id VALUES (?,?,?,?,?,?,?,SYSDATETIME(),0,0,?,?,?,?,?)";
		

		try{
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, dataform.getQueueNo());
			ps.setString(2, dataform.getResourceMstrId());
			ps.setString(3, dataform.getCareProviderId());
			ps.setString(4, dataform.getNameDoctor());
			ps.setString(5, dataform.getQueeDate());
			ps.setString(6, dataform.getMrn());
			ps.setString(7, dataform.getUserId());
			ps.setString(8, dataform.getStatus());
			ps.setString(9, dataform.getReferenceId());
			ps.setString(10, dataform.getReferenceDate());
			ps.setString(11, dataform.getDepartment());
			ps.setString(12, MobileBiz.WEB_SOURCE);
			rs = ps.executeQuery();
			if (rs.next()) {
				if(dataform.getDepartment().equals(MobileBiz.DEPARTMENT_POLI)){
					String regid = rs.getString("registrasi_id");
					String SqlGenerate = "exec generateBarcode ?";
					try{
						ps1 = connection.prepareStatement(SqlGenerate);
						ps1.setEscapeProcessing(true);
						ps1.setQueryTimeout(60000);
						ps1.setString(1, regid);
						ps1.executeUpdate();
					}
					catch (SQLException e) {
						System.out.println(e.getMessage());
						connection.rollback();
					}
					ps1.close();
				}
				data.setResponse(rs.getString("registrasi_id"));
				data.setSuccess(true);
				connection.commit();
			}
			ps.close();
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (ps1!=null) try  { ps1.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static ResponseStatus createRujukan(DataRujukan dataform) {
		ResponseStatus data = new ResponseStatus();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)	return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO tb_rujukan (no_mrn,images,reference_date,reference,created_by,created_at,source,reference_by) "
				+ "OUTPUT INSERTED.reference_id VALUES (?,?,?,?,?,SYSDATETIME(),?,?)";
		try{
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, dataform.getMrn());
			ps.setString(2, dataform.getImages());
			ps.setString(3, dataform.getReference_date());
			ps.setString(4, dataform.getReference());
			ps.setString(5, dataform.getCreateby());
			ps.setString(6, MobileBiz.WEB_SOURCE);
			ps.setString(7, dataform.getReference_by());
			rs = ps.executeQuery();
			if (rs.next()) {
				data.setResponse(rs.getString("reference_id"));
				data.setSuccess(true);
			}
			ps.close();
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static ResponseStatus createRujukanTambahan(DataRujukan dataform) {
		ResponseStatus data = new ResponseStatus();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)	return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO tb_rujukan_tambahan (no_mrn,gambar,created_date,source) "
				+ "OUTPUT INSERTED.reference_add_id VALUES (?,?,SYSDATETIME(),?)";
		try{
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, dataform.getMrn());
			ps.setString(2, dataform.getImages());
			ps.setString(3, MobileBiz.WEB_SOURCE);
			rs = ps.executeQuery();
			if (rs.next()) {
				data.setResponse(rs.getString("reference_add_id"));
				data.setSuccess(true);
			}
			ps.close();
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static ResponseStatus getInfoReject(String ids) {
		System.out.println("get Info Rejected...");
		ResponseStatus data = new ResponseStatus();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		String sql = "select keterangan from tb_alasan_tolak where registrasi_id = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, ids);
			rs = ps.executeQuery();
			if(rs.next()){
				data.setResponse(rs.getString("keterangan"));
				data.setSuccess(true);
			}
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null)	try {	ps.close();	} catch (Exception ignore) {}
			if (connection != null)	try {	connection.close();	} catch (Exception ignore) {}
		}
		return data;
	}
	
	public static ResponseStatus validNomorRujukan(String ids, String mrn) {
		System.out.println("valid Nomor Rujukan...");
		ResponseStatus data = new ResponseStatus();
		Connection connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)
			return null;
		String sql = "select reference from tb_rujukan where reference = ? and no_mrn != ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, ids);
			ps.setString(2, mrn);
			rs = ps.executeQuery();
			if(rs.next()){
				data.setResponse(rs.getString("reference"));
				data.setMessage("Nomor Rujukan sudah terdaftar");
				data.setSuccess(false);
			} else {
				data.setResponse(ids);
				data.setMessage("Nomor Rujukan belum terdaftar");
				data.setSuccess(true);
			}
		}

		catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null)	try {	ps.close();	} catch (Exception ignore) {}
			if (connection != null)	try {	connection.close();	} catch (Exception ignore) {}
		}
		return data;
	}
	
	public static DataAppointment getAppointmentbyId(String ids) {
		System.out.println("get Appointment by Id...");
		DataAppointment data = new DataAppointment();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		String sql = "select registrasi_id,queue_no,queue_date,resourcemstr_id,careprovider_id,doctor_name,no_mrn,user_id,created_at,updated_at,updated_by,status,generate_barcode,status_print,reference_id,reference_date,department,source "
				+ "FROM tb_registrasi "
				+ "where registrasi_id = ? "
				+ "order by registrasi_id desc ";	
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, ids);
			rs = ps.executeQuery();
			if(rs.next()) {
				data.setSuccess(true);
				data.setRegistrasi_id(rs.getString("registrasi_id"));
				data.setCareProviderId(rs.getString("careprovider_id"));
				data.setNameDoctor(rs.getString("doctor_name"));
				data.setResourceMstrId(rs.getString("resourcemstr_id"));
				data.setQueeDate(rs.getString("queue_date"));
				data.setQueueNo(rs.getString("queue_no"));
				data.setMrn(rs.getString("no_mrn"));
				data.setReferenceDate(rs.getString("reference_date"));
				data.setReferenceId(rs.getString("reference_id"));
				data.setSource(rs.getString("source"));
				data.setStatus(rs.getString("status"));
				data.setUserId(rs.getString("user_id"));
				data.setNameDoctor(rs.getString("doctor_name"));
				data.setDepartment(rs.getString("department"));
				data.setBarcode(rs.getString("generate_barcode"));
				data.setCreatedDate(rs.getString("created_at"));
				data.setUpdateBy(rs.getString("updated_by"));
				data.setUpdateDate(rs.getString("updated_at"));
				data.setPrintstatus(rs.getString("status_print"));
			}
			rs.close();
			ps.close();
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	
	public static List<DataAppointment> getAppointment(String mrn,String departement,String source,String tipe){
		System.out.println("get Appointment ..");
		List<DataAppointment> data = new ArrayList<DataAppointment>();
		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if (connection == null)return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		String sql = "select registrasi_id,queue_no,queue_date,resourcemstr_id,careprovider_id,doctor_name,no_mrn,user_id,created_at,updated_at,updated_by,status,generate_barcode,status_print,reference_id,reference_date,department,source "
				+ "FROM tb_registrasi "
				+ "where department = ? "
				+ "and source = ? "
				+ "and no_mrn = ? "
				+ "and status != ? "				
				+ "order by registrasi_id desc ";
		if(tipe.equals(MobileBiz.WEB_BIG)){
			sql = "select registrasi_id,queue_no,queue_date,resourcemstr_id,careprovider_id,doctor_name,no_mrn,user_id,created_at,updated_at,updated_by,status,generate_barcode,status_print,reference_id,reference_date,department,source "
					+ "FROM tb_registrasi "
					+ "where queue_date >= sysdatetime()"
					+ "and department = ? "
					+ "and source = ? "
					+ "and no_mrn = ? "					
					+ "and status != ? "
					+ "order by registrasi_id desc ";
		} else if(tipe.equals(MobileBiz.WEB_SMALL)){
			sql = "select registrasi_id,queue_no,queue_date,resourcemstr_id,careprovider_id,doctor_name,no_mrn,user_id,created_at,updated_at,updated_by,status,generate_barcode,status_print,reference_id,reference_date,department,source "
					+ "FROM tb_registrasi "
					+ "where "
					+ "department = ? "
					+ "and source = ? "
					+ "and no_mrn = ? "
					+ "and (queue_date < sysdatetime() or status = ?) "
					+ "order by registrasi_id desc ";
		}
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, departement);
			ps.setString(2, source);
			ps.setString(3, mrn);			
			if(tipe.equals(MobileBiz.WEB_ALL)){
				ps.setString(4, MobileBiz.WEB_ALL);				
			} else {
				ps.setString(4, MobileBiz.APPOINTMENT_STATUS_CANCEL);
			}
			rs = ps.executeQuery();
			while(rs.next()) {
				DataAppointment dl = new DataAppointment();
				dl.setSuccess(true);
				dl.setRegistrasi_id(rs.getString("registrasi_id"));
				dl.setCareProviderId(rs.getString("careprovider_id"));
				dl.setNameDoctor(rs.getString("doctor_name"));
				dl.setResourceMstrId(rs.getString("resourcemstr_id"));
				dl.setQueeDate(rs.getString("queue_date"));
				dl.setQueueNo(rs.getString("queue_no"));
				dl.setMrn(rs.getString("no_mrn"));
				dl.setReferenceDate(rs.getString("reference_date"));
				dl.setReferenceId(rs.getString("reference_id"));
				dl.setSource(rs.getString("source"));
				dl.setStatus(rs.getString("status"));
				dl.setUserId(rs.getString("user_id"));
				dl.setNameDoctor(rs.getString("doctor_name"));
				dl.setDepartment(rs.getString("department"));
				dl.setBarcode(rs.getString("generate_barcode"));
				dl.setCreatedDate(rs.getString("created_at"));
				dl.setUpdateBy(rs.getString("updated_by"));
				dl.setUpdateDate(rs.getString("updated_at"));
				dl.setPrintstatus(rs.getString("status_print"));
				data.add(dl);
			}
			rs.close();
			ps.close();
		}		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
}