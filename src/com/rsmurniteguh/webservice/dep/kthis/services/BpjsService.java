package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.tomcat.util.codec.binary.Base64;

public class BpjsService {
	private static final String SECRET_KEY2 = "123456";
	private static final String USER2 = "123456";
	
	private static final String SECRET_KEY = "3cTA543D2B";
	private static final String USER = "12282";
	
//	private static final String SECRET_KEY = "0bYC766F75";
//	private static final String USER = "26805";
	
	private static final String BASE_URL2 = "http://dvlp.bpjs-kesehatan.go.id:8081/devWSLokalRest/";
	private static final String BASE_URL3 = "http://api.bpjs-kesehatan.go.id/aplicaresws/rest/";
	
	//private static final String BASE_URL = "http://localhost:81/WsLokalRest/";
	//private static final String BASE_URL = "http://192.168.1.19:81/";
	private static final String BASE_URL = "http://192.168.2.21:81/WSLokalRest/";
	private static final String URL_GET_MEMBER_INFO = "Peserta/Peserta/";
	private static final String URL_GET_NIK_INFO = "Peserta/Peserta/nik/";
	private static final String URL_GET_SEP_INFO = "sep/peserta/";
	private static final String URL_GET_DATA_RUJUKAN = "Rujukan/";
	private static final String URL_GET_QUERY_TANGGAL = "Rujukan/rujukan/tglrujuk/@@Tanggal@@/query?start={start}&limit={limit}";
	private static final String URL_GET_RUJUKAN_KARTU = "Rujukan/Peserta/";
	private static final String URL_GET_NOMOR_KARTU_FKTL = "Rujukan/rujukanrs/peserta/nokartu/";
	private static final String URL_GET_TANGGAL_RUJUKAN_FKTL = "Rujukan/rujukanrs/tglrujuk/@@Tanggal@@/query?start={start}&limit={limit}";
	private static final String URL_GET_NOMOR_RUJUKAN_FKTL = "Rujukan/rujukanrs/peserta/";
	private static final String URL_GET_DETAIL_SEP = "SEP/";
	private static final String URL_GET_CBG_DIAGNOSA = "diagnosa/cbg/diagnosa/";
	private static final String URL_GET_CBG_PROCEDURE = "prosedur/cbg/";
	private static final String URL_GET_DATA_CMG = "prosedur/cmg/";
	private static final String URL_GET_DETAIL_VERIFIKASI = "sep/integrated/Kunjungan/tglMasuk/@@TglMasuk@@/tglKeluar/@@TglKeluar@@/KlsRawat/@@KlsRawat@@/Kasus/@@Kasus@@/Cari/@@Cari@@/status/@@Status@@";
	private static final String URL_GET_LAPORAN_SEP = "sep/integrated/Kunjungan/sep/";
	private static final String URL_CREATE_SEP = "SEP/insert";
	private static final String URL_UPDATE_TGL_PULANG_SEP = "Sep/updtglplg";
	private static final String URL_HAPUS_SEP = "SEP/Delete";
	private static final String URL_MAPPING_SEP = "SEP/map/trans";
	private static final String URL_GROUPER_INACBG = "gruper/grouper";
	private static final String URL_FINALISASI_INACBG = "gruper/grouper/save";
	private static final String URL_UPDATE_SEP = "Sep/Update";
	private static final String URL_RUJUKAN_RS = "Rujukan/RS";
	private static final String URL_RUJUKAN_RS_KARTU = "Rujukan/RS/Peserta/";
	private static final String URL_CBG = "sep/cbg/";
	private static final String URL_KODE_KAMAR = "ref/kelas";
	private static final String URL_CREATE_ROOM = "bed/create/0038R091";
	private static final String URL_LIST_KAMAR = "bed/read/0038R091/1/20";
	private static final String URL_UPDATE_KAMAR = "bed/update/0038R091";
	
	
	
	
	
	private static String generateHmacSHA256Signature(String data, String key) throws GeneralSecurityException {
		Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
		SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
		sha256_HMAC.init(secret_key);
		String hash = Base64.encodeBase64String(sha256_HMAC.doFinal(data.getBytes()));
		return hash;
	}
	private static HttpURLConnection SetHeader(HttpURLConnection con) throws Exception
	{
		
		try {
			long currentTime = System.currentTimeMillis() / 1000L;
			String salt = USER +"&"+ String.valueOf(currentTime);
			con.setRequestProperty("X-cons-id", USER);
			con.addRequestProperty("X-timestamp", String.valueOf(currentTime));
			con.addRequestProperty("X-signature",generateHmacSHA256Signature(salt, SECRET_KEY));
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	private static HttpURLConnection SetHeader2(HttpURLConnection con2) throws Exception
	{
		
		try {
			long currentTime2 = System.currentTimeMillis() / 1000L;
			String salt2 = USER2 +"&"+ String.valueOf(currentTime2);
			con2.setRequestProperty("X-cons-id", USER2);
			con2.addRequestProperty("X-timestamp", String.valueOf(currentTime2));
			con2.addRequestProperty("X-signature",generateHmacSHA256Signature(salt2, SECRET_KEY2));
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		return con2;
	}
	
	
	public static String GetBpjsMemberByIdService(String cardNo) {
		String url = BASE_URL+URL_GET_MEMBER_INFO+cardNo;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsMemberOnlineIdService(String cardNo) {
		String url = BASE_URL2+URL_GET_MEMBER_INFO+cardNo;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con2 = (HttpURLConnection) obj.openConnection();

			con2.setRequestMethod("GET");
			SetHeader2(con2);

			BufferedReader in = new BufferedReader(new InputStreamReader(con2.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	public static String GetBpjsMemberByNik(String Nik) {
		String url = BASE_URL+URL_GET_NIK_INFO+Nik;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsMemberSep(String cardNo) {
		String url = BASE_URL+URL_GET_SEP_INFO+cardNo;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsMemberDataRujukan(String noRujuk) {
		String url = BASE_URL+URL_GET_DATA_RUJUKAN+noRujuk;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsMemberTanggalRujukan(String Tanggal) {
		String url = BASE_URL+URL_GET_QUERY_TANGGAL;
		url.replaceAll("@@Tanggal@@", Tanggal);

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsMemberRujukanNomorKartu(String cardNo) {
		String url = BASE_URL+URL_GET_RUJUKAN_KARTU+cardNo;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsMemberRujukanFktlNomorKartu(String cardNo) {
		String url = BASE_URL+URL_GET_NOMOR_KARTU_FKTL+cardNo;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsMemberRujukanFktlTanggal(String Tanggal) {
		String url = BASE_URL+URL_GET_TANGGAL_RUJUKAN_FKTL;
		url = url.replaceAll("@@Tanggal@@", Tanggal);
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsMemberRujukanFktlNoRujuk(String noRujuk) {
		String url = BASE_URL+URL_GET_NOMOR_RUJUKAN_FKTL+noRujuk;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String KodeKamar(){
		String url = BASE_URL3+URL_KODE_KAMAR;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String ListKamar(){
		String url = BASE_URL3+URL_LIST_KAMAR;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	
	public static String GetBpjsMemberDetailNoSep(String noSep) {
		String url = BASE_URL+URL_GET_DETAIL_SEP+noSep;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String GetRujukanRs(String noRujuk) {
		String url = BASE_URL+URL_RUJUKAN_RS+noRujuk;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String GetRujukanKartu(String noKartu) {
		String url = BASE_URL+URL_RUJUKAN_RS_KARTU+noKartu;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetTarifCbg(String noSep) {
		String url = BASE_URL+URL_CBG+noSep;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsCbgDiagnosa(String Keyword) {
		String url = BASE_URL+URL_GET_CBG_DIAGNOSA+Keyword;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsCbgProsedur(String Keyword) {
		String url = BASE_URL+URL_GET_CBG_PROCEDURE+Keyword;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsDetailCmg(String Keyword) {
		String url = BASE_URL+URL_GET_DATA_CMG+Keyword;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsDetailVerifikasi(String TglMasuk, String TglKeluar, String KlsRawat, String Kasus, String Cari , String Status) {
		String url = BASE_URL+URL_GET_DETAIL_VERIFIKASI;
		url = url.replaceAll("@@TglMasuk@@", "2016-01-01");
		url = url.replaceAll("@@TglKeluar@@", "2016-01-31");
		url = url.replaceAll("@@KlsRawat@@", "1");
		url = url.replaceAll("@@Kasus@@", "1");
		url = url.replaceAll("@@Cari@@", "0");
		url = url.replaceAll("@@Status@@", "10");
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsLaporanSepNo(String noSep) {
		String url = BASE_URL+URL_GET_LAPORAN_SEP+noSep;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String CreateRoom(String kodeKelas,
			String kodeRuang,
			String namaRuang,
			String kapasitas,
			String tersedia){
		String url = BASE_URL3+URL_CREATE_ROOM;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input =  "{\"kodekelas\":\""+kodeKelas+"\","
					+ "\"koderuang\":\""+kodeRuang+"\", "
					+ "\"namaruang\":\""+namaRuang+"\", "
					+ "\"kapasitas\":\""+kapasitas+"\", "
					+ "\"tersedia\":\""+tersedia+"\",  "
					+ "\"tersediapria\":\"0\",  "
					+ "\"tersediawanita\":\"0\",  "
					+ "\"tersediapriawanita\":\"0\"}";                                          
		  
		                    
			System.out.println(input);
	        
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String UpdateRoom(String kodeKelas,
			String kodeRuang,
			String namaRuang,
			String kapasitas,
			String tersedia){
		String url = BASE_URL3+URL_UPDATE_KAMAR;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.setRequestProperty("Content-Type", "application/json");
			String input =  "{\"kodekelas\":\""+kodeKelas+"\","
					+ "\"koderuang\":\""+kodeRuang+"\", "
					+ "\"namaruang\":\""+namaRuang+"\", "
					+ "\"kapasitas\":\""+kapasitas+"\", "
					+ "\"tersedia\":\""+tersedia+"\",  "
					+ "\"tersediapria\":\"0\",  "
					+ "\"tersediawanita\":\"0\",  "
					+ "\"tersediapriawanita\":\"0\"}";                                          
		  
		                    
			System.out.println(input);
	        
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	
	
	
	
	
	public static String CreateBpjsLaporanSep(
			String noKartu,
			String tglSep,
			String tglRujukan,
			String noRujukan,
			String ppkRujukan,
			String ppkPelayanan,
			String jnsPelayanan,
			String catatan,
			String diagAwal,
			String poliTujuan,
			String klsRawat,
			String lakaLantas,
			String noMr) {
		String url = BASE_URL+URL_CREATE_SEP;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
//			String input = "<request>"
//					+ "<data>"
//					+ "<t_sep>"
//					+ "<noKartu>"+noKartu+"</noKartu>"
//					+ "<tglSep>"+tglSep+"</tglSep>"
//					+ "<tglRujukan>"+tglRujukan+"</tglRujukan>"
//					+ "<noRujukan>"+noRujukan+"</noRujukan>"
//					+ "<ppkRujukan>"+ppkRujukan+"</ppkRujukan>"
//					+ "<ppkPelayanan>"+ppkPelayanan+"</ppkPelayanan>"
//					+ "<jnsPelayanan>"+jnsPelayanan+"</jnsPelayanan>"
//					+ "<catatan>"+catatan+"</catatan>"
//					+ "<diagAwal>"+diagAwal+"</diagAwal>"
//					+ "<poliTujuan>"+poliTujuan+"</poliTujuan>"
//					+ "<klsRawat>"+klsRawat+"</klsRawat>"
//					+ "<lakaLantas>"+lakaLantas+"</lakaLantas>"
//					+ "<user>simrs</user>"
//					+ "<noMr>"+noMr+"</noMr>"
//					+ "</t_sep>"
//					+ "</data>"
//					+ "</request>";
			String input =  "{"
		               + "\"request\":"
		               + "{ "
		               + "\"t_sep\":"
		               +"{"
		               +"\"noKartu\": \""+noKartu+"\","
		               +"\"tglSep\":\""+tglSep+"\","
		               +"\"tglRujukan\":\""+tglRujukan+"\","
		               +"\"noRujukan\":\""+noRujukan+"\","
		               +"\"ppkRujukan\":\""+ppkRujukan+"\","
		               +"\"ppkPelayanan\":\""+ppkPelayanan+"\","
		               +"\"jnsPelayanan\":\""+jnsPelayanan+"\","
		               +"\"catatan\":\""+catatan+"\","
		               +"\"diagAwal\":\""+diagAwal+"\","
		               +"\"poliTujuan\":\""+poliTujuan+"\","
		               +"\"klsRawat\":\""+klsRawat+"\","
		               +"\"lakaLantas\":\""+lakaLantas+"\","
		               +"\"lokasiLaka\":\"Medan\","
		               +"\"user\":\"simrs\","
		               +"\"noMr\":\""+noMr+"\""
		               +"		}"
		               +"	}"
		               +"}";                                            
		  
		                    
			System.out.println(input);
	        
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String UpdateBpjsTglpulangSep(
			String noSep,
			String tglPlg) {
		String url = BASE_URL+URL_UPDATE_TGL_PULANG_SEP;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("PUT");
			con.setDoOutput(true);
//			String input = "<request>"
//					+ "<data>"
//					+ "<t_sep>"
//					+ "<noSep>"+noSep+"</noSep>"
//					+ "<tglPlg>"+tglPlg+"</tglPlg>"
//					+ "<ppkPelayanan>0038R091</ppkPelayanan>"
//					+ "</t_sep>"
//					+ "</data>"
//					+ "</request>";
			
			String input=" {  "
					+ "\"request\": "
					+ "{"
					+ "\"t_sep\":"
					+ "{"
					+ "\"noSep\":\""+noSep+"\","
					+ "\"tglPlg\":\""+tglPlg+"\","
					+ "\"ppkPelayanan\":\"0038R091\""
					+ "}"
					+ "}"
					+ "}";
			
	        
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String UpdateBpjsSep(
			String noSep,
			String noKartu,
			String tglSep,
			String tglRujukan,
			String noRujukan,
			String ppkRujukan,
			String ppkPelayanan,
			String jnsPelayanan,
			String catatan,
			String diagAwal,
			String poliTujuan,
			String klsRawat,
			String noMr){
		String url = BASE_URL+URL_UPDATE_SEP;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("PUT");
			con.setDoOutput(true);
//			String input = "<request>"
//					+ "<data>"
//					+ "<t_sep>"
//					+ "<noSep>"+noSep+"</noSep>"
//					+ "<tglPlg>"+tglPlg+"</tglPlg>"
//					+ "<ppkPelayanan>0038R091</ppkPelayanan>"
//					+ "</t_sep>"
//					+ "</data>"
//					+ "</request>";
			
			String input =  "{"
		               + "\"request\":"
		               + "{ "
		               + "\"t_sep\":"
		               +"{"
		               +"\"noKartu\": \""+noKartu+"\","
		               +"\"tglSep\":\""+tglSep+"\","
		               +"\"tglRujukan\":\""+tglRujukan+"\","
		               +"\"noRujukan\":\""+noRujukan+"\","
		               +"\"ppkRujukan\":\""+ppkRujukan+"\","
		               +"\"ppkPelayanan\":\""+ppkPelayanan+"\","
		               +"\"jnsPelayanan\":\""+jnsPelayanan+"\","
		               +"\"catatan\":\""+catatan+"\","
		               +"\"diagAwal\":\""+diagAwal+"\","
		               +"\"poliTujuan\":\""+poliTujuan+"\","
		               +"\"klsRawat\":\""+klsRawat+"\","
		               +"\"lakaLantas\":\"2\","
		               +"\"lokasiLaka\":\"Medan\","
		               +"\"user\":\"simrs\","
		               +"\"noMr\":\""+noMr+"\""
		               +"		}"
		               +"	}"
		               +"}";    
			
	        
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;				
		
		
	}
	
	
	public static String DeleteBpjsSep(String noSep) {
		String url = BASE_URL+URL_HAPUS_SEP;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("DELETE");
			con.setDoOutput(true);
//			String input = "<request>"
//					+ "<data>"
//					+ "<t_sep>"
//					+ "<noSep>"+noSep+"</noSep>"
//					+ "<ppkPelayanan>0038R091</ppkPelayanan>"
//					+ "</t_sep>"
//					+ "</data>"
//					+ "</request>";
			
			
			String input = " {"
					+ "\"request\":"
					+ "{"
					+ "\"t_sep\":"
					+ "{"
					+ "\"noSep\":\""+noSep+"\","
					+ "\"ppkPelayanan\":\"0038R091\""
					+ "}"
					+ "}"
					+ "}";
			
	        
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String MappingBpjsSep(String noSep, String noTrans) {
		String url = BASE_URL+URL_MAPPING_SEP;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
//			String input = "<request>"
//					+ "<data>"
//					+ "<t_map_sep>"
//					+ "<noSep>"+noSep+"</noSep>"
//					+ "<noTrans>"+noTrans+"</noTrans>"
//					+ "<ppkPelayanan>0038R091</ppkPelayanan>"
//					+ "</t_map_sep>"
//					+ "</data>"
//					+ "</request>";
			
			String input = "{"
					+ "\"request\":"
					+ "{"
					+ "\"t_map_sep\":"
					+ "{"
					+ "\"noSep\":\""+noSep+"\","
					+ "\"noTrans\":\""+noTrans+"\","
					+ "\"ppkPelayanan\":\"0038R091\""
					+ "}"
					+ "}"
					+ "}"; 
			
			
	        
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GrouperBpjsInacbg() {
		String url = BASE_URL+URL_GROUPER_INACBG;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			String input = "{ "
					+ "\"request\":"
					+ "{"
					+ "\"noMR\": \"1528\","
					+ "\"jnsBayar\": \"5\","
					+ "\"noSep\": \"0301R00105150000003\","
					+ "\"jnsPerawatan\": \"2\","
					+ "\"klsPerawatan\": \"3\","
					+ "\"tglMasuk\": \"2016‐01‐26\","
					+ "\"tglPulang\": \"2016‐01‐26\","
					+ "\"caraPulang\": \"1\","
					+ "\"namaDokter\": \"erna\","
					+ "\"beratLahir\": \"0\","
					+ "\"tarifRS\": \"2000000\","
					+ "\"suratRujukan\": \"1\","
					+ "\"kasusPerawatan\": \"0\","
					+ "\"adl\": \"0\","
					+ "\"peserta\":"
					+ "{"
					+ "\"noKartu\": \"1528\","
					+ "\"namaPeserta\": \"jadit\","
					+ "\"sex\": \"L\","
					+ "\"tglLahir\": \"1980‐01‐01\""
					+ "},"
					+ "\"cmg\":"
					+ "{"
					+ "\"Procedure\":\"\","
					+ "\"Drugs\":\"DD04\","
					+ "\"Investigation\":\"\","
					+ "\"Prosthesis\":\"\""
					+ "},"
					+ "\"diagnosa\": ["
					+ "{"
					+ "\"kodeDiagnosa\": \"I10\","
					+ "\"namaDiagnosa\": \"Abdominal pain, unspecified site\","
					+ "\"Level\": \"1\""
					+ "},"
					+ "{"
					+ "\"kodeDiagnosa\": \"I15.9\","
					+ "\"namaDiagnosa\": \"Abdominal pain, unspecified site\","
					+ "\"Level\": \"2\"},"
					+ "],"
					+ "\"prosedur\": ["
					+ "{"
					+ "\"kodeProsedur\": \"E900.0\","
					+ "\"namaProsedur\": \"Abdominal pain, unspecified site\""
					+ "},"
					+ "{"
					+ "\"kodeProsedur\": \"E907\","
					+ "\"namaProsedur\": \"Abnormalities of size and form of teeth\""
					+ "},]}}";
			
			
	        
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String FinalisasiGrouperBpjsInacbg() {
		String url = BASE_URL+URL_FINALISASI_INACBG;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			String input = "{\"request\":"
					+ "{"
					+ "\"noMR\": \"1528\","
					+ "\"jnsBayar\": \"5\","
					+ "\"noSep\": \"0301R00105150000003\","
					+ "\"jnsPerawatan\": \"2\","
					+ "\"klsPerawatan\": \"3\","
					+ "\"tglMasuk\": \"2015‐05‐04\","
					+ "\"tglPulang\": \"2015‐05‐04\","
					+ "\"caraPulang\": \"1\","
					+ "\"namaDokter\": \"erna\","
					+ "\"beratLahir\": \"0\","
					+ "\"tarifRS\": \"2000000\","
					+ "\"suratRujukan\": \"1\","
					+ "\"kasusPerawatan\": \"0\","
					+ "\"adl\": \"0\","
					+ "\"peserta\":"
					+ "{"
					+ "\"noKartu\": \"1528\","
					+ "\"namaPeserta\": \"jadit\","
					+ "\"sex\": \"L\","
					+ "\"tglLahir\": \"1980‐01‐01\""
					+ "},"
					+ "\"cmg\":"
					+ "{"
					+ "\"Procedure\":\"\","
					+ "\"Drugs\":\"DD04\","
					+ "\"Investigation\":\"\","
					+ "\"Prosthesis\":\"\""
					+ "},"
					+ "\"diagnosa\": ["
					+ "{"
					+ "\"kodeDiagnosa\": \"I10\","
					+ "\"namaDiagnosa\": \"Abdominal pain, unspecified site\","
					+ "\"Level\": \"1\""
					+ "},"
					+ "{"
					+ "\"kodeDiagnosa\": \"I15.9\","
					+ "\"namaDiagnosa\": \"Abdominal pain, unspecified site\","
					+ "\"Level\": \"2\"},"
					+ "],"
					+ "\"prosedur\": ["
					+ "{"
					+ "\"kodeProsedur\": \"E900.0\","
					+ "\"namaProsedur\": \"Abdominal pain, unspecified site\""
					+ "},"
					+ "{"
					+ "\"kodeProsedur\": \"E907\","
					+ "\"namaProsedur\": \"Abnormalities of size and form of teeth\""
					+ "},"
					+ "]}"
					+ "}";
			
			
	        
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	
	private static final String KODEPROVIDER = "0204R009";
	//private static final String TOKEN = "inh_028182829d4806aaa1574c31adfa1be9";
	//private static final String BASE_URL_INHEALTH = "http://dummy.inhealth.co.id/pelkesws2/";
	private static final String BASE_URL_INHEALTH = "https://app.inhealth.co.id/pelkesws2";
	private static final String TOKEN = "inh_08bba9e55898892215ef4720cb0bea66";
	private static final String Url_EligibilitasPeserta = "/api/EligibilitasPeserta";
	private static final String Url_CekSJP = "/api/CekSJP";
	private static final String Url_ConfirmAKTFirstPayor = "/api/ConfirmAKTFirstPayor";
	private static final String Url_HapusDetailSJP = "/api/HapusDetailSJP";
	private static final String Url_HapusSJP = "/api/HapusSJP";
	private static final String Url_HapusTindakan = "/api/HapusTindakan";
	private static final String Url_InfoSJP = "/api/InfoSJP";
	private static final String Url_Poli = "/api/Poli";
	private static final String Url_ProviderRujukan = "/api/ProviderRujukan";
	private static final String Url_RekapHasilVerifikasi = "/api/RekapHasilVerifikasi";
	private static final String Url_SimpanBiayaINACBGS = "/api/SimpanBiayaINACBGS";
	private static final String Url_SimpanRuangRawat = "/api/SimpanRuangRawat";
	private static final String Url_SimpanSJP = "/api/SimpanSJP";
	private static final String Url_SimpanTindakan = "/api/SimpanTindakan";
	private static final String Url_SimpanTindakanRITL = "/api/SimpanTindakanRITL";
	private static final String Url_UpdateTanggalPulang = "/api/UpdateTanggalPulang";

	public static String GetEligibilitasPeserta(String nokainhealth, String tglpelayanan, String jenispelayanan,
			String poli) {
		String url = BASE_URL_INHEALTH+Url_EligibilitasPeserta;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"nokainhealth\": \""+nokainhealth+"\","
					+ "\"tglpelayanan\": \""+tglpelayanan+"\","
					+ "\"jenispelayanan\": \""+jenispelayanan+"\","
					+ "\"poli\": \""+poli+"\""
					+ "}";
			
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String CekSJP(String nokainhealth, String tanggalsjp, String poli, String tkp) {
		String url = BASE_URL_INHEALTH+Url_CekSJP;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"nokainhealth\": \""+nokainhealth+"\","
					+ "\"tanggalsjp\": \""+tanggalsjp+"\","
					+ "\"poli\": \""+poli+"\","
					+ "\"tkp\": \""+tkp+"\""
					+ "}";
		
			
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String ConfirmAKTFirstPayor(String nosjp, String userid) {
		String url = BASE_URL_INHEALTH+Url_ConfirmAKTFirstPayor;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"nosjp\": \""+nosjp+"\","
					+ "\"userid\": \""+userid+"\""
					+ "}";
			
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String HapusDetailSJP(String nosjp,String notes, String userid) {
		String url = BASE_URL_INHEALTH+Url_HapusDetailSJP;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"nosjp\": \""+nosjp+"\","
					+ "\"notes\": \""+notes+"\","
					+ "\"userid\": \""+userid+"\""
					+ "}";
			
			
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String HapusSJP(String nosjp,String alasanhapus, String userid) {
		String url = BASE_URL_INHEALTH+Url_HapusSJP;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"nosjp\": \""+nosjp+"\","
					+ "\"alasanhapus\": \""+alasanhapus+"\","
					+ "\"userid\": \""+userid+"\""
					+ "}";
			
			
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	public static String SimpanBiayaINACBGS(String nosjp, String kodeinacbg, BigDecimal biayainacbgs, String nosep,
			String notes, String userid) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_SimpanBiayaINACBGS;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"nosjp\": \""+nosjp+"\","
					+ "\"kodeinacbg\": \""+kodeinacbg+"\","
					+ "\"biayainacbgs\": \""+biayainacbgs+"\","
					+ "\"nosep\": \""+nosep+"\","
					+ "\"notes\": \""+notes+"\","
					+ "\"userid\": \""+userid+"\""
					+ "}";
			
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	public static String HapusTindakan(String nosjp, String kodetindakan, String tgltindakan, String notes,
			String userid) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_HapusTindakan;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"nosjp\": \""+nosjp+"\","
					+ "\"kodetindakan\": \""+kodetindakan+"\","
					+ "\"tgltindakan\": \""+tgltindakan+"\","
					+ "\"notes\": \""+notes+"\","
					+ "\"userid\": \""+userid+"\""
					+ "}";
			
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	public static String UpdateTanggalPulang(BigDecimal id, String nosjp, String tglmasuk, String tglkeluar) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_UpdateTanggalPulang;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"id\": \""+id+"\","
					+ "\"nosjp\": \""+nosjp+"\","
					+ "\"tglmasuk\": \""+tglmasuk+"\","
					+ "\"tglkeluar\": \""+tglkeluar+"\""
					+ "}";
		
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	public static String InfoSJP(String nosjp) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_InfoSJP;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"nosjp\": \""+nosjp+"\""
					+ "}";
		
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String Poli(String keyword) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_Poli;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"keyword\": \""+keyword+"\""
					+ "}";
		
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	public static String ProviderRujukan(String keyword) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_ProviderRujukan;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"keyword\": \""+keyword+"\""
					+ "}";
		
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	public static String RekapHasilVerifikasi(String nofpk) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_RekapHasilVerifikasi;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"nofpk\": \""+nofpk+"\""
					+ "}";
		
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	public static String SimpanRuangRawat(String nosjp, String tglmasuk, String kelasrawat, String kodejenispelayanan,
			BigDecimal byharirawat) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_SimpanRuangRawat;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"nosjp\": \""+nosjp+"\","
					+ "\"tglmasuk\": \""+tglmasuk+"\","
					+ "\"kelasrawat\": \""+kelasrawat+"\","
					+ "\"kodejenispelayanan\": \""+kodejenispelayanan+"\","
					+ "\"byharirawat\": \""+byharirawat+"\""
					+ "}";
		
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	public static String SimpanSJP(String tanggalpelayanan, String jenispelayanan, String nokainhealth,
			String nomormedicalreport, String nomorasalrujukan, String kodeproviderasalrujukan,
			String tanggalasalrujukan, String kodediagnosautama, String poli, String username, String informasitambahan,
			String kodediagnosatambahan, BigDecimal kecelakaankerja, String kelasrawat, String kodejenpelruangrawat) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_SimpanSJP;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"tanggalpelayanan\": \""+tanggalpelayanan+"\","
					+ "\"jenispelayanan\": \""+jenispelayanan+"\","
					+ "\"nokainhealth\": \""+nokainhealth+"\","
					+ "\"nomormedicalreport\": \""+nomormedicalreport+"\","
					+ "\"nomorasalrujukan\": \""+nomorasalrujukan+"\","
					+ "\"kodeproviderasalrujukan\": \""+kodeproviderasalrujukan+"\","
					+ "\"tanggalasalrujukan\": \""+tanggalasalrujukan+"\","
					+ "\"kodediagnosautama\": \""+kodediagnosautama+"\","
					+ "\"poli\": \""+poli+"\","
					+ "\"username\": \""+username+"\","
					+ "\"informasitambahan\": \""+informasitambahan+"\","
					+ "\"kodediagnosatambahan\": \""+kodediagnosatambahan+"\","
					+ "\"kecelakaankerja\": \""+kecelakaankerja+"\","
					+ "\"kelasrawat\": \""+kelasrawat+"\","
					+ "\"kodejenpelruangrawat\": \""+kodejenpelruangrawat+"\""
					+ "}";
		
			
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	public static String SimpanTindakan(String jenispelayanan, String nosjp, String tglmasukrawat,
			String tanggalpelayanan, String kodetindakan, String poli, String kodedokter, String biayaaju) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_SimpanTindakan;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"jenispelayanan\": \""+jenispelayanan+"\","
					+ "\"nosjp\": \""+nosjp+"\","
					+ "\"tglmasukrawat\": \""+tglmasukrawat+"\","
					+ "\"tanggalpelayanan\": \""+tanggalpelayanan+"\","
					+ "\"kodetindakan\": \""+kodetindakan+"\","
					+ "\"poli\": \""+poli+"\","
					+ "\"kodedokter\": \""+kodedokter+"\","
					+ "\"biayaaju\": \""+biayaaju+"\""
					+ "}";

			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	public static String SimpanTindakanRITL(String jenispelayanan, String nosjp, String idakomodasi,
			String tglmasukrawat, String tanggalpelayanan, String kodetindakan, String poli, String kodedokter,
			BigDecimal biayaaju) {
		// TODO Auto-generated method stub
		String url = BASE_URL_INHEALTH+Url_SimpanTindakanRITL;

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);
			String input = "{"
					+ "\"token\": \""+TOKEN+"\", "
					+ "\"kodeprovider\": \""+KODEPROVIDER+"\","
					+ "\"jenispelayanan\": \""+jenispelayanan+"\","
					+ "\"nosjp\": \""+nosjp+"\","
					+ "\"idakomodasi\": \""+idakomodasi+"\","
					+ "\"tglmasukrawat\": \""+tglmasukrawat+"\","
					+ "\"tanggalpelayanan\": \""+tanggalpelayanan+"\","
					+ "\"kodetindakan\": \""+kodetindakan+"\","
					+ "\"poli\": \""+poli+"\","
					+ "\"kodedokter\": \""+kodedokter+"\","
					+ "\"biayaaju\": \""+biayaaju+"\""
					+ "}";

			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();

			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	
	
	
	

	


}
