package com.rsmurniteguh.webservice.dep.kthis.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.rsmurniteguh.webservice.dep.all.model.dashboard.AppointmentStatus;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.CathlabAct;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.Diagnosis;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.DischargePlanning;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.EndoskopiAct;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.InpatientBilling;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.LocationMstr;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.MaterialReport;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.MaterialReportOption;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.OutpatientCity;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.PatientVisit;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.Surgeryroom;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.SurgeryroomTime;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.TopOperation;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.UtilizationRadiology;
import com.rsmurniteguh.webservice.dep.all.model.gym.RadiologyScheduleViewModel;
import com.rsmurniteguh.webservice.dep.all.model.gym.RisReportTime;
import com.rsmurniteguh.webservice.dep.biz.DashboardBiz;
import com.rsmurniteguh.webservice.dep.biz.MobileBiz;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class DashboardSer {
	
	public static List<PatientVisit> patient_visit_report(String startdate, String enddate, String tipe) {
		
		List<PatientVisit> data=new ArrayList<PatientVisit>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        
        PreparedStatement ps=null;
        
        String sql="select tab.PATIENT_CLASS, tab.visit, count(tab.PATIENT_ID) as COUNT_PATIENT FROM " + 
        		"(select v.PATIENT_CLASS,v.PATIENT_ID , " + 
        		"CASE " + 
        		"(select count(*) from visit vt where vt.PATIENT_ID = v.PATIENT_ID and vt.PATIENT_TYPE = ? and vt.ADMISSION_DATETIME <= to_date(?, 'ddmmyyyy HH24:MI:SS')) " + 
        		"when 1 then 'new' else 'old' end as visit " + 
        		"from visit v where v.PATIENT_TYPE = ? and v.ADMISSION_DATETIME >= to_date(?, 'ddmmyyyy HH24:MI:SS') and v.ADMISSION_DATETIME <= to_date(?, 'ddmmyyyy HH24:MI:SS')) tab " + 
        		"group by tab.PATIENT_CLASS, tab.visit ";
        
        try{
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tipe);
			ps.setString(2, enddate+" 23:59:59");
			ps.setString(3, tipe);
			ps.setString(4, startdate+" 00:00:00");
			ps.setString(5, enddate+" 23:59:59");
			rs = ps.executeQuery();

			while (rs.next()) {
				PatientVisit dl = new PatientVisit();
				dl.setPatient_class(rs.getString("PATIENT_CLASS"));
				dl.setVisit_type(rs.getString("VISIT"));
				dl.setPatient_count(new BigDecimal(rs.getString("COUNT_PATIENT")));
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
	
	public static List<PatientVisit> patient_visit_los(String startdate, String enddate, String tipe, String start, String end) {
		
		List<PatientVisit> data=new ArrayList<PatientVisit>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        Integer jla = Integer.parseInt(start);
        Integer jlb = Integer.parseInt(end);
        PreparedStatement ps=null;
        
        String sql="SELECT PATIENT_CLASS, count(PATIENT_ID) as COUNT_PATIENT FROM VISIT "
        		+ " WHERE PATIENT_TYPE= ? AND ADMISSION_DATETIME >= to_date(?, 'ddmmyyyy HH24:MI:SS') AND ADMISSION_DATETIME <= to_date(?, 'ddmmyyyy HH24:MI:SS') ";
        if (end.equals("0")) {
			sql = sql+ " AND TRUNC(DISCHARGE_DATETIME - ADMISSION_DATETIME) >= ? ";
		} else {
			sql = sql+ " AND TRUNC(DISCHARGE_DATETIME - ADMISSION_DATETIME) >= ? AND TRUNC(DISCHARGE_DATETIME - ADMISSION_DATETIME) <= ?  ";
		}
        
        sql = sql+ " GROUP BY PATIENT_CLASS ";
        try{
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tipe);
			ps.setString(2, startdate+" 00:00:00");
			ps.setString(3, enddate+" 23:59:59");
			ps.setInt(4, jla);
			if (!end.equals("0")) {
				ps.setInt(5, jlb);
			}			
			rs = ps.executeQuery();

			while (rs.next()) {
				PatientVisit dl = new PatientVisit();
				dl.setPatient_class(rs.getString("PATIENT_CLASS"));
				dl.setPatient_count(new BigDecimal(rs.getString("COUNT_PATIENT")));				
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
	
	public static List<PatientVisit> patient_visit_over(String startdate, String enddate, String tipe) {
		
		List<PatientVisit> data=new ArrayList<PatientVisit>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        PreparedStatement ps=null;
        
        String sql="SELECT PATIENT_CLASS, COUNT(LAST_VISIT_NO) as COUNT_VISIT FROM VISIT"
        		+ " WHERE ADMIT_STATUS NOT IN ('AST5','AST7') AND LAST_VISIT_NO IS NOT NULL"
        		+ " AND PATIENT_TYPE= ? AND ADMISSION_DATETIME >= to_date(?, 'ddmmyyyy HH24:MI:SS') AND ADMISSION_DATETIME <= to_date(?, 'ddmmyyyy HH24:MI:SS')"
        		+ " GROUP BY PATIENT_CLASS ";
        
        try{
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tipe);
			ps.setString(2, startdate+" 00:00:00");
			ps.setString(3, enddate+" 23:59:59");
			rs = ps.executeQuery();

			while (rs.next()) {
				PatientVisit dl = new PatientVisit();
				dl.setPatient_class(rs.getString("PATIENT_CLASS"));
				dl.setPatient_count(new BigDecimal(rs.getString("COUNT_VISIT")));				
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }

	public static List<PatientVisit> patient_visit_more(String startdate, String enddate, String tipe) {
		
		List<PatientVisit> data=new ArrayList<PatientVisit>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        PreparedStatement ps=null;
        
        String sql = "SELECT v.PATIENT_CLASS, COUNT(v.PATIENT_CLASS) AS COUNT_VISIT FROM"
        			 + " (SELECT PATIENT_CLASS,to_char(ADMISSION_DATETIME, 'DD/MON/yyyy') as TANGGAL,PATIENT_ID, COUNT(PATIENT_ID)"
        			 + " FROM VISIT"
        			 + " WHERE PATIENT_TYPE = ? and ADMISSION_DATETIME >= to_date(?, 'ddmmyyyy HH24:MI:SS') and ADMISSION_DATETIME <= to_date(?, 'ddmmyyyy HH24:MI:SS')"
        			 + " GROUP BY PATIENT_CLASS,PATIENT_ID, to_char(ADMISSION_DATETIME, 'DD/MON/yyyy')"
        			 + " HAVING COUNT(PATIENT_ID) > 1) v GROUP BY v.PATIENT_CLASS";
        
        try{
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tipe);
			ps.setString(2, startdate+" 00:00:00");
			ps.setString(3, enddate+" 23:59:59");
			rs = ps.executeQuery();
			while (rs.next()) {
				PatientVisit dl = new PatientVisit();
				dl.setPatient_class(rs.getString("PATIENT_CLASS"));
				dl.setPatient_count(new BigDecimal(rs.getString("COUNT_VISIT")));				
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }

	
	public static List<Diagnosis> diagnosa_report(String startdate, String enddate) {
		
		List<Diagnosis> data=new ArrayList<Diagnosis>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        
        PreparedStatement ps=null;
        
        String sql="select  dm.diagnosis_code, dm.diagnosis_desc, count(vd.visit_diagnosismstr_id) as JUMLAH_DIAGNOSA from visitdiagnosisdetail vd " + 
        		"inner join diagnosismstr dm on dm.diagnosismstr_id = vd.visit_diagnosismstr_id " + 
        		"inner join visitdiagnosis vt on vt.visitdiagnosis_id = vd.visitdiagnosis_id " + 
        		"inner join visit v on v.visit_id = vt.visit_id " + 
        		"where v.admission_datetime >= to_date (?, 'ddmmyyyy HH24:MI:SS') " + 
        		"and v.admission_datetime <= to_date  (?, 'ddmmyyyy HH24:MI:SS') " + 
        		"and dm.diagnosis_code not in ('XX01') " + 
        		"group by dm.diagnosis_code, dm.diagnosis_desc, vd.visit_diagnosismstr_id " + 
        		"order by dm.diagnosis_code ";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, startdate+" 00:00:00");
			ps.setString(2, enddate+" 23:59:59");
			rs = ps.executeQuery();

			while (rs.next()) {
				Diagnosis dl = new Diagnosis();
				dl.setDiagnosis_code(rs.getString("diagnosis_code"));
				dl.setDiagnosis_desc(rs.getString("diagnosis_desc"));
				dl.setJumlah_diagnosa(new BigDecimal(rs.getString("JUMLAH_DIAGNOSA")));
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
 
	public static List<UtilizationRadiology> utilization_radiology_report(String startdate, String enddate) {
		
		List<UtilizationRadiology> data=new ArrayList<UtilizationRadiology>();
        Connection connection=DbConnection.getRisInstance();
        if(connection==null)return null;
        ResultSet rs=null;
        
        PreparedStatement ps=null;
        
        String sql="select s.kind_name as Prosedur, count(s.kind_name) as Total_Prosedur from schedule s " + 
        		"where s.checkin >= to_date (?,'ddmmyyyy HH24:MI:SS') " + 
        		"and s.checkin <= to_date (?,'ddmmyyyy HH24:MI:SS') " + 
        		"and s.checkin is not null " + 
        		"group by s.kind_name ";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, startdate+" 00:00:00");
			ps.setString(2, enddate+" 23:59:59");
			rs = ps.executeQuery();

			while (rs.next()) {
				UtilizationRadiology dl = new UtilizationRadiology();
				dl.setProsedur(rs.getString("Prosedur"));
				dl.setTotal_prosedur(rs.getString("Total_Prosedur"));
				data.add(dl);
			}
            
        }
        catch(SQLException e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
	
	public static List<Surgeryroom> utilization_surgeryroom_report(String ot_code, String ot_date) {
		
		List<Surgeryroom> data=new ArrayList<Surgeryroom>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        
        PreparedStatement ps=null;
        
        String sql="select " + 
        		"(select ps.person_name from person ps where ps.person_id = cp.person_id) surgeon_name, " + 
        		"(select ps.person_name from person ps where ps.person_id = p.person_id) patient_name, " + 
        		"c.card_no, " + 
        		"dm1.diagnosis_code pre_diagnosis_code, " + 
        		"dm1.diagnosis_desc pre_diagnosis_desc, " + 
        		"dm2.diagnosis_code post_diagnosis_code, " + 
        		"dm2.diagnosis_desc post_diagnosis_desc, " + 
        		"case " + 
        		"when otn.otnotes_id is not null then otn.operation_start_datetime " + 
        		"else otra.request_operation_datetime " + 
        		"end operation_start_datetime, " + 
        		"otn.operation_end_datetime, " + 
        		"((otn.operation_end_datetime - otn.operation_start_datetime) * 24 * 60) duration, " + 
        		"case " + 
        		"when otn.otnotes_id is not null then otn.request_remarks " + 
        		"else otra.ot_apprl_remarks " + 
        		"end request_remarks " + 
        		"from otrequestapproval otra " + 
        		"left join otnotes otn on otn.otrequestapproval_id = otra.otrequestapproval_id " + 
        		"inner join otroommstr orm on orm.otroommstr_id = " + 
        		"case " + 
        		"when otn.request_otlocationmstr_id is not null then otn.request_otlocationmstr_id " + 
        		"else otra.ot_location_assigned " + 
        		"end " + 
        		"inner join careprovider cp on cp.careprovider_id = " + 
        		"case " + 
        		"when otn.surgeon_1_careprovider_id is not null then otn.surgeon_1_careprovider_id " + 
        		"else otra.surgeon_1_careprovider_id " + 
        		"end " + 
        		"inner join visit v on v.visit_id = otra.visit_id " + 
        		"inner join patient p on p.patient_id = v.patient_id " + 
        		"inner join card c on c.person_id = p.person_id " + 
        		"inner join visitdiagnosisdetail vdd1 on vdd1.visitdiagnosis_id = " + 
        		"case " + 
        		"when otn.visitdiagnosis_id is not null then otn.visitdiagnosis_id " + 
        		"else otra.visitdiagnosis_id " + 
        		"end " + 
        		"inner join diagnosismstr dm1 on dm1.diagnosismstr_id = vdd1.procedure_diagnosismstr_id " + 
        		"left join visitdiagnosisdetail vdd2 on vdd2.visitdiagnosis_id = otn.after_visitdiagnosis_id " + 
        		"left join diagnosismstr dm2 on dm2.diagnosismstr_id = vdd2.procedure_diagnosismstr_id " + 
        		"where orm.otroom_code = ? and trunc(to_date (?,'ddmmyyyy HH24:MI:SS')) = " + 
        		"case " + 
        		"when otn.operation_start_datetime is not null then trunc(otn.operation_start_datetime) " + 
        		"else trunc(otra.request_operation_datetime) " + 
        		"end " + 
        		"and otra.request_status in ('OES3', 'OES5') and otra.defunct_ind = 'N' " + 
        		"order by " + 
        		"case " + 
        		"when otn.operation_start_datetime is not null then otn.operation_start_datetime " + 
        		"else otra.request_operation_datetime " + 
        		"end ";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, ot_code);
			ps.setString(2, ot_date);
			rs = ps.executeQuery();
			
			
			while (rs.next()) {
				Surgeryroom dl = new Surgeryroom();
				dl.setSurgeon_name(rs.getString("surgeon_name"));
				dl.setPatient_name(rs.getString("patient_name"));
				dl.setCard_no(rs.getString("card_no"));
				dl.setPre_diagnosis_code(rs.getString("pre_diagnosis_code"));
				dl.setPre_diagnosis_desc(rs.getString("pre_diagnosis_desc"));
				dl.setPost_diagnosis_code(rs.getString("post_diagnosis_code"));
				dl.setPost_diagnosis_desc(rs.getString("post_diagnosis_desc"));
				dl.setOperation_start_datetime(rs.getString("operation_start_datetime"));
				dl.setOperation_end_datetime(rs.getString("operation_end_datetime"));
				dl.setDuration(rs.getString("duration"));
				dl.setRequest_remarks(rs.getString("request_remarks"));
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
 
	public static List<SurgeryroomTime> utilization_surgeryroom_time_report(String efektif, String startdate, String enddate) {
		
		List<SurgeryroomTime> data=new ArrayList<SurgeryroomTime>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        
        PreparedStatement ps=null;
        
        String sql="SELECT COALESCE(SUM((LEAST(END_TIME, DATETIME + 1) - START_TIME) * 24 / ?), 0) UTILISASI, RM.OTROOMMSTR_ID, RM.OTROOM_DESC, RM.DATETIME FROM ( " + 
        		"    SELECT OTROOMMSTR_ID, OTROOM_DESC, DATETIME FROM ( " + 
        		"        SELECT TO_DATE(TRUNC(to_date (?,'ddmmyyyy HH24:MI:SS')) + (ROWNUM - 1)) DATETIME " + 
        		"        FROM DUAL CONNECT BY ROWNUM <= (trunc(to_date (?,'ddmmyyyy HH24:MI:SS')) - trunc(to_date (?,'ddmmyyyy HH24:MI:SS')) + 1) " + 
        		"    ) DATELIST, OTROOMMSTR " + 
        		"    WHERE DEFUNCT_IND = 'N' " + 
        		") RM " + 
        		"LEFT JOIN ( " + 
        		"    SELECT REQUEST_OTLOCATIONMSTR_ID, OPERATION_START_DATETIME START_TIME, OPERATION_END_DATETIME END_TIME FROM OTNOTES N " + 
        		") RA ON RA.REQUEST_OTLOCATIONMSTR_ID = RM.OTROOMMSTR_ID " + 
        		"      AND TRUNC(RA.START_TIME) = TRUNC(RM.DATETIME) " + 
        		"GROUP BY RM.OTROOMMSTR_ID, RM.OTROOM_DESC, RM.DATETIME  " + 
        		"ORDER BY RM.OTROOM_DESC, RM.DATETIME";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, efektif);
			ps.setString(2, startdate+" 00:00:00");
			ps.setString(3, enddate+" 23:59:59");
			ps.setString(4, startdate+" 00:00:00");
			rs = ps.executeQuery();
			
			while (rs.next()) {
				SurgeryroomTime dl = new SurgeryroomTime();
				dl.setUtilisasi(rs.getString("utilisasi"));
				dl.setOtroommstr_id(rs.getString("otroommstr_id"));
				dl.setOtroom_desc(rs.getString("otroom_desc"));
				dl.setDatetime(rs.getString("datetime"));
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
	
	public static List<DischargePlanning> discharge_planning_report(String tanggal) {
		
		List<DischargePlanning> data=new ArrayList<DischargePlanning>();
        Connection connection=DbConnection.getMysqlOnlineQueueInstance2();
        if(connection==null)return null;
        ResultSet rs=null;
        
        PreparedStatement ps=null;

        SimpleDateFormat df_in = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat df_out = new SimpleDateFormat("yyyy-MM-dd");
        
        
        Calendar cal = Calendar.getInstance();
        String tgl_out = df_out.format(cal.getTime());
        try {
        	Date d_tanggal = df_in.parse(tanggal);
        	tgl_out = df_out.format(d_tanggal);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
        
        String sql="SELECT 'Sudah' STATUS, COUNT(* ) JUMLAH  " + 
        		"FROM tb_info  " + 
        		"WHERE  " + 
        		"DATE(jadwal_plg) >= ? AND DATE(jadwal_plg) < DATE(DATE_ADD(?, INTERVAL 1 DAY))  " + 
        		"AND konfir_far = '39'  " + 
        		"AND konfir_ver = '86'  " + 
        		"AND konfir_kas = '38'  " + 
        		" " + 
        		"UNION ALL  " + 
        		" " + 
        		"SELECT 'Belum' STATUS, COUNT(*) JUMLAH  " + 
        		"FROM tb_info  " + 
        		"WHERE  " + 
        		"DATE(jadwal_plg) >= ?  " + 
        		"AND DATE(jadwal_plg) < DATE(DATE_ADD(?, INTERVAL 1 DAY))  " + 
        		"AND (konfir_far = '' || konfir_ver = '' || konfir_kas= '')  ";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tgl_out);
			ps.setString(2, tgl_out);
			ps.setString(3, tgl_out);
			ps.setString(4, tgl_out);
			rs = ps.executeQuery();

			while (rs.next()) {
				DischargePlanning dl = new DischargePlanning();
				dl.setStatus(rs.getString("status"));
				dl.setJumlah(rs.getString("jumlah"));
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
	
	public static List<CathlabAct> cathlab_act_report(String startdate, String enddate) {
		
		List<CathlabAct> data=new ArrayList<CathlabAct>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        
        PreparedStatement ps=null;

        SimpleDateFormat df_in = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat df_out = new SimpleDateFormat("ddMMyyyy");
        
        
        Calendar cal = Calendar.getInstance();
        String tglstart_out = df_out.format(cal.getTime());
        String tglend_out = df_out.format(cal.getTime());
        try {
        	Date d_tanggalstart = df_in.parse(startdate);
        	tglstart_out = df_out.format(d_tanggalstart);
        	Date d_tanggalend = df_in.parse(enddate);
        	tglend_out = df_out.format(d_tanggalend);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
        
        String sql="SELECT PGCOMMON.fxGetCodeDesc(ISCM.IP_RECEIPT_ITEM_CAT) as cathlab, COUNT(*) as jumlah FROM PATIENTACCOUNTTXN PAT  " + 
        		"INNER JOIN PATIENTACCOUNT PA ON PA.PATIENTACCOUNT_ID = PAT.PATIENTACCOUNT_ID  " + 
        		"INNER JOIN TXNCODEMSTR TCM ON TCM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID " + 
        		"INNER JOIN CHARGEITEMMSTR CIM ON CIM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID " + 
        		"INNER JOIN ITEMSUBCATEGORYMSTR ISCM ON ISCM.ITEMSUBCATEGORYMSTR_ID = CIM.ITEMSUBCATEGORYMSTR_ID " + 
        		"WHERE ISCM.IP_RECEIPT_ITEM_CAT IN ('IRI34') " + 
        		"AND PAT.ENTERED_DATETIME >= TO_DATE (?, 'ddmmyyyy HH24:MI:SS') " + 
        		"AND PAT.ENTERED_DATETIME <= TO_DATE (?, 'ddmmyyyy HH24:MI:SS') " + 
        		"GROUP BY ISCM.IP_RECEIPT_ITEM_CAT ";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tglstart_out+" 00:00:00");
			ps.setString(2, tglend_out+" 23:59:59");
			rs = ps.executeQuery();

			while (rs.next()) {
				CathlabAct dl = new CathlabAct();
				dl.setCathlab(rs.getString("cathlab"));
				dl.setJumlah(rs.getString("jumlah"));
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
	
	public static List<EndoskopiAct> endoskopi_act_report(String startdate, String enddate) {
		
		List<EndoskopiAct> data=new ArrayList<EndoskopiAct>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        
        PreparedStatement ps=null;

        SimpleDateFormat df_in = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat df_out = new SimpleDateFormat("ddMMyyyy");
        
        
        Calendar cal = Calendar.getInstance();
        String tglstart_out = df_out.format(cal.getTime());
        String tglend_out = df_out.format(cal.getTime());
        try {
        	Date d_tanggalstart = df_in.parse(startdate);
        	tglstart_out = df_out.format(d_tanggalstart);
        	Date d_tanggalend = df_in.parse(enddate);
        	tglend_out = df_out.format(d_tanggalend);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
        
        String sql="SELECT PGCOMMON.fxGetCodeDesc(ISCM.IP_RECEIPT_ITEM_CAT) as endoskopi, COUNT (*) as jumlah FROM PATIENTACCOUNTTXN PAT  " + 
        		"INNER JOIN PATIENTACCOUNT PA ON PA.PATIENTACCOUNT_ID = PAT.PATIENTACCOUNT_ID  " + 
        		"INNER JOIN TXNCODEMSTR TCM ON TCM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID " + 
        		"INNER JOIN CHARGEITEMMSTR CIM ON CIM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID " + 
        		"INNER JOIN ITEMSUBCATEGORYMSTR ISCM ON ISCM.ITEMSUBCATEGORYMSTR_ID = CIM.ITEMSUBCATEGORYMSTR_ID " + 
        		"WHERE ISCM.IP_RECEIPT_ITEM_CAT IN ('IRI54','IRI55','IRI56') " + 
        		"AND PAT.ENTERED_DATETIME >= TO_DATE (?, 'ddmmyyyy HH24:MI:SS') " + 
        		"AND PAT.ENTERED_DATETIME <= TO_DATE (?, 'ddmmyyyy HH24:MI:SS') " + 
        		"GROUP BY ISCM.IP_RECEIPT_ITEM_CAT ";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tglstart_out+" 00:00:00");
			ps.setString(2, tglend_out+" 23:59:59");
			rs = ps.executeQuery();

			while (rs.next()) {
				EndoskopiAct dl = new EndoskopiAct();
				dl.setEndoskopi(rs.getString("endoskopi"));
				dl.setJumlah(rs.getString("jumlah"));
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
	
	public static List<OutpatientCity> outpatient_city_report(String startdate, String enddate) {
		
		List<OutpatientCity> data=new ArrayList<OutpatientCity>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        
        PreparedStatement ps=null;

        SimpleDateFormat df_in = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat df_out = new SimpleDateFormat("ddMMyyyy");
        
        
        Calendar cal = Calendar.getInstance();
        String tglstart_out = df_out.format(cal.getTime());
        String tglend_out = df_out.format(cal.getTime());
        try {
        	Date d_tanggalstart = df_in.parse(startdate);
        	tglstart_out = df_out.format(d_tanggalstart);
        	Date d_tanggalend = df_in.parse(enddate);
        	tglend_out = df_out.format(d_tanggalend);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
        
        String sql="((SELECT CM.CITY_DESC, COUNT (*) TOTAL, 'OP' as status FROM ADDRESS ADS " + 
        		"INNER JOIN CITYMSTR CM ON ADS.CITY = CM.CITY_CODE " + 
        		"INNER JOIN PERSON PS ON ADS.STAKEHOLDER_ID = PS.STAKEHOLDER_ID " + 
        		"INNER JOIN PATIENT PT ON PT.PERSON_ID = PS.PERSON_ID " + 
        		"INNER JOIN VISIT VT ON VT.PATIENT_ID = PT.PATIENT_ID " + 
        		"WHERE VT.PATIENT_TYPE = 'PTY2'  " + 
        		"AND VT.ADMISSION_DATETIME >= TO_DATE (?,'DDMMYYYY') " + 
        		"AND VT.ADMISSION_DATETIME <= TO_DATE (?,'DDMMYYYY') " + 
        		"AND VT.ADMIT_STATUS <> 'AST5' " + 
        		"AND CM.CITY_DESC <> 'MEDAN' " + 
        		"GROUP BY CM.CITY_DESC )"
        		+ " UNION "+
        		"(SELECT CM.CITY_DESC, COUNT (*) TOTAL, 'IP' as status FROM ADDRESS ADS " + 
        		"INNER JOIN CITYMSTR CM ON ADS.CITY = CM.CITY_CODE " + 
        		"INNER JOIN PERSON PS ON ADS.STAKEHOLDER_ID = PS.STAKEHOLDER_ID " + 
        		"INNER JOIN PATIENT PT ON PT.PERSON_ID = PS.PERSON_ID " + 
        		"INNER JOIN VISIT VT ON VT.PATIENT_ID = PT.PATIENT_ID " + 
        		"WHERE VT.PATIENT_TYPE = 'PTY1'  " + 
        		"AND VT.ADMISSION_DATETIME >= TO_DATE (?,'DDMMYYYY') " + 
        		"AND VT.ADMISSION_DATETIME <= TO_DATE (?,'DDMMYYYY') " + 
        		"AND VT.ADMIT_STATUS <> 'AST5' " + 
        		"AND CM.CITY_DESC <> 'MEDAN' " + 
        		"GROUP BY CM.CITY_DESC)) ";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tglstart_out);
			ps.setString(2, tglend_out);
			ps.setString(3, tglstart_out);
			ps.setString(4, tglend_out);
			rs = ps.executeQuery();

			while (rs.next()) {
				OutpatientCity dl = new OutpatientCity();
				dl.setCity_desc(rs.getString("CITY_DESC"));
				dl.setStatus(rs.getString("status"));
				dl.setTotal(rs.getString("TOTAL"));
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
	
	public static List<MaterialReport> material_report(String opt1, String opt2) {

		List<MaterialReport> data=new ArrayList<MaterialReport>();
		Map<String, MaterialReportOption> option = new HashMap<>();
		/*
		 * MEDICINE TYPE
		 */
		MaterialReportOption MIT1 = new MaterialReportOption();
		MIT1.setType("MIT1");
		MIT1.setDead_stock(new BigDecimal(180));
		MIT1.setSlow_moving(new BigDecimal(90));
		option.put("medicine", MIT1);
		
		/*
		 * NON-MEDICINE TYPE
		 */
		MaterialReportOption MIT2 = new MaterialReportOption();
		MIT2.setType("MIT2");
		MIT2.setDead_stock(new BigDecimal(360));
		MIT2.setSlow_moving(new BigDecimal(180));
		option.put("nonmedicine", MIT2);
		
		
		
		MaterialReportOption assign = new MaterialReportOption();
		if (option.get(opt1)== null) {
			return data;
		}
		MaterialReportOption choose = option.get(opt1);
			assign.setType(choose.getType());
			if (opt2.equals("slow_moving")) {
				assign.setNumberset(choose.getSlow_moving());
			} else if (opt2.equals("dead_stock")){
				assign.setNumberset(choose.getDead_stock());
			} else {
				return data;
			}
		
		
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        
        PreparedStatement ps=null;
        
        String sql="select mim1.material_item_type,(SELECT mcm.MATERIAL_ITEM_CAT_DESC FROM MATERIALITEMCATMSTR mcm where mcm.MATERIALITEMCATMSTR_ID=mim1.MATERIALITEMCATMSTR_ID) CAT,mim1.material_item_code, mim1.material_item_name, sis1.expiry_date, sis1.balance_qty, MP.SALES_PRICE,MP.WHOLESALE_PRICE, "+
			"SM.STORE_DESC AS LOCATION from MATERIALITEMMSTR mim1  "+
			"inner join storeitem si1 on si1.materialitemmstr_id = mim1.materialitemmstr_id "+
			"inner join storeitemstock sis1 on sis1.storeitem_id = si1.storeitem_id "+
			"INNER JOIN STOREMSTR SM ON SM.STOREMSTR_ID = SI1.STOREMSTR_ID "+
			"INNER JOIN MATERIALPRICE MP ON MP.MATERIALITEMMSTR_ID = MIM1.MATERIALITEMMSTR_ID "+
			"where mim1.MATERIALITEMMSTR_ID not in  "+
			"( "+
			"select mim.MATERIALITEMMSTR_ID from MATERIALTXNDETAIL mtd  "+
			"inner join storeitemstock sis on sis.STOREITEMSTOCK_ID = mtd.STOREITEMSTOCK_ID "+
			"inner join STOREITEM si on si.STOREITEM_ID = sis.STOREITEM_ID "+
			"inner join materialitemmstr mim on mim.MATERIALITEMMSTR_ID = si.MATERIALITEMMSTR_ID "+
			"where mtd.CREATED_DATETIME > SYSDATE - ? "+
			") "+
			"and mim1.DEFUNCT_IND = 'N' and mim1.MATERIAL_ITEM_TYPE = ? "+
			"AND SIS1.BALANCE_QTY > 0 "+
			"ORDER BY SM.STORE_DESC";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setBigDecimal(1, assign.getNumberset());
			ps.setString(2, assign.getType());
			rs = ps.executeQuery();
			
			while (rs.next()) {
				MaterialReport dl = new MaterialReport();
				dl.setMaterial_item_type(rs.getString("material_item_type"));
				dl.setCat(rs.getString("CAT"));
				dl.setMaterial_item_code(rs.getString("material_item_code"));
				dl.setMaterial_item_name(rs.getString("material_item_name"));
				dl.setExpiry_date(rs.getString("expiry_date"));
				dl.setBalance_qty(rs.getString("balance_qty"));
				dl.setSales_price(rs.getString("SALES_PRICE"));
				dl.setWhosale_price(rs.getString("WHOLESALE_PRICE"));
				dl.setLocation(rs.getString("LOCATION"));
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
	
	public static List<LocationMstr> location() {
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
        
        List<LocationMstr> data = new ArrayList<LocationMstr>();
        
        PreparedStatement ps=null;
        
        String sql="SELECT LOCATIONMSTR_ID,"
        		+ " LOCATION_TYPE, "
        		+ " LOCATION_CODE, "
        		+ " LOCATION_NAME, "
        		+ " LOCATION_DESC, "
        		+ " SHORT_CODE, "
        		+ " ADDRESS "
        		+ " FROM LOCATIONMSTR"
        		+ " WHERE DEFUNCT_IND = 'N' ";        
        try{          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
//			ps.setQueryTimeout(60000);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				LocationMstr dl = new LocationMstr();
				dl.setLocationMstr_id(rs.getString("LOCATIONMSTR_ID"));
				dl.setLocation_type(rs.getString("LOCATION_TYPE"));
				dl.setLocation_code(rs.getString("LOCATION_CODE"));
				dl.setLocation_name(rs.getString("LOCATION_NAME"));
				dl.setLocation_desc(rs.getString("LOCATION_DESC"));
				dl.setShort_code(rs.getString("SHORT_CODE"));
				dl.setAddress(rs.getString("ADDRESS"));
				data.add(dl);
			}
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
    }
	
	public static List<RadiologyScheduleViewModel> radiology_schedule_report(String date, String type) {
		
		Connection connection = null;
		connection = DbConnection.getRisInstance();
		PreparedStatement st = null;
		ResultSet rs = null;
		
		List<RadiologyScheduleViewModel> all=new ArrayList<RadiologyScheduleViewModel>();
		
		if(connection == null)return null;
		String sql = "select  to_char(dt.tanggal, 'HH24:MI') as JAM, s.EXAM_DESC from "
				+ " (SELECT to_date('@date@ 00:00:00', 'yyyy-mm-dd HH24:MI:SS') + ((ROWNUM-1)* 15/1440) tanggal"
				+ " FROM DUAL CONNECT BY ROWNUM <= 96) dt"
				+ " left outer join SCHEDULE s on s.ARRANGE = dt.tanggal and to_char(arrange, 'yyyy-mm-dd') = ?  "
				+ " and modality = ? "
				+ " union all "
				+ " select to_char(s2.arrange, 'HH24:MI') as JAM, s2.EXAM_DESC from "
				+ " SCHEDULE s2 where to_char(s2.arrange, 'yyyy-mm-dd') = ?  and s2.modality = ? order by jam ";
		try {

			st=connection.prepareStatement( sql.replace("@date@", date) );			
			st.setString(1, date);
			st.setString(2, type);
			st.setString(3, date);
			st.setString(4, type);
			rs=st.executeQuery();
			
			while(rs.next())
			{
				RadiologyScheduleViewModel dl=new RadiologyScheduleViewModel();
				dl.setHour(rs.getString("JAM"));
				dl.setExamDesc(rs.getString("EXAM_DESC"));
				all.add(dl);
			}
			
			// hapus data yang null
			RadiologyScheduleViewModel temp = new RadiologyScheduleViewModel();
			for (Iterator<RadiologyScheduleViewModel> iter = all.iterator(); iter.hasNext();) {
				RadiologyScheduleViewModel dl = iter.next();
				if (temp.getExamDesc()!=null) {
					if (temp.getHour().equals(dl.getHour()) && temp.getExamDesc().equals(dl.getExamDesc())  ) {
						iter.remove();
					}
				}
				
				temp = dl;
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (st!=null) try  { st.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	public static List<RisReportTime> radiology_responsetime_report(String startdate, String enddate, String jenis) {
		List<RisReportTime> data=new ArrayList<RisReportTime>();

		Connection connection = null;
		Connection connection2 = null;
		connection=DbConnection.getRisReportInstance();
		connection2=DbConnection.getPooledConnection();
		if(connection==null || connection2==null)return null;
		
		ResultSet rs = null;
		ResultSet rs2 = null;
		
		/*
		 * KONDISI KETIKA MEMILIKI TIPE RADIOLOGY
		 */
		String filter = "";
		if (!jenis.equals("All") && jenis!=null ) {
			filter = " AND iscm.item_subcat_desc = '"+jenis+"' ";
		}
		
		String sql="SELECT R.ID_Report, R.request_date, R.patient_name,R.admission_id, R.no_rad, R.patient_ID, R.patient_sex, R.patient_age, R.request_physician, R.modality, R.create_by, " + 
				"  CONCAT(replace(convert(char(10),R.time_photo,102),'.','-'),convert(char(5), R.time_photo, 108)) as difoto, " + 
				"  CONCAT(replace(convert(char(10),D.Create_time,102),'.','-'),convert(char(5), D.Create_time, 108)) as dibaca " + 
				"  FROM Tbl_report R " + 
				"  	INNER JOIN " + 
				"		(select Row_number() OVER (partition by t.ID_Report order by t.Create_time desc) as num, t.Create_time, ID_Report from Tbl_Report_Details t where t.Create_time is not null) D ON D.ID_Report = R.ID_Report and num = 1 " + 
				"  WHERE R.admission_id IN [WHERE_IN] " +
				"  GROUP BY request_date,R.ID_Report, R.patient_name, admission_id, R.no_rad, R.patient_ID, R.patient_sex, R.patient_age, R.request_date, R.request_physician, R.modality, R.create_by, R.create_time, R.time_photo, D.Create_time ";
		
		String sql2 = "select oe.entered_datetime daftar, oe.VISIT_ID, c.card_no as patient_id, oe.ORDERENTRY_ID, oei.ORDERENTRYITEM_ID admission_id, lm.LOCATION_DESC,  " + 
				"				 p2.PERSON_NAME, iscm.item_subcat_desc modality, tcm.txn_code, tcm.TXN_DESC,  " + 
				"				 oe.REMARKS, (case v.patient_type when 'PTY1' then 'IP' when 'PTY2' then 'OP' end) as patient_source, oe.order_status, pgcommon.fxGetCodeDesc(v.PATIENT_CLASS) as PATIENT_CLASS,  " + 
				"				 pgcommon.fxGetCodeDesc(p2.SEX) as SEX, p2.BIRTH_DATE, p.person_name as REQUEST_PHYSICIAN  " + 
				"				 from ORDERENTRY oe  " + 
				"				 inner join VISIT v on v.visit_id = oe.visit_id  " + 
				"				 inner join ORDERENTRYITEM oei on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID  " + 
				"				 inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = oei.ORDERITEMMSTR_ID  " + 
				"				 inner join ITEMSUBCATEGORYMSTR iscm on iscm.ITEMSUBCATEGORYMSTR_ID = oim.ITEMSUBCATEGORYMSTR_ID  " + 
				"				 inner join ITEMCATEGORYMSTR icm on icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID  " + 
				"				 inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = oe.LOCATIONMSTR_ID  " + 
				"				 inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID  " + 
				"				 inner join patient pt on pt.patient_id = v.patient_id  " + 
				"				 inner join person p2 on p2.person_id = pt.person_id  " + 
				"				 inner join card c on c.person_id =  pt.person_id  " + 
				"				 inner join person p on p.PERSON_ID = cp.PERSON_ID  " + 
				"				 inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID  " + 
				"				 where icm.ITEM_TYPE = 'ITY7'  " + 
				"				 and oe.entered_datetime between to_date(?, 'yyyy-mm-dd HH24:mi:ss') and  " + 
				"				 to_date(?, 'yyyy-mm-dd HH24:mi:ss')  " +  filter +
				"				 order by oe.ENTERED_DATETIME";
		
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		try {
			

			ps2 = connection2.prepareStatement(sql2, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(60000);
			ps2.setString(1, startdate);
			ps2.setString(2, enddate+" 23:59:59");
			rs2=ps2.executeQuery();
			
			String where_in = "(";
			while(rs2.next())
			{
				RisReportTime dl=new RisReportTime();
				dl.setDaftar(rs2.getString("daftar"));
				dl.setAdmission_id(rs2.getString("admission_id"));
				dl.setPatient_name(rs2.getString("PERSON_NAME"));
				dl.setRequest_physician(rs2.getString("REQUEST_PHYSICIAN"));
				dl.setModality(rs2.getString("modality"));
				data.add(dl);
				
				if (rs2.isLast()) {
					where_in = where_in+rs2.getString("admission_id")+")";
				}else {
					where_in = where_in+rs2.getString("admission_id")+",";
				}
			}
			
			
			
			if (!where_in.equals("(")) {
				ps = connection.prepareStatement(sql.replace("[WHERE_IN]", where_in));
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				rs=ps.executeQuery();
				
				while(rs.next())
				{
					
					for (RisReportTime risReportTime : data) {
						if (risReportTime.getAdmission_id().equals(rs.getString("admission_id"))) {
							risReportTime.setId_report(rs.getString("ID_Report"));
							risReportTime.setRequest_date(rs.getString("request_date"));
							risReportTime.setNo_rad(rs.getString("no_rad"));
							risReportTime.setPatient_id(rs.getString("patient_ID"));
							risReportTime.setPatient_sex(rs.getString("patient_sex"));
							risReportTime.setPatient_age(rs.getString("patient_age"));
							risReportTime.setCreate_by(rs.getString("create_by"));
							risReportTime.setDipoto(rs.getString("difoto"));
							risReportTime.setDibaca(rs.getString("dibaca"));
						}
					}
				}
			}
			
			// hapus data yang null
			for (Iterator<RisReportTime> iter = data.iterator(); iter.hasNext(); ) {
				RisReportTime dl = iter.next();
			    if (dl.getPatient_name()==null ) {
			        iter.remove();
			    }
			}
			
			
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		     
		     if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection2!=null) try { connection2.close();} catch (Exception ignore){}
		   }
		
		return data;
	}
	
	public static List<AppointmentStatus> top_registered_appointment_report(String tanggal) {
		List<AppointmentStatus> data = new ArrayList<AppointmentStatus>();
		Connection connection = null;
		connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		
		
		String sql="  SELECT TOP(5)  convert(varchar,queue_date,111) as tanggal, count(careprovider_id) as count_patient, doctor_name, department " + 
				"  FROM tb_registrasi " + 
				"  WHERE queue_date = ? AND status =? AND department='"+DashboardBiz.DEPARTMENT_BPJS+"' " + 
				"  group by convert(varchar,queue_date,111), doctor_name, department, status " + 
				"  HAVING count(careprovider_id)>0  "+ 
				"  order by count(careprovider_id) DESC ";
		
		String sql2="  SELECT TOP(5)  convert(varchar,queue_date,111) as tanggal, count(careprovider_id) as count_patient, doctor_name, department " + 
				"  FROM tb_registrasi " + 
				"  WHERE queue_date = ? AND status =? AND department='"+DashboardBiz.DEPARTMENT_POLI+"' " + 
				"  group by convert(varchar,queue_date,111), doctor_name, department, status " + 
				"  HAVING count(careprovider_id)>0  "+ 
				"  order by count(careprovider_id) DESC ";
		
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(30000);
			ps.setString(1, tanggal);
			ps.setString(2, DashboardBiz.APPOINTMENT_STATUS_REGISTERED);
			rs=ps.executeQuery();
			
			while(rs.next())
			{
				AppointmentStatus dl = new AppointmentStatus();
				dl.setTanggal(rs.getString("tanggal"));
				dl.setCount_patient(rs.getString("count_patient"));
				dl.setDoctor_name(rs.getString("doctor_name"));
				dl.setDepartment(rs.getString("department"));
				data.add(dl);
			}
			
			ps2 = connection.prepareStatement(sql2);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(30000);
			ps2.setString(1, tanggal);
			ps2.setString(2, DashboardBiz.APPOINTMENT_STATUS_REGISTERED);
			rs2=ps2.executeQuery();
			
			while(rs2.next())
			{
				AppointmentStatus dl2 = new AppointmentStatus();
				dl2.setTanggal(rs2.getString("tanggal"));
				dl2.setCount_patient(rs2.getString("count_patient"));
				dl2.setDoctor_name(rs2.getString("doctor_name"));
				dl2.setDepartment(rs2.getString("department"));
				data.add(dl2);
			}
		
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     

		     if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return data;
	}
	
	public static List<AppointmentStatus> appointment_report(String tanggal, String department) {
		
		//List<AppointmentStatus> temp = new ArrayList<AppointmentStatus>();
		List<AppointmentStatus> data = new ArrayList<AppointmentStatus>();
		Connection connection = null;
		connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		ResultSet rs = null;
		
		String sql="SELECT distinct convert(varchar,queue_date,111) as tanggal, careprovider_id, doctor_name, status , department, source " + 
				"FROM tb_registrasi " + 
				"WHERE queue_date = ? AND department = ? ";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, tanggal);
			ps.setString(2, department);
			rs=ps.executeQuery();
			
			while(rs.next())
			{
				AppointmentStatus dl = new AppointmentStatus();
				dl.setTanggal(rs.getString("tanggal"));
				dl.setDoctor_name(rs.getString("doctor_name"));
				dl.setStatus(rs.getString("status"));
				dl.setDepartment(rs.getString("department"));
				dl.setSource(rs.getString("source"));
				dl.setCareprovider_id(rs.getString("careprovider_id"));
				data.add(dl);

				//System.out.println(dl.getTanggal()+"|"+dl.getDoctor_name()+"|"+dl.getDepartment()+"|"+rs.getString("count_patient"));
			}
			
//			Map<String, AppointmentSource> all = new HashMap<String, AppointmentSource>();
//			for (AppointmentStatus as : temp) {
//				if (all.get(as.getCareprovider_id()+as.getDepartment())==null) {
//					AppointmentSource dl = new AppointmentSource();
//					dl.setTanggal(as.getTanggal());
//					dl.setDoctor_name(as.getDoctor_name());
//					dl.setDepartment(as.getDepartment());
//					dl.setSource(as.getSource());
//					dl.setRegistered(new BigDecimal(0));
//					dl.setConfirm(new BigDecimal(0));
//					dl.setWait(new BigDecimal(0));
//					dl.setCancel(new BigDecimal(0));
//					all.put(as.getCareprovider_id()+as.getDepartment(),dl);
//				}
//				
//				AppointmentSource update = all.get(as.getCareprovider_id()+as.getDepartment());
//				if (as.getStatus().equals(DashboardBiz.APPOINTMENT_STATUS_REGISTERED)) {
//					update.setRegistered(update.getRegistered().add(new BigDecimal(as.getCount_patient())));
//				}else if (as.getStatus().equals(DashboardBiz.APPOINTMENT_STATUS_CONFIRM)) {
//					update.setConfirm(update.getConfirm().add(new BigDecimal(as.getCount_patient())));
//				}else if (as.getStatus().equals(DashboardBiz.APPOINTMENT_STATUS_WAIT)) {
//					update.setWait(update.getWait().add(new BigDecimal(as.getCount_patient())));
//				}else if (as.getStatus().equals(DashboardBiz.APPOINTMENT_STATUS_CANCEL) || as.getStatus().equals("Delete")) {
//					update.setCancel(update.getCancel().add(new BigDecimal(as.getCount_patient())));
//				}
//				all.put(as.getCareprovider_id()+as.getDepartment(), update);
//			
//			}
//
//			for (Map.Entry<String, AppointmentSource> as : all.entrySet())
//			{
//				data.add(as.getValue());
//			}
		
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return data;
	}
	
	public static Object inpatient_billing_report(String billing_id){
		List<InpatientBilling> data=new ArrayList<InpatientBilling>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
  
        PreparedStatement ps=null;
        
        String sql="SELECT AA.PERSON_NAME ,        AA.MRTIP ,        AA.SUBSPECIALTY_DESC ,        AA.WARD_DESC ,        AA.PATIENT_CLASS ,        AA.VISIT_NO ,        AA.iscm_cat ,        AA.txn_code,        UPPER(AA.TXN_DESC) AS TXN_DESC ,        AA.ORDERED_CAREPROVIDER_ID ,        AA.DOCTOR_NAME ,        AA.qty_uom,        AA.base_unit_price ,        AA.qty ,        AA.txn_amount,        AA.claimable_amount,        AA.deposit_txn_amount,        AA.bill_datetime ,        AA.TOTALDISC ,        AA.ADMISSION_DATETIME ,        AA.DISCHARGE_DATETIME ,        AA.datecnt ,        AA.DIAGNOSIS_DESC ,        AA.TXN_DATETIME                         ,                       AA.BILL_ID                                   ,                                 AA.USER_NAME,                                 nvl(AA.remarks, ' ') AS remarks FROM   (SELECT p.PERSON_NAME ,           pgpmi.fxGetMRN(pt.PATIENT_ID, 'MRTIP') MRTIP ,           sspm.SUBSPECIALTY_DESC ,           wm.WARD_DESC ,           pgpmi.fxGetFinancialClassNS(v.VISIT_ID) PATIENT_CLASS ,           v.VISIT_NO ,           PGCOMMON.fxGetCodeDesc(iscm.ip_receipt_item_cat) iscm_cat ,           tcm.txn_code,           pat.TXN_DESC ,           cg.ordered_careprovider_id ,           d.person_name DOCTOR_NAME ,           pat.txn_datetime                             ,                           bl.bill_id   ,um.user_name ,pat.remarks ,PGCOMMON.fxGetCodeDesc(cg.qty_uom) qty_uom,cg.base_unit_price ,sum(cg.qty) qty ,sum(pat.txn_amount) txn_amount,sum(pat.claimable_amount) claimable_amount,sum(pat.deposit_txn_amount) deposit_txn_amount,bl.bill_datetime ,      (SELECT sum(BB.TOTALDISC)       FROM         (SELECT sum(pat.claimable_amount) TOTALDISC          FROM bill bl,               patientaccounttxn pat,               txncodemstr tcm,               chargeitemmstr cim,               itemsubcategorymstr iscm,               charge cg,               patientaccount pa,               visit v,               patient pt,               person p ,               SUBSPECIALTYMSTR sspm,               WARDMSTR wm,               careprovider cp,               person d,               usermstr um          WHERE bl.bill_id =  ?            AND cg.ordered_careprovider_id = cp.careprovider_id (+)            AND cp.person_id = d.person_id (+)            AND bl.bill_id = pat.bill_id            AND pat.PATIENTACCOUNT_ID =bl.PATIENTACCOUNT_ID            AND pat.patientaccounttxn_id = cg.patientaccounttxn_id            AND pat.txncodemstr_id = tcm.txncodemstr_id            AND tcm.txncodemstr_id = cim.txncodemstr_id            AND cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id            AND pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID            AND v.VISIT_ID = pa.VISIT_ID            AND pt.PATIENT_ID = v.PATIENT_ID            AND p.PERSON_ID = pt.PERSON_ID            AND sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID            AND wm.WARDMSTR_ID(+) = v.WARDMSTR_ID            AND pat.CHARGE_STATUS = 'CTTNOR'            AND bl.entered_by = um.usermstr_id          GROUP BY bl.bill_id          UNION ALL SELECT sum(pat.claimable_amount) TOTALDISC          FROM bill bl,               patientaccounttxn pat,               txncodemstr tcm,               chargeitemmstr cim,               itemsubcategorymstr iscm,               charge cg,               patientaccount pa,               visit v,               patient pt,               person p ,               SUBSPECIALTYMSTR sspm,               WARDMSTR wm,               careprovider cp,               person d,               usermstr um          WHERE bl.bill_id =  ?            AND cg.ordered_careprovider_id = cp.careprovider_id (+)            AND cp.person_id = d.person_id (+)            AND bl.bill_id = pat.bill_id            AND pat.PATIENTACCOUNT_ID <>bl.PATIENTACCOUNT_ID            AND pat.patientaccounttxn_id = cg.patientaccounttxn_id            AND pat.txncodemstr_id = tcm.txncodemstr_id            AND tcm.txncodemstr_id = cim.txncodemstr_id            AND cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id            AND pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID            AND v.VISIT_ID = pa.VISIT_ID            AND pt.PATIENT_ID = v.PATIENT_ID            AND p.PERSON_ID = pt.PERSON_ID            AND sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID            AND wm.WARDMSTR_ID(+) = v.WARDMSTR_ID            AND pat.CHARGE_STATUS = 'CTTNOR'            AND bl.entered_by = um.usermstr_id          GROUP BY bl.bill_id) BB ) TOTALDISC , v.ADMISSION_DATETIME, v.DISCHARGE_DATETIME , decode(TRUNC(NVL(v.DISCHARGE_DATETIME,bl.BILL_DATETIME)) - trunc(v.ADMISSION_DATETIME),0,1,TRUNC(NVL(v.DISCHARGE_DATETIME,bl.BILL_DATETIME)) - trunc(v.ADMISSION_DATETIME)) datecnt ,      (SELECT decode(vdd.VISIT_DIAGNOSIS_DESC, NULL, dn.DIAGNOSIS_DESC, dn.DIAGNOSIS_DESC||vdd.VISIT_DIAGNOSISMSTR_ID)       FROM VISITDIAGNOSIS vd,            VISITDIAGNOSISDETAIL vdd,            DIAGNOSISMSTR dn       WHERE vd.VISIT_ID = v.VISIT_ID         AND vdd.VISITDIAGNOSIS_ID = vd.VISITDIAGNOSIS_ID         AND dn.DIAGNOSISMSTR_ID(+) = vdd.VISIT_DIAGNOSISMSTR_ID         AND vd.DIAGNOSIS_TYPE = 'DGT3'         AND vdd.DIAGNOSIS_STATUS = 'DGSM'         AND rownum = 1) DIAGNOSIS_DESC    FROM bill bl,         patientaccounttxn pat,         txncodemstr tcm,         chargeitemmstr cim,         itemsubcategorymstr iscm,         charge cg,         patientaccount pa,         visit v,         patient pt,         person p ,         SUBSPECIALTYMSTR sspm,         WARDMSTR wm,         careprovider cp,         person d,         usermstr um    WHERE bl.bill_id =  ?      AND cg.ordered_careprovider_id = cp.careprovider_id (+)      AND cp.person_id = d.person_id (+)      AND bl.bill_id = pat.bill_id      AND pat.PATIENTACCOUNT_ID =bl.PATIENTACCOUNT_ID      AND pat.patientaccounttxn_id = cg.patientaccounttxn_id      AND pat.txncodemstr_id = tcm.txncodemstr_id      AND tcm.txncodemstr_id = cim.txncodemstr_id      AND cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id      AND pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID      AND v.VISIT_ID = pa.VISIT_ID      AND pt.PATIENT_ID = v.PATIENT_ID      AND p.PERSON_ID = pt.PERSON_ID      AND sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID      AND wm.WARDMSTR_ID(+) = v.WARDMSTR_ID      AND pat.CHARGE_STATUS = 'CTTNOR'      AND bl.entered_by = um.usermstr_id    GROUP BY p.PERSON_NAME ,             pt.PATIENT_ID ,             sspm.SUBSPECIALTY_DESC ,             wm.WARD_DESC ,             v.VISIT_ID ,             d.person_name ,             v.VISIT_NO ,             iscm.ip_receipt_item_cat ,             tcm.txn_code ,             pat.TXN_DESC ,             cg.qty_uom ,             base_unit_price ,             v.ADMISSION_DATETIME ,             v.DISCHARGE_DATETIME ,             bl.bill_datetime ,             cg.ordered_careprovider_id ,             pat.txn_datetime                               ,                             bl.bill_id ,                                       um.user_name ,                                       pat.remarks    UNION ALL SELECT p.PERSON_NAME ,                     pgpmi.fxGetMRN(pt.PATIENT_ID, 'MRTIP') MRTIP ,                     sspm.SUBSPECIALTY_DESC ,                     wm.WARD_DESC ,                     pgpmi.fxGetFinancialClassNS(v.VISIT_ID) PATIENT_CLASS ,                     v.VISIT_NO ,                     'BABY COST' iscm_cat ,                                 tcm.txn_code,                                 UPPER(pat.TXN_DESC) AS TXN_DESC ,                                 cg.ordered_careprovider_id ,                                 d.person_name DOCTOR_NAME ,                                 pat.txn_datetime                                                   ,                                                 bl.bill_id   ,um.user_name ,pat.remarks ,PGCOMMON.fxGetCodeDesc(cg.qty_uom) qty_uom,cg.base_unit_price ,sum(cg.qty) qty ,sum(pat.txn_amount) txn_amount,sum(pat.claimable_amount) claimable_amount, sum(pat.deposit_txn_amount) deposit_txn_amount,bl.bill_datetime ,      (SELECT sum(BB.TOTALDISC)       FROM         (SELECT sum(pat.claimable_amount) TOTALDISC          FROM bill bl,               patientaccounttxn pat,               txncodemstr tcm,               chargeitemmstr cim,               itemsubcategorymstr iscm,               charge cg,               patientaccount pa,               visit v,               patient pt,               person p ,               SUBSPECIALTYMSTR sspm,               WARDMSTR wm,               careprovider cp,               person d,               usermstr um          WHERE bl.bill_id =  ?            AND cg.ordered_careprovider_id = cp.careprovider_id (+)            AND cp.person_id = d.person_id (+)            AND bl.bill_id = pat.bill_id            AND pat.PATIENTACCOUNT_ID =bl.PATIENTACCOUNT_ID            AND pat.patientaccounttxn_id = cg.patientaccounttxn_id            AND pat.txncodemstr_id = tcm.txncodemstr_id            AND tcm.txncodemstr_id = cim.txncodemstr_id            AND cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id            AND pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID            AND v.VISIT_ID = pa.VISIT_ID            AND pt.PATIENT_ID = v.PATIENT_ID            AND p.PERSON_ID = pt.PERSON_ID            AND sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID            AND wm.WARDMSTR_ID(+) = v.WARDMSTR_ID            AND pat.CHARGE_STATUS = 'CTTNOR'            AND bl.entered_by = um.usermstr_id          GROUP BY bl.bill_id          UNION ALL SELECT sum(pat.claimable_amount) TOTALDISC          FROM bill bl,               patientaccounttxn pat,               txncodemstr tcm,               chargeitemmstr cim,               itemsubcategorymstr iscm,               charge cg,               patientaccount pa,               visit v,               patient pt,               person p ,               SUBSPECIALTYMSTR sspm,               WARDMSTR wm,               careprovider cp,               person d,               usermstr um          WHERE bl.bill_id =  ?            AND cg.ordered_careprovider_id = cp.careprovider_id (+)            AND cp.person_id = d.person_id (+)            AND bl.bill_id = pat.bill_id            AND pat.PATIENTACCOUNT_ID <>bl.PATIENTACCOUNT_ID            AND pat.patientaccounttxn_id = cg.patientaccounttxn_id            AND pat.txncodemstr_id = tcm.txncodemstr_id            AND tcm.txncodemstr_id = cim.txncodemstr_id            AND cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id            AND pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID            AND v.VISIT_ID = pa.VISIT_ID            AND pt.PATIENT_ID = v.PATIENT_ID            AND p.PERSON_ID = pt.PERSON_ID            AND sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID            AND wm.WARDMSTR_ID(+) = v.WARDMSTR_ID            AND pat.CHARGE_STATUS = 'CTTNOR'            AND bl.entered_by = um.usermstr_id          GROUP BY bl.bill_id) BB ) TOTALDISC , v.ADMISSION_DATETIME, v.DISCHARGE_DATETIME , decode(TRUNC(NVL(v.DISCHARGE_DATETIME,bl.BILL_DATETIME)) - trunc(v.ADMISSION_DATETIME),0,1,TRUNC(NVL(v.DISCHARGE_DATETIME,bl.BILL_DATETIME)) - trunc(v.ADMISSION_DATETIME)) datecnt ,      (SELECT decode(vdd.VISIT_DIAGNOSIS_DESC, NULL, dn.DIAGNOSIS_DESC, dn.DIAGNOSIS_DESC||vdd.VISIT_DIAGNOSISMSTR_ID)       FROM VISITDIAGNOSIS vd,            VISITDIAGNOSISDETAIL vdd,            DIAGNOSISMSTR dn       WHERE vd.VISIT_ID = v.VISIT_ID         AND vdd.VISITDIAGNOSIS_ID = vd.VISITDIAGNOSIS_ID         AND dn.DIAGNOSISMSTR_ID(+) = vdd.VISIT_DIAGNOSISMSTR_ID         AND vd.DIAGNOSIS_TYPE = 'DGT3'         AND vdd.DIAGNOSIS_STATUS = 'DGSM'         AND rownum = 1) DIAGNOSIS_DESC    FROM bill bl,         patientaccounttxn pat,         txncodemstr tcm,         chargeitemmstr cim,         itemsubcategorymstr iscm,         charge cg,         patientaccount pa,         visit v,         patient pt,         person p ,         SUBSPECIALTYMSTR sspm,         WARDMSTR wm,         careprovider cp,         person d,         usermstr um    WHERE bl.bill_id =  ?      AND cg.ordered_careprovider_id = cp.careprovider_id (+)      AND cp.person_id = d.person_id (+)      AND bl.bill_id = pat.bill_id      AND pat.PATIENTACCOUNT_ID <>bl.PATIENTACCOUNT_ID      AND pat.patientaccounttxn_id = cg.patientaccounttxn_id      AND pat.txncodemstr_id = tcm.txncodemstr_id      AND tcm.txncodemstr_id = cim.txncodemstr_id      AND cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id      AND pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID      AND v.VISIT_ID = pa.VISIT_ID      AND pt.PATIENT_ID = v.PATIENT_ID      AND p.PERSON_ID = pt.PERSON_ID      AND sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID      AND wm.WARDMSTR_ID(+) = v.WARDMSTR_ID      AND pat.CHARGE_STATUS = 'CTTNOR'      AND bl.entered_by = um.usermstr_id    GROUP BY p.PERSON_NAME ,             pt.PATIENT_ID ,             sspm.SUBSPECIALTY_DESC ,             wm.WARD_DESC ,             v.VISIT_ID ,             d.person_name ,             v.VISIT_NO ,             iscm.ip_receipt_item_cat ,             tcm.txn_code ,             pat.TXN_DESC ,             cg.qty_uom ,             base_unit_price ,             v.ADMISSION_DATETIME ,             v.DISCHARGE_DATETIME ,             bl.bill_datetime ,             cg.ordered_careprovider_id ,             pat.txn_datetime                               ,                             bl.bill_id ,                             um.user_name ,                             pat.remarks) AA ORDER BY AA.iscm_cat,          AA.txn_datetime";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, billing_id);
			ps.setString(2, billing_id);
			ps.setString(3, billing_id);
			ps.setString(4, billing_id);
			ps.setString(5, billing_id);
			ps.setString(6, billing_id);
			rs = ps.executeQuery();

			while (rs.next()) {
				InpatientBilling dl = new InpatientBilling();
				dl.setMrtip(rs.getString("MRTIP"));
				dl.setSubspecialty_desc(rs.getString("SUBSPECIALTY_DESC"));
				dl.setWard_desc(rs.getString("WARD_DESC"));
				dl.setPatient_class(rs.getString("PATIENT_CLASS"));
				dl.setVisit_no(rs.getString("VISIT_NO"));
				dl.setIscm_cat(rs.getString("iscm_cat"));
				dl.setTxn_code(rs.getString("iscm_cat"));
				dl.setTxn_desc(rs.getString("TXN_DESC"));
				dl.setOrdered_careprovider_id(rs.getString("ORDERED_CAREPROVIDER_ID"));
				dl.setDoctor_name(rs.getString("DOCTOR_NAME"));
				dl.setQty_uom(rs.getString("qty_uom"));
				dl.setBase_nit_price(rs.getString("base_unit_price"));
				dl.setQty(rs.getString("qty"));
				dl.setTxn_amount(rs.getString("txn_amount"));
				dl.setClaimable_amount(rs.getString("claimable_amount"));
				dl.setDeposit_txn_amount(rs.getString("deposit_txn_amount"));
				dl.setBill_datetime(rs.getString("bill_datetime"));
				dl.setTotaldisc(rs.getString("TOTALDISC"));
				dl.setAdmission_datetime(rs.getString("ADMISSION_DATETIME"));
				dl.setDischarge_datetime(rs.getString("DISCHARGE_DATETIME"));
				dl.setDatecnt(rs.getString("datecnt"));
				dl.setDiagnosis_desc(rs.getString("DIAGNOSIS_DESC"));
				dl.setTxn_datetime(rs.getString("TXN_DATETIME"));
				dl.setBill_id(rs.getString("BILL_ID"));
				dl.setRemarks(rs.getString("remarks"));
				
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
	}
	
	public static Object top_operation_doctors(String startdate, String enddate){
		List<TopOperation> data=new ArrayList<TopOperation>();
        Connection connection=DbConnection.getPooledConnection();
        if(connection==null)return null;
        ResultSet rs=null;
  
        PreparedStatement ps=null;
        
        String sql="SELECT RM.RESOURCE_NAME,RM.Careprovider_id,COUNT(RM.CAREPROVIDER_ID) AS JLH_OT,OTRD.SELF_DEFINE_DESC FROM RESOURCEMSTR RM "+
			"INNER JOIN OTREQUESTAPPROVAL OTRA ON OTRA.Request_Careprovider_Id=RM.careprovider_id "+
			"INNER JOIN OTREQUESTITEMDETAIL OTRD ON OTRD.OTREQUESTAPPROVAL_ID=OTRA.OTREQUESTAPPROVAL_ID "+
			"WHERE OTRA.REQUEST_STATUS != 'OES9' AND OTRA.DEFUNCT_IND='N' "+
			"AND OTRA.REQUEST_OPERATION_DATETIME >= TO_DATE (?,'YYYY-MM-DD') AND OTRA.REQUEST_OPERATION_DATETIME <= TO_DATE (?,'YYYY-MM-DD')  "+
			" AND RM.DEFUNCT_IND='N' "+
			" AND OTRD.DEFUNCT_IND = 'N' "+
			"GROUP BY RM.RESOURCE_NAME, RM.CAREPROVIDER_ID, OTRD.SELF_DEFINE_DESC "+
			"order by jlh_ot desc ";
        
        try{
          
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, startdate);
			ps.setString(2, enddate);
			rs = ps.executeQuery();

			while (rs.next()) {
				TopOperation dl = new TopOperation();
				dl.setResource_name(rs.getString("RESOURCE_NAME"));
				dl.setCareprovider_id(rs.getString("Careprovider_id"));
				dl.setJlh_ot(rs.getString("JLH_OT"));
				dl.setSelf_define_desc(rs.getString("SELF_DEFINE_DESC"));
				
				data.add(dl);
			}
            
        }
        catch(Exception e)
        {
            e.fillInStackTrace();
            System.out.println(e.getMessage());
        }
        finally {
             if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
             if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
             if (connection!=null) try { connection.close();} catch (Exception ignore){}
           }
        return data;
	}
}
