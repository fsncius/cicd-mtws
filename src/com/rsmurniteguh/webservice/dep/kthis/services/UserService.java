package com.rsmurniteguh.webservice.dep.kthis.services;

import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.kthis.view.CodeDescView;
import com.rsmurniteguh.webservice.dep.kthis.view.UserLoginView;
import com.rsmurniteguh.webservice.dep.util.CommonUtil;
import com.rsmurniteguh.webservice.dep.util.EncryptPatient;
import com.rsmurniteguh.webservice.dep.util.EncryptUtil;
import com.rsmurniteguh.webservice.dep.util.Encryptor;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.ResponseString;

public abstract class UserService {
	public static UserLoginView getLoginDetails(String username, String pass) {

		UserLoginView view = new UserLoginView();
		ResultSet haspas = null;
		ResultSet locs=null;
		ResultSet rolResult=null;
		Statement carep=null;
		Connection connection = DbConnection.getPooledConnection();
		try {
			String query = "select USERMSTR_ID, PASSWORD_EXPIRY_DATE, "
					+ "USER_CODE, USER_NAME "
					+ "from usermstr "
					+ "where user_code = '"+username+"' "
							+ " and password = '"+pass+"' ";
			
			if(connection == null)return null;
			carep = connection.createStatement();
			haspas = carep.executeQuery(query);
			
			if(haspas.next()){
				view.setUsermstrId(haspas.getLong("USERMSTR_ID"));
				view.setPasswordExpiryDate(haspas.getDate("PASSWORD_EXPIRY_DATE"));
				view.setUserCode(haspas.getString("USER_CODE"));
				view.setUserName(haspas.getString("USER_NAME"));
			}
			
			carep.close();
			if (view.getUserCode() != null) {
				query = "select lm.LOCATIONMSTR_ID, lm.LOCATION_NAME " 
						+ "from USER_LOCATION ul "
						+ "inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = ul.LOCATIONMSTR_ID "
						+ "where USERMSTR_ID = :userid ";

				locs = carep.executeQuery(query);
				List<CodeDescView> locsview = new ArrayList<CodeDescView>();
				while (locs.next()) {
					CodeDescView location = new CodeDescView();
					location.setId(locs.getBigDecimal("LOCATIONMSTR_ID"));
					location.setCode(locs.getString("LOCATION_NAME"));
					locsview.add(location);
				}

//				stmt.close();
				locs.close();

				query = "select rm.ROLEMSTR_ID, rm.ROLE_NAME " + "from USER_ROLE ur "
						+ "inner join rolemstr rm on rm.ROLEMSTR_ID = ur.ROLEMSTR_ID "
						+ "WHERE ur.USERMSTR_ID = :userid ";

				rolResult = carep.executeQuery(query);
				List<CodeDescView> roleview = new ArrayList<CodeDescView>();
				while (rolResult.next()) {
					CodeDescView v = new CodeDescView();
					v.setId(rolResult.getBigDecimal("ROLEMSTR_ID"));
					v.setCode(rolResult.getString("ROLE_NAME"));
					roleview.add(v);
				}

				rolResult.close();
				carep.close();

				view.setLocations(locsview);
				view.setRoles(roleview);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (locs!=null) try  { locs.close(); } catch (Exception ignore){}
		     if (rolResult!=null) try  { rolResult.close(); } catch (Exception ignore){}
		     if (carep!=null) try  { carep.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		
		return view;
	}
	
	public static ResponseStatus getChangePasswordKthis(String usermstrId, String password, String new_password) {
		ResponseStatus data = new ResponseStatus();
		Connection Connection = null;
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null, rs2 = null;
		
		String pass =  EncryptUtil.getInstance().nonReversibleEncode(Encryptor.getInstance().decrypt(password));
		try {
			Connection = DbConnection.getPooledConnection();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			String selectQuery = " select um.USERMSTR_ID, um.PASSWORD_EXPIRY_DATE, "
					+ "um.USER_CODE, um.USER_NAME "
					+ ", cp.CAREPROVIDER_TYPE, PGCOMMON.FXGETCODEDESC(cp.CAREPROVIDER_TYPE) as TYPE_DESC,  ep.DESIGNATION "
					+ "from usermstr um "
					+ "inner join USERPROFILE up on up.USERMSTR_ID = um.USERMSTR_ID "
					+ "left outer join CAREPROVIDER cp on cp.PERSON_ID = up.PERSON_ID and cp.defunct_ind = 'N' "
					+ "left outer join employee ep on ep.PERSON_ID = up.PERSON_ID and ep.defunct_ind = 'N'"
					+ " where um.USERMSTR_ID = ?  and password = ?  and um.defunct_ind = 'N' "
					+ " and up.defunct_ind = 'N' ";
			
			ps = Connection.prepareStatement(selectQuery);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, usermstrId);
			ps.setString(2, pass);
			rs = ps.executeQuery();
			
			if(rs.next()){
				String Ids = rs.getString("user_code");
				String encrypt = EncryptUtil.getInstance().nonReversibleEncode(Encryptor.getInstance().decrypt(new_password));
				String updatequery = "UPDATE usermstr SET PASSWORD = ? WHERE user_code = ? ";
				ps2 = Connection.prepareStatement(updatequery);
				ps2 = CommonUtil.initialStatement(ps2);
				ps2.setString(1, encrypt);
				ps2.setString(2, Ids);
				ps2.executeUpdate();
				data.setSuccess(true);
				data.setMessage("berhasil");
			}
		}
		catch (SQLException e){
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
		}
		return data;
	}
	
	public static ResponseString getChangePasswordPatient(String usermstrId, String password, String new_password) {
		ResponseString data = new ResponseString();
		Connection Connection = null;
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null, rs2 = null;
		
		String pass =  EncryptPatient.getInstance().encryptOnlineAppointment(Encryptor.getInstance().decrypt(password));

		try {
			Connection = DbConnection.getBpjsOnlineInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			String selectQuery = "SELECT user_id FROM tb_user WHERE user_id = ? AND password = ?";
			
			ps = Connection.prepareStatement(selectQuery);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, usermstrId);
			ps.setString(2, pass);
			rs = ps.executeQuery();
			
			if(rs.next()){
				String Ids = rs.getString("user_id");
				String encrypt = EncryptPatient.getInstance().encryptOnlineAppointment(Encryptor.getInstance().decrypt(new_password));
				String updatequery = "UPDATE tb_user SET password = ? WHERE user_id = ? ";
				ps2 = Connection.prepareStatement(updatequery);
				ps2 = CommonUtil.initialStatement(ps2);
				ps2.setString(1, encrypt);
				ps2.setString(2, Ids);
				data.setResponse("Ganti password berhasil");
				rs2 = ps2.executeQuery();
			}
			else{
				data.setResponse("Password salah");
			}
		}
		catch (SQLException e){
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
		}
		return data;
	}
	
	public static ResponseStatus getChangePasswordHris(String idkaryawan, String password, String new_password) {

		ResponseStatus data = new ResponseStatus();
		Connection Connection = null;
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null, rs2 = null;
		
		String pass =  EncryptUtil.getInstance().StringtoMD5(Encryptor.getInstance().decrypt(password));
		try {
			Connection = DbConnection.getHrisInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			String selectQuery = " SELECT Username FROM TblUsers WHERE IDKaryawan = ? AND Password = ? ";
			
			ps = Connection.prepareStatement(selectQuery);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, idkaryawan);
			ps.setString(2, pass);
			rs = ps.executeQuery();
		
			
			if(rs.next()){
				String encrypt = EncryptUtil.getInstance().StringtoMD5(Encryptor.getInstance().decrypt(new_password));
				String updatequery = "UPDATE TblUsers SET Password = ? WHERE IDKaryawan = ? ";
				ps2 = Connection.prepareStatement(updatequery);
				ps2 = CommonUtil.initialStatement(ps2);
				ps2.setString(1, encrypt);
				ps2.setString(2, idkaryawan);
				ps2.executeUpdate();
				data.setSuccess(true);
				data.setMessage("berhasil");
			}
		}
		catch (SQLException e){
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
		}
		return data;
	}

}