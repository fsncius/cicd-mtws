package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.json.JSONObject;

import com.google.api.core.ApiFuture;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.mysql.jdbc.Statement;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.hris.CreateCuti;
import com.rsmurniteguh.webservice.dep.all.model.hris.CreateCutiView;
import com.rsmurniteguh.webservice.dep.all.model.hris.CutiAtasan;
import com.rsmurniteguh.webservice.dep.all.model.hris.EmployeAttendance;
import com.rsmurniteguh.webservice.dep.all.model.hris.EmployeDetail;
import com.rsmurniteguh.webservice.dep.all.model.hris.Karyawan;
import com.rsmurniteguh.webservice.dep.all.model.hris.KaryawanBawahan;
import com.rsmurniteguh.webservice.dep.all.model.hris.PermohonanCuti;
import com.rsmurniteguh.webservice.dep.all.model.hris.PermohonanIzin;
import com.rsmurniteguh.webservice.dep.all.model.hris.RequestApproval;
import com.rsmurniteguh.webservice.dep.all.model.hris.RequestCreateIzin;
import com.rsmurniteguh.webservice.dep.all.model.hris.RequestCuti;
import com.rsmurniteguh.webservice.dep.all.model.hris.RequestCutiTask;
import com.rsmurniteguh.webservice.dep.all.model.hris.RequestIzin;
import com.rsmurniteguh.webservice.dep.all.model.hris.SaldoCuti;
import com.rsmurniteguh.webservice.dep.all.model.hris.SerahTugas;
import com.rsmurniteguh.webservice.dep.all.model.hris.SimpleKaryawan;
import com.rsmurniteguh.webservice.dep.all.model.mobile.Berita;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.kthis.view.CodeDescView;
import com.rsmurniteguh.webservice.dep.kthis.view.UserLoginView;
import com.rsmurniteguh.webservice.dep.util.CommonUtil;
import com.rsmurniteguh.webservice.dep.util.EncryptUtil;
import com.rsmurniteguh.webservice.dep.util.Encryptor;

public class HrisService {
	public static Object get_employe_detail(String id_karyawan) {
		List<EmployeDetail> list = new ArrayList<EmployeDetail>();
		
		try {
			String sql = "SELECT  * FROM ViewKaryawanDetail "
					+ " WHERE IDKaryawan = ? ";
			
			
			List res = DbConnection.executeReader(DbConnection.HRIS, sql, new Object[] {id_karyawan});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));

				EmployeDetail dl = new EmployeDetail();
			
				dl.setIdkaryawan(String.valueOf(rs.get("idkaryawan")));
				dl.setFingerprintid(String.valueOf(rs.get("fingerprintid")));
				dl.setKodebagian(String.valueOf(rs.get("kodebagian")));
				dl.setKodejabatan(String.valueOf(rs.get("kodejabatan")));
				dl.setKodecabang(String.valueOf(rs.get("kodecabang")));
				dl.setKodecabangpembantu(String.valueOf(rs.get("kodecabangpembantu")));
				dl.setNoktp(String.valueOf(rs.get("noktp")));
				dl.setNosim(String.valueOf(rs.get("nosim")));
				dl.setNpwp(String.valueOf(rs.get("npwp")));
				dl.setNama(String.valueOf(rs.get("nama")));
				dl.setTempatlahir(String.valueOf(rs.get("tempatlahir")));
				dl.setTgllahir(String.valueOf(rs.get("tgllahir")));
				dl.setEmail(String.valueOf(rs.get("email")));
				dl.setAlamatpermanent(String.valueOf(rs.get("alamatpermanent")));
				dl.setAlamatsementara(String.valueOf(rs.get("alamatsementara")));
				dl.setRtrw(String.valueOf(rs.get("rtrw")));
				dl.setKel(String.valueOf(rs.get("kel")));
				dl.setKec(String.valueOf(rs.get("kec")));
				dl.setAgama(String.valueOf(rs.get("agama")));
				dl.setStatusnikah(String.valueOf(rs.get("statusnikah")));
				dl.setWn(String.valueOf(rs.get("wn")));
				dl.setKelamin(String.valueOf(rs.get("kelamin")));
				dl.setSuku(String.valueOf(rs.get("suku")));
				dl.setHp(String.valueOf(rs.get("hp")));
				dl.setTelepon(String.valueOf(rs.get("telepon")));
				dl.setUkuranbaju(String.valueOf(rs.get("ukuranbaju")));
				dl.setUkurancelana(String.valueOf(rs.get("ukurancelana")));
				dl.setUkuransepatu(String.valueOf(rs.get("ukuransepatu")));
				dl.setTglbergabung(String.valueOf(rs.get("tglbergabung")));
				dl.setBank(String.valueOf(rs.get("bank")));
				dl.setNorek(String.valueOf(rs.get("norek")));
				dl.setRekatasnama(String.valueOf(rs.get("rekatasnama")));
				dl.setHpkerabat(String.valueOf(rs.get("hpkerabat")));
				dl.setStatusaktif(String.valueOf(rs.get("statusaktif")));
				dl.setStatuskerja(String.valueOf(rs.get("statuskerja")));
				dl.setMulaikontrak(String.valueOf(rs.get("mulaikontrak")));
				dl.setAkhirkontrak(String.valueOf(rs.get("akhirkontrak")));
				dl.setFoto(String.valueOf(rs.get("foto")));
				dl.setTglinput(String.valueOf(rs.get("tglinput")));
				dl.setTglubah(String.valueOf(rs.get("tglubah")));
				dl.setUseraktif(String.valueOf(rs.get("useraktif")));
				dl.setIduser(String.valueOf(rs.get("iduser")));
				dl.setDlt(String.valueOf(rs.get("dlt")));
				dl.setKodemesin(String.valueOf(rs.get("kodemesin")));
				dl.setSisa_cuti(String.valueOf(rs.get("sisa")));
				//dl.setJammasuk(rs.getString("jammasuk"));
				//dl.setJamkeluar(rs.getString("jamkeluar"));
				
				//get saldo emoney
				dl.setSaldo_emoney(GetEmoney(String.valueOf(rs.get("idkaryawan"))));
				
				SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
		        String today = df.format(cal.getTime());
		        cal.add(Calendar.DAY_OF_MONTH, -1);
		        String yesterday = df.format(cal.getTime());
		        
				
				List<EmployeAttendance> attendance = get_employe_attendance(String.valueOf(rs.get("fingerprintid")), 
						String.valueOf(rs.get("kodemesin")), 
						yesterday, today);
				
				for (EmployeAttendance employeAttendance : attendance) {
					if(employeAttendance.getPersonalcalendardate().equals(today)){
						dl.setJammasuk(employeAttendance.getJammasuk());
					} else if (employeAttendance.getPersonalcalendardate().equals(yesterday)) {
						dl.setJamkeluar(employeAttendance.getJamkeluar());
					}
				}
				list.add(dl);

			}
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(), id_karyawan);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(), id_karyawan);
		}
		finally {
		}
		return list;
	}

	public static List<EmployeAttendance> get_employe_attendance(String finger_id, String kodemesin, String startdate, String enddate) {
		List<EmployeAttendance> list = new ArrayList<EmployeAttendance>();
		
		try {
			
			String selectQuery = "CekShift ?,?,?,? ";
			
			List res = DbConnection.executeReader(DbConnection.HRIS, selectQuery, new Object[] {finger_id,startdate,enddate,kodemesin});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				
				EmployeAttendance dl = new EmployeAttendance();
				dl.setPersonalcalendardate(String.valueOf(rs.get("personalcalendardate")));
				dl.setFingerprintid(String.valueOf(rs.get("fingerprintid")));
				dl.setHari(String.valueOf(rs.get("hari")));
				dl.setJammasuk(String.valueOf(rs.get("jammasuk")));
				dl.setJamkeluar(String.valueOf(rs.get("jamkeluar")));
				dl.setCatatan(String.valueOf(rs.get("catatan")));
				dl.setPersonalcalendarstatus(String.valueOf(rs.get("personalcalendarstatus")));
				dl.setPersonalcalendarreason(String.valueOf(rs.get("personalcalendarreason")));
				dl.setJammulai(String.valueOf(rs.get("jammulai")));
				dl.setJampulang(String.valueOf(rs.get("jampulang")));
				dl.setTerlambat(String.valueOf(rs.get("terlambat")));
				dl.setCepatpulang(String.valueOf(rs.get("cepatpulang")));
				dl.setJamefektif(String.valueOf(rs.get("jamefektif")));
				dl.setLembur(String.valueOf(rs.get("lembur")));
				dl.setShiftcode(String.valueOf(rs.get("shiftcode")));
				dl.setNonWorkingDay(String.valueOf(rs.get("NonWorkingDay")));
				
//				dl.setPersonalcalendarnotes(rs.getString("personalcalendarnotes"));
//				dl.setPersonalcalendarid(rs.getString("personalcalendarid"));
//				dl.setKodemesin(rs.getString("kodemesin"));
//				dl.setPatterncode(rs.getString("patterncode"));
				
				list.add(dl);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),"finger_id :"+finger_id);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),"finger_id :"+finger_id);
		}
		finally {
		}
		return list;
	}

	public static Object get_izin_data(String id_karyawan, String startdate, String enddate) {
		List<PermohonanIzin> list = new ArrayList<PermohonanIzin>();

		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        String tglend = sdf.format(cal.getTime());
        cal.add(Calendar.DATE, -2);
        String tglstart = sdf.format(cal.getTime());
        try {
        	Date d_startdate = sdf.parse(startdate);
        	Date d_enddate = sdf.parse(enddate);
        	tglstart = sdf.format(d_startdate);
        	tglend = sdf.format(d_enddate);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		
		try {
			String selectQuery = "SELECT  * FROM ViewPermohonanIzin "
					+ " WHERE IDKaryawan = ? AND dlt = 0 AND TglIzinKeluar  BETWEEN ? AND ? "
					+ " ORDER BY TglIzinKeluar DESC ";
			
			
			List res = DbConnection.executeReader(DbConnection.HRIS, selectQuery, new Object[] {id_karyawan,tglstart,tglend});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				
				PermohonanIzin dl = new PermohonanIzin();
				dl.setTipeizin(String.valueOf(rs.get("tipeizin")));
				dl.setKodeizin(String.valueOf(rs.get("kodeizin")));
				dl.setTglpermohonan(String.valueOf(rs.get("tglpermohonan")));
				dl.setTglizinkeluar(String.valueOf(rs.get("tglizinkeluar")));
				dl.setTglizinmasuk(String.valueOf(rs.get("tglizinmasuk")));
				dl.setJamizinmasuk(String.valueOf(rs.get("jamizinmasuk")));
				dl.setJamizinkeluar(String.valueOf(rs.get("jamizinkeluar")));
				dl.setAlasan(String.valueOf(rs.get("alasan")));
				dl.setIdkaryawan(String.valueOf(rs.get("idkaryawan")));
				dl.setJlhpax(String.valueOf(rs.get("jlhpax")));
				dl.setProgressing(String.valueOf(rs.get("progressing")));
				dl.setApproved(String.valueOf(rs.get("approved")));
				dl.setRejected(String.valueOf(rs.get("rejected")));
				dl.setStatusall(String.valueOf(rs.get("statusall")));
				dl.setDlt(String.valueOf(rs.get("dlt")));
				dl.setKendaraan(String.valueOf(rs.get("kendaraan")));
				dl.setSupir(String.valueOf(rs.get("supir")));
				dl.setDokumen1(String.valueOf(rs.get("dokumen1")));
				dl.setDokumen2(String.valueOf(rs.get("dokumen2")));
				dl.setDokumen3(String.valueOf(rs.get("dokumen3")));
				dl.setKodeapprove(String.valueOf(rs.get("kodeapprove")));
				dl.setLamaexpired(String.valueOf(rs.get("lamaexpired")));
				dl.setNamakaryawan(String.valueOf(rs.get("namakaryawan")));
				dl.setKeterangan(String.valueOf(rs.get("keterangan")));
				dl.setJenispotong(String.valueOf(rs.get("jenispotong")));
				dl.setApprovalby(String.valueOf(rs.get("approvalby")));
				dl.setLokasi(String.valueOf(rs.get("lokasi")));
				dl.setKodemesin(String.valueOf(rs.get("kodemesin")));
				list.add(dl);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),id_karyawan);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),id_karyawan);
		}
		finally {
		}
		return list;
	}

	public static Object get_cuti_data(String id_karyawan, String startdate, String enddate) {
		List<PermohonanCuti> list = new ArrayList<PermohonanCuti>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        String tglend = sdf.format(cal.getTime());
        cal.add(Calendar.DATE, -2);
        String tglstart = sdf.format(cal.getTime());
        try {
        	Date d_startdate = sdf.parse(startdate);
        	Date d_enddate = sdf.parse(enddate);
        	tglstart = sdf.format(d_startdate);
        	tglend = sdf.format(d_enddate);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		try {
			
			String selectQuery = "SELECT  * FROM ViewPermohonanCuti "
					+ " WHERE IDKaryawan = ? AND dlt = 0 AND tglmulai BETWEEN ? AND ? "
					+ " ORDER BY tglmulai DESC";
			
			List res = DbConnection.executeReader(DbConnection.HRIS, selectQuery, new Object[] {id_karyawan,tglstart,tglend});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				
				PermohonanCuti dl = new PermohonanCuti();
				dl.setJeniscuti(String.valueOf(rs.get("jeniscuti")));
				dl.setKodecuti(String.valueOf(rs.get("kodecuti")));
				dl.setTglinput(String.valueOf(rs.get("tglinput")));
				dl.setTglmulai(String.valueOf(rs.get("tglmulai")));
				dl.setTglakhir(String.valueOf(rs.get("tglakhir")));
				dl.setIdkaryawan(String.valueOf(rs.get("idkaryawan")));
				dl.setInfotambahan(String.valueOf(rs.get("infotambahan")));
				dl.setLamacuti(String.valueOf(rs.get("lamacuti")));
				dl.setBonuscuti(String.valueOf(rs.get("bonuscuti")));
				dl.setKeterangan(String.valueOf(rs.get("keterangan")));
				dl.setKodeapprove(String.valueOf(rs.get("kodeapprove")));
				dl.setDlt(String.valueOf(rs.get("dlt")));
				dl.setJlhpax(String.valueOf(rs.get("jlhpax")));
				dl.setProgressing(String.valueOf(rs.get("progressing")));
				dl.setApproved(String.valueOf(rs.get("approved")));
				dl.setRejected(String.valueOf(rs.get("rejected")));
				dl.setStatusall(String.valueOf(rs.get("statusall")));
				dl.setLamaexpired(String.valueOf(rs.get("lamaexpired")));
				dl.setNama(String.valueOf(rs.get("nama")));
				dl.setKodemesin(String.valueOf(rs.get("kodemesin")));
				list.add(dl);
			}
			

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),id_karyawan);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),id_karyawan);
		}
		finally {
		}
		return list;
	}
	
	public static Object get_data_request_izin(String id_karyawan) /*DEPRECATED*/ {
		List<RequestIzin> list = new ArrayList<RequestIzin>();
		
		try {
			String selectQuery = "SELECT  * FROM ViewApprovalRequest "
					+ " WHERE IDRequester = ? ";
			
			
			List res = DbConnection.executeReader(DbConnection.HRIS, selectQuery, new Object[] {id_karyawan});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				
				RequestIzin dl = new RequestIzin();
				dl.setKodeizin(String.valueOf(rs.get("kodeizin")));
				dl.setTipeizin(String.valueOf(rs.get("tipeizin")));
				dl.setIdrequester(String.valueOf(rs.get("idrequester")));
				dl.setTglizinkeluar(String.valueOf(rs.get("tglizinkeluar")));
				dl.setTglizinmasuk(String.valueOf(rs.get("tglizinmasuk")));
				dl.setJamizinkeluar(String.valueOf(rs.get("jamizinkeluar")));
				dl.setJamizinmasuk(String.valueOf(rs.get("jamizinmasuk")));
				dl.setKendaraan(String.valueOf(rs.get("kendaraan")));
				dl.setSupir(String.valueOf(rs.get("supir")));
				dl.setDokumen1(String.valueOf(rs.get("dokumen1")));
				dl.setDokumen2(String.valueOf(rs.get("dokumen2")));
				dl.setDokumen3(String.valueOf(rs.get("dokumen3")));
				dl.setKodeapprove(String.valueOf(rs.get("kodeapprove")));
				dl.setTglpermohonan(String.valueOf(rs.get("tglpermohonan")));
				dl.setDlt(String.valueOf(rs.get("dlt")));
				dl.setAlasanrequester(String.valueOf(rs.get("alasanrequester")));
				dl.setNamarequester(String.valueOf(rs.get("namarequester")));
				dl.setLamaexpired(String.valueOf(rs.get("lamaexpired")));
				dl.setFingerprintid(String.valueOf(rs.get("fingerprintid")));
				dl.setStatusall(String.valueOf(rs.get("statusall")));
				dl.setKodemesin(String.valueOf(rs.get("kodemesin")));
				dl.setFoto(String.valueOf(rs.get("foto")));
				dl.setKodecabang(String.valueOf(rs.get("kodecabang")));
				dl.setJenispotong(String.valueOf(rs.get("jenispotong")));
				dl.setBonuscuti(String.valueOf(rs.get("bonuscuti")));
				dl.setLokasi(String.valueOf(rs.get("lokasi")));
				list.add(dl);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		finally {
		}
		return list;
	}

	public static Object get_data_request_cuti(String id_karyawan) /*DEPRECATED*/ {
		List<RequestCuti> list = new ArrayList<RequestCuti>();
		
		try {
			
			String selectQuery = "SELECT  * FROM ViewApprovalRequestCuti "
					+ " WHERE IDKaryawan = ? ";
			
			List res = DbConnection.executeReader(DbConnection.HRIS, selectQuery, new Object[] {id_karyawan});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				
				RequestCuti dl = new RequestCuti();				
				dl.setKodecuti(String.valueOf(rs.get("kodecuti")));
				dl.setIdkaryawan(String.valueOf(rs.get("idkaryawan")));
				dl.setJeniscuti(String.valueOf(rs.get("jeniscuti")));
				dl.setTglmulai(String.valueOf(rs.get("tglmulai")));
				dl.setTglakhir(String.valueOf(rs.get("tglakhir")));
				dl.setInfotambahan(String.valueOf(rs.get("infotambahan")));
				dl.setKeterangan(String.valueOf(rs.get("keterangan")));
				dl.setLamacuti(String.valueOf(rs.get("lamacuti")));
				dl.setBonuscuti(String.valueOf(rs.get("bonuscuti")));
				dl.setKodeapprove(String.valueOf(rs.get("kodeapprove")));
				dl.setTglinput(String.valueOf(rs.get("tglinput")));
				dl.setTgledit(String.valueOf(rs.get("tgledit")));
				dl.setCreateby(String.valueOf(rs.get("createby")));
				dl.setDlt(String.valueOf(rs.get("dlt")));
				dl.setFingerprintid(String.valueOf(rs.get("fingerprintid")));
				dl.setNama(String.valueOf(rs.get("nama")));
				dl.setLamaexpired(String.valueOf(rs.get("lamaexpired")));
				dl.setStatusall(String.valueOf(rs.get("statusall")));
				dl.setKodemesin(String.valueOf(rs.get("kodemesin")));
				dl.setFoto(String.valueOf(rs.get("foto")));
				dl.setKodecabang(String.valueOf(rs.get("kodecabang")));
				list.add(dl);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		finally {
		}
		return list;
	}

	public static Object get_data_request_task_cuti(String id_karyawan) {
		List<RequestCutiTask> list = new ArrayList<RequestCutiTask>();
		
		try {
			
			String selectQuery = "SELECT  * FROM ViewApprovalRequestCutiTask "
					+ " WHERE IDKaryawan = ? ";
			
			
			List res = DbConnection.executeReader(DbConnection.HRIS, selectQuery, new Object[] {id_karyawan});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				
				RequestCutiTask dl = new RequestCutiTask();			
				dl.setKodeserahtugas(String.valueOf(rs.get("kodeserahtugas")));
				dl.setKodecuti(String.valueOf(rs.get("kodecuti")));
				dl.setIdkaryawan(String.valueOf(rs.get("idkaryawan")));
				dl.setTanggal(String.valueOf(rs.get("tanggal")));
				dl.setTugas(String.valueOf(rs.get("tugas")));
				dl.setLapmasalahpenting(String.valueOf(rs.get("lapmasalahpenting")));
				dl.setTglinput(String.valueOf(rs.get("tglinput")));
				dl.setTglubah(String.valueOf(rs.get("tglubah")));
				dl.setDlt(String.valueOf(rs.get("dlt")));
				dl.setIdrequester(String.valueOf(rs.get("idrequester")));
				dl.setNamarequester(String.valueOf(rs.get("namarequester")));
				dl.setJeniscuti(String.valueOf(rs.get("jeniscuti")));
				dl.setLamaexpired(String.valueOf(rs.get("lamaexpired")));
				dl.setFingerprintid(String.valueOf(rs.get("fingerprintid")));
				dl.setNotifikasi(String.valueOf(rs.get("notifikasi")));
				dl.setHasil(String.valueOf(rs.get("hasil")));
				dl.setFoto(String.valueOf(rs.get("foto")));
				list.add(dl);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),id_karyawan);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),id_karyawan);
		}
		finally {
		}
		return list;
	}
	
	public static CreateCutiView view_create_cuti(String id_karyawan){
		CreateCutiView data = new CreateCutiView();
		
		Connection Connection = null;
		PreparedStatement ps = null, ps2=null, ps3=null, ps4=null, ps5=null;
		ResultSet rs = null, rs2=null, rs3=null, rs4=null, rs5=null;
		
		
		try {
			Connection = DbConnection.getHrisInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			String sql2 = "CekApprove ?,2";
			String sql3 = "SELECT e.KodeDepartement FROM TblKaryawan c "
					+ "INNER JOIN Tbljabatan d ON c.KodeJabatan = d.KodeJabatan "
					+ "INNER JOIN TblDepartement e ON d.KodeDepartement = e.KodeDepartement "
					+ "WHERE c.IDKaryawan = ? ";
			String sql4 = "SELECT c.IDKaryawan, c.Nama, e.KodeHuruf, e.Departement FROM TblKaryawan c "
					+ "INNER JOIN TblJabatan d ON c.KodeJabatan = d.KodeJabatan "
					+ "INNER JOIN TblDepartement e ON d.KodeDepartement = e.KodeDepartement "
					+ "WHERE c.Dlt = 0 AND c.Nama != 'admin' AND d.KodeDepartement = ?";
			String sql5 = "SELECT c.IDKaryawan, c.Nama, e.KodeHuruf, e.Departement, d.Jabatan, d.KodeJabatan "
					+ "FROM TblKaryawan c "
					+ "INNER JOIN Tbljabatan d ON c.KodeJabatan = d.KodeJabatan "
					+ "INNER JOIN TblDepartement e ON d.KodeDepartement = e.KodeDepartement "
					+ "WHERE c.Dlt = 0 AND c.Nama != 'admin' AND d.KodeDepartement = ? AND d.Jabatan IN ('Supervisor','Manager','Direktur','General Manager','Wakil Direktur') "
					+ "ORDER BY c.Nama ";
			String sql = "SELECT  * FROM TblSaldoCuti WHERE IDKaryawan = ? AND StatusUpdate = 0 ";
			//String sql6 = "SELECT d.* FROM ViewHistorycuti d where d.Dlt = 0 AND d.StatusAll = 'Approved' AND YEAR(d.TglMulai) = YEAR(SYSDATETIME()) ORDER BY TglMulai DESC ";
			
					
			ps2 = Connection.prepareStatement(sql2);
			ps2 = CommonUtil.initialStatement(ps2);
			ps2.setString(1, id_karyawan);
			rs2 = ps2.executeQuery();

			if (rs2.next() && rs2.getInt("Hasil") != 0) {
				ps3 = Connection.prepareStatement(sql3);
				ps3 = CommonUtil.initialStatement(ps3);
				ps3.setString(1, id_karyawan);
				rs3 = ps3.executeQuery();

				if (rs3.next()) {
					//karyawan
					ps4 = Connection.prepareStatement(sql4);
					ps4 = CommonUtil.initialStatement(ps4);
					ps4.setString(1, rs3.getString("KodeDepartement"));
					rs4 = ps4.executeQuery();
					
					ArrayList<SimpleKaryawan> hd = new ArrayList<SimpleKaryawan>();
					while(rs4.next()){
						SimpleKaryawan dl = new SimpleKaryawan();
						dl.setIdkaryawan(rs4.getString("IDKaryawan"));
						dl.setNama(rs4.getString("nama"));
						dl.setKodehuruf(rs4.getString("kodehuruf"));
						dl.setDepartment(rs4.getString("Departement"));
						hd.add(dl);
					}

					// karyawan penting
					ps5 = Connection.prepareStatement(sql5);
					ps5 = CommonUtil.initialStatement(ps5);
					ps5.setString(1, rs3.getString("KodeDepartement"));
					rs5 = ps5.executeQuery();

					ArrayList<SimpleKaryawan> hd2 = new ArrayList<SimpleKaryawan>();
					while(rs5.next()){
						SimpleKaryawan dl2 = new SimpleKaryawan();
						dl2.setIdkaryawan(rs5.getString("IDKaryawan"));
						dl2.setNama(rs5.getString("nama"));
						dl2.setKodehuruf(rs5.getString("kodehuruf"));
						dl2.setDepartment(rs5.getString("Departement"));
						dl2.setJabatan(rs5.getString("jabatan"));
						dl2.setKodejabatan(rs5.getString("kodejabatan"));
						hd2.add(dl2);
					}
					
					ps = Connection.prepareStatement(sql);
					ps = CommonUtil.initialStatement(ps);
					ps.setString(1, id_karyawan);
					rs = ps.executeQuery();
					
					
					SaldoCuti dl3 = new SaldoCuti(); 
					if (rs.next()){
						dl3.setIdkaryawan(rs.getString("idkaryawan"));
						dl3.setAwal(rs.getString("awal"));
						dl3.setMasuk(rs.getString("masuk"));
						dl3.setKeluar(rs.getString("keluar"));
						dl3.setSisa(rs.getString("sisa"));
						dl3.setStatusupdate(rs.getString("statusupdate"));
						dl3.setPeriode(rs.getString("periode"));
						dl3.setTglexpired(rs.getString("tglexpired"));
						dl3.setTglexpired_sisatahunlalu(rs.getString("tglexpired_sisatahunlalu"));
					}
					
//					ps6 = Connection.prepareStatement(sql6);
//					ps6.setString(1, id_karyawan);
//					rs6 = ps6.executeQuery();
					
					
//					SaldoCuti dl3 = new SaldoCuti(); 
//					if (rs6.next()){
//						dl3.setIdkaryawan(rs.getString("idkaryawan"));
//						dl3.setAwal(rs.getString("awal"));
//						dl3.setMasuk(rs.getString("masuk"));
//						dl3.setKeluar(rs.getString("keluar"));
//						dl3.setSisa(rs.getString("sisa"));
//						dl3.setStatusupdate(rs.getString("statusupdate"));
//						dl3.setPeriode(rs.getString("periode"));
//						dl3.setTglexpired(rs.getString("tglexpired"));
//						dl3.setTglexpired_sisatahunlalu(rs.getString("tglexpired_sisatahunlalu"));
//					}
					
					
					
					
					data.setKaryawan(hd);
					data.setKaryawanpenting(hd2);
					data.setSaldocuti(dl3);
				}
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),id_karyawan);
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),id_karyawan);
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
			if (ps3 != null) try { ps3.close(); } catch (SQLException e) {}
			if (rs3 != null) try { rs3.close(); } catch (SQLException e) {}
			if (ps4 != null) try { ps4.close(); } catch (SQLException e) {}
			if (rs4 != null) try { rs4.close(); } catch (SQLException e) {}
			if (ps5 != null) try { ps5.close(); } catch (SQLException e) {}
			if (rs5 != null) try { rs5.close(); } catch (SQLException e) {}
		}
		
		return data;
	}
	
	public static ResponseStatus create_cuti(CreateCuti createCuti){
		ResponseStatus data = new ResponseStatus();
		
		Connection Connection = null, connectionMT = null;
		PreparedStatement ps = null, ps2=null, ps3=null, ps4=null, ps5=null, ps6=null, ps7=null, ps8=null, ps9=null;
		ResultSet rs = null, rs2=null, rs3=null, rs4=null, rs5=null, generatedKeys=null, rs6=null, rs7=null, rs8=null, rs9=null;
		
		Connection = DbConnection.getHrisInstance();
		connectionMT = DbConnection.getBpjsOnlineInstance();
		
		final String FMCurl = "https://fcm.googleapis.com/fcm/send";
		final String FMKey = "AAAAH9Z5K-o:APA91bGP1q7L3_JfQvW_Nm-XinKC7syUAPTXy6e4S2sX7tbCcRjB5zPuUT-nhwhfMUEj6X_bySZ5LNONqI-x-jkIrFn-nw_H3jRq5XDrN9AIiSF3BdCPb-y1TIPWIalnYP9pt47q9wG7";
			
		try {		
			if (Connection == null ) throw new Exception("Database failed to connect !");
			Connection.setAutoCommit(false);
			String sql = "SELECT  count(*) count FROM TblCuti WHERE IDKaryawan = ? AND Dlt = 0 AND TglMulai >= ? AND TglAkhir <= ? AND Dlt = 0  ";
			String sql2 = "CekApprove ?,2";
			String sql3 = "INSERT INTO tblcuti "
					+ " (IDKaryawan,JenisCuti,TglMulai,TglAkhir,InfoTambahan,Keterangan,LamaCuti,BonusCuti,KodeApprove,TglInput,CreateBy,Dlt,Provinsi,Dokument) "+
			        " VALUES (?,?,?,?,?,?,?,?,?,SYSDATETIME(),?,0,?,?)";
			String sql4 = "INSERT INTO TblSerahTerimaTugas "
					+ " (KodeCuti,IDKaryawan,Tanggal,Tugas,Dlt,TglInput,TglUbah,LapMasalahPenting,Status,Notifikasi,Hasil) "
					+ " VALUES(?,?,?,?,0,SYSDATETIME(),SYSDATETIME(),?,'Progressing',0,'')";
			String sql5 = "INSERT INTO TblCutiDetail(KodeCuti,Level,Notifikasi,TglInput,TglUbah,Alasan,Dlt,IDKaryawan,KodeJabatan,Status) VALUES(?,?,1,SYSDATETIME(),SYSDATETIME(),?,0,?,?,'Progressing')";			
			
			data.setSuccess(false);
			if(createCuti.getJlhreqcuti()>6 && !createCuti.getTipecuti().equals("Maternity") && !createCuti.getTipecuti().equals("Hamil") ){
				data.setMessage("Lama cuti tidak boleh lebih dari 6 hari!");
				throw new SQLException("Lama cuti tidak boleh lebih dari 6 hari!");
			}
			
			ps = Connection.prepareStatement(sql);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, createCuti.getIdkaryawan());
			ps.setString(2, createCuti.getTglawalcuti());
			ps.setString(3, createCuti.getTglakhircuti());
			rs = ps.executeQuery();
			
			if (rs.next() && !rs.getString("count").equals("0")) {
				data.setMessage("Tidak bisa mengajukan di tanggal yang sama. Atau anda sudah pernah mengajukan pada rentang tanggal tersebut.");
				throw new SQLException("Tidak bisa mengajukan di tanggal yang sama. Atau anda sudah pernah mengajukan pada rentang tanggal tersebut.");
			}
			
			if (createCuti.getJlhatasan() == 0) {
				ps2 = Connection.prepareStatement(sql2);
				ps2 = CommonUtil.initialStatement(ps2);
				ps2.setString(1, createCuti.getIdkaryawan());
				rs2 = ps2.executeQuery();
				
				if (rs2.next() && rs2.getInt("Hasil")==0) {
					data.setSuccess(false);
					data.setMessage("Structure Organisasi Salah. Sistem tidak menemukan data atasan anda!");
					throw new SQLException("Structure Organisasi Salah. Sistem tidak menemukan data atasan anda!");
				}
			}
			
			//assign variable static;
			int kodeapprove = 2;
			String keterangan = "Employee";
			String dokumen = "";
			
			int provinsi = 0;
			if(createCuti.getProv().equals("Luar")) provinsi = 2;
			
			ps3 = Connection.prepareStatement(sql3,Statement.RETURN_GENERATED_KEYS);
			ps3 = CommonUtil.initialStatement(ps3);
			ps3.setString(1, createCuti.getIdkaryawan());
			ps3.setString(2, createCuti.getTipecuti());
			ps3.setString(3, createCuti.getTglawalcuti());
			ps3.setString(4, createCuti.getTglakhircuti());
			ps3.setString(5, createCuti.getInfotambahan());
			ps3.setString(6, keterangan);
			ps3.setInt(7, createCuti.getJlhreqcuti());
			ps3.setString(8, createCuti.getBonuscuti());
			ps3.setInt(9, kodeapprove);
			ps3.setString(10, createCuti.getIdkaryawan());
			ps3.setInt(11, provinsi);
			ps3.setString(12, dokumen);
			ps3.execute();
			
			String id_cuti = "";
			generatedKeys = ps3.getGeneratedKeys();
			if (generatedKeys.next()) {
				id_cuti = generatedKeys.getString(1);
	        } else {
	        	data.setSuccess(false);
				data.setMessage("generate id_member failed");
	            throw new SQLException("Creating generate id_cuti failed, no generated key obtained.");
	        }
			
			//CreateCutiView createCutiView = view_create_cuti(createCuti.getIdkaryawan());
			
			 /** SERAH TERIMA TUGAS*/
			 
			if (createCuti.getList_serahtugas()!=null){
				for (SerahTugas dl2 : createCuti.getList_serahtugas()) {
					ps4 = null;
					ps4 = Connection.prepareStatement(sql4);
					ps4 = CommonUtil.initialStatement(ps4);
					ps4.setString(1, id_cuti);
					ps4.setString(2, dl2.getIdkaryawan());
					ps4.setString(3, dl2.getTanggal());
					ps4.setString(4, dl2.getTugas());
					ps4.setString(5, dl2.getLapmasalahpenting());
					ps4.execute();
					
					ps4.close();
				}
			}
						
			
		/** NOTIFIKASI ATASAN*/
			 
													
			if (createCuti.getList_cutiatasan()!=null){
				int i = 1;
				for (CutiAtasan dl3 : createCuti.getList_cutiatasan() ) {
					ps5 = null;
					ps5 = Connection.prepareStatement(sql5);
					ps5 = CommonUtil.initialStatement(ps5);
					ps5.setString(1, id_cuti);
					ps5.setInt(2, i);
					ps5.setString(3, "");
					ps5.setString(4, dl3.getIdkaryawan());
					ps5.setString(5, dl3.getKodejabatan());
					ps5.execute();
					
					ps5.close();
					i++;
				}
			}
			else
            {
				String sql6 = "InputCutiDetail ?";
				ps6 = Connection.prepareStatement(sql6);
				ps6 = CommonUtil.initialStatement(ps6);
				ps6.setString(1, createCuti.getIdkaryawan());
				ps6.execute();
            }
			/**
			 * KIRIM NOTIF 
			 */
			/**Android*/
			
			String sql7 = "CekAtasanCuti ? ";
			
			ps7 = Connection.prepareStatement(sql7);
			ps7 = CommonUtil.initialStatement(ps7);
			ps7.setString(1, createCuti.getIdkaryawan());
			rs7 = ps7.executeQuery();
			if(rs7.next()){
				String idKaryawanAtasan = rs7.getString("IDKaryawan");
				
				String sql8 = "SELECT firebase_token FROM usertoken WHERE usermstr_id = ? AND deleted = '0' AND updated_at = (SELECT MAX(updated_at) FROM usertoken where usermstr_id = ?)";
				ps8 = connectionMT.prepareStatement(sql8);
				ps8 = CommonUtil.initialStatement(ps8);
				ps8.setString(1, idKaryawanAtasan);
				ps8.setString(2, idKaryawanAtasan);
				rs8 = ps8.executeQuery();
				if(rs8.next()){
					String token = rs8.getString("firebase_token");
					
					String sql9 = "SELECT Username FROM TblUsers WHERE IDKaryawan =?";
					ps9 = Connection.prepareStatement(sql9);
					ps9 = CommonUtil.initialStatement(ps9);
					ps9.setString(1, createCuti.getIdkaryawan());
					rs9 = ps9.executeQuery();
					if(rs9.next()){
						String username = rs9.getString("Username");
						try {					
							URL url = new URL(FMCurl);
							HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					
							conn.setUseCaches(false);
							conn.setDoInput(true);
							conn.setDoOutput(true);
					
							conn.setRequestMethod("POST");
							conn.setRequestProperty("Authorization", "key=" + FMKey);
							conn.setRequestProperty("Content-Type", "application/json");
							JSONObject data1 = new JSONObject();
							data1.put("to", token);
							JSONObject info = new JSONObject();
							info.put("title", "Request Cuti");
							info.put("body", username + " Meminta Cuti");// Notification title
							info.put("click_action", "OPEN_ACTIVITY_HRIS");
							data1.put("notification", info);
					
							OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
							wr.write(data1.toString());
							wr.flush();
							wr.close();
					
							int responseCode = conn.getResponseCode();
							System.out.println("Response Code : " + responseCode);
					
							BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
							String inputLine;
							StringBuffer response = new StringBuffer();
					
							while ((inputLine = in.readLine()) != null) {
								response.append(inputLine);
							}
							in.close();
							//System.out.println(response.toString());	
						}
						catch (Exception e) {
							System.out.println(e.getMessage());
						}
					} 
				}
			}
			
			data.setSuccess(true);
			data.setMessage("Cuti Berhasi Terdaftar");
			Connection.commit();
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),createCuti.getIdkaryawan());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),createCuti.getIdkaryawan());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (connectionMT != null) try { connectionMT.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
			if (ps3 != null) try { ps3.close(); } catch (SQLException e) {}
			if (rs3 != null) try { rs3.close(); } catch (SQLException e) {}
			if (ps4 != null) try { ps4.close(); } catch (SQLException e) {}
			if (rs4 != null) try { rs4.close(); } catch (SQLException e) {}
			if (ps5 != null) try { ps5.close(); } catch (SQLException e) {}
			if (rs5 != null) try { rs5.close(); } catch (SQLException e) {}
			if (ps6 != null) try { ps6.close(); } catch (SQLException e) {}
			if (rs6 != null) try { rs6.close(); } catch (SQLException e) {}
			if (ps7 != null) try { ps7.close(); } catch (SQLException e) {}
			if (rs7 != null) try { rs7.close(); } catch (SQLException e) {}
			if (ps8 != null) try { ps8.close(); } catch (SQLException e) {}
			if (rs8 != null) try { rs8.close(); } catch (SQLException e) {}
			if (ps9 != null) try { ps9.close(); } catch (SQLException e) {}
			if (rs9 != null) try { rs9.close(); } catch (SQLException e) {}
			if (generatedKeys != null) try { generatedKeys.close(); } catch (SQLException e) {}
		}		
		return data;
	}
	
	public static Object update_cuti(CreateCuti createCuti){
		ResponseStatus data = new ResponseStatus();
		
		delete_cuti(createCuti.getKodecuti());
		data = create_cuti(createCuti);
		
		return data;
	}
	
	public static ResponseStatus delete_cuti(String kodecuti) {
		ResponseStatus data = new ResponseStatus();
		Connection Connection = null;
		PreparedStatement ps=null, ps1=null, ps2=null, ps3=null;
		ResultSet rs=null, rs1=null, rs2=null;
		boolean deleted = false; 
		
		try {
			Connection = DbConnection.getHrisInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			Connection.setAutoCommit(false);
			
			String selectQuery = "SELECT KodeCutiDetail FROM TblCutiDetail "
					+ " WHERE KodeCuti = ? AND Status = 'Progressing'";
			ps = Connection.prepareStatement(selectQuery);
			ps = CommonUtil.initialStatement(ps);
			ps.setLong(1, new Long(kodecuti));
			rs = ps.executeQuery();
			while (rs.next()){
				deleted = true;
				ps2=null;
				
				String sqldetail = "Update TblCutiDetail SET"
						+" Dlt=1"
						+" WHERE KodeCutiDetail = ?";
				ps2 = Connection.prepareStatement(sqldetail);
				ps2 = CommonUtil.initialStatement(ps2);
				ps2.setString(1, rs.getString("KodeCutiDetail"));
				ps2.execute();
				
				if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			}
			
			if (deleted) {
				String sqlupdate = "UPDATE TblCuti SET"
						+" Dlt=1"
						+" WHERE KodeCuti = ?";
				ps1 = Connection.prepareStatement(sqlupdate);
				ps1 = CommonUtil.initialStatement(ps1);
				ps1.setString(1, kodecuti);
				ps1.execute();
				
				String sqlupdate3 = "UPDATE TblSerahTerimaTugas SET"
						+" Dlt=1"
						+" WHERE KodeCuti = ?";
				ps3 = Connection.prepareStatement(sqlupdate3);
				ps3 = CommonUtil.initialStatement(ps3);
				ps3.setString(1, kodecuti);
				ps3.execute();
				
				data.setMessage("Data berhasil di hapus.");
				data.setSuccess(true);
			}
			
			Connection.commit();

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps1 != null) try { ps1.close(); } catch (SQLException e) {}
			if (rs1 != null) try { rs1.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
			if (ps3 != null) try { ps3.close(); } catch (SQLException e) {}
		}
		return data;
	}
	
	public static Object get_cuti_info(String kodecuti) {
		CreateCuti data = new CreateCuti();
		Connection Connection = null;
		PreparedStatement ps=null, ps1=null, ps2=null, ps3=null;
		ResultSet rs=null, rs1=null, rs2=null,rs3=null;
		
		try {
			Connection = DbConnection.getHrisInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			
			String sqlupdate = "SELECT * FROM TblCuti WHERE "
					+" Dlt=0"
					+" AND KodeCuti = ?";
			ps1 = Connection.prepareStatement(sqlupdate);
			ps1 = CommonUtil.initialStatement(ps1);
			ps1.setString(1, kodecuti);
			rs1 = ps1.executeQuery();
			
			if (rs1.next()) {
				
				data.setKodecuti(rs1.getString("KodeCuti"));
				data.setIdkaryawan(rs1.getString("IDKaryawan"));
				data.setTglawalcuti(rs1.getString("TglMulai"));
				data.setTglakhircuti(rs1.getString("TglAkhir"));
				data.setJlhreqcuti(0);
				data.setTipecuti(rs1.getString("JenisCuti"));
				data.setJlhatasan(0);
				data.setInfotambahan(rs1.getString("InfoTambahan"));
				data.setLamacuti(rs1.getString("LamaCuti"));
				data.setProv(rs1.getString("Provinsi"));
				data.setBonuscuti(rs1.getString("BonusCuti"));
				
				String selectQuery = "SELECT KodeCutiDetail,KodeCuti,Level, Status, Alasan,TglInput,TglUbah,Notifikasi, IDKaryawan  FROM TblCutiDetail "
						+ " WHERE KodeCuti = ?";
				ps = Connection.prepareStatement(selectQuery);
				ps = CommonUtil.initialStatement(ps);
				ps.setLong(1, new Long(kodecuti));
				rs = ps.executeQuery();
				
				List<CutiAtasan> lcuti_atasan = new ArrayList<CutiAtasan>();
				while (rs.next()){
					CutiAtasan cutiAtasan = new CutiAtasan();
					cutiAtasan.setIdkaryawan(rs.getString("IDKaryawan"));
					lcuti_atasan.add(cutiAtasan);
				}
				
				String sqlupdate3 = "SELECT KodeSerahTugas,KodeCuti,IDKaryawan,Tanggal,Tugas,LapMasalahPenting,Status FROM TblSerahTerimaTugas WHERE "
						+" Dlt=0"
						+" AND KodeCuti = ?";
				ps3 = Connection.prepareStatement(sqlupdate3);
				ps3 = CommonUtil.initialStatement(ps3);
				ps3.setString(1, kodecuti);
				rs3 = ps3.executeQuery();

				List<SerahTugas> lserah_tugas = new ArrayList<SerahTugas>();

				while (rs3.next()) {
					SerahTugas serahTugas = new SerahTugas();
					serahTugas.setIdkaryawan(rs3.getString("IDKaryawan"));
					serahTugas.setTanggal(rs3.getString("Tanggal"));
					serahTugas.setTugas(rs3.getString("Tugas"));
					serahTugas.setLapmasalahpenting(rs3.getString("LapMasalahPenting"));
					lserah_tugas.add(serahTugas);
				}
				
				data.setList_cutiatasan(lcuti_atasan);
				data.setList_serahtugas(lserah_tugas);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps1 != null) try { ps1.close(); } catch (SQLException e) {}
			if (rs1 != null) try { rs1.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
			if (ps3 != null) try { ps3.close(); } catch (SQLException e) {}
			if (rs3 != null) try { rs3.close(); } catch (SQLException e) {}
		}
		return data;
	}
	
	public static ResponseStatus create_izin_hris(RequestCreateIzin model) {
		ResponseStatus data = new ResponseStatus();
		
		final String FMCurl = "https://fcm.googleapis.com/fcm/send";
		final String FMKey = "AAAAH9Z5K-o:APA91bGP1q7L3_JfQvW_Nm-XinKC7syUAPTXy6e4S2sX7tbCcRjB5zPuUT-nhwhfMUEj6X_bySZ5LNONqI-x-jkIrFn-nw_H3jRq5XDrN9AIiSF3BdCPb-y1TIPWIalnYP9pt47q9wG7";
		
		String idatasan ="";
        String tglizinmasuk = model.getTglpermohonan();
        String tglizinkeluar = model.getTglpermohonan();
		if(model.getIdkatasan() != null && !model.getIdkatasan().equals("")){ 
			idatasan = model.getIdkatasan(); 
		}
        if(model.getTglend() != null && !model.getTglend().equals("") ){ 
        	tglizinmasuk = model.getTglend();
        }
              
		Connection Connection = null;
		Connection connectionMT = null;
		PreparedStatement ps=null, ps1a=null , ps1=null, ps2=null, ps3=null, ps4=null, ps5=null, ps6=null, ps7=null , ps8=null, ps9=null;
		ResultSet rs=null, rs1a=null, rs1=null, rs2=null, rs3=null, rs4=null, rs5=null, rs6=null, rs7=null, rs8=null, rs9=null;
		
		try {
			Connection = DbConnection.getHrisInstance();
			connectionMT = DbConnection.getBpjsOnlineInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			Connection.setAutoCommit(false);
			
			String sql_checkBatasJmlIzin = "SELECT COUNT(KodeIzin) as jumlah FROM TblPermohonanIzin WHERE IDKaryawan = ? AND Dlt = 0 AND "
					+ " DATEPART(m, TglIzinKeluar) = DATEPART(m, DATEADD(m, 0, getdate())) "
					+ " AND DATEPART(yyyy, TglIzinKeluar) = DATEPART(yyyy, DATEADD(m, 0, getdate())) "
					+ " AND ( TipeIzin = 'Late' OR TipeIzin = 'Lupa Absen Pulang') ";
			
			ps1a = Connection.prepareStatement(sql_checkBatasJmlIzin);
			ps1a = CommonUtil.initialStatement(ps1a);
			ps1a.setString(1, model.getIdkaryawan());
			rs1a = ps1a.executeQuery();
			
			if (rs1a.next() && rs1a.getInt("jumlah")>= 3) {
				data.setMessage("Batasan pembuatan izin telat dan pulang cepat adalah 3x");
				throw new Exception("Batasan pembuatan izin telat dan pulang cepat adalah 3x");
			}
			
			String selectQuery = "SELECT TipeIzin FROM TblPermohonanIzin "
					+ " WHERE IDKaryawan = ? AND Dlt = 0"
					+ " AND TglIzinKeluar >= ?  AND TglIzinKeluar <= ? OR"
					+ " IDKaryawan = ? AND Dlt = 0 "
					+ " AND TglIzinMasuk >= ?  AND TglIzinMasuk <= ?";

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date_now = new Date();
			ps = Connection.prepareStatement(selectQuery);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, model.getIdkaryawan());
			ps.setString(2, dateFormat.format(date_now));
			ps.setString(3, tglizinmasuk);
			ps.setString(4, model.getIdkaryawan());
			ps.setString(5, dateFormat.format(date_now));
			ps.setString(6, tglizinmasuk);
			rs = ps.executeQuery();
			
			int j = 0;
			String izintype ="";
			while (rs.next()){
				if(izintype.isEmpty()) izintype = rs.getString("TipeIzin");
				j++;
			}
			
			if(j == 1){
				if(izintype == model.getTipe() && !model.getTipe().equals("Hourly")){
					data.setMessage("Anda sudah mengajukan tipe izin tersebut di tanggal tersebut.");	
					throw new Exception("Anda sudah mengajukan tipe izin tersebut di tanggal tersebut.");
				} else{
					if(!izintype.equals("Hourly") && !izintype.equals("Daily Office Duty")  ) {
						if(!model.getTipe().equals("Daily Office Duty") && !model.getTipe().equals("Hourly")){
							data.setMessage("Tidak bisa mengajukan di tanggal yang sama atau anda sudah pernah mengajukan pada rentang tanggal tersebut.");
							throw new Exception("Tidak bisa mengajukan di tanggal yang sama atau anda sudah pernah mengajukan pada rentang tanggal tersebut.");
						}
					}
				}
			} else if(j > 1){
				data.setMessage("Batas pengajuan adalah 2 kali di tanggal yang sama.");
				throw new Exception("Batas pengajuan adalah 2 kali di tanggal yang sama.");
			}
			

			if(idatasan.isEmpty()){
				String sql2 = "CekApprove ?,1 ";
				ps2 = Connection.prepareStatement(sql2);
				ps2 = CommonUtil.initialStatement(ps2);
				ps2.setString(1, model.getIdkaryawan());
				rs2 = ps2.executeQuery();
				
				while(rs2.next() && rs2.getLong("Hasil")<= 0){
					data.setMessage("Struktur Organisasi Salah.");
					throw new Exception("Struktur Organisasi Salah.");
				}
			}
			
			String sql3 = " SELECT Sisa FROM TblSaldoCuti WHERE IDKaryawan = ? AND StatusUpdate = 0 ";
			ps3 = Connection.prepareStatement(sql3);
			ps3 = CommonUtil.initialStatement(ps3);
			ps3.setString(1, model.getIdkaryawan());
			rs3 = ps3.executeQuery();
			
			int sisa = 0;
			if(rs3.next()) sisa = rs3.getInt("Sisa");
			
				
			Boolean proses = false;
			String dokument1 = "";
			String dokument2 = "";
			String dokument3 = "";
			
			String jammasuk = "";
			String jamkeluar = "";
	        String kendaraan = "Own";
			String supir = "";
	        String alasan = "";
			
			int selisih = 0;
			int bonuscuti = 0;
			
	        String jenispotong = null;
	        String infopotongan = null;
	        
	        
			if (model.getRbizin().equals("Half Time"))
            {
                tglizinmasuk =  model.getTglpermohonan();
            } else {
            	tglizinmasuk = model.getTglend();
            }
			
			if(model.getJamEnd() != null && !model.getJamEnd().isEmpty() ){jammasuk = model.getJamEnd(); }
			if(model.getJamStart() != null && !model.getJamStart().isEmpty() ){ jamkeluar = model.getJamStart(); }
			if(model.getKendaraan() != null && !model.getKendaraan().isEmpty()) {
				if(model.getKendaraan().equals("Office")){
					supir = model.getSupir();
				}
			}

            if (model.getRbizin().equals("Half Time"))
            {
                jenispotong = "None";
                alasan = model.getAlasan();
            	data.setMessage("Permohonan anda berhasil dikirim.");
            	proses = true;
            }
            else if (model.getRbizin().equals("Full Time") )
            {
            	alasan = model.getAlasan();
        		int JlhCuti = model.getJlh();
        		
            	if (model.getTipe().equals("Daily Office Duty"))
            	{
            		jenispotong = "None";
            		proses = true;
            	}
            	else if (!model.getTipe().equals("Daily Office Duty"))
            	{
            		if (JlhCuti == 1)
            		{
            			if(sisa <= 0 ){
    						jenispotong = "gaji";
    						infopotongan = "gaji";
    						bonuscuti = 0;
    						dokument2 = Integer.toString(JlhCuti);
    					}
    					else if(sisa > 0)
    					{
    						jenispotong = "cuti";
							infopotongan = "saldo cuti";
							dokument3 = Integer.toString(JlhCuti);
    						bonuscuti = 0;
    					}
            		} else if (JlhCuti > 1) {
            			if(sisa <= 0 ){
    						jenispotong = "gaji";
    						infopotongan = "gaji";
    						dokument2 = Integer.toString(JlhCuti);
    					}
    					else if(sisa > 0)
    					{
    						if(JlhCuti <= sisa){
    							
    							jenispotong = "cuti";
    							infopotongan = "saldo cuti";
    							dokument3 = Integer.toString(JlhCuti);
    							
    						} else if(JlhCuti > sisa){
    							
    							jenispotong = "cuti dan gaji";
    							infopotongan = "saldo cuti dan gaji";
    							selisih = JlhCuti - sisa;
    							dokument3 = Integer.toString(sisa);
    							dokument2 = Integer.toString(selisih);
    						}
    					}
            			
            		}
	            	data.setMessage("Permohonan anda berhasil dikirim, permohonan ini akan memotong "+infopotongan+" anda.");
	            	proses = true;

                }
            	if (model.getTipe().equals("Sick"))
            	{
	            	data.setMessage("Proses.");
	            	proses = true;
            	}
            }
            String sqlinsert = "INSERT INTO TblPermohonanIzin"
					+" (TipeIzin, IDKaryawan, TglIzinKeluar, TglIzinMasuk, JamIzinKeluar, JamIzinMasuk, Alasan, Kendaraan, Supir, Dokumen1, Dokumen2, Dokumen3, KodeApprove, TglPermohonan, Dlt, Keterangan, JenisPotong, BonusCuti, Lokasi, ApprovalBy )"
					+" OUTPUT Inserted.KodeIzin"
					+" VALUES"
					+" (?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATETIME(),?,?,?,?,?,1)";
			ps4 = Connection.prepareStatement(sqlinsert);
			ps4 = CommonUtil.initialStatement(ps4);
			ps4.setString(1, model.getTipe());
			ps4.setString(2, model.getIdkaryawan());
			ps4.setString(3, tglizinkeluar);
			ps4.setString(4, tglizinmasuk);
			ps4.setString(5, jamkeluar);
			ps4.setString(6, jammasuk);
			ps4.setString(7, model.getAlasan());
			ps4.setString(8, model.getKendaraan());
			ps4.setString(9, supir);
			ps4.setString(10, dokument1);
			ps4.setString(11, dokument2);
			ps4.setString(12, dokument3);
			ps4.setString(13, "1");
			ps4.setString(14, "0");
			ps4.setString(15, "Proses");
			ps4.setString(16, jenispotong);
			ps4.setInt(17, bonuscuti);
			ps4.setString(18, model.getRbizin());
			rs4 = ps4.executeQuery();
			String KodeIzin = "";
            while(rs4.next())
            {
            	KodeIzin = Integer.toString(rs4.getInt("KodeIzin"));
            }
			
			//String sqlnama = "SELECT Nama FROM TblKaryawan WHERE IDKaryawan = ?";
			//ps1 = Connection.prepareStatement(sqlnama);
			//ps1.setEscapeProcessing(true);
			//ps1.setQueryTimeout(10000);
			//ps1.setString(1, model.getIdkaryawan());
			//rs1 = ps1.executeQuery();
			//String NamaKaryawan = "";
			//while(rs1.next()){
			//	NamaKaryawan = rs1.getString("Nama");
			//}

			//String kodejabatan = "";
			//String Email = "";
			//String pesan = "";
			
			if (idatasan.isEmpty())
            {
				String sql5 = "InputPermohonanIzinDetail ?";
				ps5 = Connection.prepareStatement(sql5);
				ps5 = CommonUtil.initialStatement(ps5);
				ps5.setString(1, model.getIdkaryawan());
				ps5.execute();
				
				//String sql6 = "CekAtasanCuti ?";
				//ps6 = Connection.prepareStatement(sql6);
				//ps6.setEscapeProcessing(true);
				//ps6.setQueryTimeout(10000);
				//ps6.setString(1, model.getIdkaryawan());
				//rs6 = ps6.executeQuery();
				//while(rs6.next()){
				//	Email = rs6.getString("Email");
				//}
				//pesan = "ID Karyawan: " + model.getIdkaryawan() + " <br />" +
                //               "Nama: " + NamaKaryawan + " <br />" +
                //                "Tipe: " + model.getTipe() + " <br />" +
                //                "Tanggal: " + tglizinkeluar + " - " + tglizinmasuk + " <br />" +
                //                "Alasan: " + model.getAlasan();
                //String sendemail = new PublicController().SendEmailNotification(Email, "Permohonan Izin", pesan); 
            }
            else if (idatasan != "")
            {	
            	String kodejabatan = "";
    			String Email = "";
                
                String sql5 = "SELECT KodeJabatan,Email FROM TblKaryawan WHERE IDKaryawan = ?";
				ps5 = Connection.prepareStatement(sql5);
				ps5 = CommonUtil.initialStatement(ps5);
				ps5.setString(1, idatasan);
				rs5 = ps5.executeQuery();
				while(rs5.next()){
					Email = rs5.getString("Email");
					kodejabatan = rs5.getString("KodeJabatan");
				}
				
				
				String sqldetail = "INSERT INTO TblPermohonanIzinDetail"
						+" (KodeIzin, KodeJabatan, Level, Status, Alasan, TglInput, TglUbah, Dlt, Notifikasi, IDKaryawan)"
						+" VALUES"
						+" (?,?,?,?,?,SYSDATETIME(),SYSDATETIME(),?,?,?)";
				
				
				ps6 = Connection.prepareStatement(sqldetail);
				ps6 = CommonUtil.initialStatement(ps6);
				ps6.setString(1, KodeIzin);
				ps6.setString(2, kodejabatan);
				ps6.setString(3, "1");
				ps6.setString(4, "Progressing");
				ps6.setString(5, model.getAlasan());
				ps6.setString(6, "0");
				ps6.setString(7, "0");
				ps6.setString(8, idatasan);
				ps6.execute();
				
				
				//pesan = "ID Karyawan: " + model.getIdkaryawan() + " <br />" +
                //                "Nama: " + NamaKaryawan + " <br />" +
                //                "Tipe: " + model.getTipe() + " <br />" +
                //                "Tanggal: " + tglizinkeluar + " - " + tglizinmasuk + " <br />" +
                //                "Alasan: " + model.getAlasan();
                //String sendemail = new PublicController().SendEmailNotification(Email, "Permohonan Izin", pesan); 
            }
			
			/**NOTIF ANDROID Firebase*/
			
			String sql7 = "CekAtasanCuti ? ";
			
			ps7 = Connection.prepareStatement(sql7);
			ps7 = CommonUtil.initialStatement(ps7);
			ps7.setString(1, model.getIdkaryawan());
			rs7 = ps7.executeQuery();
			if(rs7.next()){
				String idKaryawanAtasan = rs7.getString("IDKaryawan");
				
				String sql8 = "SELECT firebase_token FROM usertoken WHERE usermstr_id = ? AND deleted = '0' AND updated_at = (SELECT MAX(updated_at) FROM usertoken where usermstr_id = ?)";
				ps8 = connectionMT.prepareStatement(sql8);
				ps8 = CommonUtil.initialStatement(ps8);
				ps8.setString(1, idKaryawanAtasan);
				ps8.setString(2, idKaryawanAtasan);
				rs8 = ps8.executeQuery();
				if(rs8.next()){
					String token = rs8.getString("firebase_token");
					
					String sql9 = "SELECT Username FROM TblUsers WHERE IDKaryawan =?";
					ps9 = Connection.prepareStatement(sql9);
					ps9 = CommonUtil.initialStatement(ps9);
					ps9.setString(1, model.getIdkaryawan());
					rs9 = ps9.executeQuery();
					if(rs9.next()){
						String username = rs9.getString("Username");
						try {					
							URL url = new URL(FMCurl);
							HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					
							conn.setUseCaches(false);
							conn.setDoInput(true);
							conn.setDoOutput(true);
					
							conn.setRequestMethod("POST");
							conn.setRequestProperty("Authorization", "key=" + FMKey);
							conn.setRequestProperty("Content-Type", "application/json");
							JSONObject data1 = new JSONObject();
							data1.put("to", token);
							JSONObject info = new JSONObject();
							info.put("title", "Request Izin");
							info.put("body", username + " Meminta Izin");// Notification title
							info.put("click_action", "OPEN_ACTIVITY_HRIS");
							data1.put("notification", info);
					
							OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
							wr.write(data1.toString());
							wr.flush();
							wr.close();
					
							int responseCode = conn.getResponseCode();
							System.out.println("Response Code : " + responseCode);
					
							BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
							String inputLine;
							StringBuffer response = new StringBuffer();
					
							while ((inputLine = in.readLine()) != null) {
								response.append(inputLine);
							}
							in.close();
							//System.out.println(response.toString());	
						}
						catch (Exception e) {
							System.out.println(e.getMessage());
						}
					}
				}
			}
			data.setSuccess(true);
			//throw new IllegalArgumentException("Testing dev, dont commit");
			Connection.commit();			
		}		
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),model.getIdkaryawan());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),model.getIdkaryawan());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
 			if (connectionMT != null) try { connectionMT.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps1 != null) try { ps1.close(); } catch (SQLException e) {}
			if (rs1 != null) try { rs1.close(); } catch (SQLException e) {}
			if (ps1a != null) try { ps1a.close(); } catch (SQLException e) {}
			if (rs1a != null) try { rs1a.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
			if (ps3 != null) try { ps3.close(); } catch (SQLException e) {}
			if (rs3 != null) try { rs3.close(); } catch (SQLException e) {}
			if (ps4 != null) try { ps4.close(); } catch (SQLException e) {}
			if (rs4 != null) try { rs4.close(); } catch (SQLException e) {}
			if (ps5 != null) try { ps5.close(); } catch (SQLException e) {}
			if (rs5 != null) try { rs5.close(); } catch (SQLException e) {}
			if (ps6 != null) try { ps6.close(); } catch (SQLException e) {}
			if (rs6 != null) try { rs6.close(); } catch (SQLException e) {}
			if (ps7 != null) try { ps7.close(); } catch (SQLException e) {}
			if (rs7 != null) try { rs7.close(); } catch (SQLException e) {}
			if (ps8 != null) try { ps8.close(); } catch (SQLException e) {}
			if (rs8 != null) try { rs8.close(); } catch (SQLException e) {}
			if (ps9 != null) try { ps9.close(); } catch (SQLException e) {}
			if (rs9 != null) try { rs9.close(); } catch (SQLException e) {}
		}
		return data;
	}
	
	public static ResponseStatus update_izin_hris(RequestCreateIzin model) {
		ResponseStatus data = new ResponseStatus();
        String tglizinmasuk = model.getTglpermohonan();
        String tglizinkeluar = model.getTglpermohonan();
        if(model.getTglend() != null && model.getTglend() != "" ){ tglizinmasuk = model.getTglend();}
        
		Connection Connection = null;
		PreparedStatement ps=null, ps1=null, ps2=null, ps3=null, ps4=null;
		ResultSet rs=null, rs1=null, rs2=null, rs3=null, rs4=null;
		
		try {
			Connection = DbConnection.getHrisInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			Connection.setAutoCommit(false);
			
			
			
			String selectQuery = "SELECT * FROM TblPermohonanIzinDetail "
					+ " WHERE KodeIzin = ? AND Status = 'Progressing'";
			ps = Connection.prepareStatement(selectQuery);
			ps = CommonUtil.initialStatement(ps);
			ps.setLong(1, model.getKodeizin());
			rs = ps.executeQuery();
			while (rs.next()){
				
				String sql1 = " SELECT Sisa FROM TblSaldoCuti WHERE IDKaryawan = ? AND StatusUpdate = 0 ";
				ps1 = Connection.prepareStatement(sql1);
				ps1 = CommonUtil.initialStatement(ps1);
				ps1.setString(1, model.getIdkaryawan());
				rs1 = ps1.executeQuery();
				
				String dokument1 = null;
				String dokument2 = null;
				String dokument3 = null;
				String jammasuk = null;
				String jamkeluar = null;
				int selisih = 0;
				String bonuscuti = null;
				String supir = null;
		        String jenispotong = null;
		        String infopotongan = null;
		        
				if (model.getRbizin().equals("Half Time"))
	            {
	                tglizinmasuk =  model.getTglpermohonan();
	            } else {
	            	tglizinmasuk = model.getTglend();
	            }
				
				if(model.getJamEnd() != null && !model.getJamEnd().isEmpty() ){jammasuk = model.getJamEnd(); }
				if(model.getJamStart() != null && !model.getJamStart().isEmpty() ){ jamkeluar = model.getJamStart(); }
				if(model.getKendaraan() != null && !model.getKendaraan().isEmpty()) {
					if(model.getKendaraan().equals("Office")){
						supir = model.getSupir();
					}
				}

	            if (model.getRbizin().equals("Half Time"))
	            {
	            	if  (!model.getTipe().equals("Late") && !model.getTipe().equals("Lupa Absen Pulang"))
	                {
	            		jenispotong = "None";
	            	}
	            	else if (model.getTipe().equals("Late"))
	                {
	            		jenispotong = "None";
	                }
	                else if (model.getTipe().equals("Lupa Absen Pulang"))
	                {
	                    jenispotong = "None";

	                }
	            	data.setMessage("Permohonan anda berhasil dikirim.");
	            }
	            else if (model.getRbizin().equals("Full Time"))
	            {
	            	int sisa = rs1.getInt("Sisa");
	            	if (model.getTipe().equals("Daily Office Duty"))
	            	{
	            		jenispotong = "None";
	            	}
	            	else if (!model.getTipe().equals("Daily Office Duty"))
	            	{
	            		if (model.getJlh() == 1)
	            		{
	            			if(sisa <= 0 ){
	    						jenispotong = "gaji";
	    						infopotongan = "gaji";
	    						bonuscuti = Integer.toString(model.getJlh());
	    						dokument2 = Integer.toString(model.getJlh());
	    					}
	    					else if(sisa > 0)
	    					{
	    						jenispotong = "cuti";
								infopotongan = "saldo cuti";
								dokument3 = Integer.toString(model.getJlh());
	    						bonuscuti = "0";
	    					}
	            		} else if (model.getJlh() > 1) {
	            			if(sisa <= 0 ){
	    						jenispotong = "gaji";
	    						infopotongan = "gaji";
	    						dokument2 = Integer.toString(model.getJlh());
	    					}
	    					else if(sisa > 0)
	    					{
	    						bonuscuti = "0";
	    						if(model.getJlh() > sisa){
	    							jenispotong = "cuti dan gaji";
	    							infopotongan = "saldo cuti dan gaji";
	    							selisih = model.getJlh() - sisa;
	    							dokument3 = Integer.toString(sisa);
	    							dokument2 = Integer.toString(selisih);
	    						} else if(model.getJlh() <= sisa){
	    							jenispotong = "cuti";
	    							infopotongan = "saldo cuti";
	    							dokument3 = Integer.toString(model.getJlh());
	    						}
	    					}
	            			
	            		}
		            	data.setMessage("Permohonan anda berhasil dikirim, permohonan ini akan memotong "+infopotongan+" anda.");

	                }
	            	if (model.getTipe().equals("Sick"))
	            	{
		            	data.setMessage("Proses.");
	            	}
	            }
	            String sqlupdate = "UPDATE TblPermohonanIzin SET"
						+" TipeIzin=?, TglIzinKeluar=?, TglIzinMasuk=?, JamIzinKeluar=?, JamIzinMasuk=?, Alasan=?, Kendaraan=?, Supir=?, Dokumen1=?, Dokumen2=?, Dokumen3=?, KodeApprove=?, TglPermohonan=?, Dlt=?, Keterangan=?, JenisPotong=?, BonusCuti=?, Lokasi=?"
						+" WHERE KodeIzin = ?";
				ps2 = Connection.prepareStatement(sqlupdate);
				ps2 = CommonUtil.initialStatement(ps2);
				ps2.setString(1, model.getTipe());
				ps2.setString(2, tglizinkeluar);
				ps2.setString(3, tglizinmasuk);
				ps2.setString(4, jamkeluar);
				ps2.setString(5, jammasuk);
				ps2.setString(6, model.getAlasan());
				ps2.setString(7, model.getKendaraan());
				ps2.setString(8, supir);
				ps2.setString(9, dokument1);
				ps2.setString(10, dokument2);
				ps2.setString(11, dokument3);
				ps2.setString(12, "1");
				ps2.setString(13, model.getTglpermohonan());
				ps2.setString(14, "0");
				ps2.setString(15, "Proses");
				ps2.setString(16, jenispotong);
				ps2.setString(17, bonuscuti);
				ps2.setString(18, model.getRbizin());
				ps2.setString(19, rs.getString("KodeIzin"));
				ps2.execute();
				
				String sql3 = "SELECT Nama FROM TblKaryawan WHERE IDKaryawan = ?";
				ps3 = Connection.prepareStatement(sql3);
				ps3 = CommonUtil.initialStatement(ps3);
				ps3.setString(1, model.getIdkaryawan());
				rs3 = ps1.executeQuery();
				String NamaKaryawan = "";
				while(rs3.next()){
					NamaKaryawan = rs3.getString("Nama");
				}
				Calendar calendar = Calendar.getInstance();
                java.sql.Date ourJavaDateObject = new java.sql.Date(calendar.getTime().getTime());
				String sqldetail = "Update TblPermohonanIzinDetail SET"
						+" Level=?, Status=?, Alasan=?, TglInput=?, TglUbah=?, Dlt=?, Notifikasi=?"
						+" WHERE KodeIzinDetail = ?";
				ps4 = Connection.prepareStatement(sqldetail);
				ps4 = CommonUtil.initialStatement(ps4);
				ps4.setString(1, "1");
				ps4.setString(2, "Progressing");
				ps4.setString(3, model.getAlasan());
				ps4.setDate(4, ourJavaDateObject);
				ps4.setDate(5, ourJavaDateObject);
				ps4.setString(6, "0");
				ps4.setString(7, "0");
				ps4.setString(8, rs.getString("KodeIzinDetail"));
				ps4.execute();
				String pesan = "ID Karyawan: " + model.getIdkaryawan() + " <br />" +
                                "Nama: " + NamaKaryawan + " <br />" +
                                "Tipe: " + model.getTipe() + " <br />" +
                                "Tanggal: " + tglizinkeluar + " - " + tglizinmasuk + " <br />" +
                                "Alasan: " + model.getAlasan();
				data.setSuccess(true);
						
			}
			Connection.commit();

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),model.getIdkaryawan());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),model.getIdkaryawan());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps1 != null) try { ps1.close(); } catch (SQLException e) {}
			if (rs1 != null) try { rs1.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
			if (ps3 != null) try { ps3.close(); } catch (SQLException e) {}
			if (rs3 != null) try { rs3.close(); } catch (SQLException e) {}
			if (ps4 != null) try { ps4.close(); } catch (SQLException e) {}
			if (rs4 != null) try { rs4.close(); } catch (SQLException e) {}
		}
		return data;
	}
	
	public static ResponseStatus delete_izin_hris(RequestCreateIzin model) {
		ResponseStatus data = new ResponseStatus();
		Connection Connection = null;
		PreparedStatement ps=null, ps1=null, ps2=null;
		ResultSet rs=null, rs1=null, rs2=null;
		
		try {
			Connection = DbConnection.getHrisInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			Connection.setAutoCommit(false);
			
			String selectQuery = "SELECT * FROM TblPermohonanIzinDetail "
					+ " WHERE KodeIzin = ? AND Status = 'Progressing'";
			ps = Connection.prepareStatement(selectQuery);
			ps = CommonUtil.initialStatement(ps);
			ps.setLong(1, model.getKodeizin());
			rs = ps.executeQuery();
			while (rs.next()){
				ps1=null;
				ps2=null;
				
	            String sqlupdate = "UPDATE TblPermohonanIzin SET"
						+" Dlt=1"
						+" WHERE KodeIzin = ?";
				ps1 = Connection.prepareStatement(sqlupdate);
				ps1 = CommonUtil.initialStatement(ps1);
				ps1.setString(1, rs.getString("KodeIzin"));
				ps1.execute();
				
				String sqldetail = "Update TblPermohonanIzinDetail SET"
						+" Dlt=1"
						+" WHERE KodeIzinDetail = ?";
				ps2 = Connection.prepareStatement(sqldetail);
				ps2 = CommonUtil.initialStatement(ps2);
				ps2.setString(1, rs.getString("KodeIzinDetail"));
				ps2.execute();
				
				data.setMessage("Data berhasil di hapus.");
				data.setSuccess(true);
				
				if (ps1 != null) try { ps1.close(); } catch (SQLException e) {}
				if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			}
			Connection.commit();
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),model.getIdkaryawan());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),model.getIdkaryawan());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps1 != null) try { ps1.close(); } catch (SQLException e) {}
			if (rs1 != null) try { rs1.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
		}
		return data;
	}

	public static List<Karyawan> get_atasan(String id_karyawan) {
		List<Karyawan> list = new ArrayList<Karyawan>();
		
		try {
			String sql = "CekAtasanCuti ? ";
			
			List res = DbConnection.executeReader(DbConnection.HRIS, sql, new Object[] {id_karyawan});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				
				Karyawan dl = new Karyawan();
				dl.setIdkaryawan(String.valueOf(rs.get("idkaryawan")));
				dl.setFingerprintid(String.valueOf(rs.get("fingerprintid")));
				dl.setKodebagian(String.valueOf(rs.get("kodebagian")));
				dl.setKodejabatan(String.valueOf(rs.get("kodejabatan")));
				dl.setKodecabang(String.valueOf(rs.get("kodecabang")));
				dl.setKodecabangpembantu(String.valueOf(rs.get("kodecabangpembantu")));
				dl.setNoktp(String.valueOf(rs.get("noktp")));
				dl.setNosim(String.valueOf(rs.get("nosim")));
				dl.setNpwp(String.valueOf(rs.get("npwp")));
				dl.setNama(String.valueOf(rs.get("nama")));
				dl.setTempatlahir(String.valueOf(rs.get("tempatlahir")));
				dl.setTgllahir(String.valueOf(rs.get("tgllahir")));
				dl.setEmail(String.valueOf(rs.get("email")));
				dl.setAlamatpermanent(String.valueOf(rs.get("alamatpermanent")));
				dl.setAlamatsementara(String.valueOf(rs.get("alamatsementara")));
				dl.setRtrw(String.valueOf(rs.get("rtrw")));
				dl.setKel(String.valueOf(rs.get("kel")));
				dl.setKec(String.valueOf(rs.get("kec")));
				dl.setAgama(String.valueOf(rs.get("agama")));
				dl.setStatusnikah(String.valueOf(rs.get("statusnikah")));
				dl.setWn(String.valueOf(rs.get("wn")));
				dl.setKelamin(String.valueOf(rs.get("kelamin")));
				dl.setSuku(String.valueOf(rs.get("suku")));
				dl.setHp(String.valueOf(rs.get("hp")));
				dl.setTelepon(String.valueOf(rs.get("telepon")));
				dl.setUkuranbaju(String.valueOf(rs.get("ukuranbaju")));
				dl.setUkurancelana(String.valueOf(rs.get("ukurancelana")));
				dl.setUkuransepatu(String.valueOf(rs.get("ukuransepatu")));
				dl.setTglbergabung(String.valueOf(rs.get("tglbergabung")));
				dl.setBank(String.valueOf(rs.get("bank")));
				dl.setNorek(String.valueOf(rs.get("norek")));
				dl.setRekatasnama(String.valueOf(rs.get("rekatasnama")));
				dl.setHpkerabat(String.valueOf(rs.get("hpkerabat")));
				dl.setStatusaktif(String.valueOf(rs.get("statusaktif")));
				dl.setStatuskerja(String.valueOf(rs.get("statuskerja")));
				dl.setMulaikontrak(String.valueOf(rs.get("mulaikontrak")));
				dl.setFoto(String.valueOf(rs.get("foto")));
				dl.setTglinput(String.valueOf(rs.get("tglinput")));
				dl.setTglubah(String.valueOf(rs.get("tglubah")));
				dl.setUseraktif(String.valueOf(rs.get("useraktif")));
				dl.setIduser(String.valueOf(rs.get("iduser")));
				dl.setDlt(String.valueOf(rs.get("dlt")));
				dl.setKodemesin(String.valueOf(rs.get("kodemesin")));
				list.add(dl);
			}


		}
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),id_karyawan);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),id_karyawan);
		}
		finally {
		}
		return list;
	}
	
	public static Object get_bawahan(String kode_jabatan) {
		List<KaryawanBawahan> list = new ArrayList<KaryawanBawahan>();
		
		try {
			String sql = "CekBawahan ? ";
			
			List res = DbConnection.executeReader(DbConnection.HRIS, sql, new Object[] {new BigDecimal(kode_jabatan)});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				
				KaryawanBawahan dl = new KaryawanBawahan();				
				dl.setIdkaryawan(String.valueOf(rs.get("idkaryawan")));
				dl.setNama(String.valueOf(rs.get("nama")));
				dl.setJabatan(String.valueOf(rs.get("jabatan")));
				dl.setDepartment(String.valueOf(rs.get("department")));
				dl.setHp(String.valueOf(rs.get("hp")));
				dl.setEmail(String.valueOf(rs.get("email")));
				dl.setFoto(String.valueOf(rs.get("foto")));
				dl.setKelamin(String.valueOf(rs.get("kelamin")));
				dl.setStatusaktif(String.valueOf(rs.get("statusaktif")));
				dl.setTglbergabung(String.valueOf(rs.get("tglbergabung")));
				list.add(dl);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		finally {
		}
		return list;
	}
	
	public static Object konfirmasi_izin_cuti(String id, String alasan, String status, String tipe, String idkaryawan, String kodepermohonan){
		
		ResponseStatus data = new ResponseStatus();

		Connection Connection = null;
		PreparedStatement ps=null, ps2=null, ps3=null, ps4=null, ps5=null, ps6=null, ps7=null, ps8=null, ps9=null, ps10=null, ps11=null, ps12=null, ps13=null, ps14=null, ps15=null, ps16=null, ps17=null, ps18=null, ps19=null, ps20=null, ps21=null, ps22=null, ps23=null, ps24=null, ps25=null, ps26=null, ps27=null, ps28=null, ps29=null, ps30=null, ps31=null ;
		ResultSet rs=null, rs2=null, rs3=null, rs4=null, rs5=null, rs6=null, rs7=null, rs8=null, rs9=null, rs10=null, rs11=null, rs12=null, rs13=null, rs14=null, rs15=null, rs16=null, rs17=null, rs18=null, rs19=null, rs20=null, rs21=null, rs22=null, rs23=null, rs24=null, rs25=null, rs26=null, rs27=null, rs28=null, rs29=null, rs30=null, rs31=null ;
		String kodeizin = null;
		
		Connection = DbConnection.getHrisInstance();
		
		try {
			Connection.setAutoCommit(false);
			if (Connection == null ) throw new Exception("Database failed to connect !");

			String sql = "UPDATE TblPermohonanIzinDetail SET Alasan = ?, Status = ? WHERE KodeIzinDetail = ? ";
			String sql2 = "SELECT KodeIzin FROM TblPermohonanIzinDetail WHERE KodeIzinDetail = ? ";
			String sql3 = "SELECT c.TipeIzin, c.TglPermohonan, c.Alasan, c.IDKaryawan, c.JamIzinMasuk, c.Lokasi, c.Dokumen3, c.JenisPotong, c.TglIzinMasuk, c.TglIzinKeluar "
					+ " FROM TblPermohonanIzin c WHERE c.KodeIzin = ? ";
			String sql4 = "SELECT d.FingerPrintID, d.KodeMesin, d.Nama, d.Email "
					+ " FROM TblKaryawan d WHERE d.IDKaryawan = ? ";
			String sql5 = "UpdatePC_Telat ?,?,?,?,? ";
			String sql6 = "CekShiftAday ?,?,? ";
			String sql7 = "UpdatePC_PulangCepat ?,?,?,?,? ";
      			String sql8 = "UpdateSaldoCutiFromIzin ?,?,? ";
			String sql9 = "GetTanggalCuti ?,?,?,? ";
			String sql10 = "UpdatePC ?,?,?,?,? ";
			
			String sql11 = "UPDATE TblCutiDetail SET Alasan = ?, Status = ?, Notifikasi = 1, TglUbah = SYSDATETIME() WHERE KodeCutiDetail = ? ";
			String sql12 = "SELECT level FROM TblCutiDetail WHERE KodeCutiDetail = ? ";
			String sql13 = "SELECT c.JenisCuti, c.InfoTambahan, c.TglMulai, c.TglAkhir, c.IDKaryawan FROM TblCuti c WHERE c.KodeCuti = ? ";
			String sql14 = "SELECT MAX(d.Level) as max FROM TblCutiDetail d WHERE d.KodeCuti = ?";
			String sql15 = "SELECT COUNT(d.Status) as count FROM TblCutiDetail d WHERE d.KodeCuti = ? AND d.Level = ? AND d.Status = 'Approved'";
			String sql16 = "UpdateSaldoCuti ?,?";
			String sql17 = "GetTanggalCuti ?,?,?,?";
			String sql18 = "UpdatePC ?,?,?,?,? ";
			String sql19 = "UpdateNotifikasiTugas ? ";
			String sql20 = "SELECT IDKaryawan FROM TblCutiDetail WHERE KodeCuti = ? And Status = 'Progressing' ";
			String sql21 = "SELECT Email FROM TblKaryawan WHERE IDKaryawan = ? ";
			String sql22 = "UPDATE TblSerahTerimaTugas SET Status =  ?, TglUbah = SYSDATETIME() WHERE KodeSerahTugas = ? ";
			String sql23 = "UPDATE TblSPPDDetail SET Status =  ?, TglUbah = SYSDATETIME() WHERE KodeSPPDDetail = ? ";
			String sql24 = "SELECT * FROM TblSPPD WHERE KodeSPPD = ? ";
			String sql25 = "SELECT MAX(d.Level) as max FROM TblSPPDDetail d WHERE d.KodeSPPD  = ? AND d.Dlt = 0 ";
			String sql26 = "SELECT COUNT(d.Status) as count FROM TblSPPDDetail  d WHERE d.KodeSPPD = ? AND d.Level = ? AND d.Status = 'Approved'";
			String sql27 = "SELECT level FROM TblSPPDDetail WHERE KodeSPPDDetail = ? ";
			String sql28 = "SELECT d.FingerPrintID, d.KodeMesin FROM TblKaryawan d WHERE d.IDKaryawan  = ? ";
			String sql29 = "GetTanggalCuti ?,?,?,? ";
			String sql30 = "UpdatePC ?,?,?,?,? ";
			String sql31 = "UPDATE TblSPPDDetail SET Notifikasi = ? WHERE KodeSPPD = ? AND Status = 'Progressing' ";
			
			//data karyawan
			ps4 = Connection.prepareStatement(sql4);
			ps4 = CommonUtil.initialStatement(ps4);
			ps4.setString(1, idkaryawan);
			rs4 = ps4.executeQuery();
			if(rs4.next()){}
			
			
			
			String PersonalCalendarStatus  = "";
			if (tipe.trim().equals("Permission Request") || tipe.trim().equals("Permohonan Izin")) {
				ps = Connection.prepareStatement(sql);
				ps = CommonUtil.initialStatement(ps);
				ps.setString(1, alasan);
				ps.setString(2, status);
				ps.setInt(3, Integer.parseInt(id));
				ps.execute();
			
				if (status.trim().equals("Approved")) {
					
					ps2 = Connection.prepareStatement(sql2);
					ps2 = CommonUtil.initialStatement(ps2);
					ps2.setInt(1, Integer.parseInt(id));
					rs2 = ps2.executeQuery();
				
					if (rs2.next()) {
						//data izin
						kodeizin = rs2.getString("KodeIzin");
						
						ps3 = Connection.prepareStatement(sql3);
						ps3 = CommonUtil.initialStatement(ps3);
						ps3.setInt(1, Integer.parseInt(kodeizin));
						rs3 = ps3.executeQuery();
						
						if(rs3.next()){
							
							ps6 = Connection.prepareStatement(sql6);
							ps6 = CommonUtil.initialStatement(ps6);
							ps6.setInt(1, Integer.parseInt(rs4.getString("FingerPrintID")));
							ps6.setString(2, rs3.getString("TglIzinKeluar"));
							ps6.setInt(3, Integer.parseInt(rs4.getString("kodemesin")));
							rs6 = ps6.executeQuery();
							
							if(rs6.next()){
								if (rs3.getString("Lokasi").equals("Half Time")) {
									if (rs3.getString("TipeIzin").equals("Late")) {
										ps5 = Connection.prepareStatement(sql5);
										ps5 = CommonUtil.initialStatement(ps5);
										ps5.setString(1, rs3.getString("Alasan"));
										ps5.setString(2, rs6.getString("JamMulai"));
										ps5.setString(3, rs4.getString("FingerPrintID"));
										ps5.setString(4, rs3.getString("TglIzinKeluar"));
										ps5.setString(5, rs4.getString("KodeMesin"));
										ps5.execute();
									}
									
									if (!rs3.getString("Lokasi").equals("Late") && !rs3.getString("Lokasi").equals("Hourly")) {
										ps7 = Connection.prepareStatement(sql7);
										ps7 = CommonUtil.initialStatement(ps7);
										ps7.setString(1, rs3.getString("Alasan"));
										ps7.setString(2, rs6.getString("JamPulang"));
										ps7.setString(3, rs4.getString("FingerPrintID"));
										ps7.setString(4, rs3.getString("TglIzinKeluar"));
										ps7.setString(5, rs4.getString("KodeMesin"));
										ps7.execute();
									}
								}else if(rs3.getString("Lokasi").equals("Full Time")){
									if (!rs3.getString("TipeIzin").equals("Late") && !rs3.getString("TipeIzin").equals("Hourly")) {
										if (rs3.getString("TipeIzin").equals("Sick")) {
											PersonalCalendarStatus ="45";
										} else if (rs3.getString("TipeIzin").equals("Personal Matter")) {
											if(rs4.getInt("KodeMesin") == 1) {
												PersonalCalendarStatus ="56";
											} else if(rs4.getInt("KodeMesin") == 2) {
												PersonalCalendarStatus = "50";
											}
										} else if (rs3.getString("TipeIzin").equals("Daily Office Duty")) {
											PersonalCalendarStatus = "49";
										}
									}
									
									if (rs3.getString("JenisPotong").equals("cuti") || rs3.getString("JenisPotong").equals("cuti dan gaji")) {
										ps8 = Connection.prepareStatement(sql8);
										ps8 = CommonUtil.initialStatement(ps8);
										ps8.setString(1, rs3.getString("IDKaryawan"));
										ps8.setString(2, rs2.getString("KodeIzin"));
										ps8.setBigDecimal(3, new BigDecimal(rs3.getString("Dokumen3")));
										ps8.execute();
									}
									
									ps9 = Connection.prepareStatement(sql9);
									ps9 = CommonUtil.initialStatement(ps9);
									ps9.setInt(1, Integer.parseInt(rs4.getString("FingerPrintID")));
									ps9.setString(2, rs3.getString("TglIzinKeluar"));
									ps9.setString(3, rs3.getString("TglIzinMasuk"));
									ps9.setInt(4, Integer.parseInt(rs4.getString("kodemesin")));
									rs9 = ps9.executeQuery();
									
									while(rs9.next()){
										ps10 = null ; rs10 = null;
										
										ps10 = Connection.prepareStatement(sql10);
										ps10 = CommonUtil.initialStatement(ps10);
										ps10.setString(1, PersonalCalendarStatus);
										ps10.setString(2, rs3.getString("Alasan"));
										ps10.setInt(3, Integer.parseInt(rs4.getString("FingerPrintID")));
										ps10.setString(4, rs9.getString("PersonalCalendarDate"));
										ps10.setInt(5, Integer.parseInt(rs4.getString("kodemesin")));
										ps10.execute();
										
										ps10.close();
										
									}
								}
								
							}else{
								throw new Exception("date must lower than today");
							}
					
						}
					}
				}
				data.setSuccess(true);
				
			} else if (tipe.trim().equals("Leave Request") || tipe.trim().equals("Permohonan Cuti")){
				ps11 = Connection.prepareStatement(sql11);
				ps11 = CommonUtil.initialStatement(ps11);;
				ps11.setString(1, alasan);
				ps11.setString(2, status);
				ps11.setBigDecimal(3, new BigDecimal(id));
				ps11.execute();
				
				if (status.trim().equals("Approved")) {
					//cutidetail
					ps12 = Connection.prepareStatement(sql12);
					ps12 = CommonUtil.initialStatement(ps12);
					ps12.setBigDecimal(1, new BigDecimal(id));
					rs12 = ps12.executeQuery();
					
					if (rs12.next()) {
						//tbl cuti
						ps13 = Connection.prepareStatement(sql13);
						ps13 = CommonUtil.initialStatement(ps13);
						ps13.setBigDecimal(1, new BigDecimal(kodepermohonan));
						rs13 = ps13.executeQuery();
						
						String tgl_mulai13 = "";
						String tgl_akhir13 = "";
						if (rs13.next()) {
							tgl_mulai13 = rs13.getString("TglMulai");
							tgl_akhir13 = rs13.getString("TglAkhir");
						}
						
						ps14 = Connection.prepareStatement(sql14);
						ps14 = CommonUtil.initialStatement(ps14);
						ps14.setBigDecimal(1, new BigDecimal(kodepermohonan));
						rs14 = ps14.executeQuery();
						
						if (rs14.next() ) {	
							ps15 = Connection.prepareStatement(sql15);
							ps15 = CommonUtil.initialStatement(ps15);
							ps15.setInt(1, Integer.parseInt(kodepermohonan));
							ps15.setString(2, rs14.getString("max"));
							rs15 = ps15.executeQuery();							
							
							if (rs15.next()) {				

								int a = rs15.getInt("count");
								int b = rs14.getInt("max");
								int c = rs12.getInt("level");
								
								if (rs15.getInt("count")>0 && rs14.getInt("max")==rs12.getInt("level")) {
									if (!rs13.getString("JenisCuti").equals("Unpaid Leave")) {
										ps16 = Connection.prepareStatement(sql16);
										ps16 = CommonUtil.initialStatement(ps16);
										ps16.setString(1, idkaryawan);
										ps16.setString(2, kodepermohonan);
										ps16.execute();
									}
									if (rs13.getString("JenisCuti").equals("Maternity") || rs13.getString("JenisCuti").equals("Hamil")) {
										PersonalCalendarStatus = "46";
									}else{
										PersonalCalendarStatus = "47";
									}
										
									//dataTanggal 
									ps17 = Connection.prepareStatement(sql17);
									ps17 = CommonUtil.initialStatement(ps17);
									ps17.setString(1,rs4.getString("FingerPrintID"));
									ps17.setString(2, rs13.getString("TglMulai"));
									ps17.setString(3, rs13.getString("TglAkhir"));
									ps17.setString(4, rs4.getString("KodeMesin"));
									rs17 = ps17.executeQuery();
									
									while (rs17.next()) {
										ps18 = null ; rs18 = null;
										
										ps18 = Connection.prepareStatement(sql18);
										ps18 = CommonUtil.initialStatement(ps18);
										ps18.setString(1, PersonalCalendarStatus);
										ps18.setString(2, rs13.getString("InfoTambahan"));
										ps18.setBigDecimal(3, new BigDecimal(rs4.getString("FingerPrintID")));
										ps18.setString(4, rs17.getString("PersonalCalendarDate"));
										ps18.setBigDecimal(5, new BigDecimal(rs4.getString("kodemesin")));
										ps18.execute();
										
										ps18.close();
									}
									
									ps19 = Connection.prepareStatement(sql19);
									ps19 = CommonUtil.initialStatement(ps19);
									ps19.setBigDecimal(1, new BigDecimal(kodepermohonan));
									ps19.execute();
									
								}else{
									//id atasan
									ps20 = Connection.prepareStatement(sql20);
									ps20 = CommonUtil.initialStatement(ps20);
									ps20.setBigDecimal(1, new BigDecimal(kodepermohonan));
									rs20 = ps20.executeQuery();
									String idatasan = (rs20.next()) ? rs20.getString("IDKaryawan"):null;
									
									//email atasan
									ps21 = Connection.prepareStatement(sql21);
									ps21 = CommonUtil.initialStatement(ps21);
									ps21.setString(1, idatasan);
									rs21 = ps21.executeQuery();
									String emailatasan = (rs21.next() && idatasan!=null) ? rs21.getString("Email"):null;
									
									String pesan = "ID Karyawan: " + idkaryawan + " <br />" +
											"Nama: " + rs4.getString("nama") + " <br />" +
											"Tipe: " + rs13.getString("JenisCuti") + " <br />" +
											"Tanggal: " + rs13.getString("TglMulai") + " - " + rs13.getString("TglAkhir") + " <br />" +
											"Alasan: " + rs13.getString("InfoTambahan");

								}
							  }
						}
					}
					data.setSuccess(true);
				}
			} else if (tipe.trim().equals("Task Request") || tipe.trim().equals("Permohonan Tugas")) {
				
				ps22 = Connection.prepareStatement(sql22);
				ps22 = CommonUtil.initialStatement(ps22);
				ps22.setString(1,status);
				ps22.setString(2, id);
				ps22.executeUpdate();
				
			} else if (tipe.trim().equals("SPPD")) {
				
				ps23 = Connection.prepareStatement(sql23);
				ps23 = CommonUtil.initialStatement(ps23);
				ps23.setString(1,status);
				ps23.setString(2, id);
				ps23.executeUpdate();
				
				if (status.trim().equals("Approved")) {
					//datasppd
					ps24 = Connection.prepareStatement(sql24);
					ps24 = CommonUtil.initialStatement(ps24);
					ps24.setBigDecimal(1, new BigDecimal(kodepermohonan));
					rs24 = ps24.executeQuery();
					rs24.next();
					
					//maxlevel
					ps25 = Connection.prepareStatement(sql25);
					ps25 = CommonUtil.initialStatement(ps25);
					ps25.setBigDecimal(1, new BigDecimal(kodepermohonan));
					rs25 = ps25.executeQuery();
					rs25.next();
					int max = rs25.getInt("max");
					
					//cekstatusall
					ps26 = Connection.prepareStatement(sql26);
					ps26 = CommonUtil.initialStatement(ps26);
					ps26.setBigDecimal(1, new BigDecimal(kodepermohonan));
					ps26.setInt(2, max);
					rs26 = ps26.executeQuery();	
					rs26.next();
					int cekstatusall = rs26.getInt("count");
					
					ps27 = Connection.prepareStatement(sql27);
					ps27 = CommonUtil.initialStatement(ps27);
					ps27.setBigDecimal(1, new BigDecimal(id));
					rs27 = ps27.executeQuery();
					rs27.next();
					int level = rs27.getInt("level");
					
					if (cekstatusall > 0 && max == level) {
						//pelaksana
						ps28 = Connection.prepareStatement(sql28);
						ps28 = CommonUtil.initialStatement(ps28);
						ps28.setString(1, rs24.getString("IDPelaksana"));
						rs28 = ps28.executeQuery();
						
						if(rs28.next()){
							String fingerprintid = rs28.getString("FingerPrintID");
							String kodemesin = rs28.getString("KodeMesin");
							PersonalCalendarStatus = "49";

							
							ps29 = Connection.prepareStatement(sql29);
							ps29 = CommonUtil.initialStatement(ps29);
							ps29.setBigDecimal(1, new BigDecimal(fingerprintid));
							ps29.setString(2, rs24.getString("TglMulai"));
							ps29.setString(3, rs24.getString("TglAkhir"));
							ps29.setBigDecimal(4, new BigDecimal(rs28.getString("KodeMesin")));
							rs29 = ps29.executeQuery();
							
							while(rs29.next()){
								ps30 = null ; rs30 = null;
								
								ps30 = Connection.prepareStatement(sql30);
								ps30 = CommonUtil.initialStatement(ps30);
								ps30.setString(1, PersonalCalendarStatus);
								ps30.setString(2, rs24.getString("Tujuan"));
								ps30.setBigDecimal(3, new BigDecimal(fingerprintid));
								ps30.setString(4, rs29.getString("PersonalCalendarDate"));
								ps30.setBigDecimal(5, new BigDecimal(kodemesin));
								ps30.execute();
								
								ps30.close();
								
							}
						}
						
						ps31 = Connection.prepareStatement(sql31);
						ps31 = CommonUtil.initialStatement(ps31);
						ps31.setBoolean(1,true);
						ps31.setString(2, kodepermohonan);
						ps31.executeUpdate();
						
						
					}
				}
				data.setSuccess(true);
			}
			//throw new IllegalArgumentException("Testing dev, dont commit");
			Connection.commit();
		}
	
		catch (SQLException e){
			System.err.println(e.getMessage());
			logError(e.getMessage(),idkaryawan);
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logError(e.getMessage(),idkaryawan);
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
			if (ps3 != null) try { ps3.close(); } catch (SQLException e) {}
			if (rs3 != null) try { rs3.close(); } catch (SQLException e) {}
			if (ps4 != null) try { ps4.close(); } catch (SQLException e) {}
			if (rs4 != null) try { rs4.close(); } catch (SQLException e) {}
			if (ps5 != null) try { ps5.close(); } catch (SQLException e) {}
			if (rs5 != null) try { rs5.close(); } catch (SQLException e) {}
			if (ps6 != null) try { ps6.close(); } catch (SQLException e) {}
			if (rs6 != null) try { rs6.close(); } catch (SQLException e) {}
			if (ps7 != null) try { ps7.close(); } catch (SQLException e) {}
			if (rs7 != null) try { rs7.close(); } catch (SQLException e) {}
			if (ps8 != null) try { ps8.close(); } catch (SQLException e) {}
			if (rs8 != null) try { rs8.close(); } catch (SQLException e) {}
			if (ps9 != null) try { ps9.close(); } catch (SQLException e) {}
			if (rs9 != null) try { rs9.close(); } catch (SQLException e) {}
			if (ps10 != null) try { ps10.close(); } catch (SQLException e) {}
			if (rs10 != null) try { rs10.close(); } catch (SQLException e) {}
			if (ps11 != null) try { ps11.close(); } catch (SQLException e) {}
			if (rs11 != null) try { rs11.close(); } catch (SQLException e) {}
			if (ps12 != null) try { ps12.close(); } catch (SQLException e) {}
			if (rs12 != null) try { rs12.close(); } catch (SQLException e) {}
			if (ps13 != null) try { ps13.close(); } catch (SQLException e) {}
			if (rs13 != null) try { rs13.close(); } catch (SQLException e) {}
			if (ps14 != null) try { ps14.close(); } catch (SQLException e) {}
			if (rs14 != null) try { rs14.close(); } catch (SQLException e) {}
			if (ps15 != null) try { ps15.close(); } catch (SQLException e) {}
			if (rs15 != null) try { rs15.close(); } catch (SQLException e) {}
			if (ps16 != null) try { ps16.close(); } catch (SQLException e) {}
			if (rs16 != null) try { rs16.close(); } catch (SQLException e) {}
			if (ps17 != null) try { ps17.close(); } catch (SQLException e) {}
			if (rs17 != null) try { rs17.close(); } catch (SQLException e) {}
			if (ps18 != null) try { ps18.close(); } catch (SQLException e) {}
			if (rs18 != null) try { rs18.close(); } catch (SQLException e) {}
			if (ps19 != null) try { ps19.close(); } catch (SQLException e) {}
			if (rs19 != null) try { rs19.close(); } catch (SQLException e) {}
			if (ps20 != null) try { ps20.close(); } catch (SQLException e) {}
			if (rs20 != null) try { rs20.close(); } catch (SQLException e) {}
			if (ps21 != null) try { ps21.close(); } catch (SQLException e) {}
			if (rs21 != null) try { rs21.close(); } catch (SQLException e) {}
			if (ps22 != null) try { ps22.close(); } catch (SQLException e) {}
			if (rs22 != null) try { rs22.close(); } catch (SQLException e) {}
			if (ps23 != null) try { ps23.close(); } catch (SQLException e) {}
			if (rs23 != null) try { rs23.close(); } catch (SQLException e) {}
			if (ps24 != null) try { ps24.close(); } catch (SQLException e) {}
			if (rs24 != null) try { rs24.close(); } catch (SQLException e) {}
			if (ps25 != null) try { ps25.close(); } catch (SQLException e) {}
			if (rs25 != null) try { rs25.close(); } catch (SQLException e) {}
			if (ps26 != null) try { ps26.close(); } catch (SQLException e) {}
			if (rs26 != null) try { rs26.close(); } catch (SQLException e) {}
			if (ps27 != null) try { ps27.close(); } catch (SQLException e) {}
			if (rs27 != null) try { rs27.close(); } catch (SQLException e) {}
			if (ps28 != null) try { ps28.close(); } catch (SQLException e) {}
			if (rs28 != null) try { rs28.close(); } catch (SQLException e) {}
			if (ps29 != null) try { ps29.close(); } catch (SQLException e) {}
			if (rs29 != null) try { rs29.close(); } catch (SQLException e) {}
			if (ps30 != null) try { ps30.close(); } catch (SQLException e) {}
			if (rs30 != null) try { rs30.close(); } catch (SQLException e) {}
			if (ps31 != null) try { ps31.close(); } catch (SQLException e) {}
			if (rs31 != null) try { rs31.close(); } catch (SQLException e) {}
		}
		
		return data;
		
	}
	
	public static int HitungHariCuti(String id_karyawan ,String startdate, String enddate){
		int ret = 0;

		Connection Connection = null;
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null, rs2 = null;
		
		try {
			Connection = DbConnection.getHrisInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			String sql = "SELECT  KodeMesin, FingerPrintID FROM TblKaryawan "
					+ " WHERE IDKaryawan = ? ";
			String sql2 = "HitungHariCuti ?,?,?,? ";
			
			ps = Connection.prepareStatement(sql);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, id_karyawan);
			rs = ps.executeQuery();
			
			if (rs.next()){
				BigDecimal FingerID = new BigDecimal(rs.getString("fingerprintid"));
				int KodeMesin = rs.getInt("KodeMesin");

				ps2 = Connection.prepareStatement(sql2);
				ps2 = CommonUtil.initialStatement(ps2);
				ps2.setBigDecimal(1, FingerID);
				ps2.setString(2, startdate);
				ps2.setString(3, enddate);
				ps2.setInt(4, KodeMesin);
				rs2 = ps2.executeQuery();
				
				if (rs2.next()) {
					ret = rs2.getInt("haricuti");
				}
			}
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
			logError(e.getMessage(),id_karyawan);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
			logError(e.getMessage(),id_karyawan);
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
		}
		return ret;
	}
	
	public static BigDecimal GetEmoney(String nik){
		BigDecimal ret = new BigDecimal(0);
		
		String sql = "select balance from t_accountbalance where nik=? and status = 1 and deleted = 0;";
		
		try {
			
			List res = DbConnection.executeReader(DbConnection.TRX, sql, new Object[] {nik});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				ret = new BigDecimal(String.valueOf(rs.get("balance")));
				
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
        }
		return ret;
	}
	
	public static Object get_request_approval(String id_karyawan, String status) {
		List<RequestApproval> list = new ArrayList<RequestApproval>();
		
		try {
			
			String sql = "MOBILE_DataPermohonanDetail ?,? ";
			
			List res = DbConnection.executeReader(DbConnection.HRIS, sql, new Object[] {id_karyawan,status});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				
				RequestApproval dl = new RequestApproval();
				dl.setIdkaryawan(String.valueOf(rs.get("idkaryawan")));
				dl.setFingerprintid(String.valueOf(rs.get("fingerprintid")));
				dl.setKeterangan(String.valueOf(rs.get("keterangan")));
				dl.setTanggal(String.valueOf(rs.get("tanggal")));
				dl.setNama(String.valueOf(rs.get("nama")));
				dl.setKode(String.valueOf(rs.get("kode")));
				dl.setAction(String.valueOf(rs.get("action")));
				dl.setController(String.valueOf(rs.get("controller")));
				dl.setLamaexpired(String.valueOf(rs.get("lamaexpired")));
				dl.setTipe(String.valueOf(rs.get("tipe")));
				dl.setStatusall(String.valueOf(rs.get("statusall")));
				dl.setIdrequester(String.valueOf(rs.get("idrequester")));
				dl.setKodedetail(String.valueOf(rs.get("kodedetail")));
				dl.setLevel(String.valueOf(rs.get("level")));
				dl.setStatus(String.valueOf(rs.get("status")));
				dl.setNotifikasi(String.valueOf(rs.get("notifikasi")));
				dl.setAlasan(String.valueOf(rs.get("alasan")));
				dl.setTgl_keluar(String.valueOf(rs.get("tgl_keluar")));
				dl.setTgl_masuk(String.valueOf(rs.get("tgl_masuk")));
				dl.setJam_keluar(String.valueOf(rs.get("jam_keluar")));
				dl.setJam_masuk(String.valueOf(rs.get("jam_masuk")));
				list.add(dl);			
			}
		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		finally {
		}
		return list;
	}
	
	public static Object get_berita(String reader_role) {
		List<Berita> list = new ArrayList<Berita>();
		
		try {
			String sql = "SELECT berita_id, judul, tgl, isi "
					+ "FROM tb_berita WHERE tgl BETWEEN ? AND ?  ";
			
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
	        Calendar cal = Calendar.getInstance();
	        String dateTo = sdf.format(cal.getTime());
	        cal.add(Calendar.DATE, -30);
	        String dateFrom = sdf.format(cal.getTime());
			
			List res = DbConnection.executeReader(DbConnection.BPJSONLINE, sql, new Object[] {dateFrom,dateTo});
			for (int q = 0; q < res.size(); q++) {
				TreeMap<String, String> rs = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);  rs.putAll((HashMap) res.get(q));
				
				Berita dl = new Berita();
				dl.setBerita_id(String.valueOf(rs.get("berita_id")));
				dl.setJudul(String.valueOf(rs.get("judul")));
				dl.setTanggal(String.valueOf(rs.get("tgl")));
				dl.setSimple_berita(String.valueOf(rs.get("isi")));
				
				list.add(dl);
			}

		}
		catch (SQLException e){
			System.err.println(e.getMessage());
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		finally {
		}
		return list;
	}
	
	public static UserLoginView get_login_hris(String username, String password) {
		
		UserLoginView view = new UserLoginView();
		Connection Connection = null;
		PreparedStatement ps = null, ps1 = null;
		ResultSet rs = null, rs1 = null;
		
		try {			
						
			Connection = DbConnection.getHrisInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			String pass =  EncryptUtil.getInstance().StringtoMD5(Encryptor.getInstance().decrypt(password));
			String selectQuery = " SELECT usr.Username, usr.Password, usr.IDKaryawan, usr.LastLogin, usr.Aktif "
					+ "FROM TblUsers usr "
					+ " where usr.Username = ?  and usr.Password = ?  and usr.Aktif = 1 ";
			
			ps = Connection.prepareStatement(selectQuery);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, username);
			ps.setString(2, pass);
			rs = ps.executeQuery();
			if(rs.next()){
				view.setPasswordExpiryDate(rs.getDate("LastLogin"));
				view.setUserCode(rs.getString("IDKaryawan"));
				view.setUserName(rs.getString("Username"));
				String nik = rs.getString("IDKaryawan");
				long userid =  Integer.parseInt(nik.replaceAll("[^0-9]", ""));;
				String user = rs.getString("Username");
				String sql1 = "SELECT rl.RoleID, rl.RoleName FROM TblUsersRole rl WHERE rl.Username = ? ";
				
				ps1 = Connection.prepareStatement(sql1);
				ps1 = CommonUtil.initialStatement(ps1);
				ps1.setString(1, user);
				rs1 = ps1.executeQuery();
				
				List<CodeDescView> locsview = new ArrayList<CodeDescView>();
				List<CodeDescView> roleview = new ArrayList<CodeDescView>();
				while (rs1.next()){
					CodeDescView location = new CodeDescView();
					location.setId(rs1.getBigDecimal("RoleID"));
					location.setCode(rs1.getString("RoleName"));
					locsview.add(location);
					
					CodeDescView v = new CodeDescView();
					v.setId(rs1.getBigDecimal("RoleID"));
					v.setCode(rs1.getString("RoleName"));
					roleview.add(v);
				}
				view.setUsermstrId(userid);
				view.setLocations(locsview);
				view.setRoles(roleview);
			}
		}
		
		catch (SQLException e){
			System.err.println(e.getMessage());
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps1 != null) try { ps1.close(); } catch (SQLException e) {}
			if (rs1 != null) try { rs1.close(); } catch (SQLException e) {}
		}
		return view;
	}
	
	public static Object add_kotak_saran(String user_id, String user_name, String project_code, String version,
			String saran, String kontak) {
		ResponseStatus data=new ResponseStatus();

		Connection connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		
		String sql = "INSERT INTO tb_kotaksaran(user_id,user_name,project_code,version,saran,created_at, kontak) "
				+ "VALUES(?,?,?,?,?,SYSDATETIME(), ?) ";
		
		try {
				ps = connection.prepareStatement(sql);
				ps = CommonUtil.initialStatement(ps);
				ps.setString(1, user_id);
				ps.setString(2, user_name);
				ps.setString(3, project_code);
				ps.setString(4, version);
				ps.setString(5, saran);
				ps.setString(6, kontak);
				ps.executeUpdate();
				
				data.setSuccess(true);
				data.setMessage("Terimakasih atas saran yang diberikan, saran anda telah tersampaikan.");
		}
		
		catch (SQLException e)
		{
			data.setSuccess(false);
			data.setMessage("Terjadi kesalahan server, coba lagi nanti.");
			e.fillInStackTrace();
			System.out.println(e.getMessage());
		}
		finally
		{	
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}

		}
		return data;
	}
	
	public static ResponseString authPass(){
		return null;
	}

	public static Object logError(String messageError, String IDKaryawan) {
		ResponseStatus data=new ResponseStatus();

		Connection connection=DbConnection.getHrisInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql = "INSERT INTO TblHistory(Error,IDUser,status,Tanggal) "
				+ "VALUES(?,?,0,SYSDATETIME()) ";
		
		try {
				ps = connection.prepareStatement(sql);
				ps = CommonUtil.initialStatement(ps);
				ps.setString(1, messageError);
				ps.setString(2, IDKaryawan);
				ps.executeUpdate();
				
				data.setSuccess(true);
				data.setMessage("OK");
		}
		
		catch (SQLException e)
		{
			data.setSuccess(false);
			data.setMessage("Terjadi kesalahan server, coba lagi nanti.");
			e.fillInStackTrace();
			System.out.println(e.getMessage());
		}
		finally
		{	
			 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}

		}
		return data;
	}
}
