package com.rsmurniteguh.webservice.dep.kthis.services;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.RisAdmissionList;
import com.rsmurniteguh.webservice.dep.all.model.RisReportList;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.PatientVisit;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class RisReportSer {

public static List<RisReportList> getrisreport(String perstartdate) 
{
	String tahun = perstartdate.substring(0, 4);
	String bulan = perstartdate.substring(4, 6);
	String hari = perstartdate.substring(6, 8);
	String combined = hari+"-"+bulan+"-"+tahun;
	
	
		List<RisReportList> all=new ArrayList<RisReportList>();
//		try {
//			DbConnection.getRisInstance().close();
//		} catch (SQLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		Connection risConnection=DbConnection.getRisInstance();
//		if(risConnection == null) return null;
//		Statement rislist=null;
//		String abris="select ADMISSION_ID, PATIENT_ID, PATIENT_SOURCE, MODALITY, EXAM_CODE, PERFRMD_ITEM, PATIENT_NAME, COMPLETION_FLAG "
//				+ "from RIS_WORKLIST where PERFRMD_START_DATE = '"+perstartdate+"'";
		
		
		Connection hisConnection=DbConnection.getPooledConnection();
		Statement hislist=null;
		ResultSet hashis=null;
		String abhis="select oe.VISIT_ID, c.card_no as patient_id, oe.ORDERENTRY_ID, oei.ORDERENTRYITEM_ID, oe.ENTERED_DATETIME, lm.LOCATION_DESC, "
				+ "p2.PERSON_NAME, iscm.item_subcat_desc, tcm.txn_code, tcm.TXN_DESC, "
				+ "oe.REMARKS, (case v.patient_type when 'PTY1' then 'IP' when 'PTY2' then 'OP' end) as patient_source, oe.order_status, pgcommon.fxGetCodeDesc(v.PATIENT_CLASS) as PATIENT_CLASS, "
				+ "pgcommon.fxGetCodeDesc(p2.SEX) as SEX, p2.BIRTH_DATE, p.person_name as REQUEST_PHYSICIAN, er.MEDICAL_DESC, er.DIAGNOSIS_DESC, dm.DIAGNOSIS_CODE "
				+ "from ORDERENTRY oe "
				+ "inner join VISIT v on v.visit_id = oe.visit_id "
				+ "inner join ORDERENTRYITEM oei on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID "
				+ "inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = oei.ORDERITEMMSTR_ID "
				+ "inner join ITEMSUBCATEGORYMSTR iscm on iscm.ITEMSUBCATEGORYMSTR_ID = oim.ITEMSUBCATEGORYMSTR_ID "
				+ "inner join ITEMCATEGORYMSTR icm on icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID "
				+ "inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = oe.LOCATIONMSTR_ID "
				+ "inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID "
				+ "left join EXAMINATIONREQUISITION er on er.orderentry_id = oe.orderentry_id "
				+ "inner join patient pt on pt.patient_id = v.patient_id "
				+ "inner join person p2 on p2.person_id = pt.person_id "
				+ "inner join card c on c.person_id =  pt.person_id "
				+ "inner join person p on p.PERSON_ID = cp.PERSON_ID "
				+ "inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID "
				+ "left outer join DIAGNOSISMSTR dm on dm.DIAGNOSISMSTR_ID = er.DIAGNOSISMSTR_ID "
				+ "where icm.ITEM_TYPE = 'ITY7' "
//				+ "and oe.order_status not in ('OSTSTO', 'OSTCAN') "
				+ "and oe.entered_datetime between to_date('"+combined+"', 'dd-MM-yyyy HH24:mi:ss') and "
				+ "to_date('"+combined+" 23:59:59', 'dd-MM-yyyy HH24:mi:ss') "
				+ "order by oe.ENTERED_DATETIME";
		
		
		try{
//			rislist=risConnection.createStatement();
//			ResultSet hasris=rislist.executeQuery(abris);
//			while(hasris.next())
//			{
//				RisReportList rr=new RisReportList();
//				rr.setAdmission_id(hasris.getString("ADMISSION_ID"));
//				rr.setPatient_source(hasris.getString("PATIENT_SOURCE"));
//				rr.setModality(hasris.getString("MODALITY"));
//				rr.setExam_code(hasris.getString("EXAM_CODE"));
//				rr.setPerfrmd_item(hasris.getString("PERFRMD_ITEM"));
//				rr.setPatient_name(hasris.getString("PATIENT_NAME"));
//				rr.setPatient_id(hasris.getString("PATIENT_ID"));
//				rr.setCompletion_flag(hasris.getString("COMPLETION_FLAG"));
//				all.add(rr);
//			}
//			hasris.close();
//			rislist.close();
			
			hislist=hisConnection.createStatement();
			hashis=hislist.executeQuery(abhis);
			while(hashis.next())
			{
				RisReportList rr=new RisReportList();
				rr.setAdmission_id(hashis.getString("ORDERENTRYITEM_ID"));
				rr.setPatient_source(hashis.getString("PATIENT_SOURCE"));
				rr.setModality(hashis.getString("item_subcat_desc"));
				rr.setExam_code(hashis.getString("txn_code"));
				rr.setPerfrmd_item(hashis.getString("TXN_DESC"));
				rr.setPatient_name(hashis.getString("PERSON_NAME"));
				rr.setPatient_id(hashis.getString("PATIENT_ID"));
				rr.setOrder_status(hashis.getString("order_status"));
				rr.setPATIENT_CLASS(hashis.getString("PATIENT_CLASS"));
				rr.setSEX(hashis.getString("SEX"));
				rr.setBIRTH_DATE(hashis.getString("BIRTH_DATE"));
				rr.setREQUEST_PHYSICIAN(hashis.getString("REQUEST_PHYSICIAN"));
				rr.setCompletion_flag("");
				rr.setMEDICAL_DESC(hashis.getString("MEDICAL_DESC"));
				rr.setDIAGNOSIS_DESC(hashis.getString("DIAGNOSIS_DESC"));
				rr.setCreated_date(hashis.getString("ENTERED_DATETIME"));
				rr.setDiagnosis_Code(hashis.getString("DIAGNOSIS_CODE"));
				all.add(rr);
			}
			hashis.close();
			hislist.close();	
			
						
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (hislist!=null) try  { hislist.close(); } catch (Exception ignore){}
		     if (hashis!=null) try  { hashis.close(); } catch (Exception ignore){}
		     if (hisConnection!=null) try { hisConnection.close();} catch (Exception ignore){}
		   }
		return all;
	}

public static List<RisReportList> getxrayrisreport(String perstartdate) 
{
	String tahun = perstartdate.substring(0, 4);
	String bulan = perstartdate.substring(4, 6);
	String hari = perstartdate.substring(6, 8);
	String combined = hari+"-"+bulan+"-"+tahun;
		List<RisReportList> all=new ArrayList<RisReportList>();
		Connection risConnection=DbConnection.getPooledConnection();
		Statement rislist=null;
		ResultSet hasris=null;
		String abris="select oe.VISIT_ID,v.patient_id, oe.ORDERENTRY_ID, oei.ORDERENTRYITEM_ID, lm.LOCATION_DESC, "
				+ "p2.PERSON_NAME, iscm.item_subcat_desc, tcm.txn_code, tcm.TXN_DESC, "
				+ "oe.REMARKS, (case v.patient_type when 'PTY1' then 'IP' when 'PTY2' then 'OP' end) as patient_source, oe.order_status, pgcommon.fxGetCodeDesc(v.PATIENT_CLASS) as PATIENT_CLASS,  "
				+ "pgcommon.fxGetCodeDesc(p2.SEX) as SEX, p2.BIRTH_DATE, p.person_name as REQUEST_PHYSICIAN, er.MEDICAL_DESC, er.DIAGNOSIS_DESC "
				+ "from ORDERENTRY oe "
				+ "inner join VISIT v on v.visit_id = oe.visit_id "
				+ "inner join ORDERENTRYITEM oei on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID "
				+ "inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = oei.ORDERITEMMSTR_ID "
				+ "inner join ITEMSUBCATEGORYMSTR iscm on iscm.ITEMSUBCATEGORYMSTR_ID = oim.ITEMSUBCATEGORYMSTR_ID "
				+ "inner join ITEMCATEGORYMSTR icm on icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID "
				+ "left join EXAMINATIONREQUISITION er on er.orderentry_id = oe.orderentry_id "
				+ "inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = oe.LOCATIONMSTR_ID "
				+ "inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID "
				+ "inner join patient pt on pt.patient_id = v.patient_id "
				+ "inner join person p2 on p2.person_id = pt.person_id "
				+ "inner join person p on p.PERSON_ID = cp.PERSON_ID "
				+ "inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID "
				+ "where icm.ITEM_TYPE = 'ITY7' "
//				+ "and oe.order_status not in ('OSTSTO', 'OSTCAN') "
				+ "and oe.entered_datetime between to_date('"+combined+"', 'dd-MM-yyyy HH24:mi:ss') and "
				+ "to_date('"+combined+" 23:59:59', 'dd-MM-yyyy HH24:mi:ss') "
				+ "order by oe.ENTERED_DATETIME";
		try{
			rislist=risConnection.createStatement();
			hasris=rislist.executeQuery(abris);
			while(hasris.next())
			{
				RisReportList rr=new RisReportList();
				rr.setAdmission_id(hasris.getString("ORDERENTRYITEM_ID"));
				rr.setPatient_source(hasris.getString("PATIENT_SOURCE"));
				rr.setModality(hasris.getString("item_subcat_desc"));
				rr.setExam_code(hasris.getString("txn_code"));
				rr.setPerfrmd_item(hasris.getString("TXN_DESC"));
				rr.setPatient_name(hasris.getString("PERSON_NAME"));
				rr.setPatient_id(hasris.getString("PATIENT_ID"));
				rr.setOrder_status(hasris.getString("order_status"));
				rr.setPATIENT_CLASS(hasris.getString("PATIENT_CLASS"));
				rr.setSEX(hasris.getString("SEX"));
				rr.setBIRTH_DATE(hasris.getString("BIRTH_DATE"));
				rr.setREQUEST_PHYSICIAN(hasris.getString("REQUEST_PHYSICIAN"));
				rr.setCompletion_flag("");
				rr.setMEDICAL_DESC(hasris.getString("MEDICAL_DESC"));
				rr.setDIAGNOSIS_DESC(hasris.getString("DIAGNOSIS_DESC"));
				all.add(rr);
			}
			hasris.close();
			rislist.close();			
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (rislist!=null) try  { rislist.close(); } catch (Exception ignore){}
		     if (hasris!=null) try  { hasris.close(); } catch (Exception ignore){}
		     if (risConnection!=null) try { risConnection.close();} catch (Exception ignore){}
		   }
		return all;
	}

public static List<RisReportList> getRisReportBypatient(String patientName, String patientId) 
{
		List<RisReportList> all=new ArrayList<RisReportList>();
//		Connection risConnection=DbConnection.getRisInstance();
//		if(risConnection == null) return null;
//		Statement rislist=null;
//		String abris="select ADMISSION_ID,PATIENT_ID, PATIENT_SOURCE, MODALITY, EXAM_CODE, PERFRMD_ITEM, PATIENT_NAME, COMPLETION_FLAG "
//				+ "from RIS_WORKLIST where 1 = 1 ";
//		
//		if(patientName != null)
//		{
//			abris += "AND PATIENT_NAME like '%"+patientName.toUpperCase()+"%' ";
//		}
//		if(patientId != null)
//		{
//			abris += "AND patient_id like '%"+patientId.toUpperCase()+"%' ";
//		}
		 
		Connection hisConnection=DbConnection.getPooledConnection();
		Statement hislist=null;
		ResultSet hashis=null;
		String abhis="select oe.VISIT_ID, c.card_no as patient_id, oe.ORDERENTRY_ID, oei.ORDERENTRYITEM_ID, oe.ENTERED_DATETIME, lm.LOCATION_DESC, "
				+ "p2.PERSON_NAME, iscm.item_subcat_desc, tcm.txn_code, tcm.TXN_DESC, "
				+ "oe.REMARKS, (case v.patient_type when 'PTY1' then 'IP' when 'PTY2' then 'OP' end) as patient_source, oe.order_status, pgcommon.fxGetCodeDesc(v.PATIENT_CLASS) as PATIENT_CLASS,  "
				+ "pgcommon.fxGetCodeDesc(p2.SEX) as SEX, p2.BIRTH_DATE, p.person_name as REQUEST_PHYSICIAN, er.MEDICAL_DESC, er.DIAGNOSIS_DESC, dm.DIAGNOSIS_CODE "
				+ "from ORDERENTRY oe "
				+ "inner join VISIT v on v.visit_id = oe.visit_id "
				+ "inner join ORDERENTRYITEM oei on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID "
				+ "inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = oei.ORDERITEMMSTR_ID "
				+ "inner join ITEMSUBCATEGORYMSTR iscm on iscm.ITEMSUBCATEGORYMSTR_ID = oim.ITEMSUBCATEGORYMSTR_ID "
				+ "inner join ITEMCATEGORYMSTR icm on icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID "
				+ "left join EXAMINATIONREQUISITION er on er.orderentry_id = oe.orderentry_id "
				+ "inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = oe.LOCATIONMSTR_ID "
				+ "inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID "
				+ "inner join patient pt on pt.patient_id = v.patient_id "
				+ "inner join person p2 on p2.person_id = pt.person_id "
				+ "inner join card c on c.person_id =  pt.person_id "
				+ "inner join person p on p.PERSON_ID = cp.PERSON_ID "
				+ "inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID "
				+ "left outer join DIAGNOSISMSTR dm on dm.DIAGNOSISMSTR_ID = er.DIAGNOSISMSTR_ID "
				+ "where icm.ITEM_TYPE = 'ITY7' ";
//				+ "and oe.order_status not in ('OSTSTO', 'OSTCAN') ";
		
		if(patientName != null)
		{
			abhis += "AND P2.PERSON_NAME like '%"+patientName.toUpperCase()+"%' ";
		}
		if(patientId != null)
		{
			abhis += "AND c.card_no like '%"+patientId.toUpperCase()+"%' ";
		}
		
		
		try{
//			rislist=risConnection.createStatement();
//			ResultSet hasris=rislist.executeQuery(abris);
//			while(hasris.next())
//			{
//				RisReportList rr=new RisReportList();
//				rr.setAdmission_id(hasris.getString("ADMISSION_ID"));
//				rr.setPatient_source(hasris.getString("PATIENT_SOURCE"));
//				rr.setModality(hasris.getString("MODALITY"));
//				rr.setExam_code(hasris.getString("EXAM_CODE"));
//				rr.setPerfrmd_item(hasris.getString("PERFRMD_ITEM"));
//				rr.setPatient_name(hasris.getString("PATIENT_NAME"));
//				rr.setPatient_id(hasris.getString("PATIENT_ID"));
//				rr.setCompletion_flag(hasris.getString("COMPLETION_FLAG"));
//				all.add(rr);
//			}
//			
//			hasris.close();
//			rislist.close();
			
			hislist=hisConnection.createStatement();
			hashis=hislist.executeQuery(abhis);
			while(hashis.next())
			{
				RisReportList rr=new RisReportList();
				rr.setAdmission_id(hashis.getString("ORDERENTRYITEM_ID"));
				rr.setPatient_source(hashis.getString("PATIENT_SOURCE"));
				rr.setModality(hashis.getString("item_subcat_desc"));
				rr.setExam_code(hashis.getString("txn_code"));
				rr.setPerfrmd_item(hashis.getString("TXN_DESC"));
				rr.setPatient_name(hashis.getString("PERSON_NAME"));
				rr.setPatient_id(hashis.getString("PATIENT_ID"));
				rr.setOrder_status(hashis.getString("order_status"));
				rr.setPATIENT_CLASS(hashis.getString("PATIENT_CLASS"));
				rr.setSEX(hashis.getString("SEX"));
				rr.setBIRTH_DATE(hashis.getString("BIRTH_DATE"));
				rr.setREQUEST_PHYSICIAN(hashis.getString("REQUEST_PHYSICIAN"));
				rr.setCompletion_flag("");
				rr.setMEDICAL_DESC(hashis.getString("MEDICAL_DESC"));
				rr.setDIAGNOSIS_DESC(hashis.getString("DIAGNOSIS_DESC"));
				rr.setDiagnosis_Code(hashis.getString("DIAGNOSIS_CODE"));
				rr.setCreated_date(hashis.getString("ENTERED_DATETIME"));
				
				all.add(rr);
			}
			hashis.close();
			hislist.close();	
			
						
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (hislist!=null) try  { hislist.close(); } catch (Exception ignore){}
		     if (hashis!=null) try  { hashis.close(); } catch (Exception ignore){}
		     if (hisConnection!=null) try { hisConnection.close();} catch (Exception ignore){}
		   }
		return all;
	}

public static List<RisAdmissionList> getrisadmission(String admissionid) {
		List<RisAdmissionList> all=new ArrayList<RisAdmissionList>();
//		Connection risConnection=DbConnection.getRisInstance();
//		if(risConnection == null) return null;
//		Statement rislist=null;
//		String abris="select PATIENT_ID, PATIENT_NAME, PATIENT_BIRTH, PATIENT_AGE, PATIENT_SEX, MODALITY, REQUEST_PHYSICIAN, REQUEST_DATE, PERFRMD_START_DATE, PERFRMD_START_TIME, "
//				+ "REPORT_START_DATE, REPORT_START_TIME, REPORT_UID, PERFRMD_ITEM, BODY_PART, PATIENT_SOURCE, EXAM_CODE, ROOM_ID, ROOM_NAME, COMPLETION_FLAG "
//				+ "from RIS_WORKLIST "
//				+ "where ADMISSION_ID='"+admissionid+"'";
		
		Connection hisConnection=DbConnection.getPooledConnection();
		Statement hislist=null;
		ResultSet hashis=null;
		String abhis="select oe.VISIT_ID, c.card_no as patient_id, oe.ORDERENTRY_ID, "
				+ "oe.ENTERED_DATETIME, oei.ORDERENTRYITEM_ID, "
				+ "lm.LOCATION_DESC, "
				+ "p.person_name as REQUEST_PHYSICIAN, p2.PERSON_NAME, "
				+ "p2.BIRTH_DATE, substr(p2.sex, 4,1) as PATIENT_SEX, iscm.item_subcat_desc, tcm.txn_code, "
				+ "tcm.TXN_DESC, FLOOR(MONTHS_BETWEEN (SYSDATE, p2.BIRTH_DATE)/12) AS PATIENT_AGE, "
				+ "oe.REMARKS, (case v.patient_type when 'PTY1' then 'IP' when 'PTY2' then 'OP' end) as patient_source, oe.order_status, pgcommon.fxGetCodeDesc(v.PATIENT_CLASS) as PATIENT_CLASS,  "
				+ "pgcommon.fxGetCodeDesc(p2.SEX) as SEX, p2.BIRTH_DATE, er.MEDICAL_DESC, er.DIAGNOSIS_DESC, dm.DIAGNOSIS_CODE "
				+ "from ORDERENTRY oe "
				+ "inner join VISIT v on v.visit_id = oe.visit_id "
				+ "inner join ORDERENTRYITEM oei on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID "
				+ "inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = oei.ORDERITEMMSTR_ID "
				+ "inner join ITEMSUBCATEGORYMSTR iscm on iscm.ITEMSUBCATEGORYMSTR_ID = oim.ITEMSUBCATEGORYMSTR_ID "
				+ "inner join ITEMCATEGORYMSTR icm on icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID "
				+ "inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = oe.LOCATIONMSTR_ID "
				+ "inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID "
				+ "left join EXAMINATIONREQUISITION er on er.orderentry_id = oe.orderentry_id "
				+ "inner join patient pt on pt.patient_id = v.patient_id "
				+ "inner join person p2 on p2.person_id = pt.person_id "
				+ "inner join card c on c.person_id =  pt.person_id "
				+ "inner join person p on p.PERSON_ID = cp.PERSON_ID "
				+ "inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID "
				+ "left outer join DIAGNOSISMSTR dm on dm.DIAGNOSISMSTR_ID = er.DIAGNOSISMSTR_ID "
				+ "where icm.ITEM_TYPE = 'ITY7' "
//				+ "and oe.order_status not in ('OSTSTO', 'OSTCAN') "
//				+ "and oe.order_status not in ('OSTSTO', 'OSTCAN') "
				+ "and oei.orderentryitem_id = " +admissionid			
				
				+ " order by oe.ENTERED_DATETIME";
		
				
		try{
//			rislist=risConnection.createStatement();
//			ResultSet hasris=rislist.executeQuery(abris);
//			while(hasris.next())
//			{
//				RisAdmissionList ra=new RisAdmissionList();
//				ra.setPATIENT_ID(hasris.getString("PATIENT_ID"));
//				ra.setPATIENT_NAME(hasris.getString("PATIENT_NAME"));
//				ra.setPATIENT_BIRTH(hasris.getString("PATIENT_BIRTH"));
//				ra.setPATIENT_AGE(hasris.getString("PATIENT_AGE"));
//				ra.setPATIENT_SEX(hasris.getString("PATIENT_SEX"));
//				ra.setMODALITY(hasris.getString("MODALITY"));
//				ra.setREQUEST_PHYSICIAN(hasris.getString("REQUEST_PHYSICIAN"));
//				ra.setREQUEST_DATE(hasris.getString("REQUEST_DATE"));
//				ra.setPERFRMD_START_DATE(hasris.getString("PERFRMD_START_DATE"));
//				ra.setPERFRMD_START_TIME(hasris.getString("PERFRMD_START_TIME"));
//				ra.setREPORT_START_DATE(hasris.getString("REPORT_START_DATE"));
//				ra.setREPORT_START_TIME(hasris.getString("REPORT_START_TIME"));
//				ra.setREPORT_UID(hasris.getString("REPORT_UID"));
//				ra.setPERFRMD_ITEM(hasris.getString("PERFRMD_ITEM"));
//				ra.setBODY_PART(hasris.getString("BODY_PART"));
//				ra.setPATIENT_SOURCE(hasris.getString("PATIENT_SOURCE"));
//				ra.setEXAM_CODE(hasris.getString("EXAM_CODE"));
//				ra.setROOM_ID(hasris.getString("ROOM_ID"));
//				ra.setROOM_NAME(hasris.getString("ROOM_NAME"));
//				ra.setCOMPLETION_FLAG(hasris.getString("COMPLETION_FLAG"));
//				all.add(ra);				
//			}
//			hasris.close();
//			rislist.close();
			
			hislist=hisConnection.createStatement();
			hashis=hislist.executeQuery(abhis);
			while(hashis.next())
			{
				RisAdmissionList ra=new RisAdmissionList();
				ra.setPATIENT_ID(hashis.getString("patient_id"));
				ra.setPATIENT_NAME(hashis.getString("PERSON_NAME"));
				ra.setPATIENT_BIRTH(hashis.getString("BIRTH_DATE"));
				ra.setPATIENT_AGE(hashis.getString("PATIENT_AGE"));
				ra.setPATIENT_SEX(hashis.getString("PATIENT_SEX"));
				ra.setMODALITY(hashis.getString("item_subcat_desc"));
				ra.setREQUEST_PHYSICIAN(hashis.getString("REQUEST_PHYSICIAN"));
				ra.setOrder_status(hashis.getString("order_status"));
				ra.setPATIENT_CLASS(hashis.getString("PATIENT_CLASS"));
				Date d = hashis.getDate("ENTERED_DATETIME");
				Calendar cal = Calendar.getInstance();
				cal.setTime(d);
				String tahun = String.valueOf(cal.get(Calendar.YEAR));
				String bulan = String.format("%02d", cal.get(Calendar.MONTH)+1) ;
				String tgl = String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
				ra.setREQUEST_DATE(tahun+bulan+tgl);
				//ra.setREQUEST_DATE(hashis.getString("ENTERED_DATETIME"));
				ra.setPERFRMD_START_DATE("");
				ra.setPERFRMD_START_TIME("");
				ra.setREPORT_START_DATE("");
				ra.setREPORT_START_TIME("");
				ra.setREPORT_UID("");
				ra.setPERFRMD_ITEM(hashis.getString("TXN_DESC"));
				ra.setBODY_PART("");
				ra.setPATIENT_SOURCE(hashis.getString("PATIENT_SOURCE"));
				ra.setEXAM_CODE(hashis.getString("txn_code"));
				ra.setROOM_ID(hashis.getString("LOCATION_DESC"));
				ra.setROOM_NAME(hashis.getString("LOCATION_DESC"));
				ra.setCOMPLETION_FLAG("");
				ra.setSEX(hashis.getString("SEX"));
				ra.setBIRTH_DATE(hashis.getString("BIRTH_DATE"));
				ra.setMEDICAL_DESC(hashis.getString("MEDICAL_DESC"));
				ra.setDIAGNOSIS_DESC(hashis.getString("DIAGNOSIS_DESC"));
				ra.setDiagnosis_Code(hashis.getString("DIAGNOSIS_CODE"));
				all.add(ra);			
			}
			hashis.close();
			hislist.close();				
			
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (hislist!=null) try  { hislist.close(); } catch (Exception ignore){}
		     if (hashis!=null) try  { hashis.close(); } catch (Exception ignore){}
		     if (hisConnection!=null) try { hisConnection.close();} catch (Exception ignore){}
		   }
		return all;
	}

public static List<RisAdmissionList> getxrayrisreportadmission(String admissionid) {
	List<RisAdmissionList> all=new ArrayList<RisAdmissionList>();
	Connection risConnection=DbConnection.getPooledConnection();
	Statement rislist=null;
	ResultSet hasris=null;
	String abris="select oe.VISIT_ID,v.patient_id, oe.ORDERENTRY_ID, "
			+ "oe.ENTERED_DATETIME, oei.ORDERENTRYITEM_ID, "
			+ "lm.LOCATION_DESC, "
			+ "p.person_name as REQUEST_PHYSICIAN, p2.PERSON_NAME, "
			+ "p2.BIRTH_DATE, substr(p2.sex, 4,1) as PATIENT_SEX, iscm.item_subcat_desc, tcm.txn_code, "
			+ "tcm.TXN_DESC, FLOOR(MONTHS_BETWEEN (SYSDATE, p2.BIRTH_DATE)/12) AS PATIENT_AGE, "
			+ "oe.REMARKS, (case v.patient_type when 'PTY1' then 'IP' when 'PTY2' then 'OP' end) as patient_source, oe.order_status, pgcommon.fxGetCodeDesc(v.PATIENT_CLASS) as PATIENT_CLASS,  "
			+ "pgcommon.fxGetCodeDesc(p2.SEX) as SEX, p2.BIRTH_DATE, er.MEDICAL_DESC, er.DIAGNOSIS_DESC "
			+ "from ORDERENTRY oe "
			+ "inner join VISIT v on v.visit_id = oe.visit_id "
			+ "inner join ORDERENTRYITEM oei on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID "
			+ "inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = oei.ORDERITEMMSTR_ID "
			+ "inner join ITEMSUBCATEGORYMSTR iscm on iscm.ITEMSUBCATEGORYMSTR_ID = oim.ITEMSUBCATEGORYMSTR_ID "
			+ "inner join ITEMCATEGORYMSTR icm on icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID "
			+ "inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = oe.LOCATIONMSTR_ID "
			+ "inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID "
			+ "left join EXAMINATIONREQUISITION er on er.orderentry_id = oe.orderentry_id "
			+ "inner join patient pt on pt.patient_id = v.patient_id "
			+ "inner join person p2 on p2.person_id = pt.person_id "
			+ "inner join person p on p.PERSON_ID = cp.PERSON_ID "
			+ "inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID "
			+ "where icm.ITEM_TYPE = 'ITY7' "
//			+ "and oe.order_status not in ('OSTSTO', 'OSTCAN') "
			+ "and oei.orderentryitem_id = " +admissionid			
			+ " order by oe.ENTERED_DATETIME";
	
	try{
		rislist=risConnection.createStatement();
		hasris=rislist.executeQuery(abris);
		while(hasris.next())
		{
			RisAdmissionList ra=new RisAdmissionList();
			ra.setPATIENT_ID(hasris.getString("patient_id"));
			ra.setPATIENT_NAME(hasris.getString("PERSON_NAME"));
			ra.setPATIENT_BIRTH(hasris.getString("BIRTH_DATE"));
			ra.setPATIENT_AGE(hasris.getString("PATIENT_AGE"));
			ra.setPATIENT_SEX(hasris.getString("PATIENT_SEX"));
			ra.setMODALITY(hasris.getString("item_subcat_desc"));
			ra.setREQUEST_PHYSICIAN(hasris.getString("REQUEST_PHYSICIAN"));
			ra.setREQUEST_DATE(hasris.getString("ENTERED_DATETIME"));
			ra.setPERFRMD_START_DATE("");
			ra.setPERFRMD_START_TIME("");
			ra.setREPORT_START_DATE("");
			ra.setREPORT_START_TIME("");
			ra.setREPORT_UID("");
			ra.setPERFRMD_ITEM(hasris.getString("TXN_DESC"));
			ra.setBODY_PART("");
			ra.setPATIENT_SOURCE(hasris.getString("PATIENT_SOURCE"));
			ra.setEXAM_CODE(hasris.getString("txn_code"));
			ra.setROOM_ID(hasris.getString("LOCATION_DESC"));
			ra.setROOM_NAME(hasris.getString("LOCATION_DESC"));
			ra.setCOMPLETION_FLAG("");
			ra.setOrder_status(hasris.getString("order_status"));
			ra.setPATIENT_CLASS(hasris.getString("PATIENT_CLASS"));
			ra.setSEX(hasris.getString("SEX"));
			ra.setBIRTH_DATE(hasris.getString("BIRTH_DATE"));
			ra.setMEDICAL_DESC(hasris.getString("MEDICAL_DESC"));
			ra.setDIAGNOSIS_DESC(hasris.getString("DIAGNOSIS_DESC"));
			all.add(ra);				
		}
		hasris.close();
		rislist.close();
	}
	catch(SQLException e)
	{
		System.out.println(e.getMessage());
	}
	finally {
	     if (rislist!=null) try  { rislist.close(); } catch (Exception ignore){}
	     if (hasris!=null) try  { hasris.close(); } catch (Exception ignore){}
	     if (risConnection!=null) try { risConnection.close();} catch (Exception ignore){}
	   }
	return all;
}

public static List<RisAdmissionList> getrisadmissionlist(String admissionidlist) {
	List<RisAdmissionList> all=new ArrayList<RisAdmissionList>();
	Connection risConnection=DbConnection.getPooledConnection();
	Statement rislist=null;
	ResultSet hasris=null;
	
    
	String sql="select oe.VISIT_ID, c.card_no as patient_id, oe.ORDERENTRY_ID, "
			+ "oe.ENTERED_DATETIME, oei.ORDERENTRYITEM_ID, "
			+ "lm.LOCATION_DESC, "
			+ "p.person_name as REQUEST_PHYSICIAN, p2.PERSON_NAME, "
			+ "p2.BIRTH_DATE, substr(p2.sex, 4,1) as PATIENT_SEX, iscm.item_subcat_desc, tcm.txn_code, "
			+ "tcm.TXN_DESC, FLOOR(MONTHS_BETWEEN (SYSDATE, p2.BIRTH_DATE)/12) AS PATIENT_AGE, "
			+ "oe.REMARKS, (case v.patient_type when 'PTY1' then 'IP' when 'PTY2' then 'OP' end) as patient_source, oe.order_status, pgcommon.fxGetCodeDesc(v.PATIENT_CLASS) as PATIENT_CLASS,  "
			+ "pgcommon.fxGetCodeDesc(p2.SEX) as SEX, p2.BIRTH_DATE, er.MEDICAL_DESC, er.DIAGNOSIS_DESC, dm.DIAGNOSIS_CODE "
			+ "from ORDERENTRY oe "
			+ "inner join VISIT v on v.visit_id = oe.visit_id "
			+ "inner join ORDERENTRYITEM oei on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID "
			+ "inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = oei.ORDERITEMMSTR_ID "
			+ "inner join ITEMSUBCATEGORYMSTR iscm on iscm.ITEMSUBCATEGORYMSTR_ID = oim.ITEMSUBCATEGORYMSTR_ID "
			+ "inner join ITEMCATEGORYMSTR icm on icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID "
			+ "inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = oe.LOCATIONMSTR_ID "
			+ "inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID "
			+ "left join EXAMINATIONREQUISITION er on er.orderentry_id = oe.orderentry_id "
			+ "inner join patient pt on pt.patient_id = v.patient_id "
			+ "inner join person p2 on p2.person_id = pt.person_id "
			+ "inner join card c on c.person_id =  pt.person_id "
			+ "inner join person p on p.PERSON_ID = cp.PERSON_ID "
			+ "inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID "
			+ "left outer join DIAGNOSISMSTR dm on dm.DIAGNOSISMSTR_ID = er.DIAGNOSISMSTR_ID "
			+ "where icm.ITEM_TYPE = 'ITY7' "
//			+ "and oe.order_status not in ('OSTSTO', 'OSTCAN') "
			+ "and oei.orderentryitem_id IN ("+admissionidlist+") "			
			+ " order by oe.ENTERED_DATETIME";
			
	try{
		rislist=risConnection.createStatement();
		hasris=rislist.executeQuery(sql);

		while (hasris.next()) {
			RisAdmissionList ra=new RisAdmissionList();
			ra.setPATIENT_ID(hasris.getString("patient_id"));
			ra.setPATIENT_NAME(hasris.getString("PERSON_NAME"));
			ra.setPATIENT_BIRTH(hasris.getString("BIRTH_DATE"));
			ra.setPATIENT_AGE(hasris.getString("PATIENT_AGE"));
			ra.setPATIENT_SEX(hasris.getString("PATIENT_SEX"));
			ra.setMODALITY(hasris.getString("item_subcat_desc"));
			ra.setREQUEST_PHYSICIAN(hasris.getString("REQUEST_PHYSICIAN"));
			ra.setOrder_status(hasris.getString("order_status"));
			ra.setPATIENT_CLASS(hasris.getString("PATIENT_CLASS"));
			//Date d = rs.getDate("ENTERED_DATETIME");
			//Calendar cal = Calendar.getInstance();
			//cal.setTime(d);
			//String tahun = String.valueOf(cal.get(Calendar.YEAR));
			//String bulan = String.format("%02d", cal.get(Calendar.MONTH)+1) ;
			//String tgl = String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
			//ra.setREQUEST_DATE(tahun+bulan+tgl);
			ra.setREQUEST_DATE(hasris.getString("ENTERED_DATETIME"));
			ra.setPERFRMD_START_DATE("");
			ra.setPERFRMD_START_TIME("");
			ra.setREPORT_START_DATE("");
			ra.setREPORT_START_TIME("");
			ra.setREPORT_UID("");
			ra.setPERFRMD_ITEM(hasris.getString("TXN_DESC"));
			ra.setBODY_PART("");
			ra.setPATIENT_SOURCE(hasris.getString("PATIENT_SOURCE"));
			ra.setEXAM_CODE(hasris.getString("txn_code"));
			ra.setROOM_ID(hasris.getString("LOCATION_DESC"));
			ra.setROOM_NAME(hasris.getString("LOCATION_DESC"));
			ra.setCOMPLETION_FLAG("");
			ra.setSEX(hasris.getString("SEX"));
			ra.setBIRTH_DATE(hasris.getString("BIRTH_DATE"));
			ra.setMEDICAL_DESC(hasris.getString("MEDICAL_DESC"));
			ra.setDIAGNOSIS_DESC(hasris.getString("DIAGNOSIS_DESC"));
			ra.setAdmision_Id(hasris.getString("ORDERENTRYITEM_ID"));
			ra.setDiagnosis_Code(hasris.getString("DIAGNOSIS_CODE"));
			all.add(ra);
		}				
		hasris.close();
		rislist.close();
	}
	catch(SQLException e)
	{
		e.fillInStackTrace();
		System.out.println(e.getMessage());
	}
	finally {
	     if (rislist!=null) try  { rislist.close(); } catch (Exception ignore){}
	     if (hasris!=null) try  { hasris.close(); } catch (Exception ignore){}
	     if (risConnection!=null) try { risConnection.close();} catch (Exception ignore){}
	   }
	return all;
	
}

public static List<RisAdmissionList> getrisreportadmission(String admissionid){
	List<RisAdmissionList> result =new ArrayList<RisAdmissionList>();

	String query = "SELECT * FROM Tbl_Report tr"
			+ " where tr.admission_id = ? ";
	
	try {
		List list = DbConnection.executeReader(DbConnection.RIS_REPORT, query, new Object[] {admissionid});
		for (int a=0;a<list.size();a++){
			HashMap row = (HashMap)list.get(a);
			RisAdmissionList ra=new RisAdmissionList();
			
			ra.setPATIENT_ID(row.get("patient_ID").toString());
			ra.setPATIENT_NAME(row.get("patient_name").toString());
//			ra.setPATIENT_BIRTH(row.get("BIRTH_DATE").toString());
			ra.setPATIENT_AGE(row.get("patient_age").toString());
			ra.setPATIENT_SEX(row.get("patient_sex").toString());
//			ra.setMODALITY(row.get("item_subcat_desc").toString());
			ra.setREQUEST_PHYSICIAN(row.get("request_physician").toString());
			ra.setOrder_status(row.get("order_status").toString());
//			ra.setPATIENT_CLASS(row.get("PATIENT_CLASS").toString());
//			Date d = hashis.getDate("ENTERED_DATETIME");
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(d);
//			String tahun = String.valueOf(cal.get(Calendar.YEAR));
//			String bulan = String.format("%02d", cal.get(Calendar.MONTH)+1) ;
//			String tgl = String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
//			ra.setREQUEST_DATE(tahun+bulan+tgl);
			//ra.setREQUEST_DATE(row.get("ENTERED_DATETIME").toString());
			ra.setPERFRMD_START_DATE("");
			ra.setPERFRMD_START_TIME("");
			ra.setREPORT_START_DATE("");
			ra.setREPORT_START_TIME("");
			ra.setREPORT_UID("");
			ra.setPERFRMD_ITEM(row.get("perfrmd_item").toString());
			ra.setBODY_PART("");
			ra.setPATIENT_SOURCE(row.get("patient_source").toString());
//			ra.setEXAM_CODE(row.get("txn_code").toString());
//			ra.setROOM_ID(row.get("LOCATION_DESC").toString());
//			ra.setROOM_NAME(row.get("LOCATION_DESC").toString());
			ra.setCOMPLETION_FLAG("");
			ra.setSEX(row.get("patient_sex").toString());
//			ra.setBIRTH_DATE(row.get("BIRTH_DATE").toString());
			ra.setMEDICAL_DESC(row.get("medical_DESC").toString());
			ra.setDIAGNOSIS_DESC(row.get("diagnosis_DESC").toString());
			ra.setDiagnosis_Code(row.get("diagnosis_Code").toString());
			result.add(ra);	
		}
	} catch (SQLException e) {
		System.out.println(e.getMessage());
	} catch (Exception e){
		System.out.println(e.getMessage());
	}
	finally {
	   }	
	return result;
}

}
