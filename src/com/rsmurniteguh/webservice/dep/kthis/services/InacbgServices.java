package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.util.TextUtils;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.gson.Gson;
import com.rsmurniteguh.webservice.dep.all.model.InacbgResponse;
import com.rsmurniteguh.webservice.dep.all.model.listpolyres;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInfoResponse;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Metadata;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.services.IBpjsService2;

public class InacbgServices {

	private static final String KEY = "4b32b4411fbfb58be3decdb18688544e31f70dccb1d32cabfa4bd1d64ab2345f";
	private static final String BASE_URL = "http://192.168.222.107/E-Klaim/ws.php?mode=debug";	
	
	
//	public static String encrypt(String value) throws Exception {
//		byte[] clean = value.getBytes();
//
//        int ivSize = 32;
//        byte[] iv = new byte[ivSize];
//        SecureRandom random = new SecureRandom();
//        random.nextBytes(iv);
//        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
//
//        MessageDigest digest = MessageDigest.getInstance("SHA-256");
//        digest.update(KEY.getBytes("UTF-8"));
//        byte[] keyBytes = new byte[32];
//        System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);
//        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");
//
//        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
//        byte[] encrypted = cipher.doFinal(clean);
//
//        byte[] encryptedIVAndText = new byte[ivSize + encrypted.length];
//        System.arraycopy(iv, 0, encryptedIVAndText, 0, ivSize);
//        System.arraycopy(encrypted, 0, encryptedIVAndText, ivSize, encrypted.length);
//
//        return new Base64().encodeToString(encryptedIVAndText);
//	}
//	
//	
//	public static String decrypt(String text) throws Exception{
//		Cipher cipherDecrypt = Cipher.getInstance("AES/CBC/PKCS5Padding");
//		 
//		int ivSize = 32;
//        int keySize = 32;
//
//        ByteBuffer buffer = ByteBuffer.wrap(new Base64().decode(text));
//	    byte[] saltBytes = new byte[20];
//	    buffer.get(saltBytes, 0, saltBytes.length);
//	    byte[] ivBytes1 = new byte[cipherDecrypt.getBlockSize()];
//	    buffer.get(ivBytes1, 0, ivBytes1.length);
//	    byte[] encryptedIvTextBytes = new byte[buffer.capacity() - saltBytes.length - ivBytes1.length];
//        
//        
//        byte[] iv = new byte[ivSize];
//        System.arraycopy(encryptedIvTextBytes, 0, iv, 0, iv.length);
//        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
//
//        int encryptedSize = encryptedIvTextBytes.length - ivSize;
//        byte[] encryptedBytes = new byte[encryptedSize];
//        System.arraycopy(encryptedIvTextBytes, ivSize, encryptedBytes, 0, encryptedSize);
//
//        byte[] keyBytes = new byte[keySize];
//        MessageDigest md = MessageDigest.getInstance("SHA-256");
//        md.update(KEY.getBytes());
//        System.arraycopy(md.digest(), 0, keyBytes, 0, keyBytes.length);
//        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");
//
//        cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
//        byte[] decrypted = cipherDecrypt.doFinal(encryptedBytes);
//
//        return new String(decrypted);
//		
//		
//		
//		
//	}
//	
	
//	public static String encrypt(String word) throws Exception {
//		byte[] ivBytes;
//	    SecureRandom random = new SecureRandom();
//	    byte bytes[] = new byte[20];
//	    random.nextBytes(bytes);
//	    byte[] saltBytes = bytes;
//	    SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
//	    PBEKeySpec spec = new PBEKeySpec(KEY.toCharArray(),saltBytes,65556,256);
//	    SecretKey secretKey = factory.generateSecret(spec);
//	    SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
//
//	    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//	    cipher.init(Cipher.ENCRYPT_MODE, secret);
//	     
//	    AlgorithmParameters params = cipher.getParameters();
//	     
//	    ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
//	     
//	    byte[] encryptedTextBytes = cipher.doFinal(word.getBytes("UTF-8"));
//	      
//	    byte[] buffer = new byte[saltBytes.length + ivBytes.length + encryptedTextBytes.length];
//	    System.arraycopy(saltBytes, 0, buffer, 0, saltBytes.length);
//	    System.arraycopy(ivBytes, 0, buffer, saltBytes.length, ivBytes.length);
//	    System.arraycopy(encryptedTextBytes, 0, buffer, saltBytes.length + ivBytes.length, encryptedTextBytes.length);
//	    return new Base64().encodeToString(buffer);
//	}
//	
//	
//	
//	
//	private static String hex2Bin(String hex) {
//		StringBuilder output = new StringBuilder();
//	    for (int i = 0; i < hex.length(); i+=2) {
//	        String str = hex.substring(i, i+2);
//	        output.append((char)Integer.parseInt(str, 16));
//	    }
//	    return output.toString();
//	}


	public static String decrypt(String encryptedText) throws Exception {
	    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	 
	    ByteBuffer buffer = ByteBuffer.wrap(new Base64().decode(encryptedText));
	    byte[] saltBytes = new byte[20];
	    buffer.get(saltBytes, 0, saltBytes.length);
	    byte[] ivBytes1 = new byte[cipher.getBlockSize()];
	    buffer.get(ivBytes1, 0, ivBytes1.length);
	    byte[] encryptedTextBytes = new byte[buffer.capacity() - saltBytes.length - ivBytes1.length];
	  
	    buffer.get(encryptedTextBytes);
	  
	    SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
	    PBEKeySpec spec = new PBEKeySpec(KEY.toCharArray(), saltBytes, 65556, 256);
	    SecretKey secretKey = factory.generateSecret(spec);
	    SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
	    cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(ivBytes1));
	    byte[] decryptedTextBytes = null;
	    try {
	      decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
	    } catch (IllegalBlockSizeException e) {
	        e.printStackTrace();
	    } catch (BadPaddingException e) {
	        e.printStackTrace();
	    }
	   
	    return new String(decryptedTextBytes);
	}
	
	public static String cetakKlaim(String noSep) {
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
		
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\": \"claim_print\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_sep\": \""+noSep+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String tambahKlaim(String noKa, String noSep, String mrn, String nama, String tglLahir, String gender) {
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\": \"new_claim\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_kartu\": \""+noKa+"\", "
					+ "\"nomor_sep\": \""+noSep+"\", "
					+ "\"nomor_rm\": \""+mrn+"\", "
					+ "\"nama_pasien\": \""+nama+"\","
					+ "\"tgl_lahir\": \""+tglLahir+" 00:00:00\","
					+ "\"gender\": \""+gender+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String updateKlaim(String noSep, String noKa, String tglMasuk, String tglPulang, String jnsRawat,String klsRawat,
			String icuInd, String icuLos, String ventHour, String upInd, String upCls, String upLos, String addPay,
			String brWgt, String disSts, String diag, String pro, String trfPnb, String trfBdh, String trfKon,
			String trfTa, String trfKep, String trfPnj, String trfRad, String trfLab, String trfPd, String trfReh,
			String trfKmr, String trfRi, String trfOb, String trfAl, String trfBm, String trfSa, String naDok)  
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\": \"set_claim_data\", "
					+ "\"nomor_sep\": \""+noSep+"\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_sep\": \""+noSep+"\", "
					+ "\"nomor_kartu\": \""+noKa+"\", "
					+ "\"tgl_masuk\": \""+tglMasuk+"\", "
					+ "\"tgl_pulang\": \""+tglPulang+"\", "
					+ "\"jenis_rawat\": \""+jnsRawat+"\", "
					+ "\"kelas_rawat\": \""+klsRawat+"\", "
					+ "\"adl_sub_acute\": \"0\", "
					+ "\"adl_chronic\": \"0\", "
					+ "\"icu_indikator\": \""+icuInd+"\", "
					+ "\"icu_los\": \""+icuLos+"\", "
					+ "\"ventilator_hour\": \""+ventHour+"\", "
					+ "\"upgrade_class_ind\": \""+upInd+"\", "
					+ "\"upgrade_class_class\": \""+upCls+"\", "
					+ "\"upgrade_class_los\": \""+upLos+"\", "
					+ "\"add_payment_pct\": \""+addPay+"\", "
					+ "\"birth_weight\": \""+brWgt+"\", "
					+ "\"discharge_status\": \""+disSts+"\", "
					+ "\"diagnosa\": \""+diag+"\","
					+ "\"procedure\": \""+pro+"\","
					+ "\"tarif_rs\": { "
					+ "\"prosedur_non_bedah\": \""+trfPnb+"\","
					+ "\"prosedur_bedah\": \""+trfBdh+"\","
					+ "\"konsultasi\": \""+trfKon+"\","
					+ "\"tenaga_ahli\": \""+trfTa+"\", "
					+ "\"keperawatan\": \""+trfKep+"\", "
					+ "\"penunjang\": \""+trfPnj+"\", "
					+ "\"radiologi\": \""+trfRad+"\", "
					+ "\"laboratorium\": \""+trfLab+"\","
					+ "\"pelayanan_darah\": \""+trfPd+"\", "
					+ "\"rehabilitasi\": \""+trfReh+"\", "
					+ "\"kamar\": \""+trfKmr+"\", "
					+ "\"rawat_intensif\": \""+trfRi+"\", "
					+ "\"obat\": \""+trfOb+"\", "
					+ "\"alkes\": \""+trfAl+"\", "
					+ "\"bmhp\": \""+trfBm+"\", "
					+ "\"sewa_alat\": \""+trfSa+"\" "
					+ "}, "
					+ "\"tarif_poli_eks\": \"0\", "
					+ "\"nama_dokter\": \""+naDok+"\", "
					+ "\"kode_tarif\": \"BS\", "
					+ "\"payor_id\": \"3\", "
					+ "\"payor_cd\": \"JKN\", "
					+ "\"cob_cd\": \"#\", "
					+ "\"coder_nik\": \"6661\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String groupingStage1(String noSep)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{"
					+ "\"metadata\": { "
					+ "\"method\":\"grouper\", "
					+ "\"stage\":\"1\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_sep\":\"0001R0016120666662\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String groupingStage2(String noSep, String cmg)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"grouper\", "
					+ "\"stage\":\"2\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_sep\":\""+noSep+"\", "
					+ "\"special_cmg\": \""+cmg+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String finalKlaim(String noSep)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"claim_final\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_sep\":\""+noSep+"\", "
					+ "\"coder_nik\": \"6661\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String updatePasien(String mrn,String bpjs,String nama,String tgllahir,String gender)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"update_patient\", "
					+ "\"nomor_rm\":\""+mrn+"\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_kartu\":\""+bpjs+"\", "
					+ "\"nomor_rm\":\""+mrn+"\", "
					+ "\"nama_pasien\":\""+nama+"\", "
					+ "\"tgl_lahir\":\""+tgllahir+"\" 00:00:00\", "
					+ "\"gender\": \""+gender+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	
	public static String hapusPasien(String mrn)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"delete_patient\""
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_rm\":\""+mrn+"\", "
					+ "\"coder_nik\": \"6661\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String editUlangKlaim(String sep)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"reedit_claim\""
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_sep\":\""+sep+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String sendKlaim(String startdate, String enddate, String jenis, String tipe)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"send_claim\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"start_dt\":\""+startdate+"\", "
					+ "\"stop_dt\":\""+enddate+"\", "
					+ "\"jenis_rawat\":\""+jenis+"\", "
					+ "\"date_type\": \""+tipe+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String sendKlaimIndividual(String sep)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"send_claim_individual\""
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_sep\":\""+sep+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String getKlaim(String startdate, String enddate, String jenis)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"pull_claim\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"start_dt\":\""+startdate+"\", "
					+ "\"stop_dt\":\""+enddate+"\", "
					+ "\"jenis_rawat\":\""+jenis+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String getDetailKlaimbySep(String sep)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"get_claim_data\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_sep\":\""+sep+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String getStatusKlaimbySep(String sep)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"get_claim_status\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_sep\":\""+sep+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String deleteKlaimbySep(String sep)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"delete_claim\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"nomor_sep\":\""+sep+"\", "
					+ "\"coder_nik\": \"6661\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String searchDiagnosa(String keyword)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"search_diagnosis\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"keyword\":\""+keyword+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String searchProcedure(String keyword)
	{
		String url = BASE_URL;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"metadata\": { "
					+ "\"method\":\"search_procedures\" "
					+ "}, "
					+ "\"data\": { "
					+ "\"keyword\":\""+keyword+"\" "
					+ "} "
					+ "}";                                            
		  
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			s = response.toString();
			in.close();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String integrasiKthis(BigDecimal visitId)
	{
		String result = "";
		Connection connection=DbConnection.getPooledConnection();
		if(connection==null)return null;
		ResultSet rs=null;
		Statement stmt=null;
		ResultSet rs2=null;
		Statement stmt2=null;
		ResultSet rs3=null;
		Statement stmt3=null;
		ResultSet rs4=null;
		Statement stmt4=null;
		ResultSet rs5=null;
		Statement stmt5=null;
		String jk = "";
		String tipe = "";
		String kp = "";
		String uind = "";
		String ucls = "";
		String ulos = "";
		String iind = "";
		String ilos = "";
		String ven = "";
		String diag = "";
		String prod = "";
		String namaPasien = "";
		String kls = "";
		int txnCode = 0;
		StringBuilder builder = new StringBuilder();
		StringBuilder builder2 = new StringBuilder();
		
		
		String query = "SELECT PS.PERSON_NAME NADOK,SB.VISIT_ID,SB.CARD_NO,SB.SEP_NO,SB.BPJS_NO,TO_CHAR(VS.ADMISSION_DATETIME,'YYYY-MM-DD HH:MI:SS') TGLMASUK,TO_CHAR(VS.DISCHARGE_DATETIME,'YYYY-MM-DD HH:MI:SS') TGLKELUAR ,VS.PATIENT_TYPE,PS2.PERSON_NAME ,TO_CHAR(PS2.BIRTH_DATE,'YYYY-MM-DD HH:MI:SS') TGLLAHIR,PS2.SEX,VS.BPJS_UPGRADE,MR.LAST_CONDITION "
				+ "FROM SEPBPJS SB "
				+ "INNER JOIN VISIT VS ON VS.VISIT_ID = SB.VISIT_ID "
				+ "INNER JOIN CAREPROVIDER CP ON CP.CAREPROVIDER_ID = SB.CAREPROVIDER_ID "
				+ "INNER JOIN PERSON PS ON PS.PERSON_ID = CP.PERSON_ID "
				+ "INNER JOIN CARD CD ON CD.CARD_NO = SB.CARD_NO "
				+ "INNER JOIN PERSON PS2 ON PS2.PERSON_ID= CD.PERSON_ID "
				+ "LEFT OUTER JOIN MEDICAL_RESUME MR ON MR.VISIT_ID =VS.VISIT_ID "
				+ "WHERE SB.VISIT_ID="+visitId+" AND SB.DEFUNCT_IND='N' AND VS.ADMIT_STATUS <> 'AST5'";
		
		try{
			stmt=connection.createStatement();
			rs=stmt.executeQuery(query);
			rs.next();
			String noSep = rs.getString("SEP_NO");
			if(noSep == null)
			{
				result = "No Sep Tidak Ditemukan";
			}
			else
			{
				String nadok = rs.getString("NADOK");
				String mrn = rs.getString("CARD_NO");
				String noBpjs = rs.getString("BPJS_NO");
				String tglMasuk = rs.getString("TGLMASUK");
				String tglKeluar = rs.getString("TGLKELUAR");
				String tipePasien = rs.getString("PATIENT_TYPE");
				if(tipePasien.equals("PTY1"))
				{
					tipe = "1";
					txnCode = 311150498;
				}
				else
				{
					tipe = "2";
					txnCode = 440414393;
					
				}
				String tglLahir = rs.getString("TGLLAHIR");
				String sex = rs.getString("SEX");
				if(sex.equals("SEXM"))
				{
					jk = "1";
				}
				else
				{
					jk = "2";
				}
				String upgr = rs.getString("BPJS_UPGRADE");
				if(upgr == null)
				{
					uind = "0";
					ucls = "";
					ulos = "";
				}

				String lc = rs.getString("LAST_CONDITION");
				if(lc == null)
				{
					kp = "";
				}
				else
				{
					if(lc.equals("PLC1"))
					{
						kp = "1";
					}
					else if(lc.equals("PLC3"))
					{
						kp = "3";
					}
					else if(lc.equals("PLC4"))
					{
						kp = "2";
					}
					else if(lc.equals("PLC5"))
					{
						kp = "4";
					}
					else
					{
						kp = "5";
					}
				}
					
				
				String ret = BpjsService2.GetBpjsInfobyNoka(noBpjs);
	            Gson gson = new Gson();
	            BpjsInfoResponse memberInfo = gson.fromJson(ret, BpjsInfoResponse.class); 
	            
	            Metadata metadata = memberInfo == null ? null : memberInfo.getMetadata();
	            if(metadata != null) {
            		if(metadata.getCode().equals("200") && metadata.getMessage().equals("OK")){
			
            			namaPasien = memberInfo.getResponse().getPeserta().getNama();
            			kls = memberInfo.getResponse().getPeserta().getHakKelas().getKode();
            		}
	            }
				
	            
	            String query2 = "SELECT COUNT(*) Tot FROM BEDHISTORY WHERE  VISIT_ID="+visitId+" AND ACCOMMMSTR_ID=311536653";
	            stmt2=connection.createStatement();
				rs2=stmt2.executeQuery(query2);
				rs2.next();
				
				Integer tot = rs2.getInt("Tot");
				if(tot == 0)
				{
					iind = "0";
					ilos = "";
					ven = "";
				}
				rs2.close();
				stmt2.close();
				
				String query3 = "SELECT DM.DIAGNOSIS_CODE FROM VISITDIAGNOSIS VDD "
						+ "INNER JOIN VISIT VS ON VS.VISIT_ID = VDD.VISIT_ID "
						+ "INNER JOIN VISITDIAGNOSISDETAIL VSD ON VSD.VISITDIAGNOSIS_ID = VDD.VISITDIAGNOSIS_ID "
						+ "INNER JOIN DIAGNOSISMSTR DM ON DM.DIAGNOSISMSTR_ID = VSD.VISIT_DIAGNOSISMSTR_ID "
						+ "WHERE VDD.VISIT_ID="+visitId+" AND DM.VERSION='ICD10'";
				stmt3=connection.createStatement();
				rs3=stmt3.executeQuery(query3);
				List<String> icd10 = new ArrayList<String>(); 
				while(rs3.next())
				{
					icd10.add(rs3.getString("DIAGNOSIS_CODE"));
				}
				for (String createDiag : icd10) {
				    if (builder.length() > 0) 
				    	builder.append("#");
				    builder.append(createDiag);
				}

				diag = builder.toString();
				rs3.close();
				stmt3.close();
				
				String query4 = "SELECT DM.DIAGNOSIS_CODE FROM VISITDIAGNOSIS VDD "
						+ "INNER JOIN VISIT VS ON VS.VISIT_ID = VDD.VISIT_ID "
						+ "INNER JOIN VISITDIAGNOSISDETAIL VSD ON VSD.VISITDIAGNOSIS_ID = VDD.VISITDIAGNOSIS_ID "
						+ "INNER JOIN DIAGNOSISMSTR DM ON DM.DIAGNOSISMSTR_ID = VSD.VISIT_DIAGNOSISMSTR_ID "
						+ "WHERE VDD.VISIT_ID="+visitId+" AND DM.VERSION='ICD9-CMOLD'";
				stmt4=connection.createStatement();
				rs4=stmt4.executeQuery(query4);
				List<String> icd9 = new ArrayList<String>(); 
				while(rs4.next())
				{
					icd9.add(rs4.getString("DIAGNOSIS_CODE"));
				}
				for (String createProd : icd9) {
				    if (builder2.length() > 0) 
				    	builder2.append("#");
				    builder2.append(createProd);
				}

				prod = builder2.toString();
				rs4.close();
				stmt4.close();
				
				String query5 = "SELECT VS.VISIT_ID, "
						+ "(SELECT NVL(SUM(PAT.TXN_AMOUNT),0) - NVL(SUM(VDS.DISCOUNT_AMOUNT),0) FROM PATIENTACCOUNT PA "
						+ "INNER JOIN PATIENTACCOUNTTXN PAT ON PAT.PATIENTACCOUNT_ID =PA.PATIENTACCOUNT_ID "
						+ "LEFT OUTER JOIN VISITDISCOUNTDETAIL VDS ON VDS.PATIENTACCOUNTTXN_ID = PAT.PATIENTACCOUNTTXN_ID "
						+ "WHERE PA.VISIT_ID=VS.VISIT_ID AND PAT.TXNCODEMSTR_ID="+txnCode+") KONSULTASI "
						+ "FROM VISIT VS WHERE VS.VISIT_ID ="+visitId+" GROUP BY VS.VISIT_ID";
				stmt5=connection.createStatement();
				rs5=stmt5.executeQuery(query5);
				rs5.next();
				
				String kon = rs5.getString("KONSULTASI");
				
				
				String tmbhKlaim = tambahKlaim(noBpjs,noSep,mrn,namaPasien,tglLahir,jk);
				InacbgResponse ir = gson.fromJson(tmbhKlaim, InacbgResponse.class); 
	            
				Metadata data = ir == null ? null : ir.getMetadata();
	            if(data != null) {
            		if(data.getCode().equals("200") && data.getMessage().equals("Ok"))
            		{
            			String updateKlaim = updateKlaim(noSep, noBpjs, tglMasuk, tglKeluar, tipe, kls, iind, ilos, ven, uind, ucls, 
            					ulos, "", "", kp, diag, prod, "", "", kon, "", "", "", "", "", "", "", "", "", "", "", "", "", nadok);
            			
            			InacbgResponse uk = gson.fromJson(updateKlaim, InacbgResponse.class); 
            			Metadata data2 = uk == null ? null : ir.getMetadata();
        	            if(data2 != null) 
        	            {
                    		if(data2.getCode().equals("200") && data2.getMessage().equals("Ok"))
                    		{
                    			result = "Sukses";
                    		}
                    		else
                    		{
                    			result = data2.getMessage();    
                    		}
        	            }
            		}
            		else
            		{
            			result = data.getMessage();      			
            		}
            			
	            }
				
	            rs5.close();
				stmt5.close();
			}
			
			rs.close();
			stmt.close();
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		     if (stmt2!=null) try  { stmt2.close(); } catch (Exception ignore){}
		     if (rs3!=null) try  { rs3.close(); } catch (Exception ignore){}
		     if (stmt3!=null) try  { stmt3.close(); } catch (Exception ignore){}
		     if (rs4!=null) try  { rs4.close(); } catch (Exception ignore){}
		     if (stmt4!=null) try  { stmt4.close(); } catch (Exception ignore){}
		     if (rs5!=null) try  { rs4.close(); } catch (Exception ignore){}
		     if (stmt5!=null) try  { stmt4.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return result;
	}
	
}
