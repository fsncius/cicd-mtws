package com.rsmurniteguh.webservice.dep.kthis.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.StoreMaterial;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class StoreMaterialSer extends DbConnection {

	public static List<StoreMaterial> getStoreMaterial(String storemtId) {
		List<StoreMaterial> all=new ArrayList<StoreMaterial>();
		ResultSet hastore=null;
		Connection connection=DbConnection.getPooledConnection();
		if(connection == null)return null;
		Statement datat=null;
		String storequery="select s.storeitem_id, s.materialitemmstr_id, mc.material_item_cat_desc, m.material_item_name, s.min_qty, s.max_qty, s.store_uom , s.balance_qty, cm.code_desc "
				+ " from storeitem s inner join codemstr cm on cm.code_cat = substr(s.store_uom, 1, 3) and cm.code_abbr = substr(s.store_uom, 4, 5) "
				+ " inner join materialitemmstr m on m.materialitemmstr_id = s.materialitemmstr_id inner join materialitemcatmstr mc on mc.materialitemcatmstr_id = m.materialitemcatmstr_id "
				+ " where s.storemstr_id = "+storemtId+" and s.defunct_ind = 'N' and m.defunct_ind = 'N' and mc.defunct_ind = 'N' and cm.defunct_ind = 'N'";
		try {
			datat=connection.createStatement();
			hastore=datat.executeQuery(storequery);
			while(hastore.next())
			{
				StoreMaterial sm=new StoreMaterial();
				sm.setStoreItem(hastore.getLong("storeitem_id"));
				sm.setMaterialItem(hastore.getLong("materialitemmstr_id"));
				sm.setMaterialItemCat(hastore.getString("material_item_cat_desc"));
				sm.setMaterialItemName(hastore.getString("material_item_name"));
				sm.setMinQty(hastore.getBigDecimal("min_qty"));
				sm.setMaxQty(hastore.getBigDecimal("max_qty"));
				sm.setStoreUom(hastore.getString("store_uom"));
				sm.setBalanceQty(hastore.getBigDecimal("balance_qty"));
				sm.setCodeDesc(hastore.getString("code_desc"));
				all.add(sm);
			}	
			hastore.close();
			datat.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hastore!=null) try  { hastore.close(); } catch (Exception ignore){}
		     if (datat!=null) try  { datat.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

}
