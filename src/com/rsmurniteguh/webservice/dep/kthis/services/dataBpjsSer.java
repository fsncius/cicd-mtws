package com.rsmurniteguh.webservice.dep.kthis.services;

import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.http.util.TextUtils;

import com.rsmurniteguh.webservice.dep.all.model.BaseInfo;
import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.BpjsMedicalResume;
import com.rsmurniteguh.webservice.dep.all.model.BpjsQueue;
import com.rsmurniteguh.webservice.dep.all.model.CurrentDatetim;
import com.rsmurniteguh.webservice.dep.all.model.DetailDokter;
import com.rsmurniteguh.webservice.dep.all.model.DoctorSchedule;
import com.rsmurniteguh.webservice.dep.all.model.IntegratedKthis;
import com.rsmurniteguh.webservice.dep.all.model.IpMedicalResume;
import com.rsmurniteguh.webservice.dep.all.model.KamarPasien;
import com.rsmurniteguh.webservice.dep.all.model.KelasPasien;
import com.rsmurniteguh.webservice.dep.all.model.LicstCekValid;
import com.rsmurniteguh.webservice.dep.all.model.ListCanReg;
import com.rsmurniteguh.webservice.dep.all.model.ListRuangan;
import com.rsmurniteguh.webservice.dep.all.model.ListCekTxn;
import com.rsmurniteguh.webservice.dep.all.model.ListCodeCar;
import com.rsmurniteguh.webservice.dep.all.model.ListDietPasien;
import com.rsmurniteguh.webservice.dep.all.model.ListGetQueue;
import com.rsmurniteguh.webservice.dep.all.model.ListInsertQueue;
import com.rsmurniteguh.webservice.dep.all.model.ListKamarHiv;
import com.rsmurniteguh.webservice.dep.all.model.ListOnlineReg;
import com.rsmurniteguh.webservice.dep.all.model.ListPatientId;
import com.rsmurniteguh.webservice.dep.all.model.ListPoliCode;
import com.rsmurniteguh.webservice.dep.all.model.ListQueueBpjs;
import com.rsmurniteguh.webservice.dep.all.model.ListQueueUser;
import com.rsmurniteguh.webservice.dep.all.model.ListReceipt;
import com.rsmurniteguh.webservice.dep.all.model.ListResDokter;
import com.rsmurniteguh.webservice.dep.all.model.ListResourceMstr;
import com.rsmurniteguh.webservice.dep.all.model.ListResourceMstr2;
import com.rsmurniteguh.webservice.dep.all.model.ListResourceMstrPoly;
import com.rsmurniteguh.webservice.dep.all.model.ListResourceMstrPolyName;
import com.rsmurniteguh.webservice.dep.all.model.ListResourceSchedule;
import com.rsmurniteguh.webservice.dep.all.model.ListResponseTime;
import com.rsmurniteguh.webservice.dep.all.model.ListSelfReg;
import com.rsmurniteguh.webservice.dep.all.model.ListTindakan;
import com.rsmurniteguh.webservice.dep.all.model.ListTotal;
import com.rsmurniteguh.webservice.dep.all.model.ListUserNameKT;
import com.rsmurniteguh.webservice.dep.all.model.Listqueueno;
import com.rsmurniteguh.webservice.dep.all.model.ListPasien;
import com.rsmurniteguh.webservice.dep.all.model.Listtxn;
import com.rsmurniteguh.webservice.dep.all.model.MedicalResume;
import com.rsmurniteguh.webservice.dep.all.model.MedicalResumeSignature;
import com.rsmurniteguh.webservice.dep.all.model.ObatBpjs;
import com.rsmurniteguh.webservice.dep.all.model.OpMedicalResume;
import com.rsmurniteguh.webservice.dep.all.model.PatientHiv;
import com.rsmurniteguh.webservice.dep.all.model.RegData;
import com.rsmurniteguh.webservice.dep.all.model.ResourceList;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.RujukanBpjs;
import com.rsmurniteguh.webservice.dep.all.model.SelfRegistration;
import com.rsmurniteguh.webservice.dep.all.model.SignatureMedicalResume;
import com.rsmurniteguh.webservice.dep.all.model.StatusLab;
import com.rsmurniteguh.webservice.dep.all.model.UploadResume;
import com.rsmurniteguh.webservice.dep.all.model.VisitId;
import com.rsmurniteguh.webservice.dep.all.model.VisitSignatureMedicalResume;
import com.rsmurniteguh.webservice.dep.all.model.listCardNoBpjs;
import com.rsmurniteguh.webservice.dep.all.model.listHasBpjsNo;
import com.rsmurniteguh.webservice.dep.all.model.listpolyres;
import com.rsmurniteguh.webservice.dep.all.model.viewJournal2;
import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.biz.ListResourceScheme;
import com.rsmurniteguh.webservice.dep.all.model.ListValidAntrian;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.DataUtils;

public class dataBpjsSer extends DbConnection {

	public static List<listHasBpjsNo> getDataBpjsNo(String nobpjs) {
		List<listHasBpjsNo> all = new ArrayList<listHasBpjsNo>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select PS.PERSON_NAME, CD.CARD_NO from PATIENT PT, PERSON PS, CARD CD WHERE PT.BPJS_NO='"
				+ nobpjs + "' AND" + " PS.PERSON_ID=PT.PERSON_ID AND CD.PERSON_ID=PS.PERSON_ID";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			haspas.next();
			listHasBpjsNo ln = new listHasBpjsNo();
			ln.setPERSON_NAME(haspas.getString("PERSON_NAME"));
			ln.setCARD_NO(haspas.getString("CARD_NO"));
			all.add(ln);
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<listCardNoBpjs> getCardNo(String cardno) {
		// TODO Auto-generated method stub
		List<listCardNoBpjs> all = new ArrayList<listCardNoBpjs>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select PS.PERSON_NAME, PT.BPJS_NO  from CARD CD, PATIENT PT, PERSON PS where CD.CARD_NO='"
				+ cardno + "'" + " AND PT.PERSON_ID=CD.PERSON_ID AND PS.PERSON_ID=PT.PERSON_ID";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			haspas.next();
			listCardNoBpjs ln = new listCardNoBpjs();
			ln.setPERSON_NAME(haspas.getString("PERSON_NAME"));
			ln.setBPJS_NO(haspas.getString("BPJS_NO"));
			all.add(ln);
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static BaseInfo getBaseInfo(String mrn) {
		// TODO Auto-generated method stub
		BaseInfo all = new BaseInfo();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "SELECT * FROM (SELECT PS.PERSON_NAME,PS.SEX,PS.BIRTH_DATE,PT.PATIENT_CLASS,PS.MARITAL_STATUS,AD.ADDRESS_1 "
				+ "FROM CARD CD " 
				+ "INNER JOIN PERSON PS ON PS.PERSON_ID=CD.PERSON_ID "
				+ "INNER JOIN PATIENT PT ON PS.PERSON_ID=PT.PERSON_ID "
				+ "INNER JOIN STAKEHOLDER SH ON PS.STAKEHOLDER_ID = SH.STAKEHOLDER_ID "
				+ "INNER JOIN ADDRESS AD ON SH.STAKEHOLDER_ID= AD.STAKEHOLDER_ID " + "WHERE CD.CARD_NO=" + mrn
				+ " ORDER BY SH.STAKEHOLDER_ID DESC )WHERE ROWNUM=1";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			haspas.next();
			all.setPERSON_NAME(haspas.getString("PERSON_NAME"));
			all.setSEX(haspas.getString("SEX"));
			all.setBIRTH_DATE(haspas.getString("BIRTH_DATE"));
			all.setPATIENT_CLASS(haspas.getString("PATIENT_CLASS"));
			all.setMARITAL_STATUS(haspas.getString("MARITAL_STATUS"));
			all.setADDRESS_1(haspas.getString("ADDRESS_1"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<listpolyres> getPolyResourceMstrByUser(String usermasterid) {
		List<listpolyres> all = new ArrayList<listpolyres>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select cp.CAREPROVIDER_ID, rm.RESOURCEMSTR_ID, ps.PERSON_NAME from RESOURCEMSTR rm "
				+ "inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = rm.CAREPROVIDER_ID and cp.DEFUNCT_IND = 'N' "
				+ "inner join person ps on ps.PERSON_ID = cp.PERSON_ID and ps.DEFUNCT_IND = 'N' "
				+ "inner join USERPROFILE up on up.PERSON_ID = ps.PERSON_ID and up.DEFUNCT_IND = 'N' "
				+ "inner join USERMSTR um on um.USERMSTR_ID = up.USERMSTR_ID and um.DEFUNCT_IND = 'N' "
				+ "where rm.DEFUNCT_IND = 'N' and um.USERMSTR_ID = '" + usermasterid + "'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			haspas.next();
			listpolyres lp = new listpolyres();
			lp.setUSERMSTR_ID(haspas.getBigDecimal("CAREPROVIDER_ID"));
			lp.setRESOURCEMSTR_ID(haspas.getBigDecimal("RESOURCEMSTR_ID"));
			lp.setPERSON_NAME(haspas.getString("PERSON_NAME"));
			all.add(lp);
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<Listqueueno> getPatientByQueueNo(String queueno) {
		// TODO Auto-generated method stub

		Connection connection = null;
		ResultSet rs = null;

		List<Listqueueno> all = new ArrayList<Listqueueno>();
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select vd.CAREPROVIDER_ID, c.CARD_NO, ps.PERSON_NAME  from visit v "
				+ "inner join visitregntype vd on vd.VISIT_ID = v.VISIT_ID "
				+ "inner join patient pt on pt.PATIENT_ID = v.PATIENT_ID "
				+ "inner join person ps on ps.PERSON_ID = pt.PERSON_ID "
				+ "inner join card c on c.PERSON_ID = ps.PERSON_ID "
				+ "where to_char(v.ADMISSION_DATETIME, 'dd-mm-yyyy') = to_char(sysdate, 'dd-mm-yyyy') "
				+ "and v.QUEUE_NO = '" + queueno
				+ "' and vd.defunct_ind = 'N' and v.PATIENT_CLASS<>'PTC114' and v.PATIENT_TYPE = 'PTY2'";
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(abdatabp);
			rs.next();
			Listqueueno lp = new Listqueueno();
			lp.setCAREPROVIDER_ID(rs.getString("CAREPROVIDER_ID"));
			lp.setCARD_NO(rs.getBigDecimal("CARD_NO"));
			lp.setPERSON_NAME(rs.getString("PERSON_NAME"));
			all.add(lp);
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<Listqueueno> getPatientByQueueNoUpdate(String queueno, String subspeciality) {
		// TODO Auto-generated method stub

		Connection connection = null;
		ResultSet rs = null;

		List<Listqueueno> all = new ArrayList<Listqueueno>();
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select vd.CAREPROVIDER_ID, c.CARD_NO, ps.PERSON_NAME  from visit v "
				+ "inner join visitregntype vd on vd.VISIT_ID = v.VISIT_ID "
				+ "inner join patient pt on pt.PATIENT_ID = v.PATIENT_ID "
				+ "inner join person ps on ps.PERSON_ID = pt.PERSON_ID "
				+ "inner join card c on c.PERSON_ID = ps.PERSON_ID "
				+ "where to_char(v.ADMISSION_DATETIME, 'dd-mm-yyyy') = to_char(sysdate, 'dd-mm-yyyy') "
				+ "and v.QUEUE_NO = '" + queueno + "' ";
		if (subspeciality != null && !subspeciality.equals(""))
			abdatabp += " and v.subspecialtymstr_id = ('" + subspeciality + "') ";
		abdatabp += " and vd.defunct_ind = 'N' and v.PATIENT_CLASS<>'PTC114' and v.PATIENT_TYPE = 'PTY2'";
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(abdatabp);
			rs.next();
			Listqueueno lp = new Listqueueno();
			lp.setCAREPROVIDER_ID(rs.getString("CAREPROVIDER_ID"));
			lp.setCARD_NO(rs.getBigDecimal("CARD_NO"));
			lp.setPERSON_NAME(rs.getString("PERSON_NAME"));
			all.add(lp);
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListResponseTime> getPharmacyResponseTime(String tglAwal, String tglAkhir) {
		// TODO Auto-generated method stub
		Connection connection = null;
		ResultSet rs = null;

		List<ListResponseTime> all = new ArrayList<ListResponseTime>();
		connection = DbConnection.getQueueInstance();
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select distinct q.queue_date, p.queue_id, p.queue_no, q.card_no, q.patient_name,p.enterqueue_datetime, p.startserve_datetime, p.endserve_datetime "
				+ "from pharmacyqueue p inner join queue q on q.queue_id = p.queue_id where p.created_at >=  '"
				+ tglAwal + "' and p.created_at < '" + tglAkhir + "' and q.location = 3 " + "order by p.queue_no asc";
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(abdatabp);
			while (rs.next()) {
				ListResponseTime lp = new ListResponseTime();
				lp.setQueueId(rs.getString("queue_id"));
				lp.setQueueDate(rs.getString("queue_date"));
				lp.setQueueNo(rs.getString("queue_no"));
				lp.setCardNo(rs.getString("card_no"));
				lp.setPatientName(rs.getString("patient_name"));
				lp.setEnterqueueDatetime(rs.getString("enterqueue_datetime"));
				lp.setStartserveDatetime(rs.getString("startserve_datetime"));
				lp.setEndserveDatetime(rs.getString("endserve_datetime"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<Listqueueno> getPatientByQueueNo2(String queueno2) {

		Connection connection = null;
		ResultSet rs = null;

		List<Listqueueno> all = new ArrayList<Listqueueno>();
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select vd.CAREPROVIDER_ID, c.CARD_NO, ps.PERSON_NAME  from visit v "
				+ "inner join visitregntype vd on vd.VISIT_ID = v.VISIT_ID "
				+ "inner join patient pt on pt.PATIENT_ID = v.PATIENT_ID "
				+ "inner join person ps on ps.PERSON_ID = pt.PERSON_ID "
				+ "inner join card c on c.PERSON_ID = ps.PERSON_ID "
				+ "where to_char(v.ADMISSION_DATETIME, 'dd-mm-yyyy') = to_char(sysdate, 'dd-mm-yyyy') "
				+ "and v.QUEUE_NO = '" + queueno2
				+ "' and vd.defunct_ind = 'N' and v.PATIENT_CLASS='PTC114' and v.PATIENT_TYPE = 'PTY2'";
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(abdatabp);
			rs.next();
			Listqueueno lp = new Listqueueno();
			lp.setCAREPROVIDER_ID(rs.getString("CAREPROVIDER_ID"));
			lp.setCARD_NO(rs.getBigDecimal("CARD_NO"));
			lp.setPERSON_NAME(rs.getString("PERSON_NAME"));
			all.add(lp);
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<Listqueueno> getPatientByQueueNo2Update(String queueno2, String subspeciality) {

		Connection connection = null;
		ResultSet rs = null;

		List<Listqueueno> all = new ArrayList<Listqueueno>();
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select vd.CAREPROVIDER_ID, c.CARD_NO, ps.PERSON_NAME  from visit v "
				+ "inner join visitregntype vd on vd.VISIT_ID = v.VISIT_ID "
				+ "inner join patient pt on pt.PATIENT_ID = v.PATIENT_ID "
				+ "inner join person ps on ps.PERSON_ID = pt.PERSON_ID "
				+ "inner join card c on c.PERSON_ID = ps.PERSON_ID "
				+ "where to_char(v.ADMISSION_DATETIME, 'dd-mm-yyyy') = to_char(sysdate, 'dd-mm-yyyy') "
				+ "and v.QUEUE_NO = '" + queueno2 + "'";
		if (subspeciality != null && !subspeciality.equals(""))
			abdatabp += " and v.subspecialtymstr_id = ('" + subspeciality + "') ";
		abdatabp += "and vd.defunct_ind = 'N' and v.PATIENT_CLASS='PTC114' and v.PATIENT_TYPE = 'PTY2'";
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(abdatabp);
			rs.next();
			Listqueueno lp = new Listqueueno();
			lp.setCAREPROVIDER_ID(rs.getString("CAREPROVIDER_ID"));
			lp.setCARD_NO(rs.getBigDecimal("CARD_NO"));
			lp.setPERSON_NAME(rs.getString("PERSON_NAME"));
			all.add(lp);
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static BpjsQueue addBpjsQueue(String queue_no) {
		Connection connection = DbConnection.getQueueInstance();
		BpjsQueue view = new BpjsQueue();
		Statement stmt = null;
		ResultSet row = null;
		try {
			String query = "select top 1 queue_id from queue WHERE queue_no='" + queue_no
					+ "' and location = 1 and queue_date = CONVERT(date, GETDATE()) order by queue_id desc ";

			if (connection == null)
				return null;
			stmt = connection.createStatement();

			try {
				row = stmt.executeQuery(query);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			row.next();
			String qid = row.getString("queue_id");

			String query2 = "INSERT INTO [dbo].[polyqueue] "
					+ "([queue_id],[enterqueue_datetime],[startserve_datetime],[endserve_datetime],[endserve_ind],[created_at], "
					+ "[created_by],[updated_at],[updated_by],[deleted])" + "VALUES (" + qid
					+ ",CURRENT_TIMESTAMP,null,null,0,CURRENT_TIMESTAMP,327759870,CURRENT_TIMESTAMP,327759870,0)";
			PreparedStatement stmt2 = connection.prepareStatement(query2);
			stmt2.executeUpdate();
			stmt2.close();

			String query3 = "INSERT INTO [dbo].[queue_detail] ([queue_id],[queue_type],[counter_id],"
					+ "[served_by],[start_serve_datetime],[end_serve_datetime],[deleted]) " + "VALUES(" + qid
					+ ",'1','51','327759870',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,0)";
			PreparedStatement stmt3 = connection.prepareStatement(query3);
			stmt3.executeUpdate();
			stmt3.close();

			view.setStatus("OK");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}

		return view;
	}

	public static RegData GetRegData(String regno) {
		Connection connection = DbConnection.getPooledConnection();
		RegData view = new RegData();
		Statement stmt = null;
		ResultSet row = null;
		try {
			String query = "SELECT main.REGN_NO, PERSON_NAME, cp_name, cashier_name, SUBSPECIALTY_DESC, JOB_NO, regn_type, ADDRESS, "
					+ "CREATED_DATETIME regn_date, CardNo, VISIT_NO, APPOINTMENT_TIME, "
					+ "to_char(( select count(vrt1.VISITREGNTYPE_ID) from visitregntype vrt1 where vrt1.EFFICTIVE_DATETIME >= trunc(main.EFFICTIVE_DATETIME) "
					+ "and vrt1.EFFICTIVE_DATETIME < trunc(main.EFFICTIVE_DATETIME)+1 and vrt1.VISITREGNTYPE_ID <= main.VISITREGNTYPE_ID ))||'('||SESSION_DESC||')' seq_no, "
					+ "decode(GHF_amt, null,0,GHF_amt) GHF_amt, decode(ZLF_amt, null,0,ZLF_amt) ZLF_amt, decode(total_amt, null,0,total_amt) total_amt, "
					+ "NVL(AUTO_PAY,0) AUTO_PAY, to_char(EFFICTIVE_DATETIME,'YYYY-MM-DD') EFFICTIVE_DATETIME, SESSION_DESC, sysdate printdate from( select vrt.REGN_NO, p.PERSON_NAME, pc.PERSON_NAME cp_name, "
					+ "pk.PERSON_NAME cashier_name, sspm.SUBSPECIALTY_DESC,  up.JOB_NO, "
					+ "pgcommon.fxGetCodeDesc(vrt.REGISTRATION_TYPE) regn_type,lm.ADDRESS, vrt.CREATED_DATETIME, "
					+ "pgPMI.fxGetCardNo(p.PERSON_ID) CardNo, v.VISIT_NO , vrt.REGISTRATION_TYPE, vrt.RESOURCEMSTR_ID, vrt.VISITREGNTYPE_ID, vrt.EFFICTIVE_DATETIME, sm.SESSION_DESC, "
					+ "vrt.SESSIONMSTR_ID, ram.APPOINTMENT_TIME "
					+ "from visitregntype vrt, userprofile up , visit v, patient pt, person p ,careprovider cp, person pc, person pk, subspecialtymstr sspm, LOCATIONMSTR lm, sessionmstr sm ,REGNAPPOINTMENT ram "
					+ "where v.VISIT_ID = vrt.VISIT_ID and vrt.REGN_NO = " + regno + " "
					+ "and up.USERMSTR_ID = vrt.CREATED_BY and pt.PATIENT_ID = v.PATIENT_ID "
					+ "and p.PERSON_ID = pt.PERSON_ID and pk.PERSON_ID = up.PERSON_ID and "
					+ "cp.CAREPROVIDER_ID(+) = vrt.CAREPROVIDER_ID and pc.PERSON_ID(+) = cp.PERSON_ID and "
					+ "sspm.SUBSPECIALTYMSTR_ID = vrt.SUBSPECIALTYMSTR_ID and lm.LOCATIONMSTR_ID = sspm.LOCATIONMSTR_ID and "
					+ "sm.SESSIONMSTR_ID = vrt.SESSIONMSTR_ID and ram.VISITREGNTYPE_ID(+) = vrt.VISITREGNTYPE_ID) main , "
					+ "(select  vrt.REGN_NO , sum(decode(icm.ITEM_TYPE, 'ITY12',0, pat.TXN_AMOUNT)) GHF_amt , sum(decode(iCM.ITEM_TYPE, 'ITY12' , pat.TXN_AMOUNT,0)) ZLF_amt , "
					+ "sum(pat.TXN_AMOUNT) total_amt , (select sum(decode(tcm1.TXN_CODE, 'OP_AUTO_PAY', -pat1.TXN_AMOUNT,0)) AUTO_PAY from PATIENTACCOUNTTXN pat1, TXNCODEMSTR tcm1 "
					+ "where PAT1.BILL_ID = PAT.BILL_ID and TCM1.TXNCODEMSTR_ID = PAT1.TXNCODEMSTR_ID) AUTO_PAY from visitregntype vrt,visitregntypecharge vrtc, patientaccounttxn pat, "
					+ "chargeitemmstr cim, itemsubcategorymstr iscm, itemcategorymstr icm, txncodemstr tcm where vrt.REGN_NO ="
					+ regno + " and vrtc.VISITREGNTYPE_ID = vrt.VISITREGNTYPE_ID "
					+ "and pat.PATIENTACCOUNTTXN_ID = vrtc.PATIENTACCOUNTTXN_ID and cim.TXNCODEMSTR_ID = pat.TXNCODEMSTR_ID and iscm.ITEMSUBCATEGORYMSTR_ID = cim.ITEMSUBCATEGORYMSTR_ID  "
					+ "and icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID and tcm.TXNCODEMSTR_ID = pat.TXNCODEMSTR_ID  "
					+ "group by vrt.REGN_NO, PAT.BILL_ID) amt where amt.REGN_NO(+) = main.REGN_NO";

			if (connection == null)
				return null;
			stmt = connection.createStatement();

			try {
				row = stmt.executeQuery(query);
			} catch (Exception e) {

			}

			row.next();
			view.setREGN_NO(row.getString("REGN_NO"));
			view.setPERSON_NAME(row.getString("PERSON_NAME"));
			view.setCP_NAME(row.getString("cp_name"));
			view.setCASHIER_NAME(row.getString("cashier_name"));
			view.setSUBSPECIALTY_DESC(row.getString("SUBSPECIALTY_DESC"));
			view.setJOB_NO(row.getString("JOB_NO"));
			view.setREGN_TYPE(row.getString("REGN_TYPE"));
			view.setADDRESS(row.getString("ADDRESS"));
			view.setREGN_DATE(row.getString("regn_date"));
			view.setCARDNO(row.getString("CardNo"));
			view.setVISIT_NO(row.getString("VISIT_NO"));
			view.setAPPOINTMENT_TIME(row.getString("APPOINTMENT_TIME"));
			view.setSEQ_NO(row.getString("seq_no"));
			view.setGHF_AMT(row.getString("GHF_amt"));
			view.setZLF_AMT(row.getString("ZLF_amt"));
			view.setTOTAL_AMT(row.getString("total_amt"));
			view.setAUTO_PAY(row.getString("AUTO_PAY"));
			view.setEFFICTIVE_DATETIME(row.getString("EFFICTIVE_DATETIME"));
			view.setSESSION_DESC(row.getString("SESSION_DESC"));
			view.setPRINTDATE(row.getString("printdate"));

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return view;
	}

	public static List<ListSelfReg> getSelfRegistration(String cardno, String careproviderid, String noantri,
			String userid) {
		// TODO Auto-generated method stub
		List<ListSelfReg> all = new ArrayList<ListSelfReg>();
		BaseResult result = new BaseResult();
		result.setResult(IConstant.IND_NO);
		result.setStatus(IConstant.STATUS_CONNECTED);
		String patid = "";
		String stkid = "";
		String patcal = "";
		String lokasi = "336581436";
		String prk = "";
		String gen = "";
		String prk2 = "";
		String prk3 = "";
		String prk4 = "";
		String prk5 = "";
		String prk6 = "";
		String prk7 = "";
		String prk8 = "";
		String ed = "";
		String qn = "";
		String qn2 = "";

		String qr1 = "SELECT RM.RESOURCEMSTR_ID, RM.CAREPROVIDER_ID, RM.SUBSPECIALTYMSTR_ID, RM.RESOURCE_NAME, RM.REGISTRATION_TYPE, RM.RESOURCE_QUEUE_IND, "
				+ "RM.RESOURCE_CHECKOUT_IND FROM RESOURCEMSTR RM WHERE RM.RESOURCEMSTR_ID= ? AND RM.DEFUNCT_IND = 'N'";
		try {
			List list = DbConnection.executeReader(DbConnection.KTHIS, qr1, new Object[] { careproviderid });

			for (int a = 0; a < list.size(); a++) {
				HashMap row = (HashMap) list.get(a);
				ListSelfReg lsg = new ListSelfReg();
				lsg.setRESOURCEMSTR_ID(row.get("RESOURCEMSTR_ID").toString());
				String resourceid = row.get("RESOURCEMSTR_ID").toString();
				lsg.setCAREPROVIDER_ID(row.get("CAREPROVIDER_ID").toString());
				String careid = row.get("CAREPROVIDER_ID").toString();
				lsg.setSUBSPECIALTYMSTR_ID(row.get("SUBSPECIALTYMSTR_ID").toString());
				String subid = row.get("SUBSPECIALTYMSTR_ID").toString();
				lsg.setRESOURCE_NAME(row.get("RESOURCE_NAME").toString());
				lsg.setREGISTRATION_TYPE(row.get("REGISTRATION_TYPE").toString());
				String regtyp = row.get("REGISTRATION_TYPE").toString();
				lsg.setRESOURCE_QUEUE_IND(row.get("RESOURCE_QUEUE_IND").toString());
				String rqi = row.get("RESOURCE_QUEUE_IND").toString();
				lsg.setRESOURCE_CHECKOUT_IND(row.get("RESOURCE_CHECKOUT_IND").toString());
				String rci = row.get("RESOURCE_CHECKOUT_IND").toString();

				String qr2 = "SELECT PS.PERSON_ID, PT.PATIENT_ID, CD.CARD_NO, PS.PERSON_NAME, pgCOMMON.fxGetCodeDesc(PS.SEX) as sex, "
						+ "TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)||' Tahun ' || "
						+ "TRUNC(months_between(sysdate,PS.BIRTH_DATE)-12*trunc(months_between(sysdate,PS.BIRTH_DATE)/12)) || ' Bulan' as AGE, "
						+ "PS.BIRTH_DATE, PS.STAKEHOLDER_ID, PT.PATIENT_CLASS FROM PERSON PS, PATIENT PT, CARD CD WHERE PT.PERSON_ID = PS.PERSON_ID AND "
						+ "CD.PERSON_ID = PS.PERSON_ID AND CD.CARD_NO = ?";
				List list2 = DbConnection.executeReader(DbConnection.KTHIS, qr2, new Object[] { cardno });
				for (int b = 0; b < list2.size(); b++) {
					HashMap row2 = (HashMap) list2.get(b);
					lsg.setPERSON_ID(row2.get("PERSON_ID").toString());
					lsg.setPATIENT_ID(row2.get("PATIENT_ID").toString());
					patid = row2.get("PATIENT_ID").toString();
					lsg.setCARD_NO(row2.get("CARD_NO").toString());
					lsg.setPERSON_NAME(row2.get("PERSON_NAME").toString());
					lsg.setSex(row2.get("SEX") != null ? row2.get("SEX").toString() : "");
					lsg.setAGE(row2.get("AGE").toString());
					lsg.setBIRTH_DATE(row2.get("BIRTH_DATE").toString());
					;
					lsg.setSTAKEHOLDER_ID(row2.get("STAKEHOLDER_ID").toString());
					stkid = row2.get("STAKEHOLDER_ID").toString();
					lsg.setPATIENT_CLASS(row2.get("PATIENT_CLASS").toString());
					patcal = "PTC114";

				}
				String user = userid;

				String qr3 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY FROM DUAL";
				List list3 = DbConnection.executeReader(DbConnection.KTHIS, qr3, new Object[] {});
				for (int c = 0; c < list3.size(); c++) {
					HashMap row3 = (HashMap) list3.get(c);
					lsg.setVISITID(row3.get("PRIMARYKEY").toString());
					prk = row3.get("PRIMARYKEY").toString();
				}

				String qrVisitNoOp = "SELECT pgSYSFUNC.fxGenSeqNo('VISIT_NO_OP',?,'PTY2') FROM DUAL";
				String visitNo = DbConnection.executeScalar(DbConnection.KTHIS, qrVisitNoOp, new Object[] { user })
						.toString();

				String qr4 = "INSERT INTO VISIT (VISIT_ID, PATIENT_ID, SUBSPECIALTYMSTR_ID, ADMITTED_AT, ADMITTED_BY, LAST_UPDATED_BY, ADMISSION_DATETIME, "
						+ "LAST_UPDATED_DATETIME, SPECIAL_CARE_IND, HEALTH_BOOK_FEE_IND, CARD_FEE_IND, PATIENT_TYPE, PATIENT_CLASS, VISIT_TYPE, ADMIT_STATUS, ADMIT_CLASS, "
						+ "ADMITTING_SOURCE, VISIT_NO, LAST_VISIT_NO, IPT_VISIT_TO_DATE, OPT_VISIT_TO_DATE, CONSULT_STATUS, MA_TYPE, ADMISSION_TIMES, ENTITYMSTR_ID, "
						+ "IN_PATH_IND, QUEUE_NO) VALUES (?, ?, ?, ?, ?, ?, (SELECT sysdate from DUAL), "
						+ "(SELECT sysdate from DUAL), 'N', 'N', 'N', 'PTY2', ?, 'VST1', 'AST1', 'ACL1', 'ADSNORM', "
						+ "?, "
						+ "(SELECT MAX(V.VISIT_NO)FROM VISIT V WHERE V.PATIENT_ID = ?),0,0,'CNT1','MATN',1,1,'N',?)";

				Object[] input = { prk, patid, subid, lokasi, user, user, patcal, visitNo, patid, noantri };
				DbConnection.executeQuery(DbConnection.KTHIS, qr4, input);

				String qr5 = "SELECT VISIT_NO FROM VISIT WHERE VISIT_ID = ?";
				List list5 = DbConnection.executeReader(DbConnection.KTHIS, qr5, new Object[] { prk });
				for (int e = 0; e < list5.size(); e++) {
					HashMap row5 = (HashMap) list5.get(e);
					lsg.setLAST_VISIT_NO(row5.get("VISIT_NO").toString());
					gen = row5.get("VISIT_NO").toString();
				}

				String qr6 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY2 FROM DUAL";
				List list6 = DbConnection.executeReader(DbConnection.KTHIS, qr6, new Object[] {});
				for (int f = 0; f < list6.size(); f++) {
					HashMap row6 = (HashMap) list6.get(f);
					lsg.setPRIMARYKEY2(row6.get("PRIMARYKEY2").toString());
					prk2 = row6.get("PRIMARYKEY2").toString();
				}

				String qr7 = "INSERT INTO PATIENTTYPECLASSHISTORY(PATIENTTYPECLASSHISTORY_ID, VISIT_ID, NEW_PATIENT_TYPE, NEW_PATIENT_CLASS, START_DATETIME, CREATED_BY, "
						+ "CREATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME ) VALUES(?, ?, 'PTY2', ?, (SELECT sysdate from DUAL), "
						+ "?, (SELECT sysdate from DUAL), ?, (SELECT sysdate from DUAL) )";
				Object[] input2 = { prk2, prk, patcal, user, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr7, input2);

				String qr8 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY3 FROM DUAL";
				List list8 = DbConnection.executeReader(DbConnection.KTHIS, qr8, new Object[] {});
				for (int g = 0; g < list8.size(); g++) {
					HashMap row8 = (HashMap) list8.get(g);
					lsg.setPRIMARYKEY3(row8.get("PRIMARYKEY3").toString());
					prk3 = row8.get("PRIMARYKEY3").toString();
				}

				String qrRegnNo = "SELECT pgSYSFUNC.fxGenSeqNo('REGN_NO',?,NULL) FROM DUAL";
				String regnNo = DbConnection.executeScalar(DbConnection.KTHIS, qrRegnNo, new Object[] { user })
						.toString();

				String qr9 = "INSERT INTO VISITREGNTYPE (VISITREGNTYPE_ID, VISIT_ID, REGISTRATION_TYPE, RESOURCEMSTR_ID, CREATED_BY, CREATED_DATETIME, DEFUNCT_IND,  "
						+ "LAST_UPDATED_BY, LAST_UPDATED_DATETIME, CAREPROVIDER_ID, SUBSPECIALTYMSTR_ID, CREATED_AT, SESSIONMSTR_ID, REGN_NO, REGISTRATION_METHOD, "
						+ "EFFICTIVE_DATETIME ) VALUES (?, ?, ?, ?, ?, (SELECT sysdate from DUAL), 'N', ?, "
						+ "(SELECT sysdate from DUAL), ?, ?, ?, '1', ?, " + "'RGM1', (SELECT sysdate from DUAL))";
				Object[] input3 = { prk3, prk, regtyp, resourceid, user, user, careid, subid, lokasi, regnNo };
				DbConnection.executeQuery(DbConnection.KTHIS, qr9, input3);

				String qr10 = "select REGN_NO from VISITREGNTYPE where VISITREGNTYPE_ID=?";
				List list10 = DbConnection.executeReader(DbConnection.KTHIS, qr10, new Object[] { prk3 });
				for (int h = 0; h < list10.size(); h++) {
					HashMap row10 = (HashMap) list10.get(h);
					lsg.setREGN_NO(row10.get("REGN_NO").toString());
				}

				String qr11 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY4 FROM DUAL";
				List list11 = DbConnection.executeReader(DbConnection.KTHIS, qr11, new Object[] {});
				for (int i = 0; i < list11.size(); i++) {
					HashMap row11 = (HashMap) list11.get(i);
					lsg.setPRIMARYKEY4(row11.get("PRIMARYKEY4").toString());
					prk4 = row11.get("PRIMARYKEY4").toString();
				}

				String qr12 = "INSERT INTO REGNVISITACTIVATION (REGNVISITACTIVATION_ID, VISITREGNTYPE_ID, VISIT_ID, CREATED_BY, CREATED_DATETIME, "
						+ "CREATED_LOCATIONMSTR_ID ) VALUES (?, ?, ?, ?, (SELECT sysdate from DUAL), ?)";
				Object[] input4 = { prk4, prk3, prk, user, lokasi };
				DbConnection.executeQuery(DbConnection.KTHIS, qr12, input4);

				String qr13 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY5 FROM DUAL";
				List list13 = DbConnection.executeReader(DbConnection.KTHIS, qr13, new Object[] {});
				for (int j = 0; j < list13.size(); j++) {
					HashMap row13 = (HashMap) list13.get(j);
					lsg.setPRIMARYKEY5(row13.get("PRIMARYKEY5").toString());
					prk5 = row13.get("PRIMARYKEY5").toString();
				}

				String qr14 = "INSERT INTO VISITSUBSPECIALTYHISTORY (VISITSUBSPECIALTYHISTORY_ID, SUBSPECIALTYMSTR_ID, VISIT_ID, LAST_UPDATED_BY, EFFECTIVE_DATETIME, "
						+ "LAST_UPDATED_DATETIME, DEFUNCT_IND) VALUES (?, ?, ?, ?, (SELECT sysdate from DUAL), (SELECT sysdate from DUAL), 'N')";
				Object[] input5 = { prk5, subid, prk, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr14, input5);

				String qr15 = "SELECT TO_CHAR(EFFECTIVE_DATETIME,'ddmmyyyy') as efek_date FROM VISITSUBSPECIALTYHISTORY WHERE VISITSUBSPECIALTYHISTORY_ID= ?";
				List list15 = DbConnection.executeReader(DbConnection.KTHIS, qr15, new Object[] { prk5 });
				for (int k = 0; k < list15.size(); k++) {
					HashMap row15 = (HashMap) list15.get(k);
					lsg.setEfek_date(row15.get("EFEK_DATE").toString());
					ed = row15.get("EFEK_DATE").toString();
				}

				String qr16 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY6 FROM DUAL";
				List list16 = DbConnection.executeReader(DbConnection.KTHIS, qr16, new Object[] {});
				for (int l = 0; l < list16.size(); l++) {
					HashMap row16 = (HashMap) list16.get(l);
					lsg.setPRIMARYKEY6(row16.get("PRIMARYKEY6").toString());
					prk6 = row16.get("PRIMARYKEY6").toString();
				}

				String qr17 = "INSERT INTO PATIENTACCOUNT (PATIENTACCOUNT_ID, VISIT_ID, LAST_UPDATED_BY, ACCOUNT_BALANCE, BILL_SIZE, FROZEN_ACCOUNT_BALANCE, "
						+ "FROZEN_BILL_SIZE, LAST_UPDATED_DATETIME, OPEN_CARD_IND, ACCOUNT_CHANGED_IND, ACCOUNT_STATUS, "
						+ "PATIENT_ACCOUNT_NO, DEPOSIT_BALANCE) VALUES (?, ?, ?,0,0,0,0,(SELECT sysdate from DUAL),'N','N','ACS1', ?,0)";
				Object[] input6 = { prk6, prk, user, gen };
				DbConnection.executeQuery(DbConnection.KTHIS, qr17, input6);

				String qr18 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY7 FROM DUAL";
				List list18 = DbConnection.executeReader(DbConnection.KTHIS, qr18, new Object[] {});
				for (int m = 0; m < list18.size(); m++) {
					HashMap row18 = (HashMap) list18.get(m);
					lsg.setPRIMARYKEY7(row18.get("PRIMARYKEY7").toString());
					prk7 = row18.get("PRIMARYKEY7").toString();
				}

				String qr19 = "INSERT INTO ACCOUNTDEBTOR (ACCOUNTDEBTOR_ID, STAKEHOLDER_ID, PATIENTACCOUNT_ID, PAYER_SEQ, CONTRACT_BALANCE, PAID_AMOUNT, LAST_BILL_AMOUNT, "
						+ "FROZEN_CONTRACT_BALANCE, FROZEN_CONTRACT_SIZE, CN_YTD_SELFPAID_AMOUNT, CN_LAST_CLAIM_AMOUNT, CN_SELF_PAYABLE_AMOUNT, DEBTOR_CHANGED_IND, "
						+ "FINANCIAL_CLASS, CLAIM_POLICY_CODE, CN_HEALTHCARE_PLAN_IND, CN_MAJOR_DISEASE_IND, CN_APPOINTED_HOSPITAL_IND, DEFUNCT_IND, LAST_UPDATED_BY, "
						+ "LAST_UPDATED_DATETIME) VALUES (?, ?, ?,10,0,0,0,0,0,0,0,0,'N','FCC1','SELF','N','N','N','N', ?, "
						+ "(SELECT sysdate from DUAL))";
				Object[] input7 = { prk7, stkid, prk6, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr19, input7);

				if (rqi.equals("Y") && rci.equals("N")) {

					String qr20 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY8 FROM DUAL";
					List list20 = DbConnection.executeReader(DbConnection.KTHIS, qr20, new Object[] {});
					for (int n = 0; n < list20.size(); n++) {
						HashMap row20 = (HashMap) list20.get(n);
						lsg.setPRIMARYKEY8(row20.get("PRIMARYKEY8").toString());
						prk8 = row20.get("PRIMARYKEY8").toString();
					}

					String qr21 = "SELECT COUNT(1) as qn FROM VISITREGNTYPE VRT WHERE VRT.EFFICTIVE_DATETIME >= to_date(?,'ddmmyyyy') AND VRT.EFFICTIVE_DATETIME < "
							+ "to_date(?,'ddmmyyyy') + 1 AND VRT.VISITREGNTYPE_ID <= ?";
					Object[] input8 = { ed, ed, prk3 };
					List list21 = DbConnection.executeReader(DbConnection.KTHIS, qr21, input8);
					for (int o = 0; o < list21.size(); o++) {
						HashMap row21 = (HashMap) list21.get(o);
						lsg.setQn(row21.get("qn").toString());
						qn = row21.get("qn").toString();
					}

					String qr22 = "SELECT COUNT(1) as qn2 FROM OPWAITINGQUEUE WQ WHERE WQ.RESOURCEMSTR_ID = ? AND WQ.ENQUEUE_DATETIME <= SYSDATE";
					List list22 = DbConnection.executeReader(DbConnection.KTHIS, qr22, new Object[] { resourceid });
					for (int p = 0; p < list22.size(); p++) {
						HashMap row22 = (HashMap) list22.get(p);
						lsg.setQn2(row22.get("qn2").toString());
						qn2 = row22.get("qn2").toString();
					}

					String qr23 = "INSERT INTO OPWAITINGQUEUE (OPWAITINGQUEUE_ID, RESOURCEMSTR_ID, ENQUEUE_DATETIME, VISIT_ID, QUEUE_NO, QUEUE_SEQ_NO, STATUS, CAREPROVIDER_ID, "
							+ "CREATED_BY,   CREATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME ) VALUES "
							+ "(?,?, SYSDATE,?,?,?,'OQS1',?,?, SYSDATE,?,SYSDATE)";
					Object[] input9 = { prk8, resourceid, prk, qn, qn2, careid, user, user };
					DbConnection.executeQuery(DbConnection.KTHIS, qr23, input9);
				}

				String qr24 = "SELECT to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS') as tgreg FROM DUAL";
				List list24 = DbConnection.executeReader(DbConnection.KTHIS, qr24, new Object[] {});
				for (int q = 0; q < list24.size(); q++) {
					HashMap row24 = (HashMap) list24.get(q);
					lsg.setTgreg(row24.get("TGREG").toString());

				}
				all.add(lsg);
				result.setResult(IConstant.IND_YES);
			}
			result.setStatus(IConstant.STATUS_SUCCESSFULL);
		} catch (SQLException e) {
			e.printStackTrace();
			result.setStatus(IConstant.STATUS_ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(IConstant.STATUS_ERROR);
		} finally {
		}
		return all;
	}

	public static IntegratedKthis getInsBrid(String cardno, String bpjsno, String sepno, String vid,
			String careproviderid, String noantri, String userid) {
		// TODO Auto-generated method stub
		IntegratedKthis all = new IntegratedKthis();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		ResultSet haspas2 = null;

		if (connection == null)
			return null;
		Statement stmt = null;
		Statement stmt2 = null;

		String qr1 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY FROM DUAL";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(qr1);
			haspas.next();
			String prk = haspas.getString("PRIMARYKEY");

			String qr2 = "INSERT INTO SEPBPJS (SEPBPJS_ID,CARD_NO,BPJS_NO,SEP_NO,VISIT_ID,CAREPROVIDER_ID,QUEUE_NO,CREATED_BY,CREATED_AT,MODIFIED_BY,MODIFIED_AT,DEFUNCT_IND) "
					+ "VALUES ('" + prk + "','" + cardno + "','" + bpjsno + "','" + sepno + "','" + vid + "','"
					+ careproviderid + "','" + noantri + "','" + userid + "',SYSDATE,'" + userid + "',SYSDATE,'N')";
			stmt2 = connection.createStatement();
			haspas2 = stmt2.executeQuery(qr2);
			all.setStatus("OK");
			haspas2.close();
			stmt2.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (haspas2 != null)
				try {
					haspas2.close();
				} catch (Exception ignore) {
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static IntegratedKthis getInsBrid(String cardno, String bpjsno, String sepno, String vid,
			String careproviderid, String noantri, String userid, String lakaLantas) {
		// TODO Auto-generated method stub
		IntegratedKthis all = new IntegratedKthis();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		ResultSet haspas2 = null;

		if (connection == null)
			return null;
		Statement stmt = null;
		Statement stmt2 = null;

		String qr1 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY FROM DUAL";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(qr1);
			haspas.next();
			String prk = haspas.getString("PRIMARYKEY");

			String qr2 = "INSERT INTO SEPBPJS (SEPBPJS_ID,CARD_NO,BPJS_NO,SEP_NO,VISIT_ID,CAREPROVIDER_ID,QUEUE_NO,CREATED_BY,CREATED_AT,MODIFIED_BY,MODIFIED_AT,DEFUNCT_IND, LAKALANTAS) "
					+ "VALUES ('" + prk + "','" + cardno + "','" + bpjsno + "','" + sepno + "','" + vid + "','"
					+ careproviderid + "','" + noantri + "','" + userid + "',SYSDATE,'" + userid + "',SYSDATE,'N', '"
					+ lakaLantas + "')";
			stmt2 = connection.createStatement();
			haspas2 = stmt2.executeQuery(qr2);
			all.setStatus("OK");
			haspas2.close();
			stmt2.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (haspas2 != null)
				try {
					haspas2.close();
				} catch (Exception ignore) {
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static IntegratedKthis getInsBrid2(String cardno, String bpjsno, String sepno, String vid,
			String resourceMstrId, String noantri, String userid, String lakaLantas) {
		// TODO Auto-generated method stub
		IntegratedKthis all = new IntegratedKthis();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		ResultSet haspas2 = null;
		ResultSet haspas3 = null;

		if (connection == null)
			return null;
		Statement stmt = null;
		Statement stmt2 = null;
		Statement stmt3 = null;

		String qryCareProvider = "SELECT RM.CAREPROVIDER_ID FROM RESOURCEMSTR RM WHERE RM.RESOURCEMSTR_ID="
				+ resourceMstrId + " and rownum =1 ";
		String qr1 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY FROM DUAL";

		try {
			stmt3 = connection.createStatement();
			haspas3 = stmt3.executeQuery(qryCareProvider);
			haspas3.next();
			BigDecimal careProviderId = haspas3.getBigDecimal("CAREPROVIDER_ID");
			if (careProviderId != null) {
				String value = careProviderId.toString();
				stmt = connection.createStatement();
				haspas = stmt.executeQuery(qr1);
				haspas.next();
				String prk = haspas.getString("PRIMARYKEY");

				String qr2 = "INSERT INTO SEPBPJS (SEPBPJS_ID,CARD_NO,BPJS_NO,SEP_NO,VISIT_ID,CAREPROVIDER_ID,QUEUE_NO,CREATED_BY,CREATED_AT,MODIFIED_BY,MODIFIED_AT,DEFUNCT_IND, LAKALANTAS) "
						+ "VALUES ('" + prk + "','" + cardno + "','" + bpjsno + "','" + sepno + "','" + vid + "','"
						+ value + "','" + noantri + "','" + userid + "',SYSDATE,'" + userid + "',SYSDATE,'N', '"
						+ lakaLantas + "')";
				stmt2 = connection.createStatement();
				haspas2 = stmt2.executeQuery(qr2);
				all.setStatus("OK");
				haspas2.close();
				stmt2.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (haspas2 != null)
				try {
					haspas2.close();
				} catch (Exception ignore) {
				}
			if (haspas3 != null)
				try {
					haspas3.close();
				} catch (Exception ignore) {
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (stmt3 != null)
				try {
					stmt3.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static IntegratedKthis getInsSign(String mrid, String vid, String sign, String userid) {
		// TODO Auto-generated method stub
		IntegratedKthis all = new IntegratedKthis();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		ResultSet haspas2 = null;

		if (connection == null)
			return null;
		Statement stmt = null;
		Statement stmt2 = null;

		String qr1 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY FROM DUAL";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(qr1);
			haspas.next();
			String prk = haspas.getString("PRIMARYKEY");

			String data = sign;
			byte[] signature = data.getBytes();

			String qr2 = "INSERT INTO MEDICALRESUMESIGNATURE (MEDICALRESUMESIGNATURE_ID,MEDICAL_RESUME_ID,VISIT_ID,SIGNATURE,CREATED_BY,CREATED_AT,MODIFIED_BY,MODIFIED_AT,DEFUNCT_IND) "
					+ "VALUES ('" + prk + "','" + mrid + "','" + vid + "'," + signature + ",'" + userid + "',SYSDATE,'"
					+ userid + "',SYSDATE,'N')";
			stmt2 = connection.createStatement();
			haspas2 = stmt2.executeQuery(qr2);
			all.setStatus("OK");
			haspas2.close();
			stmt2.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (haspas2 != null)
				try {
					haspas2.close();
				} catch (Exception ignore) {
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static IntegratedKthis sendFileResume(UploadResume resumeform) {

		IntegratedKthis all = new IntegratedKthis();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		ResultSet haspas2 = null;
		ResultSet haspas3 = null;

		if (connection == null)
			return null;
		Statement stmt = null;
		Statement stmt2 = null;
		PreparedStatement ps = null;
		PreparedStatement ps0 = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps3 = null;

		String qr1 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY FROM DUAL";
		try {
			String resumeId = resumeform.getMrid();
			String sqltotal = "SELECT COUNT(*) as total FROM MEDICALRESUMESIGNATURE WHERE MEDICAL_RESUME_ID=?";
			ps3 = connection.prepareStatement(sqltotal);
			ps3.setEscapeProcessing(true);
			ps3.setQueryTimeout(60000);
			ps3.setString(1, resumeId);
			haspas3 = ps3.executeQuery();
			haspas3.next();
			String jum = haspas3.getString("total");
			if (!jum.equals("0")) {
				String sqlFind = "SELECT MEDICALRESUMESIGNATURE_ID,MEDICAL_RESUME_ID,SIGNATURE FROM MEDICALRESUMESIGNATURE WHERE MEDICAL_RESUME_ID=?";
				ps0 = connection.prepareStatement(sqlFind);
				ps0.setEscapeProcessing(true);
				ps0.setQueryTimeout(60000);
				ps0.setString(1, resumeId);
				haspas2 = ps0.executeQuery();
				while (haspas2.next()) {
					String id = haspas2.getString("MEDICALRESUMESIGNATURE_ID");
					if (!resumeform.getSign().equals(haspas2.getBytes("SIGNATURE"))) {
						String sql2 = "UPDATE MEDICALRESUMESIGNATURE SET DEFUNCT_IND='Y' WHERE MEDICALRESUMESIGNATURE_ID=?";
						ps1 = connection.prepareStatement(sql2);
						ps1.setString(1, id);
						ps1.execute();
						ps1.close();
					}
				}
			}

			stmt = connection.createStatement();
			haspas = stmt.executeQuery(qr1);
			haspas.next();
			String prk = haspas.getString("PRIMARYKEY");
			String sql3 = "INSERT INTO MEDICALRESUMESIGNATURE (MEDICALRESUMESIGNATURE_ID,MEDICAL_RESUME_ID,VISIT_ID,SIGNATURE,CREATED_BY,CREATED_AT,MODIFIED_BY,MODIFIED_AT,DEFUNCT_IND) "
					+ " VALUES (?,?,?,?,?,SYSDATE,?,SYSDATE,'N')";
			ps = connection.prepareStatement(sql3);
			ps.setString(1, prk);
			ps.setString(2, resumeform.getMrid());
			ps.setString(3, resumeform.getVid());
			ps.setBytes(4, resumeform.getSign());
			ps.setString(5, resumeform.getUserid());
			ps.setString(6, resumeform.getUserid());

			ps.execute();
			all.setStatus("OK");
			ps.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (haspas2 != null)
				try {
					haspas2.close();
				} catch (Exception ignore) {
				}
			if (haspas3 != null)
				try {
					haspas3.close();
				} catch (Exception ignore) {
				}
			if (ps != null)
				try {
					ps.close();
				} catch (SQLException e) {
				}
			if (ps0 != null)
				try {
					ps0.close();
				} catch (SQLException e) {
				}
			if (ps1 != null)
				try {
					ps1.close();
				} catch (SQLException e) {
				}
			if (ps3 != null)
				try {
					ps3.close();
				} catch (SQLException e) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListSelfReg> getSelfRegistration2(String cardno, String careproviderid, String noantri,
			String userid) {
		// TODO Auto-generated method stub
		List<ListSelfReg> all = new ArrayList<ListSelfReg>();
		String sequenceStatus = null;
		String patid = "";
		String stkid = "";
		String patcal = "";
		String lokasi = "311177558";
		String prk = "";
		String gen = "";
		String prk2 = "";
		String prk3 = "";
		String prk4 = "";
		String prk5 = "";
		String prk6 = "";
		String prk7 = "";
		String prk8 = "";
		String ed = "";
		String qn = "";
		String qn2 = "";

		String qr1 = "SELECT RM.RESOURCEMSTR_ID, RM.CAREPROVIDER_ID, RM.SUBSPECIALTYMSTR_ID, RM.RESOURCE_NAME, RM.REGISTRATION_TYPE, RM.RESOURCE_QUEUE_IND, "
				+ "RM.RESOURCE_CHECKOUT_IND FROM RESOURCEMSTR RM WHERE RM.RESOURCEMSTR_ID=? AND RM.DEFUNCT_IND = 'N'";
		try {
			List list = DbConnection.executeReader(DbConnection.KTHIS, qr1, new Object[] { careproviderid });

			for (int a = 0; a < list.size(); a++) {
				HashMap row = (HashMap) list.get(a);
				ListSelfReg lsg = new ListSelfReg();
				lsg.setRESOURCEMSTR_ID(row.get("RESOURCEMSTR_ID").toString());
				String resourceid = row.get("RESOURCEMSTR_ID").toString();
				lsg.setCAREPROVIDER_ID(row.get("CAREPROVIDER_ID").toString());
				String careid = row.get("CAREPROVIDER_ID").toString();
				lsg.setSUBSPECIALTYMSTR_ID(row.get("SUBSPECIALTYMSTR_ID").toString());
				String subid = row.get("SUBSPECIALTYMSTR_ID").toString();
				lsg.setRESOURCE_NAME(row.get("RESOURCE_NAME").toString());
				lsg.setREGISTRATION_TYPE(row.get("REGISTRATION_TYPE").toString());
				String regtyp = row.get("REGISTRATION_TYPE").toString();
				lsg.setRESOURCE_QUEUE_IND(row.get("RESOURCE_QUEUE_IND").toString());
				String rqi = row.get("RESOURCE_QUEUE_IND").toString();
				lsg.setRESOURCE_CHECKOUT_IND(row.get("RESOURCE_CHECKOUT_IND").toString());
				String rci = row.get("RESOURCE_CHECKOUT_IND").toString();

				String qr2 = "SELECT PS.PERSON_ID, PT.PATIENT_ID, CD.CARD_NO, PS.PERSON_NAME, pgCOMMON.fxGetCodeDesc(PS.SEX) as sex, "
						+ "TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)||' Tahun ' || "
						+ "TRUNC(months_between(sysdate,PS.BIRTH_DATE)-12*trunc(months_between(sysdate,PS.BIRTH_DATE)/12)) || ' Bulan' as AGE, "
						+ "PS.BIRTH_DATE, PS.STAKEHOLDER_ID, PT.PATIENT_CLASS FROM PERSON PS, PATIENT PT, CARD CD WHERE PT.PERSON_ID = PS.PERSON_ID AND "
						+ "CD.PERSON_ID = PS.PERSON_ID AND CD.CARD_NO = ?";
				List list2 = DbConnection.executeReader(DbConnection.KTHIS, qr2, new Object[] { cardno });
				for (int b = 0; b < list2.size(); b++) {
					HashMap row2 = (HashMap) list2.get(b);
					lsg.setPERSON_ID(row2.get("PERSON_ID").toString());
					lsg.setPATIENT_ID(row2.get("PATIENT_ID").toString());
					patid = row2.get("PATIENT_ID").toString();
					lsg.setCARD_NO(row2.get("CARD_NO").toString());
					lsg.setPERSON_NAME(row2.get("PERSON_NAME").toString());
					lsg.setSex(row2.get("SEX").toString());
					lsg.setAGE(row2.get("AGE").toString());
					lsg.setBIRTH_DATE(row2.get("BIRTH_DATE").toString());
					;
					lsg.setSTAKEHOLDER_ID(row2.get("STAKEHOLDER_ID").toString());
					stkid = row2.get("STAKEHOLDER_ID").toString();
					lsg.setPATIENT_CLASS(row2.get("PATIENT_CLASS").toString());
					patcal = row2.get("PATIENT_CLASS").toString();
				}
				String user = userid;

				String qr3 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY FROM DUAL";
				String primarykey = DbConnection.executeScalar(DbConnection.KTHIS, qr3, new Object[] {}).toString();
				lsg.setVISITID(primarykey);
				prk = primarykey;

				sequenceStatus = "1";

				String qr4 = "INSERT INTO VISIT (VISIT_ID, PATIENT_ID, SUBSPECIALTYMSTR_ID, ADMITTED_AT, ADMITTED_BY, LAST_UPDATED_BY, ADMISSION_DATETIME, "
						+ "LAST_UPDATED_DATETIME, SPECIAL_CARE_IND, HEALTH_BOOK_FEE_IND, CARD_FEE_IND, PATIENT_TYPE, PATIENT_CLASS, VISIT_TYPE, ADMIT_STATUS, ADMIT_CLASS, "
						+ "ADMITTING_SOURCE, VISIT_NO, LAST_VISIT_NO, IPT_VISIT_TO_DATE, OPT_VISIT_TO_DATE, CONSULT_STATUS, MA_TYPE, ADMISSION_TIMES, ENTITYMSTR_ID, "
						+ "IN_PATH_IND, QUEUE_NO, QUEUE_SEQUENCE) VALUES (?, ?, ?, ?, ?, ?, (SELECT sysdate from DUAL), "
						+ "(SELECT sysdate from DUAL), 'N', 'N', 'N', 'PTY2', ?, 'VST1', 'AST1', 'ACL1', 'ADSNORM', "
						+ "(SELECT pgSYSFUNC.fxGenSeqNo('VISIT_NO_OP',?,'PTY2') FROM DUAL), "
						+ "(SELECT MAX(V.VISIT_NO)FROM VISIT V WHERE V.PATIENT_ID = ?),0,0,'CNT1','MATN',1,1,'N',?,?)";
				Object[] input = { prk, patid, subid, lokasi, user, user, patcal, user, patid, noantri,
						sequenceStatus };
				DbConnection.executeQuery(DbConnection.KTHIS, qr4, input);

				String qr5 = "SELECT VISIT_NO FROM VISIT WHERE VISIT_ID = ?";
				String visit_no = DbConnection.executeScalar(DbConnection.KTHIS, qr5, new Object[] { prk }).toString();
				lsg.setLAST_VISIT_NO(visit_no);
				gen = visit_no;

				String qr6 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY2 FROM DUAL";
				String primarykey2 = DbConnection.executeScalar(DbConnection.KTHIS, qr6, new Object[] {}).toString();
				lsg.setPRIMARYKEY2(primarykey2);
				prk2 = primarykey2;

				String qr7 = "INSERT INTO PATIENTTYPECLASSHISTORY(PATIENTTYPECLASSHISTORY_ID, VISIT_ID, NEW_PATIENT_TYPE, NEW_PATIENT_CLASS, START_DATETIME, CREATED_BY, "
						+ "CREATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME ) VALUES(?, ?, 'PTY2', ?, (SELECT sysdate from DUAL), "
						+ "?, (SELECT sysdate from DUAL), ?, (SELECT sysdate from DUAL) )";
				Object[] input2 = { prk2, prk, patcal, user, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr7, input2);

				String qr8 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY3 FROM DUAL";
				String primarykey3 = DbConnection.executeScalar(DbConnection.KTHIS, qr8, new Object[] {}).toString();
				lsg.setPRIMARYKEY3(primarykey3);
				prk3 = primarykey3;

				String qr9 = "INSERT INTO VISITREGNTYPE (VISITREGNTYPE_ID, VISIT_ID, REGISTRATION_TYPE, RESOURCEMSTR_ID, CREATED_BY, CREATED_DATETIME, DEFUNCT_IND,  "
						+ "LAST_UPDATED_BY, LAST_UPDATED_DATETIME, CAREPROVIDER_ID, SUBSPECIALTYMSTR_ID, CREATED_AT, SESSIONMSTR_ID, REGN_NO, REGISTRATION_METHOD, "
						+ "EFFICTIVE_DATETIME ) VALUES (?, ?, ?, ?, ?, (SELECT sysdate from DUAL), 'N', ?, "
						+ "(SELECT sysdate from DUAL), ?, ?, ?, '1', (SELECT pgSYSFUNC.fxGenSeqNo('REGN_NO',?,NULL) FROM DUAL), "
						+ "'RGM1', (SELECT sysdate from DUAL))";
				Object[] input3 = { prk3, prk, regtyp, resourceid, user, user, careid, subid, lokasi, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr9, input3);

				String qr10 = "select REGN_NO from VISITREGNTYPE where VISITREGNTYPE_ID= ?";
				String regn_no = DbConnection.executeScalar(DbConnection.KTHIS, qr10, new Object[] { prk3 }).toString();
				lsg.setREGN_NO(regn_no);

				String qr11 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY4 FROM DUAL";
				String primarykey4 = DbConnection.executeScalar(DbConnection.KTHIS, qr11, new Object[] {}).toString();
				lsg.setPRIMARYKEY4(primarykey4);
				prk4 = primarykey4;

				String qr12 = "INSERT INTO REGNVISITACTIVATION (REGNVISITACTIVATION_ID, VISITREGNTYPE_ID, VISIT_ID, CREATED_BY, CREATED_DATETIME, "
						+ "CREATED_LOCATIONMSTR_ID ) VALUES (?, ?, ?, ?, (SELECT sysdate from DUAL), ?)";

				Object[] input4 = { prk4, prk3, prk, user, lokasi };
				DbConnection.executeQuery(DbConnection.KTHIS, qr12, input4);

				String qr13 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY5 FROM DUAL";
				String primarykey5 = DbConnection.executeScalar(DbConnection.KTHIS, qr13, new Object[] {}).toString();
				lsg.setPRIMARYKEY5(primarykey5);
				prk5 = primarykey5;

				String qr14 = "INSERT INTO VISITSUBSPECIALTYHISTORY (VISITSUBSPECIALTYHISTORY_ID, SUBSPECIALTYMSTR_ID, VISIT_ID, LAST_UPDATED_BY, EFFECTIVE_DATETIME, "
						+ "LAST_UPDATED_DATETIME, DEFUNCT_IND) VALUES (?, ?, ?, ?, (SELECT sysdate from DUAL), (SELECT sysdate from DUAL), 'N')";
				Object[] input5 = { prk5, subid, prk, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr14, input5);

				String qr15 = "SELECT TO_CHAR(EFFECTIVE_DATETIME,'ddmmyyyy') as efek_date FROM VISITSUBSPECIALTYHISTORY WHERE VISITSUBSPECIALTYHISTORY_ID= ?";
				String efek_date = DbConnection.executeScalar(DbConnection.KTHIS, qr15, new Object[] { prk5 })
						.toString();
				lsg.setEfek_date(efek_date);
				ed = efek_date;

				String qr16 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY6 FROM DUAL";
				String primarykey6 = DbConnection.executeScalar(DbConnection.KTHIS, qr16, new Object[] {}).toString();
				lsg.setPRIMARYKEY6(primarykey6);
				prk6 = primarykey6;

				String qr17 = "INSERT INTO PATIENTACCOUNT (PATIENTACCOUNT_ID, VISIT_ID, LAST_UPDATED_BY, ACCOUNT_BALANCE, BILL_SIZE, FROZEN_ACCOUNT_BALANCE, "
						+ "FROZEN_BILL_SIZE, LAST_UPDATED_DATETIME, OPEN_CARD_IND, ACCOUNT_CHANGED_IND, ACCOUNT_STATUS, "
						+ "PATIENT_ACCOUNT_NO, DEPOSIT_BALANCE) VALUES (?, ?, ?,0,0,0,0,(SELECT sysdate from DUAL),'N','N','ACS1',?,0)";
				Object[] input6 = { prk6, prk, user, gen };
				DbConnection.executeQuery(DbConnection.KTHIS, qr17, input6);

				String qr18 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY7 FROM DUAL";
				String primarykey7 = DbConnection.executeScalar(DbConnection.KTHIS, qr18, new Object[] {}).toString();
				lsg.setPRIMARYKEY7(primarykey7);
				prk7 = primarykey7;

				String qr19 = "INSERT INTO ACCOUNTDEBTOR (ACCOUNTDEBTOR_ID, STAKEHOLDER_ID, PATIENTACCOUNT_ID, PAYER_SEQ, CONTRACT_BALANCE, PAID_AMOUNT, LAST_BILL_AMOUNT, "
						+ "FROZEN_CONTRACT_BALANCE, FROZEN_CONTRACT_SIZE, CN_YTD_SELFPAID_AMOUNT, CN_LAST_CLAIM_AMOUNT, CN_SELF_PAYABLE_AMOUNT, DEBTOR_CHANGED_IND, "
						+ "FINANCIAL_CLASS, CLAIM_POLICY_CODE, CN_HEALTHCARE_PLAN_IND, CN_MAJOR_DISEASE_IND, CN_APPOINTED_HOSPITAL_IND, DEFUNCT_IND, LAST_UPDATED_BY, "
						+ "LAST_UPDATED_DATETIME) VALUES (?, ?, ?,10,0,0,0,0,0,0,0,0,'N','FCC1','SELF','N','N','N','N', ?, "
						+ "(SELECT sysdate from DUAL))";
				Object[] input7 = { prk7, stkid, prk6, user };
				DbConnection.executeQuery(DbConnection.KTHIS, qr19, input7);

				if (rqi.equals("Y") && rci.equals("N")) {

					String qr20 = "SELECT pgSYSFUNC.fxGenPrimaryKey as PRIMARYKEY8 FROM DUAL";
					String primarykey8 = DbConnection.executeScalar(DbConnection.KTHIS, qr20, new Object[] {})
							.toString();
					lsg.setPRIMARYKEY8(primarykey8);
					prk8 = primarykey8;

					String qr21 = "SELECT COUNT(1) as qn FROM VISITREGNTYPE VRT WHERE VRT.EFFICTIVE_DATETIME >= to_date(?,'ddmmyyyy') AND VRT.EFFICTIVE_DATETIME < "
							+ "to_date(?,'ddmmyyyy') + 1 AND VRT.VISITREGNTYPE_ID <= ?";
					Object[] input8 = { ed, ed, prk3 };
					String qn_query = DbConnection.executeScalar(DbConnection.KTHIS, qr21, input8).toString();
					lsg.setQn(qn_query);
					qn = qn_query;

					String qr22 = "SELECT COUNT(1) as qn2 FROM OPWAITINGQUEUE WQ WHERE WQ.RESOURCEMSTR_ID = ? AND WQ.ENQUEUE_DATETIME <= SYSDATE";
					String qn2_query = DbConnection.executeScalar(DbConnection.KTHIS, qr22, new Object[] { resourceid })
							.toString();
					lsg.setQn2(qn2_query);
					qn2 = qn2_query;

					String qr23 = "INSERT INTO OPWAITINGQUEUE (OPWAITINGQUEUE_ID, RESOURCEMSTR_ID, ENQUEUE_DATETIME, VISIT_ID, QUEUE_NO, QUEUE_SEQ_NO, STATUS, CAREPROVIDER_ID, "
							+ "CREATED_BY,   CREATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME ) VALUES "
							+ "(?, ?, SYSDATE, ?, ?, ?,'OQS1', ?, ?, SYSDATE, ?,SYSDATE)";
					Object[] input9 = { prk8, resourceid, prk, qn, qn2, careid, user, user };
					DbConnection.executeQuery(DbConnection.KTHIS, qr23, input9);

				}

				String qr24 = "SELECT to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS') as tgreg FROM DUAL";
				String tgreg = DbConnection.executeScalar(DbConnection.KTHIS, qr24, new Object[] {}).toString();
				lsg.setTgreg(tgreg);

				lsg.setResponse(true);
				lsg.setDescription("Berhasil");
				all.add(lsg);
			}
		} catch (SQLException e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage());

			ListSelfReg lsg = new ListSelfReg();
			lsg.setResponse(false);
			lsg.setDescription("Gagal | " + e.getMessage());
			all.add(lsg);
		} catch (Exception e) {

			e.fillInStackTrace();
			System.out.println(e.fillInStackTrace());

			ListSelfReg lsg = new ListSelfReg();
			lsg.setResponse(false);
			lsg.setDescription("Gagal | " + e.getMessage());
		}

		finally {
		}
		return all;
	}

	public static List<ListCekTxn> getCekTxn(String visitId) {
		List<ListCekTxn> all = new ArrayList<ListCekTxn>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select COUNT(PAX.PATIENTACCOUNTTXN_ID) as jum from PATIENTACCOUNTTXN PAX, PATIENTACCOUNT PAT "
				+ "where PAT.VISIT_ID='" + visitId
				+ "' AND PAX.PATIENTACCOUNT_ID = PAT.PATIENTACCOUNT_ID and PAX.CHARGE_STATUS = 'CTTNOR'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			haspas.next();
			ListCekTxn lp = new ListCekTxn();
			lp.setInfotxn(haspas.getString("jum"));
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
			all.add(lp);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ResourceList> SearchDoctor(String searchtext, String lokasi) {
		// System.out.println(searchtext+"|"+lokasi);
		List<ResourceList> data = new ArrayList<ResourceList>();
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		String abcarpro = "SELECT NULL RESOURCESCHEME_ID, RM.RESOURCE_NAME , RM.POLI_CODE, RM.RESOURCEMSTR_ID, PGCOMMON.fxGetCodeDesc(RM.POLI_CODE) as POLI_NAME "
				+ "FROM RESOURCEMSTR RM, SUBSPECIALTYMSTR SSPM "
				+ "WHERE RM.SUBSPECIALTYMSTR_ID = SSPM.SUBSPECIALTYMSTR_ID AND RM.RESOURCE_SCHEME_IND = 'N' "
				+ "AND RM.DEFUNCT_IND = 'N' AND RM.POLI_CODE like 'POL%' AND SSPM.SUBSPECIALTYMSTR_ID =? AND LOWER(RM.RESOURCE_NAME) LIKE LOWER(?) "
				+ " ORDER BY RM.POLI_CODE ASC";
		try {
			st = connection.prepareStatement(abcarpro);
			st.setString(1, lokasi);
			st.setString(2, "%" + searchtext + "%");
			rs = st.executeQuery();
			while (rs.next()) {
				ResourceList dl = new ResourceList();
				dl.setResourcescheme_id(rs.getString("RESOURCESCHEME_ID"));
				dl.setResource_name(rs.getString("RESOURCE_NAME"));
				dl.setPoli_code(rs.getString("POLI_CODE"));
				dl.setResourcemstr_id(rs.getBigDecimal("RESOURCEMSTR_ID"));
				dl.setPoli_name(rs.getString("POLI_NAME"));
				data.add(dl);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			if (st != null)
				try {
					st.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;

	}

	public static List<Listtxn> getlisttxn(String visitid) {
		List<Listtxn> all = new ArrayList<Listtxn>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select PAX.TXN_DESC , PAX.TXN_AMOUNT , PAX.TXN_DATETIME from PATIENTACCOUNTTXN PAX, PATIENTACCOUNT PAT "
				+ "where PAT.VISIT_ID='" + visitid
				+ "' AND PAX.PATIENTACCOUNT_ID = PAT.PATIENTACCOUNT_ID and PAX.CHARGE_STATUS = 'CTTNOR'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				Listtxn lp = new Listtxn();
				lp.setTXN_AMOUNT(haspas.getString("TXN_AMOUNT"));
				lp.setTXN_DATETIME(haspas.getString("TXN_DATETIME"));
				lp.setTXN_DESC(haspas.getString("TXN_DESC"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListCanReg> getcancelreg(String userid, String visitid, String remarks, String cancelreason) {
		// TODO Auto-generated method stub
		List<ListCanReg> all = new ArrayList<ListCanReg>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haseks1 = null;
		ResultSet haseks2 = null;
		ResultSet haseks3 = null;

		if (connection == null)
			return null;
		Statement stmt = null;
		Statement stmt2 = null;
		Statement stmt3 = null;

		String eks1 = "UPDATE VISITREGNTYPE SET CANCELLED_BY='" + userid
				+ "', CANCELLED_DATETIME=Sysdate, CANCEL_REASON='" + cancelreason + "', CANCEL_REMARKS='" + remarks
				+ "', DEFUNCT_IND='Y', " + "LAST_UPDATED_BY='" + userid
				+ "', LAST_UPDATED_DATETIME=Sysdate WHERE VISIT_ID='" + visitid + "'";
		try {
			stmt = connection.createStatement();
			haseks1 = stmt.executeQuery(eks1);
			haseks1.next();
			haseks1.close();
			stmt.close();

			String eks2 = "UPDATE VISIT SET ADMIT_STATUS='AST5', CANCELLED_BY='" + userid
					+ "', CANCELLED_DATETIME=Sysdate, CANCEL_REASON='" + cancelreason + "', " + "LAST_UPDATED_BY='"
					+ userid + "', LAST_UPDATED_DATETIME=Sysdate WHERE VISIT_ID='" + visitid + "'";
			stmt2 = connection.createStatement();
			haseks2 = stmt2.executeQuery(eks2);
			haseks2.next();
			haseks2.close();
			stmt2.close();

			String eks3 = "SELECT VT.CANCELLED_BY, VT.CANCELLED_DATETIME,VR.CANCEL_REMARKS FROM VISIT VT, VISITREGNTYPE VR WHERE VT.VISIT_ID='"
					+ visitid + "' " + "AND VR.VISIT_ID=VT.VISIT_ID";
			stmt3 = connection.createStatement();
			haseks3 = stmt3.executeQuery(eks3);
			haseks3.next();
			ListCanReg lcg = new ListCanReg();
			lcg.setCANCELLED_BY(haseks3.getString("CANCELLED_BY"));
			lcg.setCANCELLED_DATETIME(haseks3.getString("CANCELLED_DATETIME"));
			lcg.setREMARKS(haseks3.getString("CANCEL_REMARKS"));
			all.add(lcg);
			haseks3.close();
			stmt3.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haseks1 != null)
				try {
					haseks1.close();
				} catch (Exception ignore) {
				}
			if (haseks2 != null)
				try {
					haseks2.close();
				} catch (Exception ignore) {
				}
			if (haseks3 != null)
				try {
					haseks3.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (stmt3 != null)
				try {
					stmt3.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListCodeCar> getcodecar() {
		// TODO Auto-generated method stub
		List<ListCodeCar> all = new ArrayList<ListCodeCar>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;

		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select  CODE_CAT, CODE_ABBR, CODE_DESC FROM CODEMSTR WHERE CODE_CAT LIKE'%CAR%'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListCodeCar ln = new ListCodeCar();
				ln.setCODE_CAT(haspas.getString("CODE_CAT"));
				ln.setCODE_ABBR(haspas.getString("CODE_ABBR"));
				ln.setCODE_DESC(haspas.getString("CODE_DESC"));
				all.add(ln);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<LicstCekValid> getcekvalidpas(String bpjsno, String namapas, String tglahir) {
		// TODO Auto-generated method stub
		List<LicstCekValid> all = new ArrayList<LicstCekValid>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		ResultSet haspas2 = null;
		ResultSet haspas3 = null;
		ResultSet haspas4 = null;
		ResultSet haspas5 = null;

		if (connection == null)
			return null;
		Statement stmt = null;
		Statement stmt2 = null;
		Statement stmt3 = null;
		Statement stmt4 = null;
		Statement stmt5 = null;

		String abdatabp = "SELECT COUNT(*) AS JUM FROM PATIENT PT, PERSON PS WHERE pt.person_id = ps.person_id and  PT.BPJS_NO='"
				+ bpjsno + "' " + "AND PS.PERSON_NAME LIKE '%" + namapas.toUpperCase().trim()
				+ "%' AND PS.BIRTH_DATE=TO_DATE('" + tglahir.trim() + "','DD-MM-YYYY')";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			haspas.next();
			LicstCekValid ln = new LicstCekValid();
			String jum = haspas.getString("JUM");
			if (jum.equals("1")) {
				ln.setLn(haspas.getString("JUM"));
				ln.setKeterangan("NO BPJS, NAMA , DAN TGL LAHIR DITEMUKAN");
				// ln.setCODE_CAT(haspas.getString("CODE_CAT"));
				// ln.setCODE_ABBR(haspas.getString("CODE_ABBR"));
				// ln.setCODE_DESC(haspas.getString("CODE_DESC"));
				all.add(ln);
				haspas.close();
				stmt.close();
			} else {

				String ab2 = "SELECT COUNT(*) AS JUM2 FROM PATIENT PT, PERSON PS WHERE pt.person_id = ps.person_id and  PT.BPJS_NO='"
						+ bpjsno + "' " + "AND PS.PERSON_NAME LIKE '%" + namapas.toUpperCase().trim()
						+ "%' AND PS.BIRTH_DATE !=TO_DATE('" + tglahir.trim() + "','DD-MM-YYYY')";
				stmt2 = connection.createStatement();
				haspas2 = stmt2.executeQuery(ab2);
				haspas2.next();
				LicstCekValid ln2 = new LicstCekValid();
				String jum2 = haspas2.getString("JUM2");
				if (jum2.equals("1")) {
					ln2.setLn(haspas2.getString("JUM2"));
					ln2.setKeterangan("No BPJS dan Nama DITEMUKAN. Tetapi Tanggal Lahir TIDAK DITEMUKAN");
					all.add(ln2);
					haspas2.close();
					stmt2.close();
				} else {
					String ab3 = "SELECT COUNT(*) AS JUM3 FROM PATIENT PT, PERSON PS WHERE pt.person_id = ps.person_id and  PT.BPJS_NO ='"
							+ bpjsno + "' " + "AND PS.PERSON_NAME NOT LIKE '%" + namapas.toUpperCase().trim()
							+ "%' AND PS.BIRTH_DATE =TO_DATE('" + tglahir.trim() + "','DD-MM-YYYY')";
					stmt3 = connection.createStatement();
					haspas3 = stmt3.executeQuery(ab3);
					haspas3.next();
					LicstCekValid ln3 = new LicstCekValid();
					String jum3 = haspas3.getString("JUM3");
					if (jum3.equals("1")) {
						ln3.setLn(haspas3.getString("JUM3"));
						ln3.setKeterangan("BPJS NO, dan Tanggal Lahir DITEMUKAN, Nama TIDAK DITEMUKAN");
						all.add(ln3);
						haspas3.close();
						stmt3.close();
					} else {

						String ab4 = "SELECT COUNT(*) AS JUM4 FROM PATIENT PT, PERSON PS WHERE pt.person_id = ps.person_id and  PT.BPJS_NO !='"
								+ bpjsno + "' " + "AND PS.PERSON_NAME LIKE '%" + namapas.toUpperCase().trim()
								+ "%' AND PS.BIRTH_DATE =TO_DATE('" + tglahir.trim() + "','DD-MM-YYYY')";
						stmt4 = connection.createStatement();
						haspas4 = stmt4.executeQuery(ab4);
						haspas4.next();
						LicstCekValid ln4 = new LicstCekValid();
						String jum4 = haspas4.getString("JUM4");
						if (jum4.equals("1")) {
							ln4.setLn(haspas4.getString("JUM4"));
							ln4.setKeterangan("Nama dan Tanggal Lahir DITEMUKAN, BPJS NO TIDAK DITEMUKAN");
							all.add(ln4);
							haspas4.close();
							stmt4.close();
						} else {

							String ab5 = "SELECT COUNT(*) AS JUM5 FROM PATIENT PT, PERSON PS WHERE pt.person_id = ps.person_id and  PT.BPJS_NO ='"
									+ bpjsno + "' " + "AND PS.PERSON_NAME NOT LIKE '%" + namapas.toUpperCase().trim()
									+ "%' AND PS.BIRTH_DATE !=TO_DATE('" + tglahir.trim() + "','DD-MM-YYYY')";
							stmt5 = connection.createStatement();
							haspas5 = stmt5.executeQuery(ab5);
							haspas5.next();
							LicstCekValid ln5 = new LicstCekValid();
							String jum5 = haspas5.getString("JUM5");
							if (jum5.equals("1")) {
								ln5.setLn(haspas5.getString("JUM5"));
								ln5.setKeterangan("BPJS NO DITEMUKAN, Nama dan Tanggal Lahir TIDAK DITEMUKAN");
							} else {
								ln5.setLn("0");
								ln5.setKeterangan("BPJS NO, NAMA DAN TANGGAL LAHIR TIDAK DITEMUKAN");
							}
							all.add(ln5);
							haspas5.close();
							stmt5.close();
						}
					}
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (haspas2 != null)
				try {
					haspas2.close();
				} catch (Exception ignore) {
				}
			if (haspas3 != null)
				try {
					haspas3.close();
				} catch (Exception ignore) {
				}
			if (haspas4 != null)
				try {
					haspas4.close();
				} catch (Exception ignore) {
				}
			if (haspas5 != null)
				try {
					haspas5.close();
				} catch (Exception ignore) {
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (stmt3 != null)
				try {
					stmt3.close();
				} catch (Exception ignore) {
				}
			if (stmt4 != null)
				try {
					stmt4.close();
				} catch (Exception ignore) {
				}
			if (stmt5 != null)
				try {
					stmt5.close();
				} catch (Exception ignore) {
				}

			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListValidAntrian> getcekvalidantrian(String cardNo, String idNo, String bpjsNo) {
		// TODO Auto-generated method stub
		List<ListValidAntrian> all = new ArrayList<ListValidAntrian>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		ResultSet haspas2 = null;
		ResultSet haspas3 = null;
		ResultSet haspas4 = null;
		ResultSet haspas5 = null;
		ResultSet haspas6 = null;
		ResultSet haspas7 = null;

		if (connection == null)
			return null;
		Statement stmt = null;
		Statement stmt2 = null;
		Statement stmt3 = null;
		Statement stmt4 = null;
		Statement stmt5 = null;
		Statement stmt6 = null;
		Statement stmt7 = null;
		String abdatabp = "select count(*) as JUM from CARD CD, PERSON PS, PATIENT PT  "
				+ "WHERE PT.PERSON_ID=PS.PERSON_ID AND CD.PERSON_ID=PT.PERSON_ID AND trim(CD.CARD_NO)='" + cardNo
				+ "' AND trim(PS.ID_NO)='" + idNo + "' " + "AND trim(PT.BPJS_NO)='" + bpjsNo + "'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			haspas.next();
			ListValidAntrian ln = new ListValidAntrian();
			String jum = haspas.getString("JUM");
			if (jum.equals("1")) {
				ln.setLn(haspas.getString("JUM"));
				ln.setKeterangan("No Kartu Pasien Ditemukan, No KTP Ditemukan, No BPJS Ditemukan");
				// ln.setCODE_CAT(haspas.getString("CODE_CAT"));
				// ln.setCODE_ABBR(haspas.getString("CODE_ABBR"));
				// ln.setCODE_DESC(haspas.getString("CODE_DESC"));
				all.add(ln);
				haspas.close();
				stmt.close();
			} else {

				String ab2 = "select count(*) as JUM2 from CARD CD, PERSON PS, PATIENT PT  "
						+ "WHERE PT.PERSON_ID=PS.PERSON_ID AND CD.PERSON_ID=PT.PERSON_ID AND trim(CD.CARD_NO)!='"
						+ cardNo + "' AND trim(PS.ID_NO)='" + idNo + "' " + "AND trim(PT.BPJS_NO)='" + bpjsNo + "'";
				stmt2 = connection.createStatement();
				haspas2 = stmt2.executeQuery(ab2);
				haspas2.next();
				ListValidAntrian ln2 = new ListValidAntrian();
				String jum2 = haspas2.getString("JUM2");
				if (jum2.equals("1")) {
					ln2.setLn(haspas2.getString("JUM2"));
					ln2.setKeterangan("No Kartu Pasien Tidak Ditemukan, No KTP Ditemukan, No BPJS Ditemukan");
					all.add(ln2);
					haspas2.close();
					stmt2.close();
				} else {

					String ab3 = "select count(*) as JUM3 from CARD CD, PERSON PS, PATIENT PT  "
							+ "WHERE PT.PERSON_ID=PS.PERSON_ID AND CD.PERSON_ID=PT.PERSON_ID AND trim(CD.CARD_NO)='"
							+ cardNo + "' AND trim(PS.ID_NO)!='" + idNo + "' " + "AND trim(PT.BPJS_NO)='" + bpjsNo
							+ "'";
					stmt3 = connection.createStatement();
					haspas3 = stmt3.executeQuery(ab3);
					haspas3.next();
					ListValidAntrian ln3 = new ListValidAntrian();
					String jum3 = haspas3.getString("JUM3");
					if (jum3.equals("1")) {
						ln3.setLn(haspas3.getString("JUM3"));
						ln3.setKeterangan("No Kartu Pasien Ditemukan, No KTP Tidak Ditemukan, No BPJS Ditemukan");
						all.add(ln3);
						haspas3.close();
						stmt3.close();
					} else {

						String ab4 = "select count(*) as JUM4 from CARD CD, PERSON PS, PATIENT PT  "
								+ "WHERE PT.PERSON_ID=PS.PERSON_ID AND CD.PERSON_ID=PT.PERSON_ID AND trim(CD.CARD_NO)='"
								+ cardNo + "' AND trim(PS.ID_NO)='" + idNo + "' " + "AND trim(PT.BPJS_NO)!='" + bpjsNo
								+ "'";
						stmt4 = connection.createStatement();
						haspas4 = stmt4.executeQuery(ab4);
						haspas4.next();
						ListValidAntrian ln4 = new ListValidAntrian();
						String jum4 = haspas4.getString("JUM4");
						if (jum4.equals("1")) {
							ln4.setLn(haspas4.getString("JUM4"));
							ln4.setKeterangan("No Kartu Pasien Ditemukan, No KTP Ditemukan, No BPJS Tidak Ditemukan");
							all.add(ln4);
							haspas4.close();
							stmt4.close();
						} else {

							String ab5 = "select count(*) as JUM5 from CARD CD, PERSON PS, PATIENT PT  "
									+ "WHERE PT.PERSON_ID=PS.PERSON_ID AND CD.PERSON_ID=PT.PERSON_ID AND trim(CD.CARD_NO)='"
									+ cardNo + "' AND trim(PS.ID_NO)!='" + idNo + "' " + "AND trim(PT.BPJS_NO)!='"
									+ bpjsNo + "'";
							stmt5 = connection.createStatement();
							haspas5 = stmt5.executeQuery(ab5);
							haspas5.next();
							ListValidAntrian ln5 = new ListValidAntrian();
							String jum5 = haspas5.getString("JUM5");
							if (jum5.equals("1")) {
								ln5.setLn(haspas5.getString("JUM5"));
								ln5.setKeterangan(
										"No Kartu Pasien Ditemukan, No KTP Tidak Ditemukan, No BPJS Tidak Ditemukan");
								all.add(ln5);
								haspas5.close();
								stmt5.close();
							} else {

								String ab6 = "select count(*) as JUM6 from CARD CD, PERSON PS, PATIENT PT  "
										+ "WHERE PT.PERSON_ID=PS.PERSON_ID AND CD.PERSON_ID=PT.PERSON_ID AND trim(CD.CARD_NO)!='"
										+ cardNo + "' AND trim(PS.ID_NO)='" + idNo + "' " + "AND trim(PT.BPJS_NO)!='"
										+ bpjsNo + "'";
								stmt6 = connection.createStatement();
								haspas6 = stmt6.executeQuery(ab6);
								haspas6.next();
								ListValidAntrian ln6 = new ListValidAntrian();
								String jum6 = haspas6.getString("JUM6");
								if (jum6.equals("1")) {
									ln6.setLn(haspas6.getString("JUM6"));
									ln6.setKeterangan(
											"No Kartu Pasien Tidak Ditemukan, No KTP Ditemukan, No BPJS Tidak Ditemukan");
									all.add(ln6);
									haspas6.close();
									stmt6.close();
								} else {

									String ab7 = "select count(*) as JUM7 from CARD CD, PERSON PS, PATIENT PT  "
											+ "WHERE PT.PERSON_ID=PS.PERSON_ID AND CD.PERSON_ID=PT.PERSON_ID AND trim(CD.CARD_NO)!='"
											+ cardNo + "' AND trim(PS.ID_NO)!='" + idNo + "' "
											+ "AND trim(PT.BPJS_NO)='" + bpjsNo + "'";
									stmt7 = connection.createStatement();
									haspas7 = stmt7.executeQuery(ab7);
									haspas7.next();
									ListValidAntrian ln7 = new ListValidAntrian();
									String jum7 = haspas7.getString("JUM7");
									if (jum7.equals("1")) {
										ln7.setLn(haspas7.getString("JUM7"));
										ln7.setKeterangan(
												"No Kartu Pasien Tidak Ditemukan, No KTP Tidak Ditemukan, No BPJS Ditemukan");
									} else {
										ln7.setLn("0");
										ln7.setKeterangan(
												"No Kartu Pasien Tidak Ditemukan, No KTP Tidak Ditemukan, No BPJS Tidak Ditemukan");
									}
									all.add(ln7);
									haspas7.close();
									stmt7.close();
								}

							}

						}
					}
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (haspas2 != null)
				try {
					haspas2.close();
				} catch (Exception ignore) {
				}
			if (haspas3 != null)
				try {
					haspas3.close();
				} catch (Exception ignore) {
				}
			if (haspas4 != null)
				try {
					haspas4.close();
				} catch (Exception ignore) {
				}
			if (haspas5 != null)
				try {
					haspas5.close();
				} catch (Exception ignore) {
				}
			if (haspas6 != null)
				try {
					haspas6.close();
				} catch (Exception ignore) {
				}
			if (haspas7 != null)
				try {
					haspas7.close();
				} catch (Exception ignore) {
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (stmt3 != null)
				try {
					stmt3.close();
				} catch (Exception ignore) {
				}
			if (stmt4 != null)
				try {
					stmt4.close();
				} catch (Exception ignore) {
				}
			if (stmt5 != null)
				try {
					stmt5.close();
				} catch (Exception ignore) {
				}
			if (stmt6 != null)
				try {
					stmt6.close();
				} catch (Exception ignore) {
				}
			if (stmt7 != null)
				try {
					stmt7.close();
				} catch (Exception ignore) {
				}

			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListResourceMstr> getResourceMaster(String ResourceName) {
		List<ListResourceMstr> all = new ArrayList<ListResourceMstr>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "SELECT rm.RESOURCEMSTR_ID, rm.RESOURCE_NAME, rm.resource_code, rm.defunct_ind, rm.CAREPROVIDER_ID, rm.POLI_CODE FROM RESOURCEMSTR rm "
				+ "WHERE rm.SUBSPECIALTYMSTR_ID in(336581902, 311099883, 311099876, 311099877, 311530438) and rm.defunct_ind = 'N' and rm.POLI_CODE ='"
				+ ResourceName + "' and rm.defunct_ind = 'N'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListResourceMstr lp = new ListResourceMstr();
				lp.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
				lp.setRESOURCE_NAME(haspas.getString("RESOURCE_NAME"));
				lp.setResource_code(haspas.getString("resource_code"));
				lp.setDefunct_ind(haspas.getString("defunct_ind"));
				lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
				lp.setPOLI_CODE(haspas.getString("POLI_CODE"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListResourceMstr> getResourceMaster2(String ResourceName) {
		List<ListResourceMstr> all = new ArrayList<ListResourceMstr>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "SELECT rm.RESOURCEMSTR_ID, rm.RESOURCE_NAME, rm.CAREPROVIDER_ID, sm.SESSION_DESC, rp.MON_IND, rp.TUE_IND, rp.WED_IND, rp.THU_IND, "
				+ "rp.FRI_IND, rp.SAT_IND, rp.SUN_IND, rm.POLI_CODE FROM RESOURCEMSTR rm inner join RESOURCEPROFILE rp on "
				+ "rm.RESOURCEMSTR_ID = rp.RESOURCEMSTR_ID inner join SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID "
				+ "WHERE rm.SUBSPECIALTYMSTR_ID in(336581902, 311099883, 311099876, 311099877, 311530438) and rm.defunct_ind = 'N' and rp.DEFUNCT_IND = 'N' and rm.RESOURCE_NAME like '%"
				+ ResourceName + "%'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListResourceMstr lp = new ListResourceMstr();
				lp.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
				lp.setRESOURCE_NAME(haspas.getString("RESOURCE_NAME"));
				lp.setSESSION_DESC(haspas.getString("SESSION_DESC"));
				lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
				lp.setMON_IND(haspas.getString("MON_IND"));
				lp.setTUE_IND(haspas.getString("TUE_IND"));
				lp.setWED_IND(haspas.getString("WED_IND"));
				lp.setTHU_IND(haspas.getString("THU_IND"));
				lp.setFRI_IND(haspas.getString("FRI_IND"));
				lp.setSAT_IND(haspas.getString("SAT_IND"));
				lp.setSUN_IND(haspas.getString("SUN_IND"));
				lp.setPOLI_CODE(haspas.getString("POLI_CODE"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListResourceMstr2> getpolydoc(String idhari) {
		List<ListResourceMstr2> all = new ArrayList<ListResourceMstr2>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String cetak = "";
		if (idhari.equals("2")) {
			cetak = "MON_IND='Y'";
		} else if (idhari.equals("3")) {
			cetak = "TUE_IND='Y'";
		} else if (idhari.equals("4")) {
			cetak = "WED_IND='Y'";
		} else if (idhari.equals("5")) {
			cetak = "THU_IND='Y'";
		} else if (idhari.equals("6")) {
			cetak = "FRI_IND='Y'";
		} else if (idhari.equals("7")) {
			cetak = "SAT_IND='Y'";
		} else {
			cetak = "SUN_IND='Y'";
		}

		String abdatabp = "SELECT rm.RESOURCEMSTR_ID, rm.RESOURCE_NAME, " + " rm.CAREPROVIDER_ID, sm.SESSION_DESC , "
				+ " (SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR WHERE SUBSPECIALTY_CODE = RM.POLI_CODE AND DEFUNCT_IND='N' ) POLI_CODE, "
				+ " pgCOMMON.fxGetCodeDesc(rm.poli_code) POLI_NAME, " + " rm.poli_code as subspecialty_code "
				+ " FROM RESOURCEMSTR rm inner join "
				+ " RESOURCEPROFILE rp on rm.RESOURCEMSTR_ID = rp.RESOURCEMSTR_ID inner join "
				+ " SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID " + "WHERE "
				+ "rm.SUBSPECIALTYMSTR_ID=311187819 " + "and rm.defunct_ind = 'N' " + " and rp.DEFUNCT_IND = 'N' and "
				+ cetak + "";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListResourceMstr2 lp = new ListResourceMstr2();
				lp.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
				lp.setRESOURCE_NAME(haspas.getString("RESOURCE_NAME"));
				lp.setSESSION_DESC(haspas.getString("SESSION_DESC"));
				lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
				lp.setSubspecialty_code(haspas.getString("subspecialty_code"));
				lp.setPOLI_CODE(haspas.getString("POLI_CODE"));
				lp.setPOLI_NAME(haspas.getString("POLI_NAME"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListResourceMstr2> getpolybpjsdoc(String idhari) {
		List<ListResourceMstr2> all = new ArrayList<ListResourceMstr2>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String cetak = "";
		if (idhari.equals("2")) {
			cetak = "MON_IND='Y'";
		} else if (idhari.equals("3")) {
			cetak = "TUE_IND='Y'";
		} else if (idhari.equals("4")) {
			cetak = "WED_IND='Y'";
		} else if (idhari.equals("5")) {
			cetak = "THU_IND='Y'";
		} else if (idhari.equals("6")) {
			cetak = "FRI_IND='Y'";
		} else if (idhari.equals("7")) {
			cetak = "SAT_IND='Y'";
		} else {
			cetak = "SUN_IND='Y'";
		}

		String abdatabp = "SELECT rm.RESOURCEMSTR_ID, rm.RESOURCE_NAME, " + "rm.CAREPROVIDER_ID, sm.SESSION_DESC, "
				+ "(SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR WHERE SUBSPECIALTY_CODE = RM.POLI_CODE AND DEFUNCT_IND='N' ) POLI_CODE, "
				+ "pgCOMMON.fxGetCodeDesc(rm.poli_code) POLI_NAME, " + "rm.poli_code as subspecialty_code "
				+ "FROM RESOURCEMSTR rm inner join "
				+ "RESOURCEPROFILE rp on rm.RESOURCEMSTR_ID = rp.RESOURCEMSTR_ID inner join "
				+ "SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID " + "WHERE "
				+ "(rm.SUBSPECIALTYMSTR_ID=336581902 OR rm.SUBSPECIALTYMSTR_ID IN(311099877) ) " // ini hardcode untuk
																									// menampilkan dr
																									// chyntia
				+ "and rm.defunct_ind = 'N' " + "and rp.DEFUNCT_IND = 'N' and " + cetak + "";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListResourceMstr2 lp = new ListResourceMstr2();
				lp.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
				lp.setRESOURCE_NAME(haspas.getString("RESOURCE_NAME"));
				lp.setSESSION_DESC(haspas.getString("SESSION_DESC"));
				lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
				lp.setSubspecialty_code(haspas.getString("subspecialty_code"));
				lp.setPOLI_CODE(haspas.getString("POLI_CODE"));
				lp.setPOLI_NAME(haspas.getString("POLI_NAME"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListResourceMstrPoly> getpolydoc3(String idhari) {
		List<ListResourceMstrPoly> all = new ArrayList<ListResourceMstrPoly>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String cetak = "";
		if (idhari.equals("2")) {
			cetak = "MON_IND='Y'";
		} else if (idhari.equals("3")) {
			cetak = "TUE_IND='Y'";
		} else if (idhari.equals("4")) {
			cetak = "WED_IND='Y'";
		} else if (idhari.equals("5")) {
			cetak = "THU_IND='Y'";
		} else if (idhari.equals("6")) {
			cetak = "FRI_IND='Y'";
		} else if (idhari.equals("7")) {
			cetak = "SAT_IND='Y'";
		} else {
			cetak = "SUN_IND='Y'";
		}

		String abdatabp = "SELECT rm.RESOURCEMSTR_ID, rm.RESOURCE_NAME, rm.CAREPROVIDER_ID, sm.SESSION_DESC , "
				+ "ssm.SUBSPECIALTY_DESC FROM RESOURCEMSTR rm "
				+ "inner join RESOURCEPROFILE rp on rm.RESOURCEMSTR_ID = rp.RESOURCEMSTR_ID  "
				+ "inner join SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID  "
				+ "inner join SUBSPECIALTYMSTR ssm on ssm.SUBSPECIALTYMSTR_ID = rm.SUBSPECIALTYMSTR_ID  "
				+ "WHERE rm.SUBSPECIALTYMSTR_ID in (311187819,311260354,311099851) and rm.defunct_ind = 'N' and rp.DEFUNCT_IND = 'N' and "
				+ cetak + "";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListResourceMstrPoly lp = new ListResourceMstrPoly();
				String rm = haspas.getString("RESOURCE_NAME");
				String poly = haspas.getString("SUBSPECIALTY_DESC");
				String gab = rm + "( " + poly + " )";
				lp.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
				lp.setRESOURCE_NAME(gab);
				lp.setSESSION_DESC(haspas.getString("SESSION_DESC"));
				lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListResourceMstr2> getpolydoc2(String idhari, String poli) {
		List<ListResourceMstr2> all = new ArrayList<ListResourceMstr2>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String cetak = "";
		if (idhari.equals("2")) {
			cetak = "MON_IND='Y'";
		} else if (idhari.equals("3")) {
			cetak = "TUE_IND='Y'";
		} else if (idhari.equals("4")) {
			cetak = "WED_IND='Y'";
		} else if (idhari.equals("5")) {
			cetak = "THU_IND='Y'";
		} else if (idhari.equals("6")) {
			cetak = "FRI_IND='Y'";
		} else if (idhari.equals("7")) {
			cetak = "SAT_IND='Y'";
		} else {
			cetak = "SUN_IND='Y'";
		}

		String cetak2 = "";
		if (poli.equals("Kosong")) {
			cetak2 = "rm.POLI_CODE IS NULL";
		} else {
			cetak2 = "rm.POLI_CODE='" + poli + "'";
		}

		String abdatabp = "SELECT rm.RESOURCEMSTR_ID, rm.RESOURCE_NAME, " + " rm.CAREPROVIDER_ID, sm.SESSION_DESC , "
				+ " (SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR WHERE SUBSPECIALTY_CODE = RM.POLI_CODE AND DEFUNCT_IND='N' ) POLI_CODE, "
				+ " pgCOMMON.fxGetCodeDesc(rm.poli_code) POLI_NAME, " + " rm.poli_code as subspecialty_code "
				+ " FROM RESOURCEMSTR rm inner join "
				+ "RESOURCEPROFILE rp on rm.RESOURCEMSTR_ID = rp.RESOURCEMSTR_ID inner join "
				+ "SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID " + "WHERE "
				+ "rm.SUBSPECIALTYMSTR_ID=311187819 " + "and rm.defunct_ind = 'N' " + "and rp.DEFUNCT_IND = 'N' and "
				+ cetak + " and " + cetak2 + "";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListResourceMstr2 lp = new ListResourceMstr2();
				lp.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
				lp.setRESOURCE_NAME(haspas.getString("RESOURCE_NAME"));
				lp.setSESSION_DESC(haspas.getString("SESSION_DESC"));
				lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
				lp.setSubspecialty_code(haspas.getString("subspecialty_code"));
				lp.setPOLI_CODE(haspas.getString("POLI_CODE"));
				lp.setPOLI_NAME(haspas.getString("POLI_NAME"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListResourceMstrPolyName> getpolyname(String idhari) {
		List<ListResourceMstrPolyName> all = new ArrayList<ListResourceMstrPolyName>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String cetak = "";
		if (idhari.equals("2")) {
			cetak = "MON_IND='Y'";
		} else if (idhari.equals("3")) {
			cetak = "TUE_IND='Y'";
		} else if (idhari.equals("4")) {
			cetak = "WED_IND='Y'";
		} else if (idhari.equals("5")) {
			cetak = "THU_IND='Y'";
		} else if (idhari.equals("6")) {
			cetak = "FRI_IND='Y'";
		} else if (idhari.equals("7")) {
			cetak = "SAT_IND='Y'";
		} else {
			cetak = "SUN_IND='Y'";
		}

		String abdatabp = "SELECT pgCOMMON.fxGetCodeDesc(rm.POLI_CODE) POLI_NAME, "
				+ "(SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR WHERE SUBSPECIALTY_CODE = RM.POLI_CODE and defunct_ind = 'N') POLI_CODE,"
				+ "rm.POLI_CODE as subspecialty_code FROM RESOURCEMSTR rm "
				+ "inner join RESOURCEPROFILE rp on rm.RESOURCEMSTR_ID = rp.RESOURCEMSTR_ID "
				+ "inner join SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID " + "WHERE "
				+ "rm.SUBSPECIALTYMSTR_ID=311187819 " + "and rm.defunct_ind = 'N' AND " + cetak
				+ " GROUP BY rm.POLI_CODE ORDER BY POLI_NAME";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListResourceMstrPolyName lp = new ListResourceMstrPolyName();
				lp.setSubspecialty_code(haspas.getString("subspecialty_code"));
				lp.setPOLI_CODE(haspas.getString("POLI_CODE"));
				lp.setPOLI_NAME(haspas.getString("POLI_NAME"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static ListResourceMstr2 getResourceMaster3(String ResourceId) {
		ListResourceMstr2 view = new ListResourceMstr2();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		try {
			String abdatabp = "SELECT rm.RESOURCEMSTR_ID, rm.RESOURCE_NAME, rm.CAREPROVIDER_ID, sm.SESSION_DESC, rp.MON_IND, rp.TUE_IND, rp.WED_IND, rp.THU_IND, "
					+ "rp.FRI_IND, rp.SAT_IND, rp.SUN_IND, rm.POLI_CODE FROM RESOURCEMSTR rm inner join RESOURCEPROFILE rp on "
					+ "rm.RESOURCEMSTR_ID = rp.RESOURCEMSTR_ID inner join SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID "
					+ "WHERE rm.SUBSPECIALTYMSTR_ID=311187819 and rm.defunct_ind = 'N' and rp.DEFUNCT_IND = 'N' and rm.RESOURCEMSTR_ID="
					+ ResourceId + "";

			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			haspas.next();
			view.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
			view.setRESOURCE_NAME(haspas.getString("RESOURCE_NAME"));
			view.setSESSION_DESC(haspas.getString("SESSION_DESC"));
			view.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			view.setMON_IND(haspas.getString("MON_IND"));
			view.setTUE_IND(haspas.getString("TUE_IND"));
			view.setWED_IND(haspas.getString("WED_IND"));
			view.setTHU_IND(haspas.getString("THU_IND"));
			view.setFRI_IND(haspas.getString("FRI_IND"));
			view.setSAT_IND(haspas.getString("SAT_IND"));
			view.setSUN_IND(haspas.getString("SUN_IND"));
			view.setPOLI_CODE(haspas.getString("POLI_CODE"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return view;
	}

	public static SelfRegistration registrationInfo(String regno) {
		SelfRegistration view = new SelfRegistration();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet row = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String query = "select  main.REGN_NO, PERSON_NAME, cp_name, cashier_name, SUBSPECIALTY_DESC, JOB_NO, regn_type, ADDRESS, CREATED_DATETIME regn_date, CardNo, "
				+ "VISIT_NO, APPOINTMENT_TIME, to_char(( select count(vrt1.VISITREGNTYPE_ID) from visitregntype vrt1 "
				+ "where vrt1.EFFICTIVE_DATETIME >= trunc(main.EFFICTIVE_DATETIME) and vrt1.EFFICTIVE_DATETIME < trunc(main.EFFICTIVE_DATETIME)+1 "
				+ "and vrt1.VISITREGNTYPE_ID <= main.VISITREGNTYPE_ID ))||'('||SESSION_DESC||')' seq_no, decode(GHF_amt, null,0,GHF_amt) GHF_amt, "
				+ "decode(ZLF_amt, null,0,ZLF_amt) ZLF_amt, decode(total_amt, null,0,total_amt) total_amt, NVL(AUTO_PAY,0) AUTO_PAY, "
				+ "to_char(EFFICTIVE_DATETIME,'YYYY-MM-DD') EFFICTIVE_DATETIME, SESSION_DESC, sysdate PRINT_DATE from( select vrt.REGN_NO, p.PERSON_NAME, "
				+ "pc.PERSON_NAME cp_name, pk.PERSON_NAME cashier_name, sspm.SUBSPECIALTY_DESC,  up.JOB_NO,pgcommon.fxGetCodeDesc(vrt.REGISTRATION_TYPE) regn_type, "
				+ "lm.ADDRESS, vrt.CREATED_DATETIME, pgPMI.fxGetCardNo(p.PERSON_ID) CardNo, v.VISIT_NO , vrt.REGISTRATION_TYPE, vrt.RESOURCEMSTR_ID, "
				+ "vrt.VISITREGNTYPE_ID, vrt.EFFICTIVE_DATETIME, sm.SESSION_DESC, vrt.SESSIONMSTR_ID, ram.APPOINTMENT_TIME from visitregntype vrt, "
				+ "userprofile up , visit v, patient pt, person p ,careprovider cp, person pc, person pk, subspecialtymstr sspm, LOCATIONMSTR lm, sessionmstr sm ,REGNAPPOINTMENT ram  "
				+ "where v.VISIT_ID = vrt.VISIT_ID and vrt.REGN_NO = " + regno
				+ " and up.USERMSTR_ID = vrt.CREATED_BY and pt.PATIENT_ID = v.PATIENT_ID and "
				+ "p.PERSON_ID = pt.PERSON_ID and pk.PERSON_ID = up.PERSON_ID and cp.CAREPROVIDER_ID(+) = vrt.CAREPROVIDER_ID and pc.PERSON_ID(+) = cp.PERSON_ID "
				+ "and sspm.SUBSPECIALTYMSTR_ID = vrt.SUBSPECIALTYMSTR_ID and lm.LOCATIONMSTR_ID = sspm.LOCATIONMSTR_ID and sm.SESSIONMSTR_ID = vrt.SESSIONMSTR_ID and "
				+ "ram.VISITREGNTYPE_ID(+) = vrt.VISITREGNTYPE_ID) main , (select  vrt.REGN_NO , sum(decode(icm.ITEM_TYPE, 'ITY12',0, pat.TXN_AMOUNT)) GHF_amt , "
				+ "sum(decode(iCM.ITEM_TYPE, 'ITY12' , pat.TXN_AMOUNT,0)) ZLF_amt , sum(pat.TXN_AMOUNT) total_amt , (select sum(decode(tcm1.TXN_CODE, 'OP_AUTO_PAY', -pat1.TXN_AMOUNT,0)) AUTO_PAY "
				+ "from PATIENTACCOUNTTXN pat1, TXNCODEMSTR tcm1  where PAT1.BILL_ID = PAT.BILL_ID and TCM1.TXNCODEMSTR_ID = PAT1.TXNCODEMSTR_ID) AUTO_PAY from "
				+ "visitregntype vrt,visitregntypecharge vrtc, patientaccounttxn pat, chargeitemmstr cim, itemsubcategorymstr iscm, itemcategorymstr icm, txncodemstr tcm  "
				+ "where vrt.REGN_NO = " + regno
				+ " and vrtc.VISITREGNTYPE_ID = vrt.VISITREGNTYPE_ID and pat.PATIENTACCOUNTTXN_ID = vrtc.PATIENTACCOUNTTXN_ID and "
				+ "cim.TXNCODEMSTR_ID = pat.TXNCODEMSTR_ID and iscm.ITEMSUBCATEGORYMSTR_ID = cim.ITEMSUBCATEGORYMSTR_ID and  "
				+ "icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID and tcm.TXNCODEMSTR_ID = pat.TXNCODEMSTR_ID group by vrt.REGN_NO, PAT.BILL_ID) amt where amt.REGN_NO(+) = main.REGN_NO";
		try {
			stmt = connection.createStatement();
			row = stmt.executeQuery(query);
			row.next();
			view.setREGN_NO(row.getString("REGN_NO"));
			view.setPERSON_NAME(row.getString("PERSON_NAME"));
			view.setCP_NAME(row.getString("CP_NAME"));
			view.setCASHIER_NAME(row.getString("CASHIER_NAME"));
			view.setSUBSPECIALTY_DESC(row.getString("SUBSPECIALTY_DESC"));
			view.setJOB_NO(row.getString("JOB_NO"));
			view.setREGN_TYPE(row.getString("REGN_TYPE"));
			view.setADDRESS(row.getString("ADDRESS"));
			view.setREGN_DATE(row.getString("REGN_DATE"));
			view.setCARDNO(row.getString("CARDNO"));
			view.setVISIT_NO(row.getString("VISIT_NO"));
			view.setAPPOINTMENT_TIME(row.getString("APPOINTMENT_TIME"));
			view.setSEQ_NO(row.getString("SEQ_NO"));
			view.setGHF_AMT(row.getString("GHF_AMT"));
			view.setZLF_AMT(row.getString("ZLF_AMT"));
			view.setTOTAL_AMT(row.getString("TOTAL_AMT"));
			view.setAUTO_PAY(row.getString("AUTO_PAY"));
			view.setEFFICTIVE_DATETIME(row.getString("EFFICTIVE_DATETIME"));
			view.setSESSION_DESC(row.getString("SESSION_DESC"));
			view.setPRINT_DATE(row.getString("PRINT_DATE"));
			row.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return view;
	}

	public static ListTotal gettotal(String idhari) {
		ListTotal view = new ListTotal();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		try {
			String cetak = "";
			if (idhari.equals("2")) {
				cetak = "MON_IND='Y'";
			} else if (idhari.equals("3")) {
				cetak = "TUE_IND='Y'";
			} else if (idhari.equals("4")) {
				cetak = "WED_IND='Y'";
			} else if (idhari.equals("5")) {
				cetak = "THU_IND='Y'";
			} else if (idhari.equals("6")) {
				cetak = "FRI_IND='Y'";
			} else if (idhari.equals("7")) {
				cetak = "SAT_IND='Y'";
			} else {
				cetak = "SUN_IND='Y'";
			}

			String abdatabp = "SELECT count(rm.RESOURCEMSTR_ID) tot "
					+ "FROM RESOURCEMSTR rm inner join RESOURCEPROFILE rp on "
					+ "rm.RESOURCEMSTR_ID = rp.RESOURCEMSTR_ID inner join SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID "
					+ "WHERE rm.SUBSPECIALTYMSTR_ID=311187819 and rm.defunct_ind = 'N' and rp.DEFUNCT_IND = 'N' and "
					+ cetak + "";

			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			haspas.next();
			view.setTot(haspas.getString("tot"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return view;
	}

	public static List<ListResourceMstr> getResourceMasterjaddok() {
		List<ListResourceMstr> all = new ArrayList<ListResourceMstr>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "SELECT rm.RESOURCEMSTR_ID, rm.RESOURCE_NAME, rm.CAREPROVIDER_ID, sm.SESSION_DESC, rp.MON_IND, rp.TUE_IND, rp.WED_IND, rp.THU_IND, "
				+ "rp.FRI_IND, rp.SAT_IND, rp.SUN_IND, rm.POLI_CODE FROM RESOURCEMSTR rm inner join RESOURCEPROFILE rp on "
				+ "rm.RESOURCEMSTR_ID = rp.RESOURCEMSTR_ID inner join SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID "
				+ "WHERE rm.SUBSPECIALTYMSTR_ID in(336581902, 311099883, 311099876, 311099877, 311530438) and rm.defunct_ind = 'N' and rp.DEFUNCT_IND = 'N'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListResourceMstr lp = new ListResourceMstr();
				lp.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
				lp.setRESOURCE_NAME(haspas.getString("RESOURCE_NAME"));
				lp.setSESSION_DESC(haspas.getString("SESSION_DESC"));
				lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
				lp.setMON_IND(haspas.getString("MON_IND"));
				lp.setTUE_IND(haspas.getString("TUE_IND"));
				lp.setWED_IND(haspas.getString("WED_IND"));
				lp.setTHU_IND(haspas.getString("THU_IND"));
				lp.setFRI_IND(haspas.getString("FRI_IND"));
				lp.setSAT_IND(haspas.getString("SAT_IND"));
				lp.setSUN_IND(haspas.getString("SUN_IND"));
				lp.setPOLI_CODE(haspas.getString("POLI_CODE"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListResourceScheme> getResourceScheme(String resourceMstrId, String regnDate) {
		List<ListResourceScheme> all = new ArrayList<ListResourceScheme>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select RM.RESOURCESCHEME_ID, RM.RESOURCEMSTR_ID, TO_CHAR(RM.REGN_DATE,'DD-MM-YYYY') AS DATE_SCHEDULE, "
				+ "SM.SESSION_DESC from RESOURCESCHEME RM, SESSIONMSTR SM where TO_CHAR(REGN_DATE, 'DDMMYYYY') = '"
				+ regnDate + "' AND " + "RESOURCEMSTR_ID='" + resourceMstrId
				+ "' AND SM.SESSIONMSTR_ID=RM.SESSIONMSTR_ID";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListResourceScheme lp = new ListResourceScheme();
				lp.setRESOURCESCHEME_ID(haspas.getString("RESOURCESCHEME_ID"));
				lp.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
				lp.setDATE_SCHEDULE(haspas.getString("DATE_SCHEDULE"));
				lp.setSESSION_DESC(haspas.getString("SESSION_DESC"));
				all.add(lp);
			}
			// lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListPatientId> getPatiendId(String cardNo) {
		List<ListPatientId> all = new ArrayList<ListPatientId>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select CD.CARD_NO, PT.PATIENT_ID, PS.PERSON_NAME from "
				+ "PATIENT PT, CARD CD, PERSON PS where CD.CARD_NO='" + cardNo + "' AND PT.PERSON_ID=CD.PERSON_ID AND "
				+ "PS.PERSON_ID=PT.PERSON_ID";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListPatientId lp = new ListPatientId();
				lp.setCARD_NO(haspas.getString("CARD_NO"));
				lp.setPATIENT_ID(haspas.getString("PATIENT_ID"));
				lp.setPERSON_NAME(haspas.getString("PERSON_NAME"));
				all.add(lp);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static ListInsertQueue getcancelQueue(String cardno, String tgl_berobat) {

		ListInsertQueue all = new ListInsertQueue();
		Connection connection = DbConnection.getQueueInstance();
		if (connection == null)
			return null;
		Statement stmt = null;
		String ambdata = "Select count(*) as jum from queue where card_no='" + cardno
				+ "' and queue_date=CONVERT(date, '" + tgl_berobat + "', 105)";
		try {
			stmt = connection.createStatement();
			ResultSet haspas = stmt.executeQuery(ambdata);
			haspas.next();
			String hasjum = haspas.getString("jum");
			if (hasjum.equals("0")) {
				all.setKeterangan("Anda Belum Memiliki Nomor Antrian Hari Ini");
				haspas.close();
				stmt.close();
			} else {
				Statement stmt2 = null;
				String ambdata2 = "UPDATE queue SET deleted = 1 WHERE card_no ='" + cardno
						+ "' and queue_date=CONVERT(date, '" + tgl_berobat + "', 105)";
				stmt2 = connection.createStatement();
				stmt2.executeUpdate(ambdata2);
				all.setKeterangan("OK");
				stmt2.close();

			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListInsertQueue> InsertQueue(String careProviderId, String patient_name, String card_no,
			String tgl_berobat, String PatientId, String location, String group_id) {

		List<ListInsertQueue> all = new ArrayList<ListInsertQueue>();
		Connection connection = DbConnection.getQueueInstance();
		if (connection == null)
			return null;
		Statement stmt = null;
		String ambdata = "Select count(*) as jum from queue where card_no='" + card_no + "' and doctor_code = '"
				+ careProviderId + "'and queue_date=CONVERT(date, '" + tgl_berobat + "', 105) and location = "
				+ location + " and deleted=0";
		try {
			int group = Integer.parseInt(group_id);
			stmt = connection.createStatement();
			ResultSet haspas = stmt.executeQuery(ambdata);
			haspas.next();
			ListInsertQueue li = new ListInsertQueue();
			String hasjum = haspas.getString("jum");
			if (hasjum.equals("1")) {
				li.setKeterangan("Anda sudah pernah registrasi untuk hari ini");
				all.add(li);
				haspas.close();
				stmt.close();
			} else {
				Statement stmt2 = null;

				int nominator = 0;
				if (location.equals("1"))
					nominator = group * 1000;

				String ambdata2 = "INSERT INTO queue (location ,queue_no ,doctor_code ,group_id ,resourcemstr_id ,patient_name ,card_no ,counter_id ,calling_ind ,endserve_ind ,status ,queue_date, "
						+ "created_at ,created_by ,updated_at,updated_by ,deleted) VALUES (" + location
						+ ", (select coalesce(max(queue_no)," + nominator + ")+1 from queue q where q.location = "
						+ location + " and q.group_id = " + String.valueOf(group)
						+ " and convert(date, q.queue_date) = CONVERT(date, '" + tgl_berobat
						+ "', 105) and q.deleted = 0),null," + group_id + ", '" + careProviderId + "', " + "'"
						+ patient_name + "', '" + card_no + "', null, null, null, null, CONVERT(date, '" + tgl_berobat
						+ "', 105), " + "CURRENT_TIMESTAMP, '" + PatientId + "', CURRENT_TIMESTAMP, '" + PatientId
						+ "', 0)";
				stmt2 = connection.createStatement();
				stmt2.executeUpdate(ambdata2);
				ListInsertQueue li2 = new ListInsertQueue();
				li2.setKeterangan("Anda telah berhasil melakukan registrasi");

				all.add(li2);

				stmt2.close();

			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListInsertQueue> InsertQueue2(String careProviderId, String patient_name, String card_no,
			String tgl_berobat, String PatientId) {

		List<ListInsertQueue> all = new ArrayList<ListInsertQueue>();
		Connection connection = DbConnection.getQueueInstance();
		if (connection == null)
			return null;
		Statement stmt = null;
		String ambdata = "Select count(*) as jum from queue where card_no='" + card_no
				+ "' and queue_date=CONVERT(date, '" + tgl_berobat + "', 105) and deleted = 0 and resourcemstr_id = "
				+ careProviderId;
		try {
			stmt = connection.createStatement();
			ResultSet haspas = stmt.executeQuery(ambdata);
			haspas.next();
			ListInsertQueue li = new ListInsertQueue();
			String hasjum = haspas.getString("jum");
			if (hasjum.equals("1")) {
				li.setKeterangan("Anda sudah pernah registrasi untuk hari ini");
				all.add(li);
				haspas.close();
				stmt.close();
			} else {
				Statement stmt2 = null;
				String ambdata2 = "INSERT INTO queue (location ,queue_no ,doctor_code ,group_id ,resourcemstr_id ,patient_name ,card_no ,counter_id ,calling_ind ,endserve_ind ,status ,queue_date, "
						+ "created_at ,created_by ,updated_at,updated_by ,deleted) VALUES (2, (select coalesce(max(queue_no),4000)+1 from queue q where q.location = 2 "
						+ "and q.group_id = 2 and convert(date, q.queue_date) = CONVERT(date, '" + tgl_berobat
						+ "', 105) and q.deleted = 0),null,2, '" + careProviderId + "', " + "'" + patient_name + "', '"
						+ card_no + "', null, null, null, null, CONVERT(date, '" + tgl_berobat + "', 105), "
						+ "CURRENT_TIMESTAMP, '" + PatientId + "', CURRENT_TIMESTAMP, '" + PatientId + "', 0)";
				stmt2 = connection.createStatement();
				stmt2.executeUpdate(ambdata2);
				ListInsertQueue li2 = new ListInsertQueue();
				li2.setKeterangan("Anda telah berhasil melakukan registrasi");

				all.add(li2);

				stmt2.close();

			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListGetQueue> GetQueue(String cardNo, String patientId, String queueDate, String location,
			String group_id) {
		List<ListGetQueue> all = new ArrayList<ListGetQueue>();
		Connection connection = DbConnection.getQueueInstance();
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select queue_no, resourcemstr_id, queue_date, card_no, patient_name, created_by, created_at, updated_at, updated_by, deleted "
				+ "from queue where card_no='" + cardNo + "' and created_by ='" + patientId
				+ "' and queue_date=CONVERT(date, '" + queueDate + "', 105) and deleted = 0 and group_id='" + group_id
				+ "' and location = " + location;
		try {
			stmt = connection.createStatement();
			ResultSet haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListGetQueue lp = new ListGetQueue();
				lp.setQueue_no(haspas.getString("queue_no"));
				lp.setResourcemstr_id(haspas.getString("resourcemstr_id"));
				lp.setQueue_date(haspas.getString("queue_date"));
				lp.setCard_no(haspas.getString("card_no"));
				lp.setPatient_name(haspas.getString("patient_name"));
				lp.setCreated_by(haspas.getString("created_by"));
				lp.setCreated_at(haspas.getString("created_at"));
				lp.setUpdated_at(haspas.getString("updated_at"));
				lp.setUpdated_by(haspas.getString("updated_by"));
				lp.setDeleted(haspas.getString("deleted"));
				all.add(lp);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListGetQueue> GetQueue2(String cardNo, String patientId, String queueDate,
			String careprovider_id) {
		List<ListGetQueue> all = new ArrayList<ListGetQueue>();
		Connection connection = DbConnection.getQueueInstance();
		if (connection == null)
			return null;
		Statement stmt = null;

		String abdatabp = "";
		if (careprovider_id == null) {
			abdatabp = "select queue_no, resourcemstr_id, queue_date, card_no, patient_name, created_by, created_at, updated_at, updated_by, deleted "
					+ "from queue where card_no='" + cardNo + "' and created_by ='" + patientId
					+ "' and queue_date=CONVERT(date, '" + queueDate + "', 105) and group_id='2' ";
		} else {
			abdatabp = "select queue_no, resourcemstr_id, queue_date, card_no, patient_name, created_by, created_at, updated_at, updated_by, deleted "
					+ "from queue where card_no='" + cardNo + "' and created_by ='" + patientId
					+ "' and queue_date=CONVERT(date, '" + queueDate + "', 105) and group_id='2' "
					+ " and resourcemstr_id = " + careprovider_id;
		}

		try {
			stmt = connection.createStatement();
			ResultSet haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListGetQueue lp = new ListGetQueue();
				lp.setQueue_no(haspas.getString("queue_no"));
				lp.setResourcemstr_id(haspas.getString("resourcemstr_id"));
				lp.setQueue_date(haspas.getString("queue_date"));
				lp.setCard_no(haspas.getString("card_no"));
				lp.setPatient_name(haspas.getString("patient_name"));
				lp.setCreated_by(haspas.getString("created_by"));
				lp.setCreated_at(haspas.getString("created_at"));
				lp.setUpdated_at(haspas.getString("updated_at"));
				lp.setUpdated_by(haspas.getString("updated_by"));
				lp.setDeleted(haspas.getString("deleted"));
				all.add(lp);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListResDokter> GetResDokter(String careProviderId) {
		List<ListResDokter> all = new ArrayList<ListResDokter>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "SELECT RM.RESOURCEMSTR_ID, RM.RESOURCE_NAME FROM RESOURCEMSTR RM "
				+ "WHERE RM.SUBSPECIALTYMSTR_ID='336581902' AND RM.CAREPROVIDER_ID='" + careProviderId + "'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListResDokter lp = new ListResDokter();
				lp.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
				lp.setRESOURCE_NAME(haspas.getString("RESOURCE_NAME"));
				all.add(lp);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static DetailDokter GetDetailDokter(String dokterId) {
		DetailDokter dd = new DetailDokter();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		Statement stmt = null;
		String query = "SELECT RESOURCE_NAME,regexp_replace((SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR "
				+ " WHERE SUBSPECIALTY_CODE = RM.POLI_CODE AND DEFUNCT_IND='N' ),'PSB','') POLI FROM RESOURCEMSTR RM WHERE RESOURCEMSTR_ID= "
				+ dokterId + "";
		try {
			if (connection == null)
				return null;
			stmt = connection.createStatement();

			try {
				haspas = stmt.executeQuery(query);
			} catch (Exception e) {

			}

			haspas.next();
			dd.setRESOURCE_NAME(haspas.getString("RESOURCE_NAME"));
			dd.setPOLI(haspas.getString("POLI"));
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return dd;
	}

	public static List<ListQueueBpjs> GetQueueBpjs(String counterId) {
		List<ListQueueBpjs> all = new ArrayList<ListQueueBpjs>();
		Connection connection = DbConnection.getQueueInstance();
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select q.queue_no from queue q where "
				+ "q.queue_date = CONVERT(date, CURRENT_TIMESTAMP) and q.counter_id = '" + counterId
				+ "' and q.deleted=0 and q.endserve_ind= 0";
		try {
			stmt = connection.createStatement();
			ResultSet haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListQueueBpjs lp = new ListQueueBpjs();
				lp.setQueue_no(haspas.getString("queue_no"));
				all.add(lp);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListQueueUser> GetQueueUser(String userMstrId) {
		// TODO Auto-generated method stub
		List<ListQueueUser> all = new ArrayList<ListQueueUser>();
		Connection connection = DbConnection.getQueueInstance();
		if (connection == null)
			return null;
		Statement stmt = null;
		String abdatabp = "select q.queue_no from queue q inner join counter c on q.counter_id = c.counter_id "
				+ "where q.queue_date = CONVERT(date, CURRENT_TIMESTAMP) and q.deleted=0 and q.endserve_ind= 0 "
				+ "and c.usermstr_id = '" + userMstrId + "'";
		try {
			stmt = connection.createStatement();
			ResultSet haspas = stmt.executeQuery(abdatabp);
			while (haspas.next()) {
				ListQueueUser lp = new ListQueueUser();
				lp.setQueue_no(haspas.getString("queue_no"));
				all.add(lp);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static KelasPasien GetKelasPasien(String Pc) {
		Connection connection = DbConnection.getPooledConnection();
		KelasPasien view = new KelasPasien();
		Statement stmt = null;
		ResultSet haspas = null;
		try {
			String query = "select CODE_DESC from CODEMSTR WHERE CODE_CAT || '' ||CODE_ABBR='" + Pc + "'";

			if (connection == null)
				return null;
			stmt = connection.createStatement();

			try {
				haspas = stmt.executeQuery(query);
			} catch (Exception e) {

			}

			haspas.next();
			view.setCODE_DESC(haspas.getString("CODE_DESC"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return view;
	}

	public static KamarPasien GetKamarPasien(String Vid) {
		Connection connection = DbConnection.getPooledConnection();
		KamarPasien view = new KamarPasien();
		Statement stmt = null;
		ResultSet haspas = null;
		try {

			String query = "select * from (select wm.ward_desc,bm.Bed_No,bh.visit_id from wardmstr wm, roommstr rm, bedmstr bm, bedhistory bh "
					+ "where wm.wardmstr_id=rm.wardmstr_id "
					+ " and rm.roommstr_id = bm.roommstr_id and bm.bedmstr_id=bh.bedmstr_id " + "and bh.visit_id='"
					+ Vid + "' order by bh.effective_start_datetime desc)";

			if (connection == null)
				return null;
			stmt = connection.createStatement();

			try {
				haspas = stmt.executeQuery(query);
			} catch (Exception e) {

			}

			haspas.next();
			view.setWard_Desc(haspas.getString("Ward_Desc"));
			view.setBed_No(haspas.getString("Bed_No"));
			view.setVisit_Id(haspas.getString("Visit_Id"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return view;
	}

	public static VisitId GetVisitId(String mrn) {
		Connection connection = DbConnection.getPooledConnection();
		VisitId view = new VisitId();
		Statement stmt = null;
		ResultSet haspas = null;
		try {

			String query = "SELECT * FROM (SELECT VT.VISIT_ID FROM CARD CD "
					+ "INNER JOIN PATIENT PT ON PT.PERSON_ID = CD.PERSON_ID "
					+ "INNER JOIN VISIT VT ON VT.PATIENT_ID = PT.PATIENT_ID " + "WHERE CD.CARD_NO='" + mrn
					+ "' AND VT.PATIENT_TYPE='PTY1' ORDER BY VT.ADMISSION_DATETIME DESC) WHERE ROWNUM=1";

			if (connection == null)
				return null;
			stmt = connection.createStatement();

			try {
				haspas = stmt.executeQuery(query);
			} catch (Exception e) {

			}

			haspas.next();
			view.setVISIT_ID(haspas.getString("VISIT_ID"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return view;
	}

	public static ListDietPasien GetDietPasien(String Mrn) {
		Connection connection = DbConnection.getPooledConnection();
		ListDietPasien view = new ListDietPasien();
		if (connection == null)
			return null;
		PreparedStatement stmt = null;
		PreparedStatement stmt2 = null;
		ResultSet haspas = null;
		ResultSet row2 = null;
		try {
			// String query = "select distinct v.visit_id vid, p.person_name pn, "
			// +
			// "TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(p.BIRTH_DATE,'yyyymmdd')))/10000)||'
			// Th ' ||
			// TRUNC(months_between(sysdate,p.BIRTH_DATE)-12*trunc(months_between(sysdate,p.BIRTH_DATE)/12))
			// || ' Bl' Age,"
			// + "(select CODE_DESC from CODEMSTR WHERE CODE_CAT || ''
			// ||CODE_ABBR=v.patient_class) pc "
			// + "from visit v, patient pt, person p, card c,visitdiagnosis t "
			// + "where v.patient_id=pt.patient_id and pt.person_id=p.person_id and
			// c.person_id=p.person_id and t.visit_id = v.visit_id and "
			// + "c.card_no=? and v.patient_type='PTY1' and v.admit_status='AST1' and
			// (T.DIAGNOSIS_TYPE != 'DGT99' or T.DIAGNOSIS_TYPE != 'DGT9')";

			String query = "select  v.visit_id vid, p.person_name pn, "
					+ "TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(p.BIRTH_DATE,'yyyymmdd')))/10000)||' Th ' || TRUNC(months_between(sysdate,p.BIRTH_DATE)-12*trunc(months_between(sysdate,p.BIRTH_DATE)/12)) || ' Bl' Age, "
					+ "(select CODE_DESC from CODEMSTR WHERE CODE_CAT || '' ||CODE_ABBR=v.patient_class) pc  "
					+ "from visit v, patient pt, person p, card c "
					+ "where v.patient_id=pt.patient_id and pt.person_id=p.person_id and c.person_id=p.person_id and "
					+ "c.card_no=? and v.patient_type='PTY1' and v.discharge_datetime is null ";

			stmt = connection.prepareStatement(query);
			stmt.setEscapeProcessing(true);
			stmt.setQueryTimeout(60000);
			stmt.setString(1, Mrn);
			haspas = stmt.executeQuery();

			if (haspas.next()) {
				String vid = haspas.getString("vid");
				view.setVid(vid);
				view.setPn(haspas.getString("pn"));
				view.setAge(haspas.getString("Age"));
				view.setPc(haspas.getString("pc"));

				String query2 = "select * from (select wm.ward_desc wd,bm.Bed_No bn from wardmstr wm, roommstr rm, bedmstr bm, bedhistory bh "
						+ "where wm.wardmstr_id=rm.wardmstr_id and rm.roommstr_id = bm.roommstr_id and bm.bedmstr_id=bh.bedmstr_id "
						+ "and bh.visit_id=? order by bh.effective_start_datetime desc) where ROWNUM=1";

				stmt2 = connection.prepareStatement(query2);
				stmt2.setEscapeProcessing(true);
				stmt2.setQueryTimeout(60000);
				stmt2.setString(1, vid);
				row2 = stmt2.executeQuery();

				if (row2.next()) {
					String wd = row2.getString("wd");
					String bn = row2.getString("bn");
					view.setRgn(wd);
					view.setKmr(bn);
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (row2 != null)
				try {
					row2.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return view;

	}

	public static List<ListOnlineReg> GetOnlineReg(String Barcode) {
		List<ListOnlineReg> all = new ArrayList<ListOnlineReg>();
		Connection connection2 = DbConnection.getBpjsOnlineInstance();
		if (connection2 == null)return null;
		Statement stmt2 = null;
		ResultSet haspas = null;
		try {
			String getqueue = "SELECT top 1 tr.queue_no qn,tr.resourcemstr_id rid,tr.queue_date dt,FORMAT(tr.queue_date,'dd-MM-yyyy') dta,tr.no_mrn cn, "
					+ "tu.no_bpjs nb,trj.reference_date tglrujuk, trj.reference noruk ,tr.department dept "
					+ "FROM tb_registrasi tr " + "INNER JOIN tb_user tu on tu.no_mrn = tr.no_mrn "
					+ "LEFT OUTER JOIN tb_rujukan trj on trj.no_mrn = tr.no_mrn "
					+ "where tr.status in ('Confirm','Registered') and tr.generate_barcode='" + Barcode
					+ "' order by tglrujuk desc,dt desc";
			stmt2 = connection2.createStatement();
			haspas = stmt2.executeQuery(getqueue);
			while (haspas.next()) {
				ListOnlineReg lp = new ListOnlineReg();
				String qn = haspas.getString("qn");
				lp.setQn(qn);
				lp.setRid(haspas.getString("rid"));
				lp.setDt(haspas.getString("dt"));
				lp.setCn(haspas.getString("cn"));
				lp.setNb(haspas.getString("nb"));
				lp.setTglrujuk(haspas.getString("tglrujuk"));
				lp.setNorujuk(haspas.getString("noruk"));
				lp.setDept(haspas.getString("dept"));
				all.add(lp);
			}

			try {
				List<CurrentDatetim> getCurrentDatetime = StoreDispanceSer.getCurrentDatetime();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date startDate = df.parse(all.get(0).getDt());

				DateFormat d3 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date currentDate = d3.parse(getCurrentDatetime.get(0).getTanggal());

				if (all.get(0).getDept().equals("02") && startDate.getDate() == currentDate.getDate()) {
					String careProviderId = all.get(0).getRid();
					String cardno = all.get(0).getCn();
					String tglberobat = all.get(0).getDt().replace("-", "");
					List<ListPatientId> pd = getPatiendId(cardno);
					String pit = pd.get(0).getPATIENT_ID();
					String napas = pd.get(0).getPERSON_NAME();

					List<ListInsertQueue> lq = InsertQueue2(careProviderId, napas, cardno, tglberobat, pit);
					List<ListGetQueue> gq = GetQueue2(cardno, pit, tglberobat, careProviderId);
					String noant = gq.get(0).getQueue_no();
					all.get(0).setQn(noant);
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			haspas.close();
			stmt2.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)try { haspas.close();} catch (Exception ignore) {	}
			if (stmt2 != null)try {	stmt2.close();} catch (Exception ignore) {	}
			if (connection2 != null)try {connection2.close();} catch (Exception ignore) {}
		}
		return all;
	}

	// public static List<ListOnlineReg> GetOnlineReg2(String Barcode) {
	// // TODO Auto-generated method stub
	// List<ListOnlineReg> all=new ArrayList<ListOnlineReg>();
	// Connection connection=DbConnection.getMysqlOnlineQueueInstance();
	// if(connection==null)return null;
	// Statement stmt=null;
	// String getqueue="Select tb_registrasi.queue_no qn , "
	// + "tb_registrasi.resourcemstr_id rid, tb_registrasi.queue_date dt,
	// tb_registrasi.card_no cn, "
	// + "tb_user.no_bpjs nb, tb_rujukan.tgl_rujukan_terakhir tglrujuk, "
	// + "tb_rujukan.no_rujukan noruk "
	// + "from tb_registrasi,tb_rujukan,tb_user "
	// + "where tb_rujukan.id_rujukan=tb_registrasi.id_rujukan and
	// tb_registrasi.card_no = tb_user.mrn "
	// + "and tb_registrasi.status='Confirmation' and
	// tb_registrasi.generate_barcode='"+Barcode+"'";
	// try{
	// stmt=connection.createStatement();
	// ResultSet haspas=stmt.executeQuery(getqueue);
	// while(haspas.next())
	// {
	// ListOnlineReg lp=new ListOnlineReg();
	// lp.setQn(haspas.getString("qn"));
	// lp.setRid(haspas.getString("rid"));
	// lp.setDt(haspas.getString("dt"));
	// lp.setCn(haspas.getString("cn"));
	// lp.setNb(haspas.getString("nb"));
	// lp.setTglrujuk(haspas.getString("tglrujuk"));
	// lp.setNorujuk(haspas.getString("noruk"));
	// lp.setDept("01");
	// all.add(lp);
	// }
	// haspas.close();
	// stmt.close();
	// }
	// catch (SQLException e)
	// {
	// System.out.println(e.getMessage());
	// }
	// finally {
	// if (stmt!=null) try { stmt.close(); } catch (Exception ignore){}
	// if (connection!=null) try { connection.close();} catch (Exception ignore){}
	// }
	// return all;
	// }

	public static List<ListPoliCode> GetPoliCode() {
		List<ListPoliCode> all = new ArrayList<ListPoliCode>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String selpol = "select cm.CODE_CAT, cm.CODE_ABBR, cm.CODE_DESC from CODEMSTR cm where cm.code_cat = 'POL' and defunct_ind = 'N'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(selpol);
			while (haspas.next()) {
				ListPoliCode lp = new ListPoliCode();
				lp.setCODE_CAT(haspas.getString("CODE_CAT"));
				lp.setCODE_ABBR(haspas.getString("CODE_ABBR"));
				lp.setCODE_DESC(haspas.getString("CODE_DESC"));
				all.add(lp);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListUserNameKT> GetUserName(String UserCode) {
		// TODO Auto-generated method stub
		List<ListUserNameKT> all = new ArrayList<ListUserNameKT>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String selpol = "SELECT USER_NAME, USER_CODE, PASSWORD FROM USERMSTR WHERE USERMSTR_ID='" + UserCode + "'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(selpol);
			while (haspas.next()) {
				ListUserNameKT lp = new ListUserNameKT();
				lp.setUSER_NAME(haspas.getString("USER_NAME"));
				lp.setUSER_CODE(haspas.getString("USER_CODE"));
				lp.setPASSWORD(haspas.getString("PASSWORD"));
				all.add(lp);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListTindakan> GetTindakan(String VisitId) {
		// TODO Auto-generated method stub
		List<ListTindakan> all = new ArrayList<ListTindakan>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String selpol = "SELECT TCM.TXN_CODE,CG.ORDERED_CAREPROVIDER_ID,TO_CHAR(PAT.TXN_DATETIME,'YYYY-MM-DD') TGL FROM PATIENTACCOUNT PA "
				+ "INNER JOIN PATIENTACCOUNTTXN PAT ON PAT.PATIENTACCOUNT_ID = PA.PATIENTACCOUNT_ID "
				+ "INNER JOIN TXNCODEMSTR TCM ON TCM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID "
				+ "INNER JOIN CHARGE CG ON CG.PATIENTACCOUNTTXN_ID = PAT.PATIENTACCOUNTTXN_ID "
				+ "WHERE PAT.CHARGE_STATUS='CTTNOR' AND PA.VISIT_ID='" + VisitId + "'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(selpol);
			while (haspas.next()) {
				ListTindakan lp = new ListTindakan();
				lp.setTXN_CODE(haspas.getString("TXN_CODE"));
				lp.setORDERED_CAREPROVIDER_ID(haspas.getString("ORDERED_CAREPROVIDER_ID"));
				lp.setTGL(haspas.getString("TGL"));
				all.add(lp);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<DoctorSchedule> getDailyDoctorSchedule(String RegnDate) {
		List<DoctorSchedule> all = new ArrayList<DoctorSchedule>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		PreparedStatement preparedStatement = null;
		if (connection == null)
			return null;

		String sql = "select Rs.RESOURCESCHEME_ID, Rs.RESOURCEMSTR_ID,  TO_CHAR(Rs.REGN_DATE,'DD-MM-YYYY HH:MI') AS DATE_SCHEDULE, pgcommon.fxGetCodeDesc(rm.poli_code) as poli, rm.resource_name,"
				+ " SM.START_TIME, sm.end_time, rs.normal_regn_limit from RESOURCESCHEME Rs, SESSIONMSTR SM, resourcemstr rm"
				+ " where TO_CHAR(REGN_DATE, 'DDMMYYYY') = ?  AND SM.SESSIONMSTR_ID=rs.SESSIONMSTR_ID and rm.resourcemstr_id = rs.resourcemstr_id"
				+ " and rm.defunct_ind = 'N' and rm.SUBSPECIALTYMSTR_ID in(336581902, 311099883, 311099876, 311099877, 311530438)";
		try {

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, RegnDate);

			haspas = preparedStatement.executeQuery();
			while (haspas.next()) {
				DoctorSchedule lp = new DoctorSchedule();
				lp.setResourcemstrId(haspas.getBigDecimal("RESOURCEMSTR_ID"));
				lp.setResourceName(haspas.getString("resource_name"));
				lp.setSpecialyst(haspas.getString("POLI"));
				lp.setMaxReg(haspas.getInt("normal_regn_limit"));
				lp.setRegnDate(DataUtils.StringToDate(haspas.getString("DATE_SCHEDULE")));
				lp.setStartTime(DataUtils.StringToDate(haspas.getString("START_TIME")));
				lp.setEndTime(DataUtils.StringToDate(haspas.getString("end_time")));
				all.add(lp);
			}
			haspas.close();
			preparedStatement.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (preparedStatement != null)
				try {
					preparedStatement.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListReceipt> GetReceipt(String sysReceptNo) {
		// TODO Auto-generated method stub
		List<ListReceipt> all = new ArrayList<ListReceipt>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		String selpol = "select RECEIPT_ID from receipt where system_receipt_no = '" + sysReceptNo + "'";
		try {
			stmt = connection.createStatement();
			haspas = stmt.executeQuery(selpol);
			while (haspas.next()) {
				ListReceipt lp = new ListReceipt();
				lp.setRECEIPT_ID(haspas.getString("RECEIPT_ID"));
				all.add(lp);
			}
			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListKamarHiv> ListKamar() {
		List<ListKamarHiv> all = new ArrayList<ListKamarHiv>();
		Connection connection = DbConnection.getRoomConnection();
		Connection connection2 = DbConnection.getPooledConnection();
		ResultSet row = null;
		ResultSet row2 = null;
		ResultSet row3 = null;
		ResultSet row4 = null;
		ResultSet row5 = null;
		if (connection == null || connection2 == null)
			return null;
		Statement stmt = null;
		Statement stmt2 = null;
		Statement stmt3 = null;
		Statement stmt4 = null;
		Statement stmt5 = null;
		YearMonth thisMonth = YearMonth.now();
		YearMonth lastMonth = thisMonth.minusMonths(1);

		DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("yyyy-MM");
		String blns = thisMonth.format(monthYearFormatter);
		String blnl = lastMonth.format(monthYearFormatter);
		String query1 = "SELECT nomor,mrn,created_at,tgltes,tglhsl,sgt FROM [mtdocumentcontainer].[dbo].[hiv_pitc] where (created_at between '"
				+ blnl + "-26 00:00:00' and '" + blns + "-25 23:59:59')";
		try {
			stmt = connection.createStatement();
			row = stmt.executeQuery(query1);
			while (row.next()) {
				ListKamarHiv lkv = new ListKamarHiv();
				String mrn = row.getString("mrn");
				String query2 = "SELECT * FROM (SELECT PS.PERSON_NAME,VD.VISIT_ID FROM CARD CD,PERSON PS,VISIT VD,PATIENT PT "
						+ "WHERE CD.CARD_NO=" + mrn + " AND PS.PERSON_ID=CD.PERSON_ID AND "
						+ "PS.PERSON_ID=PT.PERSON_ID AND PT.PATIENT_ID=VD.PATIENT_ID ORDER BY VD.VISIT_ID DESC) WHERE ROWNUM<=1";
				stmt2 = connection2.createStatement();
				row2 = stmt2.executeQuery(query2);
				row2.next();

				String vid = row2.getString("VISIT_ID");
				String pn = row2.getString("PERSON_NAME");
				String query3 = "select count(*) htg from (select wm.ward_desc,bm.Bed_No,bh.visit_id from wardmstr wm, roommstr rm, bedmstr bm, bedhistory bh "
						+ "where wm.wardmstr_id=rm.wardmstr_id "
						+ " and rm.roommstr_id = bm.roommstr_id and bm.bedmstr_id=bh.bedmstr_id " + "and bh.visit_id='"
						+ vid + "' order by bh.effective_start_datetime desc)";

				stmt3 = connection2.createStatement();
				row3 = stmt3.executeQuery(query3);
				row3.next();

				String tung = row3.getString("htg");
				if (tung.equals("0")) {
					String query5 = "SELECT SBS.SUBSPECIALTY_DESC FROM VISIT V,SUBSPECIALTYMSTR SBS "
							+ "WHERE V.SUBSPECIALTYMSTR_ID=SBS.SUBSPECIALTYMSTR_ID AND V.VISIT_ID=" + vid + "";
					stmt5 = connection2.createStatement();
					row5 = stmt5.executeQuery(query5);
					row5.next();

					lkv.setNomor(row.getString("nomor"));
					lkv.setMrn(mrn);
					lkv.setNamaPasien(pn);
					lkv.setRuangan(row5.getString("SUBSPECIALTY_DESC"));
					lkv.setKamar("-");
					lkv.setTgltes(row.getString("tgltes"));
					lkv.setTglhsl(row.getString("tglhsl"));
					lkv.setSgt(row.getString("sgt"));
					lkv.setCreated_at(row.getString("created_at"));
					all.add(lkv);
					stmt5.close();
					row5.close();
					row3.close();
					stmt3.close();
				} else {
					String query4 = "select * from (select wm.ward_desc,bm.Bed_No,bh.visit_id from wardmstr wm, roommstr rm, bedmstr bm, bedhistory bh "
							+ "where wm.wardmstr_id=rm.wardmstr_id "
							+ " and rm.roommstr_id = bm.roommstr_id and bm.bedmstr_id=bh.bedmstr_id "
							+ "and bh.visit_id='" + vid + "' order by bh.effective_start_datetime desc)";

					stmt4 = connection2.createStatement();
					row4 = stmt3.executeQuery(query4);
					row4.next();
					lkv.setNomor(row.getString("nomor"));
					lkv.setMrn(mrn);
					lkv.setNamaPasien(pn);
					lkv.setRuangan(row4.getString("Ward_Desc"));
					lkv.setKamar(row4.getString("Bed_No"));
					lkv.setTgltes(row.getString("tgltes"));
					lkv.setTglhsl(row.getString("tglhsl"));
					lkv.setSgt(row.getString("sgt"));
					lkv.setCreated_at(row.getString("created_at"));
					all.add(lkv);
					row4.close();
					stmt4.close();
					row3.close();
					stmt3.close();
				}
				// all.add(lkv);
				row2.close();
				stmt2.close();
			}
			row.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
			if (row2 != null)
				try {
					row2.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (row3 != null)
				try {
					row3.close();
				} catch (Exception ignore) {
				}
			if (stmt3 != null)
				try {
					stmt3.close();
				} catch (Exception ignore) {
				}
			if (row4 != null)
				try {
					row4.close();
				} catch (Exception ignore) {
				}
			if (stmt4 != null)
				try {
					stmt4.close();
				} catch (Exception ignore) {
				}
			if (row5 != null)
				try {
					row5.close();
				} catch (Exception ignore) {
				}
			if (stmt5 != null)
				try {
					stmt5.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListRuangan> ListRuangan() {
		List<ListRuangan> all = new ArrayList<ListRuangan>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet row = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		try {
			String query = "SELECT WM.WARD_DESC || '( IP )' AS RUANGAN,1 NILAI,WM.WARD_DESC SUBSPECIALTY FROM "
					+ "MEDICAL_RESUME MR, VISIT V, WARDMSTR WM,BEDMSTR BM, DIAGNOSISMSTR DMI,DIAGNOSISMSTR DMM, "
					+ "CAREPROVIDER CP, PERSON P, PERSON PS2, CARD CD,PATIENT PT, SUBSPECIALTYMSTR SM WHERE "
					+ "MR.VISIT_ID = V.VISIT_ID AND " + "V.WARDMSTR_ID = WM.WARDMSTR_ID AND "
					+ "V.BEDMSTR_ID = BM.BEDMSTR_ID(+) AND " + "MR.IN_DIAGNOSISMSTR_ID = DMI.DIAGNOSISMSTR_ID  AND "
					+ "MR.MAIN_DIAGNOSISMSTR_ID = DMM.DIAGNOSISMSTR_ID AND "
					+ "MR.CAREPROVIDER_ID = CP.CAREPROVIDER_ID AND " + "CP.PERSON_ID = P.PERSON_ID AND "
					+ "V.PATIENT_ID = PT.PATIENT_ID AND " + "PT.PERSON_ID = PS2.PERSON_ID AND "
					+ "SM.SUBSPECIALTYMSTR_ID = V.SUBSPECIALTYMSTR_ID AND "
					+ "CD.PERSON_ID = PS2.PERSON_ID AND MR.LAST_UPDATED_DATETIME > TRUNC(SYSDATE) GROUP BY WM.WARD_DESC "
					+ "UNION ALL " + "SELECT " + "SM.SUBSPECIALTY_DESC || '( OP )',2 ,SM.SUBSPECIALTY_DESC " + "FROM  "
					+ "MEDICAL_RESUME MR,VISIT V,DIAGNOSISMSTR DMM,CAREPROVIDER CP,PERSON P,PERSON PS, "
					+ "PATIENT PT,ADDRESS AR,CARD C,SUBSPECIALTYMSTR SM " + "WHERE MR.VISIT_ID = V.VISIT_ID  "
					+ "AND MR.MAIN_DIAGNOSISMSTR_ID = DMM.DIAGNOSISMSTR_ID  "
					+ "AND MR.CAREPROVIDER_ID = CP.CAREPROVIDER_ID AND CP.PERSON_ID = P.PERSON_ID AND V.PATIENT_ID = PT.PATIENT_ID "
					+ "AND PT.PERSON_ID = PS.PERSON_ID " + "AND AR.STAKEHOLDER_ID= PS.STAKEHOLDER_ID "
					+ "AND C.PERSON_ID = PS.PERSON_ID  " + "AND SM.SUBSPECIALTYMSTR_ID= V.SUBSPECIALTYMSTR_ID "
					+ "AND MR.LAST_UPDATED_DATETIME > TRUNC(SYSDATE) GROUP BY SM.SUBSPECIALTY_DESC ORDER BY RUANGAN";

			stmt = connection.createStatement();
			row = stmt.executeQuery(query);
			while (row.next()) {
				ListRuangan lr = new ListRuangan();
				lr.setRuangan(row.getString("RUANGAN"));
				lr.setNilai(row.getString("NILAI"));
				lr.setSubspecialty(row.getString("SUBSPECIALTY"));
				all.add(lr);
			}
			row.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ObatBpjs> GetBpjsMedicine(String vid) {
		List<ObatBpjs> all = new ArrayList<ObatBpjs>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet row = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		try {
			String query = "SELECT NAMA_OBAT FROM OBATBPJS WHERE VISIT_ID='" + vid + "' AND  DEFUNCT_IND='N'";

			stmt = connection.createStatement();
			row = stmt.executeQuery(query);
			while (row.next()) {
				ObatBpjs lr = new ObatBpjs();
				lr.setNamaObat(row.getString("NAMA_OBAT"));
				all.add(lr);
			}
			row.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static List<ListPasien> ListPasien(String Ruangan, String Tipe) {
		List<ListPasien> all = new ArrayList<ListPasien>();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet row = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		try {
			if (Tipe.equals("1")) {
				String query = "SELECT MR.MEDICAL_RESUME_ID,MR.VISIT_ID,PS2.PERSON_NAME,CD.CARD_NO "
						+ "FROM MEDICAL_RESUME MR,VISIT V,WARDMSTR WM,BEDMSTR BM,DIAGNOSISMSTR DMI,DIAGNOSISMSTR DMM,CAREPROVIDER CP, "
						+ "PERSON P,PERSON PS2,CARD CD,PATIENT PT WHERE "
						+ "MR.VISIT_ID = V.VISIT_ID AND V.WARDMSTR_ID = WM.WARDMSTR_ID AND V.BEDMSTR_ID = BM.BEDMSTR_ID(+) AND "
						+ "MR.IN_DIAGNOSISMSTR_ID = DMI.DIAGNOSISMSTR_ID  AND MR.MAIN_DIAGNOSISMSTR_ID = DMM.DIAGNOSISMSTR_ID AND "
						+ "MR.CAREPROVIDER_ID = CP.CAREPROVIDER_ID AND " + "CP.PERSON_ID = P.PERSON_ID AND "
						+ "V.PATIENT_ID = PT.PATIENT_ID AND " + "PT.PERSON_ID = PS2.PERSON_ID AND "
						+ "CD.PERSON_ID = PS2.PERSON_ID AND " + "MR.LAST_UPDATED_DATETIME > TRUNC(SYSDATE)  "
						+ "AND WM.WARD_DESC='" + Ruangan + "'";

				stmt = connection.createStatement();
				row = stmt.executeQuery(query);
				while (row.next()) {
					ListPasien lp = new ListPasien();
					lp.setMedicalResumeId(row.getString("MEDICAL_RESUME_ID"));
					lp.setVisitId(row.getString("VISIT_ID"));
					lp.setPersonName(row.getString("PERSON_NAME"));
					lp.setCardNo(row.getString("CARD_NO"));
					lp.setRuangan(Ruangan);
					lp.setTipe(Tipe);
					all.add(lp);
				}
				row.close();
				stmt.close();
			} else {
				String query = "SELECT MR.MEDICAL_RESUME_ID,MR.VISIT_ID,PS.PERSON_NAME,C.CARD_NO "
						+ "FROM MEDICAL_RESUME MR,VISIT V,DIAGNOSISMSTR DMM,CAREPROVIDER CP, "
						+ "PERSON P,PERSON PS,PATIENT PT,ADDRESS AR,CARD C,CAREPROVIDERLICENSEHISTORY CPL,SUBSPECIALTYMSTR SM "
						+ "WHERE MR.VISIT_ID = V.VISIT_ID " + "AND MR.MAIN_DIAGNOSISMSTR_ID = DMM.DIAGNOSISMSTR_ID "
						+ "AND MR.CAREPROVIDER_ID = CP.CAREPROVIDER_ID " + "AND CP.PERSON_ID = P.PERSON_ID "
						+ "AND V.PATIENT_ID = PT.PATIENT_ID " + "AND PT.PERSON_ID = PS.PERSON_ID "
						+ "AND AR.STAKEHOLDER_ID= PS.STAKEHOLDER_ID " + "AND C.PERSON_ID = PS.PERSON_ID   "
						+ "AND SM.SUBSPECIALTYMSTR_ID= V.SUBSPECIALTYMSTR_ID "
						+ "AND CPL.CAREPROVIDER_ID(+) = CP.CAREPROVIDER_ID " + "AND AR.ADDRESS_TYPE='ADRRES'  "
						+ "AND MR.LAST_UPDATED_DATETIME > TRUNC(SYSDATE) " + "AND SM.SUBSPECIALTY_DESC='" + Ruangan
						+ "' " + "ORDER BY AR.ADDRESS_ID";
				stmt = connection.createStatement();
				row = stmt.executeQuery(query);
				while (row.next()) {
					ListPasien lp = new ListPasien();
					lp.setMedicalResumeId(row.getString("MEDICAL_RESUME_ID"));
					lp.setVisitId(row.getString("VISIT_ID"));
					lp.setPersonName(row.getString("PERSON_NAME"));
					lp.setCardNo(row.getString("CARD_NO"));
					lp.setRuangan(Ruangan);
					lp.setTipe(Tipe);
					all.add(lp);
				}
				row.close();
				stmt.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	public static MedicalResumeSignature GetSignatureReport(String mrid) {
		MedicalResumeSignature mrs = new MedicalResumeSignature();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet row = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		try {
			String query = "SELECT * FROM (SELECT VISIT_ID,SIGNATURE FROM MEDICALRESUMESIGNATURE "
					+ "WHERE MEDICAL_RESUME_ID='" + mrid
					+ "' AND DEFUNCT_IND='N' ORDER BY CREATED_AT DESC) WHERE ROWNUM=1";

			stmt = connection.createStatement();
			row = stmt.executeQuery(query);
			if (row.next()) {
				mrs.setVisitId(row.getString("VISIT_ID"));
				mrs.setSignature(row.getBytes("SIGNATURE"));
			}
			row.close();
			stmt.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return mrs;
	}

	public static MedicalResume GetMedicalResume(String MedicalResumeId, String Ruangan, String Tipe) {
		MedicalResume mr = new MedicalResume();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet row = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;
		try {
			if (Tipe.equals("1")) {
				String queryBaru = "SELECT " + "MR.MEDICAL_RESUME_ID, "
						+ "(SELECT CPH.NO_SIP FROM CAREPROVIDERLICENSEHISTORY CPH WHERE CPH.CAREPROVIDER_ID=CP.CAREPROVIDER_ID AND CPH.DEFUNCT_IND IS NULL AND CPH.EXPIRED_DATE>=SYSDATE  AND ROWNUM=1) SIP, "
						+ "MR.VISIT_ID, " + "MR.IN_DIAGNOSISMSTR_ID, " + "DMI.DIAGNOSIS_CODE IN_DIAGNOSISMSTR_CODE, "
						+ "DMI.DIAGNOSIS_DESC IN_DIAGNOSISMSTR_DESC, " + "MR.MAIN_DIAGNOSISMSTR_ID, "
						+ "DMM.DIAGNOSIS_CODE MAIN_DIAGNOSISMSTR_CODE, "
						+ "DMM.DIAGNOSIS_DESC MAIN_DIAGNOSISMSTR_DESC, " + "MR.SEC_DIAGNOSIS_DESC, "
						+ "MR.ANAMNESE_DESC, " + "MR.PHYSIC_CHECK_DESC, " + "MR.SUPPORT_CHECK_DESC, "
						+ "MR.THERAPY_DESC, " + "MR.LAST_CONDITION, " + "V.ADMISSION_DATETIME VISIT_DATETIME, "
						+ "MR.CONTROL_DATETIME, " + "MR.RESUME_DATETIME, " + "MR.DISCHARGED_DATETIME, "
						+ "MR.CAREPROVIDER_ID, " + "CP.CAREPROVIDER_CODE, " + "P.PERSON_NAME CAREPROVIDER_NAME, "
						+ "WM.WARD_DESC, " + "BM.BED_NO, " + "MR.IP_INDICATION_DESC, " + "MR.STATUS_KESADARAN, "
						+ "MR.THERAPY_DISCHARGED_DESC, " + "PS2.PERSON_NAME, " + "CD.CARD_NO, "
						+ "pgCOMMON.fxGetCodeDesc(PS2.SEX) SEX, " + "TO_CHAR(PS2.BIRTH_DATE,'DD-MM-YYYY') TGLLAHIR, "
						+ "(SELECT TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)||' Tahun ' || TRUNC(months_between(sysdate,PS.BIRTH_DATE)-12*trunc(months_between(sysdate,PS.BIRTH_DATE)/12)) || ' Bulan' FROM PERSON PS,PATIENT PA,VISIT VS WHERE V.PATIENT_ID = PA.PATIENT_ID AND PA.PERSON_ID=PS.PERSON_ID AND VS.VISIT_ID=V.VISIT_ID) Umur, "
						+ "MR.BLOOD_PLEASURE, " + "MR.PULSE, " + "MR.PULSE_TYPE, " + "MR.TEMPERATURE, "
						+ "MR.BREATHING, " + "MR.SPO2, " + "MR.URGENT_CONTROL, "
						+ "(SELECT DMO.DIAGNOSIS_DESC FROM DIAGNOSISMSTR DMO WHERE DMO.DIAGNOSISMSTR_ID=MR.DIAGNOSIS_O_MSTR_ID) DIAGNOSA_0, "
						+ "(SELECT DMO.DIAGNOSIS_DESC FROM DIAGNOSISMSTR DMO WHERE DMO.DIAGNOSISMSTR_ID=MR.DIAGNOSIS_IX_MSTR_ID) DIAG_IX, "
						+ "(SELECT LETTER_NO FROM CONTROLLETTER WHERE VISIT_ID = MR.VISIT_ID AND ROWNUM = 1) NO_SURAT "
						+ "FROM  " + "MEDICAL_RESUME MR, " + "VISIT V, " + "WARDMSTR WM, " + "BEDMSTR BM, "
						+ "DIAGNOSISMSTR DMI, " + "DIAGNOSISMSTR DMM, " + "CAREPROVIDER CP, " + "PERSON P, "
						+ "PERSON PS2, " + "CARD CD, " + "PATIENT PT " + "WHERE " + "MR.VISIT_ID = V.VISIT_ID AND "
						+ "V.WARDMSTR_ID = WM.WARDMSTR_ID AND " + "V.BEDMSTR_ID = BM.BEDMSTR_ID(+) AND "
						+ "MR.IN_DIAGNOSISMSTR_ID = DMI.DIAGNOSISMSTR_ID  AND "
						+ "MR.MAIN_DIAGNOSISMSTR_ID = DMM.DIAGNOSISMSTR_ID AND "
						+ "MR.CAREPROVIDER_ID = CP.CAREPROVIDER_ID AND " + "CP.PERSON_ID = P.PERSON_ID AND "
						+ "V.PATIENT_ID = PT.PATIENT_ID AND " + "PT.PERSON_ID = PS2.PERSON_ID AND "
						+ "CD.PERSON_ID = PS2.PERSON_ID AND " + "MR.MEDICAL_RESUME_ID = '" + MedicalResumeId + "'";

				stmt = connection.createStatement();
				row = stmt.executeQuery(queryBaru);

				if (row.next()) {
					IpMedicalResume ipMr = new IpMedicalResume();
					String noreg = row.getString("NO_SURAT");
					ipMr.setMedicalResumeId(row.getString("MEDICAL_RESUME_ID"));
					ipMr.setVisitId(row.getString("VISIT_ID"));
					ipMr.setInDiagnosismstrId(row.getString("IN_DIAGNOSISMSTR_ID"));
					ipMr.setInDiagnosisCode(row.getString("IN_DIAGNOSISMSTR_CODE"));
					ipMr.setInDiagnosisDesc(row.getString("IN_DIAGNOSISMSTR_DESC"));
					ipMr.setMainDiagnosisId(row.getString("MAIN_DIAGNOSISMSTR_ID"));
					ipMr.setMainDiagnosisCode(row.getString("MAIN_DIAGNOSISMSTR_CODE"));
					ipMr.setMainDiagnosisDesc(row.getString("MAIN_DIAGNOSISMSTR_DESC"));
					ipMr.setSecDiagnosisDesc(row.getString("SEC_DIAGNOSIS_DESC"));
					ipMr.setAnamneseDesc(row.getString("ANAMNESE_DESC"));
					ipMr.setPhysicCheckDesc(row.getString("PHYSIC_CHECK_DESC"));
					ipMr.setSupportCheckDesc(row.getString("SUPPORT_CHECK_DESC"));
					ipMr.setTherapyDesc(row.getString("THERAPY_DESC"));
					ipMr.setLastCondition(row.getString("LAST_CONDITION"));
					ipMr.setVisitDatetime(row.getString("VISIT_DATETIME"));
					ipMr.setControlDatetime(row.getString("CONTROL_DATETIME"));
					ipMr.setResumeDatetime(row.getString("RESUME_DATETIME"));
					ipMr.setDischargedDatetime(row.getString("DISCHARGED_DATETIME"));
					ipMr.setCareproviderId(row.getString("CAREPROVIDER_ID"));
					ipMr.setCareproviderCode(row.getString("CAREPROVIDER_CODE"));
					ipMr.setCareproviderName(row.getString("CAREPROVIDER_NAME"));
					ipMr.setWardDesc(row.getString("WARD_DESC"));
					ipMr.setBedNo(row.getString("BED_NO"));
					ipMr.setIpIndicationDesc(row.getString("IP_INDICATION_DESC"));
					ipMr.setStatusKesadaran(row.getString("STATUS_KESADARAN"));
					ipMr.setTherapyDischargedDesc(row.getString("THERAPY_DISCHARGED_DESC"));
					ipMr.setPersonName(row.getString("PERSON_NAME"));
					ipMr.setCardNo(row.getString("CARD_NO"));
					ipMr.setSex(row.getString("SEX"));
					ipMr.setTglLahir(row.getString("TGLLAHIR"));
					ipMr.setUmur(row.getString("Umur"));
					ipMr.setBloodPleasure(row.getString("BLOOD_PLEASURE"));
					ipMr.setPulse(row.getString("PULSE"));
					ipMr.setPulseType(row.getString("PULSE_TYPE"));
					ipMr.setTemperature(row.getString("TEMPERATURE"));
					ipMr.setBreathing(row.getString("BREATHING"));
					ipMr.setSpo2(row.getString("SPO2"));
					ipMr.setUrgentControl(row.getString("URGENT_CONTROL"));
					ipMr.setDiagnosa0(row.getString("DIAGNOSA_0"));
					ipMr.setDiagIX(row.getString("DIAG_IX"));
					ipMr.setNoSip(row.getString("SIP"));
					ipMr.setNoControl(row.getString("NO_SURAT"));
					mr.setIpMedicalResume(ipMr);
					mr.setBpjsMedicalResume(null);
					mr.setOpMedicalResume(null);
				}

				row.close();
				stmt.close();
				// }
				// else if(Ruangan.equals("BPJS POLYCLINIC") && Tipe.equals("2"))
				// {
			} else {
				String selectQuery = "select mr.medical_resume_id, v.patient_type,v.patient_class from medical_resume mr "
						+ " inner join visit v on v.visit_id = mr.visit_id " + " where mr.medical_resume_id = ? "
						+ " order by mr.medical_resume_id desc ";
				pstmtGetReport = connection.prepareStatement(selectQuery);
				pstmtGetReport.setString(1, MedicalResumeId);
				rsReport = pstmtGetReport.executeQuery();

				if (rsReport.next()) {
					String clas = rsReport.getString("patient_class");
					if (clas.equals("PTC114")) {
						String queryBaru = "SELECT " + "MR.MEDICAL_RESUME_ID, " + "MR.VISIT_ID, "
								+ "MR.MAIN_DIAGNOSISMSTR_ID, " + "DMM.DIAGNOSIS_CODE MAIN_DIAGNOSISMSTR_CODE, "
								+ "DMM.DIAGNOSIS_DESC MAIN_DIAGNOSISMSTR_DESC, " + "MR.SEC_DIAGNOSIS_DESC, "
								+ "MR.ANAMNESE_DESC, " + "MR.PHYSIC_CHECK_DESC, " + "MR.SUPPORT_CHECK_DESC, "
								+ "MR.THERAPY_DESC, " + "MR.LAST_CONDITION, " + "MR.CRITICAL_FINDING, "
								+ "MR.RECONTROL, " + "MR.PENGOBATAN_ULANG, " + "MR.FISIOTERAPI, " + "MR.HEMODIALISA, "
								+ "MR.KEMOTERAPI, " + "MR.RADIOTERAPI, " + "MR.PENGOBATAN_ULANG, "
								+ "V.ADMISSION_DATETIME VISIT_DATETIME, " + "MR.CONTROL_DATETIME, "
								+ "MR.CAREPROVIDER_ID, " + "CP.CAREPROVIDER_CODE, "
								+ "(select US.USER_NAME from USERMSTR US where US.USERMSTR_ID = MR.INPUT_BY) INPUT_BY, "
								+ "P.PERSON_NAME CAREPROVIDER_NAME, "
								+ "(case ps.sex when 'SEXM' then 'PRIA'else 'WANITA'end) sexl, " + "PS.BIRTH_DATE, "
								+ "ROUND(SYSDATE-to_date(to_char(ps.birth_date,'dd-mon-yyyy'),'dd-mon-yyyy')) AS Days, "
								+ "AR.ADDRESS_1, " + "PS.STAKEHOLDER_ID, " + "PS.PERSON_NAME, " + "C.CARD_NO,"
								+ "(SELECT TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)||' Tahun '  FROM PERSON PS,PATIENT PA,VISIT VS WHERE V.PATIENT_ID = PA.PATIENT_ID AND PA.PERSON_ID=PS.PERSON_ID AND VS.VISIT_ID=V.VISIT_ID) Umur, "
								+ "(SELECT CPL.NO_SIP FROM CAREPROVIDERLICENSEHISTORY CPL WHERE CPL.CAREPROVIDER_ID=MR.CAREPROVIDER_ID AND CPL.EXPIRED_DATE >= MR.CREATED_AT AND ROWNUM=1 ) NO_SIP, "
								+ "(SELECT DMO.DIAGNOSIS_DESC FROM DIAGNOSISMSTR DMO WHERE DMO.DIAGNOSISMSTR_ID=MR.DIAGNOSIS_O_MSTR_ID) DIAGNOSA_0, "
								+ "(SELECT DMO.DIAGNOSIS_DESC FROM DIAGNOSISMSTR DMO WHERE DMO.DIAGNOSISMSTR_ID=MR.DIAGNOSIS_IX_MSTR_ID) DIAG_IX, "
								+ "(SELECT COUNT(*) FROM VISIT WHERE PATIENT_ID=V.PATIENT_ID) TOTAL,  "
								+ "to_char(sysdate, 'YYYY','nls_date_language = INDONESIAN') TAHUN, "
								+ "to_char(sysdate, 'RM','nls_date_language = INDONESIAN') BULAN, "
								+ "(SELECT LETTER_NO FROM CONTROLLETTER WHERE VISIT_ID = MR.VISIT_ID AND ROWNUM = 1) NO_SURAT "
								+ "FROM  " + "MEDICAL_RESUME MR, " + "VISIT V, " + "DIAGNOSISMSTR DMM, "
								+ "CAREPROVIDER CP, " + "PERSON P, " + "PERSON PS, " + "PATIENT PT, " + "ADDRESS AR, "
								+ "CARD C " + "WHERE " + "MR.VISIT_ID = V.VISIT_ID AND "
								+ "MR.MAIN_DIAGNOSISMSTR_ID = DMM.DIAGNOSISMSTR_ID AND "
								+ "MR.CAREPROVIDER_ID = CP.CAREPROVIDER_ID AND " + "CP.PERSON_ID = P.PERSON_ID AND "
								+ "MR.MEDICAL_RESUME_ID = '" + MedicalResumeId + "' "
								+ "AND V.PATIENT_ID = PT.PATIENT_ID " + "AND PT.PERSON_ID = PS.PERSON_ID "
								+ "AND AR.STAKEHOLDER_ID= PS.STAKEHOLDER_ID " + "AND C.PERSON_ID = PS.PERSON_ID "
								+ "AND AR.ADDRESS_TYPE='ADRRES' " + "ORDER BY AR.ADDRESS_ID ";

						stmt = connection.createStatement();
						row = stmt.executeQuery(queryBaru);
						if (row.next()) {

							String noreg = row.getString("NO_SURAT");
							if (noreg == null) {
								noreg = row.getString("TOTAL") + "/" + row.getString("BULAN") + "/"
										+ row.getString("TAHUN");
							}
							BpjsMedicalResume bmr = new BpjsMedicalResume();
							bmr.setMedicalResumeId(row.getString("MEDICAL_RESUME_ID"));
							bmr.setVisitId(row.getString("VISIT_ID"));
							bmr.setMainDiagnosisId(row.getString("MAIN_DIAGNOSISMSTR_ID"));
							bmr.setMainDiagnosisDesc(row.getString("MAIN_DIAGNOSISMSTR_CODE"));
							bmr.setMainDiagnosisDesc(row.getString("MAIN_DIAGNOSISMSTR_DESC"));
							bmr.setSecDiagnosisDesc(row.getString("SEC_DIAGNOSIS_DESC"));
							bmr.setAnamneseDesc(row.getString("ANAMNESE_DESC"));
							bmr.setPhysicCheckDesc(row.getString("PHYSIC_CHECK_DESC"));
							bmr.setSupportCheckDesc(row.getString("SUPPORT_CHECK_DESC"));
							bmr.setTherapyDesc(row.getString("THERAPY_DESC"));
							bmr.setLastCondition(row.getString("LAST_CONDITION"));
							bmr.setCriticalFinding(row.getString("CRITICAL_FINDING"));
							bmr.setRecontrol(row.getString("RECONTROL"));
							bmr.setVisitDrug(row.getString("PENGOBATAN_ULANG"));
							bmr.setFisioterapi(row.getString("FISIOTERAPI"));
							bmr.setHemodialisa(row.getString("HEMODIALISA"));
							bmr.setKemoterapi(row.getString("KEMOTERAPI"));
							bmr.setRadioterapi(row.getString("RADIOTERAPI"));
							bmr.setPengobatanUlang(row.getString("PENGOBATAN_ULANG"));
							bmr.setVisitDatetime(row.getString("VISIT_DATETIME"));
							bmr.setControlDatetime(row.getString("CONTROL_DATETIME"));
							bmr.setCareproviderId(row.getString("CAREPROVIDER_ID"));
							bmr.setCareproviderCode(row.getString("CAREPROVIDER_CODE"));
							bmr.setInputBy(row.getString("INPUT_BY"));
							bmr.setCareproviderName(row.getString("CAREPROVIDER_NAME"));
							bmr.setSexl(row.getString("sexl"));
							bmr.setDays(row.getString("Days"));
							bmr.setAddress1(row.getString("ADDRESS_1"));
							bmr.setStakeholderId(row.getString("STAKEHOLDER_ID"));
							bmr.setPersonName(row.getString("PERSON_NAME"));
							bmr.setCardNo(row.getString("CARD_NO"));
							bmr.setUmur(row.getString("Umur"));
							bmr.setTglLahir(row.getString("BIRTH_DATE"));
							bmr.setNoSip(row.getString("NO_SIP"));
							bmr.setNoRegist(noreg);
							bmr.setDiagnosa0(row.getString("DIAGNOSA_0"));
							bmr.setDiagIX(row.getString("DIAG_IX"));
							mr.setBpjsMedicalResume(bmr);
							mr.setIpMedicalResume(null);
							mr.setOpMedicalResume(null);
						}
						row.close();
						stmt.close();

					} else {

						String queryBaru = "SELECT " + "MR.MEDICAL_RESUME_ID, " + "MR.VISIT_ID, " + "MR.RECONTROL, "
								+ "MR.VISIT_DRUG, " + "MR.FISIOTERAPI, " + "MR.HEMODIALISA, " + "MR.KEMOTERAPI, "
								+ "MR.RADIOTERAPI, " + "MR.PENGOBATAN_ULANG, " + "MR.MAIN_DIAGNOSISMSTR_ID, "
								+ "DMM.DIAGNOSIS_CODE MAIN_DIAGNOSISMSTR_CODE, "
								+ "DMM.DIAGNOSIS_DESC MAIN_DIAGNOSISMSTR_DESC, " + "MR.SEC_DIAGNOSIS_DESC, "
								+ "NVL(MR.ANAMNESE_DESC,MR.SEC_DIAGNOSIS_DESC) ANAMNESE_DESC, "
								+ "MR.PHYSIC_CHECK_DESC, " + "MR.SUPPORT_CHECK_DESC, " + "MR.THERAPY_DESC, "
								+ "MR.LAST_CONDITION, " + "MR.CRITICAL_FINDING, " + "MR.THERAPY_DISCHARGED_DESC, "
								+ "V.ADMISSION_DATETIME VISIT_DATETIME, " + "MR.CONTROL_DATETIME, "
								+ "MR.CAREPROVIDER_ID, " + "CP.CAREPROVIDER_CODE, "
								+ "(select US.USER_NAME from USERMSTR US where US.USERMSTR_ID = MR.INPUT_BY) INPUT_BY, "
								+ "P.PERSON_NAME CAREPROVIDER_NAME, "
								+ "(case ps.sex when 'SEXM' then 'PRIA'else 'WANITA'end) sexl, " + "PS.BIRTH_DATE, "
								+ "ROUND(SYSDATE-to_date(to_char(ps.birth_date,'dd-mon-yyyy'),'dd-mon-yyyy')) AS Days, "
								+ "AR.ADDRESS_1, PS.STAKEHOLDER_ID, " + "PS.PERSON_NAME, " + "C.CARD_NO, "
								+ "(SELECT TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)||' Tahun '  FROM PERSON PS,PATIENT PA,VISIT VS WHERE V.PATIENT_ID = PA.PATIENT_ID AND PA.PERSON_ID=PS.PERSON_ID AND VS.VISIT_ID=V.VISIT_ID) Umur, "
								+ "(SELECT DMO.DIAGNOSIS_DESC FROM DIAGNOSISMSTR DMO WHERE DMO.DIAGNOSISMSTR_ID=MR.DIAGNOSIS_O_MSTR_ID) DIAGNOSA_0, "
								+ "(SELECT DMO.DIAGNOSIS_DESC FROM DIAGNOSISMSTR DMO WHERE DMO.DIAGNOSISMSTR_ID=MR.DIAGNOSIS_IX_MSTR_ID) DIAG_IX, "
								+ "(SELECT LETTER_NO FROM CONTROLLETTER WHERE VISIT_ID = MR.VISIT_ID AND ROWNUM = 1) NO_SURAT, "
								+ "(CASE WHEN MR.CREATED_AT IS NOT NULL THEN (SELECT CPL.NO_SIP FROM CAREPROVIDERLICENSEHISTORY CPL WHERE CPL.CAREPROVIDER_ID=MR.CAREPROVIDER_ID AND CPL.EXPIRED_DATE >= MR.CREATED_AT AND ROWNUM=1) "
								+ "ELSE (SELECT CPL.NO_SIP FROM CAREPROVIDERLICENSEHISTORY CPL WHERE CPL.CAREPROVIDER_ID=MR.CAREPROVIDER_ID AND CPL.EXPIRED_DATE >= MR.LAST_UPDATED_DATETIME AND ROWNUM=1) END) NO_SIP "
								+ "FROM  " + "MEDICAL_RESUME MR, " + "VISIT V, " + "DIAGNOSISMSTR DMM, "
								+ "CAREPROVIDER CP, " + "PERSON P, " + "PERSON PS, " + "PATIENT PT, " + "ADDRESS AR, "
								+ "CARD C " + "WHERE " + "MR.VISIT_ID = V.VISIT_ID AND "
								+ "MR.MAIN_DIAGNOSISMSTR_ID = DMM.DIAGNOSISMSTR_ID AND "
								+ "MR.CAREPROVIDER_ID = CP.CAREPROVIDER_ID AND " + "CP.PERSON_ID = P.PERSON_ID AND "
								+ "MR.MEDICAL_RESUME_ID = '" + MedicalResumeId + "' "
								+ "AND V.PATIENT_ID = PT.PATIENT_ID " + "AND PT.PERSON_ID = PS.PERSON_ID "
								+ "AND AR.STAKEHOLDER_ID= PS.STAKEHOLDER_ID " + "AND ROWNUM = 1 "
								+ "AND C.PERSON_ID = PS.PERSON_ID " + "ORDER BY AR.ADDRESS_ID";

						stmt = connection.createStatement();
						row = stmt.executeQuery(queryBaru);
						if (row.next()) {
							OpMedicalResume omr = new OpMedicalResume();
							omr.setMedicalResumeId(row.getString("MEDICAL_RESUME_ID"));
							omr.setVisitId(row.getString("VISIT_ID"));
							omr.setRecontrol(row.getString("RECONTROL"));
							omr.setVisitDrug(row.getString("VISIT_DRUG"));
							omr.setFisioterapi(row.getString("FISIOTERAPI"));
							omr.setHemodialisa(row.getString("HEMODIALISA"));
							omr.setKemoterapi(row.getString("KEMOTERAPI"));
							omr.setRadioterapi(row.getString("RADIOTERAPI"));
							omr.setPengobatanUlang(row.getString("PENGOBATAN_ULANG"));
							omr.setMainDiagnosisId(row.getString("MAIN_DIAGNOSISMSTR_ID"));
							omr.setMainDiagnosisDesc(row.getString("MAIN_DIAGNOSISMSTR_CODE"));
							omr.setMainDiagnosisDesc(row.getString("MAIN_DIAGNOSISMSTR_DESC"));
							omr.setSecDiagnosisDesc(row.getString("SEC_DIAGNOSIS_DESC"));
							omr.setAnamneseDesc(row.getString("ANAMNESE_DESC"));
							omr.setPhysicCheckDesc(row.getString("PHYSIC_CHECK_DESC"));
							omr.setSupportCheckDesc(row.getString("SUPPORT_CHECK_DESC"));
							omr.setTherapyDesc(row.getString("THERAPY_DESC"));
							omr.setTherapyDischargedDesc(row.getString("THERAPY_DISCHARGED_DESC"));
							omr.setLastCondition(row.getString("LAST_CONDITION"));
							omr.setCriticalFinding(row.getString("CRITICAL_FINDING"));
							omr.setVisitDatetime(row.getString("VISIT_DATETIME"));
							omr.setControlDatetime(row.getString("CONTROL_DATETIME"));
							omr.setCareproviderId(row.getString("CAREPROVIDER_ID"));
							omr.setCareproviderCode(row.getString("CAREPROVIDER_CODE"));
							omr.setCareproviderName(row.getString("CAREPROVIDER_NAME"));
							omr.setInputBy(row.getString("INPUT_BY"));
							omr.setSexl(row.getString("sexl"));
							omr.setBirthDate(row.getString("BIRTH_DATE"));
							omr.setDays(row.getString("Days"));
							omr.setUmur(row.getString("Umur"));
							omr.setAddress1(row.getString("ADDRESS_1"));
							omr.setStakeholderId(row.getString("STAKEHOLDER_ID"));
							omr.setPersonName(row.getString("PERSON_NAME"));
							omr.setCardNo(row.getString("CARD_NO"));
							omr.setNoSip(row.getString("NO_SIP"));
							omr.setDiagnosa0(row.getString("DIAGNOSA_0"));
							omr.setDiagIX(row.getString("DIAG_IX"));
							omr.setNoControl(row.getString("NO_SURAT"));
							mr.setOpMedicalResume(omr);
							mr.setIpMedicalResume(null);
							mr.setBpjsMedicalResume(null);
						}
						row.close();
						stmt.close();
					}
				}
				rsReport.close();
				pstmtGetReport.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return mr;
	}

	public static MedicalResume getMedicalResumeByMrnNo(String mrnNo) {
		String medicalResumeId = "";
		String ruangan = "";
		String tipe = "";

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "select mr.medical_resume_id, ssm.subspecialty_desc, v.patient_type from card c "
					+ " inner join patient pat on pat.person_id = c.person_id "
					+ " inner join visit v on v.patient_id = pat.patient_id "
					+ " inner join medical_resume mr on mr.visit_id = v.visit_id "
					+ " inner join subspecialtymstr ssm on ssm.subspecialtymstr_id = v.subspecialtymstr_id "
					+ " where c.card_no = ? "
					// + " and rownum = 1 "
					+ " order by mr.medical_resume_id desc ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);

			pstmtGetReport.setString(1, mrnNo);

			rsReport = pstmtGetReport.executeQuery();

			if (rsReport.next()) {
				medicalResumeId = rsReport.getString("medical_resume_id");
				ruangan = rsReport.getString("subspecialty_desc");
				String subs = rsReport.getString("patient_type");
				if (subs.equals("PTY1")) {
					tipe = "1";
				} else if (subs.equals("PTY2")) {
					tipe = "2";
				}
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}

		if (tipe.equals(""))
			return null;
		return GetMedicalResume(medicalResumeId, ruangan, tipe);
	}

	public static MedicalResume GetMedicalResumeById(String resumeId) {
		String medicalResumeId = "";
		String ruangan = "";
		String tipe = "";

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "select mr.medical_resume_id, ssm.subspecialty_desc, v.patient_type,v.patient_class,v.subspecialtymstr_id from card c "
					+ " inner join patient pat on pat.person_id = c.person_id "
					+ " inner join visit v on v.patient_id = pat.patient_id "
					+ " inner join medical_resume mr on mr.visit_id = v.visit_id "
					+ " inner join subspecialtymstr ssm on ssm.subspecialtymstr_id = v.subspecialtymstr_id "
					+ " where mr.medical_resume_id = ? "
					// + " and rownum = 1 "
					+ " order by mr.medical_resume_id desc ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);

			pstmtGetReport.setString(1, resumeId);

			rsReport = pstmtGetReport.executeQuery();

			if (rsReport.next()) {
				medicalResumeId = rsReport.getString("medical_resume_id");
				ruangan = rsReport.getString("subspecialty_desc");
				String ruang = rsReport.getString("subspecialtymstr_id");
				String subs = rsReport.getString("patient_type");
				String clas = rsReport.getString("patient_class");
				if (subs.equals("PTY1")) {
					tipe = "1";
				} else if (subs.equals("PTY2")) {
					tipe = "2";
				}
				if (clas.equals("PTC114")) {
					ruangan = "BPJS POLYCLINIC";
				}
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}

		if (tipe.equals(""))
			return null;
		return GetMedicalResume(medicalResumeId, ruangan, tipe);
	}

	private static String getCareProviderIdByUserMstrId(String userMstrId) {
		String careProviderId = "";

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT CP.CAREPROVIDER_ID FROM CAREPROVIDER CP "
					+ " INNER JOIN PERSON PER ON PER.PERSON_ID = CP.PERSON_ID "
					+ " INNER JOIN USERPROFILE UP ON UP.PERSON_ID = PER.PERSON_ID "
					+ " INNER JOIN USERMSTR UM ON UM.USERMSTR_ID = UP.USERMSTR_ID " + " WHERE UM.USERMSTR_ID = ? ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);

			pstmtGetReport.setString(1, userMstrId);

			rsReport = pstmtGetReport.executeQuery();

			if (rsReport.next()) {
				careProviderId = rsReport.getString("CAREPROVIDER_ID");
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}

		if (careProviderId.equals(""))
			return null;
		return careProviderId;
	}

	public static List<VisitSignatureMedicalResume> getListMedicalVisitByMRN(String mrn) {
		if (mrn == null)
			return null;
		List<VisitSignatureMedicalResume> list = new ArrayList<VisitSignatureMedicalResume>();
		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String sql = "SELECT * FROM (SELECT C.CARD_NO,P.PERSON_ID,V.PATIENT_ID,V.VISIT_ID,V.PATIENT_TYPE,V.PATIENT_CLASS,V.ADMISSION_DATETIME, MR.MEDICAL_RESUME_ID,CP.CAREPROVIDER_ID,PS.PERSON_NAME "
					+ "FROM CARD C " + "INNER JOIN PATIENT P ON C.PERSON_ID = P.PERSON_ID "
					+ "INNER JOIN VISIT V ON V.PATIENT_ID = P.PATIENT_ID "
					+ "LEFT JOIN MEDICAL_RESUME MR ON MR.VISIT_ID = V.VISIT_ID "
					+ "LEFT JOIN CAREPROVIDER CP ON CP.CAREPROVIDER_ID = MR.CAREPROVIDER_ID "
					+ "LEFT JOIN PERSON PS ON PS.PERSON_ID = CP.PERSON_ID " + "WHERE C.CARD_NO = ? "
					+ "ORDER BY V.ADMISSION_DATETIME DESC) " + "WHERE ROWNUM <=10 ";

			pstmtGetReport = pooledConnection.prepareStatement(sql);

			pstmtGetReport.setString(1, mrn);

			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				String tipe = rsReport.getString("PATIENT_TYPE");
				String clas = rsReport.getString("PATIENT_CLASS");
				if (tipe.equals("PTY1")) {
					tipe = "IP";
				} else if (tipe.equals("PTY2")) {
					tipe = "OP";
				}
				if (clas.equals("PTC114")) {
					clas = "BPJS";
				} else {
					clas = "GENERAL";
				}
				VisitSignatureMedicalResume newView = new VisitSignatureMedicalResume();
				newView.setMedicalResumeId(rsReport.getBigDecimal("MEDICAL_RESUME_ID"));
				newView.setCareproviderId(rsReport.getString("CAREPROVIDER_ID"));
				newView.setPersonName(rsReport.getString("PERSON_NAME"));
				newView.setVisitId(rsReport.getBigDecimal("VISIT_ID"));
				newView.setCardNo(rsReport.getString("CARD_NO"));
				newView.setPatientId(rsReport.getString("PATIENT_ID"));
				newView.setPersonId(rsReport.getString("PERSON_ID"));
				newView.setAdmisionDate(rsReport.getString("ADMISSION_DATETIME"));
				newView.setPatientType(tipe);
				newView.setPatientClass(clas);

				list.add(newView);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static List<SignatureMedicalResume> getListPatientMedicalResumeByUserMstrId(String userMstrId) {
		String careProviderId = getCareProviderIdByUserMstrId(userMstrId);
		if (careProviderId == null)
			return null;
		List<SignatureMedicalResume> list = new ArrayList<SignatureMedicalResume>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "( " + " SELECT "
					+ "  VRT.VISITREGNTYPE_ID,    VRT.EFFICTIVE_DATETIME,   VRT.REGN_NO , VRT.SUBSPECIALTYMSTR_ID, "
					+ "  SSM.SUBSPECIALTY_DESC,   VRT.REGISTRATION_TYPE,    RM.RESOURCE_NAME,        SM.SESSION_DESC,"
					+ "  VRT.CAREPROVIDER_ID,   V.VISIT_ID, "
					+ "  PT.PATIENT_ID,           V.CONSULT_STATUS,          V.CONSULT_START_TIME,   P1.SEX, "
					+ "  PGCOMMON.fxGetCodeDesc(V.CONSULT_STATUS) CONSULT_STATUS_DESC,"
					+ "  DECODE(V.CONSULT_CAREPROVIDER_ID,NULL, "
					+ "   (SELECT P.PERSON_NAME FROM CAREPROVIDER CP,PERSON P WHERE CP.PERSON_ID = P.PERSON_ID AND CP.CAREPROVIDER_ID = VRT.CAREPROVIDER_ID),"
					+ "   (SELECT P.PERSON_NAME FROM CAREPROVIDER CP,PERSON P WHERE CP.PERSON_ID = P.PERSON_ID AND CP.CAREPROVIDER_ID = V.CONSULT_CAREPROVIDER_ID) "
					+ "  ) CAREPROVIDERNAME, "
					+ "  NVL(V.CONSULT_CAREPROVIDER_ID,  VRT.CAREPROVIDER_ID ) CONSULT_CAREPROVIDER_ID, "
					+ "  DECODE(V.CONSULT_CAREPROVIDER_ID,NULL, "
					+ "   (SELECT CareProvider_Code FROM CAREPROVIDER CP WHERE  CP.CAREPROVIDER_ID = VRT.CAREPROVIDER_ID), "
					+ "   (SELECT CareProvider_Code FROM CAREPROVIDER CP WHERE  CP.CAREPROVIDER_ID = V.CONSULT_CAREPROVIDER_ID) "
					+ "  ) CareProvider_Code,"
					+ "  V.VISIT_ID, V.PATIENT_CLASS, P1.PERSON_NAME, MR.MEDICAL_RESUME_ID, C.CARD_NO "
					+ " FROM VISITREGNTYPE VRT,VISIT V,SUBSPECIALTYMSTR SSM,RESOURCEMSTR RM,SESSIONMSTR SM, REGNVISITACTIVATION RVA, "
					+ " PATIENT PT,PERSON P1, MEDICAL_RESUME MR, CARD C " + " WHERE V.VISIT_ID = RVA.VISIT_ID "
					+ " AND MR.VISIT_ID = V.VISIT_ID " + " AND VRT.VISITREGNTYPE_ID =RVA.VISITREGNTYPE_ID "
					+ " AND V.ADMIT_STATUS <> 'AST5' " + " AND SSM.SUBSPECIALTYMSTR_ID(+) = VRT.SUBSPECIALTYMSTR_ID "
					+ " AND RM.RESOURCEMSTR_ID(+)  = VRT.RESOURCEMSTR_ID "
					+ " AND SM.SESSIONMSTR_ID(+)  = VRT.SESSIONMSTR_ID "
					// + " AND (VRT.SUBSPECIALTYMSTR_ID = #subspecialtyMstrId# OR
					// VRT.SUBSPECIALTYMSTR_ID IS NULL ) "
					+ " AND V.ADMISSION_DATETIME >=pgregn.fxGetOPOMRegnValidityDateTime('RTTNOR') "
					+ " AND (VRT.REGISTRATION_TYPE = 'RTTNOR' or VRT.REGISTRATION_TYPE = 'RTTSP' "
					+ "  OR VRT.REGISTRATION_TYPE IS NULL "
					+ "  OR INSTR(','||(SELECT P.VALUE FROM PARAMETER P WHERE P.PARAMETER_NAME ='UNLIMITED_REGN_TYPE_LIST_FOR_CONSULT')||',', ','||VRT.REGISTRATION_TYPE ||',') > 0 "
					+ " )" + " AND VRT.CAREPROVIDER_ID = ? " // (VRT.CAREPROVIDER_ID = #careProviderId# OR
																// VRT.CAREPROVIDER_ID IS NULL )"
					// + " AND ( V.CONSULT_STATUS IN ('CNT1')) "
					+ " AND PT.PATIENT_ID = V.PATIENT_ID " + " AND P1.PERSON_ID = PT.PERSON_ID "
					+ " AND C.PERSON_ID = P1.PERSON_ID " + " AND V.ADMISSION_DATETIME <TRUNC(SYSDATE) + 1 " + " )"
					+ " ORDER BY EFFICTIVE_DATETIME, CONSULT_START_TIME ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);

			pstmtGetReport.setString(1, careProviderId);

			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				SignatureMedicalResume newView = new SignatureMedicalResume();
				newView.setMedicalResumeId(rsReport.getBigDecimal("MEDICAL_RESUME_ID"));
				newView.setVisitId(rsReport.getBigDecimal("VISIT_ID"));
				newView.setPersonName(rsReport.getString("PERSON_NAME"));
				newView.setCardNo(rsReport.getString("CARD_NO"));
				newView.setRuangan(rsReport.getString("SUBSPECIALTY_DESC"));
				list.add(newView);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static RujukanBpjs GetDataKartu(String bpjsNo) {
		RujukanBpjs rb = new RujukanBpjs();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet row = null;
		if (connection == null)
			return null;
		Statement stmt = null;
		try {
			String query = "SELECT * FROM (SELECT ASAL_FKTP,TGL_RUJUKAN,NOMOR_RUJUKAN FROM BPJS_RUJUKAN WHERE BPJS_NO='"
					+ bpjsNo + "' ORDER BY CREATED_AT DESC) WHERE ROWNUM=1";

			stmt = connection.createStatement();
			row = stmt.executeQuery(query);

			if (row.next()) {
				rb.setAsalFktp(row.getString("ASAL_FKTP"));
				rb.setTglRujukan(row.getString("TGL_RUJUKAN"));
				rb.setNomorRujukan(row.getString("NOMOR_RUJUKAN"));
			}
			row.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return rb;
	}

	public static StatusLab GetLabReport(String mrn) {
		StatusLab sl = new StatusLab();
		Connection connection = DbConnection.getPooledConnection();
		if (connection == null)
			return null;
		ResultSet row = null;
		ResultSet row2 = null;
		ResultSet row3 = null;
		Statement stmt = null;
		Statement stmt2 = null;
		Statement stmt3 = null;
		String query1 = "SELECT * FROM (SELECT VT.VISIT_ID FROM CARD CD,PATIENT PT,VISIT VT WHERE CD.CARD_NO=" + mrn
				+ " AND CD.PERSON_ID = PT.PERSON_ID AND VT.PATIENT_ID = PT.PATIENT_ID ORDER BY VT.ADMISSION_DATETIME DESC) WHERE ROWNUM=1";
		try {
			stmt = connection.createStatement();
			row = stmt.executeQuery(query1);
			row.next();
			String vid = row.getString("VISIT_ID");

			String cek = "SELECT count(*) tot FROM ORDERENTRY ORE, ORDERENTRYITEM OEI WHERE ORE.VISIT_ID=" + vid
					+ " AND ORE.ORDERENTRY_ID = OEI.ORDERENTRY_ID AND ORE.ORDER_STATUS='OSTCON' AND "
					+ "OEI.ORDERITEMMSTR_ID IN (311122213,357603045)";
			stmt2 = connection.createStatement();
			row2 = stmt2.executeQuery(cek);
			row2.next();
			String tot = row2.getString("tot");
			if (tot.equals("0")) {
				sl.setOrderid("-");
				sl.setNadok("-");
				sl.setNood("-");
				sl.setCdt("-");
			} else {
				String query2 = "SELECT ORE.ORDERENTRY_ID,(select ps.PERSON_NAME from person ps inner join CAREPROVIDER cp on cp.PERSON_ID = ps.PERSON_ID where cp.CAREPROVIDER_ID = ORE.ORDERED_CAREPROVIDER_ID) NADOK,"
						+ "TO_CHAR(ORE.LAST_CONFIRMED_DATETIME,'yyyy-mm-dd') CDT,ORE.ORDER_ENTRY_NO FROM ORDERENTRY ORE, ORDERENTRYITEM OEI WHERE ORE.VISIT_ID="
						+ vid + " AND ORE.ORDERENTRY_ID = OEI.ORDERENTRY_ID AND ORE.ORDER_STATUS='OSTCON' AND "
						+ "OEI.ORDERITEMMSTR_ID IN (311122213,357603045)";
				stmt3 = connection.createStatement();
				row3 = stmt3.executeQuery(query2);
				row3.next();
				sl.setOrderid(row3.getString("ORDERENTRY_ID"));
				sl.setNadok(row3.getString("NADOK"));
				sl.setNood(row3.getString("ORDER_ENTRY_NO"));
				sl.setCdt(row3.getString("CDT"));
				stmt3.close();
				row3.close();
			}
			stmt2.close();
			row2.close();
			stmt.close();
			row.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
			if (row2 != null)
				try {
					row2.close();
				} catch (Exception ignore) {
				}
			if (stmt2 != null)
				try {
					stmt2.close();
				} catch (Exception ignore) {
				}
			if (row3 != null)
				try {
					row3.close();
				} catch (Exception ignore) {
				}
			if (stmt3 != null)
				try {
					stmt3.close();
				} catch (Exception ignore) {
				}
		}
		return sl;
	}

	public static List<PatientHiv> GetPatientHiv(String startdate, String enddate) {

		List<PatientHiv> data = new ArrayList<PatientHiv>();
		Connection connection = DbConnection.getPooledConnection();
		ListDietPasien view = new ListDietPasien();
		if (connection == null)
			return null;
		PreparedStatement stmt = null;
		ResultSet haspas = null;
		try {

			String query = "SELECT (select ps.PERSON_NAME from person ps inner join CAREPROVIDER cp on cp.PERSON_ID = ps.PERSON_ID where cp.CAREPROVIDER_ID = ORE.ORDERED_CAREPROVIDER_ID) NADOK, "
					+ "TO_CHAR(ORE.LAST_CONFIRMED_DATETIME,'yyyy-mm-dd') CDT,PS.PERSON_NAME,WS.WARD_DESC,SBS.SUBSPECIALTY_DESC, CD.CARD_NO "
					+ "FROM ORDERENTRY ORE  "
					+ "INNER JOIN ORDERENTRYITEM OEI ON OEI.ORDERENTRY_ID = ORE.ORDERENTRY_ID "
					+ "INNER JOIN VISIT VT ON VT.VISIT_ID = ORE.VISIT_ID "
					+ "INNER JOIN PATIENT PT ON PT.PATIENT_ID = VT.PATIENT_ID "
					+ "INNER JOIN PERSON PS ON PS.PERSON_ID = PT.PERSON_ID "
					+ "INNER JOIN SUBSPECIALTYMSTR SBS ON SBS.SUBSPECIALTYMSTR_ID = VT.SUBSPECIALTYMSTR_ID "
					+ "INNER JOIN CARD CD ON CD.PERSON_ID = PS.PERSON_ID "
					+ "LEFT OUTER JOIN WARDMSTR WS ON WS.WARDMSTR_ID = VT.WARDMSTR_ID "
					+ "WHERE ORE.ORDER_STATUS='OSTCON' AND OEI.ORDERITEMMSTR_ID IN (311122213,357603045,311122220,314864365,311122919) "
					+ "AND ORE.PLANNED_DATETIME >= TO_DATE(?,'YYYY-MM-DD') AND ORE.PLANNED_DATETIME < TO_DATE(?,'YYYY-MM-DD') "
					+ "ORDER BY ORE.LAST_CONFIRMED_DATETIME ASC ";

			stmt = connection.prepareStatement(query);
			stmt.setEscapeProcessing(true);
			stmt.setQueryTimeout(60000);
			stmt.setString(1, startdate);
			stmt.setString(2, enddate);
			haspas = stmt.executeQuery();

			while (haspas.next()) {
				PatientHiv dl = new PatientHiv();
				dl.setNadok(haspas.getString("NADOK"));
				dl.setCdt(haspas.getString("CDT"));
				dl.setPerson_name(haspas.getString("PERSON_NAME"));
				dl.setWard_desc(haspas.getString("WARD_DESC"));
				dl.setSubspeciality_desc(haspas.getString("SUBSPECIALTY_DESC"));
				dl.setCard_no(haspas.getString("CARD_NO"));
				data.add(dl);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return data;

	}

	public static List<ListResourceSchedule> getDoctorScheduleDay(String idhari, String subspecialty_id) {
		List<ListResourceSchedule> all = new ArrayList<ListResourceSchedule>();

		Connection connection = DbConnection.getPooledConnection();
		ResultSet haspas = null;
		if (connection == null)
			return null;
		PreparedStatement stmt = null;

		String cetak = "SUN_IND='Y'";
		if (idhari.equals("2")) {
			cetak = "MON_IND='Y'";
		} else if (idhari.equals("3")) {
			cetak = "TUE_IND='Y'";
		} else if (idhari.equals("4")) {
			cetak = "WED_IND='Y'";
		} else if (idhari.equals("5")) {
			cetak = "THU_IND='Y'";
		} else if (idhari.equals("6")) {
			cetak = "FRI_IND='Y'";
		} else if (idhari.equals("7")) {
			cetak = "SAT_IND='Y'";
		}

		String abdatabp = "SELECT rm.RESOURCEMSTR_ID, rm.RESOURCE_NAME,  " +
				"				rm.CAREPROVIDER_ID, sm.SESSION_DESC ,  " +
				"		     pgCOMMON.fxGetCodeDesc(rm.poli_code) POLI_NAME,  " +
				"		     rm.poli_code as subspecialty_code  , rm.sequence as seq_doctor, cm.code_seq as SEQ_POLI, " +
				"		     (SELECT SPECIALTY_CODE FROM DOCTORSPECIALTYMSTR WHERE SUBSPECIALTY_CODE = RM.POLI_CODE AND DEFUNCT_IND = 'N') POLI_CODE , " +
				"				 (case when SIP.CAREPROVIDER_ID is null then 'N' else 'Y' end) as LICENSED " +
				"		     FROM RESOURCEMSTR rm " +
				"		     inner join RESOURCEPROFILE rp on rm.RESOURCEMSTR_ID = rp.RESOURCEMSTR_ID " +
				"		     inner join SESSIONMSTR sm on sm.SESSIONMSTR_ID = rp.SESSIONMSTR_ID " +
				"			 left join " +
				"				 (select CAREPROVIDER_ID from ( " +
				"					select " +
				"					CPLH.CAREPROVIDER_ID, " +
				"					CPLH.EFFECTIVE_DATE, " +
				"					CPLH.EXPIRED_DATE, " +
				"					CPLH.NO_SIP, COUNT(NO_SIP) OVER (PARTITION BY NO_SIP) AS NO_SIP_COUNT, " +
				"					COUNT(CAREPROVIDER_ID) OVER (PARTITION BY CAREPROVIDER_ID) AS CAREPROVIDER_COUNT " +
				"					from CAREPROVIDERLICENSEHISTORY CPLH " +
				"					where " +
				"					(EFFECTIVE_DATE <= TRUNC(SYSDATE) and EXPIRED_DATE >= TRUNC(SYSDATE)) " +
				"					and (DEFUNCT_IND = 'N' or DEFUNCT_IND is null) " +
				"					order by CAREPROVIDER_ID, EFFECTIVE_DATE) " +
				"					where no_sip_count =1 " +
				"					GROUP BY CAREPROVIDER_ID) SIP on SIP.CAREPROVIDER_ID = rm.CAREPROVIDER_ID " +
				"		     left join CODEMSTR cm ON cm.code_cat||cm.code_abbr = rm.poli_code " +
				"		     WHERE " +
				"		         rm.SUBSPECIALTYMSTR_ID = ? " +
				"				 and rm.defunct_ind = 'N'  " +
				"				 and rp.DEFUNCT_IND = 'N' AND " + cetak;

		try {
			stmt = connection.prepareStatement(abdatabp);
			stmt.setEscapeProcessing(true);
			stmt.setQueryTimeout(10000);
			stmt.setBigDecimal(1, new BigDecimal(subspecialty_id));
			haspas = stmt.executeQuery();

			String null_seq_poly = "99";
			int null_seq_doctor = 99;
			while (haspas.next()) {
				ListResourceSchedule lp = new ListResourceSchedule();
				lp.setRESOURCEMSTR_ID(haspas.getString("RESOURCEMSTR_ID"));
				lp.setRESOURCE_NAME(haspas.getString("RESOURCE_NAME"));
				lp.setSESSION_DESC(haspas.getString("SESSION_DESC"));
				lp.setCAREPROVIDER_ID(haspas.getString("CAREPROVIDER_ID"));
				lp.setSubspecialty_code(haspas.getString("subspecialty_code"));
				lp.setPOLI_NAME(haspas.getString("POLI_NAME"));
				lp.setSeq_doctor(haspas.getString("seq_doctor"));
				lp.setSeq_poli(haspas.getString("SEQ_POLI"));
				lp.setPOLI_CODE(haspas.getString("POLI_CODE"));
				lp.setLicensed(haspas.getString("LICENSED"));

				if (lp.getPOLI_NAME() == null && lp.getPOLI_CODE() == null) {
					lp.setPOLI_NAME("Lainnya");
					lp.setPOLI_CODE("POLLNN");

					if (lp.getSeq_poli() == null)
						lp.setSeq_poli(null_seq_poly);
					if (lp.getSeq_doctor() == null) {
						lp.setSeq_doctor(String.valueOf(null_seq_doctor));
						null_seq_doctor--;
					}
				}

				all.add(lp);
			}

			haspas.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (haspas != null)
				try {
					haspas.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return all;
	}

	private static Timestamp parseDate(String date) throws ParseException {
		Timestamp result = null;
		if (TextUtils.isEmpty(date))
			return result;
		Boolean isFormat1 = date.contains("/") ? true : false;
		SimpleDateFormat dateFormat = isFormat1 ? new SimpleDateFormat("yyyy/MM/dd")
				: new SimpleDateFormat("yyyy-MM-dd");
		Date parsedDate = dateFormat.parse(date);
		result = new java.sql.Timestamp(parsedDate.getTime());
		return result;
	}

	private static ResponseStatus newResponseStatus() {
		ResponseStatus result = new ResponseStatus();
		result.setMessage("Inisialisasi data.");
		result.setSuccess(false);
		return result;
	}

	private static ResponseStatus fail(String message) {
		ResponseStatus result = new ResponseStatus();
		result.setMessage(message);
		result.setSuccess(false);
		return result;
	}

	public static ResponseStatus checkStopResource(BigDecimal resourceMstrId, String regDate) {
		ResponseStatus result = newResponseStatus();
		try {
			String sb = "SELECT" + "	CAST(" + "	CASE" + "			" + "			WHEN EXISTS ("
					+ "			SELECT" + "				1 " + "			FROM" + "				RESOURCESTOP "
					+ "			WHERE" + "				RESOURCEMSTR_ID = ? "
					+ "				AND TRUNC( RESOURCESTOP_DATE ) = to_date( ?, 'YYYY-MM-DD' ) "
					+ "				AND RESOURCESTOP_IND = 'Y' " + "				AND DEFUNCT_IND = 'N' "
					+ "				AND ROWNUM = 1 " + "				) THEN" + "				'Y' ELSE 'N' "
					+ "			END AS VARCHAR2 ( 1 )) AS STOP " + "FROM" + "	DUAL";

			Timestamp dateInput = parseDate(regDate);
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateConvert = new Date(dateInput.getTime());
			String convertedDate = sf.format(dateConvert);

			Object stop = DbConnection.executeScalar(DbConnection.KTHIS, sb,
					new Object[] { resourceMstrId, convertedDate });
			if (stop == null)
				return fail("Stop null. (Webservice error)");
			String ind = stop instanceof String ? (String) stop : null;
			if (ind == null)
				return fail("Ind null. (Webservice error)");
			Boolean isStopResource = ind.equals(IConstant.IND_YES) ? true : false;
			result.setSuccess(true);
			result.setData(isStopResource);
			result.setMessage("Berhasil cek stattus stop.");

		} catch (SQLException e) {
			String message = e.getMessage();
			System.err.println(e.getMessage());
			result = fail(message);
		} catch (Exception e) {
			String message = e.getMessage();
			System.err.println(message);
			result = fail(message);
		} finally {

		}
		return result;
	}
}