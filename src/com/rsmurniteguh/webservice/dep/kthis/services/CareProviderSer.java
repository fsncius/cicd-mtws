package com.rsmurniteguh.webservice.dep.kthis.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.CareProvider;
import com.rsmurniteguh.webservice.dep.all.model.ListCekDispas;
import com.rsmurniteguh.webservice.dep.all.model.ListDoctorUser;
import com.rsmurniteguh.webservice.dep.all.model.ListQueNum;
import com.rsmurniteguh.webservice.dep.all.model.ListSpecialQueNum;
import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.ISysConstant;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class CareProviderSer extends DbConnection {

	public static List<CareProvider> getCareProvider(String caretype) {
		List<CareProvider> all=new ArrayList<CareProvider>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hascare = null; 
		if(connection == null)return null;
		Statement carep=null;
		String abcarpro="select t.careprovider_id, p.person_name, up.usermstr_id from"
				+ " careprovider t, person p, userprofile up where t.person_id=p.person_id "
				+ " and up.person_id = p.person_id and t.careprovider_type = '"+caretype+"' "
						+ " and t.defunct_ind='N' ";
		try {
			carep=connection.createStatement();
			hascare=carep.executeQuery(abcarpro);
			while(hascare.next())
				{
					CareProvider cp=new CareProvider();
					cp.setCareProviderId(hascare.getBigDecimal("careprovider_id"));
					cp.setPersonName(hascare.getString("person_name"));
					cp.setUsermstrId(hascare.getBigDecimal("usermstr_id"));
					all.add(cp);
				}
			hascare.close();
			carep.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (hascare!=null) try  { hascare.close(); } catch (Exception ignore){}
		     if (carep!=null) try  { carep.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		
		return all;
	}

	public static List<ListDoctorUser> getdoctoruser() {
		List<ListDoctorUser> all=new ArrayList<ListDoctorUser>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hascare=null;
		if(connection == null)return null;
		Statement carep=null;
		String abcarpro="select t.careprovider_id, p.person_name, up.usermstr_id "
				+ "from careprovider t, person p, userprofile up "
				+ "where t.person_id=p.person_id and up.person_id = p.person_id and t.careprovider_type = 'CPR1' and t.defunct_ind='N' "
				+ "AND up.LAST_UPDATED_DATETIME >= to_date('01-01-2015 00:00:01','dd/mm/yyyy HH24:MI:SS')   and up.USERMSTR_ID !='311401397' and up.USERMSTR_ID !='376394604' "
				+ "ORDER BY p.PERSON_NAME asc";
		try {
			carep=connection.createStatement();
			hascare=carep.executeQuery(abcarpro);
			while(hascare.next())
				{
					ListDoctorUser cp=new ListDoctorUser();
					cp.setCareprovider_id(hascare.getBigDecimal("careprovider_id"));
					cp.setPerson_name(hascare.getString("person_name"));
					cp.setUsermstr_id(hascare.getBigDecimal("usermstr_id"));
					all.add(cp);
				}
			hascare.close();
			carep.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (hascare!=null) try  { hascare.close(); } catch (Exception ignore){}
		     if (carep!=null) try  { carep.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<ListCekDispas> getcekdispas(String visitid) {
		// TODO Auto-generated method stub
		List<ListCekDispas> all=new ArrayList<ListCekDispas>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hascare=null;
		if(connection == null)return null;
		Statement carep=null;
		String abcarpro="SELECT COUNT(PATIENT_ID)AS TOTDATA FROM VISIT WHERE VISIT_ID='"+visitid+"' AND ADMIT_STATUS='AST4'";
		try {
			carep=connection.createStatement();
			hascare=carep.executeQuery(abcarpro);
			while(hascare.next())
				{
					ListCekDispas cp=new ListCekDispas();
					cp.setTOTDATA(hascare.getString("TOTDATA"));					
					all.add(cp);
				}
			hascare.close();
			carep.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (hascare!=null) try  { hascare.close(); } catch (Exception ignore){}
		     if (carep!=null) try  { carep.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	public static List<ListQueNum> getDoctorQueueList(String careproviderId, String subspecialtymstr) {
		List<ListQueNum> all=new ArrayList<ListQueNum>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet rs=null;
		if(connection == null)return null;
		Statement carep=null;
		String abcarpro="SELECT VRT.CAREPROVIDER_ID, V.QUEUE_NO, V.VISIT_ID "
				+ " FROM VISITREGNTYPE VRT,VISIT V,SUBSPECIALTYMSTR SSM,RESOURCEMSTR RM,SESSIONMSTR SM, REGNVISITACTIVATION RVA, "
				+ " PATIENT PT,PERSON P1 WHERE V.VISIT_ID = RVA.VISIT_ID "
				+ " AND VRT.VISITREGNTYPE_ID =RVA.VISITREGNTYPE_ID AND V.ADMIT_STATUS <> 'AST5' "
				+ " AND SSM.SUBSPECIALTYMSTR_ID(+) = VRT.SUBSPECIALTYMSTR_ID "
				+ " AND RM.RESOURCEMSTR_ID(+)  = VRT.RESOURCEMSTR_ID "
				+ " AND SM.SESSIONMSTR_ID(+)  = VRT.SESSIONMSTR_ID "
				+ " and (VRT.SUBSPECIALTYMSTR_ID = "+subspecialtymstr+" OR VRT.SUBSPECIALTYMSTR_ID IS NULL ) "
				+ " AND V.ADMISSION_DATETIME >=pgregn.fxGetOPOMRegnValidityDateTime(VRT.REGISTRATION_TYPE) "
				+ " and ( V.CONSULT_CAREPROVIDER_ID  in ("+careproviderId+") OR ( V.CONSULT_CAREPROVIDER_ID IS NULL AND   "
				+ " (VRT.CAREPROVIDER_ID in ("+careproviderId+") OR VRT.CAREPROVIDER_ID IS NULL))) "
				+ " and VRT.CREATED_DATETIME >= TRUNC(SYSDATE) "
				+ " and	  VRT.CREATED_DATETIME <= TRUNC(SYSDATE) + 1 "
				+ " and V.CONSULT_STATUS IN ('CNT1') "
				+ " AND PT.PATIENT_ID = V.PATIENT_ID AND P1.PERSON_ID = PT.PERSON_ID AND V.ADMISSION_DATETIME < TRUNC(SYSDATE) + 1 "
				+ " ORDER BY EFFICTIVE_DATETIME, CONSULT_START_TIME";
		try {
			carep=connection.createStatement();
			rs=carep.executeQuery(abcarpro);
			while(rs.next())
				{
					ListQueNum cp=new ListQueNum();
					cp.setCareprovider_id(rs.getString("careprovider_id"));
					cp.setVisit_id(rs.getString("visit_id"));
					cp.setQUEUE_NO(rs.getString("QUEUE_NO"));
					all.add(cp);
				}
			rs.close();
			carep.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (carep!=null) try  { carep.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	public static List<ListQueNum> getquenum(String queueno) {
		Connection connection = null;
//		Connection connection_queue = null;
		
		PreparedStatement stmt2 = null;
		Statement stmt = null;
		
		ResultSet rs = null;
		ResultSet rs2 = null;
		   
		List<ListQueNum> all=new ArrayList<ListQueNum>();
		connection=DbConnection.getPooledConnection();
		
//		connection_queue = DbConnection.getQueueInstance();
		
		if(connection == null 
//				|| connection_queue == null
				)return null;
		if(queueno == null)
		{
			
			String abcarpro = "select * from(select v.visit_id, v.QUEUE_NO, dd.PREPARE_BATCH_NO, dd.prepared_datetime, pe.person_name, ca.card_no, " +
					"					dd.DISPENSE_BATCH_NO, dd.dispensed_datetime, dd.RECEIVE_BY ,b.bill_status, ROW_NUMBER() OVER (PARTITION BY dd.VISIT_ID ORDER BY dd.PREPARE_BATCH_NO ASC) AS rn " +
					"					from DRUGDISPENSE  dd inner join visit v on v.VISIT_ID = dd.VISIT_ID inner JOIN PATIENTACCOUNT pa on pa.VISIT_ID = v.VISIT_ID " +
					"					inner join PATIENT pt on pt.patient_id = v.patient_id inner join PERSON pe on pe.person_id = pt.person_id inner join CARD ca on ca.person_id = pe.person_id " +
					"					left outer join bill b on b.PATIENTACCOUNT_ID = pa.PATIENTACCOUNT_ID " +
					"					where PREPARED_DATETIME >= trunc(sysdate) and v.PATIENT_TYPE = 'PTY2' " +
					"					and v.PATIENT_CLASS <> 'PTC114' " +
					"				 order by to_number(REPLACE(v.QUEUE_NO, '-', ''), '999999') " +
					"				) " +
					"				where rn = 1 ";

			
			//String sql ="INSERT INTO pharmacypolyqueue (queue_no ,prepare_batch_no, enterqueue_datetime ,prepare_datetime ,startserve_datetime ,endserve_datetime ,endserve_ind, "
			//		+ "created_at ,created_by ,updated_at,updated_by ,deleted) "
			//		+ "VALUES (?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, null, null, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP,null, 0)";
			
			
			
			try {
				stmt=connection.createStatement();
				rs=stmt.executeQuery(abcarpro);
				while(rs.next())
					{
						ListQueNum cp=new ListQueNum();
						cp.setVisit_id(rs.getString("visit_id"));
						cp.setQUEUE_NO(rs.getString("QUEUE_NO"));
						cp.setPREPARE_BATCH_NO(rs.getString("PREPARE_BATCH_NO"));
						cp.setPrepared_datetime(rs.getString("prepared_datetime"));
						cp.setPerson_name(rs.getString("person_name"));
						cp.setCard_no(rs.getString("card_no"));
						cp.setDISPENSE_BATCH_NO(rs.getString("DISPENSE_BATCH_NO"));
						cp.setDispensed_datetime(rs.getString("dispensed_datetime"));
						cp.setReceive_bY(rs.getString("RECEIVE_BY"));
						cp.setBill_status(rs.getString("bill_status"));
						cp.setRn(rs.getString("rn"));
						all.add(cp);
					}
				
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
//			     if (connection_queue!=null) try { connection_queue.close();} catch (Exception ignore){}
			   }
			addToPolyQueue((ArrayList<ListQueNum>) all);
		}
		else
		{
			String abcarpro="select * from(select v.visit_id, v.QUEUE_NO, dd.PREPARE_BATCH_NO, dd.prepared_datetime, pe.person_name, ca.card_no,"
					+ "dd.DISPENSE_BATCH_NO, dd.dispensed_datetime, dd.RECEIVE_BY ,b.bill_status, ROW_NUMBER() OVER (PARTITION BY dd.VISIT_ID ORDER BY dd.PREPARE_BATCH_NO ASC) AS rn "
					+ "from DRUGDISPENSE  dd inner join visit v on v.VISIT_ID = dd.VISIT_ID inner JOIN PATIENTACCOUNT pa on pa.VISIT_ID = v.VISIT_ID "
					+ "inner join PATIENT pt on pt.patient_id = v.patient_id inner join PERSON pe on pe.person_id = pt.person_id inner join CARD ca on ca.person_id = pe.person_id "
					+ "left outer join bill b on b.PATIENTACCOUNT_ID = pa.PATIENTACCOUNT_ID " // and b.BILL_STATUS = 'BISP' "
					+ "where PREPARED_DATETIME >= trunc(sysdate) and v.PATIENT_TYPE = 'PTY2' "
					+ "and v.PATIENT_CLASS <> 'PTC114'  order by to_number(v.QUEUE_NO, '9999')) where rn = 1 and queue_no in ("+queueno+")";
			try {
				stmt=connection.createStatement();
				rs=stmt.executeQuery(abcarpro);
				while(rs.next())
					{
						ListQueNum cp=new ListQueNum();
						cp.setVisit_id(rs.getString("visit_id"));
						cp.setQUEUE_NO(rs.getString("QUEUE_NO"));
						cp.setPREPARE_BATCH_NO(rs.getString("PREPARE_BATCH_NO"));
						cp.setPrepared_datetime(rs.getString("prepared_datetime"));
						cp.setPerson_name(rs.getString("person_name"));
						cp.setCard_no(rs.getString("card_no"));
						cp.setDISPENSE_BATCH_NO(rs.getString("DISPENSE_BATCH_NO"));
						cp.setDispensed_datetime(rs.getString("dispensed_datetime"));
						cp.setReceive_bY(rs.getString("RECEIVE_BY"));
						cp.setBill_status(rs.getString("bill_status"));
						cp.setRn(rs.getString("rn"));
						all.add(cp);
					}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
//			     if (connection_queue!=null) try { connection_queue.close();} catch (Exception ignore){}
			   }
		}
		return all;
	}
	
	private static void addToPolyQueue(ArrayList<ListQueNum> list){
		Connection connection = null;
		PreparedStatement pstmtCheckSql = null;
		PreparedStatement pstmtInsertSql = null;
		ResultSet rsReport = null;
		
		try {
			connection = DbConnection.getQueueInstance();
			if (connection == null ) throw new Exception("Database failed to connect !");
			
			for(ListQueNum view : list){
				String selectQuery = "select count (*) as rn from pharmacypolyqueue where prepare_batch_no = ?";
				pstmtCheckSql = connection.prepareStatement(selectQuery);
				pstmtCheckSql.setString(1, view.getPREPARE_BATCH_NO());
				
				rsReport = pstmtCheckSql.executeQuery();
						
				if (rsReport.next()){
					if(rsReport.getBigDecimal("rn").compareTo(BigDecimal.ZERO) == 0){
					//if(rsReport.getString(view.getPREPARE_BATCH_NO()).equals("")){
						if(view.getQUEUE_NO() != null)
						{
							String insertQuery = "INSERT INTO pharmacypolyqueue (queue_no ,person_name, card_no, prepare_batch_no, enterqueue_datetime ,prepare_datetime ,startserve_datetime ,endserve_datetime ,endserve_ind, "
									+ "created_at ,created_by ,updated_at,updated_by ,deleted) "
									+ "VALUES (?, ?, ? , ? ,CURRENT_TIMESTAMP, null, null, null, 0, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP,null, 0)";
							pstmtInsertSql = connection.prepareStatement(insertQuery);
							pstmtInsertSql.setString(1, view.getQUEUE_NO());
							pstmtInsertSql.setString(2, view.getPerson_name());
							pstmtInsertSql.setString(3, view.getCard_no());
							pstmtInsertSql.setString(4, view.getPREPARE_BATCH_NO());
							pstmtInsertSql.executeQuery();
						}
					}
				}
				rsReport.close();
				pstmtCheckSql.close();
			}		
			connection.close();
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
		    if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
	 		if (connection != null) try { connection.close(); } catch (SQLException e) {}
			if (pstmtCheckSql != null) try { pstmtCheckSql.close(); } catch (SQLException e) {}
			if (pstmtInsertSql != null) try { pstmtInsertSql.close(); } catch (SQLException e) {}
			if (rsReport != null) try { rsReport.close(); } catch (SQLException e) {}
		}		
	}
	
	public static String getCareprovidersByUsermstr(String users) {
		String all="";
		String abcarpro="select cp.CAREPROVIDER_ID from USERMSTR um "
				+ " inner join USERPROFILE up on up.USERMSTR_ID = um.USERMSTR_ID "
				+ " inner join CAREPROVIDER cp on cp.PERSON_ID = up.PERSON_ID "
				+ " where um.USERMSTR_ID in("+users+")";
		try {
			List list = DbConnection.executeReader(DbConnection.KTHIS, abcarpro, new Object[] {});
			for (int a=0;a<list.size();a++){
				HashMap row = (HashMap)list.get(a);
				all += row.get("CAREPROVIDER_ID").toString() +", ";	
			}
			all +="0 ";
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
		finally {
		   }
		return all;
	}
	
	public static Long getCareProviderIdByResourceMstrId(Long resourceMstrId) {
		Long result = null;
		try {
			String query = "SELECT CAREPROVIDER_ID FROM RESOURCEMSTR WHERE RESOURCEMSTR_ID = ?";
			Object[] param = new Object[] {resourceMstrId};
			Object rawData = DbConnection.executeScalar(DbConnection.KTHIS, query, param);
			if(rawData != null) result = Long.valueOf(((BigDecimal)rawData).toString());
		}catch(Exception ex) {
			ex.fillInStackTrace();
			System.out.println(ex.getMessage());
		}
		return result;
	}

	public static List<ListQueNum> getDoctorQueueListByUsermstrId(String usermstrId, String subspecialtymstr_id) {
		String careproviders = getCareprovidersByUsermstr(usermstrId);
		List<ListQueNum> all=new ArrayList<ListQueNum>();
		String abcarpro="SELECT VRT.CAREPROVIDER_ID, V.QUEUE_NO, V.VISIT_ID, UM.USERMSTR_ID, P1.PERSON_NAME  "
				+ " FROM VISITREGNTYPE VRT,VISIT V,SUBSPECIALTYMSTR SSM,RESOURCEMSTR RM,"
				+ " SESSIONMSTR SM, REGNVISITACTIVATION RVA, "
				+ " PATIENT PT,PERSON P1, USERMSTR UM, USERPROFILE UP, CAREPROVIDER cp "
				+ " WHERE V.VISIT_ID = RVA.VISIT_ID "
				+ " AND VRT.VISITREGNTYPE_ID =RVA.VISITREGNTYPE_ID and UM.USERMSTR_ID = UP.USERMSTR_ID "
				+ " and UP.PERSON_ID = cp.PERSON_ID and VRT.CAREPROVIDER_ID = cp.CAREPROVIDER_ID"
				+ " AND V.ADMIT_STATUS <> 'AST5' "
				+ " AND SSM.SUBSPECIALTYMSTR_ID(+) = VRT.SUBSPECIALTYMSTR_ID "
				+ " AND RM.RESOURCEMSTR_ID(+)  = VRT.RESOURCEMSTR_ID "
				+ " AND SM.SESSIONMSTR_ID(+)  = VRT.SESSIONMSTR_ID "
				+ " and (VRT.SUBSPECIALTYMSTR_ID = "+subspecialtymstr_id+" OR VRT.SUBSPECIALTYMSTR_ID IS NULL ) "
				+ " AND V.ADMISSION_DATETIME >=pgregn.fxGetOPOMRegnValidityDateTime(VRT.REGISTRATION_TYPE) "
				+ " and ( V.CONSULT_CAREPROVIDER_ID  in ("+careproviders+") OR ( V.CONSULT_CAREPROVIDER_ID IS NULL AND   "
				+ " (VRT.CAREPROVIDER_ID in ("+careproviders+") OR VRT.CAREPROVIDER_ID IS NULL))) "
				+ " and VRT.CREATED_DATETIME >= TRUNC(SYSDATE) "
				+ " and	  VRT.CREATED_DATETIME <= TRUNC(SYSDATE) + 1 "
				+ " and V.CONSULT_STATUS IN ('CNT1') "
				+ " AND PT.PATIENT_ID = V.PATIENT_ID AND P1.PERSON_ID = PT.PERSON_ID AND V.ADMISSION_DATETIME < TRUNC(SYSDATE) + 1 "
				+ " ORDER BY EFFICTIVE_DATETIME, CONSULT_START_TIME";
		try {
			List list = DbConnection.executeReader(DbConnection.KTHIS, abcarpro, new Object[] {});
			for (int a=0;a<list.size();a++){
				HashMap row = (HashMap)list.get(a);
				ListQueNum cp=new ListQueNum();
				cp.setCareprovider_id(row.get("careprovider_id").toString());
				cp.setVisit_id(row.get("visit_id").toString());
				cp.setQUEUE_NO(row.get("QUEUE_NO").toString());
				cp.setUsermstr_id(row.get("USERMSTR_ID").toString());
				cp.setPerson_name(row.get("PERSON_NAME").toString());
				all.add(cp);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
		finally {
		   }
		return all;
	}
	
	public static List<ListSpecialQueNum> getspecialquenum(String queueno) {
		Connection connection = null;
		
		PreparedStatement stmt2 = null;
		Statement stmt = null;
		
		ResultSet rs = null;
		   
		List<ListSpecialQueNum> all=new ArrayList<ListSpecialQueNum>();
		connection=DbConnection.getPooledConnection();
		
		
		if(connection == null)return null;
		if(queueno == null)
		{
			
			String abcarpro="select v.visit_id, v.patient_id, v.subspecialtymstr_id, v.queue_no, v.admission_datetime from visit v where to_char(v.admission_datetime, 'ddmmyyyy') = to_char(sysdate, 'ddmmyyyy')";
			try {
				stmt=connection.createStatement();
				rs=stmt.executeQuery(abcarpro);
				while(rs.next())
					{
						ListSpecialQueNum cp=new ListSpecialQueNum();
						cp.setVisit_id(rs.getString("visit_id"));
						cp.setQueue_no(rs.getString("queue_no"));
						cp.setPatient_id(rs.getString("patient_id"));
						cp.setSubspecialytymstr_id(rs.getString("subspecialtymstr_id"));
						cp.setAdmission_datetime(rs.getString("admission_datetime"));
						all.add(cp);
					}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		}
		else
		{
			String abcarpro="select v.visit_id, v.patient_id, v.subspecialtymstr_id, v.queue_no, v.admission_datetime from visit v where to_char(v.admission_datetime, 'ddmmyyyy') = to_char(sysdate, 'ddmmyyyy') and v.queue_no = ('"+queueno+"') and v.patient_class != 'PTC114'";
			try {
				stmt=connection.createStatement();
				rs=stmt.executeQuery(abcarpro);
				while(rs.next())
					{
						ListSpecialQueNum cp=new ListSpecialQueNum();
						cp.setVisit_id(rs.getString("visit_id"));
						cp.setQueue_no(rs.getString("queue_no"));
						cp.setPatient_id(rs.getString("patient_id"));
						cp.setSubspecialytymstr_id(rs.getString("subspecialtymstr_id"));
						cp.setAdmission_datetime(rs.getString("admission_datetime"));
						all.add(cp);
					}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		}
		return all;
	}
	
	public static List<ListSpecialQueNum> getspecialquenum2(String queueno, String subspeciality) {
		Connection connection = null;
		
		PreparedStatement stmt2 = null;
		Statement stmt = null;
		
		ResultSet rs = null;
		   
		List<ListSpecialQueNum> all=new ArrayList<ListSpecialQueNum>();
		connection=DbConnection.getPooledConnection();
		
		
		if(connection == null)return null;
		if(queueno == null)
		{
			
			String abcarpro="select v.visit_id, v.patient_id, v.subspecialtymstr_id, v.queue_no, v.admission_datetime from visit v where to_char(v.admission_datetime, 'ddmmyyyy') = to_char(sysdate, 'ddmmyyyy')";
			try {
				stmt=connection.createStatement();
				rs=stmt.executeQuery(abcarpro);
				while(rs.next())
					{
						ListSpecialQueNum cp=new ListSpecialQueNum();
						cp.setVisit_id(rs.getString("visit_id"));
						cp.setQueue_no(rs.getString("queue_no"));
						cp.setPatient_id(rs.getString("patient_id"));
						cp.setSubspecialytymstr_id(rs.getString("subspecialtymstr_id"));
						cp.setAdmission_datetime(rs.getString("admission_datetime"));
						all.add(cp);
					}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		}
		else
		{
			String abcarpro="select v.visit_id, v.patient_id, v.subspecialtymstr_id, v.queue_no, v.admission_datetime from visit v where to_char(v.admission_datetime, 'ddmmyyyy') = to_char(sysdate, 'ddmmyyyy') ";
					if (subspeciality != null && !subspeciality.equals("")) abcarpro += "and v.subspecialtymstr_id = ('"+subspeciality+"') ";
					abcarpro += "and v.queue_no = ('"+queueno+"') and v.patient_class != 'PTC114'";
			try {
				stmt=connection.createStatement();
				rs=stmt.executeQuery(abcarpro);
				while(rs.next())
					{
						ListSpecialQueNum cp=new ListSpecialQueNum();
						cp.setVisit_id(rs.getString("visit_id"));
						cp.setQueue_no(rs.getString("queue_no"));
						cp.setPatient_id(rs.getString("patient_id"));
						cp.setSubspecialytymstr_id(rs.getString("subspecialtymstr_id"));
						cp.setAdmission_datetime(rs.getString("admission_datetime"));
						all.add(cp);
					}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		}
		return all;
	}
	
	public static BaseResult getIsDoctorByUserMstrId(String userMstrId){
		BaseResult result = new BaseResult();
		result.setResult(IConstant.IND_NO);
		result.setStatus(IConstant.STATUS_CONNECTED);
		
		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;
		
		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null ) throw new Exception("Database failed to connect !");
			
			String selectQuery = "SELECT CP.CAREPROVIDER_TYPE FROM CAREPROVIDER CP "
					+ " INNER JOIN USERPROFILE UP ON UP.PERSON_ID = CP.PERSON_ID "
					+ " INNER JOIN USERMSTR UM ON UM.USERMSTR_ID = UP.USERMSTR_ID "
					+ " WHERE UM.USERMSTR_ID = ? "
					+ " AND CP.DEFUNCT_IND = 'N' ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			
			pstmtGetReport.setString(1, userMstrId);
			
			rsReport = pstmtGetReport.executeQuery();
			
			if (rsReport.next()){
				if(rsReport.getString("CAREPROVIDER_TYPE").equals("CPR1")){
					result.setResult(IConstant.IND_YES);
				}
			}
			
			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
			result.setStatus(IConstant.STATUS_SUCCESSFULL);
		}
		catch (SQLException e){
			e.printStackTrace();
		    if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		    result.setStatus(IConstant.STATUS_ERROR);
		}
		catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
			result.setStatus(IConstant.STATUS_ERROR);
		}
		finally {
 			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
			if (pstmtGetReport != null) try { pstmtGetReport.close(); } catch (SQLException e) {}
			if (rsReport != null) try { rsReport.close(); } catch (SQLException e) {}
		}
		
		return result;
	}
	
	public static BaseResult syncSIPDoctor(String careProviderId){
		BaseResult result = new BaseResult();
		result.setResult(IConstant.IND_NO);
		result.setStatus(IConstant.STATUS_CONNECTED);
		try {
			String selectQuery = "SELECT CPLH.CAREPROVIDERLICENSEHISTORY_ID, PER.PERSON_NAME, CPLH.EFFECTIVE_DATE, CPLH.EXPIRED_DATE, CPLH.NO_SIP, CPLH.DEFUNCT_IND "
					+ " FROM CAREPROVIDERLICENSEHISTORY CPLH "
					+ " INNER JOIN CAREPROVIDER CP ON CP.CAREPROVIDER_ID = CPLH.CAREPROVIDER_ID "
					+ " INNER JOIN PERSON PER ON PER.PERSON_ID = CP.PERSON_ID "
					+ " WHERE CPLH.CAREPROVIDER_ID = ? ";
			List list = DbConnection.executeReader(DbConnection.KTHIS, selectQuery, new Object[] {careProviderId});
			for (int a=0;a<list.size();a++){
				HashMap row = (HashMap)list.get(a);
				
				String checkQuery = "SELECT * FROM tb_ContractMaintenance WHERE nomor_contract = ?";
				Object[] params = {row.get("NO_SIP")};				
				List list2 = DbConnection.executeReader(DbConnection.ROOMCONNECTION, checkQuery, params);	
				
				String syncQuery = "";
				if (list2.size() > 0){
					syncQuery = "UPDATE tb_ContractMaintenance SET contract_careprovider=?,nama_contract=?,nomor_contract=?,tgl_mulai_contract=?,tgl_akhir_contract=?,kategori_contract='SDM',subkategori='Medis',status=0,update_at='0',update_date=CURRENT_TIMESTAMP,deleted=? "
							+ " WHERE nomor_contract = ?";
					Object[] parameters = {row.get("CAREPROVIDERLICENSEHISTORY_ID").toString()
							,row.get("PERSON_NAME")
							, row.get("NO_SIP")
							, row.get("EFFECTIVE_DATE")
							, row.get("EXPIRED_DATE")
							, (row.get("DEFUNCT_IND") == null || row.get("DEFUNCT_IND").equals("") || row.get("DEFUNCT_IND").toString().equals(IConstant.IND_NO) ? 0 : 1) 
							,row.get("NO_SIP")};
					DbConnection.executeQuery(DbConnection.ROOMCONNECTION, syncQuery, parameters);
				}
				else {
					syncQuery = "INSERT INTO tb_ContractMaintenance (contract_careprovider,nama_contract,nomor_contract,tgl_mulai_contract,tgl_akhir_contract,kategori_contract,subkategori,status,created_at,created_date,deleted) "
							+ " VALUES (?,?, ?, ?, ?,'SDM','Medis','0','0',CURRENT_TIMESTAMP,?)";	
					Object[] parameters = {row.get("CAREPROVIDERLICENSEHISTORY_ID").toString()
							,row.get("PERSON_NAME")
							, row.get("NO_SIP")
							, row.get("EFFECTIVE_DATE")
							, row.get("EXPIRED_DATE")
							, (row.get("DEFUNCT_IND") == null || row.get("DEFUNCT_IND").equals("") || row.get("DEFUNCT_IND").toString().equals(IConstant.IND_NO) ? 0 : 1) };
					DbConnection.executeQuery(DbConnection.ROOMCONNECTION, syncQuery, parameters);
				}
				
				
				
				result.setResult(IConstant.IND_YES);
			}
			result.setStatus(IConstant.STATUS_SUCCESSFULL);
		}
		catch (SQLException e){
			e.printStackTrace();
			result.setStatus(IConstant.STATUS_ERROR);
		}
		catch (Exception e) {
			e.printStackTrace();
			result.setStatus(IConstant.STATUS_ERROR);
		}
		finally {
		}
		return result;
	}

	public static BaseResult syncSIPDoctorAll(){
		BaseResult result = new BaseResult();
		result.setResult(IConstant.IND_NO);
		result.setStatus(IConstant.STATUS_CONNECTED);
		try {
			String selectQuery = "SELECT CPLH.CAREPROVIDER_ID "
					+ " FROM CAREPROVIDERLICENSEHISTORY CPLH "
					+ " INNER JOIN CAREPROVIDER CP ON CP.CAREPROVIDER_ID = CPLH.CAREPROVIDER_ID "
					+ " INNER JOIN PERSON PER ON PER.PERSON_ID = CP.PERSON_ID ";
			List list = DbConnection.executeReader(DbConnection.KTHIS, selectQuery, new Object[] {});
			for (int a=0;a<list.size();a++){
				HashMap row = (HashMap)list.get(a);
				
				syncSIPDoctor(row.get("CAREPROVIDER_ID").toString());
				result.setResult(IConstant.IND_YES);
			}
			result.setStatus(IConstant.STATUS_SUCCESSFULL);
		}
		catch (SQLException e){
			e.printStackTrace();
			result.setStatus(IConstant.STATUS_ERROR);
		}
		catch (Exception e) {
			e.printStackTrace();
			result.setStatus(IConstant.STATUS_ERROR);
		}
		finally {
		}
		return result;
	}
}
