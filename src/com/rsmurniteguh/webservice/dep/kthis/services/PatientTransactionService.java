package com.rsmurniteguh.webservice.dep.kthis.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.exolab.castor.mapping.xml.Sql;
import org.quartz.JobExecutionContext;

import com.rsmurniteguh.webservice.dep.all.model.PatientTransactionStatus;
import com.rsmurniteguh.webservice.dep.all.model.PurchaseStatus;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.tasks.DailyEmailScheduledTask;
import com.rsmurniteguh.webservice.dep.tasks.EmailScheduledTask;

public class PatientTransactionService extends DbConnection {
	private static String glAccountIPBill = "11.31.99.02";
	private static String glAccountOPBill = "11.31.99.01";
	private static String glAccountIPBPJSBill = "11.31.99.02B";
	private static String glAccountOPBPJSBill = "11.31.99.01B";
	private static String glAccountIPTransaction = "11.31.99.02S";
	private static String glAccountOPTransaction = "11.31.99.01S";
	
	private static String glAccountPendapatanDrugsInPatient = "41.42.02.00";
	private static String glAccountPendapatanDrugsOutPatient = "41.42.01.00";
	
	public static List<PatientTransactionStatus> addPatientTransaction(String visitId, String journalMonth, String journalYear, String postedBy){
		List<PatientTransactionStatus> listView = new ArrayList<PatientTransactionStatus>();
		PatientTransactionStatus view = new PatientTransactionStatus();
		
//		view.setStatus("OK");
//		listView.add(view);
//		return listView;
		
		Connection pooledConnection = null;
		Connection purchaseConnection = null;
		
		PreparedStatement pstmtPatientTransaction = null;
		PreparedStatement pstmtInsertJurnal = null;
		PreparedStatement pstmtInsertJurnalDetail = null;
		PreparedStatement pstmtUpdateStatus = null;
		PreparedStatement pstmtInsertHistory = null;
		
		ResultSet rsPatientTransaction = null;
		
//		String kodeNoBukti;
		
		postedBy = java.net.URLDecoder.decode(postedBy);
		String[] dummyPostedBy = postedBy.split("###");
		postedBy = dummyPostedBy[0];
		
		try {
			if(dummyPostedBy.length <= 1){
				throw new Exception("Parameter Invalid");
			}
			String postedById = dummyPostedBy[1];
			
			String firstCheck = checkTxnForGLAccountNull(visitId, journalMonth, journalYear);
			if(firstCheck != null) throw new Exception(firstCheck);
			
			Calendar calEndMonth = Calendar.getInstance();
			calEndMonth.set(Calendar.YEAR, Integer.parseInt(journalYear));
			calEndMonth.set(Calendar.MONTH, Integer.parseInt(journalMonth) - 1);
			calEndMonth.set(Calendar.DAY_OF_MONTH, calEndMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
			calEndMonth.set(Calendar.HOUR, 1);
			calEndMonth.set(Calendar.MINUTE, 0);
			calEndMonth.set(Calendar.SECOND, 0);
			
			Timestamp tsEndMonth = new Timestamp(calEndMonth.getTimeInMillis());
			calEndMonth.add(Calendar.DATE, 1);
			Timestamp tsStartMonth = new Timestamp(calEndMonth.getTimeInMillis());
			tsEndMonth.setNanos(0);
			tsStartMonth.setNanos(0);
			
			pooledConnection = DbConnection.getPooledConnection();
			purchaseConnection = DbConnection.getPurchaseConnection();
			if (pooledConnection == null || purchaseConnection == null) throw new Exception("Database failed to connect !");
			purchaseConnection.setAutoCommit(false);
			pooledConnection.setAutoCommit(false);
			
			BigDecimal totalAmount = BigDecimal.ZERO;
			String txnNoStart = "";
			String txnNoEnd = "";
			String glDesc = "";
			String periodeStart = "";
			String periodeEnd = "";
			String glAccountDebit = "";
			String patClass = "";
			String patBPSJ = "";
			String kodePasien = "";
			
			String selectPatientTransaction = "SELECT TB.VISIT_ID, TB.GL_ACCOUNT, TB.TOTTXN, CARD.CARD_NO,PERSON.PERSON_NAME"
//					+ " , TB.TXN_DESC "
					+ " , (SELECT CODE_DESC FROM CODEMSTR WHERE CODE_CAT = SUBSTR(V.PATIENT_TYPE,1,3) AND CODE_ABBR = SUBSTR(V.PATIENT_TYPE,4)) PATIENT_TYPE "
					+ " , (SELECT CODE_DESC FROM CODEMSTR WHERE CODE_CAT = SUBSTR(V.PATIENT_CLASS,1,3) AND CODE_ABBR = SUBSTR(V.PATIENT_CLASS,4)) ISBPJS "
					+ " FROM "
					+ " ( SELECT  PA.VISIT_ID "
					+ "  , (CASE WHEN ICM.ITEM_TYPE = 'ITY1' OR ICM.ITEM_TYPE = 'ITY13' THEN 'KODEOBAT' ELSE CIM.GL_ACCOUNT END) GL_ACCOUNT "
//					+ "  ,PAT.TXN_DESC"
					+ "  , SUM(TXN_AMOUNT) TOTTXN FROM PATIENTACCOUNTTXN PAT "
					+ "  INNER JOIN CHARGEITEMMSTR CIM ON CIM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID "
					+ "  INNER JOIN PATIENTACCOUNT PA ON PA.PATIENTACCOUNT_ID = PAT.PATIENTACCOUNT_ID "
					+ "  INNER JOIN ITEMSUBCATEGORYMSTR ISCM ON ISCM.ITEMSUBCATEGORYMSTR_ID = CIM.ITEMSUBCATEGORYMSTR_ID "
					+ "  INNER JOIN ITEMCATEGORYMSTR ICM ON ICM.ITEMCATEGORYMSTR_ID = ISCM.ITEMCATEGORYMSTR_ID "
					+ "  WHERE PAT.BILL_ID IS NULL "
					+ "  AND PA.VISIT_ID = '" + visitId + "' "
					+ "  AND PAT.TXN_DATETIME < TO_DATE('" + new SimpleDateFormat("MM/dd/yyyy").format(tsStartMonth) + "','MM/dd/yyyy')"
					+ "  AND (SELECT COUNT(*) isAlreadyPost FROM TRANSACTIONPOSTING TP "
					+ "   WHERE TP.PATIENTACCOUNTTXN_ID = PAT.PATIENTACCOUNTTXN_ID "
					+ "   AND MONTHPOST = '" + journalMonth + "' "
					+ "   AND YEARPOST = '" + journalYear + "') = 0 "
					+ "  GROUP BY PA.VISIT_ID "
					+ "  , (CASE WHEN ICM.ITEM_TYPE = 'ITY1' OR ICM.ITEM_TYPE = 'ITY13' THEN 'KODEOBAT' ELSE CIM.GL_ACCOUNT END) "
//					+ "  , PAT.TXN_DESC"
					+ "  ) TB "
					+ " INNER JOIN VISIT V ON V.VISIT_ID = TB.VISIT_ID "
					+ " INNER JOIN PATIENT ON PATIENT.PATIENT_ID = V.PATIENT_ID "
					+ " INNER JOIN PERSON ON PERSON.PERSON_ID = PATIENT.PERSON_ID "
					+ " INNER JOIN CARD ON CARD.PERSON_ID = PERSON.PERSON_ID "
					+ " WHERE TOTTXN > 0 ";
			pstmtPatientTransaction = pooledConnection.prepareStatement(selectPatientTransaction);
			rsPatientTransaction = pstmtPatientTransaction.executeQuery();

			int sequence = 1;
			while(rsPatientTransaction.next()){
//				txnNo = rsPatientTransaction.getString("CARD_NO"); // rsPatientTransaction.getString("TXN_NO");//problem
				BigDecimal txnAmount = rsPatientTransaction.getBigDecimal("TOTTXN").setScale(2, RoundingMode.HALF_UP);
//				Timestamp txnDateTime = rsPatientTransaction.getTimestamp("TXN_DATETIME");
				periodeStart = Integer.toString(tsStartMonth.getYear() + 1900) + "123456789ABC".charAt(tsStartMonth.getMonth()); 
				periodeEnd = Integer.toString(tsEndMonth.getYear() + 1900) + "123456789ABC".charAt(tsEndMonth.getMonth()); 
				String glAccount = rsPatientTransaction.getString("GL_ACCOUNT");
				glDesc = "" + rsPatientTransaction.getString("CARD_NO") + "-" + rsPatientTransaction.getString("PERSON_NAME");
				patClass = rsPatientTransaction.getString("PATIENT_TYPE");
				patBPSJ = rsPatientTransaction.getString("ISBPJS");
//				String txnDesc = rsPatientTransaction.getString("TXN_DESC");
				
//				if (txnDateTime.compareTo(tsStartMonth) > 0) throw new Exception("Cannot post transaction to same or older time");
				
//				if(glAccount == null) throw new Exception("Product/Service account is not set [" + txnDesc + "]");
				if(patClass.equals("INPATIENTS")){
					glAccountDebit = glAccountIPTransaction;// "11.31.99.02";
//					if(patBPSJ.equals("BPJS")){
//						glAccountDebit = glAccountIPTransaction;
//					}
					if(glAccount.equals("KODEOBAT")){
						glAccount = glAccountPendapatanDrugsInPatient;
					}
					kodePasien = "IP";
				}else if(patClass.equals("OUTPATIENTS")){
					glAccountDebit = glAccountOPTransaction; // "11.31.99.01";
//					if(patBPSJ.equals("BPJS")){
//						glAccountDebit = glAccountOPTransaction;
//					}
					if(glAccount.equals("KODEOBAT")){
						glAccount = glAccountPendapatanDrugsOutPatient;
					}
					kodePasien = "OP";
				}else{
					throw new Exception("Patient account is not set");
				}
				
				if(patBPSJ.equals("BPJS")) kodePasien = kodePasien.substring(0,1) + "B";
				
//				if(txnPostCount.compareTo(txnReversePostCount) != 0) throw new Exception("Post Count and Reverse Post Count should be equall to Post a new Journal");
//				if(billId == null || billId == "") throw new Exception("Transaksi sudah masuk ke dalam bill");
				
				if(txnNoStart.equals("") || txnNoEnd.equals("")){
//					PreparedStatement pstmtSelectKode = null;
					PreparedStatement pstmtSelectJurnal = null;
//					ResultSet rsSelectKode = null;
					ResultSet rsSelectJurnalEnd = null;
					ResultSet rsSelectJurnalStart = null;
					
//					String selectKode = "SELECT KATEGORIJURNAL FROM GL_GLMas "
//							+ " WHERE No_Acc = ? "
//							+ " ORDER BY Periode DESC";
//					pstmtSelectKode = purchaseConnection.prepareStatement(selectKode);
//					pstmtSelectKode.setString(1, glAccount);
//					rsSelectKode = pstmtSelectKode.executeQuery();
//					
//					if(!rsSelectKode.next()) throw new Exception("Kategori Journal is empty");
//					kodeNoBukti = rsSelectKode.getString("KATEGORIJURNAL");
					
					//00001T/AD/RSMT/IB/04/2017
					String selectJurnalDetailEnd = "SELECT TOP (1) NoBukti FROM GL_JurnalDetail "
							+ " WHERE Periode = ? "
//							+ " AND Keterangan = '10. Auto Jurnal Transaksi' "
							+ " AND NoBukti like '%T/_%_%_%_%/RSMT/" + kodePasien + "/%' "
							+ " ORDER BY NoBukti DESC";
					pstmtSelectJurnal = purchaseConnection.prepareStatement(selectJurnalDetailEnd);
					pstmtSelectJurnal.setString(1, periodeEnd);
					rsSelectJurnalEnd = pstmtSelectJurnal.executeQuery();
					
					if(!rsSelectJurnalEnd.next()) {
						txnNoEnd = "00001";
					}
					else{
						String[] noEndSebelumnya = rsSelectJurnalEnd.getString("NoBukti").split("/");
						int noEndSebelum = Integer.parseInt(noEndSebelumnya[0].replace("T", "").replace("R", "")) + 1;
						txnNoEnd = String.format("%05d", noEndSebelum);
					}
//					txnNoEnd = "" + txnNoEnd + "T/" + kodeNoBukti + "/RSMT/" + kodePasien + "/" + new SimpleDateFormat("MM/YYYY").format(tsEndMonth);
					txnNoEnd = "" + txnNoEnd + "T/" + visitId + "/RSMT/" + kodePasien + "/" + new SimpleDateFormat("MM/YYYY").format(tsEndMonth);
					
					String selectJurnalDetailStart = "SELECT TOP (1) NoBukti FROM GL_JurnalDetail "
							+ " WHERE Periode = ? "
//							+ " AND Keterangan = '10. Auto Jurnal Transaksi' "
							+ " AND NoBukti like '%TR/_%_%_%_%/RSMT/" + kodePasien + "/%' "
							+ " ORDER BY NoBukti DESC";
					pstmtSelectJurnal = purchaseConnection.prepareStatement(selectJurnalDetailStart);
					pstmtSelectJurnal.setString(1, periodeStart);
					rsSelectJurnalStart = pstmtSelectJurnal.executeQuery();
					
					if(!rsSelectJurnalStart.next()){
						txnNoStart = "00001";
					}
					else{
						String[] noEndSebelumnya = rsSelectJurnalStart.getString("NoBukti").split("/");
						int noEndSebelum = Integer.parseInt(noEndSebelumnya[0].replace("T", "").replace("R", "")) + 1;
						txnNoStart = String.format("%05d", noEndSebelum);
					}
					txnNoStart = "" + txnNoStart + "TR/" + visitId + "/RSMT/" + kodePasien + "/" + new SimpleDateFormat("MM/YYYY").format(tsStartMonth);
				}
				
				if(txnNoEnd.equals("") || txnNoStart.equals("")) throw new Exception("Something wrong with numbering. Please contact Administrator");
				
				String insertJurnalDetail = "insert into dbo.GL_JurnalDetail "
						+ "(NoBukti, NoUrut, NomorUrut, Sandi, Keterangan, Debet, Kredit, TglTrans, TglTransKTHIS, Posted, "
						+ "ProsesAkhir, Unit, Periode, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NAMAREF, TEMPNUMBER, NOKAS) "
						+ "values "
//						+ "(?, ?, ?, ?, '10. Auto Jurnal Transaksi', ?, ?, ?, ?, 0, "
						+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, 0, "
						+ "0, 0.00, ?, ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, NULL)";
				pstmtInsertJurnalDetail = purchaseConnection.prepareStatement(insertJurnalDetail);
				
				// journal detail kredit
				pstmtInsertJurnalDetail.setString(1, txnNoEnd);
				pstmtInsertJurnalDetail.setInt(2, sequence);
				pstmtInsertJurnalDetail.setInt(3, sequence);
				pstmtInsertJurnalDetail.setString(4, glAccount);
				pstmtInsertJurnalDetail.setString(5, glDesc);
				pstmtInsertJurnalDetail.setBigDecimal(6, BigDecimal.ZERO);
				pstmtInsertJurnalDetail.setBigDecimal(7, txnAmount);
				pstmtInsertJurnalDetail.setTimestamp(8, tsEndMonth);
				pstmtInsertJurnalDetail.setTimestamp(9, tsEndMonth);
				pstmtInsertJurnalDetail.setString(10, periodeEnd);
				pstmtInsertJurnalDetail.setString(11, postedBy);
				pstmtInsertJurnalDetail.setString(12, postedBy);			
				pstmtInsertJurnalDetail.executeUpdate();
				
				// journal balek detail kredit
				pstmtInsertJurnalDetail.setString(1, txnNoStart);
				pstmtInsertJurnalDetail.setInt(2, sequence);
				pstmtInsertJurnalDetail.setInt(3, sequence);
				pstmtInsertJurnalDetail.setString(4, glAccount);
				pstmtInsertJurnalDetail.setString(5, glDesc);
				pstmtInsertJurnalDetail.setBigDecimal(6, txnAmount);
				pstmtInsertJurnalDetail.setBigDecimal(7,  BigDecimal.ZERO);
				pstmtInsertJurnalDetail.setTimestamp(8, tsStartMonth);
				pstmtInsertJurnalDetail.setTimestamp(9, tsStartMonth);
				pstmtInsertJurnalDetail.setString(10, periodeStart);
				pstmtInsertJurnalDetail.setString(11, postedBy);
				pstmtInsertJurnalDetail.setString(12, postedBy);			
				pstmtInsertJurnalDetail.executeUpdate();
				sequence++;
				
				totalAmount = totalAmount.add(txnAmount);
				
				String updateStatus = "UPDATE PATIENTACCOUNTTXN SET LAST_DATE_POST_ACC = sysdate, "
						+ " POST_ACC_COUNT = POST_ACC_COUNT + 1,"
						+ " LAST_DATE_REVERSE_POST_ACC = sysdate,"
						+ " REVERSE_POST_ACC_COUNT = REVERSE_POST_ACC_COUNT + 1 "
						+ " WHERE PATIENTACCOUNTTXN_ID IN "
						+ " (SELECT PAT.PATIENTACCOUNTTXN_ID FROM PATIENTACCOUNTTXN PAT "
						+ "  INNER JOIN PATIENTACCOUNT PA ON PA.PATIENTACCOUNT_ID = PAT.PATIENTACCOUNT_ID "
						+ "  WHERE PAT.BILL_ID IS NULL "
						+ " AND PA.VISIT_ID = '" + visitId + "' "
						+ " AND (SELECT COUNT(*) isAlreadyPost FROM TRANSACTIONPOSTING TP "
						+ "  WHERE TP.PATIENTACCOUNTTXN_ID = PAT.PATIENTACCOUNTTXN_ID "
						+ "  AND MONTHPOST = '" + journalMonth + "'"
						+ "  AND YEARPOST = '" + journalYear + "') = 0 )";
				pstmtUpdateStatus = pooledConnection.prepareStatement(updateStatus);
				pstmtUpdateStatus.executeUpdate();
				
				String insertHistory = "INSERT INTO TRANSACTIONPOSTING "
						+ " (PATIENTACCOUNTTXN_ID, MONTHPOST, DATEPOST, CREATED_BY, CREATED_DATETIME, YEARPOST) "
						+ " SELECT PATIENTACCOUNTTXN_ID, ?,sysdate,?,sysdate,? FROM PATIENTACCOUNTTXN "
						+ " WHERE PATIENTACCOUNTTXN_ID IN "
						+ " (SELECT PAT.PATIENTACCOUNTTXN_ID FROM PATIENTACCOUNTTXN PAT "
						+ "  INNER JOIN PATIENTACCOUNT PA ON PA.PATIENTACCOUNT_ID = PAT.PATIENTACCOUNT_ID "
						+ "  WHERE PAT.BILL_ID IS NULL "
						+ " AND PA.VISIT_ID = '" + visitId + "' "
						+ " AND (SELECT COUNT(*) isAlreadyPost FROM TRANSACTIONPOSTING TP "
						+ "  WHERE TP.PATIENTACCOUNTTXN_ID = PAT.PATIENTACCOUNTTXN_ID "
						+ "  AND MONTHPOST = '" + journalMonth + "'"
						+ "  AND YEARPOST = '" + journalYear + "') = 0 )";
				pstmtInsertHistory = pooledConnection.prepareStatement(insertHistory);
				pstmtInsertHistory.setString(1, journalMonth);
				pstmtInsertHistory.setString(2, postedById);
				pstmtInsertHistory.setString(3, journalYear);
				pstmtInsertHistory.executeUpdate();
//				view.setSuccessCount(view.getSuccessCount().add(BigDecimal.ONE));
				view.addSuccessCount();
			}
			
			if(totalAmount.equals(BigDecimal.ZERO)) {
				throw new Exception("Patient Transaction (" + visitId + ") is not valid or transaction amount equals to zero or already posted or posting date is equal/less than the transaction date");
			}
			else{
				String selectTotJurnal = "SeLECT ";
				
				String insertJurnal = "INSERT INTO dbo.GL_JurnalMast"
						+ "(NoBukti, Debet, Kredit, TglTrans, Periode, Posted, ProsesAkhir, Tipe, "
						+ "Keterangan, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NoRef, TEMPNUMBER, "
						+ "NAMAREF, SANDIREF, Checked, ADMISSION_DATETIME, NOKAS, CheckedBy, NoJurnalParent) "
						+ "values "
						+ "(?, '" + totalAmount + "', '" + totalAmount + "', ?, ?, 0, 0, '12. Auto Jurnal Transaksi Patient', "
//						+ "(?, '" + totalAmount + "', '" + totalAmount + "', ?, ?, 0, 0, '" + glDesc + "', "
						+ " ?, ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, "
						+ "NULL, NULL, 0, NULL, NULL, NULL, ?)";
				
				pstmtInsertJurnal = purchaseConnection.prepareStatement(insertJurnal);
				
				//journal 1
				pstmtInsertJurnal.setString(1,txnNoEnd);
				pstmtInsertJurnal.setTimestamp(2, tsEndMonth);
				pstmtInsertJurnal.setString(3, periodeEnd);
				pstmtInsertJurnal.setString(4, glDesc);
				pstmtInsertJurnal.setString(5, postedBy);
				pstmtInsertJurnal.setString(6, postedBy);
				pstmtInsertJurnal.setString(7, null);
				
				pstmtInsertJurnal.executeUpdate();
				
				// journal balek
				pstmtInsertJurnal.setString(1,txnNoStart);
				pstmtInsertJurnal.setTimestamp(2, tsStartMonth);
				pstmtInsertJurnal.setString(3, periodeStart);
				pstmtInsertJurnal.setString(4, glDesc);
				pstmtInsertJurnal.setString(5, postedBy);
				pstmtInsertJurnal.setString(6, postedBy);
				pstmtInsertJurnal.setString(7, txnNoEnd);
				
				pstmtInsertJurnal.executeUpdate();
				
//				if(patBPSJ.equals("BPJS")){
//					glAccountDebit = glAccountDebit.concat("B");
//				}
				
				//journal debit
				pstmtInsertJurnalDetail.setString(1, txnNoEnd);
				pstmtInsertJurnalDetail.setInt(2, sequence);
				pstmtInsertJurnalDetail.setInt(3, sequence);
				pstmtInsertJurnalDetail.setString(4, glAccountDebit);
				pstmtInsertJurnalDetail.setString(5, glDesc);
				pstmtInsertJurnalDetail.setBigDecimal(6, totalAmount);
				pstmtInsertJurnalDetail.setBigDecimal(7,  BigDecimal.ZERO);
				pstmtInsertJurnalDetail.setTimestamp(8, tsEndMonth);
				pstmtInsertJurnalDetail.setTimestamp(9, tsEndMonth);
				pstmtInsertJurnalDetail.setString(10, periodeEnd);
				pstmtInsertJurnalDetail.setString(11, postedBy);
				pstmtInsertJurnalDetail.setString(12, postedBy);		
				pstmtInsertJurnalDetail.executeUpdate();
				
				//journal balek debit
				pstmtInsertJurnalDetail.setString(1, txnNoStart);
				pstmtInsertJurnalDetail.setInt(2, sequence);
				pstmtInsertJurnalDetail.setInt(3, sequence);
				pstmtInsertJurnalDetail.setString(4, glAccountDebit);
				pstmtInsertJurnalDetail.setString(5, glDesc);
				pstmtInsertJurnalDetail.setBigDecimal(6, BigDecimal.ZERO);
				pstmtInsertJurnalDetail.setBigDecimal(7, totalAmount);
				pstmtInsertJurnalDetail.setTimestamp(8, tsStartMonth);
				pstmtInsertJurnalDetail.setTimestamp(9, tsStartMonth);
				pstmtInsertJurnalDetail.setString(10, periodeStart);
				pstmtInsertJurnalDetail.setString(11, postedBy);
				pstmtInsertJurnalDetail.setString(12, postedBy);		
				pstmtInsertJurnalDetail.executeUpdate();
				
				purchaseConnection.commit();
				pooledConnection.commit();
				
				pstmtInsertJurnalDetail.close();
				pstmtInsertJurnal.close();
			}
			view.setStatus("OK");
			
		} catch (SQLException e) {
			view.setStatus("Error SQL");
			if (e.getErrorCode() == 2627) view.setErrorMessage("Posting failed : Transaction has been posting before !"); 
			else view.setErrorMessage("Error SQL : " + e.getMessage());
		    if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		    if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}catch (Exception e){
			view.setStatus("Error Checking");
			view.setErrorMessage("Posting failed : " + e.getMessage());
			if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
			if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally{
			if (purchaseConnection != null) try { purchaseConnection.setAutoCommit(true); purchaseConnection.close(); } catch (SQLException e) {}
 			if (pooledConnection != null) try { pooledConnection.setAutoCommit(true); pooledConnection.close(); } catch (SQLException e) {}
			if (pstmtPatientTransaction != null) try { pstmtPatientTransaction.close(); } catch (SQLException e) {}
			if (pstmtInsertJurnal != null) try { pstmtInsertJurnal.close(); } catch (SQLException e) {}
			if (pstmtInsertJurnalDetail != null) try { pstmtInsertJurnalDetail.close(); } catch (SQLException e) {}
			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
			if (rsPatientTransaction != null) try { rsPatientTransaction.close(); } catch (SQLException e) {}
			listView.add(view);
		}
		return listView;
	}
	
	//bill
	public static List<PatientTransactionStatus> addPatientBill(String billId){
		System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Receive Bill ID : " + billId.toString());
		List<PatientTransactionStatus> listView = new ArrayList<PatientTransactionStatus>();
		PatientTransactionStatus view = new PatientTransactionStatus();
		
//		view.setStatus("OK");
//		listView.add(view);
//		return listView;
		
		Connection pooledConnection = null;
		Connection purchaseConnection = null;
		
		PreparedStatement pstmtPatientBill = null;
		PreparedStatement pstmtInsertJurnal = null;
		PreparedStatement pstmtInsertJurnalDetail = null;
		PreparedStatement pstmtUpdateStatus = null;
		
		ResultSet rsPatientBill = null;
		
		try {
			String firstCheck = checkBillForGLAccountNull(billId);
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " check Bill ID : " + billId.toString());
			if(firstCheck != null) throw new Exception(firstCheck);
			
			pooledConnection = DbConnection.getPooledConnection();
			purchaseConnection = DbConnection.getPurchaseConnection();
			if (pooledConnection == null || purchaseConnection == null) throw new Exception("Database failed to connect !");
			purchaseConnection.setAutoCommit(false);
			pooledConnection.setAutoCommit(false);
			
			BigDecimal totalAmount = BigDecimal.ZERO;
			String txnNo = "";
			String glDesc = "";
			String periode = "";
			String glAccountDebit = "";
			String patClass = "";
			String patBPSJ = "";
			Timestamp txnDateTime = null;
			String postedBy="";
			String kodePasien = "";
			String billNo = "";
			
			String selectPatientBill = "SELECT B.BILL_DATETIME, T.TOTBILL, B.BILL_ID,T.GL_ACCOUNT "
					+ " ,(SELECT CODE_DESC FROM CODEMSTR WHERE CODE_CAT = SUBSTR(V.PATIENT_TYPE,1,3) AND CODE_ABBR = SUBSTR(V.PATIENT_TYPE,4)) PATIENT_TYPE "
					+ " ,(SELECT CODE_DESC FROM CODEMSTR WHERE CODE_CAT = SUBSTR(V.PATIENT_CLASS,1,3) AND CODE_ABBR = SUBSTR(V.PATIENT_CLASS,4)) ISBPJS "
					+ " , B.BILL_DATETIME, CARD.CARD_NO, PERSON.PERSON_NAME "
					+ " , UM.USER_NAME ,B.POST_ACC_STATUS ,B.BILL_NO "
					+ " FROM ( "
					+ "   SELECT SUM(PAT.TXN_AMOUNT - (PAT.CLAIMABLE_AMOUNT)) TOTBILL ,PAT.BILL_ID "
					+ "  , (CASE WHEN ICM.ITEM_TYPE = 'ITY1' OR ICM.ITEM_TYPE = 'ITY13' THEN 'KODEOBAT' ELSE CIM.GL_ACCOUNT END) GL_ACCOUNT "
					+ "  FROM PATIENTACCOUNTTXN PAT " 
					+ "  INNER JOIN CHARGEITEMMSTR CIM ON CIM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID "
					+ "  INNER JOIN ITEMSUBCATEGORYMSTR ISCM ON ISCM.ITEMSUBCATEGORYMSTR_ID = CIM.ITEMSUBCATEGORYMSTR_ID "
					+ "  INNER JOIN ITEMCATEGORYMSTR ICM ON ICM.ITEMCATEGORYMSTR_ID = ISCM.ITEMCATEGORYMSTR_ID "
					+ "  WHERE PAT.BILL_ID = '" + billId + "' "
					+ "  GROUP BY pAT.BILL_ID, (CASE WHEN ICM.ITEM_TYPE = 'ITY1' OR ICM.ITEM_TYPE = 'ITY13' THEN 'KODEOBAT' ELSE CIM.GL_ACCOUNT END) "
					+ " ) T "
					+ " INNER JOIN BILL B ON B.BILL_ID = T.BILL_ID "
					+ " INNER JOIN PATIENTACCOUNT PA ON PA.PATIENTACCOUNT_ID = B.PATIENTACCOUNT_ID "
					+ " INNER JOIN VISIT V ON V.VISIT_ID = PA.VISIT_ID "
					+ " INNER JOIN PATIENT ON PATIENT.PATIENT_ID = V.PATIENT_ID "
					+ " INNER JOIN PERSON ON PERSON.PERSON_ID = PATIENT.PERSON_ID "
					+ " INNER JOIN CARD ON CARD.PERSON_ID = PERSON.PERSON_ID "
					+ " INNER JOIN USERMSTR UM ON UM.USERMSTR_ID = B.LAST_UPDATED_BY";
			pstmtPatientBill = pooledConnection.prepareStatement(selectPatientBill);
			rsPatientBill = pstmtPatientBill.executeQuery();
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Read Detail Oracle Bill ID : " + billId.toString());

			int sequence = 1;
			while(rsPatientBill.next()){
//				txnNo = rsPatientTransaction.getString("CARD_NO"); // rsPatientTransaction.getString("TXN_NO");//problem
				BigDecimal txnAmount = rsPatientBill.getBigDecimal("TOTBILL").setScale(2, RoundingMode.HALF_UP);
				txnDateTime = rsPatientBill.getTimestamp("BILL_DATETIME");
				periode = Integer.toString(txnDateTime.getYear() + 1900) + "123456789ABC".charAt(txnDateTime.getMonth());
				String glAccount = rsPatientBill.getString("GL_ACCOUNT");
				glDesc = "" + rsPatientBill.getString("CARD_NO") + "-" + rsPatientBill.getString("PERSON_NAME");
				patClass = rsPatientBill.getString("PATIENT_TYPE");
				patBPSJ = rsPatientBill.getString("ISBPJS");
				postedBy = rsPatientBill.getString("USER_NAME");
//				String txnDesc = rsPatientBill.getString("TXN_DESC");
				String postStatus = rsPatientBill.getString("POST_ACC_STATUS");
				billNo = rsPatientBill.getString("BILL_NO");
				
				if (postStatus.equals("Y")) throw new Exception("Bill have been posted");
				
//				if(glAccount == null) throw new Exception("Product/Service account is not set in bill [" + billNo + "]");
				
				if(patClass.equals("INPATIENTS")){
					glAccountDebit = glAccountIPBill; // "11.31.99.02";
					kodePasien = "IP";
					if(patBPSJ.equals("BPJS")){
						glAccountDebit = glAccountIPBPJSBill; //
						kodePasien = "IB";
					}
					if(glAccount.equals("KODEOBAT")){
						glAccount = glAccountPendapatanDrugsInPatient;
					}
				}else if(patClass.equals("OUTPATIENTS")){
					glAccountDebit = glAccountOPBill; // "11.31.99.01";
					kodePasien = "OP";
					if(patBPSJ.equals("BPJS")){
						glAccountDebit = glAccountOPBPJSBill; //
						kodePasien = "OB";
					}
					if(glAccount.equals("KODEOBAT")){
						glAccount = glAccountPendapatanDrugsOutPatient;
					}
					
				}else{
					throw new Exception("Patient account is not set");
				}

//				if(txnPostCount.compareTo(txnReversePostCount) != 0) throw new Exception("Post Count and Reverse Post Count should be equall to Post a new Journal");
//				if(billId == null || billId == "") throw new Exception("Transaksi sudah masuk ke dalam bill");
				
				if(txnNo.equals("")){
//					PreparedStatement pstmtSelectKode = null;
//					PreparedStatement pstmtSelectJurnal = null;
//					ResultSet rsSelectKode = null;
//					ResultSet rsSelectJurnal = null;
//					
////					String selectKode = "SELECT KATEGORIJURNAL FROM GL_GLMas "
////							+ " WHERE No_Acc = ? "
////							+ " ORDER BY Periode DESC";
////					pstmtSelectKode = purchaseConnection.prepareStatement(selectKode);
////					pstmtSelectKode.setString(1, glAccount);
////					rsSelectKode = pstmtSelectKode.executeQuery();
////					
////					if(!rsSelectKode.next()) throw new Exception("Kategori Journal is empty");
////					String kodeNoBukti = rsSelectKode.getString("KATEGORIJURNAL");
//					
//					//00001T/AD/RSMT/IB/04/2017
//					String selectJurnalDetailEnd = "SELECT TOP (1) NoBukti FROM GL_JurnalDetail "
//							+ " WHERE Periode = ? "
////							+ " AND Keterangan = '10. Auto Jurnal Transaksi' "
//							+ " AND NoBukti like '%/RSMT/" + kodePasien + "/%' "
//							+ " ORDER BY NoBukti DESC";
//					pstmtSelectJurnal = purchaseConnection.prepareStatement(selectJurnalDetailEnd);
//					pstmtSelectJurnal.setString(1, periode);
//					rsSelectJurnal = pstmtSelectJurnal.executeQuery();
//					
//					if(!rsSelectJurnal.next()) {
//						txnNo = "00001";
//					}
//					else{
//						String[] noEndSebelumnya = rsSelectJurnal.getString("NoBukti").split("/");
//						int noEndSebelum = Integer.parseInt(noEndSebelumnya[0].substring(0,noEndSebelumnya[0].length()-1)) + 1;
//						txnNo = String.format("%05d", noEndSebelum);
//					}
					txnNo = "" + billNo + "T/RSMT/" + kodePasien + "/" + new SimpleDateFormat("MM/YYYY").format(txnDateTime);
				}
				
				String insertJurnalDetail = "insert into dbo.GL_JurnalDetail "
						+ "(NoBukti, NoUrut, NomorUrut, Sandi, Keterangan, Debet, Kredit, TglTrans, TglTransKTHIS, Posted, "
						+ "ProsesAkhir, Unit, Periode, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NAMAREF, TEMPNUMBER, NOKAS) "
						+ "values "
//						+ "('" + txnNo + "', ?, ?, ?, '10. Auto Jurnal Transaksi Bill', ?, ?, ?, ?, 0, "
						+ "('" + txnNo + "', ?, ?, ?, ?, ?, ?, ?, ?, 0, "
						+ "0, 0.00, ?, ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, NULL)";
				pstmtInsertJurnalDetail = purchaseConnection.prepareStatement(insertJurnalDetail);
				
				// journal detail kredit
				pstmtInsertJurnalDetail.setInt(1, sequence);
				pstmtInsertJurnalDetail.setInt(2, sequence);
				pstmtInsertJurnalDetail.setString(3, glAccount);
				pstmtInsertJurnalDetail.setString(4, glDesc);
				pstmtInsertJurnalDetail.setBigDecimal(5, BigDecimal.ZERO);
				pstmtInsertJurnalDetail.setBigDecimal(6, txnAmount);
				pstmtInsertJurnalDetail.setTimestamp(7, txnDateTime);
				pstmtInsertJurnalDetail.setTimestamp(8, txnDateTime);
				pstmtInsertJurnalDetail.setString(9, periode);
				pstmtInsertJurnalDetail.setString(10, postedBy);
				pstmtInsertJurnalDetail.setString(11, postedBy);			
				pstmtInsertJurnalDetail.executeUpdate();
				
				sequence++;
				
				totalAmount = totalAmount.add(txnAmount);
				
//				view.setSuccessCount(view.getSuccessCount().add(BigDecimal.ONE));
				view.addSuccessCount();
			}
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " insert detail sqlserver Bill ID : " + billId.toString());
			if(totalAmount.equals(BigDecimal.ZERO)) {
				throw new Exception("Bill Transaction (" + billNo + ") is not valid or already posted");
			}
			else{
				String insertJurnal = "INSERT INTO dbo.GL_JurnalMast"
						+ "(NoBukti, Debet, Kredit, TglTrans, Periode, Posted, ProsesAkhir, Tipe, "
						+ "Keterangan, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NoRef, TEMPNUMBER, "
						+ "NAMAREF, SANDIREF, Checked, ADMISSION_DATETIME, NOKAS, CheckedBy) "
						+ "values "
						+ "('" + txnNo + "', '" + totalAmount + "', '" + totalAmount + "', ?, ?, 0, 0, '10. Auto Jurnal Transaksi Bill', "
						+ "?, ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, "
						+ "NULL, NULL, 0, NULL, NULL, NULL)";
				
				pstmtInsertJurnal = purchaseConnection.prepareStatement(insertJurnal);
				
				pstmtInsertJurnal.setTimestamp(1, txnDateTime);
				pstmtInsertJurnal.setString(2, periode);
				pstmtInsertJurnal.setString(3, glDesc);
				pstmtInsertJurnal.setString(4, postedBy);
				pstmtInsertJurnal.setString(5, postedBy);
				
				pstmtInsertJurnal.executeUpdate();
				
//				if(patBPSJ.equals("BPJS")){
//					glAccountDebit = glAccountDebit.concat("B");
//				}
				
				//journal debit
				pstmtInsertJurnalDetail.setInt(1, sequence);
				pstmtInsertJurnalDetail.setInt(2, sequence);
				pstmtInsertJurnalDetail.setString(3, glAccountDebit);
				pstmtInsertJurnalDetail.setString(4, glDesc);
				pstmtInsertJurnalDetail.setBigDecimal(5, totalAmount);
				pstmtInsertJurnalDetail.setBigDecimal(6,  BigDecimal.ZERO);
				pstmtInsertJurnalDetail.setTimestamp(7, txnDateTime);
				pstmtInsertJurnalDetail.setTimestamp(8, txnDateTime);
				pstmtInsertJurnalDetail.setString(9, periode);
				pstmtInsertJurnalDetail.setString(10, postedBy);
				pstmtInsertJurnalDetail.setString(11, postedBy);		
				pstmtInsertJurnalDetail.executeUpdate();
				System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " insert master sqlserver Bill ID : " + billId.toString());
				
				String updateStatus = "UPDATE BILL SET POST_ACC_STATUS = 'Y' WHERE BILL_ID = '" + billId + "' ";
				pstmtUpdateStatus = pooledConnection.prepareStatement(updateStatus);
				pstmtUpdateStatus.executeUpdate();
				
				purchaseConnection.commit();
				System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " commit sql server Bill ID : " + billId.toString());
				pooledConnection.commit();
				System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " commit oracle Bill ID : " + billId.toString());
				
				pstmtInsertJurnalDetail.close();
				pstmtInsertJurnal.close();
			}
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Finish Bill ID : " + billId.toString());
			view.setStatus("OK");
		} catch (SQLException e) {
			view.setStatus("Error SQL");
			if (e.getErrorCode() == 2627) view.setErrorMessage("Posting failed : Transaction has been posting before !"); 
			else view.setErrorMessage("Error SQL : " + e.getMessage());
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " error : " + billId.toString() + " " + e.getMessage());
		    if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		    if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}catch (Exception e){
			view.setStatus("Error Checking");
			view.setErrorMessage("Posting failed : " + e.getMessage());
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " error : " + billId.toString() + " " + e.getMessage());
			if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
			if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally{
			if (purchaseConnection != null) try { purchaseConnection.setAutoCommit(true); purchaseConnection.close(); } catch (SQLException e) {}
 			if (pooledConnection != null) try {pooledConnection.setAutoCommit(true); pooledConnection.close(); } catch (SQLException e) {}
 			if (pstmtPatientBill != null) try { pstmtPatientBill.close(); } catch (SQLException e) {}
			if (pstmtInsertJurnal != null) try { pstmtInsertJurnal.close(); } catch (SQLException e) {}
			if (pstmtInsertJurnalDetail != null) try { pstmtInsertJurnalDetail.close(); } catch (SQLException e) {}
			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
			if (rsPatientBill != null) try { rsPatientBill.close(); } catch (SQLException e) {}
			listView.add(view);
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Finally Bill ID : " + billId.toString());
		}
		return listView;
	}
	
	public static List<PatientTransactionStatus> addPatientCancelBill(String billId){
		System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Receive Cancel Bill ID : " + billId.toString());
		List<PatientTransactionStatus> listView = new ArrayList<PatientTransactionStatus>();
		PatientTransactionStatus view = new PatientTransactionStatus();
		
//		view.setStatus("OK");
//		listView.add(view);
//		return listView;
		
		Connection pooledConnection = null;
		Connection purchaseConnection = null;
		
		PreparedStatement pstmtPatientBill = null;
		PreparedStatement pstmtSelectJurnal = null;
		PreparedStatement pstmtInsertJurnal = null;
		PreparedStatement pstmtInsertJurnalDetail = null;
		PreparedStatement pstmtUpdateStatus = null;
		
		ResultSet rsPatientBill = null;
		ResultSet rsSelectJurnal = null;
		
		try {
			String firstCheck = checkBillForGLAccountNull(billId);
			if(firstCheck != null) throw new Exception(firstCheck);
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " check bill Cancel Bill ID : " + billId.toString());
			
			pooledConnection = DbConnection.getPooledConnection();
			purchaseConnection = DbConnection.getPurchaseConnection();
			if (pooledConnection == null || purchaseConnection == null) throw new Exception("Database failed to connect !");
			purchaseConnection.setAutoCommit(false);
			pooledConnection.setAutoCommit(false);
			
			BigDecimal totalAmount = BigDecimal.ZERO;
			String txnNo = "";
			String glDesc = "";
			String periode = "";
			String glAccountDebit = "";
			String patClass = "";
			String patBPSJ = "";
			Timestamp txnDateTime = null;
			String postedBy="";
			String kodePasien = "";
			String billNo = "";
			
			String selectPatientCancelBill = "SELECT B.CANCELLED_DATETIME, B.BILL_ID "
					+ " ,(SELECT CODE_DESC FROM CODEMSTR WHERE CODE_CAT = SUBSTR(V.PATIENT_TYPE,1,3) AND CODE_ABBR = SUBSTR(V.PATIENT_TYPE,4)) PATIENT_TYPE "
					+ " ,(SELECT CODE_DESC FROM CODEMSTR WHERE CODE_CAT = SUBSTR(V.PATIENT_CLASS,1,3) AND CODE_ABBR = SUBSTR(V.PATIENT_CLASS,4)) ISBPJS "
					+ " , B.BILL_DATETIME, CARD.CARD_NO, PERSON.PERSON_NAME ,B.BILL_NO  , UM.USER_NAME, B.POST_ACC_CANCEL_STATUS, B.POST_ACC_STATUS, B.BILL_STATUS "
					+ " FROM BILL B "
					+ " INNER JOIN PATIENTACCOUNT PA ON PA.PATIENTACCOUNT_ID = B.PATIENTACCOUNT_ID "
					+ " INNER JOIN VISIT V ON V.VISIT_ID = PA.VISIT_ID "
					+ " INNER JOIN PATIENT ON PATIENT.PATIENT_ID = V.PATIENT_ID "
					+ " INNER JOIN PERSON ON PERSON.PERSON_ID = PATIENT.PERSON_ID "
					+ " INNER JOIN CARD ON CARD.PERSON_ID = PERSON.PERSON_ID "
					+ " INNER JOIN USERMSTR UM ON UM.USERMSTR_ID = B.LAST_UPDATED_BY "
					+ " WHERE B.BILL_ID = '" + billId + "' ";
			
//			String selectPatientBill = "SELECT B.CANCELLED_DATETIME, T.TOTBILL, B.BILL_ID,T.GL_ACCOUNT, T.TOTBILL "
//					+ " ,(SELECT CODE_DESC FROM CODEMSTR WHERE CODE_CAT = SUBSTR(V.PATIENT_TYPE,1,3) AND CODE_ABBR = SUBSTR(V.PATIENT_TYPE,4)) PATIENT_TYPE "
//					+ " ,(SELECT CODE_DESC FROM CODEMSTR WHERE CODE_CAT = SUBSTR(V.PATIENT_CLASS,1,3) AND CODE_ABBR = SUBSTR(V.PATIENT_CLASS,4)) ISBPJS "
//					+ " , B.BILL_DATETIME, CARD.CARD_NO, PERSON.PERSON_NAME ,B.BILL_NO "
//					+ " , UM.USER_NAME, B.POST_ACC_CANCEL_STATUS, B.POST_ACC_STATUS, B.BILL_STATUS "
//					+ " FROM ( "
//					+ "	  SELECT SUM(PAT.TXN_AMOUNT - (PAT.CLAIMABLE_AMOUNT) ) TOTBILL ,PAT.BILL_ID "
//					+ "  , (CASE WHEN ICM.ITEM_TYPE = 'ITY1' OR ICM.ITEM_TYPE = 'ITY13' THEN 'KODEOBAT' ELSE CIM.GL_ACCOUNT END) GL_ACCOUNT "
//					+ "  FROM PATIENTACCOUNTTXN PAT "
//					+ "  INNER JOIN CHARGEITEMMSTR CIM ON CIM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID "
//					+ "  INNER JOIN ITEMSUBCATEGORYMSTR ISCM ON ISCM.ITEMSUBCATEGORYMSTR_ID = CIM.ITEMSUBCATEGORYMSTR_ID "
//					+ "  INNER JOIN ITEMCATEGORYMSTR ICM ON ICM.ITEMCATEGORYMSTR_ID = ISCM.ITEMCATEGORYMSTR_ID "
//					+ "  WHERE PAT.BILL_ID = '" + billId + "' "
//					+ "  GROUP BY PAT.BILL_ID,  (CASE WHEN ICM.ITEM_TYPE = 'ITY1' OR ICM.ITEM_TYPE = 'ITY13' THEN 'KODEOBAT' ELSE CIM.GL_ACCOUNT END) "
//					+ " ) T "
//					+ " INNER JOIN BILL B ON B.BILL_ID = T.BILL_ID "
//					+ " INNER JOIN PATIENTACCOUNT PA ON PA.PATIENTACCOUNT_ID = B.PATIENTACCOUNT_ID "
//					+ " INNER JOIN VISIT V ON V.VISIT_ID = PA.VISIT_ID "
//					+ " INNER JOIN PATIENT ON PATIENT.PATIENT_ID = V.PATIENT_ID "
//					+ " INNER JOIN PERSON ON PERSON.PERSON_ID = PATIENT.PERSON_ID "
//					+ " INNER JOIN CARD ON CARD.PERSON_ID = PERSON.PERSON_ID "
//					+ " INNER JOIN USERMSTR UM ON UM.USERMSTR_ID = B.LAST_UPDATED_BY ";
			pstmtPatientBill = pooledConnection.prepareStatement(selectPatientCancelBill);
			rsPatientBill = pstmtPatientBill.executeQuery();
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " read bill Cancel Bill ID : " + billId.toString());

			int sequence = 1;
			if(rsPatientBill.next()){
//				txnNo = rsPatientTransaction.getString("CARD_NO"); // rsPatientTransaction.getString("TXN_NO");//problem
//				BigDecimal txnAmount = rsPatientBill.getBigDecimal("TOTBILL").setScale(2, RoundingMode.HALF_UP);
				txnDateTime = rsPatientBill.getTimestamp("CANCELLED_DATETIME");
				periode = Integer.toString(txnDateTime.getYear() + 1900) + "123456789ABC".charAt(txnDateTime.getMonth());
//				String glAccount = rsPatientBill.getString("GL_ACCOUNT");
				glDesc = "" + rsPatientBill.getString("CARD_NO") + "-" + rsPatientBill.getString("PERSON_NAME");
				patClass = rsPatientBill.getString("PATIENT_TYPE");
				patBPSJ = rsPatientBill.getString("ISBPJS");
				postedBy = rsPatientBill.getString("USER_NAME");
//				String txnDesc = rsPatientBill.getString("TXN_DESC");
				String postStatusCancel = rsPatientBill.getString("POST_ACC_CANCEL_STATUS");
				String postStatus = rsPatientBill.getString("POST_ACC_STATUS");
				String billStatus = rsPatientBill.getString("BILL_STATUS");
				billNo = rsPatientBill.getString("BILL_NO");
				
				if (postStatusCancel.equals("Y")) throw new Exception("Bill have been posted cancel");
				if (postStatus.equals("N")) throw new Exception("Bill should have been posted before posted Cancel");
				if (!billStatus.equals("BISC")) throw new Exception ("Bill Status is not cancel");
//				if (glAccount == null) throw new Exception("Product/Service account is not set in bill [" + billNo + "]");
				
				if(patClass.equals("INPATIENTS")){
//					glAccountDebit = glAccountIPBill;// "11.31.99.02";
//					kodePasien = "IP";
//					if(patBPSJ.equals("BPJS")){
//						glAccountDebit = glAccountIPBPJSBill; //
//						kodePasien = "IB";
//					}
//					if(glAccount.equals("KODEOBAT")){
//						glAccount = glAccountPendapatanDrugsInPatient;
//					}
				}else if(patClass.equals("OUTPATIENTS")){
//					glAccountDebit = glAccountOPBill; // "11.31.99.01";
//					kodePasien = "OP";
//					if(patBPSJ.equals("BPJS")){
//						glAccountDebit = glAccountOPBPJSBill; //
//						kodePasien = "OB";
//					}
//					if(glAccount.equals("KODEOBAT")){
//						glAccount = glAccountPendapatanDrugsOutPatient;
//					}
				}else{
					throw new Exception("Patient account is not set");
				}
							
				if(txnNo.equals("")){
					txnNo = "" + billNo + "TR/RSMT/" + kodePasien + "/" + new SimpleDateFormat("MM/YYYY").format(txnDateTime);
				}
				
				String selectJurnalDetail = "SELECT sandi,debet,kredit FROM dbo.GL_JurnalDetail "
						+ " WHERE NoBukti like ? "
						+ " ORDER BY NoUrut ASC";
				pstmtSelectJurnal = purchaseConnection.prepareStatement(selectJurnalDetail);
				pstmtSelectJurnal.setString(1, "%" + billNo + "T/RSMT%");
				
				rsSelectJurnal = pstmtSelectJurnal.executeQuery();
				
				while(rsSelectJurnal.next()){
					String insertJurnalDetail = "insert into dbo.GL_JurnalDetail "
							+ "(NoBukti, NoUrut, NomorUrut, Sandi, Keterangan, Debet, Kredit, TglTrans, TglTransKTHIS, Posted, "
							+ "ProsesAkhir, Unit, Periode, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NAMAREF, TEMPNUMBER, NOKAS) "
							+ "values "
							+ "('" + txnNo + "', ?, ?, ?, ?, ?, ?, ?, ?, 0, "
							+ "0, 0.00, ?, ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, NULL)";
					pstmtInsertJurnalDetail = purchaseConnection.prepareStatement(insertJurnalDetail);
					
					// journal detail kredit
					pstmtInsertJurnalDetail.setInt(1, sequence);
					pstmtInsertJurnalDetail.setInt(2, sequence);
					pstmtInsertJurnalDetail.setString(3, rsSelectJurnal.getString("sandi"));
					pstmtInsertJurnalDetail.setString(4, glDesc);
					pstmtInsertJurnalDetail.setBigDecimal(5, rsSelectJurnal.getBigDecimal("kredit"));
					pstmtInsertJurnalDetail.setBigDecimal(6, rsSelectJurnal.getBigDecimal("debet"));
					pstmtInsertJurnalDetail.setTimestamp(7, txnDateTime);
					pstmtInsertJurnalDetail.setTimestamp(8, txnDateTime);
					pstmtInsertJurnalDetail.setString(9, periode);
					pstmtInsertJurnalDetail.setString(10, postedBy);
					pstmtInsertJurnalDetail.setString(11, postedBy);
					pstmtInsertJurnalDetail.executeUpdate();
					
					sequence++;
					
					totalAmount = totalAmount.add(rsSelectJurnal.getBigDecimal("kredit")).add(rsSelectJurnal.getBigDecimal("debet"));
					
//					view.setSuccessCount(view.getSuccessCount().add(BigDecimal.ONE));
					view.addSuccessCount();
				}				
			}
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " insert detail sqlserver Cancel Bill ID : " + billId.toString());
			if(totalAmount.equals(BigDecimal.ZERO)) {
				throw new Exception("Bill Transaction ID (" + billNo + ") is not valid or already posted");
			}
			else{
				String insertJurnal = "INSERT INTO dbo.GL_JurnalMast"
						+ "(NoBukti, Debet, Kredit, TglTrans, Periode, Posted, ProsesAkhir, Tipe, "
						+ "Keterangan, CreatedBy, CreatedTime, ModifiedBy, ModifiedTime, NoRef, TEMPNUMBER, "
						+ "NAMAREF, SANDIREF, Checked, ADMISSION_DATETIME, NOKAS, CheckedBy) "
						+ "values "
						+ "('" + txnNo + "', '" + totalAmount + "', '" + totalAmount + "', ?, ?, 0, 0, '11. Auto Jurnal Transaksi Cancel', "
//						+ "('" + txnNo + "', '" + totalAmount + "', '" + totalAmount + "', ?, ?, 0, 0, '" + glDesc + "', "
						+ "?, ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, NULL, NULL, "
						+ "NULL, NULL, 0, NULL, NULL, NULL)";
				
				pstmtInsertJurnal = purchaseConnection.prepareStatement(insertJurnal);
				
				pstmtInsertJurnal.setTimestamp(1, txnDateTime);
				pstmtInsertJurnal.setString(2, periode);
				pstmtInsertJurnal.setString(3, glDesc);
				pstmtInsertJurnal.setString(4, postedBy);
				pstmtInsertJurnal.setString(5, postedBy);
				
				pstmtInsertJurnal.executeUpdate();
				
//				if(patBPSJ.equals("BPJS")){
//					glAccountDebit = glAccountDebit.concat("B");
//				}
				
				//journal debit
//				pstmtInsertJurnalDetail.setInt(1, sequence);
//				pstmtInsertJurnalDetail.setInt(2, sequence);
//				pstmtInsertJurnalDetail.setString(3, glAccountDebit);
//				pstmtInsertJurnalDetail.setString(4, glDesc);
//				pstmtInsertJurnalDetail.setBigDecimal(5,  BigDecimal.ZERO);
//				pstmtInsertJurnalDetail.setBigDecimal(6, totalAmount);				
//				pstmtInsertJurnalDetail.setTimestamp(7, txnDateTime);
//				pstmtInsertJurnalDetail.setTimestamp(8, txnDateTime);
//				pstmtInsertJurnalDetail.setString(9, periode);
//				pstmtInsertJurnalDetail.setString(10, postedBy);
//				pstmtInsertJurnalDetail.setString(11, postedBy);		
//				pstmtInsertJurnalDetail.executeUpdate();
				System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " insert master sqlserver Cancel Bill ID : " + billId.toString());
				
				String updateStatus = "UPDATE BILL SET POST_ACC_CANCEL_STATUS = 'Y' WHERE BILL_ID = '" + billId + "' ";
				pstmtUpdateStatus = pooledConnection.prepareStatement(updateStatus);
				pstmtUpdateStatus.executeUpdate();
				
				purchaseConnection.commit();
				System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " commit sqlserver Cancel Bill ID : " + billId.toString());
				pooledConnection.commit();
				System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " commit oracle Cancel Bill ID : " + billId.toString());
				
				pstmtInsertJurnalDetail.close();
				pstmtInsertJurnal.close();
			}
			view.setStatus("OK");
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Finish Cancel Bill ID : " + billId.toString());
		} catch (SQLException e) {
			view.setStatus("Error SQL");
			if (e.getErrorCode() == 2627) view.setErrorMessage("Posting failed : Transaction has been posting before !"); 
			else view.setErrorMessage("Error SQL : " + e.getMessage());
		    if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
		    if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}catch (Exception e){
			view.setStatus("Error Checking");
			view.setErrorMessage("Posting failed : " + e.getMessage());
			if (purchaseConnection != null) try { purchaseConnection.rollback(); } catch (SQLException sqle) {}
			if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		}
		finally{
			if (purchaseConnection != null) try {purchaseConnection.setAutoCommit(true); purchaseConnection.close(); } catch (SQLException e) {}
 			if (pooledConnection != null) try {pooledConnection.setAutoCommit(true); pooledConnection.close(); } catch (SQLException e) {}
 			if (pstmtPatientBill != null) try { pstmtPatientBill.close(); } catch (SQLException e) {}
			if (pstmtInsertJurnal != null) try { pstmtInsertJurnal.close(); } catch (SQLException e) {}
			if (pstmtInsertJurnalDetail != null) try { pstmtInsertJurnalDetail.close(); } catch (SQLException e) {}
			if (pstmtUpdateStatus != null) try { pstmtUpdateStatus.close(); } catch (SQLException e) {}
			if (pstmtSelectJurnal != null) try { pstmtSelectJurnal.close(); } catch (SQLException e) {}
			if (rsPatientBill != null) try { rsPatientBill.close(); } catch (SQLException e) {}
			if (rsSelectJurnal != null) try { rsSelectJurnal.close(); } catch (SQLException e) {}
			listView.add(view);
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Finally Cancel Bill ID : " + billId.toString());
		}
		return listView;
	}
	
	private static String checkBillForGLAccountNull(String billId){
		String result=null;
		
		Connection pooledConnection = null;
		PreparedStatement pstmtPatientBill = null;
		
		ResultSet rsBillDetail = null;
		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null) throw new Exception("Database failed to connect !");
			String selectBillDetail = " SELECT (CASE WHEN ICM.ITEM_TYPE = 'ITY1' OR ICM.ITEM_TYPE = 'ITY13' THEN 'KODEOBAT' ELSE CIM.GL_ACCOUNT END) GL_ACCOUNT"
					+ "  , PAT.TXN_DESC "
					+ "  FROM BILLDETAIL BD "
					+ "  INNER JOIN BILLACCOUNT BA ON BA.BILLACCOUNT_ID = BD.BILLACCOUNT_ID "
					+ "  INNER JOIN PATIENTACCOUNTTXN PAT ON PAT.PATIENTACCOUNTTXN_ID = BD.PATIENTACCOUNTTXN_ID "
					+ "  INNER JOIN CHARGEITEMMSTR CIM ON CIM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID "
					+ "  INNER JOIN ITEMSUBCATEGORYMSTR ISCM ON ISCM.ITEMSUBCATEGORYMSTR_ID = CIM.ITEMSUBCATEGORYMSTR_ID"
					+ "  INNER JOIN ITEMCATEGORYMSTR ICM ON ICM.ITEMCATEGORYMSTR_ID = ISCM.ITEMCATEGORYMSTR_ID"
					+ "  WHERE BA.BILL_ID = '" + billId + "' ";
			pstmtPatientBill = pooledConnection.prepareStatement(selectBillDetail);
			rsBillDetail = pstmtPatientBill.executeQuery();
			
			while(rsBillDetail.next()){
				if(rsBillDetail.getString("GL_ACCOUNT") == null || rsBillDetail.getString("GL_ACCOUNT").equals("")){
					throw new Exception("Product/Service account is not set [" + rsBillDetail.getString("TXN_DESC") + "]");
				}
			}
			
		}catch(Exception e){
			result = e.getMessage();
		}
		finally{
 			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
 			if (pstmtPatientBill != null) try { pstmtPatientBill.close(); } catch (SQLException e) {}
			if (rsBillDetail != null) try { rsBillDetail.close(); } catch (SQLException e) {}
		}
		return result;
	}
	
	private static String checkTxnForGLAccountNull(String visitId, String journalMonth, String journalYear){
		String result = null;
		
		Connection pooledConnection = null;
		PreparedStatement pstmtPatientTransaction = null;
		
		ResultSet rsTransactionDetail = null;
		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null) throw new Exception("Database failed to connect !");
			String selectBillDetail = " SELECT  PA.VISIT_ID "
					+ "  , (CASE WHEN ICM.ITEM_TYPE = 'ITY1' OR ICM.ITEM_TYPE = 'ITY13' THEN 'KODEOBAT' ELSE CIM.GL_ACCOUNT END) GL_ACCOUNT "
					+ "  ,PAT.TXN_DESC, SUM(TXN_AMOUNT) TOTTXN FROM PATIENTACCOUNTTXN PAT "
					+ "  INNER JOIN CHARGEITEMMSTR CIM ON CIM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID "
					+ "  INNER JOIN PATIENTACCOUNT PA ON PA.PATIENTACCOUNT_ID = PAT.PATIENTACCOUNT_ID "
					+ "  INNER JOIN ITEMSUBCATEGORYMSTR ISCM ON ISCM.ITEMSUBCATEGORYMSTR_ID = CIM.ITEMSUBCATEGORYMSTR_ID "
					+ "  INNER JOIN ITEMCATEGORYMSTR ICM ON ICM.ITEMCATEGORYMSTR_ID = ISCM.ITEMCATEGORYMSTR_ID "
					+ "  WHERE PAT.BILL_ID IS NULL "
					+ "  AND PA.VISIT_ID = '" + visitId + "' "
					+ "  AND (SELECT COUNT(*) isAlreadyPost FROM TRANSACTIONPOSTING TP "
					+ "   WHERE TP.PATIENTACCOUNTTXN_ID = PAT.PATIENTACCOUNTTXN_ID "
					+ "   AND MONTHPOST = '" + journalMonth + "'"
					+ "   AND YEARPOST = '" + journalYear + "') = 0 "
					+ "  GROUP BY PA.VISIT_ID"
					+ "  , (CASE WHEN ICM.ITEM_TYPE = 'ITY1' OR ICM.ITEM_TYPE = 'ITY13' THEN 'KODEOBAT' ELSE CIM.GL_ACCOUNT END) "
					+ "  , PAT.TXN_DESC ";
			pstmtPatientTransaction = pooledConnection.prepareStatement(selectBillDetail);
			rsTransactionDetail = pstmtPatientTransaction.executeQuery();
			
			while(rsTransactionDetail.next()){
				if(rsTransactionDetail.getString("GL_ACCOUNT") == null || rsTransactionDetail.getString("GL_ACCOUNT").equals("")){
					throw new Exception("Product/Service account is not set [" + rsTransactionDetail.getString("TXN_DESC") + "]");
				}
			}
			
		}catch(Exception e){
			result = e.getMessage();
		}
		finally{
 			if (pooledConnection != null) try { pooledConnection.close(); } catch (SQLException e) {}
 			if (pstmtPatientTransaction != null) try { pstmtPatientTransaction.close(); } catch (SQLException e) {}
			if (rsTransactionDetail != null) try { rsTransactionDetail.close(); } catch (SQLException e) {}
		}
		
		return result;
	}

	public static void processAllBill(Timestamp thisDate){
		System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Start Automatic Post for : " + new SimpleDateFormat("yyyy MM dd").format(thisDate));
		
		Connection pooledConnection = null;		
		PreparedStatement pstmtPatientBill = null;		
		ResultSet rsPatientBill = null;
		
		try {			
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null) throw new Exception("Database failed to connect !");
						
			String selectPatientBill = "SELECT BILL_ID,BILL_STATUS FROM BILL"
					+ " WHERE (POST_ACC_STATUS = 'N' OR POST_ACC_CANCEL_STATUS = 'N') "
					+ " AND (TRUNC(bill_datetime) = TO_DATE('" + new SimpleDateFormat("YYYY-MM-dd").format(thisDate) + "','YYYY-MM-dd')"
					+ "  OR TRUNC(cancelled_datetime) = TO_DATE('" + new SimpleDateFormat("YYYY-MM-dd").format(thisDate) + "','YYYY-MM-dd'))";
			pstmtPatientBill = pooledConnection.prepareStatement(selectPatientBill);
			rsPatientBill = pstmtPatientBill.executeQuery();

			while(rsPatientBill.next()){
				addPatientBill(rsPatientBill.getString("BILL_ID"));
				if (rsPatientBill.getString("BILL_STATUS").equals("BISC")){
					addPatientCancelBill(rsPatientBill.getString("BILL_ID"));
				}
			}
			rsPatientBill.close();
			
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Finish Automatic Post for : " + new SimpleDateFormat("yyyy MM dd").format(thisDate));
		} catch (SQLException e) {
		    if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		    e.printStackTrace();
		}catch (Exception e){
			if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
			e.printStackTrace();
		}
		finally{
 			if (pooledConnection != null) try {pooledConnection.setAutoCommit(true); pooledConnection.close(); } catch (SQLException e) {}
 			if (pstmtPatientBill != null) try { pstmtPatientBill.close(); } catch (SQLException e) {}
			if (rsPatientBill != null) try { rsPatientBill.close(); } catch (SQLException e) {}
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Complete Automatic Post for : " + new SimpleDateFormat("yyyy MM dd").format(thisDate));
		}
	}

	public static void processMonthlyTransaction(String journalMonth, String journalYear ,String postedBy){
		System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Start Automatic Post Transaction for : " + journalYear + "/" + journalMonth);
		
		Connection pooledConnection = null;		
		PreparedStatement pstmtPatientBill = null;		
		ResultSet rsPatientBill = null;
		
		try {			
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null) throw new Exception("Database failed to connect !");
						
			String selectPatientBill = "SELECT V.VISIT_ID, "
					+ " CARD.CARD_NO as MEDREC, PS.PERSON_NAME, "
					+ "	PGCOMMON.FXGETCODEDESC(V.patient_type) as PATIENT_TYPE, "
					+ "	PGCOMMON.FXGETCODEDESC(V.patient_class) as PATIENT_CLASS, "
					+ " V.ADMISSION_DATETIME, V.DISCHARGE_DATETIME, IHIS.PGCOMMON.fxGetCodeDesc(ISCM.ip_receipt_item_cat) as RECEIPT, "
					+ "	TCM.TXN_CODE, ISCM.ITEM_SUBCAT_DESC, ISCM.ITEM_SUBCAT_CODE, RCM.REVENUE_CENTRE_DESC, "
					+ "	PAT.TXN_DATETIME, TCM.TXN_DESC, CIM.GL_ACCOUNT, PAT.TXN_AMOUNT, PAT.CLAIMABLE_AMOUNT as DISCOUNT_AMOUNT, PAT.CLAIMABLE_PERCENT, "
					+ " PAT.ENTERED_DATETIME, PAT.PATIENTACCOUNTTXN_ID, PAT.LAST_DATE_POST_ACC, PAT.LAST_DATE_REVERSE_POST_ACC,"
					+ "	PAT.POST_ACC_COUNT, PAT.REVERSE_POST_ACC_COUNT "
					+ " FROM PATIENTACCOUNTTXN PAT "
					+ "	LEFT JOIN CHARGEITEMMSTR CIM ON CIM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID "
					+ " INNER JOIN ITEMSUBCATEGORYMSTR ISCM ON CIM.ITEMSUBCATEGORYMSTR_ID = ISCM.ITEMSUBCATEGORYMSTR_ID "
					+ " INNER JOIN PATIENTACCOUNT PA ON PA.PATIENTACCOUNT_ID = PAT.PATIENTACCOUNT_ID "
					+ " INNER JOIN VISIT V ON V.VISIT_ID = PA.VISIT_ID "
					+ " INNER JOIN PATIENT ON PATIENT.PATIENT_ID = V.PATIENT_ID "
					+ " INNER JOIN PERSON PS ON PS.PERSON_ID = PATIENT.PERSON_ID "
					+ " INNER JOIN CARD ON CARD.PERSON_ID = PS.PERSON_ID "
					+ " INNER JOIN TXNCODEMSTR TCM ON TCM.TXNCODEMSTR_ID = PAT.TXNCODEMSTR_ID "
					+ " LEFT JOIN REVENUECENTREMSTR RCM ON PAT.REVENUECENTREMSTR_ID = RCM.REVENUECENTREMSTR_ID "
					+ " WHERE 1=1 "
					+ " AND V.ADMIT_STATUS IN('AST1','AST3','AST4','AST7') "
					+ " AND V.CANCEL_REASON IS NULL "
					+ "	AND trunc(PAT.ENTERED_DATETIME) >= trunc(?) "
					+ "	AND trunc(PAT.ENTERED_DATETIME) <= trunc(?) "
					+ " AND	PAT.BILL_ID IS NULL "
					+ "	ORDER BY V.VISIT_ID";
			pstmtPatientBill = pooledConnection.prepareStatement(selectPatientBill);
			
			int iYear = Integer.parseInt(journalYear);
			int iMonth = Integer.parseInt(journalMonth) - 1; // 1 (months begin with 0)
			int iDay = 1;

			Calendar startCal = new GregorianCalendar(iYear, iMonth, iDay);
			
			int daysInMonth = startCal.getActualMaximum(Calendar.DAY_OF_MONTH);
			
			Calendar endCal = new GregorianCalendar(iYear, iMonth, daysInMonth);
			
			pstmtPatientBill.setTimestamp(1, new Timestamp(startCal.getTimeInMillis()));
			pstmtPatientBill.setTimestamp(2, new Timestamp(endCal.getTimeInMillis()));
			
			rsPatientBill = pstmtPatientBill.executeQuery();

			while(rsPatientBill.next()){
				addPatientTransaction(rsPatientBill.getString("VISIT_ID"),journalMonth,journalYear,postedBy);
			}
			rsPatientBill.close();
			
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Finish Automatic Post Transaction for : " + journalYear + "/" + journalMonth);
		} catch (SQLException e) {
		    if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
		    e.printStackTrace();
		}catch (Exception e){
			if (pooledConnection != null) try { pooledConnection.rollback(); } catch (SQLException sqle) {}
			e.printStackTrace();
		}
		finally{
 			if (pooledConnection != null) try {pooledConnection.setAutoCommit(true); pooledConnection.close(); } catch (SQLException e) {}
 			if (pstmtPatientBill != null) try { pstmtPatientBill.close(); } catch (SQLException e) {}
			if (rsPatientBill != null) try { rsPatientBill.close(); } catch (SQLException e) {}
			System.out.println(new SimpleDateFormat("yyyy MM dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()).toString() + " Complete Automatic Post Transaction for : " + journalYear + "/" + journalMonth);
		}
	}
}
