package com.rsmurniteguh.webservice.dep.kthis.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.NamaLabUser;
import com.rsmurniteguh.webservice.dep.all.model.NamaOrder;
import com.rsmurniteguh.webservice.dep.all.model.NamaRadiologi;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class LabUserSer {

	public static List<NamaLabUser> getlabuser(String submstrid) {
		// TODO Auto-generated method stub
		List<NamaLabUser> all=new ArrayList<NamaLabUser>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet idmstr=null;
		if(connection == null)return null;
		Statement userlab=null;
		String mstid="SELECT NULL RESOURCESCHEME_ID, "
				+ "RM.RESOURCEMSTR_ID, "
				+ "RM.RESOURCE_CODE, "
				+ "RM.RESOURCE_SHORT_CODE, "
				+ "RM.RESOURCE_QUICK_CODE, "
				+ "RM.RESOURCE_NAME, "
				+ "RM.RESOURCE_NAME_LANG1, "
				+ "RM.REGISTRATION_TYPE, "
				+ "RM.SUBSPECIALTYMSTR_ID, "
				+ "SSPM.SUBSPECIALTY_DESC, "
				+ "SSPM.SUBSPECIALTY_DESC_LANG1, "
				+ "RM.CAREPROVIDER_ID, "
				+ "NULL SESSIONCODE, "
				+ "NULL SESSIONDESC, "
				+ "NULL SESSIONDESC1, "
				+ "NULL REGNLIMIT, "
				+ "NULL REGNCOUNT, "
				+ "NULL REGNAVAILABLECOUNT, "
				+ "(SELECT REGN_TYPE "
				+ "FROM (SELECT RPCL.REGN_TYPE "
				+ "FROM REGNPATIENTCLASSLIMIT RPCL "
				+ " WHERE 1=1 "
				+ "and RPCL.PATIENT_CLASS = 'PTC110' "
				+ "AND RPCL.DEFUNCT_IND = 'N' "
				+ "ORDER BY RPCL.REGN_TYPE) TEMP "
				+ "WHERE ROWNUM = 1) PATIENT_CLASS_REGN_TYPE "
				+ "FROM RESOURCEMSTR RM, SUBSPECIALTYMSTR SSPM "
				+ "WHERE RM.SUBSPECIALTYMSTR_ID = SSPM.SUBSPECIALTYMSTR_ID "
				+ "AND RM.RESOURCE_SCHEME_IND = 'N' "
				+ "AND RM.DEFUNCT_IND = 'N' "
				+ "and sspm.subspecialtymstr_id = '"+submstrid+"' "
				+ "AND EXISTS ( "
				+ "SELECT 0 "
				+ "FROM REGNPATIENTTYPELIMIT RPL, "
				+ "(SELECT PATIENT_TYPE, NVL(PATIENT_CLASS, 'NULL') PATIENT_CLASS "
				+ "FROM (SELECT RPL.PATIENT_TYPE, RPL.PATIENT_CLASS "
				+ "FROM REGNPATIENTTYPELIMIT RPL "
				+ "WHERE RPL.DEFUNCT_IND = 'N' "
				+ "AND RPL.PATIENT_TYPE = 'PTY2' "
				+ "AND (RPL.PATIENT_CLASS = '' OR RPL.PATIENT_CLASS IS NULL) "
				+ "ORDER BY RPL.PATIENT_CLASS) "
				+ "WHERE ROWNUM = 1) MATCHED_RPL_POLICY "
				+ "WHERE RPL.DEFUNCT_IND = 'N' "
				+ "AND RPL.PATIENT_TYPE = MATCHED_RPL_POLICY.PATIENT_TYPE "
				+ "AND NVL(RPL.PATIENT_CLASS, 'NULL') = MATCHED_RPL_POLICY.PATIENT_CLASS "
				+ "AND RPL.REGN_TYPE = RM.REGISTRATION_TYPE) ";
		try {
			userlab=connection.createStatement();
			idmstr=userlab.executeQuery(mstid);
			while (idmstr.next())
			{
				NamaLabUser nlu=new NamaLabUser();
				nlu.setResourcemstrID(idmstr.getString("RESOURCEMSTR_ID"));
				nlu.setResourceCode(idmstr.getString("RESOURCE_CODE"));
				nlu.setResourceshortCode(idmstr.getString("RESOURCE_SHORT_CODE"));
				nlu.setResourcequickCode(idmstr.getString("RESOURCE_QUICK_CODE"));
				nlu.setResourceName(idmstr.getString("RESOURCE_NAME"));
				nlu.setResourceNameLang(idmstr.getString("RESOURCE_NAME"));
				nlu.setRegistrationType(idmstr.getString("REGISTRATION_TYPE"));
				nlu.setSubspecialmstrId(idmstr.getString("SUBSPECIALTYMSTR_ID"));
				nlu.setSubspecialtyDesc(idmstr.getString("SUBSPECIALTY_DESC"));
				nlu.setSubspecialtyDesclang(idmstr.getString("SUBSPECIALTY_DESC_LANG1"));
				nlu.setCareproviderID(idmstr.getString("CAREPROVIDER_ID"));
				nlu.setPatientClassRegnType(idmstr.getString("PATIENT_CLASS_REGN_TYPE"));
				all.add(nlu);
			}
			idmstr.close();
			userlab.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (idmstr!=null) try  { idmstr.close(); } catch (Exception ignore){}
		     if (userlab!=null) try  { userlab.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<NamaOrder> getorder() {
		// TODO Auto-generated method stub
		List<NamaOrder> all=new ArrayList<NamaOrder>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet idorder=null;
		if(connection == null)return null;
		Statement getorder=null;
		String orderid="select oim.ORDERITEMMSTR_ID, lgm.LABGROUP_DESC , PGCOMMON.FXGETCODEDESC(lgi.SPECIMEN_TYPE) as specimen, "
				+ "tcm.TXN_DESC from LABGROUPITEM lgi "
				+ "inner join LABGROUPMSTR lgm on lgm.LABGROUPMSTR_ID = lgi.LABGROUPMSTR_ID "
				+ "inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = lgi.ORDERITEMMSTR_ID "
				+ "inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID";
		try {
			getorder =connection.createStatement();
			idorder =getorder.executeQuery(orderid);
			while (idorder.next())
			{
				NamaOrder no =new NamaOrder();
				no.setORDERITEMMSTR_ID(idorder.getString("ORDERITEMMSTR_ID"));
				no.setLABGROUP_DESC(idorder.getString("LABGROUP_DESC"));
				no.setSpecimen(idorder.getString("specimen"));
				no.setTXN_DESC(idorder.getString("TXN_DESC"));
				all.add(no);
			}
			idorder.close();
			getorder.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (idorder!=null) try  { idorder.close(); } catch (Exception ignore){}
		     if (getorder!=null) try  { getorder.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<NamaRadiologi> getradiologi() {
		// TODO Auto-generated method stub
		List<NamaRadiologi> all = new ArrayList<NamaRadiologi>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet RadId=null;
		if(connection == null)return null;
		Statement getradiologi=null;
		String idRad = "SELECT OIM.ORDERITEMMSTR_ID, "
				+ "TXN.TXNCODEMSTR_ID,TXN.TXN_CODE,TXN.TXN_DESC, "
				+ "TXN.TXN_DESC_LANG1, "
				+ "' '||TXN.SHORT_CODE||DECODE(MIM.BRAND_NAME_LANG2,NULL,'', "
				+ "'(' ||MIM.BRAND_NAME_LANG2||')') AS SHORT_CODE, "
				+ "TXN.PARENT_TXNCODEMSTR_ID , "
				+ "DECODE(ICM.ITEM_TYPE, 'ITY1', 'Y', 'ITY2','Y', 'ITY3','Y','N') AS ITEM_TYPE_DRUG, "
				+ "ICM.ITEM_CAT_DESC, "
				+ "ICM.ITEM_CAT_DESC_LANG1, ICM.ITEM_CAT_CODE, "
				+ "ICM.ITEM_TYPE, "
				+ "ISCM.ITEM_SUBCAT_CODE, "
				+ "ISCM.ITEM_SUBCAT_DESC,ISCM.ITEM_SUBCAT_DESC_LANG1, "
				+ "CIM.UNIT_PRICE, "
				+ "CIM.PRICE_UOM , "
				+ "CIM_TXN.TXN_CODE AS CIM_TXN_CODE, "
				+ "CIM.CHARGEITEMMSTR_ID,			 "
				+ "CIM.UNIT_PRICE AS PRICE, "
				+ "CIM.PRICE_UOM  AS UNIT, "
				+ "( SELECT CM.CODE_DESC FROM CODEMSTR CM WHERE CM.CODE_CAT||CODE_ABBR = CIM.PRICE_UOM ) UNIT_DESC, "
				+ "( SELECT CM.CODE_DESC FROM CODEMSTR CM WHERE CM.CODE_CAT||CODE_ABBR = ICM.ITEM_TYPE )ITEMTYPE_DESC_NORMAL "
				+ "FROM TXNCODEMSTR TXN, "
				+ "ITEMSUBCATEGORYMSTR ISCM, "
				+ "ITEMCATEGORYMSTR ICM, "
				+ "CHARGEITEMMSTR CIM, "
				+ "ORDERITEMMSTR OIM, "
				+ "TCMINCOMMONUSEMSTR TMM, "
				+ "MATERIALITEMMSTR MIM, "
				+ "TXNCODEMSTR CIM_TXN,	"
				+ "CN_INTERFACEITEMMSTR MIO, "
				+ "((SELECT NULL AS MATERIALITEMMSTR_ID, NULL AS "
				+ "BALANCE_QTY,"
				+ "NULL AS SIM_ITY  FROM DUAL WHERE 1 =2))SIM "
				+ "WHERE OIM.TXNCODEMSTR_ID = TXN.TXNCODEMSTR_ID "
				+ "AND OIM.ITEMSUBCATEGORYMSTR_ID = ISCM.ITEMSUBCATEGORYMSTR_ID "
				+ "AND ICM.ITEMCATEGORYMSTR_ID = ISCM.ITEMCATEGORYMSTR_ID "
				+ "AND ICM.DEFUNCT_IND = 'N' "
				+ "AND ISCM.DEFUNCT_IND = 'N' "
				+ "AND CIM.CHARGEITEMMSTR_ID (+)= OIM.CHARGEITEMMSTR_ID "
				+ "AND TXN.PARENT_TXNCODEMSTR_ID = CIM_TXN.TXNCODEMSTR_ID(+) "
				+ "AND OIM.DEFUNCT_IND = 'N' "
				+ "AND TXN.TXNCODEMSTR_ID = TMM.TXNCODEMSTR_ID(+) "
				+ "AND OIM.OUTPATIENT_IND = 'Y' "
				+ "AND OIM.APPLYTOORDERENTRY_IND = 'Y' "
				+ "AND (OIM.EFFECTIVE_DATE IS NULL OR OIM.EFFECTIVE_DATE <= SYSDATE) "
				+ "AND (OIM.EXPIRY_DATE IS NULL OR OIM.EXPIRY_DATE >= SYSDATE) "
				+ "AND TXN.DEFUNCT_IND = 'N' "
				+ "AND OIM.MATERIALITEMMSTR_ID =MIM.MATERIALITEMMSTR_ID(+) "
				+ "and CIM.TXNCODEMSTR_ID = CIM_TXN.TXNCODEMSTR_ID "
				+ "AND CIM_TXN.DEFUNCT_IND = 'N'  "
				+ "AND CIM.CHARGE_TYPE IN ('ICTCH','ICTFX') "
				+ "AND CIM.DEFUNCT_IND = 'N'  	"
				+ "and OIM.ORDERITEMMSTR_ID IN (SELECT EIP.ORDERITEMMSTR_ID FROM EXAMINATIONITEMSSPMTYPE EIP "
				+ "WHERE EIP.DEFUNCT_IND ='N'  and          							"
				+ "EIP.SUBSPECIALTYMSTR_ID  = 311099875) "
				+ "and ICM.ITEM_TYPE = 'ITY7'       "
				+ "AND SIM.MATERIALITEMMSTR_ID(+) =  OIM.MATERIALITEMMSTR_ID " 
				+ "AND CIM.CHARGEITEMMSTR_ID = MIO.CHARGEITEMMSTR_ID (+) "
				+ "AND MIO.DEFUNCT_IND(+) = 'N' "
				+ "ORDER BY  TXN_DESC, TXN_CODE	, ORDERITEMMSTR_ID ";
		try 
		{
			getradiologi = connection.createStatement();
			RadId = getradiologi.executeQuery(idRad);
			while (RadId.next())
			{
				NamaRadiologi nr = new NamaRadiologi();
				nr.setORDERITEMMSTR_ID(RadId.getString("ORDERITEMMSTR_ID")); 
				nr.setTXNCODEMSTR_ID(RadId.getString("TXNCODEMSTR_ID"));
				nr.setTXN_CODE(RadId.getString("TXN_CODE"));
				nr.setTXN_DESC(RadId.getString("TXN_DESC"));
				nr.setTXN_DESC_LANG1(RadId.getString("TXN_DESC_LANG1"));
				nr.setSHORT_CODE(RadId.getString("SHORT_CODE"));
				nr.setPARENT_TXNCODEMSTR_ID(RadId.getString("PARENT_TXNCODEMSTR_ID"));
				nr.setITEM_TYPE_DRUG(RadId.getString("ITEM_TYPE_DRUG"));
				nr.setITEM_CAT_DESC(RadId.getString("ITEM_CAT_DESC"));
				nr.setITEM_CAT_DESC_LANG1(RadId.getString("ITEM_CAT_DESC_LANG1"));
				nr.setITEM_CAT_CODE(RadId.getString("ITEM_CAT_CODE"));
				nr.setITEM_TYPE(RadId.getString("ITEM_TYPE"));
				nr.setITEM_SUBCAT_CODE(RadId.getString("ITEM_SUBCAT_CODE"));
				nr.setITEM_SUBCAT_DESC(RadId.getString("ITEM_SUBCAT_DESC"));
				nr.setITEM_SUBCAT_DESC_LANG1(RadId.getString("ITEM_SUBCAT_DESC_LANG1"));
				nr.setUNIT_PRICE(RadId.getString("UNIT_PRICE"));
				nr.setPRICE_UOM(RadId.getString("PRICE_UOM"));
				nr.setCIM_TXN_CODE(RadId.getString("CIM_TXN_CODE"));
				nr.setCHARGEITEMMSTR_ID(RadId.getString("CHARGEITEMMSTR_ID"));
				nr.setPRICE(RadId.getString("PRICE"));
				nr.setUNIT(RadId.getString("UNIT"));
				nr.setUNIT_DESC(RadId.getString("UNIT_DESC"));
				nr.setITEMTYPE_DESC_NORMAL(RadId.getString("ITEMTYPE_DESC_NORMAL"));
				all.add(nr);
			}
			RadId.close();
			getradiologi.close();
						
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (RadId!=null) try  { RadId.close(); } catch (Exception ignore){}
		     if (getradiologi!=null) try  { getradiologi.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

}
