package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.Response;

import org.apache.http.util.TextUtils;
import org.apache.tomcat.util.codec.binary.Base64;

import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.rsmurniteguh.webservice.dep.all.model.PengajuanKlaim;
import com.rsmurniteguh.webservice.dep.all.model.Procedure;
import com.rsmurniteguh.webservice.dep.all.model.RujukanPcareOrRsByNoka;
import com.rsmurniteguh.webservice.dep.all.model.bpjs.RegisterSepParam;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInfoResponse;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsInsertSepResponse;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsRujukanListResponse;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsRujukanResponse;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsRujukanResponse.Info;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.BpjsRujukanResponse.Rujukan;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.GetSep;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.Metadata;
import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.IParameterConstant;
import com.rsmurniteguh.webservice.dep.kthis.services.BpjsIntegration.ISep.Platform;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.CollectionUtil;
import com.rsmurniteguh.webservice.dep.util.JsonUtil;
import com.rsmurniteguh.webservice.dep.all.model.BpjsSepUpdateParam;
import com.rsmurniteguh.webservice.dep.all.model.CurrentDatetim;
import com.rsmurniteguh.webservice.dep.all.model.Diagnosa;
import com.rsmurniteguh.webservice.dep.all.model.InsertSepMtregParam;


public class BpjsService2 {
	
//	private static final String SECRET_KEY = "0bYC766F75";
//	private static final String USER = "26805";
//	private static final String BASE_URL = "http://dvlp.bpjs-kesehatan.go.id:8081/VClaim-rest/SEP/insert";

//	private static final String SECRET_KEY = "3cTA543D2B";
//	private static final String USER = "12282";
//	private static final String BASE_URL = "http://api.bpjs-kesehatan.go.id:8080/vclaim-rest/";

	private static final String SECRET_KEY = getBaseConfig(IParameterConstant.BPJS_CONFIG_SECRET_KEY);
	private static final String USER = getBaseConfig(IParameterConstant.BPJS_CONFIG_USER);
	private static final String BASE_URL = getBaseConfig(IParameterConstant.BPJS_CONFIG_BASE_URL);
	private static final String URL_GET_MEMBER_INFO = "Peserta/nokartu/@@noka@@/tglSEP/@@tglSep@@";
	private static final String URL_GET_MEMBER_INFO_NIK = "Peserta/nik/@@nik@@/tglSEP/@@tglSep@@";
	private static final String URL_GET_MEMBER_INFO_REFERENCE_I = "Rujukan/@@noRujukI@@";
	private static final String URL_GET_MEMBER_INFO_REFERENCE_II = "Rujukan/RS/@@noRujukII@@";
	private static final String URL_GET_RUJUKAN_NORUJUKAN = "Rujukan/";
	private static final String URL_GET_RUJUKANRS_NORUJUKAN = "Rujukan/RS/";
	private static final String URL_GET_RUJUKAN_NOKA = "Rujukan/Peserta/";
	private static final String URL_GET_RUJUKANRS_NOKA = "Rujukan/RS/Peserta/";
	private static final String URL_GET_RUJUKAN_LIST_NOKA = "Rujukan/List/Peserta/";
	private static final String URL_GET_RUJUKANRS_LIST_NOKA = "Rujukan/RS/List/Peserta/";
	private static final String URL_GET_RUJUKAN_TGLRUJUKAN = "Rujukan/List/Peserta/";
	private static final String URL_GET_RUJUKANRS_TGLRUJUKAN = "Rujukan/RS/TglRujukan/";
	private static final String URL_GET_DPJP = "referensi/dokter/pelayanan/@@jnsPelayanan@@/tglPelayanan/@@tglSep@@/Spesialis/@@spesialis@@";
	
	private static final String URL_GET_REFERENSI_PROCEDURE = "referensi/procedure/";
	private static final String URL_GET_REFERENSI_KELAS_RAWAT = "referensi/kelasrawat";
	private static final String URL_GET_REFERENSI_DOKTER = "referensi/dokter/";
	private static final String URL_GET_REFERENSI_SPESIALISTIK = "referensi/spesialistik";
	private static final String URL_GET_REFERENSI_RUANG_RAWAT = "referensi/ruangrawat";
	private static final String URL_GET_REFERENSI_CARA_KELUAR = "referensi/carakeluar";
	private static final String URL_GET_REFERENSI_PASCA_PULANG = "referensi/pascapulang";
	private static final String URL_GET_REFERENSI_PROPINSI = "referensi/propinsi";
	private static final String URL_GET_REFERENSI_KABUPATEN = "referensi/kabupaten/propinsi/";
	private static final String URL_GET_REFERENSI_KECAMATAN = "referensi/kecamatan/kabupaten/";
	private static final String URL_GET_REFERENSI_POLI = "referensi/poli/@@kodeNama@@";
	private static final String URL_GET_SUPLESI_JASARAHARJA = "sep/JasaRaharja/Suplesi/@@noBPJS@@/tglPelayanan/@@tglSEP@@";
	

	private static final String URL_INSERT_SEP = "SEP/insert";
	private static final String URL_INSERT_SEP11 = "/SEP/1.1/insert";
	private static final String URL_DETAIL_SEP = "SEP/";
	private static final String URL_HAPUS_SEP = "SEP/Delete";
	private static final String URL_UPDATE_SEP = "Sep/Update";
	private static final String URL_PENGAJUAN_SEP = "Sep/pengajuanSEP";
	private static final String URL_APPROVAL_SEP = "Sep/aprovalSEP";
	private static final String URL_UPDATE_TGL_PLG = "Sep/updtglplg";
	private static final String URL_INTEGRASI_INACBG = "sep/cbg/";
	private static final String URL_INSERT_RUJUKAN = "Rujukan/insert";
	private static final String URL_UPDATE_RUJUKAN = "Rujukan/update";
	private static final String URL_DELETE_RUJUKAN = "Rujukan/delete";
	private static final String URL_DATA_KUNJUNGAN = "Monitoring/Kunjungan/Tanggal/@@tglSep@@/JnsPelayanan/@@jnsPelayanan@@";
	private static final String URL_DATA_KLAIM = "Monitoring/Klaim/Tanggal/@@tglPulang@@/JnsPelayanan/@@jnsPelayanan@@/Status/@@status@@";
	private static final String URL_FASKES= "referensi/faskes/@@koders@@/@@tipe@@";
	private static final String URL_DIAGNOSA= "referensi/diagnosa/@@diag@@";
	private static final String URL_INSERT_LPK = "LPK/insert";
	private static final String URL_UPDATE_SEP_1_1 = "/SEP/1.1/Update";
	private static String holderNoRujukan;
	private static String holderTglRujukan;
	private static final String paramBpjsVersion = getBaseConfig(IParameterConstant.BPJS_VERSION);
	
	private static String getBaseConfig(String parameter){
		CommonService cs = new CommonService();
		return cs.getParameterValue(parameter);
	}
	
	
	private static String generateHmacSHA256Signature(String data, String key) throws GeneralSecurityException {
		Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
		SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
		sha256_HMAC.init(secret_key);
		String hash = Base64.encodeBase64String(sha256_HMAC.doFinal(data.getBytes()));
		return hash;
	}
	private static HttpURLConnection SetHeader(HttpURLConnection con) throws Exception
	{
		
		try {
			long currentTime = System.currentTimeMillis() / 1000L;
			String salt = USER +"&"+ String.valueOf(currentTime);
			con.setRequestProperty("X-cons-id", USER);
			con.addRequestProperty("X-timestamp", String.valueOf(currentTime));
			con.addRequestProperty("X-signature",generateHmacSHA256Signature(salt, SECRET_KEY));
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	public static String GetBpjsInfobyNoka(String noKa) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		
		String tgl = dateFormat.format(date);
		
		String url = BASE_URL+URL_GET_MEMBER_INFO;
		url = url.replaceAll("@@noka@@", noKa);
		url = url.replaceAll("@@tglSep@@", tgl);

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	

	
	public static String GetBpjsInfobyReferenceI(String nik) {
		String url = BASE_URL+URL_GET_MEMBER_INFO_REFERENCE_I;
		url = url.replaceAll("@@noRujukI@@", nik);
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsInfobyReferenceII(String nik) {
		
		String url = BASE_URL+URL_GET_MEMBER_INFO_REFERENCE_II;
		url = url.replaceAll("@@noRujukII@@", nik);
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public enum BpjsInfoType{
		CardNo("CARD"),
		NIK("NIK"),
		ReferenceI("REF1"),
		ReferenceII("REF2");
		
		public static BpjsInfoType from(String type) {
			if(type ==null)return null;
			BpjsInfoType result  = null;
			List<BpjsInfoType> enums = new ArrayList(EnumSet.allOf(BpjsInfoType.class));
			if(enums == null)return null;
			for(BpjsInfoType target : enums) {
				if(target.toString().equals(type)) 
				{
					result = target;
					break;
				} 
			}
			return result;
		}
		
		private String type;
		BpjsInfoType(String type){
			this.type = type;
		}
		
		@Override
		public String toString() {
			return this.type;
		}
		
	}
	
	public static BpjsInfoResponse GetBpjsInfobyType(BpjsInfoType type, String target) {
		BpjsInfoResponse result = new BpjsInfoResponse();
		Metadata metadata = new Metadata();
		metadata.setCode("400");
		metadata.setMessage("Bad Request.");
		result.setMetadata(metadata);
		Gson gson = new Gson();
		BpjsRujukanResponse rujukanResponse = null;
		BpjsInfoResponse.Peserta peserta = null;
		BpjsInfoResponse.Response responsePeserta = null;
		if(type==null)return result;
		switch(type) {
			case CardNo :
				result = gson.fromJson(GetBpjsInfobyNoka(target), BpjsInfoResponse.class);
				break;
			case NIK :
				result = gson.fromJson(GetBpjsInfobyNik(target), BpjsInfoResponse.class);
				break;
			case ReferenceI :
				rujukanResponse = gson.fromJson(GetBpjsInfobyReferenceI(target), BpjsRujukanResponse.class);
				if(rujukanResponse== null)return result;
				if(rujukanResponse.getMetaData() == null) return result;
				result.setMetadata(rujukanResponse.getMetaData());
				if(rujukanResponse.getMetaData().getCode() == null)return result;
				if(!rujukanResponse.getMetaData().getCode().equals("200"))return result;
				result = new BpjsInfoResponse();
				result.setMetadata(rujukanResponse.getMetaData());
				peserta = rujukanResponse.getResponse() == null ? null : rujukanResponse.getResponse().getRujukan() == null
						? null : rujukanResponse.getResponse().getRujukan().getPeserta() ;
				responsePeserta = BpjsInfoResponse.Response.from(peserta);
				result.setResponse(responsePeserta);
				
				break;
			case ReferenceII :
				rujukanResponse = gson.fromJson(GetBpjsInfobyReferenceII(target), BpjsRujukanResponse.class);
				if(rujukanResponse== null)return result;
				if(rujukanResponse.getMetaData() == null) return result;
				result.setMetadata(rujukanResponse.getMetaData());
				if(rujukanResponse.getMetaData().getCode() == null)return result;
				if(!rujukanResponse.getMetaData().getCode().equals("200"))return result;
				result = new BpjsInfoResponse();
				result.setMetadata(rujukanResponse.getMetaData());
				peserta = rujukanResponse.getResponse() == null ? null : rujukanResponse.getResponse().getRujukan() == null
						? null : rujukanResponse.getResponse().getRujukan().getPeserta() ;
				responsePeserta = BpjsInfoResponse.Response.from(peserta);
				result.setResponse(responsePeserta);
				break;
			default :
				break;
		}
		return result;
	}
	
	public static String dpjp(String jnsPelayanan,
			String spesialis, String tanggal) {
		String url = BASE_URL+URL_GET_DPJP;
		String s = "";
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date tglRaw = dateFormat.parse(tanggal);
			String tgl = dateFormat.format(tglRaw);
			url = url.replaceAll("@@jnsPelayanan@@", jnsPelayanan);
			url = url.replaceAll("@@tglSep@@", tgl);
			url = url.replaceAll("@@spesialis@@", spesialis);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String GetBpjsInfobyNik(String nik) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		
		String tgl = dateFormat.format(date);
		
		String url = BASE_URL+URL_GET_MEMBER_INFO_NIK;
		url = url.replaceAll("@@nik@@", nik);
		url = url.replaceAll("@@tglSep@@", tgl);

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
		
	public static String insertSep(String noKa,String jnsPelayanan,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String poli,String noHp,String user){
		String url = BASE_URL+URL_INSERT_SEP;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			String tgl = dateFormat.format(date);
			
			
			String input =  "{"
					+ "\"request\": { "
					+ "\"t_sep\": { "
					+ "\"noKartu\": \""+noKa+"\", "
					+ "\"tglSep\": \""+tgl+"\","
					+ "\"ppkPelayanan\": \"0038R091\", "
					+ "\"jnsPelayanan\": \""+jnsPelayanan+"\", "
					+ "\"klsRawat\": \""+klsRawat+"\", "
					+ "\"noMR\": \""+mrn+"\", "
					+ "\"rujukan\": { "
					+ "\"asalRujukan\": \"1\", "
					+ "\"tglRujukan\": \""+tglRujuk+"\", "
					+ "\"noRujukan\": \""+noRujuk+"\", "
					+ "\"ppkRujukan\": \""+asalRujuk+"\" "
					+ "}, "
					+ "\"catatan\": \""+catatan+"\", "
					+ "\"diagAwal\": \""+diagAwal+"\", "
					+ "\"poli\": {"
					+ "\"tujuan\": \""+poli+"\", "
					+ "\"eksekutif\": \"0\""
					+ "}, "
					+ "\"cob\": { "
					+ "\"cob\": \"0\""
					+ "}, "
					+ "\"jaminan\": { "
					+ "\"lakaLantas\": \"0\", "
					+ "\"penjamin\": \"-\", "
					+ "\"lokasiLaka\": \"-\""
					+ "}, "
					+ "\"noTelp\": \""+noHp+"\","
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "} ";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String insertSep4(String noKa,String tglSep,String jnsPelayanan,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String poli,String noHp,String user){
		String url = BASE_URL+URL_INSERT_SEP;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			String tgl = dateFormat.format(date);
			
			
			String input =  "{"
					+ "\"request\": { "
					+ "\"t_sep\": { "
					+ "\"noKartu\": \""+noKa+"\", "
					+ "\"tglSep\": \""+tglSep+"\","
					+ "\"ppkPelayanan\": \"0038R091\", "
					+ "\"jnsPelayanan\": \""+jnsPelayanan+"\", "
					+ "\"klsRawat\": \""+klsRawat+"\", "
					+ "\"noMR\": \""+mrn+"\", "
					+ "\"rujukan\": { "
					+ "\"asalRujukan\": \"1\", "
					+ "\"tglRujukan\": \""+tglRujuk+"\", "
					+ "\"noRujukan\": \""+noRujuk+"\", "
					+ "\"ppkRujukan\": \""+asalRujuk+"\" "
					+ "}, "
					+ "\"catatan\": \""+catatan+"\", "
					+ "\"diagAwal\": \""+diagAwal+"\", "
					+ "\"poli\": {"
					+ "\"tujuan\": \""+poli+"\", "
					+ "\"eksekutif\": \"0\""
					+ "}, "
					+ "\"cob\": { "
					+ "\"cob\": \"0\""
					+ "}, "
					+ "\"jaminan\": { "
					+ "\"lakaLantas\": \"0\", "
					+ "\"penjamin\": \"-\", "
					+ "\"lokasiLaka\": \"-\""
					+ "}, "
					+ "\"noTelp\": \""+noHp+"\","
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "} ";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String insertSep2(String noKa,String jnsPelayanan,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String poli,String noHp,String user){
		String url = BASE_URL+URL_INSERT_SEP;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			String tgl = dateFormat.format(date);
			
			
			String input =  "{"
					+ "\"request\": { "
					+ "\"t_sep\": { "
					+ "\"noKartu\": \""+noKa+"\", "
					+ "\"tglSep\": \""+tgl+"\","
					+ "\"ppkPelayanan\": \"0038R091\", "
					+ "\"jnsPelayanan\": \""+jnsPelayanan+"\", "
					+ "\"klsRawat\": \""+klsRawat+"\", "
					+ "\"noMR\": \""+mrn+"\", "
					+ "\"rujukan\": { "
					+ "\"asalRujukan\": \"2\", "
					+ "\"tglRujukan\": \""+tglRujuk+"\", "
					+ "\"noRujukan\": \""+noRujuk+"\", "
					+ "\"ppkRujukan\": \"0038R091\" "
					+ "}, "
					+ "\"catatan\": \""+catatan+"\", "
					+ "\"diagAwal\": \""+diagAwal+"\", "
					+ "\"poli\": {"
					+ "\"tujuan\": \""+poli+"\", "
					+ "\"eksekutif\": \"0\""
					+ "}, "
					+ "\"cob\": { "
					+ "\"cob\": \"0\""
					+ "}, "
					+ "\"jaminan\": { "
					+ "\"lakaLantas\": \"0\", "
					+ "\"penjamin\": \"-\", "
					+ "\"lokasiLaka\": \"-\""
					+ "}, "
					+ "\"noTelp\": \""+noHp+"\","
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "} ";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	private static RujukanPcareOrRsByNoka.Rujukan buildDefaultFaskes2Rujukan(String faskes)
	{
		String asalRujuk = "0038R091"; // ASAL RUJUK UTAMA RS MURNI TEGUH
		RujukanPcareOrRsByNoka.Rujukan result = RujukanPcareOrRsByNoka.getDefault().getResponse().getRujukan();
		result.getProvPerujuk().setKode(asalRujuk);
		result.setNoKunjungan(holderNoRujukan);
		result.setTglKunjungan(holderTglRujukan);
		result.setFaskes(faskes);
		return result;
	}
	
	private static RujukanPcareOrRsByNoka.Rujukan getAsalRujukFaskes(String noKa, String faskes) {
		RujukanPcareOrRsByNoka.Rujukan result = buildDefaultFaskes2Rujukan(faskes);
		
		try {
			String ret = getRujukanPcareorRSbyNoka(noKa);
			Gson gson = new Gson();
			RujukanPcareOrRsByNoka view = gson.fromJson(ret, RujukanPcareOrRsByNoka.class);
			if(view == null)return result;
			if(view.getMetaData() == null)return result;
			if(!view.getMetaData().getCode().equals("200"))return result;
			if(view.getResponse() == null)return result;
			if(view.getResponse().getRujukan() == null) return result;
			if(view.getResponse().getRujukan().getTglKunjungan() == null)return result;
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date referenceDate = dateFormat.parse(view.getResponse().getRujukan().getTglKunjungan());
			Calendar calTglexpired = Calendar.getInstance();
			calTglexpired.setTime(referenceDate);
			calTglexpired.add(Calendar.MONTH, 3);
			Date expiredDate = calTglexpired.getTime();
			List<CurrentDatetim> getCurrentDatetime = StoreDispanceSer.getCurrentDatetime();
			String currenttime = getCurrentDatetime.get(0).getTanggal();
			DateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    		Date serverDatetime = df2.parse(currenttime);
    		if (serverDatetime.before(expiredDate)) {
    			if(view.getResponse().getRujukan().getProvPerujuk() == null)return result;
				result = view.getResponse().getRujukan();
			}else return result;
		}catch(Exception ex) {
			String fullTrack = ex.getMessage()+" | function : "+ex.getStackTrace()[0].getMethodName();
            ex.fillInStackTrace();
            System.out.println(fullTrack);
		}
		
		return result;
	}
	
	public static String insertSepMtRegBpjsV1_0(InsertSepMtregParam param, Boolean faskes) {
		String result = "";
		if(param == null)return result;
		String url = BASE_URL+URL_INSERT_SEP;
		
		String noKa = param.getNoKa();
		String jnsPelayanan = param.getJnsPelayanan();
		String klsRawat = param.getKlsRawat();
		String mrn = param.getMrn(); 
		String asalRujuk = param.getAsalRujuk();
		String tglRujuk = param.getTglRujuk();
		String noRujuk = param.getNoRujuk();
		String catatan = param.getCatatan();
		String diagAwal = param.getDiagAwal();
		String poli = param.getPoli();
		String noHp = param.getNoHp();
		String user = param.getUser();
		holderNoRujukan = noRujuk;
		holderTglRujukan = tglRujuk;
		
		// Faskes = true 	=> Faskes I.
		// Faskes = false	=> Faskes II.
		String asalRujukan = faskes ? String.valueOf(1) : String.valueOf(2); 
		RujukanPcareOrRsByNoka.Rujukan rujukan =  !faskes ? getAsalRujukFaskes(noKa, faskes ? "1" : "2") : null;
		if(rujukan != null) {
			asalRujuk = rujukan.getProvPerujuk().getKode();
			noRujuk = rujukan.getNoKunjungan();
			tglRujuk = rujukan.getTglKunjungan();
			asalRujukan = rujukan.getFaskes();
		}
		
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			Date date = new Date();
			
			String tgl = dateFormat.format(date);
			
			
			String input =  "{"
					+ "\"request\": { "
					+ "\"t_sep\": { "
					+ "\"noKartu\": \""+noKa+"\", "
					+ "\"tglSep\": \""+tgl+"\","
					+ "\"ppkPelayanan\": \"0038R091\", "
					+ "\"jnsPelayanan\": \""+jnsPelayanan+"\", "
					+ "\"klsRawat\": \""+klsRawat+"\", "
					+ "\"noMR\": \""+mrn+"\", "
					+ "\"rujukan\": { "
					+ "\"asalRujukan\": \""+asalRujukan+"\", "
					+ "\"tglRujukan\": \""+tglRujuk+"\", "
					+ "\"noRujukan\": \""+noRujuk+"\", "
					+ "\"ppkRujukan\": \""+asalRujuk+"\" "
					+ "}, "
					+ "\"catatan\": \""+catatan+"\", "
					+ "\"diagAwal\": \""+diagAwal+"\", "
					+ "\"poli\": {"
					+ "\"tujuan\": \""+poli+"\", "
					+ "\"eksekutif\": \"0\""
					+ "}, "
					+ "\"cob\": { "
					+ "\"cob\": \"0\""
					+ "}, "
					+ "\"jaminan\": { "
					+ "\"lakaLantas\": \"0\", "
					+ "\"penjamin\": \"-\", "
					+ "\"lokasiLaka\": \"-\""
					+ "}, "
					+ "\"noTelp\": \""+noHp+"\","
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "} ";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			result = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public enum BPJSVersion{
		Version1_0("1.0"),
		Version1_1("1.1");
		private String version;
		public String getVersion() {
			return version;
		}
		public void setVersion(String version) {
			this.version = version;
		}
		BPJSVersion(String version){
			this.version = version;
		}
		
		@Override
		public String toString() {
			return this.version;
		}
		
		public static Boolean isVersion(BPJSVersion version, String target) {
			return target == null ? false : version.toString().toLowerCase().equals(target.toLowerCase());
		}
	}
	
	private static String defaultResponse(String code, String message) {
		return " {           \"metaData\": {              \"code\": \""+code+"\",              \"message\": \""+message+"\"           },           \"response\": {           }        }";
	}
	
	public static String insertSepMtreg(InsertSepMtregParam param, Boolean faskes) {
		String result = defaultResponse("999", "Versi API BPJS tidak ditemukan. (internal)");
		try {
			if(BPJSVersion.isVersion(BPJSVersion.Version1_0, paramBpjsVersion))result = insertSepMtRegBpjsV1_0(param, faskes);
			else if(BPJSVersion.isVersion(BPJSVersion.Version1_1, paramBpjsVersion)) 
			{
				RegisterSepParam registerSepParam = new RegisterSepParam();
				registerSepParam.setNoKa(param.getNoKa());
				registerSepParam.setMrn(param.getMrn());
				registerSepParam.setJnsPelayanan(param.getJnsPelayanan());
				registerSepParam.setPoli(param.getPoli());
				registerSepParam.setCareProviderId(Long.valueOf(param.getCareProviderId()));
				registerSepParam.setFaskes(faskes ? "1" : "2");
				result = BpjsIntegration.SEP.Register(registerSepParam, Platform.SELF_REGISTRATION);
//						insertSepMtregBpjsV1_1(param, faskes);
			} 
		}catch(Exception ex) {
			System.out.print(ex.getMessage());
			ex.printStackTrace();
			result = defaultResponse("901", ex.getMessage());
		}
		
		return result;
	}
	

	
	public static String getMapHFISId(Long careProviderId) {
		String result = "";
		try {
			String query = "SELECT * FROM (SELECT HFIS_ID FROM HFISBPJS WHERE CAREPROVIDER_ID = ? AND DEFUNCT_IND = 'N' ORDER BY CREATED_DATETIME DESC) WHERE ROWNUM = 1";
			Object[] param = new Object[] {careProviderId};
			String rawData = (String) DbConnection.executeScalar(DbConnection.KTHIS, query, param);
			if(rawData == null)return result;
			result = rawData;
		}catch(Exception ex) {
			System.out.print(ex.getMessage());
			ex.printStackTrace();
		}
		return result;
	}
	
	
	public static String insertSepMtregBpjsV1_1(InsertSepMtregParam param, Boolean faskes) throws Exception{
		String result = "";
		if(param == null)return result;
		String url = BASE_URL+URL_INSERT_SEP;
		
		String noKa = param.getNoKa();
		String jnsPelayanan = param.getJnsPelayanan();
		String klsRawat = param.getKlsRawat();
		String mrn = param.getMrn(); 
		String asalRujuk = param.getAsalRujuk();
		String tglRujuk = param.getTglRujuk();
		String noRujuk = param.getNoRujuk();
		String catatan = param.getCatatan();
		String diagAwal = param.getDiagAwal();
		String poli = param.getPoli();
		String noHp = param.getNoHp();
		String user = param.getUser();
		String careProviderId = param.getCareProviderId();
		holderNoRujukan = noRujuk;
		holderTglRujukan = tglRujuk;
		
		// Faskes = true 	=> Faskes I.
		// Faskes = false	=> Faskes II.
		String jenisRujukan = faskes ? String.valueOf(1) : String.valueOf(2); 
		RujukanPcareOrRsByNoka.Rujukan rujukan =  !faskes ? getAsalRujukFaskes(noKa, faskes ? "1" : "2") : null;
		if(rujukan != null) {
			asalRujuk = rujukan.getProvPerujuk().getKode();
			noRujuk = rujukan.getNoKunjungan();
			tglRujuk = rujukan.getTglKunjungan();
			jenisRujukan = rujukan.getFaskes();
		}
		
		String dpjp = getMapHFISId(Long.valueOf(careProviderId));
		String lakaLantas =String.valueOf(0); //Self registrasi laka latas =0 (tidak terjadi kecelakaan)
		String Eks = String.valueOf(0); //Self registrasi pendaftar eksekutif = 0
		String penjamin = "";
		String tglKejadian = "";
		String ket = "";
		String suplesi = "0";
		String noSuplesi = "";
		String kdPro = "";
		String kdKab = "";
		String kdKec = "";
		String noSurat = getLastControlletter(mrn);
		if(TextUtils.isEmpty(dpjp))throw new Exception("DPJP tidak ditemukan, Silahkan hubungi CS.");
//		if(TextUtils.isEmpty(noSurat)) throw new Exception("Nomor Surat kunjungan terakhir belum terbit, Silahkan hubungi CS.");
		try {
			result = insertSep3(noKa,
					jnsPelayanan,
					klsRawat,
					mrn,
					jenisRujukan,
					tglRujuk,
					noRujuk,
					asalRujuk,
					catatan,
					diagAwal,
					poli,
					Eks,
					lakaLantas,
					penjamin,
					tglKejadian,
					ket,
					suplesi,
					noSuplesi,
					kdPro,
					kdKab,
					kdKec,
					noSurat,
					dpjp,
					noHp,
					user);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		}
		return result;
	}
	
	public static String max6DigitNumeric(String target){
		if(target == null)return target;
		String result = target;
		result = result.replaceAll("[\\D.]", "");
		if(result.length() >6){
			result = result.substring(result.length() - 6);
		}
		return result;
	}
	
	public static String getLastControlletter(String mrn) {
		String result = "";
		try {
			String query = "SELECT" +
					"	LETTER_NO " +
					"FROM" +
					"	(" +
					"	SELECT" +
					"		CL.LETTER_NO " +
					"	FROM" +
					"		CONTROLLETTER CL" +
					"		INNER JOIN VISIT VS ON CL.VISIT_ID = VS.VISIT_ID" +
					"		INNER JOIN PATIENT PT ON PT.PATIENT_ID = VS.PATIENT_ID " +
					"		AND PT.DEFUNCT_IND = 'N'" +
					"		INNER JOIN CARD CD ON CD.PERSON_ID = PT.PERSON_ID " +
					"		AND CD.CARD_NO = ? " +
					"		AND CD.DEFUNCT_IND = 'N' " +
					"	WHERE" +
					"		CL.DEFUNCT_IND = 'N' " +
					"	ORDER BY" +
					"		CL.CREATED_AT DESC " +
					"	) " +
					"WHERE" +
					"	ROWNUM =1";
			Object[] param = new Object[] {mrn};
			Object rawData = DbConnection.executeScalar(DbConnection.KTHIS, query, param);
			if(rawData != null) result = max6DigitNumeric(String.valueOf(rawData));
		}catch(Exception ex) {
			ex.fillInStackTrace();
			System.out.println(ex.getMessage());
		}
		return result;
	}


	public static String insertSep3(
			String noKa,
			String jnsPelayanan,
			String klsRawat,
			String mrn,
			String asalRujukan,
			String tglRujukan,
			String noRujukan,
			String asalPerujuk,
			String catatan,
			String diagAwal,
			String poli,
			String Eks,
			String lakaLantas,
			String penjamin,
			String tglKejadian,
			String ket,
			String suplesi,
			String noSuplesi,
			String kdPro,
			String kdKab,
			String kdKec,
			String noSurat,
			String dpjp,
			String noHp,
			String user){
		String url = BASE_URL+URL_INSERT_SEP11;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			String tgl = dateFormat.format(date);
			String input = "{"
					+ "\"request\": { "
					+ "\"t_sep\": { "
					+ "\"noKartu\": \""+noKa+"\", "
					+ "\"tglSep\": \""+tgl+"\", "
					+ "\"ppkPelayanan\": \"0038R091\", "
					+ "\"jnsPelayanan\": \""+jnsPelayanan+"\", "
					+ "\"klsRawat\": \""+klsRawat+"\", "
					+ "\"noMR\": \""+mrn+"\", "
					+ "\"rujukan\": { "
					+ "\"asalRujukan\": \""+asalRujukan+"\", "
					+ "\"tglRujukan\": \""+tglRujukan+"\", "
					+ "\"noRujukan\": \""+noRujukan+"\","
					+ "\"ppkRujukan\": \""+asalPerujuk+"\" "
					+ "}, "
					+ "\"catatan\": \""+catatan+"\", "
					+ "\"diagAwal\": \""+diagAwal+"\", "
					+ "\"poli\": { "
					+ "\"tujuan\": \""+poli+"\", "
					+ "\"eksekutif\": \""+Eks+"\" "
					+ "},"
					+ "\"cob\": { "
					+ "\"cob\": \"0\" "
					+ "}, "
					+ "\"katarak\": { "
					+ "\"katarak\": \"0\" "
					+ "}, "
					+ "\"jaminan\": { "
					+ "\"lakaLantas\": \""+lakaLantas+"\", "
					+ "\"penjamin\": { "
					+ "\"penjamin\": \""+penjamin+"\", "
					+ "\"tglKejadian\": \""+tglKejadian+"\", "
					+ "\"keterangan\": \""+ket+"\", "
					+ "\"suplesi\": { "
					+ "\"suplesi\": \""+suplesi+"\","
					+ "\"noSepSuplesi\": \""+noSuplesi+"\", "
					+ "\"lokasiLaka\": { "
					+ "\"kdPropinsi\": \""+kdPro+"\", "
					+ "\"kdKabupaten\": \""+kdKab+"\", "
					+ "\"kdKecamatan\": \""+kdKec+"\" "
					+ "} "
					+ "} "
					+ "} "
					+ "}, "
					+ "\"skdp\": { "
					+ "\"noSurat\": \""+noSurat+"\", "
					+ "\"kodeDPJP\": \""+dpjp+"\" "
					+ "}, "
					+ "\"noTelp\": \""+noHp+"\", "
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "} ";
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response == null ? null : response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String detailSep(String noSep) {
		String url = BASE_URL+URL_DETAIL_SEP+noSep;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String hapusSep(String noSep,String user){
		String url = BASE_URL+URL_HAPUS_SEP;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("DELETE");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"request\": { "
					+ "\"t_sep\": { "
					+ "\"noSep\": \""+noSep+"\", "
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "}";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String updateSep2(BpjsSepUpdateParam param) {
		String result = "";
		String url = BASE_URL+URL_UPDATE_SEP_1_1;
		
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("PUT");
			con.setRequestProperty("Content", "Application/x-www-form-urlencoded");
			con.setDoOutput(true);
			Gson gson = new Gson();
			
			String input =  gson.toJson(param);                          
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			result = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String updateSep(String noSep,String klsRawat,String mrn, String asalRujuk,
			String tglRujuk,String noRujuk,String catatan,String diagAwal,String noHp,String user) {
		String url = BASE_URL+URL_UPDATE_SEP;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("PUT");
			con.setDoOutput(true);
			
			String input =  " { "
					+ "\"request\": { "
					+ "\"t_sep\": { "
					+ "\"noSep\": \""+noSep+"\","
					+ "\"klsRawat\": \""+klsRawat+"\", "
					+ "\"noMR\": \""+mrn+"\", "
					+ "\"rujukan\": { "
					+ "\"asalRujukan\": \"1\", "
					+ "\"tglRujukan\": \""+tglRujuk+"\", "
					+ "\"noRujukan\": \""+noRujuk+"\", "
					+ "\"ppkRujukan\": \""+asalRujuk+"\" "
					+ "}, "
					+ "\"catatan\": \""+catatan+"\", "
					+ "\"diagAwal\": \""+diagAwal+"\","
					+ "\"poli\": { "
					+ "\"eksekutif\": \"0\""
					+ "},"
					+ "\"cob\": { "
					+ "\"cob\": \"0\" "
					+ "}, "
					+ "\"jaminan\": { "
					+ "\"lakaLantas\": \"0\", "
					+ "\"penjamin\": \"-\", "
					+ "\"lokasiLaka\": \"-\" "
					+ "}, "
					+ "\"noTelp\": \""+noHp+"\", "
					+ "\"user\": \""+user+"\" "
					+ "}"
					+ "}"
					+ "}";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String pengajuanSep(String noKa,String tglPelayanan,String jnsPelayanan,String keterangan,String user){
		String url = BASE_URL+URL_PENGAJUAN_SEP;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			String tgl = dateFormat.format(date);
			
			String input =  "{ "
					+ "\"request\": { "
					+ "\"t_sep\": { "
					+ "\"noKartu\": \""+noKa+"\",  "
					+ "\"tglSep\": \""+tglPelayanan+"\","
					+ "\"jnsPelayanan\": \""+jnsPelayanan+"\","
					+ "\"keterangan\": \""+keterangan+"\", "
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "}";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String approvalSep(String noKa,String tglPelayanan,String jnsPelayanan,String keterangan,String user){
		String url = BASE_URL+URL_APPROVAL_SEP;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			String tgl = dateFormat.format(date);
			
			String input =  "{ "
					+ "\"request\": { "
					+ "\"t_sep\": { "
					+ "\"noKartu\": \""+noKa+"\",  "
					+ "\"tglSep\": \""+tglPelayanan+"\","
					+ "\"jnsPelayanan\": \""+jnsPelayanan+"\","
					+ "\"keterangan\": \""+keterangan+"\", "
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "}";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String updateTglPlg(String noSep,String tglPulang,String user){
		String url = BASE_URL+URL_UPDATE_TGL_PLG;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("PUT");
			con.setDoOutput(true);
			
			String input =  "{   "
					+ "\"request\":  "
					+ "{    "
					+ "\"t_sep\": "
					+ "{"
					+ "\"noSep\":\""+noSep+"\", "
					+ "\"tglPulang\":\""+tglPulang+"\", "
					+ "\"user\":\""+user+"\" "
					+ "} "
					+ "} "
					+ "} ";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String integrasiInacbg(String noSep){
		String url = BASE_URL+URL_INTEGRASI_INACBG;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String insertRujukan(String noSep,String ppkDirujuk,String jnsPelayanan,String catatan,String diagRujukan,String tipeRujukan,String poliRujukan,String user){
		String url = BASE_URL+URL_INSERT_RUJUKAN;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			String tgl = dateFormat.format(date);
			
			String input =  "{ "
					+ "\"request\": { "
					+ "\"t_rujukan\": { "
					+ "\"noSep\": \""+noSep+"\", "
					+ "\"tglRujukan\": \""+tgl+"\", "
					+ "\"ppkDirujuk\": \""+ppkDirujuk+"\", "
					+ "\"jnsPelayanan\": \""+jnsPelayanan+"\", "
					+ "\"catatan\": \""+catatan+"\", "
					+ "\"diagRujukan\": \""+diagRujukan+"\", "
					+ "\"tipeRujukan\": \""+tipeRujukan+"\", "
					+ "\"poliRujukan\": \""+poliRujukan+"\", "
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "} ";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String updateRujukan(String noRujukan,String ppkDirujuk,String tipe,String jnsPelayanan,String catatan,String diagRujukan,String tipeRujukan,String poliRujukan,String user){
		String url = BASE_URL+URL_UPDATE_RUJUKAN;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("PUT");
			con.setDoOutput(true);
			
			String input =  "{ "
					+ "\"request\": { "
					+ "\"t_rujukan\": { "
					+ "\"noRujukan\": \""+noRujukan+"\", "
					+ "\"ppkDirujuk\": \""+ppkDirujuk+"\", "
					+ "\"tipe\": \""+tipe+"\", "
					+ "\"jnsPelayanan\": \""+jnsPelayanan+"\", "
					+ "\"catatan\": \""+catatan+"\", "
					+ "\"diagRujukan\": \""+diagRujukan+"\", "
					+ "\"tipeRujukan\": \""+tipeRujukan+"\", "
					+ "\"poliRujukan\": \""+poliRujukan+"\", "
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "} ";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String deleteRujukan(String noRujukan,String user){
		String url = BASE_URL+URL_DELETE_RUJUKAN;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("DELETE");
			con.setDoOutput(true);
			
			String input =  " { "
					+ "\"request\": { "
					+ "\"t_rujukan\": { "
					+ "\"noRujukan\": \""+noRujukan+"\", "
					+ "\"user\": \""+user+"\" "
					+ "} "
					+ "} "
					+ "} ";                                            
		  
		                    
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String dataKunjungan(String tglSep,String jnsPelayanan){
		String url = BASE_URL+URL_DATA_KUNJUNGAN;
		url = url.replaceAll("@@tglSep@@", tglSep);
		url = url.replaceAll("@@jnsPelayanan@@", jnsPelayanan);

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String dataKlaim(String tglPulang,String jnsPelayanan,String status) {
		String url = BASE_URL+URL_DATA_KLAIM;
		url = url.replaceAll("@@tglPulang@@", tglPulang);
		url = url.replaceAll("@@jnsPelayanan@@", jnsPelayanan);
		url = url.replaceAll("@@status@@", status);

		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String insertLpk(PengajuanKlaim pengajuanKlaim) {
		String url = BASE_URL+URL_INSERT_LPK;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			SetHeader(con);
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			String tgl = dateFormat.format(date);
			
			ArrayList<Diagnosa> diag = pengajuanKlaim.getDiagnosa();
			
			ArrayList<Procedure> prod = pengajuanKlaim.getProcedure();
			
			String add_input = "";
			for (Diagnosa dl : diag) {
				HashMap<String, String> diags = new HashMap<String,String>();
				diags.put("kode", dl.getKode());
				diags.put("level", dl.getLevel());
				add_input = add_input+input_post_json(diags)+",";
			}
			
			
			if (add_input != null && add_input.length() > 0 ) {
				add_input = add_input.substring(0, add_input.length() - 1);
		    }
			
			String text = "";
			
			for(Procedure pd : prod){
				HashMap<String, String> prods = new HashMap<String,String>();
				prods.put("kode", pd.getKode());
				text = text+input_post_json(prods)+",";
			}
			
			if (text != null && text.length() > 0 ) {
				text = text.substring(0, text.length() - 1);
		    }
		
			String input =  "{"
					+ "\"request\": { "
					+ "\"t_lpk\": { "
					+ "\"noSep\": \""+pengajuanKlaim.getNoSep()+"\", "
					+ "\"tglMasuk\": \""+pengajuanKlaim.getTglMasuk()+"\", "
					+ "\"tglKeluar\": \""+pengajuanKlaim.getTglKeluar()+"\", "
					+ "\"jaminan\": \"1\", "
					+ "\"poli\": { "
					+ "\"poli\": \""+pengajuanKlaim.getPoli()+"\" "
					+ "}, "
					+ "\"perawatan\": { "
					+ "\"ruangRawat\": \""+pengajuanKlaim.getRuangRawat()+"\", "
					+ "\"kelasRawat\": \""+pengajuanKlaim.getKelasRawat()+"\", "
					+ "\"spesialistik\": \""+pengajuanKlaim.getSpesialistik()+"\", "
					+ "\"caraKeluar\": \""+pengajuanKlaim.getCaraKeluar()+"\", "
					+ "\"kondisiPulang\": \""+pengajuanKlaim.getKondisiPulang()+"\" "
					+ "}, "
					+ "\"diagnosa\": [ "+add_input+" "
					+ "], "
					+ "\"procedure\": [ "+text+" "
					+ "], "
					+ "\"rencanaTL\": { "
					+ "\"tindakLanjut\": \""+pengajuanKlaim.getTindakLanjut()+"\", "
					+ "\"dirujukKe\": { "
					+ "\"kodePPK\": \"\" "
					+ "}, "
					+ "\"kontrolKembali\": { "
					+ "\"tglKontrol\": \""+pengajuanKlaim.getTglKontrol()+"\", "
					+ "\"poli\": \"\" "
					+ "} }, "
					+ "\"DPJP\": \""+pengajuanKlaim.getDpjp()+"\", "
					+ "\"user\": \""+pengajuanKlaim.getUser()+"\" "
					+ "} } } ";               
			
			
			
			System.out.println(input);
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(input);
			wr.flush();
			wr.close();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String dataFaskes(String tipe) {
		return dataFaskes("0038R091", tipe);
	}
	
	public static String dataFaskes(String kodeRS, String tipe) {
		String url = BASE_URL+URL_FASKES;
		if(TextUtils.isEmpty(kodeRS) && TextUtils.isEmpty(tipe)) {
			url = BASE_URL+"referensi/faskes";
		}else {
			url = url.replaceAll("@@koders@@", kodeRS);
			url = url.replaceAll("@@tipe@@", tipe);
		}
		
		
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String diagnosa(String diag) {
		String url = BASE_URL+URL_DIAGNOSA;
		url = url.replaceAll("@@diag@@", diag);

		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	public static String input_post_json(Map<String,String> params){
		String input = "{";
		try {
			Iterator it = params.entrySet().iterator();
		    while (it.hasNext()) {
		    	if (!input.equals("{")) {
		    		input = input.concat(",");
				}
		        Map.Entry pair = (Map.Entry)it.next();
		        input = input.concat("\""+pair.getKey()+"\":\""+pair.getValue()+"\"");
		    }       
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	    input = input.concat("}");
		return input;
	}
	
	
	/* ---------- Andy Three S ----*/
	
	public static String getRujukanbyNmrRujukan(String nmrRujukan) {
		String url = BASE_URL+URL_GET_RUJUKAN_NORUJUKAN+nmrRujukan;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	public static String getRujukanRSbyNmrRujukan(String nmrRujukan) {
		String url = BASE_URL+URL_GET_RUJUKANRS_NORUJUKAN+nmrRujukan;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getRujukanbyNoka(String noKa) {
		String url = BASE_URL+URL_GET_RUJUKAN_NOKA+noKa;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getRujukanRSbyNoka(String noKa) {
		String url = BASE_URL+URL_GET_RUJUKANRS_NOKA+noKa;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getRujukanPcareorRSbyNoka(String noKa){
		String result = "";
		Gson gson = new Gson();
		result = gson.toJson(getRujukanVclaimTerbaru(noKa));
		return result;
	}
	
	public static String getRujukanListPcareorRSbyNoka(String noKa){
		String result = "";
		Gson gson = new Gson();
		result = gson.toJson(getRujukanVclaimTerbaruFromList(noKa));
		return result;
	}
	
	public static BpjsRujukanResponse generateRujukan(String noKa, String faskes) {
		BpjsRujukanResponse result = null;
		try {
			BpjsRujukanResponse generateRujukan = new BpjsRujukanResponse();
			String ppkrujukan = "0038R091";
			String diagAwal = "Z04.8";
			String klsrawat = "";
			String tr = faskes;
			String asalFaskes = "";
			String tglrujuk = "";
			String norujuk = "";
			String getBpjsinfo =  GetBpjsInfobyNoka(noKa);
			Gson gson = new Gson();
			BpjsInfoResponse memberInfo = gson.fromJson(getBpjsinfo, BpjsInfoResponse.class);
			if(memberInfo == null) result = BpjsRujukanResponse.fail("Gagal reqeust member info, silahkan coba lagi.");
			String code2 = memberInfo.getMetadata().getCode();
    		String message2 = memberInfo.getMetadata().getMessage();
			
			if(code2.equals("200")&&message2.equals("OK"))
			{
				klsrawat = memberInfo.getResponse().getPeserta().getHakKelas().getKode();
				asalFaskes =  memberInfo.getResponse().getPeserta().getProvUmum().getNmProvider();
				tglrujuk = getServerDateString();
				String bulan = getServerMonthString();
				norujuk = ppkrujukan+bulan+getNomorRujukanTerakhir();
				BpjsRujukanResponse.ResponseBuilder builder = new BpjsRujukanResponse.ResponseBuilder();
				builder.setDiagnosa(Info.from(diagAwal, diagAwal));
				builder.setPeserta(memberInfo.getResponse().getPeserta());
				if(!faskes.equals("1")) {
					builder.getPeserta().getProvUmum().setKdProvider(ppkrujukan);
					builder.getPeserta().getProvUmum().setNmProvider("RS. Murni Teguh Medan");
					
				}
				builder.setTglKunjungan(tglrujuk);
				builder.setNoKunjungan(norujuk);
				result = BpjsRujukanResponse.success(builder);
				result.getResponse().getRujukan().setFaskes(faskes);
				if(!faskes.equals("1")) {
					result.getResponse().getRujukan().setProvPerujuk(Info.from(ppkrujukan, "RS. Murni Teguh Medan"));
				}else {
					result.getResponse().getRujukan().setProvPerujuk(Info.from(builder.getPeserta().getProvUmum().getKdProvider(), java.net.URLEncoder.encode("RS. Murni Teguh Medan", "UTF-8")));
				}
			}else result = BpjsRujukanResponse.fail(code2, message2);
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		return result;
	}
	
	private static String getNomorRujukanTerakhir() throws SQLException, Exception {
		String result = "Y006669";
		String month = getServerMonthOnlyString();
		String year = getServerYearOnlyString();
		String query = "SELECT TO_NUMBER(COUNT(*)) AS HASIL FROM BPJS_RUJUKAN WHERE EXTRACT(MONTH FROM TGL_RUJUKAN) = ? AND EXTRACT(YEAR FROM TGL_RUJUKAN) = ?";
		Object rawData = DbConnection.executeScalar(DbConnection.KTHIS, query, new Object[] {month, year});
		if(rawData == null)return result;
		if(!(rawData instanceof BigDecimal))return result;
		Integer target = ((BigDecimal)rawData).intValueExact();
		if(target != null)target++;
		if(target.toString().length() >6){
			result = "Y" + target.toString().substring(target.toString().length() - 6);
		}else result = "Y" + String.format("%06d",  target);
		return result;
	}
	
	private static BpjsRujukanResponse getRujukanVclaimTerbaru(String noKa) {
		BpjsRujukanResponse result = null;
		Gson gson = new Gson();
		String rujukanPcare = getRujukanbyNoka(noKa);
		String rujukanRs = getRujukanRSbyNoka(noKa); 
		String tglPcare = null;
		String tglRs = null;
		BpjsRujukanResponse dataRujukanPcare = gson.fromJson(rujukanPcare, BpjsRujukanResponse.class);
		Metadata metadataPcare = dataRujukanPcare.getMetaData();		
		if(metadataPcare.getCode().equals("200") && metadataPcare.getMessage().equals("OK")) {			
			Rujukan item = dataRujukanPcare.getResponse().getRujukan();
			if(item.getTglKunjungan() != null && item.getTglKunjungan() != ""){
				tglPcare = item.getTglKunjungan();
			}			
		}
		gson = new Gson();
		BpjsRujukanResponse dataRujukanRs = gson.fromJson(rujukanRs, BpjsRujukanResponse.class);
		Metadata metadataRs = dataRujukanRs.getMetaData();		
		if(metadataRs.getCode().equals("200") && metadataRs.getMessage().equals("OK")) {			
			Rujukan item = dataRujukanRs.getResponse().getRujukan();
			if(item.getTglKunjungan() != null && item.getTglKunjungan() != ""){
				tglRs = item.getTglKunjungan();
			}			
		}
		if(tglPcare != null && tglRs == null){
			dataRujukanPcare.getResponse().getRujukan().setFaskes("1");
			result = dataRujukanPcare;
		} else if(tglPcare != null && tglRs != null){
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			//SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		    Date datePcare;
		    Date dateRs;
			try {
				datePcare = df.parse(tglPcare);
				dateRs = df.parse(tglRs);
			    int comparison = dateRs.compareTo(datePcare);
			    if(comparison >= 0) {
			    	dataRujukanRs.getResponse().getRujukan().setFaskes("2");
			    	result = dataRujukanRs;
			    } else {
			    	dataRujukanRs.getResponse().getRujukan().setFaskes("1");
			    	result = dataRujukanPcare;
			    }
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			if(tglRs != null)dataRujukanRs.getResponse().getRujukan().setFaskes("2");
			result = dataRujukanRs;
		}
		return result;
	}
	
	private static BpjsRujukanResponse getRujukanVclaimTerbaruFromList(String noKa) {
		BpjsRujukanResponse result = null;
		Gson gson = new Gson();
		String rujukanPcare = getRujukanListbyNoka(noKa);
		String rujukanRs = getRujukanRSListbyNoka(noKa); 
//		String rujukanRs = "{\"metaData\": {\"code\": \"200\",\"message\": \"OK\"},\"response\": {\"rujukan\": [  {\"diagnosa\": {\"kode\": \"K31.4\",\"nama\": null},\"keluhan\": null,\"noKunjungan\": \"0207R0031118B000028\",\"pelayanan\": {\"kode\": \"1\",\"nama\": \"Rawat Inap\"},\"peserta\": {\"cob\": {\"nmAsuransi\": null,\"noAsuransi\": null,\"tglTAT\": null,\"tglTMT\": null},\"hakKelas\": {\"keterangan\": \"KELAS II\",\"kode\": \"2\"},\"informasi\": {\"dinsos\": null,\"noSKTM\": null,\"prolanisPRB\": null},\"jenisPeserta\": {\"keterangan\": \"PENERIMA PENSIUN SWASTA\",\"kode\": \"25\"},\"mr\": {\"noMR\": \"1712135798\",\"noTelepon\": \"081265100553\"},\"nama\": \"LENA SIRAIT\",\"nik\": \"1208204107500012\",\"noKartu\": \"0001492963086\",\"pisa\": \"3\",\"provUmum\": {\"kdProvider\": \"0029B017\",\"nmProvider\": \"KLINIK PUSKESBUN SIDAMANIK\"},\"sex\": \"P\",\"statusPeserta\": {\"keterangan\": \"AKTIF\",\"kode\": \"0\"},\"tglCetakKartu\": \"2018-11-17\",\"tglLahir\": \"1950-07-01\",\"tglTAT\": \"2051-01-01\",\"tglTMT\": \"2016-10-18\",\"umur\": {\"umurSaatPelayanan\": \"68 tahun ,4 bulan ,16 hari\",\"umurSekarang\": \"68 tahun ,5 bulan ,18 hari\"}},\"poliRujukan\": {\"kode\": \"\",\"nama\": \"\"},\"provPerujuk\": {\"kode\": \"0207R003\",\"nama\": \"Rumah Sakit Balimbingan PTP IV\"},\"tglKunjungan\": \"2018-11-17\"},  {\"diagnosa\": {\"kode\": \"C16.9\",\"nama\": null},\"keluhan\": null,\"noKunjungan\": \"0207R0031218B000014\",\"pelayanan\": {\"kode\": \"2\",\"nama\": \"Rawat Jalan\"},\"peserta\": {\"cob\": {\"nmAsuransi\": null,\"noAsuransi\": null,\"tglTAT\": null,\"tglTMT\": null},\"hakKelas\": {\"keterangan\": \"KELAS II\",\"kode\": \"2\"},\"informasi\": {\"dinsos\": null,\"noSKTM\": null,\"prolanisPRB\": null},\"jenisPeserta\": {\"keterangan\": \"PENERIMA PENSIUN SWASTA\",\"kode\": \"25\"},\"mr\": {\"noMR\": \"1712135798\",\"noTelepon\": \"081265100553\"},\"nama\": \"LENA SIRAIT\",\"nik\": \"1208204107500012\",\"noKartu\": \"0001492963086\",\"pisa\": \"3\",\"provUmum\": {\"kdProvider\": \"0029B017\",\"nmProvider\": \"KLINIK PUSKESBUN SIDAMANIK\"},\"sex\": \"P\",\"statusPeserta\": {\"keterangan\": \"AKTIF\",\"kode\": \"0\"},\"tglCetakKartu\": \"2018-11-17\",\"tglLahir\": \"1950-07-01\",\"tglTAT\": \"2051-01-01\",\"tglTMT\": \"2016-10-18\",\"umur\": {\"umurSaatPelayanan\": \"68 tahun ,5 bulan ,11 hari\",\"umurSekarang\": \"68 tahun ,5 bulan ,18 hari\"}},\"poliRujukan\": {\"kode\": \"018\",\"nama\": \"BEDAH DIGESTIF \"},\"provPerujuk\": {\"kode\": \"0207R003\",\"nama\": \"Rumah Sakit Balimbingan PTP IV\"},\"tglKunjungan\": \"2018-12-12\"}]}}";
		String tglPcare = null;
		String tglRs = null;
		BpjsRujukanListResponse dataRujukanPcareList = gson.fromJson(rujukanPcare, BpjsRujukanListResponse.class);
		Metadata metadataPcare = dataRujukanPcareList.getMetaData();
		BpjsRujukanResponse dataRujukanPcare = null;
		if(metadataPcare.getCode().equals("200") && metadataPcare.getMessage().equals("OK")) {
			ArrayList<Rujukan> listItem= dataRujukanPcareList.getResponse().getRujukan();
			if(listItem != null) System.out.print("Rujukan count faskes I : " + listItem.size() +" items." );
			if(listItem.size() > 0) {
				Rujukan[] arrItem = listItem.toArray(new Rujukan[listItem.size()]);
				Arrays.sort(arrItem);
				Rujukan item = arrItem[0];
				if(item.getTglKunjungan() != null && item.getTglKunjungan() != ""){
					tglPcare = item.getTglKunjungan();
					dataRujukanPcare = BpjsRujukanResponse.success(item);
				}
			}
		}
		gson = new Gson();
		BpjsRujukanListResponse dataRujukanRsList = gson.fromJson(rujukanRs, BpjsRujukanListResponse.class);
		Metadata metadataRs = dataRujukanRsList.getMetaData();		
		BpjsRujukanResponse dataRujukanRs = null;
		if(metadataRs.getCode().equals("200") && metadataRs.getMessage().equals("OK")) {			
			ArrayList<Rujukan> listItem= dataRujukanRsList.getResponse().getRujukan();
			if(listItem != null) System.out.print("Rujukan count faskes II : " + listItem.size() +" items." );
			if(listItem.size() > 0) {
				Rujukan[] arrItem = listItem.toArray(new Rujukan[listItem.size()]);
				Arrays.sort(arrItem);
				Rujukan item = arrItem[0];
				if(item.getTglKunjungan() != null && item.getTglKunjungan() != ""){
					tglRs = item.getTglKunjungan();
					dataRujukanRs =  BpjsRujukanResponse.success(item);
				}	
			}		
		}
		if(tglPcare != null && tglRs == null){
			dataRujukanPcare.getResponse().getRujukan().setFaskes("1");
			result = dataRujukanPcare;
		} else if(tglPcare != null && tglRs != null){
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			//SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date datePcare;
			Date dateRs;
			try {
				datePcare = df.parse(tglPcare);
				dateRs = df.parse(tglRs);
				int comparison = dateRs.compareTo(datePcare);
				if(comparison >= 0) {
					dataRujukanRs.getResponse().getRujukan().setFaskes("2");
					result = dataRujukanRs;
				} else {
					dataRujukanRs.getResponse().getRujukan().setFaskes("1");
					result = dataRujukanPcare;
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			if(tglRs != null)dataRujukanRs.getResponse().getRujukan().setFaskes("2");
			result = dataRujukanRs;
		}
		return result;
	}
	
	private static String getServerDateTimeString() {
		String result = "";
		try {
            Date now = Calendar.getInstance().getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //$NON-NLS-1$
            result = formatter.format(now);
        } catch (Exception e) {
            return null;
        }
		return result;
	}
	
	private static String getServerDateString() {
		String result = "";
		try {
			Date now = Calendar.getInstance().getTime();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$
			result = formatter.format(now);
		} catch (Exception e) {
			return null;
		}
		return result;
	}
	
	private static String getServerMonthOnlyString() {
		String result = "";
		try {
			Date now = Calendar.getInstance().getTime();
			SimpleDateFormat formatter = new SimpleDateFormat("MM"); //$NON-NLS-1$
			result = formatter.format(now);
		} catch (Exception e) {
			return null;
		}
		return result;
	}
	
	private static String getServerYearOnlyString() {
		String result = "";
		try {
			Date now = Calendar.getInstance().getTime();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy"); //$NON-NLS-1$
			result = formatter.format(now);
		} catch (Exception e) {
			return null;
		}
		return result;
	}
	
	private static String getServerMonthString() {
        try {
            Date now = Calendar.getInstance().getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("MMyy"); //$NON-NLS-1$

            return formatter.format(now);
        } catch (Exception e) {
            return null;
        }
    }
	
	
	public static String getRujukanListbyNoka(String noKa) {
		String url = BASE_URL+URL_GET_RUJUKAN_LIST_NOKA+noKa;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getRujukanRSListbyNoka(String noKa) {
		String url = BASE_URL+URL_GET_RUJUKANRS_LIST_NOKA+noKa;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getRujukanbyTglRujukan(String tglRujukan) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");		
		Date tglRujuk = dateFormat.parse(tglRujukan);		
		String tgl = dateFormat.format(tglRujuk);
		String url = BASE_URL+URL_GET_RUJUKAN_TGLRUJUKAN+tgl;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getRujukanRSbyTglRujukan(String tglRujukan) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date tglRujuk = dateFormat.parse(tglRujukan);		
		String tgl = dateFormat.format(tglRujuk);
		String url = BASE_URL+URL_GET_RUJUKANRS_TGLRUJUKAN+tgl;
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getReferensiProcedure(String kode, String nama) {
		String url = BASE_URL+URL_GET_REFERENSI_PROCEDURE;
		if(kode != null)
		{
			url += kode;
		} else {
			url += nama;
		}
		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
		
	public static String getReferensiKelasRawat() {
		String url = BASE_URL+URL_GET_REFERENSI_KELAS_RAWAT;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getReferensiDokter(String namaDokter) {
		String nama = namaDokter;
		String url = BASE_URL+URL_GET_REFERENSI_DOKTER+nama;		
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getReferensiSpesialistik() {
		String url = BASE_URL+URL_GET_REFERENSI_SPESIALISTIK;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getReferensiPoli(String kodename) {
		String url = BASE_URL+URL_GET_REFERENSI_POLI;
		String s = "";
		try {
			url = url.replaceAll("@@kodeNama@@", URLEncoder.encode(kodename, "UTF-8"));
			url = url.replace("+", "%20");
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getReferensiRuangRawat() {
		String url = BASE_URL+URL_GET_REFERENSI_RUANG_RAWAT;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getReferensiCaraKeluar() {
		String url = BASE_URL+URL_GET_REFERENSI_CARA_KELUAR;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getReferensiPascaPulang() {
		String url = BASE_URL+URL_GET_REFERENSI_PASCA_PULANG;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getReferensiPropinsi() {
		String url = BASE_URL+URL_GET_REFERENSI_PROPINSI;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getReferensiKabupaten(String kodePropinsi) {
		String url = BASE_URL+URL_GET_REFERENSI_KABUPATEN+kodePropinsi;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getReferensiKecamatan(String kodeKabupaten) {
		String url = BASE_URL+URL_GET_REFERENSI_KECAMATAN+kodeKabupaten;
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String getSuplesiJasaRaharja(String noBPJS,String tglSEP) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date tglLayanan = dateFormat.parse(tglSEP);		
		String tgl = dateFormat.format(tglLayanan);
		
		String url = BASE_URL+URL_GET_SUPLESI_JASARAHARJA;
		url = url.replaceAll("@@noBPJS@@", noBPJS);
		url = url.replaceAll("@@tglSEP@@", tgl);
		String s = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			TrustModifier.relaxHostChecking(con);
			con.setRequestMethod("GET");
			SetHeader(con);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response.toString());
			s = response.toString();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
}
