package com.rsmurniteguh.webservice.dep.kthis.services;

import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.rsmurniteguh.webservice.dep.all.model.AntrianDispance;
import com.rsmurniteguh.webservice.dep.all.model.CurrentDatetim;
import com.rsmurniteguh.webservice.dep.all.model.Dispensedetailinfo;
import com.rsmurniteguh.webservice.dep.all.model.DrugDispense;
import com.rsmurniteguh.webservice.dep.all.model.InfoCekRem;
import com.rsmurniteguh.webservice.dep.all.model.InfoRekonObat;
import com.rsmurniteguh.webservice.dep.all.model.InfoRekonObatForCat;
import com.rsmurniteguh.webservice.dep.all.model.InfoTxnDateTime;
import com.rsmurniteguh.webservice.dep.all.model.Infostockreceipt;
import com.rsmurniteguh.webservice.dep.all.model.ListDataPasienSms;
import com.rsmurniteguh.webservice.dep.all.model.ListDispanceCount;
import com.rsmurniteguh.webservice.dep.all.model.ListGetHasBPJS;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoBalance;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoCek;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoKaryawan;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoTransaksi;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoViewTrx;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoViewTrxNew;
import com.rsmurniteguh.webservice.dep.all.model.ListInfoViewTrxNewDetail;
import com.rsmurniteguh.webservice.dep.all.model.ListKamar;
import com.rsmurniteguh.webservice.dep.all.model.ListQueNum;
import com.rsmurniteguh.webservice.dep.all.model.ListgetPharPulvis;
import com.rsmurniteguh.webservice.dep.all.model.PrepareDatetime;
import com.rsmurniteguh.webservice.dep.all.model.ReturnResult;
import com.rsmurniteguh.webservice.dep.all.model.StoreDispance;
import com.rsmurniteguh.webservice.dep.all.model.StoreDispance2;
import com.rsmurniteguh.webservice.dep.all.model.VisitNoIp;
import com.rsmurniteguh.webservice.dep.all.model.viewJournal;
import com.rsmurniteguh.webservice.dep.all.model.viewJournal2;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;


import oracle.net.aso.n;

import com.rsmurniteguh.webservice.dep.all.model.QueuePhar;

import java.util.List;
import java.util.Locale;

public class StoreDispanceSer extends DbConnection {

	public static List<DrugDispense> getdrugdispense(String drugdispense_id) {

		Connection connection = null;
		   Statement stmt = null;
		   ResultSet rs = null;
		
		List<DrugDispense> all=new ArrayList<DrugDispense>();
		connection=DbConnection.getPooledConnection();
		if(connection == null)return null;
		if(drugdispense_id == null)
		{			
			String abcarpro="select * from(select dd.DRUGDISPENSE_ID, v.visit_id, v.QUEUE_NO, dd.PREPARE_BATCH_NO, dd.prepared_datetime,"
					+ "dd.DISPENSE_BATCH_NO, dd.dispensed_datetime ,dd.RECEIVE_STATUS, dd.RECEIVE_DATETIME, b.bill_status, ROW_NUMBER() OVER (PARTITION BY dd.VISIT_ID ORDER BY dd.PREPARE_BATCH_NO ASC) AS rn "
					+ "from DRUGDISPENSE  dd inner join visit v on v.VISIT_ID = dd.VISIT_ID inner JOIN PATIENTACCOUNT pa on pa.VISIT_ID = v.VISIT_ID "
					+ "left outer join bill b on b.PATIENTACCOUNT_ID = pa.PATIENTACCOUNT_ID and dd.RECEIVE_STATUS = 'RECEIVE' "
					+ "where to_char(PREPARED_DATETIME, 'ddmmyyyy') = to_char(sysdate, 'ddmmyyyy') and v.PATIENT_TYPE = 'PTY2' "
					+ "order by to_number(v.QUEUE_NO, '9999'))";
			try {
				stmt=connection.createStatement();
				rs=stmt.executeQuery(abcarpro);
				while(rs.next())
					{
						DrugDispense cp=new DrugDispense();
						cp.setDRUGDISPENSE_ID(rs.getString("DRUGDISPENSE_ID"));
						cp.setVisit_id(rs.getString("visit_id"));
						cp.setQUEUE_NO(rs.getString("QUEUE_NO"));
						cp.setPREPARE_BATCH_NO(rs.getString("PREPARE_BATCH_NO"));
						cp.setPrepared_datetime(rs.getString("prepared_datetime"));
						cp.setDISPENSE_BATCH_NO(rs.getString("DISPENSE_BATCH_NO"));
						cp.setDispensed_datetime(rs.getString("dispensed_datetime"));
						cp.setRECEIVE_STATUS(rs.getString("RECEIVE_STATUS"));
						cp.setRECEIVE_DATETIME(rs.getString("RECEIVE_DATETIME"));
						cp.setBill_status(rs.getString("bill_status"));
						cp.setRn(rs.getString("rn"));
						all.add(cp);
					}
				
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		}
		else
		{
			String abcarpro="select * from(select dd.DRUGDISPENSE_ID, v.visit_id, v.QUEUE_NO, dd.PREPARE_BATCH_NO, dd.prepared_datetime,"
					+ "dd.DISPENSE_BATCH_NO, dd.dispensed_datetime ,dd.RECEIVE_STATUS, dd.RECEIVE_DATETIME, b.bill_status, ROW_NUMBER() OVER (PARTITION BY dd.VISIT_ID ORDER BY dd.PREPARE_BATCH_NO ASC) AS rn "
					+ "from DRUGDISPENSE  dd inner join visit v on v.VISIT_ID = dd.VISIT_ID inner JOIN PATIENTACCOUNT pa on pa.VISIT_ID = v.VISIT_ID "
					+ "left outer join bill b on b.PATIENTACCOUNT_ID = pa.PATIENTACCOUNT_ID and dd.RECEIVE_STATUS = 'RECEIVE'  "
					+ "and v.PATIENT_TYPE = 'PTY2' "
					+ "where DRUGDISPENSE_ID = ("+drugdispense_id+"))";
			try {
				stmt=connection.createStatement();
				rs=stmt.executeQuery(abcarpro);
				while(rs.next())
					{
						DrugDispense cp=new DrugDispense();
						cp.setDRUGDISPENSE_ID(rs.getString("DRUGDISPENSE_ID"));
						cp.setVisit_id(rs.getString("visit_id"));
						cp.setQUEUE_NO(rs.getString("QUEUE_NO"));
						cp.setPREPARE_BATCH_NO(rs.getString("PREPARE_BATCH_NO"));
						cp.setPrepared_datetime(rs.getString("prepared_datetime"));
						cp.setDISPENSE_BATCH_NO(rs.getString("DISPENSE_BATCH_NO"));
						cp.setDispensed_datetime(rs.getString("dispensed_datetime"));
						cp.setRECEIVE_STATUS(rs.getString("RECEIVE_STATUS"));
						cp.setRECEIVE_DATETIME(rs.getString("RECEIVE_DATETIME"));
						cp.setBill_status(rs.getString("bill_status"));
						cp.setRn(rs.getString("rn"));
						all.add(cp);
					}
				
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		}
		return all;
	}
	
	
	public static List<DrugDispense> getdrugdispensevisit(String visit_id, String prepared_datetime) {

		Connection connection = null;
		   Statement stmt = null;
		   ResultSet rs = null;
		
		List<DrugDispense> all=new ArrayList<DrugDispense>();
		connection=DbConnection.getPooledConnection();
		if(connection == null)return null;
		if(visit_id == null)
		{			
			String abcarpro="select * from(select dd.DRUGDISPENSE_ID, v.visit_id, MM.MATERIAL_ITEM_NAME, v.QUEUE_NO, dd.PREPARE_BATCH_NO, dd.prepared_datetime,"
					+ "dd.DISPENSE_BATCH_NO, dd.dispensed_datetime, dd.RECEIVE_STATUS, dd.RECEIVE_DATETIME, b.bill_status, ROW_NUMBER() OVER (PARTITION BY dd.VISIT_ID ORDER BY dd.PREPARE_BATCH_NO ASC) AS rn "
					+ "from DRUGDISPENSE  dd inner join visit v on v.VISIT_ID = dd.VISIT_ID inner JOIN PATIENTACCOUNT pa on pa.VISIT_ID = v.VISIT_ID INNER JOIN MATERIALTXN MT ON MT.MATERIALTXN_ID = dd.MATERIALTXN_ID INNER JOIN MATERIALITEMMSTR MM ON MM.MATERIALITEMMSTR_ID = MT.MATERIALITEMMSTR_ID "
					+ "left outer join bill b on b.PATIENTACCOUNT_ID = pa.PATIENTACCOUNT_ID and dd.RECEIVE_STATUS = 'RECEIVE'"
					+ "where PREPARED_DATETIME BETWEEN to_date('"+prepared_datetime+" 00:00:00','dd/mm/yyyy HH24:MI:SS') and to_date('"+prepared_datetime+" 23:59:59','dd/mm/yyyy HH24:MI:SS') and v.PATIENT_TYPE = 'PTY2' "
					+ "order by to_number(v.QUEUE_NO, '9999'))";
			try {
				stmt=connection.createStatement();
				rs=stmt.executeQuery(abcarpro);
				while(rs.next())
					{
						DrugDispense cp=new DrugDispense();
						cp.setDRUGDISPENSE_ID(rs.getString("DRUGDISPENSE_ID"));
						cp.setVisit_id(rs.getString("visit_id"));
						cp.setMATERIAL_ITEM_NAME(rs.getString("MATERIAL_ITEM_NAME"));
						cp.setQUEUE_NO(rs.getString("QUEUE_NO"));
						cp.setPREPARE_BATCH_NO(rs.getString("PREPARE_BATCH_NO"));
						cp.setPrepared_datetime(rs.getString("prepared_datetime"));
						cp.setDISPENSE_BATCH_NO(rs.getString("DISPENSE_BATCH_NO"));
						cp.setDispensed_datetime(rs.getString("dispensed_datetime"));
						cp.setRECEIVE_STATUS(rs.getString("RECEIVE_STATUS"));
						cp.setRECEIVE_DATETIME(rs.getString("RECEIVE_DATETIME"));
						cp.setBill_status(rs.getString("bill_status"));
						cp.setRn(rs.getString("rn"));
						all.add(cp);
					}
				
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		}
		else
		{
			String abcarpro="select * from(select dd.DRUGDISPENSE_ID, v.visit_id, MM.MATERIAL_ITEM_NAME, v.QUEUE_NO, dd.PREPARE_BATCH_NO, dd.prepared_datetime,"
					+ "dd.DISPENSE_BATCH_NO, dd.dispensed_datetime , dd.RECEIVE_STATUS, dd.RECEIVE_DATETIME, b.bill_status, ROW_NUMBER() OVER (PARTITION BY dd.VISIT_ID ORDER BY dd.PREPARE_BATCH_NO ASC) AS rn "
					+ "from DRUGDISPENSE  dd inner join visit v on v.VISIT_ID = dd.VISIT_ID inner JOIN PATIENTACCOUNT pa on pa.VISIT_ID = v.VISIT_ID INNER JOIN MATERIALTXN MT ON MT.MATERIALTXN_ID = dd.MATERIALTXN_ID INNER JOIN MATERIALITEMMSTR MM ON MM.MATERIALITEMMSTR_ID = MT.MATERIALITEMMSTR_ID "
					+ "left outer join bill b on b.PATIENTACCOUNT_ID = pa.PATIENTACCOUNT_ID and dd.RECEIVE_STATUS = 'RECEIVE'"
					+ "where PREPARED_DATETIME BETWEEN to_date('"+prepared_datetime+" 00:00:00','dd/mm/yyyy HH24:MI:SS') and to_date('"+prepared_datetime+" 23:59:59','dd/mm/yyyy HH24:MI:SS') and v.PATIENT_TYPE = 'PTY2' "
					+ "order by to_number(v.QUEUE_NO, '9999')) where visit_id = ("+visit_id+")";
			try {
				stmt=connection.createStatement();
				rs=stmt.executeQuery(abcarpro);
				while(rs.next())
					{
						DrugDispense cp=new DrugDispense();
						cp.setDRUGDISPENSE_ID(rs.getString("DRUGDISPENSE_ID"));
						cp.setVisit_id(rs.getString("visit_id"));
						cp.setMATERIAL_ITEM_NAME(rs.getString("MATERIAL_ITEM_NAME"));
						cp.setQUEUE_NO(rs.getString("QUEUE_NO"));
						cp.setPREPARE_BATCH_NO(rs.getString("PREPARE_BATCH_NO"));
						cp.setPrepared_datetime(rs.getString("prepared_datetime"));
						cp.setDISPENSE_BATCH_NO(rs.getString("DISPENSE_BATCH_NO"));
						cp.setDispensed_datetime(rs.getString("dispensed_datetime"));
						cp.setRECEIVE_STATUS(rs.getString("RECEIVE_STATUS"));
						cp.setRECEIVE_DATETIME(rs.getString("RECEIVE_DATETIME"));
						cp.setBill_status(rs.getString("bill_status"));
						cp.setRn(rs.getString("rn"));
						all.add(cp);
					}
				
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		}
		return all;
	}
	
	public static List<DrugDispense> updatedrugdispense(String drugdispense_id) {

		Connection connection = null;
		   Statement stmt = null;
		   ResultSet rs = null;
		   int rs1 = 0;
		   PreparedStatement preparedStmt;
		List<DrugDispense> all=new ArrayList<DrugDispense>();
		connection=DbConnection.getPooledConnection();
//		
		
		if(drugdispense_id != null)
		{			
			String abcarpro= "UPDATE DRUGDISPENSE SET RECEIVE_STATUS = 'RECEIVE', RECEIVE_DATETIME = (current_timestamp) WHERE DRUGDISPENSE_ID = ?"; 
			try {
				preparedStmt = connection.prepareStatement(abcarpro);
				preparedStmt.setBigDecimal(1, new BigDecimal(drugdispense_id));
				preparedStmt.executeUpdate();
				connection.commit();
//				stmt=connection.createStatement();
//				stmt.set
//				rs1=stmt.executeUpdate(abcarpro);
				
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			finally {
			     if (rs != null) try  { rs.close(); } catch (Exception ignore){}
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		}
		return all;
	}
	

	
	
	public static List<StoreDispance> getDispanceBatch(String dispancebano) {
		List<StoreDispance> all=new ArrayList<StoreDispance>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="select dd.dispense_batch_no, mtx.storemstr_id, mtx.materialitemmstr_id, mm.material_item_name, dd.dispense_qty, cm.code_desc, "
				+ " v.patient_class from drugdispense dd inner join materialtxn mtx on mtx.materialtxn_id = dd.materialtxn_id "
				+ " inner join visit v on v.visit_id = dd.visit_id inner join materialitemmstr mm on mm.materialitemmstr_id = mtx.materialitemmstr_id"
				+ " inner join codemstr cm on cm.code_cat = substr(dd.dispense_uom, 0, 3) and cm.code_abbr = substr(dd.dispense_uom, 4, 6) "
				+ " where dd.dispense_batch_no = '"+dispancebano+"'";
		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				StoreDispance sdis=new StoreDispance();
				sdis.setBatchNo(hasdis.getString("dispense_batch_no"));
				sdis.setStoreMaster(hasdis.getBigDecimal("storemstr_id"));
				sdis.setMaterialitemmstr_id(hasdis.getBigDecimal("materialitemmstr_id"));
				sdis.setMaterial_item_name(hasdis.getString("material_item_name"));
				sdis.setQty(hasdis.getBigDecimal("dispense_qty"));
				sdis.setPatient_class(hasdis.getString("patient_class"));
				sdis.setUom(hasdis.getString("code_desc"));
				all.add(sdis);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<StoreDispance> getDispanceBatch2(String dispancebano2, String patienttype, String patientclass) {
		List<StoreDispance> all=new ArrayList<StoreDispance>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="select dd.dispense_batch_no, mtx.storemstr_id, mtx.materialitemmstr_id, mm.material_item_name, dd.dispense_qty, cm.code_desc, v.patient_class from drugdispense dd inner join "
				+ " materialtxn mtx on mtx.materialtxn_id = dd.materialtxn_id inner join visit v on v.visit_id = dd.visit_id inner join materialitemmstr mm on mm.materialitemmstr_id = mtx.materialitemmstr_id"
				+ " inner join codemstr cm on cm.code_cat = substr(dd.dispense_uom, 0, 3) and cm.code_abbr = substr(dd.dispense_uom, 4, 6) where to_number(dispense_batch_no) > "+dispancebano2+" and "
				+ " v.patient_type = '"+patienttype+"' and v.patient_class = '"+patientclass+"'order by dd.dispensed_datetime";
		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				StoreDispance sdis=new StoreDispance();
				sdis.setBatchNo(hasdis.getString("dispense_batch_no"));
				sdis.setStoreMaster(hasdis.getBigDecimal("storemstr_id"));
				sdis.setMaterialitemmstr_id(hasdis.getBigDecimal("materialitemmstr_id"));
				sdis.setMaterial_item_name(hasdis.getString("material_item_name"));
				sdis.setQty(hasdis.getBigDecimal("dispense_qty"));
				sdis.setUom(hasdis.getString("code_desc"));
				sdis.setPatient_class(hasdis.getString("patient_class"));
				all.add(sdis);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<StoreDispance> getdrugstore(String drugreturnno, String patienttype, String patientclass) {
				List<StoreDispance> all=new ArrayList<StoreDispance>();
				Connection connection=DbConnection.getPooledConnection();
				ResultSet hasdis=null;
				if(connection == null)return null;
				Statement storedis=null;
				String abdis="select dr.drugreturn_no, mtx.storemstr_id, mtx.materialitemmstr_id, mm.material_item_name, dd.dispense_qty, cm.code_desc, v.patient_class from drugreturn dr"
						+ " inner join drugdispense dd  on dr.drugdispense_id = dd.drugdispense_id inner join materialtxn mtx on mtx.materialtxn_id = dr.materialtxn_id "
						+ " inner join visit v on v.visit_id = dd.visit_id inner join materialitemmstr mm on mm.materialitemmstr_id = mtx.materialitemmstr_id "
						+ " inner join codemstr cm on cm.code_cat = substr(dr.return_uom, 0, 3) and cm.code_abbr = substr(dr.return_uom, 4, 6) where to_number(dr.drugreturn_no) > "+drugreturnno+""
						+ " and v.patient_type = '"+patienttype+"' and v.patient_class = '"+patientclass+"' order by dr.return_datetime";
				try {
					storedis=connection.createStatement();
					hasdis=storedis.executeQuery(abdis);
					while (hasdis.next())
					{
						StoreDispance sdis=new StoreDispance();
						sdis.setBatchNo(hasdis.getString("drugreturn_no"));
						sdis.setStoreMaster(hasdis.getBigDecimal("storemstr_id"));
						sdis.setMaterialitemmstr_id(hasdis.getBigDecimal("materialitemmstr_id"));
						sdis.setMaterial_item_name(hasdis.getString("material_item_name"));
						sdis.setQty(hasdis.getBigDecimal("dispense_qty"));
						sdis.setUom(hasdis.getString("code_desc"));
						sdis.setPatient_class(hasdis.getString("patient_class"));						
						
						all.add(sdis);
					}
					hasdis.close();
					storedis.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
				finally {
				     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
				     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
				     if (connection!=null) try { connection.close();} catch (Exception ignore){}
				   }
				return all;
	}

	public static List<StoreDispance2> getdispensebatchno(String dispensebatchno) {
		List<StoreDispance2> all=new ArrayList<StoreDispance2>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="select dd.DISPENSED_DATETIME, v.VISIT_NO, p_1.PERSON_NAME as patient_name, pgpmi.fxDisplayAge(p_1.BIRTH_DATE) as age, PGCOMMON.fxGetCodeDesc(p_1.sex) as sex, p_1.BIRTH_DATE, "
				+ "pgPMI.fxGetMRN(v.PATIENT_ID, 'MRTOP') as MRTOP, oei.FORM_NO, p_2.PERSON_NAME as care_name, sspm.SUBSPECIALTY_DESC, oe.ENTERED_DATETIME, mim.MATERIAL_ITEM_NAME, "
				+ "vm.VENDOR_NAME as VENDOR_CODE, "
				+ "miu.ITEM_UOM_DESC as spec, dd.DISPENSE_QTY, pgCommon.fxGetCodeDesc(miu.ITEM_UOM_CODE) as itemuomcode, "
				+ "DECODE(mt.RETAIL_PRICE, TRUNC(mt.RETAIL_PRICE, 2), TO_CHAR(mt.RETAIL_PRICE, 'FM99999999990.00'), "
				+ "TO_CHAR(mt.RETAIL_PRICE, 'FM99999999990.9999')) AS PRICE, mt.RETAIL_PRICE AS price_Amt, "
				+ "DECODE(mt.TXN_TOTAL_PRICE, TRUNC(mt.TXN_TOTAL_PRICE, 2), TO_CHAR(mt.TXN_TOTAL_PRICE, 'FM99999999990.00'), "
				+ "TO_CHAR(mt.TXN_TOTAL_PRICE, 'FM99999999990.9999')) AS money, mt.TXN_TOTAL_PRICE AS money_Amt, "
				+ "mt.TXN_TOTAL_PRICE as amt, pgCommon.fxGetCodeDesc(mt.RETAIL_PRICE_UOM) as retail_uom ,fm.frequency_desc  "
				+ ",pgcommon.fxGetCodeDesc(oe.Duration_Qty) as Duration_Qty ,dm.duration_desc ,pgcommon.fxGetCodeDesc(oei.ROA) as ROA ,"
				+ "oei.dosage_qty||pgcommon.fxGetCodeDesc(oei.dosage_unit) as dosage_desc , "
				+ "pgcommon.fxGetCodeDesc(oei.dosage_unit) as dosage_unit "
				+ " from drugdispense dd inner join orderconfirmation oc on oc.ORDERCONFIRMATION_ID = dd.ORDERCONFIRMATION_ID "
				+ "inner join orderentryitem oei on oei.ORDERENTRYITEM_ID = oc.ORDERENTRYITEM_ID "
				+ "inner join orderentry oe on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID inner join visit v on v.VISIT_ID = oe.VISIT_ID "
				+ "inner join patient pt on pt.PATIENT_ID = v.PATIENT_ID "
				+ "inner join person p_1 on p_1.PERSON_ID = pt.PERSON_ID "
				+ "inner join careprovider cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID "
				+ "inner join person p_2 on p_2.PERSON_ID = cp.PERSON_ID "
				+ "inner join subspecialtymstr sspm on sspm.SUBSPECIALTYMSTR_ID = oe.SUBSPECIALTYMSTR_ID "
				+ "inner join materialitemmstr mim on mim.MATERIALITEMMSTR_ID = dd.MATERIALITEMMSTR_ID "
				+ "inner join materialitemuom miu on miu.MATERIALITEMMSTR_ID = mim.MATERIALITEMMSTR_ID and miu.ITEM_UOM_CODE = dd.DISPENSE_UOM "
				+ "left outer join vendormstr vm on vm.VENDORMSTR_ID = mim.manufacturer_vendormstr_id "
				+ "inner join materialtxn mt on mt.MATERIALTXN_ID = dd.MATERIALTXN_ID "
				+ "left outer join FREQUENCYMSTR fm on oe.FREQUENCYMSTR_ID = fm.frequencymstr_id "
				+ "left outer join DURATIONMSTR dm on oe.DURATIONMSTR_ID = dm.durationmstr_id where dd.DISPENSE_BATCH_NO = '"+dispensebatchno+"' ORDER BY OEI.FORM_NO";

		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				StoreDispance2 sdis=new StoreDispance2();
				sdis.setDispensedDateTime(hasdis.getDate("DISPENSED_DATETIME"));
				sdis.setVisit_No(hasdis.getString("VISIT_NO"));
				sdis.setPatientName(hasdis.getString("patient_name"));
				sdis.setAge(hasdis.getString("age"));
				sdis.setSex(hasdis.getString("sex"));
				sdis.setBirthDate(hasdis.getDate("BIRTH_DATE"));
				sdis.setMrtop(hasdis.getString("MRTOP"));
				sdis.setFormNo(hasdis.getString("FORM_NO"));
				sdis.setPersonName(hasdis.getString("care_name"));
				sdis.setSubspecialitydesc(hasdis.getString("SUBSPECIALTY_DESC"));
				sdis.setEnteredDatetime(hasdis.getDate("ENTERED_DATETIME"));
				sdis.setMaterial_item_name(hasdis.getString("MATERIAL_ITEM_NAME"));
				sdis.setVendorName(hasdis.getString("VENDOR_CODE"));
				sdis.setItemUomDesc(hasdis.getString("spec"));
				sdis.setDispenseQty(hasdis.getBigDecimal("DISPENSE_QTY"));
				sdis.setItemuoncode(hasdis.getString("itemuomcode"));
				
				sdis.setPrice(hasdis.getString("PRICE"));
				sdis.setPrice_amt(hasdis.getBigDecimal("price_Amt"));
				
				
				sdis.setMoney(hasdis.getString("money"));
				sdis.setMoney_Amt(hasdis.getBigDecimal("money_Amt"));
				sdis.setAmt(hasdis.getBigDecimal("amt"));
				sdis.setRetail_uom(hasdis.getString("retail_uom"));
				sdis.setFreq_desc(hasdis.getString("frequency_desc"));
				sdis.setDurationQty(hasdis.getBigDecimal("Duration_Qty"));
				sdis.setDurationDesc(hasdis.getString("duration_desc"));
				sdis.setROA(hasdis.getString("ROA"));	

				sdis.setDosagedesc(hasdis.getString("dosage_desc"));	
				sdis.setDosageUnit(hasdis.getString("dosage_unit"));
				all.add(sdis);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<AntrianDispance> getstoremstrid(String storemstrid) {
		List<AntrianDispance> all=new ArrayList<AntrianDispance>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="SELECT OC.CONFIRMED_DATETIME AS VALID_DATETIME, P.PERSON_ID, PT.PATIENT_ID, P.PERSON_NAME, OEI.ORDERENTRYITEM_ID, OEI.Orderentry_Id, CARD.CARD_NO "
				+ "FROM ORDERCONFIRMATION OC, ORDERENTRYITEM OEI, ORDERENTRY OE, VISIT V,PATIENT PT, PERSON P, CARD CARD, ORDERPERFORMLOCATIONMSTR OPLM, STOREMSTR SM "
				+ "WHERE OC.CONFIRMED_DATETIME > TRUNC(SYSDATE) - 0.5 AND OC.BILL_MODE = 'BLMD' AND OC.PERFORM_LOCATION_MODE = 'PLM1' AND OC.ORDER_STATUS <> 'OSTCAN' "
				+ "AND OEI.ORDERENTRYITEM_ID = OC.ORDERENTRYITEM_ID AND OEI.ORDER_STATUS <> 'OSTCAN' AND OEI.SELF_PREPARED_IND = 'N' AND OE.ORDERENTRY_ID = OEI.ORDERENTRY_ID "
				+ "AND OE.ORDER_STATUS <> 'OSTCAN' AND V.VISIT_ID = OE.VISIT_ID "
                + "AND V.PATIENT_TYPE IN ( SELECT CM.PARENT_CODE_CAT || CM.CODE_ABBR FROM CODEMSTR CM WHERE CM.CODE_CAT = 'OPT' AND CM.DEFUNCT_IND = 'N' ) "
                + "AND v.patient_class <> 'PTC114' AND PT.PATIENT_ID = V.PATIENT_ID AND P.PERSON_ID = PT.PERSON_ID AND CARD.PERSON_ID(+) = P.PERSON_ID AND CARD.CARD_STATUS(+) = 'CDSVLD' "
                + "AND NOT EXISTS ( SELECT 1 FROM DRUGDISPENSE DD WHERE DD.ORDERCONFIRMATION_ID = OC.ORDERCONFIRMATION_ID AND DD.DISPENSE_STATUS = 'OSTDIS' ) "
                + "AND OPLM.ORDERPERFORMLOCATIONMSTR_ID = OC.ORDERPERFORMINGLOCATIONMSTR_ID AND SM.LOCATIONMSTR_ID = OPLM.LOCATIONMSTR_ID "
                + "AND ( ( INSTR(',' || 'OP_MIX_DRUGDISPENSE_STORE_LIST' || ',', ',' || SM.STORE_CODE || ',') > 0 AND "
                + "INSTR(',' || 'OP_MIX_DRUGDISPENSE_STORE_LIST' || ',', ',' || SM.STORE_CODE || ',') > 0 ) OR "
                + "( INSTR(',' || 'OP_MIX_DRUGDISPENSE_STORE_LIST' || ',', ',' || SM.STORE_CODE || ',') <= 0 AND SM.STOREMSTR_ID = '"+storemstrid+"' ))";

		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				AntrianDispance sdis=new AntrianDispance();
				sdis.setConfirmedDatetime(hasdis.getDate("VALID_DATETIME"));
				sdis.setPersonId(hasdis.getLong("PERSON_ID"));
				sdis.setPatientId(hasdis.getLong("PATIENT_ID"));
				sdis.setPersonName(hasdis.getString("PERSON_NAME"));
				sdis.setOrderEntryItemId(hasdis.getString("ORDERENTRYITEM_ID"));
				sdis.setOrderEntryId(hasdis.getString("Orderentry_Id"));
				sdis.setCardNo(hasdis.getString("CARD_NO"));				
				all.add(sdis);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<Dispensedetailinfo> getdrugdispensedetailinfo(String dispensebatchno) {
		List<Dispensedetailinfo> all=new ArrayList<Dispensedetailinfo>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="select to_char(dd.DISPENSED_DATETIME, 'dd/mm/yyyy HH24:MI:SS') as DISPENSED_DATETIME, v.VISIT_NO, to_char(v.ADMISSION_DATETIME, 'dd/mm/yyyy HH24:MI:SS') AS ADMISSION_DATETIME, p_1.PERSON_NAME As patient_name, pgpmi.fxDisplayAge(p_1.BIRTH_DATE)as age, PGCOMMON.fxGetCodeDesc(p_1.sex) as sex, "
				+ " to_char(p_1.BIRTH_DATE, 'dd/mm/yyyy HH24:MI:SS') AS BIRTH_DATE, pgPMI.fxGetMRN(v.PATIENT_ID, 'MRTOP')as MRTOP, oei.FORM_NO, p_2.PERSON_NAME as care_name, sspm.SUBSPECIALTY_DESC, to_char(oe.ENTERED_DATETIME, 'dd/mm/yyyy HH24:MI:SS') AS ENTERED_DATETIME , "
				+ "mim.MATERIAL_ITEM_NAME, vm.VENDOR_NAME as VENDOR_CODE, miu.ITEM_UOM_DESC as spec, dd.DISPENSE_QTY, pgCommon.fxGetCodeDesc(miu.ITEM_UOM_CODE)as itemuomcode, "
				+ "DECODE(mt.RETAIL_PRICE,TRUNC(mt.RETAIL_PRICE, 2),TO_CHAR(mt.RETAIL_PRICE, 'FM99999999990.00'),TO_CHAR(mt.RETAIL_PRICE, 'FM99999999990.9999')) AS PRICE, "
				+ "mt.RETAIL_PRICE AS price_Amt, DECODE(-mt.TXN_TOTAL_PRICE, TRUNC(-mt.TXN_TOTAL_PRICE, 2),TO_CHAR(-mt.TXN_TOTAL_PRICE, 'FM99999999990.00'),"
				+ "TO_CHAR(-mt.TXN_TOTAL_PRICE, 'FM99999999990.9999')) AS money, -mt.TXN_TOTAL_PRICE AS money_Amt, -mt.TXN_TOTAL_PRICE as amt, "
				+ "pgCommon.fxGetCodeDesc(mt.RETAIL_PRICE_UOM)as retail_uom ,fm.frequency_desc, pgcommon.fxGetCodeDesc(oe.Duration_Qty)as Duration_Qty ,dm.duration_desc "
				+ ",pgcommon.fxGetCodeDesc(oei.ROA)as ROA ,oei.dosage_qty||pgcommon.fxGetCodeDesc(oei.dosage_unit) as dosage_desc, pgcommon.fxGetCodeDesc(oei.dosage_unit)as dosage_unit, v.QUEUE_NO, dd.PREPARE_BATCH_NO "
				+ "from drugdispense dd,orderconfirmation oc, orderentryitem oei, orderentry oe,visit v,patient pt,person p_1,careprovider cp,person p_2, subspecialtymstr sspm, "
				+ "materialitemmstr mim, materialitemuom miu, vendormstr vm, materialtxn mt, FREQUENCYMSTR fm, DURATIONMSTR dm "
				+ "where dd.PREPARE_BATCH_NO = '"+dispensebatchno+"' and oc.ORDERCONFIRMATION_ID = dd.ORDERCONFIRMATION_ID and oei.ORDERENTRYITEM_ID = oc.ORDERENTRYITEM_ID "
				+ "and oe.ORDERENTRY_ID = oei.ORDERENTRY_ID and v.VISIT_ID = oe.VISIT_ID and pt.PATIENT_ID = v.PATIENT_ID and p_1.PERSON_ID = pt.PERSON_ID "
				+ "and cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID and p_2.PERSON_ID = cp.PERSON_ID and sspm.SUBSPECIALTYMSTR_ID = oe.SUBSPECIALTYMSTR_ID "
				+ "and mim.MATERIALITEMMSTR_ID = dd.MATERIALITEMMSTR_ID and miu.MATERIALITEMMSTR_ID = mim.MATERIALITEMMSTR_ID and miu.ITEM_UOM_CODE = dd.DISPENSE_UOM "
				+ "and vm.VENDORMSTR_ID(+) = mim.MANUFACTURER_VENDORMSTR_ID and mt.MATERIALTXN_ID = dd.MATERIALTXN_ID and oe.FREQUENCYMSTR_ID = fm.frequencymstr_id(+) "
				+ "and oe.DURATIONMSTR_ID = dm.durationmstr_id(+) ORDER BY OEI.FORM_NO";

		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				Dispensedetailinfo sdis=new Dispensedetailinfo();
				sdis.setPREPARE_BATCH_NO(hasdis.getString("PREPARE_BATCH_NO"));
				sdis.setDispensedDateTime(hasdis.getString("DISPENSED_DATETIME"));
				sdis.setVisit_No(hasdis.getString("VISIT_NO"));		
				sdis.setVisit_Date(hasdis.getString("ADMISSION_DATETIME"));
				sdis.setPatientName(hasdis.getString("patient_name"));
				sdis.setAge(hasdis.getString("age"));
				sdis.setSex(hasdis.getString("sex"));
				sdis.setBirthDate(hasdis.getString("BIRTH_DATE"));
				sdis.setMrtop(hasdis.getString("MRTOP"));
				sdis.setFormNo(hasdis.getString("FORM_NO"));
				sdis.setPersonName(hasdis.getString("care_name"));
				sdis.setSubspecialitydesc(hasdis.getString("SUBSPECIALTY_DESC"));
				sdis.setEnteredDatetime(hasdis.getString("ENTERED_DATETIME"));
				sdis.setMaterial_item_name(hasdis.getString("MATERIAL_ITEM_NAME"));
				sdis.setVendorName(hasdis.getString("VENDOR_CODE"));
				sdis.setItemUomDesc(hasdis.getString("spec"));
				sdis.setDispenseQty(hasdis.getBigDecimal("DISPENSE_QTY"));
				sdis.setItemuoncode(hasdis.getString("itemuomcode"));
				sdis.setPrice(hasdis.getString("PRICE"));
				sdis.setPrice_amt(hasdis.getBigDecimal("price_Amt"));
				sdis.setMoney(hasdis.getString("money"));
				sdis.setMoney_Amt(hasdis.getBigDecimal("money_Amt"));
				sdis.setAmt(hasdis.getBigDecimal("amt"));
				sdis.setRetail_uom(hasdis.getString("retail_uom"));
				sdis.setFreq_desc(hasdis.getString("frequency_desc"));
				sdis.setDurationQty(hasdis.getBigDecimal("Duration_Qty"));
				sdis.setDurationDesc(hasdis.getString("duration_desc"));
				sdis.setROA(hasdis.getString("ROA"));
				sdis.setDosagedesc(hasdis.getString("dosage_desc"));
				sdis.setDosageUnit(hasdis.getString("dosage_unit"));
				sdis.setQUEUE_NO(hasdis.getString("QUEUE_NO"));
				all.add(sdis);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	
	public static List<Dispensedetailinfo> getdrugdispensedetailinfo2(String dispensebatchno) {
		List<Dispensedetailinfo> all=new ArrayList<Dispensedetailinfo>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="select to_char(dd.DISPENSED_DATETIME, 'dd/mm/yyyy HH24:MI:SS') as DISPENSED_DATETIME, v.VISIT_NO, p_1.PERSON_NAME As patient_name, pgpmi.fxDisplayAge(p_1.BIRTH_DATE)as age, PGCOMMON.fxGetCodeDesc(p_1.sex) as sex, "
				+ " to_char(p_1.BIRTH_DATE, 'dd/mm/yyyy HH24:MI:SS') AS BIRTH_DATE, pgPMI.fxGetMRN(v.PATIENT_ID, 'MRTOP')as MRTOP, oei.FORM_NO, p_2.PERSON_NAME as care_name, sspm.SUBSPECIALTY_DESC, to_char(oe.ENTERED_DATETIME, 'dd/mm/yyyy HH24:MI:SS') AS ENTERED_DATETIME , "
				+ "mim.MATERIAL_ITEM_NAME, vm.VENDOR_NAME as VENDOR_CODE, miu.ITEM_UOM_DESC as spec, dd.DISPENSE_QTY, pgCommon.fxGetCodeDesc(miu.ITEM_UOM_CODE)as itemuomcode, "
				+ "DECODE(mt.RETAIL_PRICE,TRUNC(mt.RETAIL_PRICE, 2),TO_CHAR(mt.RETAIL_PRICE, 'FM99999999990.00'),TO_CHAR(mt.RETAIL_PRICE, 'FM99999999990.9999')) AS PRICE, "
				+ "mt.RETAIL_PRICE AS price_Amt, DECODE(-mt.TXN_TOTAL_PRICE, TRUNC(-mt.TXN_TOTAL_PRICE, 2),TO_CHAR(-mt.TXN_TOTAL_PRICE, 'FM99999999990.00'),"
				+ "TO_CHAR(-mt.TXN_TOTAL_PRICE, 'FM99999999990.9999')) AS money, -mt.TXN_TOTAL_PRICE AS money_Amt, -mt.TXN_TOTAL_PRICE as amt, "
				+ "pgCommon.fxGetCodeDesc(mt.RETAIL_PRICE_UOM)as retail_uom ,fm.frequency_desc, pgcommon.fxGetCodeDesc(oe.Duration_Qty)as Duration_Qty ,dm.duration_desc "
				+ ",pgcommon.fxGetCodeDesc(oei.ROA)as ROA ,oei.dosage_qty||pgcommon.fxGetCodeDesc(oei.dosage_unit) as dosage_desc, pgcommon.fxGetCodeDesc(oei.dosage_unit)as dosage_unit, v.QUEUE_NO, dd.PREPARE_BATCH_NO "
				+ "from drugdispense dd,orderconfirmation oc, orderentryitem oei, orderentry oe,visit v,patient pt,person p_1,careprovider cp,person p_2, subspecialtymstr sspm, "
				+ "materialitemmstr mim, materialitemuom miu, vendormstr vm, materialtxn mt, FREQUENCYMSTR fm, DURATIONMSTR dm "
				+ "where dd.dispense_batch_no = '"+dispensebatchno+"' and oc.ORDERCONFIRMATION_ID = dd.ORDERCONFIRMATION_ID and oei.ORDERENTRYITEM_ID = oc.ORDERENTRYITEM_ID "
				+ "and oe.ORDERENTRY_ID = oei.ORDERENTRY_ID and v.VISIT_ID = oe.VISIT_ID and pt.PATIENT_ID = v.PATIENT_ID and p_1.PERSON_ID = pt.PERSON_ID "
				+ "and cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID and p_2.PERSON_ID = cp.PERSON_ID and sspm.SUBSPECIALTYMSTR_ID = oe.SUBSPECIALTYMSTR_ID "
				+ "and mim.MATERIALITEMMSTR_ID = dd.MATERIALITEMMSTR_ID and miu.MATERIALITEMMSTR_ID = mim.MATERIALITEMMSTR_ID and miu.ITEM_UOM_CODE = dd.DISPENSE_UOM "
				+ "and vm.VENDORMSTR_ID(+) = mim.MANUFACTURER_VENDORMSTR_ID and mt.MATERIALTXN_ID = dd.MATERIALTXN_ID and oe.FREQUENCYMSTR_ID = fm.frequencymstr_id(+) "
				+ "and oe.DURATIONMSTR_ID = dm.durationmstr_id(+) ORDER BY OEI.FORM_NO";

		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				Dispensedetailinfo sdis=new Dispensedetailinfo();
				sdis.setPREPARE_BATCH_NO(hasdis.getString("PREPARE_BATCH_NO"));
				sdis.setDispensedDateTime(hasdis.getString("DISPENSED_DATETIME"));
				sdis.setVisit_No(hasdis.getString("VISIT_NO"));		
				sdis.setPatientName(hasdis.getString("patient_name"));
				sdis.setAge(hasdis.getString("age"));
				sdis.setSex(hasdis.getString("sex"));
				sdis.setBirthDate(hasdis.getString("BIRTH_DATE"));
				sdis.setMrtop(hasdis.getString("MRTOP"));
				sdis.setFormNo(hasdis.getString("FORM_NO"));
				sdis.setPersonName(hasdis.getString("care_name"));
				sdis.setSubspecialitydesc(hasdis.getString("SUBSPECIALTY_DESC"));
				sdis.setEnteredDatetime(hasdis.getString("ENTERED_DATETIME"));
				sdis.setMaterial_item_name(hasdis.getString("MATERIAL_ITEM_NAME"));
				sdis.setVendorName(hasdis.getString("VENDOR_CODE"));
				sdis.setItemUomDesc(hasdis.getString("spec"));
				sdis.setDispenseQty(hasdis.getBigDecimal("DISPENSE_QTY"));
				sdis.setItemuoncode(hasdis.getString("itemuomcode"));
				sdis.setPrice(hasdis.getString("PRICE"));
				sdis.setPrice_amt(hasdis.getBigDecimal("price_Amt"));
				sdis.setMoney(hasdis.getString("money"));
				sdis.setMoney_Amt(hasdis.getBigDecimal("money_Amt"));
				sdis.setAmt(hasdis.getBigDecimal("amt"));
				sdis.setRetail_uom(hasdis.getString("retail_uom"));
				sdis.setFreq_desc(hasdis.getString("frequency_desc"));
				sdis.setDurationQty(hasdis.getBigDecimal("Duration_Qty"));
				sdis.setDurationDesc(hasdis.getString("duration_desc"));
				sdis.setROA(hasdis.getString("ROA"));
				sdis.setDosagedesc(hasdis.getString("dosage_desc"));
				sdis.setDosageUnit(hasdis.getString("dosage_unit"));
				sdis.setQUEUE_NO(hasdis.getString("QUEUE_NO"));
				all.add(sdis);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	public static List<CurrentDatetim> getCurrentDatetime() {
		List<CurrentDatetim> all=new ArrayList<CurrentDatetim>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="select to_char(sysdate, 'dd/mm/yyyy HH24:MI:SS') as tanggal from dual";

		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				CurrentDatetim sdis=new CurrentDatetim();
				sdis.setTanggal(hasdis.getString("tanggal"));
				all.add(sdis);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<PrepareDatetime> getprepareddatetime(String firstdate, String lastdate) {
		List<PrepareDatetime> all=new ArrayList<PrepareDatetime>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		ResultSet rs=null;
		if(connection == null)return null;
		Statement storedis=null;
		Statement sm=null;
		String abdis="select dd.DISPENSE_BATCH_NO ,lm.location_name, um.user_name as doctor, ps1.person_name as patient, oc.confirmed_datetime as resep, "
				+ "dd.PREPARED_DATETIME as dipersiapkan, dd.DISPENSED_DATETIME as selesai "
				+ "from DRUGDISPENSE dd inner join visit v on v.VISIT_ID = dd.VISIT_ID inner join orderconfirmation oc on oc.orderconfirmation_id = dd.orderconfirmation_id "
				+ "inner join patient pt on pt.patient_id = v.patient_id inner join person ps1 on ps1.person_id = pt.person_id "
				+ "inner join usermstr um on um.usermstr_id = oc.confirmed_by inner join locationmstr lm on lm.locationmstr_id = oc.confirmed_locationmstr_id "
				+ "where v.PATIENT_TYPE='PTY2' and v.PATIENT_CLASS NOT IN ('PTC114') and dd.drugdispense_id in ((select (select min(drugdispense_id) "
				+ "from DRUGDISPENSE where DISPENSE_BATCH_NO = dbatch.DISPENSE_BATCH_NO) as id from (select distinct(dd1.DISPENSE_BATCH_NO) from DRUGDISPENSE dd1 "
				+ "where dd1.PREPARED_DATETIME BETWEEN to_date('"+firstdate+" 00:00:00','dd/mm/yyyy HH24:MI:SS') and to_date('"+lastdate+" 23:59:59','dd/mm/yyyy HH24:MI:SS')) dbatch))order by dd.DISPENSE_BATCH_NO";
		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{ 
				String dispensebatchno = hasdis.getString("DISPENSE_BATCH_NO");
				String sqlCek ="SELECT JLH FROM  (SELECT ORDERENTRY_ID,COUNT(*) AS JLH FROM "
						+ "( SELECT OE.ORDERENTRY_ID FROM DRUGDISPENSE DD "
						+ " INNER JOIN PLANNEDEXECUTION PD ON DD.ORDERCONFIRMATION_ID = PD.ORDERCONFIRMATION_ID "
						+ " INNER JOIN ORDERENTRYITEM OE ON PD.ORDERENTRYITEM_ID = OE.ORDERENTRYITEM_ID "
						+ " WHERE DD.DISPENSE_BATCH_NO = '"+dispensebatchno+"' order by OE.ORDERENTRY_ID) "
						+ " GROUP BY ORDERENTRY_ID HAVING COUNT(*) > 1)";
				sm=connection.createStatement();
				rs=sm.executeQuery(sqlCek);
				PrepareDatetime sdis=new PrepareDatetime();
				if(rs.next()){
					if(rs.getString("JLH") == null){
						sdis.setDispenseBatchNo(hasdis.getString("DISPENSE_BATCH_NO"));
						sdis.setLocationName(hasdis.getString("location_name"));
						sdis.setDoctor(hasdis.getString("doctor"));
						sdis.setPatientName(hasdis.getString("patient"));
						sdis.setResep(hasdis.getString("resep"));
						sdis.setPreparedDatetime(hasdis.getString("dipersiapkan"));
						sdis.setDispenseDatetime(hasdis.getString("selesai"));
						all.add(sdis);
					}
				} else {
					sdis.setDispenseBatchNo(hasdis.getString("DISPENSE_BATCH_NO"));
					sdis.setLocationName(hasdis.getString("location_name"));
					sdis.setDoctor(hasdis.getString("doctor"));
					sdis.setPatientName(hasdis.getString("patient"));
					sdis.setResep(hasdis.getString("resep"));
					sdis.setPreparedDatetime(hasdis.getString("dipersiapkan"));
					sdis.setDispenseDatetime(hasdis.getString("selesai"));
					all.add(sdis);
				}
				
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (sm!=null) try  { sm.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<InfoTxnDateTime> getTxnDateTime(String first, String last, String codedesc, String codedesc2) {
		Connection connection = null;
		   Statement st = null;
		   ResultSet rs = null;
		
		List<InfoTxnDateTime> all=new ArrayList<InfoTxnDateTime>();
		connection=DbConnection.getPooledConnection();
		if(connection == null)return null;
		String abdis="SELECT v2.VISIT_ID, v2.ADMISSION_DATETIME, v2.DISCHARGE_DATETIME, cm1.CODE_DESC as patient_type, cm2.CODE_DESC as patient_class, pat2.TXN_DATETIME, "
				+ "IHIS.PGCOMMON.fxGetCodeDesc(iscm2.ip_receipt_item_cat) as receipt, iscm2.ITEMCATEGORYMSTR_ID, iscm2.ITEM_SUBCAT_DESC, tcm.TXN_DESC, cim2.GL_ACCOUNT, pat2.TXN_AMOUNT, "
				+ "pat2.DISCOUNT_AMOUNT, pat2.CLAIMABLE_PERCENT, ps.PERSON_NAME, mrn.card_no as medrec "
				+ "FROM IHIS.visit v2, IHIS.patientaccount pa2, IHIS.patientaccounttxn pat2, IHIS.chargeitemmstr cim2, PATIENT pt, PERSON ps, "
				+ "IHIS.itemsubcategorymstr iscm2, IHIS.CODEMSTR cm1, IHIS.CODEMSTR cm2, IHIS.TXNCODEMSTR tcm, card mrn "
				+ "WHERE pat2.TXN_DATETIME BETWEEN TO_DATE('"+first+" 00:00:01', 'yyyy/mm/dd HH24:MI:SS') and TO_DATE ('"+last+" 23:59:59', 'yyyy/mm/dd HH24:MI:SS') and cm1.CODE_CAT = substr(V2.PATIENT_TYPE, 0, 3) and "
				+ "cm1.CODE_ABBR = substr (V2.PATIENT_TYPE, 4, 6) and cm2.CODE_CAT = substr(v2.PATIENT_CLASS, 0, 3) and cm2.CODE_ABBR = substr (v2.PATIENT_CLASS, 4, 6) "
				+ "AND V2.ADMIT_STATUS IN('AST1','AST3','AST4','AST7') AND V2.CANCEL_REASON IS NULL AND PA2.VISIT_ID = V2.VISIT_ID AND PA2.PATIENTACCOUNT_ID = PAT2.PATIENTACCOUNT_ID "
				+ "AND PAT2.TXNCODEMSTR_ID = CIM2.TXNCODEMSTR_ID and tcm.TXNCODEMSTR_ID = pat2.TXNCODEMSTR_ID AND CIM2.ITEMSUBCATEGORYMSTR_ID = ISCM2.ITEMSUBCATEGORYMSTR_ID "
				+ "AND PAT2.CHARGE_STATUS = 'CTTNOR' and pt.PATIENT_ID=v2.PATIENT_ID and ps.PERSON_ID=pt.PERSON_ID and cm1.CODE_DESC='"+codedesc+"' and cm2.CODE_DESC='"+codedesc2+"' and mrn.person_id=ps.person_id "
				+ "order by v2.VISIT_ID, receipt,iscm2.ITEMCATEGORYMSTR_ID ";
		try {
			st=connection.createStatement();
			rs=st.executeQuery(abdis);
			while (rs.next())
			{
				InfoTxnDateTime sdis=new InfoTxnDateTime();
				sdis.setVisit_id(rs.getString("VISIT_ID"));
				sdis.setAdmission_Datetime(rs.getString("ADMISSION_DATETIME"));
				sdis.setDischarge_Datetime(rs.getString("DISCHARGE_DATETIME"));
				sdis.setPatient_Type(rs.getString("patient_type"));
				sdis.setPatient_Class(rs.getString("patient_class"));
				sdis.setTxn_Datetime(rs.getString("TXN_DATETIME"));
				sdis.setReceipt(rs.getString("receipt"));
				sdis.setItemCategoryMstr_Id(rs.getString("ITEMCATEGORYMSTR_ID"));
				sdis.setItem_Subcat_Desc(rs.getString("ITEM_SUBCAT_DESC"));
				sdis.setTxn_Desc(rs.getString("TXN_DESC"));
				sdis.setGl_Account(rs.getString("GL_ACCOUNT"));
				sdis.setTxn_Amount(rs.getString("TXN_AMOUNT"));
				sdis.setDiscount_Amount(rs.getString("DISCOUNT_AMOUNT"));
				sdis.setClaimable_Percent(rs.getString("CLAIMABLE_PERCENT"));
				sdis.setPerson_Name(rs.getString("PERSON_NAME"));
				sdis.setMrn(rs.getString("medrec"));
				all.add(sdis);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
		finally {
	     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
	     if (st!=null) try  { st.close(); } catch (Exception ignore){}
	     if (connection!=null) try { connection.close();} catch (Exception ignore){}
	   }
		return all;
	}
	
	
	public static List<InfoTxnDateTime> getEnterDateTime(String first, String last, String codedesc, String codedesc2) {
		
		
		Connection connection = null;
		   Statement st = null;
		   ResultSet rs = null;
		
		List<InfoTxnDateTime> all=new ArrayList<InfoTxnDateTime>();
		
		connection=DbConnection.getPooledConnection();
		if(connection == null)return null;
		String abdis="SELECT v2.VISIT_ID, v2.ADMISSION_DATETIME, v2.DISCHARGE_DATETIME, cm1.CODE_DESC as patient_type, cm2.CODE_DESC as patient_class, pat2.TXN_DATETIME, "
				+ "IHIS.PGCOMMON.fxGetCodeDesc(iscm2.ip_receipt_item_cat) as receipt, iscm2.ITEMCATEGORYMSTR_ID, iscm2.ITEM_SUBCAT_DESC, tcm.TXN_DESC, cim2.GL_ACCOUNT, pat2.TXN_AMOUNT, "
				+ "pat2.DISCOUNT_AMOUNT, pat2.CLAIMABLE_PERCENT,pat2.ENTERED_DATETIME, ps.PERSON_NAME, mrn.card_no as medrec "
				+ "FROM IHIS.visit v2, IHIS.patientaccount pa2, IHIS.patientaccounttxn pat2, IHIS.chargeitemmstr cim2, PATIENT pt, PERSON ps, "
				+ "IHIS.itemsubcategorymstr iscm2, IHIS.CODEMSTR cm1, IHIS.CODEMSTR cm2, IHIS.TXNCODEMSTR tcm, card mrn "
				+ "WHERE pat2.ENTERED_DATETIME BETWEEN TO_DATE('"+first+" 00:00:01', 'yyyy/mm/dd HH24:MI:SS') and TO_DATE ('"+last+" 23:59:59', 'yyyy/mm/dd HH24:MI:SS') and cm1.CODE_CAT = substr(V2.PATIENT_TYPE, 0, 3) and "
				+ "cm1.CODE_ABBR = substr (V2.PATIENT_TYPE, 4, 6) and cm2.CODE_CAT = substr(v2.PATIENT_CLASS, 0, 3) and cm2.CODE_ABBR = substr (v2.PATIENT_CLASS, 4, 6) "
				+ "AND V2.ADMIT_STATUS IN('AST1','AST3','AST4','AST7') AND V2.CANCEL_REASON IS NULL AND PA2.VISIT_ID = V2.VISIT_ID AND PA2.PATIENTACCOUNT_ID = PAT2.PATIENTACCOUNT_ID "
				+ "AND PAT2.TXNCODEMSTR_ID = CIM2.TXNCODEMSTR_ID and tcm.TXNCODEMSTR_ID = pat2.TXNCODEMSTR_ID AND CIM2.ITEMSUBCATEGORYMSTR_ID = ISCM2.ITEMSUBCATEGORYMSTR_ID "
				+ "AND PAT2.CHARGE_STATUS = 'CTTNOR' and pt.PATIENT_ID=v2.PATIENT_ID and ps.PERSON_ID=pt.PERSON_ID and cm1.CODE_DESC='"+codedesc+"' and cm2.CODE_DESC='"+codedesc2+"' and mrn.person_id=ps.person_id "
				+ "order by v2.VISIT_ID, receipt,iscm2.ITEMCATEGORYMSTR_ID ";
		try {
			st=connection.createStatement();
			rs=st.executeQuery(abdis);
			while (rs.next())
			{
				InfoTxnDateTime sdis=new InfoTxnDateTime();
				sdis.setVisit_id(rs.getString("VISIT_ID"));
				sdis.setAdmission_Datetime(rs.getString("ADMISSION_DATETIME"));
				sdis.setDischarge_Datetime(rs.getString("DISCHARGE_DATETIME"));
				sdis.setPatient_Type(rs.getString("patient_type"));
				sdis.setPatient_Class(rs.getString("patient_class"));
				sdis.setTxn_Datetime(rs.getString("TXN_DATETIME"));
				sdis.setReceipt(rs.getString("receipt"));
				sdis.setItemCategoryMstr_Id(rs.getString("ITEMCATEGORYMSTR_ID"));
				sdis.setItem_Subcat_Desc(rs.getString("ITEM_SUBCAT_DESC"));
				sdis.setTxn_Desc(rs.getString("TXN_DESC"));
				sdis.setGl_Account(rs.getString("GL_ACCOUNT"));
				sdis.setTxn_Amount(rs.getString("TXN_AMOUNT"));
				sdis.setDiscount_Amount(rs.getString("DISCOUNT_AMOUNT"));
				sdis.setClaimable_Percent(rs.getString("CLAIMABLE_PERCENT"));
				sdis.setPerson_Name(rs.getString("PERSON_NAME"));
				sdis.setMrn(rs.getString("medrec"));
				sdis.setEntered_Datetime(rs.getString("ENTERED_DATETIME"));
				all.add(sdis);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (st!=null) try  { st.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	public static VisitNoIp getVisitNo(String mrn)
	{
		Connection connection = DbConnection.getPooledConnection();
		VisitNoIp view = new VisitNoIp();
		Statement stmt = null;
		ResultSet haspas = null;
		try {
			String query = "select * from (SELECT V.VISIT_ID FROM VISIT V,PATIENT PT,CARD CD WHERE V.PATIENT_ID = PT.PATIENT_ID AND PT.PERSON_ID = CD.PERSON_ID AND V.PATIENT_TYPE='PTY1' AND CD.CARD_NO="+mrn+" ORDER BY V.ADMISSION_DATETIME asc) where ROWNUM=1";
		
			if(connection == null)return null;
			stmt = connection.createStatement();
			
			try
			{
				haspas = stmt.executeQuery(query);
			}
			catch(Exception e)
			{
				
			}
			
			haspas.next();
			view.setVisit_ID(haspas.getString("Visit_ID"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return view;
	}

	
	public static List<InfoRekonObat> getrekonobat(String visitid) {
		List<InfoRekonObat> all=new ArrayList<InfoRekonObat>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="select PGCOMMON.FXGETCODEDESC(oe.ORDER_TYPE) as tipe, oe.VISIT_ID, oe.ORDERENTRY_ID, oei.ORDERENTRYITEM_ID, lm.LOCATION_DESC,  p.PERSON_NAME, tcm.TXN_DESC, oei.DOSAGE_QTY, PGCOMMON.FXGETCODEDESC(oei.DOSAGE_UNIT) as dosage_unit, "
				+ "fm.FREQUENCY_DESC, oe.REMARKS "
				+ "from ORDERENTRY oe inner join ORDERENTRYITEM oei on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = oei.ORDERITEMMSTR_ID "
				+ "inner join ITEMSUBCATEGORYMSTR iscm on iscm.ITEMSUBCATEGORYMSTR_ID = oim.ITEMSUBCATEGORYMSTR_ID inner join ITEMCATEGORYMSTR icm on icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID "
				+ "inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = oe.LOCATIONMSTR_ID inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID "
				+ "inner join person p on p.PERSON_ID = cp.PERSON_ID inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID "
				+ "inner join FREQUENCYMSTR fm on oe.FREQUENCYMSTR_ID = fm.FREQUENCYMSTR_ID "
				+ "where icm.ITEM_TYPE = 'ITY1' and oei.DRUG_IND= 'Y' and oe.VISIT_ID in ("+visitid+") order by oe.ORDER_TYPE, oe.ENTERED_DATETIME";
		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				InfoRekonObat inrek=new InfoRekonObat();
				inrek.setTipe(hasdis.getString("tipe"));
				inrek.setVISIT_ID(hasdis.getString("VISIT_ID"));
				inrek.setORDERENTRY_ID(hasdis.getString("ORDERENTRY_ID"));
				inrek.setORDERENTRYITEM_ID(hasdis.getString("ORDERENTRYITEM_ID"));
				inrek.setLOCATION_DESC(hasdis.getString("LOCATION_DESC"));
				inrek.setPERSON_NAME(hasdis.getString("PERSON_NAME"));
				inrek.setTXN_DESC(hasdis.getString("TXN_DESC"));
				inrek.setDOSAGE_QTY(hasdis.getString("DOSAGE_QTY"));
				inrek.setDosage_unit(hasdis.getString("dosage_unit"));
				inrek.setFREQUENCY_DESC(hasdis.getString("FREQUENCY_DESC"));
				inrek.setREMARKS(hasdis.getString("REMARKS"));
				all.add(inrek);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<InfoCekRem> getcekrem(String fromdate, String todate) {
		List<InfoCekRem> all=new ArrayList<InfoCekRem>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="select remark, count(*) as total from (select case when ROUND(1440 * (enter - ADMISSION_DATETIME),2) <= 60 then 'BELOW' "
				+ " when ROUND(1440 * (enter - ADMISSION_DATETIME),2) > 60 then 'OVER'end as remark "
				+ "from (SELECT V.VISIT_ID, V.ADMISSION_DATETIME, (SELECT MIN(OE.ENTERED_DATETIME) FROM IHIS.ORDERENTRY OE "
				+ "WHERE (OE.VISIT_ID = V.VISIT_ID) AND (OE.ENTERED_DATETIME IS NOT NULL)) as enter, CM.CODE_DESC "
				+ "FROM IHIS.VISIT V INNER JOIN IHIS.CODEMSTR CM ON CONCAT(CM.CODE_CAT , CM.CODE_ABBR) = V.PATIENT_CLASS "
				+ "WHERE V.PATIENT_TYPE = 'PTY2' and v.admit_status in('AST1', 'AST2', 'AST3', 'AST4', 'AST7')"
				+ "and v.admission_datetime >= to_date('"+fromdate+" 00:00:00', 'yyyy-MM-dd HH24:MI:SS') "
				+ "and v.admission_datetime <= to_date('"+todate+" 23:59:59', 'yyyy-MM-dd HH24:MI:SS'))"
				+ "where enter is not null) group by remark";
		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				InfoCekRem inrek=new InfoCekRem();
				inrek.setRemark(hasdis.getString("remark"));
				inrek.setTotal(hasdis.getString("total"));
				all.add(inrek);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<Infostockreceipt> getstockreceipt(String fromdate, String todate) {
		List<Infostockreceipt> all=new ArrayList<Infostockreceipt>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="SELECT  SRD.INVOICE_NO, VM.VENDOR_NAME, MIM.MATERIAL_ITEM_NAME, SRD.RECEIPT_QTY_OF_BASE_UOM , "
				+ "PGCOMMON.FXGETCODEDESC(SRD.BASE_UOM) AS BASE_UOM, SRD.WHOLESALE_PRICE, SRD.UNIT_PRICE , SRD.DISC_UNIT,  SRD.PPN_RATE, "
				+ "SRD.TOTAL_PRICE "
				+ "FROM STOCKRECEIPT SR, STOCKRECEIPTDETAIL SRD, MATERIALITEMMSTR MIM, VENDORMSTR VM, MATERIALITEMCATMSTR MCM "
				+ "WHERE TO_CHAR(SR.RECEIPT_DATETIME,'DDMMYYYY')>='"+fromdate+"' AND TO_CHAR(SR.RECEIPT_DATETIME,'DDMMYYYY')<='"+todate+"' "
				+ "AND  SR.STOREMSTR_ID='311102896' AND SRD.STOCKRECEIPT_ID=SR.STOCKRECEIPT_ID AND MIM.MATERIALITEMMSTR_ID=SRD.MATERIALITEMMSTR_ID "
				+ "AND VM.VENDORMSTR_ID=SR.VENDORMSTR_ID AND SR.RECEIPT_TYPE='SRT1' AND MCM.MATERIALITEMCATMSTR_ID=MIM.MATERIALITEMCATMSTR_ID "
				+ "ORDER BY VM.VENDOR_NAME ASC";
		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			
			while (hasdis.next())
			{
				String totwhole=hasdis.getString("WHOLESALE_PRICE");
					BigDecimal hastotwhole=new BigDecimal(totwhole);
				String totunitprice=hasdis.getString("UNIT_PRICE");
					BigDecimal hastotunitprice=new BigDecimal(totunitprice);
				String qty=hasdis.getString("RECEIPT_QTY_OF_BASE_UOM");
					BigDecimal hasqty=new BigDecimal(qty);
				String ppn=hasdis.getString("PPN_RATE");
					BigDecimal hasppn=new BigDecimal(ppn);
				//Hasil total WHOLESALE PRICE * QTY
				BigDecimal totall= hastotwhole.multiply(hasqty);
				
				//Hasil total WHOLESALE PRICE - UNIT PRICE
				BigDecimal totaldiscountperunit=hastotwhole.subtract(hastotunitprice);
				
				// Hasil total Kurprice * Qty
				BigDecimal totaldiscountkeseluruhan=totaldiscountperunit.multiply(hasqty);
				
				//Hasil Total Price setelah di discount
				BigDecimal hastot=totall.subtract(totaldiscountkeseluruhan);
				
				//Hasil PPN setelah discount
				BigDecimal fippn=hastot.multiply(hasppn);
				BigDecimal nilai= new BigDecimal(100);
				BigDecimal finppn=fippn.divide(nilai);
				
				Infostockreceipt inrek=new Infostockreceipt();
				inrek.setTotaldiscountperunit(totaldiscountperunit);
				inrek.setTotaldiscountkeseluruhan(totaldiscountkeseluruhan);
				inrek.setTotalppnkeseluruhan(finppn);
				inrek.setINVOICE_NO(hasdis.getString("INVOICE_NO"));
				inrek.setVENDOR_NAME(hasdis.getString("VENDOR_NAME"));
				inrek.setMATERIAL_ITEM_NAME(hasdis.getString("MATERIAL_ITEM_NAME"));
				inrek.setRECEIPT_QTY_OF_BASE_UOM(hasdis.getString("RECEIPT_QTY_OF_BASE_UOM"));
				inrek.setBASE_UOM(hasdis.getString("BASE_UOM"));
				inrek.setTotalWHOLESALE_PRICEperunit(hasdis.getString("WHOLESALE_PRICE"));
				inrek.setTotalWHOLESALE_PRICEkeseluruhan(totall);
				inrek.setUNIT_PRICE(hasdis.getString("UNIT_PRICE"));
				inrek.setDISC_UNIT(hasdis.getString("DISC_UNIT"));
				inrek.setPPN_RATE(hasdis.getString("PPN_RATE"));
				inrek.setTOTAL_PRICE(hasdis.getString("TOTAL_PRICE"));				
				all.add(inrek);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<ListDispanceCount> getDispanceCount(String fromdate, String todate) {
		
		Connection connection = null;
		   Statement st = null;
		   ResultSet rs = null;
		
		List<ListDispanceCount> all=new ArrayList<ListDispanceCount>();
		connection=DbConnection.getPooledConnection();
		if(connection == null)return null;
		
		String abdis="SELECT TO_CHAR(DISPENSED_DATETIME,'yyyy-MM') DISPENSED_DATETIME,DISPENSE_BATCH_NO, "
				+ "TIPE,KELAS, "
				+ "SUBSPECIALTY_DESC, GUDANG, "
				+ "COUNT(DISTINCT(DISPENSE_BATCH_NO)) AS TOTAL FROM (SELECT GUDANG,TO_CHAR(RESEP.DISPENSED_DATETIME,'yyyy-MM'), "
				+ "RESEP.DISPENSE_BATCH_NO, "
				+ "RESEP.DISPENSED_DATETIME, "
				+ "SSM.SUBSPECIALTY_DESC, "
				+ "CASE V.PATIENT_CLASS WHEN 'PTC114' THEN 'BPJS' ELSE 'NON BPJS' END KELAS, "
				+ "pgcommon.fxGetCodeDesc(V.PATIENT_TYPE) TIPE "
				+ "FROM ORDERCONFIRMATION OC "
				+ "INNER JOIN (SELECT DD.DISPENSE_BATCH_NO,DD.ORDERCONFIRMATION_ID,DD.DISPENSED_DATETIME,SM.STORE_DESC GUDANG FROM DRUGDISPENSE DD "
				+ "INNER JOIN STOREMSTR SM ON SM.STOREMSTR_ID = DD.STOREMSTR_ID "
				+ "WHERE DD.DISPENSED_DATETIME >= to_date('"+fromdate+"', 'yyyy-MM-dd HH24:MI') AND DD.DISPENSED_DATETIME<= to_date('"+todate+"', 'yyyy-MM-dd HH24:MI') AND DD.STOREMSTR_ID = SM.STOREMSTR_ID ) RESEP ON OC.ORDERCONFIRMATION_ID = RESEP.ORDERCONFIRMATION_ID "
				+ "INNER JOIN ORDERENTRYITEM OEI ON OEI.ORDERENTRYITEM_ID = OC.ORDERENTRYITEM_ID "
				+ "INNER JOIN ORDERENTRY OE ON OE.ORDERENTRY_ID = OEI.ORDERENTRY_ID "
				+ "INNER JOIN VISIT V ON V.VISIT_ID = OE.VISIT_ID "
				+ "INNER JOIN SUBSPECIALTYMSTR SSM ON SSM.SUBSPECIALTYMSTR_ID = V.SUBSPECIALTYMSTR_ID) GROUP BY GUDANG,TO_CHAR(DISPENSED_DATETIME,'yyyy-MM'),TIPE,KELAS,SUBSPECIALTY_DESC,DISPENSE_BATCH_NO";
		
		/*
		String abdis="select to_char(dispensed_datetime, 'yyyy-MM') as dispensed_datetime, tipe, kelas, subspecialty_desc, store, count(dispense_batch_no) as dispense_batch_no "
				+ "from (select store, to_char (resep.dispensed_datetime, 'yyyy-MM'), resep.dispense_batch_no, resep.dispensed_datetime, ssm.subspecialty_desc, case v.patient_class when 'PTC114' THEN 'BPJS' ELSE 'NON BPJS' end kelas , pgcommon.fxGetCodeDesc(v.patient_type) tipe "
				+ "from orderconfirmation oc inner join "
				+ "(select dd.dispense_batch_no, dd.orderconfirmation_id, dd.dispensed_datetime, sm.STORE_DESC as store from drugdispense dd, STOREMSTR sm where "
				+ "dd.dispensed_datetime >= to_date('"+fromdate+"', 'yyyy-MM-dd HH24:MI:SS') and dd.dispensed_datetime <= to_date('"+todate+"', 'yyyy-MM-dd HH24:MI:SS') and sm.STOREMSTR_ID=dd.STOREMSTR_ID) resep "
				+ "on oc.orderconfirmation_id = resep.orderconfirmation_id inner join orderentryitem oei on oei.orderentryitem_id = oc.orderentryitem_id inner join orderentry oe on oe.orderentry_id = oei.orderentry_id "
				+ "inner join visit v on v.visit_id = oe.visit_id inner join subspecialtymstr ssm on ssm.subspecialtymstr_id = v.subspecialtymstr_id)  hasil "
				+ "WHERE ROWID IN ( SELECT MAX(rowid) FROM drugdispense GROUP BY dispense_batch_no ) group by store,tipe, kelas, subspecialty_desc, to_char(dispensed_datetime, 'yyyy-MM')";
		*/
		try {
			st=connection.createStatement();
			rs=st.executeQuery(abdis);
			while (rs.next())
			{
				ListDispanceCount inrek=new ListDispanceCount();
				inrek.setDispensed_datetime(rs.getString("DISPENSED_DATETIME"));
				inrek.setTipe(rs.getString("TIPE"));
				inrek.setKelas(rs.getString("KELAS"));
				inrek.setTotal(rs.getString("TOTAL"));
				inrek.setSubspecialty_desc(rs.getString("SUBSPECIALTY_DESC"));
				inrek.setDispense_batch_no(rs.getString("DISPENSE_BATCH_NO"));
				inrek.setStore(rs.getString("GUDANG"));
				all.add(inrek);
			}
			connection.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
	     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
	     if (st!=null) try  { st.close(); } catch (Exception ignore){}
	     if (connection!=null) try { connection.close();} catch (Exception ignore){}
	   }
		
		return all;
	}

	public static List<ListgetPharPulvis> getPharPulvis(String fromdate, String todate) {
		Connection connection = null;
	   	Statement st = null;
	   	ResultSet rs = null;
		List<ListgetPharPulvis> all=new ArrayList<ListgetPharPulvis>();
		connection=DbConnection.getPooledConnection();
		if(connection == null)return null;
		String abdis="select store, period, tipe, count(*) as jum from (select store, period, dispense_batch_no, case (sum(jlh)-count(jlh))when 0 then 'NORMAL' else 'PULVIS' end tipe "
				+ "from (select store, period, dispense_batch_no, count(orderentry_id) as jlh from (select sm.STORE_DESC as store, to_char(dd.dispensed_datetime, 'yyyy-MM') as period, dd.dispense_batch_no, "
				+ "dd.orderconfirmation_id, oe.orderentry_id, oei.orderentryitem_id from drugdispense dd inner join orderconfirmation oc on "
				+ "oc.orderconfirmation_id = dd.orderconfirmation_id "
				+ "inner join orderentryitem oei on oei.orderentryitem_id = oc.orderentryitem_id inner join orderentry oe on oe.orderentry_id = oei.orderentry_id "
				+ "inner join STOREMSTR sm on sm.STOREMSTR_ID = dd.STOREMSTR_ID "
				+ "where dd.dispensed_datetime >= to_date('"+fromdate+"', 'yyyy-MM-dd HH24:MI:SS') and "
				+ "dd.dispensed_datetime <= to_date('"+todate+"', 'yyyy-MM-dd HH24:MI:SS')) group by orderentry_id, dispense_batch_no, period, store) "
				+ "group by store, dispense_batch_no, period) group by tipe, period, store order by period";
		try {
			st=connection.createStatement();
			rs=st.executeQuery(abdis);
			while (rs.next())
			{
				ListgetPharPulvis inrek=new ListgetPharPulvis();
				inrek.setTipe(rs.getString("tipe"));
				inrek.setPeriod(rs.getString("period"));
				inrek.setJum(rs.getString("jum"));
				inrek.setStore(rs.getString("store"));
				all.add(inrek);
			}
			connection.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (st!=null) try  { st.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
			
		return all;
	}
	
	
	public static List<InfoRekonObatForCat> getrekonobatforcat(String visitid) {
		List<InfoRekonObatForCat> all=new ArrayList<InfoRekonObatForCat>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="select oe.ENTERED_DATETIME, PGCOMMON.FXGETCODEDESC(oe.ROA) as cara, pe.PLANNED_DATETIME, osj.ORDERSTATUSJOURNAL_ID, "
				+ "(select um.USER_NAME from USERMSTR um where um.USERMSTR_ID = osj.LAST_UPDATED_BY) as perawat, "
				+ "oe.STOPPED_DATETIME, (select um.USER_NAME from USERMSTR um where um.USERMSTR_ID = oe.STOPPED_BY) as stopperawat, "
				+ "osj.PERFORMED_DATETIME, PGCOMMON.FXGETCODEDESC(oe.ORDER_TYPE) as tipe, "
				+ "oe.VISIT_ID, oe.ORDERENTRY_ID, oei.ORDERENTRYITEM_ID, lm.LOCATION_DESC,  p.PERSON_NAME, tcm.TXN_DESC, oei.DOSAGE_QTY, "
				+ "PGCOMMON.FXGETCODEDESC(oei.DOSAGE_UNIT) as dosage_unit, fm.FREQUENCY_DESC, oe.REMARKS "
				+ "from ORDERENTRY oe inner join ORDERENTRYITEM oei on oe.ORDERENTRY_ID = oei.ORDERENTRY_ID "
				+ "inner join ORDERITEMMSTR oim on oim.ORDERITEMMSTR_ID = oei.ORDERITEMMSTR_ID "
				+ "inner join ITEMSUBCATEGORYMSTR iscm on iscm.ITEMSUBCATEGORYMSTR_ID = oim.ITEMSUBCATEGORYMSTR_ID "
				+ "inner join ITEMCATEGORYMSTR icm on icm.ITEMCATEGORYMSTR_ID = iscm.ITEMCATEGORYMSTR_ID "
				+ "inner join LOCATIONMSTR lm on lm.LOCATIONMSTR_ID = oe.LOCATIONMSTR_ID "
				+ "inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = oe.ORDERED_CAREPROVIDER_ID "
				+ "inner join person p on p.PERSON_ID = cp.PERSON_ID "
				+ "inner join TXNCODEMSTR tcm on tcm.TXNCODEMSTR_ID = oim.TXNCODEMSTR_ID "
				+ "inner join FREQUENCYMSTR fm on oe.FREQUENCYMSTR_ID = fm.FREQUENCYMSTR_ID "
				+ "inner join PLANNEDEXECUTION pe on pe.ORDERENTRYITEM_ID = oei.ORDERENTRYITEM_ID "
				+ "inner join ORDERCONFIRMATION oc on oc.ORDERENTRYITEM_ID = oei.ORDERENTRYITEM_ID "
				+ "inner join ORDERSTATUSJOURNAL osj on osj.PLANNEDEXECUTION_ID = pe.PLANNEDEXECUTION_ID and "
				+ "osj.ORDERCONFIRMATION_ID = oc.ORDERCONFIRMATION_ID and osj.ORDER_STATUS = 'OSTSRV' "
				+ "where icm.ITEM_TYPE = 'ITY1' and oei.DRUG_IND= 'Y' and oe.VISIT_ID in ("+visitid+")  order by oe.ORDER_TYPE, oe.ENTERED_DATETIME";
		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				InfoRekonObatForCat inrek=new InfoRekonObatForCat();
				inrek.setENTERED_DATETIME(hasdis.getString("ENTERED_DATETIME"));
				inrek.setCara(hasdis.getString("cara"));	
				inrek.setPLANNED_DATETIME(hasdis.getString("PLANNED_DATETIME"));
				inrek.setORDERSTATUSJOURNAL_ID(hasdis.getString("ORDERSTATUSJOURNAL_ID"));
				inrek.setPerawat(hasdis.getString("perawat"));
				inrek.setSTOPPED_DATETIME(hasdis.getString("STOPPED_DATETIME"));
				inrek.setStopperawat(hasdis.getString("stopperawat"));
				inrek.setPERFORMED_DATETIME(hasdis.getString("PERFORMED_DATETIME"));
				inrek.setTipe(hasdis.getString("tipe"));
				inrek.setVISIT_ID(hasdis.getString("VISIT_ID"));
				inrek.setORDERENTRY_ID(hasdis.getString("ORDERENTRY_ID"));
				inrek.setORDERENTRYITEM_ID(hasdis.getString("ORDERENTRYITEM_ID"));
				inrek.setLOCATION_DESC(hasdis.getString("LOCATION_DESC"));
				inrek.setPERSON_NAME(hasdis.getString("PERSON_NAME"));
				inrek.setTXN_DESC(hasdis.getString("TXN_DESC"));
				inrek.setDOSAGE_QTY(hasdis.getString("DOSAGE_QTY"));
				inrek.setDosage_unit(hasdis.getString("dosage_unit"));
				inrek.setFREQUENCY_DESC(hasdis.getString("FREQUENCY_DESC"));
				inrek.setREMARKS(hasdis.getString("REMARKS"));
				all.add(inrek);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<ListInfoViewTrx> getInfoViewTrx(String fromdate, String todate, String idnik) {
		List<ListInfoViewTrx> all=new ArrayList<ListInfoViewTrx>();
		Connection connection=DbConnection.getTrxInstance();
		if(connection == null)return null;
		Statement storedis=null;
		ResultSet hasdis = null;
		String abdis="SELECT tab.nik, tab.card_no, tab.user_name, ttx.trx_type, ttx.invoice_no, ttx.payment_no, ttx.trx_amount, ttx.trx_date "
				+ "FROM t_accounttrx ttx, t_accountbalance tab "
				+ "where tab.company='"+idnik+"' and ttx.trx_date>='"+fromdate+" 00:01:00' and ttx.trx_date<='"+todate+" 23:59:00' "
				+ "and ttx.payment_no is not null and tab.accountbalance_id=ttx.accountbalance_id";
		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListInfoViewTrx inrek=new ListInfoViewTrx();
				inrek.setNik(hasdis.getString("nik"));
				inrek.setCard_no(hasdis.getString("card_no"));
				inrek.setUser_name(hasdis.getString("user_name"));
				inrek.setTrx_type(hasdis.getString("trx_type"));
				inrek.setInvoice_no(hasdis.getString("invoice_no"));
				inrek.setPayment_no(hasdis.getString("payment_no"));
				inrek.setTrx_amount(hasdis.getString("trx_amount"));
				inrek.setTrx_date(hasdis.getString("trx_date"));
				all.add(inrek);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
		    if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		    if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
	}

	public static List<ListInfoCek> getInfoCekSts(String cardno) {
		List<ListInfoCek> all=new ArrayList<ListInfoCek>();
		Connection connection=DbConnection.getTrxInstance();
		if(connection == null)return null;
		Statement storedis=null;
		ResultSet hasdis = null;
		String abdis="select nik,user_name,status,balance,company from t_accountbalance  where card_no='"+cardno+"'";
		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListInfoCek inrek=new ListInfoCek();
				inrek.setNik(hasdis.getString("nik"));
				inrek.setUser_name(hasdis.getString("user_name"));
				inrek.setStatus(hasdis.getString("status"));
				inrek.setBalance(hasdis.getString("balance"));
				inrek.setCompany(hasdis.getString("company"));
				all.add(inrek);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
		    if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		    if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
	}

	public static List<ListInfoKaryawan> getInfoKaryawan(String idnik) {
		List<ListInfoKaryawan> all=new ArrayList<ListInfoKaryawan>();
		Connection connection=DbConnection.getTrxInstance();
		if(connection == null)return null;
		Statement storedis=null;
		ResultSet hasdis = null;
		String abdis="SELECT TAB.nik, TAB.card_no, TAB.user_name, TAB.balance, TAB.updated_at, TAB.updated_by "
				+ "FROM t_accountbalance TAB "
				+ "WHERE TAB.status=1 and TAB.company ='"+idnik+"'";

		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListInfoKaryawan inrek=new ListInfoKaryawan();
				inrek.setNik(hasdis.getString("nik"));
				inrek.setCard_no(hasdis.getString("card_no"));
				inrek.setUser_name(hasdis.getString("user_name"));
				inrek.setBalance(hasdis.getString("balance"));
				inrek.setUpdated_at(hasdis.getString("updated_at"));
				inrek.setUpdated_by(hasdis.getString("updated_by"));
				all.add(inrek);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
		    if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		    if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
	}

	public static List<ListGetHasBPJS> getHasBPJS(String patientid) {
		List<ListGetHasBPJS> all=new ArrayList<ListGetHasBPJS>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="SELECT BPJS_NO FROM PATIENT WHERE PATIENT_ID="+patientid;

		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListGetHasBPJS inrek=new ListGetHasBPJS();
				inrek.setBPJS_NO(hasdis.getString("BPJS_NO"));
				all.add(inrek);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<ListInfoViewTrx> getAllViewTrx(String fromdate, String todate) {
		List<ListInfoViewTrx> all=new ArrayList<ListInfoViewTrx>();
		Connection connection=DbConnection.getTrxInstance();
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="SELECT tab.nik, tab.card_no, tab.user_name, ttx.trx_type, ttx.invoice_no, ttx.payment_no, ttx.trx_amount, ttx.trx_date, tab.company "
				+ "FROM t_accounttrx ttx, t_accountbalance tab "
				+ "where ttx.trx_date>='"+fromdate+" 00:01:00' and ttx.trx_date<='"+todate+" 23:59:00' "
				+ "and ttx.payment_no is not null and tab.accountbalance_id=ttx.accountbalance_id";
		try {
			storedis=connection.createStatement();
			ResultSet hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListInfoViewTrx inrek=new ListInfoViewTrx();
				inrek.setNik(hasdis.getString("nik"));
				inrek.setCard_no(hasdis.getString("card_no"));
				inrek.setUser_name(hasdis.getString("user_name"));
				inrek.setTrx_type(hasdis.getString("trx_type"));
				inrek.setInvoice_no(hasdis.getString("invoice_no"));
				inrek.setPayment_no(hasdis.getString("payment_no"));
				inrek.setTrx_amount(hasdis.getString("trx_amount"));
				inrek.setTrx_date(hasdis.getString("trx_date"));
				inrek.setCompany(hasdis.getString("company"));
				all.add(inrek);
			}
			hasdis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<ListInfoKaryawan> getAllKaryawan() {
		List<ListInfoKaryawan> all=new ArrayList<ListInfoKaryawan>();
		Connection connection=DbConnection.getTrxInstance();
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="SELECT TAB.nik, TAB.card_no, TAB.user_name, TAB.balance, TAB.updated_at, TAB.updated_by, TAB.company "
				+ "FROM t_accountbalance TAB where TAB.status=1 ";

		try {
			storedis=connection.createStatement();
			ResultSet hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListInfoKaryawan inrek=new ListInfoKaryawan();
				inrek.setNik(hasdis.getString("nik"));
				inrek.setCard_no(hasdis.getString("card_no"));
				inrek.setUser_name(hasdis.getString("user_name"));
				inrek.setBalance(hasdis.getString("balance"));
				inrek.setUpdated_at(hasdis.getString("updated_at"));
				inrek.setUpdated_by(hasdis.getString("updated_by"));
				inrek.setCompany(hasdis.getString("company"));
				all.add(inrek);
			}
			hasdis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
			 if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<ListKamar> getAllRoom() {
		List<ListKamar> all = new ArrayList<ListKamar>();
		Connection connection = DbConnection.getRoomConnection();
		if(connection == null)return null;
		Statement roomdis=null;
		String Query="SELECT kelas kodekelas,ttd kapasitas, "
				+ "(CONVERT(decimal(10,0),(CAST(ttd AS varchar(10)))) - CONVERT(decimal(10,0),(CAST(ttdt AS varchar(10))))) tersedia "
				+ "FROM [mtdocumentcontainer].[dbo].[tb_ttdclass_systems] where status='Baru'";
		try
		{
			roomdis=connection.createStatement();
			ResultSet row = roomdis.executeQuery(Query);
			while(row.next())
			{
				ListKamar fetch = new ListKamar();
				fetch.setKodekelas(row.getString("kodekelas"));
				fetch.setKapasitas(row.getString("kapasitas"));
				fetch.setTersedia(row.getString("tersedia"));
				all.add(fetch);
			}
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (roomdis!=null) try  { roomdis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
		
	}
	//-----jurnal pembelian
	public static List<viewJournal> addApotekGl(String nopo,String total,String tipe)
	{
		List<viewJournal> all = new  ArrayList<viewJournal>();
		Connection connection = null;
		
		if(tipe.equals("1"))
		{
			connection = DbConnection.getGlApotekConnection();
		}
		else if(tipe.equals("2"))
		{
			connection = DbConnection.getGlApotekConnection2();		
		}
		else
		{
			connection = DbConnection.getGlApotekConnection3();
		}
		
		Statement stmt =null;
		String query ="Select count(*) as jum from Tr_Jurnal where NoBukti='"+nopo+"'";
		try
		{
			stmt=connection.createStatement();
			ResultSet row =stmt.executeQuery(query);
			row.next();
			viewJournal li=new viewJournal();
			String jum=row.getString("jum");
			if(jum.equals("1"))
			{
				li.setStatus("ERROR");
				all.add(li);		
				row.close();
				stmt.close();
			}
			else
			{
				Statement stmt2 =null;
				String query2 = "INSERT INTO [dbo].[Tr_Jurnal] ([ComLocID],[NoBukti],[TipeJurnal],[Debet],[Kredit],[Posted],[TglPosting], "
						+ "[NoRef],[AutoJurnal],[CreatedBy],[CreatedTime],[ModifiedBy],[ModifiedTime],[Marked],[MarkedBy],"
						+ "[MarkedDate],[Tahun],[CetakBPK],[DiCetakOleh],[TglCetak])VALUES "
						+ "('HO','"+nopo+"','AP',"+total+","+total+",0,NULL,NULL,1,'khoman',CURRENT_TIMESTAMP,'-',CURRENT_TIMESTAMP,0,'',NULL,YEAR(GETDATE()),0,'',NULL)";
				stmt2=connection.createStatement();
				stmt2.executeUpdate(query2);
				viewJournal vj=new viewJournal();
				vj.setStatus("OK");
				all.add(vj);
				stmt2.close();
				row.close();
				stmt.close();
			}
			
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	
	public static List<viewJournal2> addApotekGlDetail(String nopo,String tglt,String nourt,String sandi,String kat,String nasup,String total,String ido,String tipe)
	{
		List<viewJournal2> all = new  ArrayList<viewJournal2>();
		Connection connection= null;
		if(tipe.equals("1"))
		{
			connection = DbConnection.getGlApotekConnection();
		}
		else if(tipe.equals("2"))
		{
			connection = DbConnection.getGlApotekConnection2();		
		}
		else
		{
			connection = DbConnection.getGlApotekConnection3();
		}
		Statement stmt =null;
		String query ="Select count(*) as jum from Tr_JurnalDet where NoBukti='"+nopo+"' and NoUrut='"+nourt+"'";
		try
		{
			stmt=connection.createStatement();
			ResultSet row =stmt.executeQuery(query);
			row.next();
			viewJournal2 li=new viewJournal2();
			String jum=row.getString("jum");
			if(jum.equals("1"))
			{
				li.setStatus("ERROR");
				all.add(li);		
				row.close();
				stmt.close();
			}
			else
			{
				Statement stmtdet =null;
				String querydetail = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+nopo+"','"+tglt+"','"+nourt+"','5.1.02."+sandi+"','"+kat+"', '"+nasup+": Pembelian "+kat+"','"+total+"',0,1,'"+total+"','"+ido+"',NULL,YEAR(GETDATE()))";
				stmtdet=connection.createStatement();
				stmtdet.executeUpdate(querydetail);
				viewJournal2 vj2=new viewJournal2();
				vj2.setStatus("OK");
				all.add(vj2);
				stmtdet.close();
				row.close();
				stmt.close();
				
			}
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
	return all;
	}
	
	
	public static List<viewJournal> addNilai(String sandi,String tgl,String nsp,String nopo,String ppn,String nppn,String dis,String tot,String nopr,String noref, String tipe)
	{
		List<viewJournal> all = new  ArrayList<viewJournal>();
		Connection connection = null;
		if(tipe.equals("1"))
		{
			connection = DbConnection.getGlApotekConnection();
		}
		else if(tipe.equals("2"))
		{
			connection = DbConnection.getGlApotekConnection2();		
		}
		else
		{
			connection = DbConnection.getGlApotekConnection3();
		}
		Statement stmt =null;
		String query ="SELECT max(NoUrut) nourt FROM Tr_JurnalDet where NoBukti='"+noref+"' and NoRef='"+nopr+"'";
		try
		{
			stmt=connection.createStatement();
			ResultSet row =stmt.executeQuery(query);
			row.next();
			int no = row.getInt("nourt");
			int num1 = no + 1;
			int num2 = no + 2;
			int num3 = no + 3;
//			row.close();
//			stmt.close();	

			if(ppn.equals("nol") && dis.equals("nol") )
			{
				
				String query4 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+noref+"','"+tgl+"','"+num1+"','"+sandi+"',NULL, '"+nsp+": Pembelian "+noref+"',0,"+tot+",1,"+tot+",'"+nopr+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt4 = connection.prepareStatement(query4);
				stmt4.executeUpdate();
				viewJournal fin=new viewJournal();
				fin.setStatus("OK");
				all.add(fin);
				stmt4.close();
				
			}
			else if(ppn.equals("nol"))
			{
				String query3 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
					+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
					+ "VALUES ('HO','"+noref+"','"+tgl+"','"+num1+"','5.1.04',NULL, '"+nsp+": Pembelian "+noref+"',0,'"+dis+"',1,'"+dis+"','"+nopr+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt3 = connection.prepareStatement(query3);
				stmt3.executeUpdate();
				stmt3.close();
				
				String query4 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+noref+"','"+tgl+"','"+num2+"','"+sandi+"',NULL, '"+nsp+": Pembelian "+noref+"',0,'"+tot+"',1,'"+tot+"','"+nopr+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt4 = connection.prepareStatement(query4);
				stmt4.executeUpdate();
				viewJournal fin=new viewJournal();
				fin.setStatus("OK");
				all.add(fin);
				stmt4.close();
				row.close();
				stmt.close();
			}
			else if(dis.equals("nol"))
			{
				String query2 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+noref+"','"+tgl+"','"+num1+"','2.1.06',NULL, '"+nsp+": PPN Pembelian "+noref+"','"+ppn+"',0,'"+nppn+"','"+ppn+"','"+nopr+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt2 = connection.prepareStatement(query2);
				stmt2.executeUpdate();
				stmt2.close();
				
				String query4 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+noref+"','"+tgl+"','"+num2+"','"+sandi+"',NULL, '"+nsp+": Pembelian "+noref+"',0,'"+tot+"',1,'"+tot+"','"+nopr+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt4 = connection.prepareStatement(query4);
				stmt4.executeUpdate();
				viewJournal fin=new viewJournal();
				fin.setStatus("OK");
				all.add(fin);
				stmt4.close();
				row.close();
				stmt.close();
			}
			else
			{
				
				String query2 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+noref+"','"+tgl+"','"+num1+"','2.1.06',NULL, '"+nsp+": PPN Pembelian "+noref+"','"+ppn+"',0,'"+nppn+"','"+ppn+"','"+nopr+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt2 = connection.prepareStatement(query2);
				stmt2.executeUpdate();
				stmt2.close();
				
				String query3 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+noref+"','"+tgl+"','"+num2+"','5.1.04',NULL, '"+nsp+": Pembelian "+noref+"',0,'"+dis+"',1,'"+dis+"','"+nopr+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt3 = connection.prepareStatement(query3);
				stmt3.executeUpdate();
				stmt3.close();
				
				String query4 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+noref+"','"+tgl+"','"+num3+"','"+sandi+"',NULL, '"+nsp+": Pembelian "+noref+"',0,"+tot+",1,"+tot+",'"+nopr+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt4 = connection.prepareStatement(query4);
				stmt4.executeUpdate();
				viewJournal fin=new viewJournal();
				fin.setStatus("OK");
				all.add(fin);
				stmt4.close();
				row.close();
				stmt.close();
			}
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	
	public static List<viewJournal> addHutang(String nobukti,String ids,String tgl,String tgljt,String user,String total, String tipe)
	{
		List<viewJournal> all = new  ArrayList<viewJournal>();
		Connection connection = null;
		if(tipe.equals("1"))
		{
			connection = DbConnection.getGlApotekConnection();
		}
		else if(tipe.equals("2"))
		{
			connection = DbConnection.getGlApotekConnection2();		
		}
		else
		{
			connection = DbConnection.getGlApotekConnection3();
		}
		Statement stmt =null;
		String query ="SELECT count(*) jum FROM tr_hutang where nobukti='"+nobukti+"'";
		try
		{
			stmt=connection.createStatement();
			ResultSet row =stmt.executeQuery(query);
			row.next();
			viewJournal li=new viewJournal();
			String jum=row.getString("jum");
			if(jum.equals("1"))
			{
				li.setStatus("ERROR");
				all.add(li);		
				row.close();
				stmt.close();
			}
			else
			{
				final DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				
				final LocalDate d1 = LocalDate.parse(tgl, fmt);
				final LocalDate d2 = LocalDate.parse(tgljt, fmt);
				
				final long daysInBetween = ChronoUnit.DAYS.between(d1, d2);
				final long hari;
				if(daysInBetween == 0)
				{
					hari = 1;
				}
				else
				{
					hari = daysInBetween;
				}
					
				
				String query4 = "INSERT INTO [dbo].[tr_hutang]([nobukti],[kredituraccount_id],[tanggal],[tanggaljt],[term],[totalhutang],[totallunas],[createdby] "
						+ ",[createdtime],[modifiedby],[modifiedtime],[db_id],[keterangan],[ms_comloc_id]) VALUES "
						+ "('"+nobukti+"','"+ids+"','"+tgl+"','"+tgljt+"',"+hari+","+total+",0,'"+user+"',CURRENT_TIMESTAMP,'"+user+"',CURRENT_TIMESTAMP,'PEMBELIAN','PEMBELIAN "+nobukti+"','HO')";
				PreparedStatement stmt4 = connection.prepareStatement(query4);
				stmt4.executeUpdate();
				viewJournal fin=new viewJournal();
				fin.setStatus("OK");
				all.add(fin);
				stmt4.close();
			}
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	public static List<viewJournal> addPiutang(String nobukti,String ids,String tgl,String user,String total, String tipe)
	{
		List<viewJournal> all = new  ArrayList<viewJournal>();
		Connection connection = null;
		if(tipe.equals("1"))
		{
			connection = DbConnection.getGlApotekConnection();
		}
		else if(tipe.equals("2"))
		{
			connection = DbConnection.getGlApotekConnection2();		
		}
		else
		{
			connection = DbConnection.getGlApotekConnection3();
		}
		Statement stmt =null;
		String query ="SELECT count(*) jum FROM tr_piutang where nobukti='"+nobukti+"'";
		try
		{
			stmt=connection.createStatement();
			ResultSet row =stmt.executeQuery(query);
			row.next();
			viewJournal li=new viewJournal();
			String jum=row.getString("jum");
			if(jum.equals("1"))
			{
				li.setStatus("ERROR");
				all.add(li);		
				row.close();
				stmt.close();
			}
			else
			{
				String query4="INSERT INTO [dbo].[tr_piutang]([nobukti],[debituraccount_id],[tanggal],[tanggaljt],[term],[totalpiutang],[totallunas],[keterangan],[createdby],[createdtime] "
						+ ",[modifiedby],[modifiedtime],[db_id],[ms_comloc_id]) VALUES "
						+ "('"+nobukti+"','"+ids+"','"+tgl+"','"+tgl+"',30,'"+total+"',0,'AR "+nobukti+"','"+user+"',CURRENT_TIMESTAMP,'"+user+"',CURRENT_TIMESTAMP,'PENJUALAN','HO')";
				PreparedStatement stmt4 = connection.prepareStatement(query4);
				stmt4.executeUpdate();
				viewJournal fin=new viewJournal();
				fin.setStatus("OK");
				all.add(fin);
				stmt4.close();
			}
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	
	
	public static List<viewJournal> addHutangDetail(String nobukti,String kategori,String sandi,BigDecimal qty,BigDecimal biaya, String tipe)
	{
		List<viewJournal> all = new  ArrayList<viewJournal>();
		Connection connection = null;
		if(tipe.equals("1"))
		{
			connection = DbConnection.getGlApotekConnection();
		}
		else if(tipe.equals("2"))
		{
			connection = DbConnection.getGlApotekConnection2();		
		}
		else
		{
			connection = DbConnection.getGlApotekConnection3();
		}
		Statement stmt =null;
		String query ="SELECT tr_hutang_id FROM tr_hutang where nobukti='"+nobukti+"'";
		try
		{
			stmt=connection.createStatement();
			ResultSet row =stmt.executeQuery(query);
			row.next();
			
			String id = row.getString("tr_hutang_id");
			if(id.equals(null) || id.equals(" "))
			{
				viewJournal li=new viewJournal();
				li.setStatus("ERROR");
				all.add(li);		
				row.close();
				stmt.close();
			}
			else
			{
				Integer jum = qty.intValue();
				Integer ns = biaya.intValue();
				Integer total = jum*ns;
				String query4 = "INSERT INTO [dbo].[tr_hutangdet] ([tr_hutang_id],[itemid],[ms_account_id],[keterangan],[qty],[biaya],[total]) "
						+ "VALUES ('"+id+"','"+kategori+"','"+sandi+"','Pembelian "+nobukti+"',"+qty+","+biaya+","+total+")";
				PreparedStatement stmt4 = connection.prepareStatement(query4);
				stmt4.executeUpdate();
				viewJournal fin=new viewJournal();
				fin.setStatus("OK");
				all.add(fin);
				stmt4.close();
			}
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	public static List<viewJournal> addPiutangDetail(String nobukti,String kategori,String sandi,BigDecimal qty,BigDecimal biaya, String tipe)
	{
		List<viewJournal> all = new  ArrayList<viewJournal>();
		Connection connection = null;
		if(tipe.equals("1"))
		{
			connection = DbConnection.getGlApotekConnection();
		}
		else if(tipe.equals("2"))
		{
			connection = DbConnection.getGlApotekConnection2();		
		}
		else
		{
			connection = DbConnection.getGlApotekConnection3();
		}
		Statement stmt =null;
		String query ="SELECT tr_piutang_id FROM tr_piutang where nobukti='"+nobukti+"'";
		try
		{
			stmt=connection.createStatement();
			ResultSet row =stmt.executeQuery(query);
			row.next();
			
			String id = row.getString("tr_piutang_id");
			if(id.equals(null) || id.equals(" "))
			{
				viewJournal li=new viewJournal();
				li.setStatus("ERROR");
				all.add(li);		
				row.close();
				stmt.close();
			}
			else
			{
				Integer jum = qty.intValue();
				Integer ns = biaya.intValue();
				Integer total = jum*ns;
				String query4 = "INSERT INTO [dbo].[tr_piutangdet] ([tr_piutang_id],[itemid],[ms_account_id],[keterangan],[qty],[biaya],[total]) "
						+ "VALUES ('"+id+"','"+kategori+"','"+sandi+"','Penjualan "+nobukti+"',"+qty+","+biaya+","+total+")";
				PreparedStatement stmt4 = connection.prepareStatement(query4);
				stmt4.executeUpdate();
				viewJournal fin=new viewJournal();
				fin.setStatus("OK");
				all.add(fin);
				stmt4.close();
			}
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally {
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
//---------------jurnal return
		public static List<viewJournal> addRtb(String nopo,String total,String tipe)
		{
			List<viewJournal> all = new  ArrayList<viewJournal>();
			Connection connection = null;
			
			if(tipe.equals("1"))
			{
				connection = DbConnection.getGlApotekConnection();
			}
			else if(tipe.equals("2"))
			{
				connection = DbConnection.getGlApotekConnection2();		
			}
			else
			{
				connection = DbConnection.getGlApotekConnection3();
			}
			
			Statement stmt =null;
			String query ="Select count(*) as jum from Tr_Jurnal where NoBukti='"+nopo+"'";
			try
			{
				stmt=connection.createStatement();
				ResultSet row =stmt.executeQuery(query);
				row.next();
				viewJournal li=new viewJournal();
				String jum=row.getString("jum");
				if(jum.equals("1"))
				{
					li.setStatus("ERROR");
					all.add(li);		
					row.close();
					stmt.close();
				}
				else
				{
					Statement stmt2 =null;
					String query2 = "INSERT INTO [dbo].[Tr_Jurnal] ([ComLocID],[NoBukti],[TipeJurnal],[Debet],[Kredit],[Posted],[TglPosting], "
							+ "[NoRef],[AutoJurnal],[CreatedBy],[CreatedTime],[ModifiedBy],[ModifiedTime],[Marked],[MarkedBy],"
							+ "[MarkedDate],[Tahun],[CetakBPK],[DiCetakOleh],[TglCetak])VALUES "
							+ "('HO','"+nopo+"','RETURN PEMBELIAN',"+total+","+total+",0,NULL,NULL,1,'khoman',CURRENT_TIMESTAMP,'-',CURRENT_TIMESTAMP,0,'',NULL,YEAR(GETDATE()),0,'',NULL)";
					stmt2=connection.createStatement();
					stmt2.executeUpdate(query2);
					viewJournal vj=new viewJournal();
					vj.setStatus("OK");
					all.add(vj);
					stmt2.close();
					row.close();
					stmt.close();
				}
				
			}
			catch (SQLException e)
			{
				System.out.println(e.getMessage());
			}
			finally {
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
			return all;
		}
		
		public static List<viewJournal> nilaiRtb(String nopo,String nsp,String tgl,String totalret,String ppn,String total,String kategori,String sandi,String tipe)
		{
			List<viewJournal> all = new  ArrayList<viewJournal>();
			Connection connection= null;
			if(tipe.equals("1"))
			{
				connection = DbConnection.getGlApotekConnection();
			}
			else if(tipe.equals("2"))
			{
				connection = DbConnection.getGlApotekConnection2();		
			}
			else
			{
				connection = DbConnection.getGlApotekConnection3();
			}
			Statement stmt =null;
			String query ="SELECT max(NoUrut) nourt FROM Tr_JurnalDet where NoBukti='"+nopo+"'";
			try
			{
				stmt=connection.createStatement();
				ResultSet row =stmt.executeQuery(query);
				row.next();
				String nour = row.getString("nourt");
				
				int num1 = 0;
				int num2 =0;
				int num3=0;
				if(nour.equals(null))
				{
					num1 = 0;
					num2 = num1 + 1;
					num3 = num1 + 2;	
				}
				else
				{
					int no = row.getInt("nourt");
					num1 = no + 1;
					num2 = no + 2;
					num3 = no + 3;	
				}
				
//				row.close();
//				stmt.close();	

				String query2 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+nopo+"','"+tgl+"','"+num1+"','"+sandi+"',NULL, '"+nsp+": Return Pembelian "+nopo+"','"+totalret+"',0,1,'"+total+"','"+nopo+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt2 = connection.prepareStatement(query2);
				stmt2.executeUpdate();
				stmt2.close();
				
				String query3 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+nopo+"','"+tgl+"','"+num2+"','2.1.06',NULL, '"+nsp+": PPN pembelian "+nopo+"',0,'"+ppn+"','10','"+ppn+"','"+nopo+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt3 = connection.prepareStatement(query3);
				stmt3.executeUpdate();
				stmt3.close();
				
				String query4 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
						+ "VALUES ('HO','"+nopo+"','"+tgl+"','"+num3+"','5.1.05','"+kategori+"', '"+nsp+": Return Pembelian "+kategori+" "+nopo+"',0,"+total+",1,"+total+",'"+nopo+"',NULL,YEAR(GETDATE()))";
				PreparedStatement stmt4 = connection.prepareStatement(query4);
				stmt4.executeUpdate();
				viewJournal fin=new viewJournal();
				fin.setStatus("OK");
				all.add(fin);
				stmt4.close();
				row.close();
				stmt.close();
			}
			catch (SQLException e)
			{
				System.out.println(e.getMessage());
			}
			finally {
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
			return all;
		}	
//jurnal penjualan
		public static List<viewJournal> addJpj(String idt,String total,String tipe,String user)
		{
			List<viewJournal> all = new  ArrayList<viewJournal>();
			Connection connection = null;
			
			if(tipe.equals("1"))
			{
				connection = DbConnection.getGlApotekConnection();
			}
			else if(tipe.equals("2"))
			{
				connection = DbConnection.getGlApotekConnection2();		
			}
			else
			{
				connection = DbConnection.getGlApotekConnection3();
			}
			
			Statement stmt =null;
			String query ="Select count(*) as jum from Tr_Jurnal where NoBukti='"+idt+"'";
			try
			{
				stmt=connection.createStatement();
				ResultSet row =stmt.executeQuery(query);
				row.next();
				viewJournal li=new viewJournal();
				String jum=row.getString("jum");
				if(jum.equals("1"))
				{
					li.setStatus("ERROR");
					all.add(li);		
					row.close();
					stmt.close();
				}
				else
				{
					Statement stmt2 =null;
//					String query2 = "INSERT INTO [dbo].[Tr_Jurnal] ([ComLocID],[NoBukti],[TipeJurnal],[Debet],[Kredit],[Posted],[TglPosting], "
//							+ "[NoRef],[AutoJurnal],[CreatedBy],[CreatedTime],[ModifiedBy],[ModifiedTime],[Marked],[MarkedBy],"
//							+ "[MarkedDate],[Tahun],[CetakBPK],[DiCetakOleh],[TglCetak])VALUES "
//							+ "('HO','"+idt+"','AP',"+total+","+total+",0,NULL,NULL,1,'"+user+"',CURRENT_TIMESTAMP,'-',CURRENT_TIMESTAMP,0,'',NULL,YEAR(GETDATE()),0,'',NULL)";
					String query2 = "INSERT INTO [dbo].[tr_jurnal] ([tahun],[ms_comloc_id],[nobukti],[tipejurnal],[debet],[kredit],[noref], "
							+ "[idref],[autojurnal],[tglposting],[createdby],[createdtime],[modifiedby],[modifiedtime],[marked],[markedby],[markeddate]) VALUES "
							+ "(YEAR(GETDATE()),'HO','"+idt+"','POS',"+total+","+total+",NULL,NULL,1,CURRENT_TIMESTAMP,'"+user+"',CURRENT_TIMESTAMP,'-',CURRENT_TIMESTAMP,NULL,NULL,CURRENT_TIMESTAMP)";
					stmt2=connection.createStatement();
					stmt2.executeUpdate(query2);
					viewJournal vj=new viewJournal();
					vj.setStatus("OK");
					all.add(vj);
					stmt2.close();
					row.close();
					stmt.close();
				}
				
			}
			catch (SQLException e)
			{
				System.out.println(e.getMessage());
			}
			finally {
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
			return all;
		}
		
		public static List<viewJournal2> addJpjDetail(String idt,String tglt,String dis,String sandi,String nasup,String total,String tipe)
		{
			List<viewJournal2> all = new  ArrayList<viewJournal2>();
			Connection connection= null;
			if(tipe.equals("1"))
			{
				connection = DbConnection.getGlApotekConnection();
			}
			else if(tipe.equals("2"))
			{
				connection = DbConnection.getGlApotekConnection2();		
			}
			else
			{
				connection = DbConnection.getGlApotekConnection3();
			}
			Statement stmt =null; 
			String query ="SELECT tr_jurnal_id FROM tr_jurnal where nobukti='"+idt+"'";
			try
			{
				stmt=connection.createStatement();
				ResultSet row =stmt.executeQuery(query);
				row.next();
				viewJournal2 li=new viewJournal2();
				String jum=row.getString("tr_jurnal_id");
				if(jum.equals(" ") || jum.equals(null))
				{
					li.setStatus("ERROR, DATA TIDAK DI TEMUKAN DI TABEL HEADER");
					all.add(li);		
					row.close();
					stmt.close();
				}
				else
				{
					Statement stmtdet =null;
					if(dis.equals("nol") || dis.equals("") || dis.equals("0.00"))
					{
//						String querydetail = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
//								+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
//								+ "VALUES ('HO','"+idt+"','"+tglt+"','1','"+sandi+"',NULL, '"+nasup+": Penjualan "+idt+"','"+total+"',0,1,'"+total+"','"+idt+"',NULL,YEAR(GETDATE()))";
						String querydetail = "INSERT INTO [dbo].[tr_jurnaldet] ([tr_jurnal_id],[tgltrans],[nourut],[itemid],[ms_account_id],[keterangan],[debet], "
								+ "[kredit],[totalunit],[hargaunit],[noref1],[noref2],[noref3]) "
								+ "VALUES "
								+ "('"+jum+"','"+tglt+"',0,'','"+sandi+"','"+nasup+": Penjualan "+idt+"','"+total+"',0,1,'"+total+"','','','')";
						stmtdet=connection.createStatement();
						stmtdet.executeUpdate(querydetail);
						stmtdet.close();
						
					}	
					else
					{
						String code = "";
						if(sandi.equals("1.1.05.005") || sandi.equals("1.1.05.006"))
						{
							code="4.1.06";
						}
						else
						{
							code="4.1.02";
						}
						
//						String querydetail = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
//								+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
//								+ "VALUES ('HO','"+idt+"','"+tglt+"','1','"+sandi+"',NULL, '"+nasup+": Penjualan "+idt+"','"+total+"',0,1,'"+total+"','"+idt+"',NULL,YEAR(GETDATE()))";
						String querydetail = "INSERT INTO [dbo].[tr_jurnaldet] ([tr_jurnal_id],[tgltrans],[nourut],[itemid],[ms_account_id],[keterangan],[debet], "
								+ "[kredit],[totalunit],[hargaunit],[noref1],[noref2],[noref3]) "
								+ "VALUES "
								+ "('"+jum+"','"+tglt+"',0,'','"+sandi+"','"+nasup+": Penjualan "+idt+"','"+total+"',0,1,'"+total+"','','','')";
						stmtdet=connection.createStatement();
						stmtdet.executeUpdate(querydetail);
						stmtdet.close();
						
//						String query3 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
//								+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
//								+ "VALUES ('HO','"+idt+"','"+tglt+"','2','4.1.02',NULL, '"+nasup+":  Penjualan "+idt+"','"+dis+"',0,1,'"+dis+"','"+idt+"',NULL,YEAR(GETDATE()))";
						String query3 = "INSERT INTO [dbo].[tr_jurnaldet] ([tr_jurnal_id],[tgltrans],[nourut],[itemid],[ms_account_id],[keterangan],[debet], "
								+ "[kredit],[totalunit],[hargaunit],[noref1],[noref2],[noref3]) "
								+ "VALUES "
								+ "('"+jum+"','"+tglt+"',1,'','"+code+"','"+nasup+": Penjualan "+idt+"','"+dis+"',0,1,'"+dis+"','','','')";
						PreparedStatement stmt3 = connection.prepareStatement(query3);
						stmt3.executeUpdate();
						stmt3.close();
						
					}
					viewJournal2 vj2=new viewJournal2();
					vj2.setStatus("OK");
					all.add(vj2);
					
					row.close();
					stmt.close();
					
				}
			}
			catch (SQLException e)
			{
				System.out.println(e.getMessage());
			}
			finally {
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
		return all;
		}
	
		public static List<viewJournal> addJpjNilai(String sandi,String tgl,String nsp,String idt,String tot,String kat, String tipe)
		{
			List<viewJournal> all = new  ArrayList<viewJournal>();
			Connection connection = null;
			if(tipe.equals("1"))
			{
				connection = DbConnection.getGlApotekConnection();
			}
			else if(tipe.equals("2"))
			{
				connection = DbConnection.getGlApotekConnection2();		
			}
			else
			{
				connection = DbConnection.getGlApotekConnection3();
			}
			Statement stmt =null;
			Statement stmt2 = null;
			String query ="SELECT tr_jurnal_id FROM tr_jurnal where nobukti='"+idt+"'";
			try
			{
				stmt=connection.createStatement();
				ResultSet row =stmt.executeQuery(query);
				row.next();
				viewJournal li=new viewJournal();
				String jum=row.getString("tr_jurnal_id");
				if(jum.equals(" ") || jum.equals(null))
				{
					li.setStatus("ERROR, DATA TIDAK DI TEMUKAN DI TABEL HEADER");
					all.add(li);		
					row.close();
					stmt.close();
				}
				else
				{
				
					String query2="SELECT max(nourut) nourt FROM tr_jurnaldet jd,tr_jurnal tj where tj.tr_jurnal_id =jd.tr_jurnal_id and tj.nobukti='"+idt+"'";
					stmt2=connection.createStatement();
					ResultSet row2 =stmt.executeQuery(query2);
					row2.next();
					int no = row2.getInt("nourt");
					int num1 = no + 1;
					
					
	//				String query4 = "INSERT INTO [dbo].[Tr_JurnalDet] ([ComLocID],[NoBukti],[TglTrans],[NoUrut],[AccID], "
	//						+ "[ItemID],[Keterangan],[Debet],[Kredit],[TotalUnit],[HargaUnit],[NoRef],[NoRef1],[Tahun])"
	//						+ "VALUES ('HO','"+idt+"','"+tgl+"','"+num1+"','"+sandi+"','"+kat+"', '"+nsp+": Penjualan "+kat+" "+idt+"',0,"+tot+",1,"+tot+",'"+idt+"',NULL,YEAR(GETDATE()))";
					String query4 = "INSERT INTO [dbo].[tr_jurnaldet] ([tr_jurnal_id],[tgltrans],[nourut],[itemid],[ms_account_id],[keterangan],[debet], "
							+ "[kredit],[totalunit],[hargaunit],[noref1],[noref2],[noref3]) "
							+ "VALUES "
							+ "('"+jum+"','"+tgl+"','"+num1+"','"+kat+"','"+sandi+"','"+nsp+": Penjualan "+kat+" "+idt+"',0,"+tot+",1,'"+tot+"','','','')";
					PreparedStatement stmt4 = connection.prepareStatement(query4);
					stmt4.executeUpdate();
					viewJournal fin=new viewJournal();
					fin.setStatus("OK");
					all.add(fin);
					stmt4.close();
					row2.close();
					row.close();
					stmt.close();
					stmt2.close();
				}
			}
			catch (SQLException e)
			{
				System.out.println(e.getMessage());
			}
			finally {
			     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
			     if (connection!=null) try { connection.close();} catch (Exception ignore){}
			   }
			return all;
		}
	
		
		
	public static List<ListInfoBalance> getCekBalanceNik(String nik) {
		List<ListInfoBalance> all=new ArrayList<ListInfoBalance>();
		Connection connection=DbConnection.getTrxInstance();
		if(connection == null)return null;
		Statement storedis=null;
		ResultSet hasdis = null;
		String abdis="SELECT accountbalance_id, nik, card_no, user_name, balance, company FROM t_accountbalance WHERE status=1 and nik LIKE '%"+nik+"%'";

		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListInfoBalance inrek=new ListInfoBalance();
				inrek.setAccountbalance_id(hasdis.getString("accountbalance_id"));
				inrek.setNik(hasdis.getString("nik"));
				inrek.setCard_no(hasdis.getString("card_no"));
				inrek.setUser_name(hasdis.getString("user_name"));
				inrek.setBalance(hasdis.getString("balance"));
				inrek.setCompany(hasdis.getString("company"));
				all.add(inrek);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
		    if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		    if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
	}

	public static List<ListInfoTransaksi> getCekTransaksiNik(String accountBlcId) {
		List<ListInfoTransaksi> all=new ArrayList<ListInfoTransaksi>();
		Connection connection=DbConnection.getTrxInstance();
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="SELECT invoice_no, payment_no, trx_date, old_amount, trx_amount, new_amount, created_at, created_by "
				+ "FROM t_accounttrx WHERE accountbalance_id='"+accountBlcId+"'";
		try {
			storedis=connection.createStatement();
			ResultSet hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListInfoTransaksi inrek=new ListInfoTransaksi();
				inrek.setInvoice_no(hasdis.getString("invoice_no"));
				inrek.setPayment_no(hasdis.getString("payment_no"));
				inrek.setTrx_date(hasdis.getString("trx_date"));
				inrek.setOld_amount(hasdis.getString("old_amount"));
				inrek.setTrx_amount(hasdis.getString("trx_amount"));
				inrek.setNew_amount(hasdis.getString("new_amount"));
				inrek.setCreated_at(hasdis.getString("created_at"));
				inrek.setCreated_by(hasdis.getString("created_by"));
				all.add(inrek);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
		    if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
	}

	public static Object getDataPasienSms() {
		List<ListDataPasienSms> all=new ArrayList<ListDataPasienSms>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet hasdis=null;
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="SELECT PERSON_ID, PERSON_NAME, MOBILE_PHONE_NO "
				+ "FROM PERSON "
				+ "WHERE MOBILE_PHONE_NO IS NOT NULL and LAST_UPDATED_DATETIME >= to_date('2016-01-01 00:00:01', 'yyyy-MM-dd HH24:MI:SS') and LAST_UPDATED_DATETIME<=to_date('2017-12-31 23:59:59', 'yyyy-MM-dd HH24:MI:SS') order by PERSON_ID desc";

		try {
			storedis=connection.createStatement();
			hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListDataPasienSms inrek=new ListDataPasienSms();
				inrek.setPERSON_ID(hasdis.getString("PERSON_ID"));
				inrek.setPERSON_NAME(hasdis.getString("PERSON_NAME"));
				inrek.setMOBILE_PHONE_NO(hasdis.getString("MOBILE_PHONE_NO"));
				all.add(inrek);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		finally {
		     if (hasdis!=null) try  { hasdis.close(); } catch (Exception ignore){}
		     if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<ListInfoViewTrxNew> getInfoViewTrxNew(String IdCompany) {
		List<ListInfoViewTrxNew> all=new ArrayList<ListInfoViewTrxNew>();
		Connection connection=DbConnection.getTrxInstance();
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="SELECT tab.accountbalance_id, tab.nik, tab.card_no, tab.user_name, tab.balance"
				+ " FROM t_accountbalance tab "
				+ " where tab.company='"+IdCompany+"' and tab.status='1'";
		try {
			storedis=connection.createStatement();
			ResultSet hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListInfoViewTrxNew inrek=new ListInfoViewTrxNew();
				inrek.setAccountbalance_id(hasdis.getString("accountbalance_id"));	
				inrek.setNik(hasdis.getString("nik"));
				inrek.setCard_no(hasdis.getString("card_no"));
				inrek.setUser_name(hasdis.getString("user_name"));
				inrek.setBalance(hasdis.getString("balance"));
				all.add(inrek);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
		    if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
	}
	
	
	public static List<ListInfoViewTrxNewDetail> getInfoViewTrxNewDetail(String fromDate, String toDate) {
		List<ListInfoViewTrxNewDetail> all=new ArrayList<ListInfoViewTrxNewDetail>();
		Connection connection=DbConnection.getTrxInstance();
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="SELECT ttx.accountbalance_id, ttx.trx_type, ttx.invoice_no, ttx.payment_no, ttx.old_amount, ttx.trx_amount, ttx.new_amount, ttx.trx_date" + 
					" FROM t_accounttrx ttx " + 
					" where ttx.trx_date>='"+fromDate+" 00:01:00' and ttx.trx_date<='"+toDate+" 23:59:59' order by ttx.accounttrx_id desc";
		try {
			storedis=connection.createStatement();
			ResultSet hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListInfoViewTrxNewDetail inrek=new ListInfoViewTrxNewDetail();
				inrek.setAccountbalance_id(hasdis.getString("accountbalance_id"));
				inrek.setTrx_type(hasdis.getString("trx_type"));	
				inrek.setInvoice_no(hasdis.getString("invoice_no"));
				inrek.setPayment_no(hasdis.getString("payment_no"));
				inrek.setOld_amount(hasdis.getString("old_amount"));
				inrek.setTrx_amount(hasdis.getString("trx_amount"));
				inrek.setNew_amount(hasdis.getString("new_amount"));
				inrek.setTrx_date(hasdis.getString("trx_date"));
				all.add(inrek);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
		    if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
	}

	public static List<ListInfoViewTrxNew> getInfoViewTrxNewNoCompa() {
		List<ListInfoViewTrxNew> all=new ArrayList<ListInfoViewTrxNew>();
		Connection connection=DbConnection.getTrxInstance();
		if(connection == null)return null;
		Statement storedis=null;
		String abdis="SELECT tab.accountbalance_id, tab.nik, tab.card_no, tab.user_name, tab.balance, tab.company"
				+ " FROM t_accountbalance tab "
				+ " where tab.status='1' order by tab.company asc";
		try {
			storedis=connection.createStatement();
			ResultSet hasdis=storedis.executeQuery(abdis);
			while (hasdis.next())
			{
				ListInfoViewTrxNew inrek=new ListInfoViewTrxNew();
				inrek.setAccountbalance_id(hasdis.getString("accountbalance_id"));	
				inrek.setNik(hasdis.getString("nik"));
				inrek.setCard_no(hasdis.getString("card_no"));
				inrek.setUser_name(hasdis.getString("user_name"));
				inrek.setBalance(hasdis.getString("balance"));
				inrek.setCompany(hasdis.getString("company"));
				all.add(inrek);
			}
			hasdis.close();
			storedis.close();
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
		    if (storedis!=null) try  { storedis.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
	}

	public static ReturnResult GetDelQueuePhar(QueuePhar DataPhar)
	{
		ReturnResult ret = new ReturnResult(); 
		String message = "OK";
		Boolean success = true;
		Connection connection=DbConnection.getQueueInstance();
		if(connection==null)return null;
		
		String SPsql = "EXEC CompletePharmacy ?,?,?";   // for stored proc taking 2 parameters
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try 
		{
			ps = connection.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, DataPhar.getLocation());
			ps.setString(2, DataPhar.getQueue_no());
			ps.setString(3, DataPhar.getUser());
			rs =  ps.executeQuery();
			
			while(rs.next())
			{
				message = rs.getString("mess");
			}
			
		} 
		
		catch (SQLException e) 
		{
			success = false;
			message ="Failed to Create Account, Error : "+e.getMessage(); 
		} finally {
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		ret.setSuccess(success);
		ret.message = message;
		return ret;
	}

//	public static List<ListgetDataSms> getDataSms(String nohp, String pesan, String keterangan, String status) {
//		List<ListgetDataSms> all=new ArrayList<ListgetDataSms>();
//		Connection connection=DbConnection.getsmsconnection();
//		if(connection == null)return null;
//		Statement storedis=null;
//		String abdis="Insert into tb_datasms values('"+nohp+"','"+pesan+"','"+keterangan+"','"+status+"')";
//		try {
//			storedis=connection.createStatement();
//			ResultSet hasdis=storedis.executeQuery(abdis);
//			hasdis.next();			
//			connection.close();
//			hasdis.close();
//			storedis.close();
//		} catch (SQLException e) {
//			// TODO: handle exception
//			System.out.println(e.getMessage());
//		}
//		return all;
//	}
}
