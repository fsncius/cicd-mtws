package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.apache.http.util.TextUtils;
import org.apache.openjpa.kernel.exps.Constant;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mysql.jdbc.Statement;
import com.rsmurniteguh.webservice.dep.all.model.AbsensiView;
import com.rsmurniteguh.webservice.dep.all.model.PaymentModel;
import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.ReturnResult;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.AppointmentStatus;
import com.rsmurniteguh.webservice.dep.all.model.gym.AccountBalance;
import com.rsmurniteguh.webservice.dep.all.model.gym.AccountBalanceParam;
import com.rsmurniteguh.webservice.dep.all.model.gym.CreateMember;
import com.rsmurniteguh.webservice.dep.all.model.gym.ExtendMembership;
import com.rsmurniteguh.webservice.dep.all.model.gym.InfoMember;
import com.rsmurniteguh.webservice.dep.all.model.gym.ResponseGym;
import com.rsmurniteguh.webservice.dep.all.model.gym.TrxMember;
import com.rsmurniteguh.webservice.dep.biz.MtGymBiz;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.DataUtils;
import com.securecontext.secure.EncryptorConnection;

public class MtGymSer {

	private static final String COMPANY = "GM";
	private static final double COST_GYM_A_MONTH = 0;

	public static AccountBalance info_account_from_cardno(String cardno) {
		//System.out.println(EncryptorConnection.getInstance().encrypt("${db.mydb.url}=jdbc:oracle:thin:@oracledb.homtmh.local:1521:KTHIS|${db.mydb.uid}=IHIS|${db.mydb.pwd}=IHIS"));
		System.out.println("info_account_from_rfidno...");
		
		AccountBalance data=new AccountBalance();
		
		Connection connection=DbConnection.getTrxInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT accountbalance_id, nik, card_no, nfc_no, user_name, user_detail, balance, remarks, status, company "
				+ "FROM t_accountbalance "
				+ "WHERE deleted = 0 "
					+ "AND (card_no = ? or nfc_no = ?) AND status = 1 ";
		
		try {
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, cardno);
			ps.setString(2, cardno);
			rs = ps.executeQuery();
			
			if(rs.next())
			{	
				HashMap<String,String> user_detail = new Gson().fromJson(rs.getString("user_detail"), new TypeToken<HashMap<String, String>>(){}.getType());
				
				data.setAccountbalance_id(rs.getString("accountbalance_id"));
				data.setNik(rs.getString("nik"));
				data.setCardno(rs.getString("card_no"));
				data.setNfcno(rs.getString("nfc_no"));
				data.setUsername(rs.getString("user_name"));
				data.setUser_detail(user_detail);
				data.setBalance(rs.getString("balance"));
				data.setRemarks(rs.getString("remarks"));
				data.setStatus(rs.getString("status"));
				data.setCompany(rs.getString("company"));
				if (rs.getString("nfc_no")!=null) {
					if (rs.getString("nfc_no").equals(cardno)) data.setFromNfc(true);
				}
			}
			
			connection.commit();
		}
		
		catch (SQLException e)
		{
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back in "+e.getStackTrace()[0].getMethodName());
	                connection.rollback();
	            } catch(SQLException excep) {
	            	System.out.println(excep.getMessage());
	            }
	        }
		}
		finally
		{	
		     try { connection.setAutoCommit(true); } catch (SQLException e) {}
		 	 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	public static ResponseGym update_account(AccountBalance accountBalance) {
		/*
		 * yang dapat di update hanya field tertentu (nfc_no & userdetail)
		 * meskipun user baru terdaftar di account tetap untuk nfc_no dan userdetail disimpan disini
		 */
		
		System.out.println("info_account_from_rfidno...");
		
		ResponseGym data=new ResponseGym();
		
		Connection connection=DbConnection.getTrxInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT accountbalance_id, nik, card_no, nfc_no, user_name, user_detail, balance, remarks, status, company "
				+ "FROM t_accountbalance "
				+ "WHERE deleted = 0 "
					+ "AND (card_no = ? or nfc_no = ?)  AND status = 1";
		
		try {
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, "");
			ps.executeQuery();
			
			
			connection.commit();
		}
		
		catch (SQLException e)
		{
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back in "+e.getStackTrace()[0].getMethodName());
	                connection.rollback();
	            } catch(SQLException excep) {
	            	System.out.println(excep.getMessage());
	            }
	        }
		}
		finally
		{	
		     try { connection.setAutoCommit(true); } catch (SQLException e) {}
		 	 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}
	
	private static PaymentModel convert(AccountBalance ab){
		PaymentModel paymentModel = new PaymentModel();
        paymentModel.setAccountbalance_id(ab.getAccountbalance_id());
        paymentModel.setCardNo(ab.getCardno());
        paymentModel.setCardName(ab.getUsername());
        paymentModel.setNfcNo(ab.getNfcno());
//        paymentModel.setUserId(ab.getCreated_by());
        paymentModel.setUserId("0"); //must_change
        paymentModel.setNik(ab.getNik());
        paymentModel.setStatuskr("1");
        paymentModel.setPin("1111");
        paymentModel.setStatusperusahaan(ab.getCompany());

        return paymentModel;
	}
	
	private static class RegisterAccountProcResult{
		private String mess;
		private Integer res;

		public Integer getRes() {
			return res;
		}

		public void setRes(Integer res) {
			this.res = res;
		}

		public String getMess() {
			return mess;
		}

		public void setMess(String mess) {
			this.mess = mess;
		}
	}
	
	public static ResponseStatus create_account(AccountBalanceParam accountBalanceParam) {
		ResponseStatus result = newResponseStatus();
		if(accountBalanceParam == null)return fail("Parameter tidak boleh kosong");
		if(accountBalanceParam.getAccountBalance() == null) return fail("Parameter account balance tidak boleh kosong.");
		try{
			AccountBalance accountBalance = accountBalanceParam.getAccountBalance();
			String cardNo = accountBalance.getCardno();
			String nfcNo = accountBalance.getNfcno();
			Gson gson = new Gson();
			if(TextUtils.isEmpty(cardNo) && TextUtils.isEmpty(nfcNo))return fail("Nomor kartu atau nfc harus diisi.");
			if(accountBalanceParam.isNewAccount()){
				String userName = accountBalance.getUsername();
				Long userId = accountBalanceParam.getUserId();
				String nik = accountBalance.getNik();
				String status = accountBalance.getStatus();
				status = "1";
				String company = accountBalance.getCompany();
				String userDetail = gson.toJson(accountBalance.getUser_detail());
				String query = "EXEC registeraccount ?,?,?,?,?,?,?,?";
				Object[] params = new Object[]{ cardNo, nfcNo, userName, userId, nik, status, company, userDetail};
				List rawCardData = DbConnection.executeReader(DbConnection.TRX, query, params);
				RegisterAccountProcResult data = new RegisterAccountProcResult();
				if(rawCardData == null) return fail("Gagal procedure.");
				if(rawCardData.isEmpty())return fail("Gagal procedure kosong");
				if(rawCardData.size()!= 1) return fail("Gagal procedure lebih dari 1");
				if(rawCardData.size() == 1){
					HashMap rowInfo = (HashMap)rawCardData.get(0);
					data = getMappedClass(rowInfo, RegisterAccountProcResult.class);
					if(data.getRes() == 1)result = success(TextUtils.isEmpty(data.getMess()) ? "Berhasil membuat akun baru." : data.getMess());
					else result = fail(TextUtils.isEmpty(data.getMess()) ? "Procedure gagal." : data.getMess());
					
				}
			}else{
				ReturnResult rawData = DepoManagementService.UpdateAccount(convert(accountBalance));
				Boolean success = rawData.getSuccess();
				if(success) result = success("Berhasil membuat akun baru.");
				else result = fail("Gagal membuat akun.");
			}
			
		}catch(Exception ex){
			ex.fillInStackTrace();
			System.out.println(ex);
		}
		return result;
	}
	
	public static Object info_member(String nfcno) {
		System.out.println("info_member...");
		
		InfoMember data=new InfoMember();
		List<ExtendMembership> le = new ArrayList<ExtendMembership>();

		Connection connection=DbConnection.getGymInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql = "SELECT id_membership, valid_from, valid_through, status, remarks, CONVERT(date, created_at) registration_date "
				+ "FROM tb_membership "
				+ "WHERE deleted = 0 "
					+ "AND accountbalance_id = ? "
					+ "AND (SYSDATETIME() BETWEEN valid_from AND valid_through OR valid_through > SYSDATETIME()) "
				+ "ORDER BY valid_through ASC ";
		

		try {
			AccountBalance accountBalance = info_account_from_cardno(nfcno);
			data.setInfo_account(accountBalance);
			if (!accountBalance.isFromNfc()) return data;

				Calendar now = Calendar.getInstance();
				Date datenow = now.getTime();				
				
				data.setAccountbalance_id(accountBalance.getAccountbalance_id());
				data.setNfcno(accountBalance.getNfcno());
				data.setValid(false);
				
				ps = connection.prepareStatement(sql);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				ps.setString(1, accountBalance.getAccountbalance_id());
				rs = ps.executeQuery();
				int index = 0;
				while (rs.next()) {
					if(index ==0)data.setFirstMemberId(rs.getString("id_membership"));
					ExtendMembership dl = new ExtendMembership();
					dl.setId_membership( rs.getString("id_membership"));
					dl.setValid_from(rs.getString("valid_from"));
					dl.setValid_through(rs.getString("valid_through"));
					dl.setRemarks(rs.getString("remarks"));
					dl.setStatus(rs.getString("status"));
					dl.setRegistration_date(rs.getString("registration_date"));

					Date startdate = DataUtils.DATE_FORMAT_DB.parse(rs.getString("valid_from"));
					Date enddate = DataUtils.DATE_FORMAT_DB.parse(rs.getString("valid_through"));
					
					if ((datenow.before(enddate) && datenow.after(startdate))) {
						data.setValid_from(rs.getString("valid_from"));
						data.setValid_through(rs.getString("valid_through"));
						data.setValid(true);
					} else if (datenow.before(enddate)){
						data.setValid_from(rs.getString("valid_from"));
						data.setValid_through(rs.getString("valid_through"));
						data.setValid(true);
					}
					
					le.add(dl);
					index++;
				}
			data.setExtendMemberlist(le);
		}
		
		catch (SQLException e)
		{
			e.fillInStackTrace();
			System.out.println(e.getMessage());
		} catch (ParseException e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage());
		}
		finally
		{	
			 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		    
		}
		return data;
	}

	public static Object create_member(CreateMember create_member) {
		boolean testing = true;
		System.out.println("create_member...");
		
		ResponseGym data=new ResponseGym();
		
		Connection connection=DbConnection.getGymInstance();
		if(connection==null)return null;
		ResultSet generatedKeys = null;
		ResultSet generatedKeys2 = null;
		PreparedStatement ps = null, ps2 = null, ps3 = null, ps4 = null;
		
		
		String sql = "INSERT INTO tb_membership ( accountbalance_id, valid_from, valid_through, status, remarks, created_at, created_by, updated_at, updated_by, deleted) "
				+"VALUES (?,?,?,?,?,?,?,?,?,?) ";
		String sql2 = "INSERT INTO tb_trx ( trx_amount,created_at, created_by, nfc_no, status, type) "
				+"VALUES (?,?,?,?,?,?) ";
		String sql3 = "UPDATE tb_trx SET payment_no = ?, invoice_no = ? "
				+"WHERE id_trx = ? ";
		String sql4 = "UPDATE tb_membership SET id_trx = ? "
				+"WHERE id_membership = ? ";
		
		try {
			double balance = 0;
			ReturnResult rr_balance = DepoManagementService.GetBalance(create_member.getNfcno());
			PaymentModel paymentModel = new PaymentModel();
			balance = Double.parseDouble(rr_balance.getMessage());
			
			if ((!rr_balance.getSuccess() || balance <= COST_GYM_A_MONTH ) && !testing) {
				data.setResponse(false);
				data.setMessage("cant get balance or not enough");
				return data;
			}
			
			
	        /*
	         * CREATED VALID THROUGH
	         * IF NOT SET = TODAY
	         */
			SimpleDateFormat df_in = new SimpleDateFormat("dd-MM-yyyy");
	        SimpleDateFormat df_out = new SimpleDateFormat("yyyy-MM-dd");
			
	        Calendar cal = Calendar.getInstance();
	        String dateFrom = df_out.format(cal.getTime());
	        String dateTo = df_out.format(cal.getTime());
	        try {
	        	Date d_tanggal = df_in.parse(create_member.getValid_from());
	        	dateFrom = df_out.format(d_tanggal);
	        	cal.setTime(d_tanggal);
	        	cal.add(Calendar.MONTH, 1);
	        	dateTo = df_out.format(cal.getTime());
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			
	        
	        /*
	         * INSERTED PROCESS
	         */
			AccountBalance account_balance = info_account_from_cardno(create_member.getNfcno());
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, account_balance.getAccountbalance_id()); //accountbalance_id
			ps.setString(2, dateFrom); //valid_from
			ps.setString(3, dateTo); //valid_through
			ps.setString(4, "1"); //status --override to 1 that means active
			ps.setString(5, ""); //remarks
			ps.setTimestamp(6, new Timestamp(System.currentTimeMillis())); //created_at
			ps.setString(7, create_member.getCreated_by()); //created_by
			ps.setTimestamp(8, new Timestamp(System.currentTimeMillis())); //updated_at
			ps.setString(9, create_member.getCreated_by()); //updated_by
			ps.setString(10, "0"); //deleted
			ps.execute();
			String id_member = "";
			generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				id_member = generatedKeys.getString(1);
	        } else {
	        	data.setResponse(false);
				data.setMessage("generate id_member failed");
	            throw new SQLException("Creating generate id_member failed, no generated key obtained.");
	        }
			
			
			/*
			 * INSERT TRX LOG
			 */
			ps2 = connection.prepareStatement(sql2,Statement.RETURN_GENERATED_KEYS);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(60000);
			ps2.setString(1, String.valueOf(COST_GYM_A_MONTH)); //trx_ammount
			ps2.setTimestamp(2, new Timestamp(System.currentTimeMillis())); //created_at
			ps2.setString(3, create_member.getCreated_by()); //valid_through
			ps2.setString(4, create_member.getNfcno()); // nfc
			ps2.setString(5, "1"); //status --override to 1 that means active
			ps2.setString(6, "PAY"); //status --override to 1 that means active
			ps2.execute();
			String unique_id = "";
			generatedKeys2 = ps2.getGeneratedKeys();
			if (generatedKeys2.next()) {
				unique_id = generatedKeys2.getString(1);
				paymentModel.setInvoiceNo(unique_id);
	        } else {
	        	data.setResponse(false);
				data.setMessage("generate inv. no failed");
	            throw new SQLException("Creating generate inv. no failed, no generated key obtained.");
	        }
			
			/*
			 * FUNCTION DECREASE E-MONEY
			 */
			paymentModel.setCardNo(create_member.getNfcno());
			paymentModel.setTrxAmount(COST_GYM_A_MONTH);
			paymentModel.setUserId(create_member.getCreated_by());
			if (testing) {
				paymentModel.setPaymentNo("GYM_NO_DECREASE_EMONEY");
			}else{
//				ReturnResult do_trx = DepoManagementService.postPayment(paymentModel); // --when done get all parameter arg
//				if (!do_trx.getSuccess()) {
//					data.setResponse(false);
//					data.setMessage("trx failed");
//					throw new SQLException("trx failed.");
//				}
//				paymentModel.setPaymentNo(do_trx.getMessage());
			}
			
			/*
			 * UPDATE TRX LOG after get iv_no and payment no
			 */
			ps3 = connection.prepareStatement(sql3);
			ps3.setEscapeProcessing(true);
			ps3.setQueryTimeout(60000);
			ps3.setString(1, paymentModel.getPaymentNo()); //payment_no
			ps3.setString(2, paymentModel.getInvoiceNo()); //inv_no
//			ps3.setString(2, "-"); //inv_no
			ps3.setString(3, unique_id); //inv_no
//			ps3.setString(3, "0"); //inv_no
			ps3.executeUpdate();
			
			
			
			
			/*
			 * UPDATE MEMBERSHIP after get iv_no and payment no to set trx_id
			 */
			ps4 = connection.prepareStatement(sql4);
			ps4.setEscapeProcessing(true);
			ps4.setQueryTimeout(60000);
			ps4.setString(1, unique_id); //payment_no
//			ps4.setString(1, "0"); //payment_no
			ps4.setString(2, id_member); //inv_no
			ps4.executeUpdate();
			
			
			connection.commit();
			data.setResponse(true);
			data.setMessage("OK");
		}
		
		catch (SQLException e)
		{
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			data.setMessage(e.getMessage());
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back in "+e.getStackTrace()[0].getMethodName());
	                connection.rollback();
	            } catch(SQLException excep) {
	            	System.out.println(excep.getMessage());
	            }
	        }
		}
		finally
		{	
		     try { connection.setAutoCommit(true); } catch (SQLException e) {}
		     if (generatedKeys2!=null) try  { generatedKeys2.close(); } catch (Exception ignore){}
		     if (generatedKeys!=null) try  { generatedKeys.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}
		     if (ps4!=null) try  { ps4.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}

	public static Object topup_account(String total, String nfcno, String created_by) {
		ResponseGym res = new ResponseGym();
		PaymentModel model = new PaymentModel();
		
		Connection connection=DbConnection.getGymInstance();
		if(connection==null)return null;
		ResultSet generatedKeys = null;
		PreparedStatement ps = null, ps2 = null;
		
		String sql = "INSERT INTO tb_trx ( trx_amount,created_at, created_by, nfc_no, invoice_no, status, type) "
				+"VALUES (?,?,?,?,?,?,?) ";
		String sql2 = "UPDATE tb_trx SET payment_no = ? "
				+"WHERE id_trx = ? ";
		
		try {
			connection.setAutoCommit(false);
			ReturnResult generate_inv = DepoManagementService.getGeneratedDepositInvoice();
			
			/*
			 * INSERT TRX LOG
			 */
			ps = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, String.valueOf(total)); //trx_ammount
			ps.setTimestamp(2, new Timestamp(System.currentTimeMillis())); //created_at
			ps.setString(3, created_by); //valid_through
			ps.setString(4, nfcno); //status --override to 1 that means active
			ps.setString(5, generate_inv.getMessage()); // inv
			ps.setString(6, "1"); //status --override to 1 that means active
			ps.setString(7, "DEP"); 
			ps.execute();
			String unique_id = "", trx_id="";
			generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				trx_id = generatedKeys.getString(1);
				unique_id = generatecode_gym(trx_id);
				
				model.setPaymentNo(unique_id);
	        } else {
	        	res.setResponse(false);
	        	res.setMessage("generate inv. no failed");
	            throw new SQLException("Creating generate inv. no failed, no generated key obtained.");
	        }
			
			/*
			 * UPDATE TRX LOG after get iv_no and payment no
			 */
			ps2 = connection.prepareStatement(sql2);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(60000);
			ps2.setString(1, unique_id); //payment_no
			ps2.setString(2, trx_id); 
			ps2.executeUpdate();
			
			model.setCardNo(nfcno);
			model.setTrxAmount(new Double(total));
			model.setInvoiceNo(generate_inv.getMessage());
			model.setUserId(created_by);
			
			ReturnResult depo_trx = DepoManagementService.postPayment(model);
			if (depo_trx.getSuccess()) {
				res.setResponse(depo_trx.getSuccess());
				res.setMessage(depo_trx.getMessage());
			}else{
				res.setResponse(false);
	        	res.setMessage("postPayment failed");
	            throw new SQLException("postPayment failed");
			}
			
		} catch (Exception e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			res.setMessage(e.getMessage());
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back in "+e.getStackTrace()[0].getMethodName());
	                connection.rollback();
	            } catch(SQLException excep) {
	            	System.out.println(excep.getMessage());
	            }
	        }
		}finally {
			try { connection.setAutoCommit(true); } catch (SQLException e) {}
		     if (generatedKeys!=null) try  { generatedKeys.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
			
		
		return res;
	}
	
	public static Object cancel_topup_account(String invoice_no, String created_by) {
		ResponseGym res = new ResponseGym();
		PaymentModel model = new PaymentModel();
		
		Connection connection=DbConnection.getGymInstance();
		if(connection==null)return null;
		ResultSet generatedKeys = null;
		PreparedStatement ps = null, ps2 = null, ps3 = null, ps4 = null;
		ResultSet rs = null;

		String sql = "SELECT trx_amount, id_trx, nfc_no FROM tb_trx WHERE invoice_no = ? AND status = '1'";
		String sql1 = "INSERT INTO tb_trx ( trx_amount,created_at, created_by, nfc_no, invoice_no, status, type) "
				+"VALUES (?,?,?,?,?,?,?) ";
		String sql2 = "UPDATE tb_trx SET payment_no = ? "
				+"WHERE id_trx = ? ";
		String sql3 = "UPDATE tb_trx SET status = ? "
				+"WHERE id_trx = ? ";
		
		
		try {
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, invoice_no);
			rs = ps.executeQuery();
			
			if (rs.next()) {

				ReturnResult generate_inv = DepoManagementService.getGeneratedDepositInvoiceCan();
			
				/*
				 * INSERT TRX LOG
				 */
				ps2 = connection.prepareStatement(sql1,Statement.RETURN_GENERATED_KEYS);
				ps2.setEscapeProcessing(true);
				ps2.setQueryTimeout(60000);
				ps2.setString(1, rs.getString("trx_amount")); //trx_ammount
				ps2.setTimestamp(2, new Timestamp(System.currentTimeMillis())); //created_at
				ps2.setString(3, created_by); //valid_through
				ps2.setString(4, rs.getString("nfc_no")); //status --override to 1 that means active
				ps2.setString(5, generate_inv.getMessage());
				ps2.setString(6, "1"); //status --override to 1 that means active
				ps2.setString(7, "CAN"); //status --override to 1 that means active
				ps2.execute();
				String unique_id = "", trx_id="";
				generatedKeys = ps2.getGeneratedKeys();
				if (generatedKeys.next()) {
					trx_id = generatedKeys.getString(1);
					unique_id = generatecode_gym(trx_id);
					
					
					model.setPaymentNo(unique_id);
		        } else {
		        	res.setResponse(false);
		        	res.setMessage("generate inv. no failed");
		            throw new SQLException("Creating generate inv. no failed, no generated key obtained.");
		        }
				
				/*
				 * UPDATE TRX LOG after get iv_no and payment no
				 */
				ps3 = connection.prepareStatement(sql2);
				ps3.setEscapeProcessing(true);
				ps3.setQueryTimeout(60000);
				ps3.setString(1, unique_id); //
				ps3.setString(2, trx_id); //
				ps3.executeUpdate();
				
				ps4 = connection.prepareStatement(sql3);
				ps4.setEscapeProcessing(true);
				ps4.setQueryTimeout(60000);
				ps4.setString(1, "0"); //
				ps4.setString(2, rs.getString("id_trx")); //
				ps4.executeUpdate();
				
				model.setCardNo(rs.getString("nfc_no"));
				model.setTrxAmount(new Double(rs.getString("trx_amount")));
				model.setInvoiceNo(generate_inv.getMessage());
				model.setUserId(created_by);
				
				ReturnResult depo_trx = DepoManagementService.postPayment(model);
				if (depo_trx.getSuccess()) {
					res.setResponse(depo_trx.getSuccess());
					res.setMessage(depo_trx.getMessage());
				}else{
					res.setResponse(false);
		        	res.setMessage("postPayment failed");
		            throw new SQLException("postPayment failed");
				}
			
			}
			
		} catch (Exception e) {
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			res.setMessage(e.getMessage());
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back in "+e.getStackTrace()[0].getMethodName());
	                connection.rollback();
	            } catch(SQLException excep) {
	            	System.out.println(excep.getMessage());
	            }
	        }
		}finally {
			try { connection.setAutoCommit(true); } catch (SQLException e) {}
		     if (generatedKeys!=null) try  { generatedKeys.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (ps3!=null) try  { ps3.close(); } catch (Exception ignore){}
		     if (ps4!=null) try  { ps4.close(); } catch (Exception ignore){}
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
			
		
		return res;
	}
	
	public static Object member() {
		System.out.println("member...");
		
		InfoMember data=new InfoMember();
		List<ExtendMembership> le = new ArrayList<ExtendMembership>();

		Connection connection=DbConnection.getGymInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql = "SELECT id_membership, valid_from, valid_through, status, remarks "
				+ "FROM tb_membership "
				+ "WHERE deleted = 0 "
					//+ "AND accountbalance_id = ? "
					+ "AND SYSDATETIME() BETWEEN valid_from AND valid_through OR valid_through > SYSDATETIME() "
				+ "ORDER BY valid_through ASC ";
		

		try {	
				ps = connection.prepareStatement(sql);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				rs = ps.executeQuery();
				
				while (rs.next()) {
					ExtendMembership dl = new ExtendMembership();
					dl.setId_membership( rs.getString("id_membership"));
					dl.setValid_from(rs.getString("valid_from"));
					dl.setValid_through(rs.getString("valid_through"));
					dl.setRemarks(rs.getString("remarks"));
					dl.setStatus(rs.getString("status"));
					le.add(dl);
				}
		}
		
		catch (SQLException e)
		{
			e.fillInStackTrace();
			System.out.println(e.getMessage());
		}
		finally
		{	
			 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		    
		}
		return data;
	}
	
	public static Object trx(String date,String type){
		
		List<TrxMember> ltm = new ArrayList<TrxMember>();

		Connection connection=DbConnection.getGymInstance();
		Connection connection2=DbConnection.getTrxInstance();
		if(connection==null || connection2==null)return null;
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null, rs2 = null;
		
		String filter = "";
		if (type!=null) {
			if (type.equals("Deposit")) filter = " AND t.type = 'DEP' ";
			if (type.equals("Payment")) filter = " AND t.type = 'PAY' "; 
		}
		
		
		String sql = "SELECT t.id_trx, t.payment_no, t.trx_amount, t.invoice_no, t.created_at, t.created_by, t.nfc_no, t.type "
				+ "FROM tb_trx t  "
				//+ "WHERE status = 1  " + filter
				+ "WHERE status = 1 AND type != 'CAN' AND CONVERT(date, created_at) = ? " + filter
				+ "ORDER BY created_at DESC ";
		
		String sql2 = "SELECT user_name, user_detail, balance, nfc_no "
				+ "FROM t_accountbalance  "
				+ "WHERE status = 1 AND deleted = 0  AND nfc_no IN [WHERE_IN] AND active = 1 AND deleted = 0 ";

		try {	
			
			SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
	        String tgl_out = df.format(cal.getTime());
	        try {
	        	Date d_tanggal = df.parse(date);
	        	tgl_out = df.format(d_tanggal);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			
				ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				ps.setString(1, tgl_out);
				rs = ps.executeQuery();
				
				String where_in = "(";
				while (rs.next()) {
					TrxMember dl = new TrxMember();
					dl.setId_trx( rs.getString("id_trx"));
					dl.setPayment_no(rs.getString("payment_no"));
					dl.setTrx_amount(rs.getString("trx_amount"));
					dl.setInvoice_no(rs.getString("invoice_no"));
					dl.setCreated_by(rs.getString("created_by"));
					dl.setNfc_no(rs.getString("nfc_no"));
					dl.setType(rs.getString("type"));
					ltm.add(dl);
					
					if (rs.isLast()) {
						where_in = where_in+"'"+rs.getString("nfc_no")+"')";
					}else {
						where_in = where_in+"'"+rs.getString("nfc_no")+"',";
					}
				}
				
				if (!where_in.equals("(")) {
					ps2 = connection2.prepareStatement(sql2.replace("[WHERE_IN]", where_in));
					ps2.setEscapeProcessing(true);
					ps2.setQueryTimeout(60000);
					rs2=ps2.executeQuery();
					
					while(rs2.next())
					{
						
						for (TrxMember tm : ltm) {
							if (tm.getNfc_no()!=null ) {
								if (tm.getNfc_no().equals(rs2.getString("nfc_no"))) {
									tm.setUsername(rs2.getString("user_name"));
									tm.setBalance(rs2.getString("balance"));
									tm.setUser_detail(rs2.getString("user_detail"));
								}
							}
						}
					}
				}
				
				// hapus data yang null
				for (Iterator<TrxMember> iter = ltm.iterator(); iter.hasNext(); ) {
					TrxMember dl = iter.next();
				    if (dl.getUsername()==null ) {
				        iter.remove();
				    }
				}
				
		}
		
		catch (SQLException e)
		{
			e.fillInStackTrace();
			System.out.println(e.getMessage());
		}
		finally
		{	
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		     
		     if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (connection2!=null) try { connection2.close();} catch (Exception ignore){}
		    
		}
		return ltm;
	}
	
	public static String generatecode_gym(String keyid) {
		SimpleDateFormat tahun = new SimpleDateFormat("yyyy",Locale.US);
        SimpleDateFormat bulan = new SimpleDateFormat("MM",Locale.US);
        SimpleDateFormat hari = new SimpleDateFormat("dd",Locale.US);

        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        
        return "GYM"+tahun.format(date)+bulan.format(date)+"-"+String.format("%06d", Integer.parseInt(keyid));    
	}
	
	public static Object get_rfid(String machineNo){
		// sebelum kesini lakukan terlebih dahulu 'prepare get mechine id'  -- PrepareGetCardDataFromMachine
		ResponseStatus data=new ResponseStatus();
		
		Connection connection=DbConnection.getTrxInstance();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select e.card_num1+e.card_num2 as cardNo from events e inner join profile_cards pc on pc.hw_num = e.hw_num and pc.user_num = e.user_num "
				+ " and pc.card_num1 = e.card_num1 and pc.card_num2 = e.card_num2 "
				+ "where e.door_fullid = ? and e.event_msg like '%Normal%' and e.status='0' order by e.date_created desc";
		
		try {
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setString(1, machineNo);
			rs = ps.executeQuery();
			
			if(rs.next())
			{	
				data.setSuccess(true);
				data.setMessage(rs.getString("cardNo"));
				//DepoManagementService.PrepareGetCardDataFromMachine(machineNo);
			}
			
			connection.commit();
		}
		
		catch (SQLException e)
		{
			e.fillInStackTrace();
			System.out.println(e.getMessage());
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back in "+e.getStackTrace()[0].getMethodName());
	                connection.rollback();
	            } catch(SQLException excep) {
	            	System.out.println(excep.getMessage());
	            }
	        }
		}
		finally
		{	
		     try { connection.setAutoCommit(true); } catch (SQLException e) {}
		 	 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return data;
	}

	public static ResponseStatus absensi(String accountBalanceId, String memberShipId, String machineCode) {
		ResponseStatus result = newResponseStatus();
		if(TextUtils.isEmpty(accountBalanceId) || TextUtils.isEmpty(memberShipId))return fail("Parameter tidak boleh kosong");		
		AbsensiView data = new AbsensiView();
		try{
//			String query = "DECLARE @T TABLE (MESSAGE VARCHAR(100), SUCCESS BIT ) " +
//					"	INSERT @T EXEC DOABSENSI ?, ?, ?; " +
//					"	SELECT TOP 1 * FROM @T ";
			
			String query = "EXEC DOABSENSI ?, ?, ?;";

			
			Object[] params = new Object[]{ machineCode, accountBalanceId, memberShipId};
			List rawData = DbConnection.executeReader(DbConnection.GYM, query, params);
			if(rawData == null)return fail("Data kosong");
			if(rawData.isEmpty())return fail("Data kosong");
			if(rawData.size()!= 1) return fail("Data double");
			HashMap row = (HashMap)rawData.get(0);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
			String json = ow.writeValueAsString(row);
			data = mapClass(mapper, json, AbsensiView.class);
			result= success("Absensi berhasil", data);
			
		}catch(Exception ex){
			ex.fillInStackTrace();
			String message = ex.getMessage();
			message = TextUtils.isEmpty(message) ? "Error {null exception}" : message;
            System.out.println(message);
            result = fail(message);  
		}
		return result;
	}
	
	private static <T> T getMappedClass(HashMap rowInfo, Class<T> clazz) throws JsonProcessingException, IOException{
		T result = null;
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(rowInfo);
		result = mapClass(mapper, json, clazz);
		return result;
	}
	
	private static <T> T mapClass(ObjectMapper mapper, String json, Class<T> clazz) throws JsonProcessingException, IOException
	{
		T result = null;
		if(mapper == null)return result;
		result = mapper.readValue(json, clazz);
		return result;
	}
	
	private static ResponseStatus newResponseStatus(){
		ResponseStatus result = new ResponseStatus();
		result.setMessage("Inisialisasi data.");
		result.setSuccess(false);
		return result;
	}
	
	private static ResponseStatus fail(String message){
		ResponseStatus result = new ResponseStatus();
		result.setMessage(message);
		result.setSuccess(false);
		return result;
	}
	
	private static ResponseStatus fail(String message, Object data){
		ResponseStatus result = new ResponseStatus();
		result.setMessage(message);
		result.setSuccess(false);
		result.setData(data);
		return result;
	}
	
	private static ResponseStatus success(String message){
		ResponseStatus result = new ResponseStatus();
		result.setMessage(message);
		result.setSuccess(true);
		return result;
	}
	
	private static ResponseStatus success(String message, Object data){
		ResponseStatus result = new ResponseStatus();
		result.setMessage(message);
		result.setSuccess(true);
		result.setData(data);
		return result;
	}
	
	private static class CardEventData{
		private String cardNo;

		public String getCardNo() {
			return cardNo;
		}

		public void setCardNo(String cardNo) {
			this.cardNo = cardNo;
		}

		
	}
	
	private static class AccountBalanceData{
		private String userName;
		private String balance;
		private String company;
		private String nik;
		private String accountBalanceId;
		public String getAccountBalanceId() {
			return accountBalanceId;
		}
		public void setAccountBalanceId(String accountBalanceId) {
			this.accountBalanceId = accountBalanceId;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getBalance() {
			return balance;
		}
		public void setBalance(String balance) {
			this.balance = balance;
		}
		public String getCompany() {
			return company;
		}
		public void setCompany(String company) {
			this.company = company;
		}
		public String getNik() {
			return nik;
		}
		public void setNik(String nik) {
			this.nik = nik;
		}
	}
	
	private static AccountBalance failCard(String cardNo){
		AccountBalance result = new AccountBalance();
		result.setCardno(cardNo);
		return result;
	}

	public static AccountBalance info_account_from_old_card(String machineNo) {
		AccountBalance result = new AccountBalance();
		if(TextUtils.isEmpty(machineNo))return null;
		try{
//			String query = "DECLARE @T TABLE (MESSAGE VARCHAR(100), SUCCESS BIT ) " +
//					"	INSERT @T EXEC DOABSENSI ?, ?, ?; " +
//					"	SELECT TOP 1 * FROM @T ";
			
			String SPsql;
			SPsql = "SELECT E.CARD_NUM1+E.CARD_NUM2 AS cardNo FROM EVENTS E INNER JOIN PROFILE_CARDS PC ON PC.HW_NUM = E.HW_NUM AND PC.USER_NUM = E.USER_NUM "
					+ " AND PC.CARD_NUM1 = E.CARD_NUM1 AND PC.CARD_NUM2 = E.CARD_NUM2 "
					+ "WHERE E.DOOR_FULLID = ? AND E.EVENT_MSG LIKE '%Normal%' AND E.STATUS='0' ORDER BY E.DATE_CREATED DESC";
			
			String ceknameSql = "SELECT USER_NAME AS userName, NIK AS nik, BALANCE AS balance, COMPANY AS company, ACCOUNTBALANCE_ID AS accountBalanceId FROM T_ACCOUNTBALANCE WHERE ( CARD_NO = ? ) "
					+ "AND DELETED = 0 AND STATUS = '1';";
			
			Object[] params = new Object[]{ machineNo};
			List rawEventData = DbConnection.executeReader(DbConnection.SOYAL, SPsql, params);
			if(rawEventData == null)return null;
			if(rawEventData.isEmpty())return null;
			if(rawEventData.size()!= 1) return null;
			if(rawEventData.size() == 1){
				HashMap row = (HashMap)rawEventData.get(0);
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
				String json = ow.writeValueAsString(row);
				CardEventData eventData = mapClass(mapper, json, CardEventData.class);
				String cardNo = eventData.getCardNo();
				params = new Object[] {cardNo};
				List rawCardData = DbConnection.executeReader(DbConnection.TRX, ceknameSql, params);
				if(rawCardData == null)return null;
				if(rawCardData.isEmpty())return null;
				if(rawCardData.size()!= 1) return null;
				if(rawCardData.size() == 1){
					HashMap rowInfo = (HashMap)rawCardData.get(0);
					json = ow.writeValueAsString(rowInfo);
					AccountBalanceData accountBalanceData = mapClass(mapper, json, AccountBalanceData.class);
					result.setCompany(accountBalanceData.getCompany());
					result.setNik(accountBalanceData.getNik());
					result.setCardno(cardNo);
					result.setBalance(accountBalanceData.getBalance());
					result.setUsername(accountBalanceData.getUserName());
					result.setAccountbalance_id(accountBalanceData.getAccountBalanceId());
					
				}
			}
			
			
		}catch(Exception ex){
			ex.fillInStackTrace();
			String message = ex.getMessage();
			message = TextUtils.isEmpty(message) ? "Error {null exception}" : message;
            System.out.println(message);
            result = null;  
		}
		return result;
	}


	
	
	

}
