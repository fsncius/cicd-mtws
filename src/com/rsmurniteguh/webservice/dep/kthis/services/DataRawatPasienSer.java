package com.rsmurniteguh.webservice.dep.kthis.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.infodatapasien;
import com.rsmurniteguh.webservice.dep.all.model.infopasien;
import com.rsmurniteguh.webservice.dep.all.model.infopasiendata;
import com.rsmurniteguh.webservice.dep.kthis.model.ProtokolHD;
import com.rsmurniteguh.webservice.dep.kthis.model.ProtokolHdPart;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.kthis.view.ProtokolHdPartView;
import com.rsmurniteguh.webservice.dep.kthis.view.ProtokolHdView;

public class DataRawatPasienSer extends DbConnection {

	public static List<infopasien> getDataRawatPasien(String cardno) {
		Statement stmt=null;
		Statement amvis=null;
		Statement ward=null;
		
		ResultSet has=null;
		ResultSet halam=null;
		ResultSet haswa=null;
		
		Connection connection=DbConnection.getPooledConnection();
		
		List<infopasien> all=new ArrayList<infopasien>();
		try 
			{

			
			if(connection == null)return null;
			String abdata="select distinct v.visit_id, c.card_no, p.person_name, vd.careprovider_id, p.sex, p.birth_date, (select medical_record_no from medicalrecordno where "
					+ "patient_id = pt.patient_id and medical_record_no_type = 'MRTOP') as opmrn, (select medical_record_no from medicalrecordno where "
					+ "patient_id = pt.patient_id and medical_record_no_type = 'MRTIP') as ipmrn from visit v, visitdoctor vd, patient pt, person p, card c "
					+ "where c.card_no="+cardno+" and v.visit_id=vd.visit_id and v.patient_id=pt.patient_id and pt.person_id=p.person_id and "
					+ "c.person_id=p.person_id and v.patient_type='PTY1' Order by v.visit_id Desc ";
				stmt=connection.createStatement();
				has=stmt.executeQuery(abdata);
				has.next();
				
					infopasien ip= new infopasien();
					ip.setVisitid(has.getBigDecimal("visit_id"));
					BigDecimal Visitid=has.getBigDecimal("visit_id");
					ip.setCardNo(has.getBigDecimal("card_no"));
					ip.setPersonName(has.getString("person_name"));
					ip.setCareProvider(has.getBigDecimal("careprovider_id"));
					ip.setSex(has.getString("sex"));
					ip.setBirthdate(has.getDate("birth_date"));
					ip.setOpmrn(has.getString("opmrn"));
					ip.setIpmrn(has.getString("ipmrn"));
					has.close();
					stmt.close();
					
					
					
					String alamat="SELECT AR.ADDRESS_1 , AR.ADDRESS_2 , AR.ADDRESS_3 FROM PERSON P, CARD CD, VISIT V, PATIENT PT, STAKEHOLDER SH, ADDRESS AR WHERE "
							+ "V.VISIT_ID = "+Visitid+" AND P.STAKEHOLDER_ID = SH.STAKEHOLDER_ID AND AR.STAKEHOLDER_ID = SH.STAKEHOLDER_ID AND PT.PERSON_ID = P.PERSON_ID "
							+ "AND V.PATIENT_ID = PT.PATIENT_ID AND CD.PERSON_ID = P.PERSON_ID";					
					amvis=connection.createStatement();
					halam=amvis.executeQuery(alamat);
					halam.next();
					
							ip.setAr1(halam.getString("ADDRESS_1"));
							ip.setAr2(halam.getString("ADDRESS_2"));
							ip.setAr3(halam.getString("ADDRESS_3"));							
						
					halam.close();
					amvis.close();
					
					
					String abward="select * from (select wm.ward_desc,bm.Bed_No,bh.visit_id from wardmstr wm, roommstr rm, bedmstr bm, bedhistory bh where wm.wardmstr_id=rm.wardmstr_id and "
							+ "rm.roommstr_id = bm.roommstr_id and bm.bedmstr_id=bh.bedmstr_id and bh.visit_id="+Visitid+" order by bh.effective_start_datetime desc) where ROWNUM <= 1 ";
					ward=connection.createStatement();
					haswa=ward.executeQuery(abward);
					haswa.next();
					
							ip.setWardDesc(haswa.getString("ward_desc"));
							ip.setBedNo(haswa.getString("Bed_No"));
							ip.setVisiti(haswa.getBigDecimal("visit_id"));
						
					all.add(ip);
					haswa.close();
					ward.close();
					
			} 
		catch (SQLException e) 
			{
				System.out.println(e.getMessage());
			}
		
		finally {
		     if (has!=null) try  { has.close(); } catch (Exception ignore){}
		     if (halam!=null) try  { halam.close(); } catch (Exception ignore){}
		     if (haswa!=null) try  { haswa.close(); } catch (Exception ignore){}
		     
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (amvis!=null) try  { amvis.close(); } catch (Exception ignore){}
		     if (ward!=null) try  { ward.close(); } catch (Exception ignore){}
		     
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		
		return all;
	}

	public static List<infopasiendata> getinfopasien(String cardNo) {
		Statement stmt=null;
		ResultSet has=null;
		Connection connection=DbConnection.getPooledConnection();
		List<infopasiendata> all=new ArrayList<infopasiendata>();
		try 
			{

			
			if(connection == null)return null;
			String abdata="select  c.card_no, p.person_name, p.sex, p.birth_date, "
					+ "(select medical_record_no from medicalrecordno where patient_id = pt.patient_id and medical_record_no_type = 'MRTOP') as opmrn, "
					+ "(select medical_record_no from medicalrecordno where patient_id = pt.patient_id and medical_record_no_type = 'MRTIP') as ipmrn "
					+ "from  patient pt, person p, card c where c.card_no='"+cardNo+"' and pt.person_id=p.person_id and c.person_id=p.person_id";
				stmt=connection.createStatement();
				has=stmt.executeQuery(abdata);
				has.next();
				
					infopasiendata ip= new infopasiendata();					
					ip.setCardNo(has.getBigDecimal("card_no"));
					ip.setPersonName(has.getString("person_name"));
					ip.setSex(has.getString("sex"));
					ip.setBirthdate(has.getDate("birth_date"));
					ip.setOpmrn(has.getString("opmrn"));
					ip.setIpmrn(has.getString("ipmrn"));
					has.close();
					stmt.close();	
					all.add(ip);					
			} 
		catch (SQLException e) 
			{
				System.out.println(e.getMessage());
			}
		
		finally {
		     if (has!=null) try  { has.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static List<infodatapasien> getdatapasien(String cardNo) {
		Statement stmt=null;
		ResultSet has=null;
		Connection connection=DbConnection.getPooledConnection();
		List<infodatapasien> all=new ArrayList<infodatapasien>();
		try 
			{

			
			if(connection == null)return null;
			String abdata="SELECT PRS.PERSON_ID, PRS.PERSON_NAME, PGCOMMON.FXGETCODEDESC(PRS.SEX) AS SEX, PRS.BIRTH_DATE, PRS.ID_NO, "
					+ "PGCOMMON.FXGETCODEDESC(PRS.MARITAL_STATUS) AS STATUS_NIKAH, PGCOMMON.FXGETCODEDESC(PRS.NATIONALITY) AS WARGANEGARA, "
					+ "PGCOMMON.FXGETCODEDESC(PRS.RACE) AS RAS, PGCOMMON.FXGETCODEDESC(PRS.ETHNIC) AS ETNIS, PGCOMMON.FXGETCODEDESC(PRS.RELIGION) AS AGAMA, "
					+ "PGCOMMON.FXGETCODEDESC(PRS.OCCUPATION_GROUP) AS STATUSKERJA, PRS.MOBILE_PHONE_NO "
					+ "FROM PERSON PRS, CARD CD "
					+ "WHERE CD.CARD_NO="+cardNo+" AND PRS.PERSON_ID=CD.PERSON_ID";
				stmt=connection.createStatement();
				has=stmt.executeQuery(abdata);
				has.next();
				
					infodatapasien ip= new infodatapasien();
					ip.setPERSON_ID(has.getString("PERSON_ID"));
					ip.setPERSON_NAME(has.getString("PERSON_NAME"));
					ip.setSEX(has.getString("SEX"));
					ip.setBIRTH_DATE(has.getString("BIRTH_DATE"));
					ip.setID_NO(has.getString("ID_NO"));
					ip.setSTATUS_NIKAH(has.getString("STATUS_NIKAH"));
					ip.setWARGANEGARA(has.getString("WARGANEGARA"));
					ip.setRAS(has.getString("RAS"));
					ip.setETNIS(has.getString("ETNIS"));
					ip.setAGAMA(has.getString("AGAMA"));
					ip.setSTATUSKERJA(has.getString("STATUSKERJA"));
					ip.setMOBILE_PHONE_NO(has.getString("MOBILE_PHONE_NO"));
					has.close();
					stmt.close();	
					all.add(ip);					
			} 
		catch (SQLException e) 
			{
				System.out.println(e.getMessage());
			}
		finally {
		     if (has!=null) try  { has.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}

	public static ProtokolHdView getvisitprotokolhd(String visitid) {
		Connection connectionHD = DbConnection.getProtokolHDConnection();
		Connection connectionKTHIS = DbConnection.getPooledConnection();
		if(connectionHD == null || connectionKTHIS == null)return null;
		ResultSet rsHD=null;
		ResultSet rsHDCheck=null;
		ResultSet rsKthisCheck=null;
		//PreparedStatement psInsertKTHIS=null;
		PreparedStatement psHD=null;
		PreparedStatement psCheckHD=null;
		PreparedStatement psCheckKTHIS=null;
		List<ProtokolHD> listAll = new ArrayList<ProtokolHD>();
		List<ProtokolHdPartView> Listview = new ArrayList<ProtokolHdPartView>();
		ProtokolHdView view = new ProtokolHdView();
		
		String no_kartu = "";
		try {
			String visit = "SELECT V.VISIT_ID, TRUNC(v.admission_datetime) as admission_datetime, CA.CARD_NO FROM VISIT V INNER JOIN PATIENT PA "
					+ "ON PA.PATIENT_ID = V.PATIENT_ID INNER JOIN PERSON PE "
					+ "ON PE.PERSON_ID = PA.PERSON_ID INNER JOIN CARD CA "
					+ "ON CA.PERSON_ID = PE.PERSON_ID "
					+ "WHERE V.VISIT_ID = '"+visitid+"'";
			psCheckKTHIS = connectionKTHIS.prepareStatement(visit);
			rsKthisCheck = psCheckKTHIS.executeQuery();
			
			if(rsKthisCheck.next())
			{
				no_kartu = rsKthisCheck.getString("CARD_NO");
				SimpleDateFormat visittime = new SimpleDateFormat("yyyy-MM-dd");
			    Date parsedDatevisit = visittime.parse(rsKthisCheck.getTimestamp("admission_datetime").toString());			    
			    String parsedDater = visittime.format(parsedDatevisit);
			    			    
				String datadiri = "SELECT dd.`nokartu`, dd.`haritgjam`, dd.`diagnosamedis`, dd.`riwayatkesehatan`, dd.`nomesin`, dd.`hemodialiske`, dd.`tipedialis`, dd.`carabayar`,dd.`tanggal` "
						+ "FROM tb_datadiri dd WHERE LENGTH(DD.`haritgjam`) = 17 AND dd.`haritgjam` IS NOT NULL AND dd.`haritgjam` != '' AND DD.`tanggal` = '"+parsedDater+"' and dd.nokartu = '"+no_kartu+"' LIMIT 1";
					
				psHD = connectionHD.prepareStatement(datadiri);
				rsHD = psHD.executeQuery();
					while(rsHD.next())
					{
						BigDecimal visitKthis = new BigDecimal(visitid);
						SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
					    Date parsedDate = dateFormat.parse(rsHD.getString("haritgjam"));
					   
					    Timestamp tanggalHd = new java.sql.Timestamp(parsedDate.getTime());
					    
					    SimpleDateFormat dateFormatTanggal = new SimpleDateFormat("yyyy-MM-dd");
					    Date parsedDateTanggal = dateFormatTanggal.parse(rsHD.getString("tanggal"));
					    Timestamp tanggal = new java.sql.Timestamp(parsedDateTanggal.getTime());
					    
						ProtokolHD phd = new ProtokolHD();
						phd.setCardNo(rsHD.getBigDecimal("nokartu"));
						phd.setVisitId(visitKthis);
						phd.setTanggalHd(tanggalHd);
						phd.setDiagnosaMedis(rsHD.getString("diagnosamedis"));
						phd.setRiwayatKesehatan(rsHD.getString("riwayatkesehatan"));
						phd.setNomorMesin(rsHD.getString("nomesin"));
						phd.setHemodialisisKe(rsHD.getString("hemodialiske"));
						phd.setTipeDialiser(rsHD.getString("tipedialis"));
						phd.setCaraBayar(rsHD.getString("carabayar"));
						phd.setTanggal(tanggal);
						phd.setTanggalString(rsHD.getString("tanggal"));
						
						BigDecimal noKartu = phd.getCardNo();
						Timestamp tglT = phd.getTanggal();

						Date date = new Date();
						date.setTime(tglT.getTime());
						String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
						
						/*String checkKthis = "SELECT * FROM PROTOKOLHD PH WHERE TRUNC(PH.CREATED_DATETIME) = '"+formattedDate+"' AND PH.CARD_NO = '"+noKartu+"'";
						if (psCheckKTHIS != null) try { psCheckKTHIS.close(); } catch (SQLException e) {}
						if (rsKthisCheck != null) try { rsKthisCheck.close(); } catch (SQLException e) {}
						psCheckKTHIS = connectionKTHIS.prepareStatement(checkKthis);
						rsKthisCheck = psCheckKTHIS.executeQuery();
						if(rsKthisCheck.next())
						{
							continue;
						}
						else
						{*/
							
							String status = "SELECT CONCAT('{\"merokok\":',IF(st.`rokok` = 'Merokok', 'true,', 'false,'),'\"konsumsiAlkohol\":',IF(st.`alkohol` = 'Konsumsi Alkohol', 'true,', 'false,'),'\"seksBebas\":',IF(st.`seks` = 'Seks Bebas', 'true,', 'false,'),"
									+ "'\"penggunaObatTerlarang\":',IF(st.`obat` = 'Pengguna Obat Terlarang', 'true,', 'false,'),'\"dll\":\"',st.`lainpsiko`,'\"}') AS STATUS_PSIKOSOSIAL,"
									+ "CONCAT('{\"kecemasanBerlebih\":',IF(st.`cemas` = 'Kecemasan berlebih', 'true,', 'false,'),'\"prilakuTidakNormal\":',IF(st.`perilaku` = 'Perilaku Tidak Normal', 'true,', 'false,'),'\"sifatKasar\":',IF(st.`kasar` = 'Sifat Kasar / Berbahaya', 'true,', 'false,'),"
									+ "'\"tidakBisaMengurusDiri\":',IF(st.`tdkbsurusdiri` = 'Tidak Bisa Mengurus Diri', 'true,', 'false,'),'\"dll\":\"',st.`lainstatusfungsi`,'\"}') AS STATUS_FUNGSIONAL,"
									+ "CONCAT('{\"tidakBerisiko\":',IF(st.`tidakrisiko` = 'Tidak Berisiko', 'true,', 'false,'),'\"risikoRendah\":',IF(st.`risikorendah` = 'Risiko Rendah', 'true,', 'false,'),'\"risikoSedang\":',IF(st.`risikosedang` = 'Risiko Sedang', 'true,', 'false,'),"
									+ "'\"risikoTinggi\":',IF(st.`risikotinggi` = 'Risiko Tinggi', 'true,', 'false,'),'\"catatan\":\"',st.`catatanrisiko`,'\"}') AS RISIKO_JATUH,"
									+ "CONCAT('{\"alergiYa\":false,\"alergiTidak\":true,\"dll\":\"\"}') AS ALERGI_OBAT"
									+ " FROM tb_status st WHERE st.nokartu = '"+noKartu+"' AND st.tanggal = '"+tglT+"'ORDER BY st.idstatus DESC LIMIT 1";
							
							psCheckHD = connectionHD.prepareStatement(status);
							rsHDCheck = psCheckHD.executeQuery();
							while(rsHDCheck.next())
							{
								phd.setStatusPsikososial(rsHDCheck.getString("STATUS_PSIKOSOSIAL"));
								phd.setStatusFungsional(rsHDCheck.getString("STATUS_FUNGSIONAL"));
								//phd.setRisikoJatuh(rsHDCheck.getString("RISIKO_JATUH"));
								phd.setAlergiObat(rsHDCheck.getString("ALERGI_OBAT"));
							}
							
							if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
							if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}

							String pre_hd = "SELECT ph.`sensorium`, ph.`td3`, ph.`nadi3`, ph.`nafas`, ph.`suhu3`, ph.`bbkering`, ph.`bbdatang`, ph.`bbsebelumnya`, ph.`kenaikanbb`, ph.`aksesvaskular`, ph.`sens`, ph.`td2`, ph.`nadi2`, ph.`nafas2`, ph.`suhu2`, ph.`bbselesai`, ph.`bedabb`, ph.`catatan`"
									+ "FROM tb_pre_hd ph WHERE ph.`nokartu` = '"+noKartu+"' AND ph.`tanggal` = '"+tglT+"' ORDER BY ph.idprehd DESC LIMIT 1";
							psCheckHD = connectionHD.prepareStatement(pre_hd);
							rsHDCheck = psCheckHD.executeQuery();
							while(rsHDCheck.next())
							{
								phd.setSensoriumPreHd(rsHDCheck.getString("sensorium"));
								phd.setTdPreHd(rsHDCheck.getString("td3"));
								phd.setNadiPreHd(rsHDCheck.getString("nadi3"));
								phd.setNafasPreHd(rsHDCheck.getString("nafas"));
								phd.setSuhuPreHd(rsHDCheck.getString("suhu3"));
								phd.setBbKeringPreHd(rsHDCheck.getString("bbkering"));
								phd.setBbDatangPreHd(rsHDCheck.getString("bbdatang"));
								phd.setBbSebelumnyaPreHd(rsHDCheck.getString("bbsebelumnya"));
								phd.setBbKenaikanPreHd(rsHDCheck.getString("kenaikanbb"));
								phd.setAksesVaskularPreHd(rsHDCheck.getString("aksesvaskular"));
								phd.setSensoriumPostHd(rsHDCheck.getString("sens"));
								phd.setTdPostHd(rsHDCheck.getString("td2"));
								phd.setNadiPostHd(rsHDCheck.getString("nadi2"));
								phd.setNafasPostHd(rsHDCheck.getString("nafas2"));
								phd.setSuhuPostHd(rsHDCheck.getString("suhu2"));
								phd.setBbSelesaiPostHd(rsHDCheck.getString("bbselesai"));
								phd.setBedaBbPostHd(rsHDCheck.getString("bedabb"));
								phd.setCatatanPostHd(rsHDCheck.getString("catatan"));
							}
							
							if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
							if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}

							String instruksi_medik = "SELECT im.`waktumulai`, im.`lamahd`, im.`waktuselesai`, im.`primingmasuk`, im.`primingkeluar`, im.`antikoagulasi`, im.`awal`, im.`maintenance`, CONCAT('{\"tanpaHeprain\":',IF(im.`tpheparin` = 'Tanpa Heparin', 'true', 'false'),'}') AS TANPA_HEPARIN,"
									+ "im.`ufg`, im.`resephd`, im.`durasi`, im.`qb`, im.`qd`, im.`td`, im.`ufg2`, im.`ufr`, im.`profiling`, im.`antikoagulasi2`, im.`namadokter`, im.`periksa_fisik`"
									+ "FROM tb_instruksi_medik im WHERE im.`nokartu` = '"+noKartu+"' AND im.`tanggal` = '"+tglT+"' ORDER BY im.idinstruksi DESC LIMIT 1";
							psCheckHD = connectionHD.prepareStatement(instruksi_medik);
							rsHDCheck = psCheckHD.executeQuery();
							while(rsHDCheck.next())
							{
								if(rsHDCheck.getString("waktumulai").equals("") || rsHDCheck.getString("waktuselesai").equals(""))
								{
									phd.setWaktuMulai(null);
									phd.setWaktuSelesai(null);
								}
								else
								{	
									SimpleDateFormat dateFormatTanggal2 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
								    Date parsedDateTanggal2 = dateFormatTanggal2.parse(phd.getTanggalString() + " "+rsHDCheck.getString("waktumulai"));
								    Timestamp waktumulai = new java.sql.Timestamp(parsedDateTanggal2.getTime());
								    
								    Date parsedDateTanggal1 = dateFormatTanggal2.parse(phd.getTanggalString() + " "+rsHDCheck.getString("waktuselesai"));
								    Timestamp waktuselesai = new java.sql.Timestamp(parsedDateTanggal1.getTime());
					    
									phd.setWaktuMulai(waktumulai);
									phd.setWaktuSelesai(waktuselesai);
								}				
								phd.setLamaHd(rsHDCheck.getBigDecimal("lamahd"));
								phd.setPrimingMasuk(rsHDCheck.getString("primingmasuk"));
								phd.setPrimingKeluar(rsHDCheck.getString("primingkeluar"));
								phd.setAntiKoagulasi(rsHDCheck.getString("antikoagulasi"));
								phd.setAwalHd(rsHDCheck.getString("awal"));
								phd.setMaintenance(rsHDCheck.getString("maintenance"));
								phd.setTanpaHeparin(rsHDCheck.getString("TANPA_HEPARIN"));
								phd.setUfgHd(rsHDCheck.getString("ufg"));
								phd.setResepHd(rsHDCheck.getString("resephd"));
								phd.setDurasiHd(rsHDCheck.getString("durasi"));
								phd.setQbInstruksiMedik(rsHDCheck.getString("qb"));
								phd.setQdInstruksiMedik(rsHDCheck.getString("qd"));
								phd.setTdInstruksiMedik(rsHDCheck.getString("td"));
								phd.setUfgInstruksiMedik(rsHDCheck.getString("ufg2"));
								phd.setUfrInstruksiMedik(rsHDCheck.getString("ufr"));
								phd.setProfillingInstruksiMedik(rsHDCheck.getString("profiling"));
								phd.setAntikoagulasiInstruksiMedik(rsHDCheck.getString("antikoagulasi2"));
								phd.setNamaDokter(rsHDCheck.getString("namadokter"));
								phd.setPemeriksaanFisik(rsHDCheck.getString("periksa_fisik"));
							}

							
							if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
							if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
							
							String indikator_sakit = "SELECT CONCAT('{\"anemis\":',IF(idn.`anemis` = 'Anemis', 'true,', 'false,'),'\"ikterus\":',IF(idn.`ikterus` = 'Ikterus', 'true,', 'false,'),'\"sianosis\":',IF(idn.`sianosis` = 'Sianosis', 'true,', 'false,'),"
									+ "'\"sesak\":',IF(idn.`sesak` = 'Sesak', 'true,', 'false,'),'\"edema\":',IF(idn.`edema` = 'Edema', 'true,', 'false,'),'\"asites\":',IF(idn.`asites` = 'Asites', 'true', 'false'),'}') AS KEADAAN_UMUM,"
									+ "CONCAT('{\"tenang\":',IF(idn.`tenang` = 'Tenang', 'true,', 'false,'),'\"gelisah\":',IF(idn.`gelisah` = 'Gelisah', 'true,', 'false,'),'\"takut\":',IF(idn.`takut` = 'Takut', 'true,', 'false,'),"
									+ "'\"marah\":',IF(idn.`marah` = 'Marah', 'true,', 'false,'),'\"mudahTersinggung\":',IF(idn.`singgung` = 'Mudah Tersinggung', 'true', 'false'),'}') AS KONDISI_SAATINI,"
									+ "CONCAT('{\"nyeri\":',IF(idn.`nyeri` = 'Nyeri', 'true,', 'false,'),'\"akut\":',IF(idn.`akut` = 'Akut', 'true,', 'false,'),'\"kronik\":',IF(idn.`kronik` = 'Kronik', 'true', 'false'),'}') AS KONDISI_SAKIT,"
									+ "idn.`nilaiindikator` "
									+ "FROM tb_indikator_nyeri idn WHERE idn.`nokartu` = '"+noKartu+"' AND idn.`tanggal` = '"+tglT+"' ORDER BY idn.idindikatornyeri DESC LIMIT 1";
							psCheckHD = connectionHD.prepareStatement(indikator_sakit);
							rsHDCheck = psCheckHD.executeQuery();
							while(rsHDCheck.next())
							{
								phd.setKeadaanUmum(rsHDCheck.getString("KEADAAN_UMUM"));
								phd.setKondisiSaatIni(rsHDCheck.getString("KONDISI_SAATINI"));
								phd.setKondisiSakit(rsHDCheck.getString("KONDISI_SAKIT"));
								phd.setIndikatorSakit(rsHDCheck.getBigDecimal("nilaiindikator"));
							}
							if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
							if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}

							String diagnosa_intervensi = "SELECT CONCAT('{\"kelebihanVolumeCairan\":',IF(di.`lebihvolume` = 'Kelebihan Volume Cairan', 'true,', 'false,'),'\"gangguanPertukaranGas\":',IF(di.`gangguantukargas` = 'Gangguan Pertukaran Gas', 'true,', 'false,'),'\"gangguanElektrolit\":',IF(di.`gangguanelektrolit` = 'Takut', 'true,', 'false,'),"
									+ "'\"penurunanCurahJantung\":',IF(di.`penurunancurah` = 'Penurunan Curah Jantung', 'true,', 'false,'),'\"kurangNutrisi\":',IF(di.`kurangnutrisi` = 'Kurang Nutrisi', 'true,', 'false,'),'\"tidakPatuhTerhadapDiet\":',IF(di.`tidakpatuhdiet` = 'Tidak Patuh Terhadap Diet', 'true,', 'false,'),"
									+ "'\"gangguanKeseimbanganAsamBasa\":',IF(di.`gangguanasambasa` = 'Gangguan Keseimbangan Asam Basa', 'true,', 'false,'),'\"gangguanRasaNyaman\":',IF(di.`gangguanrasanyaman` = 'Gangguan Rasa Nyaman / Nyeri', 'true,', 'false,'),'\"dll\":\"',di.`laindiagnosarawat`,'\"}') AS DIAGNOSA_KEPERAWATAN,"
									+ "CONCAT('{\"monitorBB\":',IF(di.`monitorbbasup` = 'Monitor BB, Asupan, Keluaran', 'true,', 'false,'),'\"aturPosisi\":',IF(di.`aturposisipasien` = 'Atur Posisi Pasien Agar Ventilasi Adekuat', 'true,', 'false,'),'\"memberikanOksigen\":',IF(di.`memberikanoksigen` = 'Memberikan Oksigen Sesuai Kebutuhan', 'true,', 'false,'),"
									+ "'\"observasiPasien\":',IF(di.`observasipasien` = 'Observasi Pasien dan Tindakan', 'true,', 'false,'),'\"menanganiHipotensi\":',IF(di.`tanganihipotensi` = 'Menangani Hipotensi Sesuai SOP', 'true,', 'false,'),'\"kajiKemampuan\":',IF(di.`kajikemampuan` = 'Kaji Kemampuan Mendapatkan Nutrisi Yang Sesuai', 'true,', 'false,'),"
									+ "'\"gejalaInfeksi\":',IF(di.`monitortandainfeksi` = 'Monitor Tanda dan Gejala Infeksi', 'true,', 'false,'),'\"gejalaHipoglikemi\":',IF(di.`monitortandahipo` = 'Monitor Tanda dan Gejala Hipoglikemi', 'true,', 'false,'),'\"edukasiPasien\":',IF(di.`edukasipasien` = 'Mengedukasi Pasien', 'true,', 'false,'),"
									+ "'\"dll\":\"',di.`lainintervensirawat`,'\"}') AS INTERVENSI_KEPERAWATAN,"
									+ "CONCAT('{\"programHd\":',IF(di.`programhd` = 'Program HD', 'true,', 'false,'),'\"transfusiDarah\":',IF(di.`transfusidarah` = 'Transfusi Darah', 'true,', 'false,'),'\"kolaborasiDiet\":',IF(di.`kolaborasidiet` = 'Kolaborasi Diet', 'true,', 'false,'),"
									+ "'\"programPreparatBesi\":',IF(di.`programprebesi` = 'Program Preparat Besi', 'true,', 'false,'),'\"pemberianEpo\":',IF(di.`beriepo` = 'Pemberian EPO', 'true,', 'false,'),'\"pemberianAntipiretik\":',IF(di.`beriantipire` = 'Pemberian Antipiretik', 'true,', 'false,'),"
									+ "'\"pemberianCaGluconas\":',IF(di.`bericagluco` = 'Pemberian Ca Gluconas', 'true,', 'false,'),'\"analgetik\":',IF(di.`analgetik` = 'Analgetik', 'true,', 'false,'),'\"pemberianAntibiotik\":',IF(di.`beriantibio` = 'Pemberian Antibiotik', 'true,', 'false,'),"
									+ "'\"obatEmergensi\":',IF(di.`obatemergensi` = 'Obat-Obat Emergensi', 'true,', 'false,'),'\"dll\":\"',di.`lainintervensikorab`,'\"}') AS INTERVENSI_KOLABORASI "
									+ "FROM tb_diagnosa_intervensi di WHERE di.`nokartu` = '"+noKartu+"' AND di.`tanggal` = '"+tglT+"' ORDER BY di.iddiagnosa DESC LIMIT 1";
							psCheckHD = connectionHD.prepareStatement(diagnosa_intervensi);
							rsHDCheck = psCheckHD.executeQuery();
							while(rsHDCheck.next())
							{
								phd.setDiagnosaKeperawatan(rsHDCheck.getString("DIAGNOSA_KEPERAWATAN"));
								phd.setIntervensiKeperawatan(rsHDCheck.getString("INTERVENSI_KEPERAWATAN"));
								phd.setIntervensiKolaborasi(rsHDCheck.getString("INTERVENSI_KOLABORASI"));
							}
							if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
							if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
							
							String catat_rawat = "SELECT cr.`catatrawat`, cr.`namarawat` FROM tb_catat_rawat cr WHERE cr.`nokartu` = '"+noKartu+"' AND cr.`tanggal` = '"+tglT+"' ORDER BY cr.idcatatrawat DESC LIMIT 1";
							psCheckHD = connectionHD.prepareStatement(catat_rawat);
							rsHDCheck = psCheckHD.executeQuery();
							while(rsHDCheck.next())
							{
								phd.setCatatanKeperawatan(rsHDCheck.getString("catatrawat"));
								phd.setNamaPerawat(rsHDCheck.getString("namarawat"));
							}
							if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
							if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
							
							String catatan_perkembangan = "SELECT cp.`jam2`, cp.`tepidokter`, cp.`tepiklinis`, cp.`tepittd`, cp.`asupangizi`, cp.`kalori`, cp.`protein`, cp.`pengkajian`, cp.`materi`, CONCAT('{\"leafletBrosur\":',IF(cp.`leaflet` = 'leaflet', 'true', 'false'),'}') AS LEAFLET_BROSUR,cp.`rencanakedepan` "
									+ "FROM tb_catatan_perkembangan cp WHERE cp.`nokartu` = '"+noKartu+"' AND cp.`tanggal` = '"+tglT+"' ORDER BY cp.idcatatan DESC LIMIT 1";
							psCheckHD = connectionHD.prepareStatement(catatan_perkembangan);
							rsHDCheck = psCheckHD.executeQuery();
							while(rsHDCheck.next())
							{
								if(rsHDCheck.getString("jam2").equals(null) || rsHDCheck.getString("jam2").equals(""))
								{
									phd.setWaktuCatatanPerkembangan(null);						
								}
								else
								{						
									SimpleDateFormat dateFormatTanggal1 = new SimpleDateFormat("hh:mm");
								    Date parsedDateTanggal1 = dateFormatTanggal1.parse(rsHDCheck.getString("jam2"));
								    Timestamp waktucatatan = new java.sql.Timestamp(parsedDateTanggal1.getTime());
									phd.setWaktuCatatanPerkembangan(waktucatatan);
								}

								phd.setUtkDokter(rsHDCheck.getString("tepidokter"));
								phd.setUtkStaff(rsHDCheck.getString("tepiklinis"));
								phd.setTtdPerawat(rsHDCheck.getString("tepittd"));
								phd.setRekomendasiGizi(rsHDCheck.getString("asupangizi"));
								phd.setKaloriGizi(rsHDCheck.getString("kalori"));
								phd.setProteinGizi(rsHDCheck.getString("protein"));
								phd.setPengkajianGizi(rsHDCheck.getString("pengkajian"));
								phd.setEdukasiMateri(rsHDCheck.getString("materi"));
								phd.setLeafletBrosur(rsHDCheck.getString("LEAFLET_BROSUR"));
								phd.setRencanaKedepan(rsHDCheck.getString("rencanakedepan"));
							}					
							if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
							if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
							
							String akhir = "SELECT ak.`mesinhemo`, ak.`dialyzer`, ak.`desiheat`, ak.`rinse`, ak.`jenis`, ak.`komplikasi`, ak.`conductivity`, ak.`temperatur`, ak.`dialisat`, ak.`sisapriming`, ak.`drip`, ak.`darah`, ak.`washout`, ak.`minumsonde`,"
									+ "ak.`jumlah`, ak.`penekanan`, ak.`desiheater`, ak.`rinse2` FROM "
									+ "tb_akhir ak WHERE ak.`nokartu` = '"+noKartu+"' AND ak.`tanggal` = '"+tglT+"' ORDER BY ak.idakhir DESC LIMIT 1";
							psCheckHD = connectionHD.prepareStatement(akhir);
							rsHDCheck = psCheckHD.executeQuery();
							while(rsHDCheck.next())
							{
								phd.setNamaMesinHd(rsHDCheck.getString("mesinhemo"));
								phd.setDialyzerHd(rsHDCheck.getString("dialyzer"));
								phd.setDesinfectansSebelumHd(rsHDCheck.getBigDecimal("desiheat"));
								if(rsHDCheck.getString("rinse").equals("-"))
								{
									phd.setRinserSebelumHD(null);
								}
								else
								{
									phd.setRinserSebelumHD(rsHDCheck.getBigDecimal("rinse"));
								}
								phd.setJenisHd(rsHDCheck.getString("jenis"));
								phd.setKomplikasiHd(rsHDCheck.getString("komplikasi"));
								phd.setConductivitySebelumHd(rsHDCheck.getString("conductivity"));
								phd.setTemperaturSebelumHd(rsHDCheck.getString("temperatur"));
								phd.setDialisatSebelumHd(rsHDCheck.getString("dialisat"));
								phd.setSisaPriming(rsHDCheck.getString("sisapriming"));
								phd.setDripMasuk(rsHDCheck.getString("drip"));
								phd.setDarahMasuk(rsHDCheck.getString("darah"));
								phd.setWashOut(rsHDCheck.getString("washout"));
								phd.setMinumMasuk(rsHDCheck.getString("minumsonde"));
								phd.setJumlahMasuk(rsHDCheck.getString("jumlah"));
								
								if(rsHDCheck.getString("penekanan").equals("5 - 10"))
								{
									phd.setPenekananMasuk(null);
								}
								else
								{
									phd.setPenekananMasuk(rsHDCheck.getBigDecimal("penekanan"));
								}
								phd.setDesinfectantsSesudahHd(rsHDCheck.getBigDecimal("desiheater"));
								if(rsHDCheck.getString("rinse2").equals("-"))
								{
									phd.setRinseSesudahHd(null);
								}
								else
								{
									phd.setRinseSesudahHd(rsHDCheck.getBigDecimal("rinse2"));
								}
							}
							if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
							if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
							
							String waktu_instruksi = "SELECT wi.nokartu, wi.totwakmul, wi.tanggal FROM tb_waktu_instruksi wi where wi.`tanggal` = '"+tglT+"' and wi.nokartu = '"+no_kartu+"'";
							
							psCheckHD = connectionHD.prepareStatement(waktu_instruksi);
							rsHDCheck = psCheckHD.executeQuery();
							while(rsHDCheck.next())
							{			    
							    SimpleDateFormat dateFormatTanggal1 = new SimpleDateFormat("yyyy-MM-dd");
							    Date parsedDateTanggal1 = dateFormatTanggal1.parse(rsHD.getString("tanggal"));
							    Timestamp tanggal1 = new java.sql.Timestamp(parsedDateTanggal1.getTime());
							    
							    String[] parts = rsHDCheck.getString("totwakmul").split("\\+");			    
							    String rightside = parts[0];
							    rightside = rightside.replace(".", ":").replace(";", ":").replace(" ", "").replace("O", "0").replace("`", "");
							    int counter = 0;
							    for( int i=0; i<rightside.length(); i++ ) {
							        if( rightside.charAt(i) == ':' ) {
							            counter++;
							        } 
							    }
							    if(counter == 1 && rightside.length() >= 4)
							    {
									SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
							    	Date parsedDate1 = dateFormat1.parse(rsHD.getString("tanggal") + " " +rightside);
								    Timestamp waktu_pemeriksaan = new java.sql.Timestamp(parsedDate1.getTime());
								    
								    ProtokolHdPartView protokolHdPartView = new ProtokolHdPartView();
								    protokolHdPartView.setNokartu(rsHDCheck.getBigDecimal("nokartu"));
								    protokolHdPartView.getProtokolHdPart().setWaktuPemeriksaan(waktu_pemeriksaan);
								    protokolHdPartView.getProtokolHdPart().setQbPemeriksaan(parts[1]);
								    protokolHdPartView.getProtokolHdPart().setQdPemeriksaan(parts[2]);
								    protokolHdPartView.getProtokolHdPart().setUfrPemeriksaan(parts[3]);
								    protokolHdPartView.getProtokolHdPart().setTdPemeriksaan(parts[4]);
								    protokolHdPartView.getProtokolHdPart().setNadiPemeriksaan(parts[5]);
								    protokolHdPartView.getProtokolHdPart().setNrsPemeriksaan(parts[6]);
								    protokolHdPartView.getProtokolHdPart().setSuhuPemeriksaan(parts[7]);
								    protokolHdPartView.getProtokolHdPart().setRrPemeriksaan(parts[8]);
								    protokolHdPartView.getProtokolHdPart().setVpPemeriksaan(parts[9]);
								    protokolHdPartView.getProtokolHdPart().setApPemeriksaan(parts[10]);
								    protokolHdPartView.getProtokolHdPart().setTmpPemeriksaan(parts[11]);
								    protokolHdPartView.getProtokolHdPart().setKeteranganPemeriksaan(parts[12]);
								    protokolHdPartView.getProtokolHdPart().setParafPemeriksa(parts[13]);
								    protokolHdPartView.getProtokolHdPart().setCreatedDatetime(tanggal);
								    Listview.add(protokolHdPartView);
							    }
							    else
							    {
							    	continue;
							    }
							    
							}						
							view.getProtokolHd().setVisitId(phd.getVisitId());
							view.getProtokolHd().setTanggalHd(phd.getTanggalHd());
							view.getProtokolHd().setDiagnosaMedis(phd.getDiagnosaMedis());
							view.getProtokolHd().setRiwayatKesehatan(phd.getRiwayatKesehatan());
							view.getProtokolHd().setNomorMesin(phd.getNomorMesin());
							view.getProtokolHd().setHemodialisisKe(phd.getHemodialisisKe());
							view.getProtokolHd().setTipeDialiser(phd.getTipeDialiser());
							view.getProtokolHd().setCaraBayar(phd.getCaraBayar());
							view.getProtokolHd().setStatusPsikososial(phd.getStatusPsikososial());
							view.getProtokolHd().setStatusFungsional(phd.getStatusFungsional());
							//view.getProtokolHd().setRisikoJatuh(phd.getRisikoJatuh());
							view.getProtokolHd().setAlergiObat(phd.getAlergiObat());
							view.getProtokolHd().setSensoriumPreHd(phd.getSensoriumPreHd());
							view.getProtokolHd().setTdPreHd(phd.getTdPreHd());
							view.getProtokolHd().setNadiPreHd(phd.getNadiPreHd());
							view.getProtokolHd().setNafasPreHd(phd.getNafasPreHd());
							view.getProtokolHd().setSuhuPreHd(phd.getSuhuPreHd());
							view.getProtokolHd().setBbKeringPreHd(phd.getBbKeringPreHd());
							view.getProtokolHd().setBbDatangPreHd(phd.getBbDatangPreHd());
							view.getProtokolHd().setBbSebelumnyaPreHd(phd.getBbSebelumnyaPreHd());
							view.getProtokolHd().setBbKenaikanPreHd(phd.getBbKenaikanPreHd());
							view.getProtokolHd().setAksesVaskularPreHd(phd.getAksesVaskularPreHd());
							view.getProtokolHd().setSensoriumPostHd(phd.getSensoriumPostHd());
							view.getProtokolHd().setTdPostHd(phd.getTdPostHd());
							view.getProtokolHd().setNadiPostHd(phd.getNadiPostHd());
							view.getProtokolHd().setNafasPostHd(phd.getNafasPostHd());
							view.getProtokolHd().setSuhuPostHd(phd.getSuhuPostHd());
							view.getProtokolHd().setBbSelesaiPostHd(phd.getBbSelesaiPostHd());
							view.getProtokolHd().setBedaBbPostHd(phd.getBedaBbPostHd());
							view.getProtokolHd().setCatatanPostHd(phd.getCatatanPostHd());
							view.getProtokolHd().setWaktuMulai(phd.getWaktuMulai());
							view.getProtokolHd().setLamaHd(phd.getLamaHd());
							view.getProtokolHd().setWaktuSelesai(phd.getWaktuSelesai());
							view.getProtokolHd().setPrimingMasuk(phd.getPrimingMasuk());
							view.getProtokolHd().setPrimingKeluar(phd.getPrimingKeluar());
							view.getProtokolHd().setAntiKoagulasi(phd.getAntiKoagulasi());
							view.getProtokolHd().setAwalHd(phd.getAwalHd());
							view.getProtokolHd().setMaintenance(phd.getMaintenance());
							view.getProtokolHd().setTanpaHeparin(phd.getTanpaHeparin());
							view.getProtokolHd().setUfgHd(phd.getUfgHd());
							view.getProtokolHd().setResepHd(phd.getResepHd());
							view.getProtokolHd().setDurasiHd(phd.getDurasiHd());
							view.getProtokolHd().setQbInstruksiMedik(phd.getQbInstruksiMedik());
							view.getProtokolHd().setQdInstruksiMedik(phd.getQdInstruksiMedik());
							view.getProtokolHd().setTdInstruksiMedik(phd.getTdInstruksiMedik());
							view.getProtokolHd().setUfgInstruksiMedik(phd.getUfgInstruksiMedik());
							view.getProtokolHd().setUfrInstruksiMedik(phd.getUfrInstruksiMedik());
							view.getProtokolHd().setProfillingInstruksiMedik(phd.getProfillingInstruksiMedik());
							view.getProtokolHd().setAntikoagulasiInstruksiMedik(phd.getAntikoagulasiInstruksiMedik());
							view.getProtokolHd().setNamaDokter(phd.getNamaDokter());
							view.getProtokolHd().setPemeriksaanFisik(phd.getPemeriksaanFisik());
							view.getProtokolHd().setKeadaanUmum(phd.getKeadaanUmum());
							view.getProtokolHd().setKondisiSaatIni(phd.getKondisiSaatIni());
							view.getProtokolHd().setKondisiSakit(phd.getKondisiSakit());
							view.getProtokolHd().setIndikatorSakit(phd.getIndikatorSakit());
							view.getProtokolHd().setDiagnosaKeperawatan(phd.getDiagnosaKeperawatan());
							view.getProtokolHd().setIntervensiKeperawatan(phd.getIntervensiKeperawatan());
							view.getProtokolHd().setIntervensiKolaborasi(phd.getIntervensiKolaborasi());
							view.getProtokolHd().setCatatanKeperawatan(phd.getCatatanKeperawatan());
							view.getProtokolHd().setNamaPerawat(phd.getNamaPerawat());
							view.getProtokolHd().setWaktuCatatanPerkembangan(phd.getWaktuCatatanPerkembangan());
							view.getProtokolHd().setUtkDokter(phd.getUtkDokter());
							view.getProtokolHd().setUtkStaff(phd.getUtkStaff());
							view.getProtokolHd().setTtdPerawat(phd.getTtdPerawat());
							view.getProtokolHd().setRekomendasiGizi(phd.getRekomendasiGizi());
							view.getProtokolHd().setKaloriGizi(phd.getKaloriGizi());
							view.getProtokolHd().setProteinGizi(phd.getProteinGizi());
							view.getProtokolHd().setPengkajianGizi(phd.getPengkajianGizi());
							view.getProtokolHd().setEdukasiMateri(phd.getEdukasiMateri());
							view.getProtokolHd().setLeafletBrosur(phd.getLeafletBrosur());
							view.getProtokolHd().setRencanaKedepan(phd.getRencanaKedepan());
							view.getProtokolHd().setNamaMesinHd(phd.getNamaMesinHd());
							view.getProtokolHd().setDialyzerHd(phd.getDialyzerHd());
							view.getProtokolHd().setDesinfectansSebelumHd(phd.getDesinfectansSebelumHd());
							view.getProtokolHd().setRinserSebelumHD(phd.getRinserSebelumHD());
							view.getProtokolHd().setJenisHd(phd.getJenisHd());
							view.getProtokolHd().setKomplikasiHd(phd.getKomplikasiHd());
							view.getProtokolHd().setConductivitySebelumHd(phd.getConductivitySebelumHd());
							view.getProtokolHd().setTemperaturSebelumHd(phd.getTemperaturSebelumHd());
							view.getProtokolHd().setDialisatSebelumHd(phd.getDialisatSebelumHd());
							view.getProtokolHd().setSisaPriming(phd.getSisaPriming());
							view.getProtokolHd().setDripMasuk(phd.getDripMasuk());
							view.getProtokolHd().setDarahMasuk(phd.getDarahMasuk());
							view.getProtokolHd().setWashOut(phd.getWashOut());
							view.getProtokolHd().setMinumMasuk(phd.getMinumMasuk());
							view.getProtokolHd().setJumlahMasuk(phd.getJumlahMasuk());
							view.getProtokolHd().setPenekananMasuk(phd.getPenekananMasuk());
							view.getProtokolHd().setDesinfectantsSesudahHd(phd.getDesinfectantsSesudahHd());
							view.getProtokolHd().setRinseSesudahHd(phd.getRinseSesudahHd());
							view.getProtokolHd().setCreatedBy(null);
							view.getProtokolHd().setCreatedDateTime(phd.getTanggal());
							view.getProtokolHd().setLastUpdatedBy(null);
							view.getProtokolHd().setLastUpdatedDateTime(phd.getTanggal());
							view.getProtokolHd().setCardNo(phd.getCardNo());
							view.getProtokolHd().setDefunctInd("N");
							view.setProtokolHdPartView(Listview);

							if (psCheckHD != null) try { psCheckHD.close(); } catch (SQLException e) {}
							if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
						}
					}
			}	
		catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		finally {
			if (psCheckHD != null) try {psCheckHD.close(); } catch (SQLException e) {}
			if (psCheckKTHIS != null) try {psCheckKTHIS.close(); } catch (SQLException e) {}
			if (psHD != null) try { psHD.close(); } catch (SQLException e) {}
			if (rsHD != null) try { rsHD.close(); } catch (SQLException e) {}
			if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
			if (rsKthisCheck != null) try { rsKthisCheck.close(); } catch (SQLException e) {}
			if (connectionHD!=null) try { connectionHD.close();} catch (Exception ignore){}
			if (connectionKTHIS!=null) try { connectionKTHIS.close();} catch (Exception ignore){}
		   }
		return view;
	}
	
	/*public static List<ProtokolHdPartView> getvisitprotokolhdPart(String visitid) {
		Connection connectionHD = DbConnection.getMysqlOnlineQueueInstance2();
		Connection connectionKTHIS = DbConnection.getPooledConnection();
		if(connectionHD == null || connectionKTHIS == null)return null;
		ResultSet rsHD=null;
		ResultSet rsHDCheck=null;
		ResultSet rsKthisCheck=null;
		PreparedStatement psInsertKTHIS=null;
		PreparedStatement psHD=null;
		PreparedStatement psCheckHD=null;
		PreparedStatement psCheckKTHIS=null;
		List<ProtokolHdPartView> Listview = new ArrayList<ProtokolHdPartView>();

		String no_kartu = "";
		
		try {
			String visit = "SELECT V.VISIT_ID, TRUNC(v.admission_datetime) as admission_datetime, CA.CARD_NO FROM VISIT V INNER JOIN PATIENT PA "
					+ "ON PA.PATIENT_ID = V.PATIENT_ID INNER JOIN PERSON PE "
					+ "ON PE.PERSON_ID = PA.PERSON_ID INNER JOIN CARD CA "
					+ "ON CA.PERSON_ID = PE.PERSON_ID "
					+ "WHERE V.VISIT_ID = '"+visitid+"'";
			psCheckKTHIS = connectionKTHIS.prepareStatement(visit);
			rsKthisCheck = psCheckKTHIS.executeQuery();

			if(rsKthisCheck.next())
			{	
				no_kartu = rsKthisCheck.getString("CARD_NO");
				SimpleDateFormat visittime = new SimpleDateFormat("yyyy-MM-dd");
			    Date parsedDatevisit = visittime.parse(rsKthisCheck.getTimestamp("admission_datetime").toString());
			    String parsedDater = visittime.format(parsedDatevisit);
			    
				String waktu_instruksi = "SELECT wi.nokartu, wi.totwakmul, wi.tanggal FROM tb_waktu_instruksi wi where wi.`tanggal` = '"+parsedDater+"' and wi.nokartu = '"+no_kartu+"'";
					
				psHD = connectionHD.prepareStatement(waktu_instruksi);
				rsHD = psHD.executeQuery();
				while(rsHD.next())
				{			    
				    SimpleDateFormat dateFormatTanggal = new SimpleDateFormat("yyyy-MM-dd");
				    Date parsedDateTanggal = dateFormatTanggal.parse(rsHD.getString("tanggal"));
				    Timestamp tanggal = new java.sql.Timestamp(parsedDateTanggal.getTime());
				    
				    String[] parts = rsHD.getString("totwakmul").split("\\+");			    
				    String rightside = parts[0];
				    rightside = rightside.replace(".", ":").replace(";", ":").replace(" ", "").replace("O", "0").replace("`", "");
				    int counter = 0;
				    for( int i=0; i<rightside.length(); i++ ) {
				        if( rightside.charAt(i) == ':' ) {
				            counter++;
				        } 
				    }
				    if(counter == 1 && rightside.length() >= 4)
				    {
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				    	Date parsedDate = dateFormat.parse(rsHD.getString("tanggal") + " " +rightside);
					    Timestamp waktu_pemeriksaan = new java.sql.Timestamp(parsedDate.getTime());
					    
					    ProtokolHdPartView view = new ProtokolHdPartView();
					    view.setNokartu(rsHD.getBigDecimal("nokartu"));
					    view.getProtokolHdPart().setWaktuPemeriksaan(waktu_pemeriksaan);
					    view.getProtokolHdPart().setQbPemeriksaan(parts[1]);
					    view.getProtokolHdPart().setQdPemeriksaan(parts[2]);
					    view.getProtokolHdPart().setUfrPemeriksaan(parts[3]);
					    view.getProtokolHdPart().setTdPemeriksaan(parts[4]);
					    view.getProtokolHdPart().setNadiPemeriksaan(parts[5]);
					    view.getProtokolHdPart().setNrsPemeriksaan(parts[6]);
					    view.getProtokolHdPart().setSuhuPemeriksaan(parts[7]);
					    view.getProtokolHdPart().setRrPemeriksaan(parts[8]);
					    view.getProtokolHdPart().setVpPemeriksaan(parts[9]);
					    view.getProtokolHdPart().setApPemeriksaan(parts[10]);
					    view.getProtokolHdPart().setTmpPemeriksaan(parts[11]);
					    view.getProtokolHdPart().setKeteranganPemeriksaan(parts[12]);
					    view.getProtokolHdPart().setParafPemeriksa(parts[13]);
					    view.getProtokolHdPart().setCreatedDatetime(tanggal);
					    Listview.add(view);
				    }
				    else
				    {
				    	continue;
				    }
				    
				}
				if (psHD != null) try { psHD.close(); } catch (SQLException e) {}
				if (rsHD != null) try { rsHD.close(); } catch (SQLException e) {}
				connectionHD.close(); 
				connectionKTHIS.close();
			}
		}catch(Exception e){
			e.printStackTrace();
			if (connectionHD != null) try { connectionHD.rollback();} catch (Exception ex){}
			if (connectionKTHIS != null) try { connectionKTHIS.rollback();} catch (Exception ex){}
		}finally {
			if (psInsertKTHIS != null) try {psInsertKTHIS.close(); } catch (SQLException e) {}
			if (psCheckHD != null) try {psCheckHD.close(); } catch (SQLException e) {}
			if (psCheckKTHIS != null) try {psCheckKTHIS.close(); } catch (SQLException e) {}
			if (psHD != null) try { psHD.close(); } catch (SQLException e) {}
			if (rsHD != null) try { rsHD.close(); } catch (SQLException e) {}
			if (rsHDCheck != null) try { rsHDCheck.close(); } catch (SQLException e) {}
			if (rsKthisCheck != null) try { rsKthisCheck.close(); } catch (SQLException e) {}
		}		 
		return Listview;
	}*/
}