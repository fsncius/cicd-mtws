package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rsmurniteguh.webservice.dep.all.model.DataPasien;
import com.rsmurniteguh.webservice.dep.all.model.ListKpiQuer1;
import com.rsmurniteguh.webservice.dep.all.model.ListKpiQuer2;
import com.rsmurniteguh.webservice.dep.all.model.ListViewJenisKamar;
import com.rsmurniteguh.webservice.dep.all.model.ListViewKunjungan;
import com.rsmurniteguh.webservice.dep.all.model.ListViewPembiayaan;
import com.rsmurniteguh.webservice.dep.all.model.ListViewWaktuLayanIGD;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class KpiSystemSer extends DbConnection {
	
	public static List<ListKpiQuer1> getKpiQuer1() {
		 List<ListKpiQuer1> all= new ArrayList<ListKpiQuer1>();
		 Connection connection=DbConnection.getPooledConnection();
		 ResultSet haspas=null;
		 if(connection == null)return null;
		 Statement stmt=null;
		 String abpas="SELECT PS.PERSON_ID, PS.PERSON_NAME, PS.SEX, PS.BIRTH_DATE, PS.ID_NO, PS.TITLE, PS.MARITAL_STATUS, PS.NATIONALITY, PS.RELIGION, PS.RACE, CD.CARD_NO, PT.PATIENT_CLASS, CM.CODE_DESC, "
		 		+ "(TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) UMUR, "
		 		+ "CASE WHEN (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) = 5 THEN 'BALITA (0 - 5 ) TAHUN' "
		 		+ "WHEN (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) > 5 AND (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) <= 11 THEN 'MASA KANAK-KANAK (6-11) TAHUN' "
		 		+ "WHEN (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) > 11 AND (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) <= 16 THEN 'MASA REMAJA AWAL (12-16) TAHUN'  "
		 		+ "WHEN (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) > 16 AND (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) <= 25 THEN 'MASA REMAJA AKHIR (17-25) TAHUN' "
		 		+ "WHEN (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) > 25 AND (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) <= 35 THEN 'MASA DEWASA AWAL (26-35) TAHUN' "
		 		+ "WHEN (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) > 35 AND (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) <= 45 THEN 'MASA DEWASA AKHIR (36-45) TAHUN' "
		 		+ "WHEN (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) > 45 AND (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) <= 55 THEN 'MASA LANSIA AWAL (46-55) TAHUN'  "
		 		+ "WHEN (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) > 55 AND (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) <= 65 THEN 'MASA LANSIA AKHIR (56-65) TAHUN'  "
		 		+ "WHEN (TRUNC((TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd'))-TO_NUMBER(TO_CHAR(PS.BIRTH_DATE,'yyyymmdd')))/10000)) > 65 THEN 'MANULA ( >= 66 ) TAHUN' END AS KELOMPOKUMUR "
		 		+ "FROM PATIENT PT "
		 		+ "LEFT OUTER JOIN CARD CD ON PT.PERSON_ID = CD.PERSON_ID  "
		 		+ "INNER JOIN PERSON PS ON PS.PERSON_ID = PT.PERSON_ID  "
		 		+ "LEFT OUTER JOIN CODEMSTR CM ON PS.MARITAL_STATUS = 'CM.CODE_CAT + CM.CODE_ABBR'";
		 try 
		 	{
			 	stmt=connection.createStatement();
			 	haspas=stmt.executeQuery(abpas);
			 	while(haspas.next())
			 	{
				 	ListKpiQuer1 dp=new ListKpiQuer1();
				 	dp.setPERSON_ID(haspas.getString("PERSON_ID"));
				 	dp.setPERSON_NAME(haspas.getString("PERSON_NAME"));
				 	dp.setSEX(haspas.getString("SEX"));
				 	dp.setBIRTH_DATE(haspas.getString("BIRTH_DATE"));
				 	dp.setID_NO(haspas.getString("ID_NO"));
				 	dp.setTITLE(haspas.getString("TITLE"));
				 	dp.setMARITAL_STATUS(haspas.getString("MARITAL_STATUS"));
				 	dp.setNATIONALITY(haspas.getString("NATIONALITY"));
				 	dp.setRELIGION(haspas.getString("RELIGION"));
				 	dp.setRACE(haspas.getString("RACE"));
				 	dp.setCARD_NO(haspas.getString("CARD_NO"));
				 	dp.setPATIENT_CLASS(haspas.getString("PATIENT_CLASS"));
				 	dp.setCODE_DESC(haspas.getString("CODE_DESC"));
				 	dp.setUMUR(haspas.getString("UMUR"));
				 	dp.setKELOMPOKUMUR(haspas.getString("KELOMPOKUMUR"));
				 	all.add(dp);
			 	}
			 	haspas.close();
			 	stmt.close();
		 	} 
		 catch (SQLException e) 
		 	{
			 	System.out.println(e.getMessage());
		 	}
		 finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		 return all;
	}
	
	public static List<ListKpiQuer2> getKpiQuer2() {
		 List<ListKpiQuer2> all= new ArrayList<ListKpiQuer2>();
		 Connection connection=DbConnection.getPooledConnection();
		 ResultSet haspas=null;
		 if(connection == null)return null;
		 Statement stmt=null;
		 String abpas="SELECT DIAGNOSIS_CODE, DIAGNOSIS_DESC FROM (SELECT DM.DIAGNOSIS_CODE, DM.DIAGNOSIS_DESC "
		 		+ "FROM IHIS.VISITDIAGNOSISDETAIL  VDD INNER JOIN IHIS.DIAGNOSISMSTR  DM ON VDD.VISIT_DIAGNOSISMSTR_ID = DM.DIAGNOSISMSTR_ID "
		 		+ "AND DM.DIAGNOSIS_CODE <> 'XX01')  derivedtbl_1";
		 try 
		 	{
			 	stmt=connection.createStatement();
			 	haspas=stmt.executeQuery(abpas);
			 	while(haspas.next())
			 	{
				 	ListKpiQuer2 dp=new ListKpiQuer2();
				 	dp.setDIAGNOSIS_CODE(haspas.getString("DIAGNOSIS_CODE"));
				 	dp.setDIAGNOSIS_DESC(haspas.getString("DIAGNOSIS_DESC"));
				 	all.add(dp);
			 	}
			 	haspas.close();
			 	stmt.close();
		 	} 
		 catch (SQLException e) 
		 	{
			 	System.out.println(e.getMessage());
		 	}
		 finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		 return all;
	}
	
	
	
	
	public static List<ListViewJenisKamar> getViewJenisKamar()
	{
		List<ListViewJenisKamar> all = new ArrayList<ListViewJenisKamar>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet row=null;
		if(connection == null)return null;
		Statement stmt=null;
		String query="SELECT * FROM (SELECT V.VISIT_ID, VWH.TGL, BM.BED_DESC, RM.ROOM_DESC, WM.WARD_DESC, AM.ACCOMM_DESC "
				+ "FROM IHIS.VISIT V INNER JOIN (SELECT BH.VISIT_ID, MAX(BH.EFFECTIVE_START_DATETIME) AS TGL "
				+ "FROM IHIS.BEDHISTORY BH "
				+ "GROUP BY BH.VISIT_ID) VWH ON V.VISIT_ID = VWH.VISIT_ID "
				+ "INNER JOIN IHIS.BEDHISTORY VW ON VW.VISIT_ID = V.VISIT_ID AND VW.EFFECTIVE_START_DATETIME = VWH.TGL "
				+ "INNER JOIN IHIS.BEDMSTR BM ON BM.BEDMSTR_ID = VW.BEDMSTR_ID "
				+ "INNER JOIN IHIS.ROOMMSTR RM ON RM.ROOMMSTR_ID = BM.ROOMMSTR_ID "
				+ "INNER JOIN IHIS.WARDMSTR  WM ON WM.WARDMSTR_ID = RM.WARDMSTR_ID "
				+ "INNER JOIN IHIS.ACCOMMMSTR  AM ON AM.ACCOMMMSTR_ID = BM.ACCOMMMSTR_ID WHERE (V.PATIENT_TYPE = 'PTY1')) where rownum <=100";
		try
		{
			stmt=connection.createStatement();
		 	row=stmt.executeQuery(query);
		 	while(row.next())
		 	{
		 		ListViewJenisKamar lvjk = new ListViewJenisKamar();
		 		lvjk.setVISIT_ID(row.getBigDecimal("VISIT_ID"));
		 		lvjk.setTGL(row.getString("TGL"));
		 		lvjk.setBED_DESC(row.getString("BED_DESC"));
		 		lvjk.setROOM_DESC(row.getString("ROOM_DESC"));
		 		lvjk.setWARD_DESC(row.getString("WARD_DESC"));
		 		lvjk.setACCOMM_DESC(row.getString("ACCOMM_DESC"));
			 	all.add(lvjk);
		 	}
		 	row.close();
		 	stmt.close();
			
		}
		catch (SQLException e) 
	 	{
		 	System.out.println(e.getMessage());
	 	}
		finally {
		     if (row!=null) try  { row.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
		
		
		
	}
	
	public static List<ListViewKunjungan> getViewKunjungan()
	{
		List<ListViewKunjungan> all = new ArrayList<ListViewKunjungan>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet row=null;
		if(connection == null)return null;
		Statement stmt=null;
		String query="SELECT ROW_NUMBER() OVER (ORDER BY CAT) AS NO, CAT, COUNT(TOTV) AS TOTAL "
				+ "FROM (SELECT TOTALVISIT AS TOTV, "
				+ "(CASE WHEN maintab.totalvisit = 0 THEN 'TIDAK PERNAH VISIT' WHEN maintab.totalvisit = 1 THEN 'VISIT SATU KALI' WHEN maintab.totalvisit > 1 THEN 'VISIT LEBIH DARI SATU' END) AS CAT "
				+ "FROM (SELECT PS.PERSON_NAME, (SELECT COUNT(*) AS Expr1 "
				+ "FROM IHIS.VISIT V WHERE (V.PATIENT_ID = PT.PATIENT_ID)) AS TOTALVISIT "
				+ "FROM IHIS.PATIENT PT INNER JOIN IHIS.PERSON PS ON PS.PERSON_ID = PT.PERSON_ID) MAINTAB) MAINTAB2 "
				+ "GROUP BY CAT";
		try
		{
			stmt=connection.createStatement();
		 	row=stmt.executeQuery(query);
		 	while(row.next())
		 	{
			 	ListViewKunjungan lvk = new ListViewKunjungan();
			 	lvk.setNO(row.getString("NO"));
			 	lvk.setCAT(row.getString("CAT"));
			 	lvk.setTOTAL(row.getString("TOTAL"));
			 	all.add(lvk);
		 	}
		 	row.close();
		 	stmt.close();
			
		}
		catch (SQLException e) 
	 	{
		 	System.out.println(e.getMessage());
	 	}
		finally {
		     if (row!=null) try  { row.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
		
	}

	public static List<ListViewPembiayaan> getViewPembiayaan()
	{
		List<ListViewPembiayaan> all = new ArrayList<ListViewPembiayaan>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet row=null;
		if(connection == null)return null;
		Statement stmt=null;
		String query="SELECT JENIS, PATIENT_ID, ADMISSION_DATETIME "
				+ "FROM (SELECT  CASE WHEN SUBSTR(CM.CODE_DESC,1, 5) = 'PAKET' THEN 'PAKET MCU' "
				+ "WHEN  CM.CODE_DESC = 'CLASS I' OR CM.CODE_DESC = 'CLASS III' OR CM.CODE_DESC = 'VVIP' OR  CM.CODE_DESC = 'ICU / ICCU / PCU' OR CM.CODE_DESC = 'VIP' THEN 'GENERAL' "
				+ "ELSE CM.CODE_DESC END AS JENIS,V.PATIENT_ID, V.ADMISSION_DATETIME "
				+ "FROM IHIS.VISIT  V INNER JOIN IHIS.CODEMSTR CM ON CM.CODE_CAT = SUBSTR(V.PATIENT_CLASS, 1, 3) AND CM.CODE_ABBR = SUBSTR(V.PATIENT_CLASS, 4, 3) WHERE EXTRACT(YEAR FROM V.ADMISSION_DATETIME) >= EXTRACT(YEAR FROM SYSDATE) -1) derivedtbl_1";
		try
		{
			stmt=connection.createStatement();
		 	row=stmt.executeQuery(query);
		 	while(row.next())
		 	{
		 		ListViewPembiayaan lvp = new ListViewPembiayaan();
		 		lvp.setJENIS(row.getString("JENIS"));
		 		lvp.setPATIENT_ID(row.getBigDecimal("PATIENT_ID"));
		 		lvp.setADMISSION_DATETIME(row.getString("ADMISSION_DATETIME"));
			 	all.add(lvp);
		 	}
		 	row.close();
		 	stmt.close();
			
		}
		catch (SQLException e) 
	 	{
		 	System.out.println(e.getMessage());
	 	}
		finally {
		     if (row!=null) try  { row.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return all;
	}

	public static List<ListViewWaktuLayanIGD> getWaktuLayanIGD()
	{
		List<ListViewWaktuLayanIGD> all = new ArrayList<ListViewWaktuLayanIGD>();
		Connection connection=DbConnection.getPooledConnection();
		ResultSet row=null;
		if(connection == null)return null;
		Statement stmt=null;
		String query="SELECT VISIT_ID, ADMISSION_DATETIME, "
				+ "(SELECT MIN(OE.ENTERED_DATETIME) FROM IHIS.ORDERENTRY OE WHERE (OE.VISIT_ID=VISIT_ID) AND (OE.ENTERED_DATETIME IS NOT NULL)) AS WAKTUTUNGGU, "
				+ "CODE_DESC AS PATIENT_CLASS "
				+ "FROM (SELECT V.VISIT_ID, V.ADMISSION_DATETIME,(SELECT MIN(OE.ENTERED_DATETIME) FROM IHIS.ORDERENTRY OE "
				+ "WHERE (OE.VISIT_ID = V.VISIT_ID) AND (OE.ENTERED_DATETIME IS NOT NULL)), CM.CODE_DESC "
				+ "FROM IHIS.VISIT V INNER JOIN IHIS.CODEMSTR CM ON CONCAT(CM.CODE_CAT , CM.CODE_ABBR) = V.PATIENT_CLASS WHERE V.PATIENT_TYPE ='PTY2') derivedtbl_1";
		try
		{
			stmt=connection.createStatement();
		 	row=stmt.executeQuery(query);
		 	while(row.next())
		 	{
		 		ListViewWaktuLayanIGD lvwl = new ListViewWaktuLayanIGD();
		 		lvwl.setVISIT_ID(row.getBigDecimal("VISIT_ID"));
		 		lvwl.setADMISSION_DATETIME(row.getString("ADMISSION_DATETIME"));
		 		lvwl.setWAKTUTUNGGU(row.getString("WAKTUTUNGGU"));
		 		lvwl.setPATIENT_CLASS(row.getString("PATIENT_CLASS"));
			 	all.add(lvwl);
		 	}
		 	row.close();
		 	stmt.close();
			
		}
		catch (SQLException e) 
	 	{
		 	System.out.println(e.getMessage());
	 	}
		finally {
		     if (row!=null) try  { row.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		
		
		return all;
	}
	
	
	
	
}
