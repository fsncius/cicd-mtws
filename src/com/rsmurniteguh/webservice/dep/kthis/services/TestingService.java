package com.rsmurniteguh.webservice.dep.kthis.services;

import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.ReturnResult;
import com.rsmurniteguh.webservice.dep.all.model.TestModel;
import com.rsmurniteguh.webservice.dep.kthis.services.email.TestingEmail;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class TestingService extends DbConnection {

	public static ReturnResult save(TestModel test) {
		
		
		
		ReturnResult result = new ReturnResult();
		result.setSuccess(true);
		
		
		
		return result;
	}
	
	public static BaseResult testEmail(){
		return TestingEmail.getInstance().sendEmail();
	}
	
}
