package com.rsmurniteguh.webservice.dep.kthis.services;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bouncycastle.jce.provider.JDKKeyFactory.RSA;

import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.tasks.DailyEmailScheduledTask;
import com.rsmurniteguh.webservice.dep.util.FileUploadUtil;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.MedicalResumeSignature;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.BillReport;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.LabReport;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.PoctReport;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.RadiologyReport;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.ResumeMedis;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.SepReport;
import com.rsmurniteguh.webservice.dep.all.model.eclaim.SepReportDetail;
import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.ISysConstant;

public class EClaimService extends DbConnection {
	static SimpleDateFormat sdfFile = new SimpleDateFormat("yyyy\\MM\\dd\\");
	protected static String dir;
	protected static String reportName;

	public static List<ResumeMedis> getReportSep(String EClaimBPJS, String sepNo) {
		List<ResumeMedis> list = new ArrayList<ResumeMedis>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT PU.FILE_NAME, PU.FILE_TYPE, PU.FILE_UPLOAD, PU.FILE_CAT CATEGORY, PU.CREATED_AT CREATED, PU.MODIFIED_AT MODIFIED "
					+ " , V.ADMISSION_DATETIME, V.VISIT_ID " + " FROM PDFUPLOAD PU "
					+ " INNER JOIN SEPBPJS SB ON SB.VISIT_ID = PU.VISIT_ID "
					+ " INNER JOIN VISIT V ON V.VISIT_ID = SB.VISIT_ID " + " WHERE SB.SEP_NO = ? "
					+ " AND PU.DEFUNCT_IND = 'N' " + " AND SB.DEFUNCT_IND = 'N' " + " AND PU.FILE_CAT = 'SEP' ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			pstmtGetReport.setString(1, sepNo);

			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				ResumeMedis view = new ResumeMedis();
				view.setReportName(rsReport.getString("FILE_TYPE"));
				view.setFileName(rsReport.getString("FILE_NAME"));
				byte[] data = FileUploadUtil.getInstance().getByteFromServer(ISysConstant.SEP_FILE_LOCATION
						+ sdfFile.format(rsReport.getTimestamp("ADMISSION_DATETIME"))
						+ rsReport.getBigDecimal("VISIT_ID").toString() + "\\" + rsReport.getString("FILE_NAME"));
				view.setFileByte(data); // rsReport.getBytes("FILE_UPLOAD"));
				if (rsReport.getDate("MODIFIED") == null) {
					view.setTglUpload(rsReport.getTimestamp("CREATED"));
				} else {
					view.setTglUpload(rsReport.getTimestamp("MODIFIED"));
				}
				view.setReportCategory(rsReport.getString("CATEGORY"));
				list.add(view);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static List<ResumeMedis> getResumeMedis(String EClaimBPJS, String sepNo) {
		List<ResumeMedis> list = new ArrayList<ResumeMedis>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT PU.FILE_NAME, PU.FILE_TYPE, PU.FILE_UPLOAD, PU.FILE_CAT CATEGORY, PU.CREATED_AT CREATED, PU.MODIFIED_AT MODIFIED "
					+ " , V.ADMISSION_DATETIME, V.VISIT_ID " + " FROM PDFUPLOAD PU "
					+ " INNER JOIN SEPBPJS SB ON SB.VISIT_ID = PU.VISIT_ID "
					+ " INNER JOIN VISIT V ON V.VISIT_ID = SB.VISIT_ID " + " WHERE SB.SEP_NO = ? "
					+ " AND PU.DEFUNCT_IND = 'N' " + " AND SB.DEFUNCT_IND = 'N' "
					+ " AND PU.FILE_CAT = 'Medical Resume' ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			pstmtGetReport.setString(1, sepNo);

			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				ResumeMedis view = new ResumeMedis();
				view.setReportName(rsReport.getString("FILE_TYPE"));
				view.setFileName(rsReport.getString("FILE_NAME"));
				byte[] data = FileUploadUtil.getInstance().getByteFromServer(ISysConstant.SEP_FILE_LOCATION
						+ sdfFile.format(rsReport.getTimestamp("ADMISSION_DATETIME"))
						+ rsReport.getBigDecimal("VISIT_ID").toString() + "\\" + rsReport.getString("FILE_NAME"));
				view.setFileByte(data); // view.setFileByte(rsReport.getBytes("FILE_UPLOAD"));
				view.setReportCategory(rsReport.getString("CATEGORY"));
				if (rsReport.getDate("MODIFIED") == null) {
					view.setTglUpload(rsReport.getTimestamp("CREATED"));
				} else {
					view.setTglUpload(rsReport.getTimestamp("MODIFIED"));
				}
				list.add(view);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static MedicalResumeSignature GetSignatureReport(String EClaimBPJS, String sepNo) {
		MedicalResumeSignature mrs = new MedicalResumeSignature();
		Connection connection = DbConnection.getPooledConnection();
		ResultSet row = null;
		if (connection == null)
			return null;
		PreparedStatement stmt = null;
		try {
			String query = "SELECT * FROM(SELECT ME.VISIT_ID,ME.SIGNATURE FROM MEDICALRESUMESIGNATURE ME "
					+ "INNER JOIN SEPBPJS SB ON SB.VISIT_ID = ME.VISIT_ID " + "WHERE SB.SEP_NO='" + sepNo
					+ "' AND ME.DEFUNCT_IND ='N' ORDER BY ME.CREATED_AT DESC) WHERE ROWNUM=1";

			stmt = connection.prepareStatement(query);
			stmt.setEscapeProcessing(true);
			stmt.setQueryTimeout(10000);
			row = stmt.executeQuery();
			if (row.next()) {
				mrs.setVisitId(row.getString("VISIT_ID"));
				mrs.setSignature(row.getBytes("SIGNATURE"));
			}
			row.close();
			stmt.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (row != null)
				try {
					row.close();
				} catch (Exception ignore) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (Exception ignore) {
				}
		}
		return mrs;
	}

	public static List<LabReport> getLabReport(String EClaimBPJS, String sepNo) {
		List<LabReport> list = new ArrayList<LabReport>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		// LabReport view2 = new LabReport();
		// view2.setReportNo("testNo");
		// view2.setLabReportFile("testFile");
		// byte[] data2 = null;
		// try {
		// data2 =
		// FileUploadUtil.getInstance().getByteFromFtpFile(ISysConstant.LAB_FILE_LOCATION);
		// } catch (Exception e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }
		// view2.setFileByte(data2);
		// list.add(view2);
		// if (list.size() > 0)
		// return list;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = " SELECT DISTINCT "
					+ " FIRST_VALUE(LAB_REQUISITION_NO) OVER(PARTITION BY LAB_REPORT_FILE ORDER BY LAB_REQUISITION_NO) LAB_REQUISITION_NO "
					+ " ,LAB_REPORT_FILE "
					+ " ,FIRST_VALUE(LR.LAST_UPDATED_DATETIME) OVER(PARTITION BY LAB_REPORT_FILE ORDER BY LAB_REQUISITION_NO) LAST_UPDATED_DATETIME "
					+ " FROM   LAB_REQUISITION LR "
					+ " INNER JOIN ORDERENTRY OE ON OE.ORDERENTRY_ID = LR.ORDERENTRY_ID "
					+ " INNER JOIN SEPBPJS SB ON SB.VISIT_ID = OE.VISIT_ID " + " WHERE LR.LAB_REPORT_FILE IS NOT NULL "
					+ " AND SB.DEFUNCT_IND = 'N' " + " AND SB.SEP_NO = ?  " + "UNION ALL "
					+ "SELECT NULL LAB_REQUISITION_NO,TO_CHAR(BSL.CHARGEENTRY_ID) AS LAB_REPORT_FILE,BSL.CREATED_AT LAST_UPDATED_DATETIME "
					+ "FROM BLOODSUGARLEVEL BSL " + "INNER JOIN SEPBPJS SB ON SB.VISIT_ID = BSL.VISIT_ID "
					+ "WHERE BSL.DEFUNCT_IND='N' AND SB.SEP_NO= ? " + "ORDER BY LAB_REQUISITION_NO ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			pstmtGetReport.setString(1, sepNo);
			pstmtGetReport.setString(2, sepNo);

			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				LabReport view = new LabReport();
				String reportNo = rsReport.getString("LAB_REQUISITION_NO");

				if (reportNo != null) {
					view.setReportNo(reportNo);
					view.setLabReportFile(rsReport.getString("LAB_REPORT_FILE") + ".pdf");
					view.setTglUpload(rsReport.getTimestamp("LAST_UPDATED_DATETIME"));
					byte[] data = null;
					try {
						data = FileUploadUtil.getInstance().getByteFromFtpFile(
								ISysConstant.LAB_FILE_LOCATION + rsReport.getString("LAB_REPORT_FILE") + ".pdf");
					} catch (Exception ignore) {
					}
					view.setFileByte(data);
				} else {
					BigDecimal cid = new BigDecimal(rsReport.getString("LAB_REPORT_FILE"));

					view.setReportNo("");
					view.setLabReportFile("poct.pdf");
					view.setTglUpload(rsReport.getTimestamp("LAST_UPDATED_DATETIME"));

					Map<String, Object> paramMap = new HashMap<String, Object>();
					paramMap.put("cid", cid);

					ClassLoader classLoader = new DailyEmailScheduledTask().getClass().getClassLoader();
					File file = new File(classLoader.getResource("report/PT017_KGD_Report.jrxml").getFile());
					dir = file.getParent();

					String property = "java.io.tmpdir";

					String tempDir = System.getProperty(property);
					System.out.println("Directory : " + tempDir);

					JasperReport jasperReport = JasperCompileManager
							.compileReport(dir.concat("/PT017_KGD_Report.jrxml"));
					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, paramMap, pooledConnection);
					JasperExportManager.exportReportToPdfFile(jasperPrint, tempDir.concat("/poct.pdf"));

					byte[] data = FileUploadUtil.getInstance().getByteFromServer(tempDir.concat("/poct.pdf"));
					view.setFileByte(data);
				}
				list.add(view);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static List<String> getPendingLabReport(String fromdate, String todate) {
		List<String> list = new ArrayList<String>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			java.util.Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(fromdate);
			java.util.Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(todate);
			Calendar from =Calendar.getInstance();
			from.setTime(date1);
			Calendar to =Calendar.getInstance();
			to.setTime(date2);
			
			
			while (true) {
				if (from.compareTo(to) > 0)
					break;
				String month = String.format("%02d", from.get(Calendar.MONTH)+1);
				String date = String.format("%02d", from.get(Calendar.DATE));
				String year = String.format("%04d", from.get(Calendar.YEAR));
				String year2d = year.length() > 2 ? year.substring(year.length() - 2) : year;

				String searchlike = year2d + month + date;
				String selectQuery = " SELECT DISTINCT  FIRST_VALUE(LAB_REQUISITION_NO) "
						+ "OVER(PARTITION BY LAB_REPORT_FILE ORDER BY LAB_REQUISITION_NO) LAB_REQUISITION_NO  ,LAB_REPORT_FILE, "
						+ " FIRST_VALUE(LR.LAST_UPDATED_DATETIME) OVER(PARTITION BY LAB_REPORT_FILE ORDER BY LAB_REQUISITION_NO) LAST_UPDATED_DATETIME  "
						+ " FROM   LAB_REQUISITION LR  INNER JOIN ORDERENTRY OE ON OE.ORDERENTRY_ID = LR.ORDERENTRY_ID  "
						+ " INNER JOIN SEPBPJS SB ON SB.VISIT_ID = OE.VISIT_ID "
						+ " WHERE LR.LAB_REPORT_FILE IS NOT NULL  "
						+ " AND SB.DEFUNCT_IND = 'N'  AND lr.lab_report_file like  ? ";
				pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
				pstmtGetReport.setEscapeProcessing(true);
				pstmtGetReport.setQueryTimeout(10000);
				pstmtGetReport.setString(1, "%" + searchlike + "%");

				rsReport = pstmtGetReport.executeQuery();

				while (rsReport.next()) {
					String reportNo = rsReport.getString("LAB_REQUISITION_NO");

					if (reportNo != null) {
						byte[] data = null;
						try {
							data = FileUploadUtil.getInstance().getByteFromFtpFile(
									ISysConstant.LAB_FILE_LOCATION + rsReport.getString("LAB_REPORT_FILE") + ".pdf");
						} catch (Exception ignore) {
						}

						if (data == null) {
							list.add(rsReport.getString("LAB_REPORT_FILE"));
						}
					}
				}

				from.add(Calendar.DATE, 1);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();

		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static List<PoctReport> getPoctReport(String EClaimBPJS, String sepNo) {
		List<PoctReport> list = new ArrayList<PoctReport>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		String reportName = "Laporan Pemeriksaan Gula Rutin";
		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT BSL.VISIT_ID,BSL.CHARGEITEMMSTR_ID,BSL.CHARGEENTRY_ID,SB.SEP_NO "
					+ "FROM BLOODSUGARLEVEL BSL " + "INNER JOIN SEPBPJS SB ON SB.VISIT_ID = BSL.VISIT_ID "
					+ "WHERE BSL.DEFUNCT_IND='N' AND SB.SEP_NO= ? ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			pstmtGetReport.setString(1, sepNo);

			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				PoctReport view = new PoctReport();
				BigDecimal cid = rsReport.getBigDecimal("CHARGEENTRY_ID");
				view.setVisitId(rsReport.getBigDecimal("VISIT_ID"));
				view.setChargeItemId(rsReport.getBigDecimal("CHARGEITEMMSTR_ID"));
				view.setChargeEntryId(cid);
				view.setSepNo(rsReport.getString("SEP_NO"));

				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("cid", cid);

				ClassLoader classLoader = new DailyEmailScheduledTask().getClass().getClassLoader();
				File file = new File(classLoader.getResource("report/PT017_KGD_Report.jrxml").getFile());
				dir = file.getParent();

				String property = "java.io.tmpdir";

				String tempDir = System.getProperty(property);
				System.out.println("Directory : " + tempDir);

				JasperReport jasperReport = JasperCompileManager.compileReport(dir.concat("/PT017_KGD_Report.jrxml"));
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, paramMap, pooledConnection);
				JasperExportManager.exportReportToPdfFile(jasperPrint, tempDir.concat("/poct.pdf"));

				byte[] data = FileUploadUtil.getInstance().getByteFromServer(tempDir.concat("/poct.pdf"));
				view.setFileByte(data);
				list.add(view);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static List<RadiologyReport> getRadiologyReport(String EClaimBPJS, String sepNo) {
		List<RadiologyReport> list = new ArrayList<RadiologyReport>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT OEI.ORDERENTRYITEM_ID, OEI.LAST_UPDATED_DATETIME FROM ORDERENTRYITEM OEI "
					+ " INNER JOIN ORDERENTRY OE ON OE.ORDERENTRY_ID = OEI.ORDERENTRY_ID "
					+ " INNER JOIN SEPBPJS SB ON SB.VISIT_ID = OE.VISIT_ID " + " WHERE OE.ORDER_STATUS = 'OSTCON' "
					+ " AND SB.SEP_NO = ? " + " AND SB.DEFUNCT_IND = 'N' ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			pstmtGetReport.setString(1, sepNo);
			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				RadiologyReport view = new RadiologyReport();
				view.setAdmissionId(rsReport.getBigDecimal("ORDERENTRYITEM_ID"));
				view.setTglUpload(rsReport.getTimestamp("LAST_UPDATED_DATETIME"));
				// view.setReportName(rsReport.getString("FILE_NAME"));
				// view.setFileByte(rsReport.getBytes("FILE_UPLOAD"));
				// view.setReportCategory(rsReport.getString("CATEGORY"));
				list.add(view);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}

		return list;
	}

	public static BaseResult getCheckSep(String EClaimBPJS, String sepNo) {
		BaseResult result = new BaseResult();
		result.setResult(IConstant.IND_NO);
		result.setStatus(IConstant.STATUS_CONNECTED);

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT COUNT(*) TOTAL_COUNT FROM SEPBPJS SB "
					+ (EClaimBPJS.equals(ISysConstant.ECLAIM_REGISTRY_TYPE_TEMPORARY) ? ""
							: " INNER JOIN BPJS_SEP_IMPORT BSI ON BSI.SEP_NO = SB.SEP_NO ")
					+ " WHERE SB.SEP_NO = ? " + " AND SB.DEFUNCT_IND = 'N' ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);

			pstmtGetReport.setString(1, sepNo);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			rsReport = pstmtGetReport.executeQuery();

			if (rsReport.next()) {
				if (rsReport.getBigDecimal("TOTAL_COUNT").compareTo(BigDecimal.ZERO) > 0) {
					result.setResult(IConstant.IND_YES);
				}
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
			result.setStatus(IConstant.STATUS_SUCCESSFULL);
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
			result.setStatus(IConstant.STATUS_ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
			result.setStatus(IConstant.STATUS_ERROR);
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}

		return result;
	}

	public static List<ResumeMedis> getReportDll(String EClaimBPJS, String sepNo) {
		List<ResumeMedis> list = new ArrayList<ResumeMedis>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT PU.FILE_NAME, PU.FILE_TYPE, PU.FILE_UPLOAD, PU.FILE_CAT CATEGORY, PU.CREATED_AT CREATED, PU.MODIFIED_AT MODIFIED "
					+ " , V.ADMISSION_DATETIME, V.VISIT_ID " + " FROM PDFUPLOAD PU "
					+ " INNER JOIN SEPBPJS SB ON SB.VISIT_ID = PU.VISIT_ID "
					+ " INNER JOIN VISIT V ON V.VISIT_ID = SB.VISIT_ID " + " WHERE SB.SEP_NO = ? "
					+ " AND PU.DEFUNCT_IND = 'N' " + " AND SB.DEFUNCT_IND = 'N' "
					+ " AND PU.FILE_CAT NOT IN ('Medical Resume','SEP') ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			pstmtGetReport.setString(1, sepNo);

			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				ResumeMedis view = new ResumeMedis();
				view.setReportName(rsReport.getString("FILE_TYPE"));
				view.setFileName(rsReport.getString("FILE_NAME"));
				byte[] data = FileUploadUtil.getInstance().getByteFromServer(ISysConstant.SEP_FILE_LOCATION
						+ sdfFile.format(rsReport.getTimestamp("ADMISSION_DATETIME"))
						+ rsReport.getBigDecimal("VISIT_ID").toString() + "\\" + rsReport.getString("FILE_NAME"));
				view.setFileByte(data); // rsReport.getBytes("FILE_UPLOAD"));
				view.setReportCategory(rsReport.getString("CATEGORY"));
				if (rsReport.getDate("MODIFIED") == null) {
					view.setTglUpload(rsReport.getTimestamp("CREATED"));
				} else {
					view.setTglUpload(rsReport.getTimestamp("MODIFIED"));
				}
				list.add(view);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static List<BillReport> getBilling(String EClaimBPJS, String sepNo) {
		List<BillReport> list = new ArrayList<BillReport>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT B.BILL_ID FROM BILL B "
					+ " INNER JOIN PATIENTACCOUNT PAT ON PAT.PATIENTACCOUNT_ID = B.PATIENTACCOUNT_ID "
					+ " INNER JOIN SEPBPJS SB ON SB.VISIT_ID = PAT.VISIT_ID "
					+ (EClaimBPJS.equals(ISysConstant.ECLAIM_REGISTRY_TYPE_TEMPORARY) ? ""
							: " INNER JOIN BPJS_SEP_IMPORT BSI ON BSI.SEP_NO = SB.SEP_NO AND BSI.HOSPITAL_BILL = B.BILL_SIZE AND BSI.DEFUNCT_IND = 'N' ")
					+ " WHERE SB.DEFUNCT_IND = 'N' " + " AND B.BILL_STATUS <> 'BISC' " + " AND SB.SEP_NO = ? ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			pstmtGetReport.setString(1, sepNo);

			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				List<BillReport> blList = new ArrayList<BillReport>();
				blList = getBillingByBillId(rsReport.getBigDecimal("BILL_ID"));
				list.addAll(blList);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static List<SepReport> getSepReport(String EClaimBPJS, String fromDate, String toDate, String tipe,
			String noBatch) {
		List<SepReport> list = new ArrayList<SepReport>();
		HashMap<String, SepReport> listMap = new HashMap<String, SepReport>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "select sb.SEP_NO, per.PERSON_NAME, V.ADMISSION_DATETIME CREATED_AT, sb.BPJS_NO, cm.CODE_DESC PATIENT_TYPE, v.DISCHARGE_DATETIME "
					+ " ,sih.STATUS, sih.BA_NO, V.ADMISSION_DATETIME SIH_CREATED_AT from sepbpjs sb "
					+ " inner join visit v on v.visit_id = sb.visit_id "
					// + " inner join (select isb.sep_no, min(isb.created_at) created from sepbpjs
					// isb where isb.defunct_ind = 'N' group by isb.sep_no) sb1 on sb1.sep_no =
					// sb.sep_no and sb1.created = sb.created_at"
					+ " inner join patient pat on pat.patient_id = v.patient_id "
					+ " inner join person per on per.person_id = pat.person_id "
					+ " inner join codemstr cm on concat( cm.code_cat,  cm.code_abbr) = v.patient_type ";
			// if (noBatch != null && !noBatch.equals("")){
			selectQuery += " left join sep_import_history sih on sih.sep_no = sb.sep_no and sih.defunct_ind = 'N' ";
			// }
			if (!EClaimBPJS.equals(ISysConstant.ECLAIM_REGISTRY_TYPE_TEMPORARY)) {
				selectQuery += " INNER JOIN PATIENTACCOUNT PA ON PA.VISIT_ID = v.Visit_Id ";
				selectQuery += " INNER JOIN BILL B ON B.PATIENTACCOUNT_ID = PA.PATIENTACCOUNT_ID ";
				selectQuery += " INNER JOIN BPJS_SEP_IMPORT BSI ON BSI.SEP_NO = SB.SEP_NO AND BSI.DEFUNCT_IND = 'N' ";
				selectQuery += " inner join (select isb.sep_no, min(isb.created_at) created from sepbpjs isb "
						+ " inner join bpjs_sep_import bsi1 on bsi1.sep_no = isb.sep_no "
						+ " inner JOIN PATIENTACCOUNT PA ON PA.VISIT_ID = isb.Visit_Id "
						+ " inner JOIN BILL B ON B.PATIENTACCOUNT_ID = PA.PATIENTACCOUNT_ID and bsi1.hospital_bill = b.bill_size "
						+ " where isb.defunct_ind = 'N' " + " and bsi1.defunct_ind = 'N' "
						+ " group by isb.sep_no) sb1 on sb1.sep_no = sb.sep_no and sb1.created = sb.created_at ";
			}
			selectQuery += " where sb.defunct_ind = 'N' ";
			if (tipe != null && !tipe.equals("")) {
				if (!tipe.toLowerCase().equals("keluar")) {
					if (!EClaimBPJS.equals(ISysConstant.ECLAIM_REGISTRY_TYPE_TEMPORARY)) {
						selectQuery += " and trunc(BSI.ADMISSION_DATETIME) >= trunc(to_date('" + fromDate
								+ "','yyyy-MM-dd')) " + " and trunc(BSI.ADMISSION_DATETIME) <= trunc(to_date('" + toDate
								+ "','yyyy-MM-dd')) " + " AND BSI.HOSPITAL_BILL = B.BILL_SIZE ";
					} else {
						selectQuery += " and trunc(V.ADMISSION_DATETIME) >= trunc(to_date('" + fromDate
								+ "','yyyy-MM-dd')) " + " and trunc(V.ADMISSION_DATETIME) <= trunc(to_date('" + toDate
								+ "','yyyy-MM-dd')) ";
					}
					// selectQuery += " and trunc(sb.created_at) >= trunc(to_date('" + fromDate +
					// "','yyyy-MM-dd')) "
					// + " and trunc(sb.created_at) <= trunc(to_date('" + toDate + "','yyyy-MM-dd'))
					// ";
				} else {
					selectQuery += " and trunc(v.discharge_datetime) >= trunc(to_date('" + fromDate
							+ "','yyyy-MM-dd')) " + " and trunc(v.discharge_datetime) <= trunc(to_date('" + toDate
							+ "','yyyy-MM-dd')) ";
				}
			}
			if (noBatch != null && !noBatch.equals("")) {
				selectQuery += " and sih.BA_NO like '%" + noBatch.replace("'", "").trim() + "%' ";
			}
			// if (noBatch != null && !noBatch.equals("")){
			selectQuery += " order by sih.CREATED_AT ASC ";
			// }

			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				String sepNo = rsReport.getString("SEP_NO");
				if (!listMap.containsKey(sepNo)) {
					SepReport view = new SepReport();
					view.setNoSep(sepNo);
					view.setPatientName(rsReport.getString("PERSON_NAME"));
					view.setTglRequest(rsReport.getTimestamp("CREATED_AT"));
					view.setNoBPJS(rsReport.getString("BPJS_NO"));
					view.setPatientType(rsReport.getString("PATIENT_TYPE"));
					view.setTglDischarge(rsReport.getTimestamp("DISCHARGE_DATETIME"));
					view.setStatus(rsReport.getString("STATUS"));
					listMap.put(rsReport.getString("SEP_NO"), view);
				} else {
					SepReport rep = listMap.get(sepNo);
					if (rep.getStatus() != null && rep.getStatus().equals("")) {
						rep.setStatus(rsReport.getString("STATUS"));
					} else {
						rep.setStatus(rep.getStatus() + "," + rsReport.getString("STATUS"));
					}
					SepReportDetail srd = new SepReportDetail();
					srd.setNoBA(rsReport.getString("BA_NO"));
					srd.setCreatedAt(rsReport.getTimestamp("SIH_CREATED_AT"));
					srd.setStatus(rsReport.getString("STATUS"));
					rep.getHistory().add(srd);
				}
			}

			list = new ArrayList<SepReport>(listMap.values());

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static List<SepReportDetail> getNoBa(String EClaimBPJS, String noBa) {
		List<SepReportDetail> list = new ArrayList<SepReportDetail>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT BA_NO FROM SEP_IMPORT_HISTORY SIH "
					+ " WHERE DEFUNCT_IND = 'N' AND SIH.BA_NO like '%" + noBa.replace("'", "").trim() + "%' ";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			rsReport = pstmtGetReport.executeQuery();

			while (rsReport.next()) {
				SepReportDetail srd = new SepReportDetail();
				srd.setNoBA(rsReport.getString("BA_NO"));
				list.add(srd);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	private static List<BillReport> getBillingByBillId(BigDecimal billId) {
		List<BillReport> list = new ArrayList<BillReport>();

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		ResultSet rsReport = null;

		try {
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "SELECT AA.PERSON_NAME " + " ,AA.MRTIP " + " ,AA.SUBSPECIALTY_DESC "
					+ " ,AA.WARD_DESC " + " ,AA.PATIENT_CLASS " + " ,AA.VISIT_NO " + " ,AA.iscm_cat "
					+ " ,AA.txn_code, UPPER(AA.TXN_DESC) as TXN_DESC " + " ,AA.ORDERED_CAREPROVIDER_ID "
					+ " ,AA.DOCTOR_NAME " + " ,AA.qty_uom " + " ,AA.base_unit_price " + " ,AA.qty " + " ,AA.txn_amount "
					+ " ,AA.claimable_amount " + " ,AA.deposit_txn_amount " + " ,AA.bill_datetime " + " ,AA.TOTALDISC "
					+ " ,AA.ADMISSION_DATETIME " + " ,AA.DISCHARGE_DATETIME " + " ,AA.datecnt " + " ,AA.DIAGNOSIS_DESC "
					+ " ,AA.TXN_DATETIME " + " ,AA.BILL_ID " + " ,AA.USER_NAME,nvl(AA.remarks, ' ') as remarks "
					+ " FROM ( " + "  select p.PERSON_NAME " + "   ,pgpmi.fxGetMRN(pt.PATIENT_ID, 'MRTIP') MRTIP "
					+ "   ,sspm.SUBSPECIALTY_DESC " + "   ,wm.WARD_DESC "
					+ "   ,pgpmi.fxGetFinancialClassNS(v.VISIT_ID) PATIENT_CLASS " + "   ,v.VISIT_NO "
					+ "   ,PGCOMMON.fxGetCodeDesc(iscm.ip_receipt_item_cat) iscm_cat "
					+ "   ,tcm.txn_code,pat.TXN_DESC " + "   ,cg.ordered_careprovider_id "
					+ "   ,d.person_name DOCTOR_NAME " + "   ,pat.txn_datetime " + "   ,bl.bill_id "
					+ "   ,um.user_name " + "   ,pat.remarks "
					+ "   ,PGCOMMON.fxGetCodeDesc(cg.qty_uom) qty_uom,cg.base_unit_price ,sum(cg.qty) qty "
					+ "   ,sum(pat.txn_amount) txn_amount " + "   ,sum(pat.claimable_amount) claimable_amount "
					+ "   ,sum(pat.deposit_txn_amount) deposit_txn_amount " + "   ,bl.bill_datetime "
					+ "   ,(SELECT sum(BB.TOTALDISC) FROM ( "
					+ "      select bl.bill_id,sum(pat.claimable_amount) TOTALDISC "
					+ "      from bill bl,patientaccounttxn pat,txncodemstr tcm "
					+ "           ,chargeitemmstr cim,itemsubcategorymstr iscm,charge cg, patientaccount pa, visit v, patient pt, person p "
					+ "           ,SUBSPECIALTYMSTR sspm, WARDMSTR wm, careprovider cp, person d, usermstr um "
					+ "      where bl.bill_id = ? " // 480963142 --$P{billId} "
					+ "      and cg.ordered_careprovider_id = cp.careprovider_id (+) "
					+ "      and cp.person_id = d.person_id (+) " + "      and bl.bill_id = pat.bill_id "
					+ "      and pat.PATIENTACCOUNT_ID =bl.PATIENTACCOUNT_ID "
					+ "      and pat.patientaccounttxn_id = cg.patientaccounttxn_id "
					+ "      and pat.txncodemstr_id = tcm.txncodemstr_id "
					+ "      and tcm.txncodemstr_id = cim.txncodemstr_id "
					+ "      and cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id "
					+ "      and pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID " + "      and v.VISIT_ID = pa.VISIT_ID "
					+ "      and pt.PATIENT_ID = v.PATIENT_ID " + "      and p.PERSON_ID = pt.PERSON_ID "
					+ "      and sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID "
					+ "      and wm.WARDMSTR_ID(+) = v.WARDMSTR_ID " + "      and pat.CHARGE_STATUS = 'CTTNOR' "
					+ "      and bl.entered_by = um.usermstr_id " + "      group by bl.bill_id " + "     UNION ALL "
					+ "      select bl.bill_id,sum(pat.claimable_amount) TOTALDISC "
					+ "      from bill bl,patientaccounttxn pat,txncodemstr tcm "
					+ "           ,chargeitemmstr cim,itemsubcategorymstr iscm,charge cg, patientaccount pa, visit v, patient pt, person p "
					+ "           ,SUBSPECIALTYMSTR sspm, WARDMSTR wm, careprovider cp, person d, usermstr um "
					+ "      where bl.bill_id = ? " // 480963142 --$P{billId} "
					+ "      and cg.ordered_careprovider_id  = cp.careprovider_id (+) "
					+ "      and cp.person_id  = d.person_id (+) " + "      and bl.bill_id = pat.bill_id "
					+ "      and pat.PATIENTACCOUNT_ID <>bl.PATIENTACCOUNT_ID "
					+ "      and pat.patientaccounttxn_id = cg.patientaccounttxn_id "
					+ "      and pat.txncodemstr_id = tcm.txncodemstr_id "
					+ "      and tcm.txncodemstr_id = cim.txncodemstr_id "
					+ "      and cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id "
					+ "      and pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID " + "      and v.VISIT_ID = pa.VISIT_ID "
					+ "      and pt.PATIENT_ID = v.PATIENT_ID " + "      and p.PERSON_ID = pt.PERSON_ID "
					+ "      and sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID "
					+ "      and wm.WARDMSTR_ID(+) = v.WARDMSTR_ID " + "      and pat.CHARGE_STATUS = 'CTTNOR' "
					+ "      and bl.entered_by = um.usermstr_id " + "      group by bl.bill_id) BB "
					+ "      WHERE BB.bill_id = bl.bill_id " + "   ) TOTALDISC "
					+ "   ,v.ADMISSION_DATETIME, v.DISCHARGE_DATETIME "
					+ "   ,decode(TRUNC(NVL(v.DISCHARGE_DATETIME,bl.BILL_DATETIME)) - trunc(v.ADMISSION_DATETIME),0,1,TRUNC(NVL(v.DISCHARGE_DATETIME,bl.BILL_DATETIME)) - trunc(v.ADMISSION_DATETIME)) datecnt "
					+ "   ,(select decode( vdd.VISIT_DIAGNOSIS_DESC, null, dn.DIAGNOSIS_DESC, dn.DIAGNOSIS_DESC||vdd.VISIT_DIAGNOSISMSTR_ID) "
					+ "      from VISITDIAGNOSIS vd, VISITDIAGNOSISDETAIL vdd, DIAGNOSISMSTR dn "
					+ "      where vd.VISIT_ID = v.VISIT_ID "
					+ "      and vdd.VISITDIAGNOSIS_ID = vd.VISITDIAGNOSIS_ID "
					+ "      and dn.DIAGNOSISMSTR_ID(+) = vdd.VISIT_DIAGNOSISMSTR_ID "
					+ "      and vd.DIAGNOSIS_TYPE = 'DGT3' " + "      and vdd.DIAGNOSIS_STATUS = 'DGSM' "
					+ "      and rownum = 1) DIAGNOSIS_DESC " + "  from bill bl,patientaccounttxn pat,txncodemstr tcm "
					+ "       ,chargeitemmstr cim,itemsubcategorymstr iscm,charge cg, patientaccount pa, visit v, patient pt, person p "
					+ "       ,SUBSPECIALTYMSTR sspm, WARDMSTR wm, careprovider cp, person d, usermstr um "
					+ "  where bl.bill_id = ? " // bl.bill_id = 480963142 --$P{billId} "
					+ "  and cg.ordered_careprovider_id = cp.careprovider_id (+) "
					+ "  and cp.person_id = d.person_id (+) " + "  and bl.bill_id = pat.bill_id "
					+ "  and pat.PATIENTACCOUNT_ID =bl.PATIENTACCOUNT_ID "
					+ "  and pat.patientaccounttxn_id = cg.patientaccounttxn_id "
					+ "  and pat.txncodemstr_id = tcm.txncodemstr_id "
					+ "  and tcm.txncodemstr_id = cim.txncodemstr_id "
					+ "  and cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id "
					+ "  and pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID " + "  and v.VISIT_ID = pa.VISIT_ID "
					+ "  and pt.PATIENT_ID = v.PATIENT_ID " + "  and p.PERSON_ID = pt.PERSON_ID "
					+ "  and sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID "
					+ "  and wm.WARDMSTR_ID(+) = v.WARDMSTR_ID " + "  and pat.CHARGE_STATUS = 'CTTNOR' "
					+ "  and bl.entered_by = um.usermstr_id " + "  group by p.PERSON_NAME " + "     ,pt.PATIENT_ID "
					+ "     ,sspm.SUBSPECIALTY_DESC " + "     ,wm.WARD_DESC " + "     ,v.VISIT_ID "
					+ "     ,d.person_name " + "     ,v.VISIT_NO " + "     ,iscm.ip_receipt_item_cat "
					+ "     ,tcm.txn_code " + "     ,pat.TXN_DESC " + "     ,cg.qty_uom " + "     ,base_unit_price "
					+ "     ,v.ADMISSION_DATETIME " + "     ,v.DISCHARGE_DATETIME " + "     ,bl.bill_datetime "
					+ "     ,cg.ordered_careprovider_id " + "     ,pat.txn_datetime " + "     ,bl.bill_id "
					+ "     ,um.user_name " + "     ,pat.remarks " + "UNION ALL " + " select p.PERSON_NAME "
					+ "  ,pgpmi.fxGetMRN(pt.PATIENT_ID, 'MRTIP') MRTIP " + "  ,sspm.SUBSPECIALTY_DESC "
					+ "  ,wm.WARD_DESC " + "  ,pgpmi.fxGetFinancialClassNS(v.VISIT_ID) PATIENT_CLASS "
					+ "  ,v.VISIT_NO " + "  ,'BABY COST' iscm_cat " + "  ,tcm.txn_code,UPPER(pat.TXN_DESC) as TXN_DESC "
					+ "  ,cg.ordered_careprovider_id " + "  ,d.person_name DOCTOR_NAME " + "  ,pat.txn_datetime "
					+ "  ,bl.bill_id " + "  ,um.user_name " + "  ,pat.remarks "
					+ "  ,PGCOMMON.fxGetCodeDesc(cg.qty_uom) qty_uom,cg.base_unit_price ,sum(cg.qty) qty "
					+ "  ,sum(pat.txn_amount) txn_amount,sum(pat.claimable_amount) claimable_amount, sum(pat.deposit_txn_amount) deposit_txn_amount,bl.bill_datetime "
					+ "  ,(SELECT  sum(BB.TOTALDISC) "
					+ " FROM (select bl.bill_id, sum(pat.claimable_amount) TOTALDISC "
					+ "   from  bill bl,patientaccounttxn pat,txncodemstr tcm, "
					+ "         chargeitemmstr cim,itemsubcategorymstr iscm,charge cg, patientaccount pa, visit v, patient pt, person p "
					+ "         , SUBSPECIALTYMSTR sspm, WARDMSTR wm, careprovider cp, person d, usermstr um "
					+ "   where bl.bill_id = ? " // 480963142--$P{billId} "
					+ "   and cg.ordered_careprovider_id = cp.careprovider_id (+) "
					+ "   and cp.person_id = d.person_id (+) " + "   and bl.bill_id = pat.bill_id "
					+ "   and pat.PATIENTACCOUNT_ID =bl.PATIENTACCOUNT_ID "
					+ "   and pat.patientaccounttxn_id = cg.patientaccounttxn_id "
					+ "   and pat.txncodemstr_id = tcm.txncodemstr_id "
					+ "   and tcm.txncodemstr_id = cim.txncodemstr_id "
					+ "   and cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id "
					+ "   and pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID " + "   and v.VISIT_ID = pa.VISIT_ID "
					+ "   and pt.PATIENT_ID = v.PATIENT_ID " + "   and p.PERSON_ID = pt.PERSON_ID "
					+ "   and sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID "
					+ "   and wm.WARDMSTR_ID(+) = v.WARDMSTR_ID " + "   and pat.CHARGE_STATUS = 'CTTNOR' "
					+ "   and bl.entered_by = um.usermstr_id " + "   group by bl.bill_id " + "  UNION ALL "
					+ "   select bl.bill_id, sum(pat.claimable_amount) TOTALDISC "
					+ "   from  bill bl,patientaccounttxn pat,txncodemstr tcm "
					+ "         ,chargeitemmstr cim,itemsubcategorymstr iscm,charge cg, patientaccount pa, visit v, patient pt, person p "
					+ "         ,SUBSPECIALTYMSTR sspm, WARDMSTR wm, careprovider cp, person d, usermstr um "
					+ "   where bl.bill_id = ? " // 480963142--$P{billId} "
					+ "   and cg.ordered_careprovider_id  = cp.careprovider_id (+) "
					+ "   and cp.person_id  = d.person_id (+) " + "   and bl.bill_id = pat.bill_id "
					+ "   and pat.PATIENTACCOUNT_ID <>bl.PATIENTACCOUNT_ID "
					+ "   and pat.patientaccounttxn_id = cg.patientaccounttxn_id "
					+ "   and pat.txncodemstr_id = tcm.txncodemstr_id "
					+ "   and tcm.txncodemstr_id = cim.txncodemstr_id "
					+ "   and cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id "
					+ "   and pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID " + "   and v.VISIT_ID = pa.VISIT_ID "
					+ "   and pt.PATIENT_ID = v.PATIENT_ID " + "   and p.PERSON_ID = pt.PERSON_ID "
					+ "   and sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID "
					+ "   and wm.WARDMSTR_ID(+) = v.WARDMSTR_ID " + "   and pat.CHARGE_STATUS = 'CTTNOR' "
					+ "   and bl.entered_by = um.usermstr_id " + "   group by bl.bill_id) BB "
					+ "   WHERE BB.bill_id = bl.bill_id" + " ) TOTALDISC "
					+ " ,v.ADMISSION_DATETIME, v.DISCHARGE_DATETIME "
					+ " ,decode(TRUNC(NVL(v.DISCHARGE_DATETIME,bl.BILL_DATETIME)) - trunc(v.ADMISSION_DATETIME),0,1,TRUNC(NVL(v.DISCHARGE_DATETIME,bl.BILL_DATETIME)) - trunc(v.ADMISSION_DATETIME)) datecnt "
					+ " ,(select decode( vdd.VISIT_DIAGNOSIS_DESC, null, dn.DIAGNOSIS_DESC, dn.DIAGNOSIS_DESC||vdd.VISIT_DIAGNOSISMSTR_ID) "
					+ "   from VISITDIAGNOSIS vd, VISITDIAGNOSISDETAIL vdd, DIAGNOSISMSTR dn "
					+ "   where vd.VISIT_ID = v.VISIT_ID " + "   and vdd.VISITDIAGNOSIS_ID = vd.VISITDIAGNOSIS_ID "
					+ "   and dn.DIAGNOSISMSTR_ID(+) = vdd.VISIT_DIAGNOSISMSTR_ID "
					+ "   and vd.DIAGNOSIS_TYPE = 'DGT3' " + "   and vdd.DIAGNOSIS_STATUS = 'DGSM' "
					+ "   and rownum = 1) DIAGNOSIS_DESC " + " from  bill bl,patientaccounttxn pat,txncodemstr tcm "
					+ "       ,chargeitemmstr cim,itemsubcategorymstr iscm,charge cg, patientaccount pa, visit v, patient pt, person p "
					+ "       ,SUBSPECIALTYMSTR sspm, WARDMSTR wm, careprovider cp, person d, usermstr um "
					+ " where bl.bill_id = ? " // bl.bill_id = --$P{billId} "
					+ " and cg.ordered_careprovider_id  = cp.careprovider_id (+) "
					+ " and cp.person_id  = d.person_id (+) " + " and bl.bill_id = pat.bill_id "
					+ " and pat.PATIENTACCOUNT_ID <>bl.PATIENTACCOUNT_ID "
					+ " and pat.patientaccounttxn_id = cg.patientaccounttxn_id and pat.txncodemstr_id = tcm.txncodemstr_id "
					+ " and tcm.txncodemstr_id = cim.txncodemstr_id "
					+ " and cim.itemsubcategorymstr_id = iscm.itemsubcategorymstr_id "
					+ " and pa.PATIENTACCOUNT_ID = bl.PATIENTACCOUNT_ID " + " and v.VISIT_ID = pa.VISIT_ID "
					+ " and pt.PATIENT_ID = v.PATIENT_ID " + " and p.PERSON_ID = pt.PERSON_ID "
					+ " and sspm.SUBSPECIALTYMSTR_ID(+) = v.SUBSPECIALTYMSTR_ID "
					+ " and wm.WARDMSTR_ID(+) = v.WARDMSTR_ID " + " and pat.CHARGE_STATUS = 'CTTNOR' "
					+ " and bl.entered_by = um.usermstr_id " + " group by p.PERSON_NAME " + "     ,pt.PATIENT_ID "
					+ "     ,sspm.SUBSPECIALTY_DESC " + "     ,wm.WARD_DESC " + "     ,v.VISIT_ID "
					+ "     ,d.person_name " + "     ,v.VISIT_NO " + "     ,iscm.ip_receipt_item_cat "
					+ "     ,tcm.txn_code " + "     ,pat.TXN_DESC " + "     ,cg.qty_uom " + "     ,base_unit_price "
					+ "     ,v.ADMISSION_DATETIME " + "     ,v.DISCHARGE_DATETIME " + "     ,bl.bill_datetime "
					+ "     ,cg.ordered_careprovider_id " + "     ,pat.txn_datetime " + "     ,bl.bill_id "
					+ "     ,um.user_name " + "     ,pat.remarks " + ") AA " + " order by AA.iscm_cat, AA.txn_datetime";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			pstmtGetReport.setBigDecimal(1, billId);
			pstmtGetReport.setBigDecimal(2, billId);
			pstmtGetReport.setBigDecimal(3, billId);
			pstmtGetReport.setBigDecimal(4, billId);
			pstmtGetReport.setBigDecimal(5, billId);
			pstmtGetReport.setBigDecimal(6, billId);

			rsReport = pstmtGetReport.executeQuery();

			SimpleDateFormat formatTgl = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat formatTglWaktu = new SimpleDateFormat("dd-MM-yyyy HH:mm");

			while (rsReport.next()) {
				BillReport view = new BillReport();
				view.setPersonName(rsReport.getString("PERSON_NAME"));
				view.setMrnNo(rsReport.getString("MRTIP"));
				view.setDepartment(rsReport.getString("SUBSPECIALTY_DESC"));
				view.setWardDesc(rsReport.getString("WARD_DESC"));
				view.setVisitNo(rsReport.getString("VISIT_NO"));
				view.setIscmCat(rsReport.getString("ISCM_CAT"));
				view.setTxnCode(rsReport.getString("TXN_CODE"));
				view.setTxnDesc(rsReport.getString("TXN_DESC"));
				view.setDoctorName(rsReport.getString("DOCTOR_NAME"));
				view.setQtyDesc(rsReport.getString("QTY_UOM"));
				view.setBaseUnitPrice(rsReport.getString("BASE_UNIT_PRICE"));
				view.setQty(rsReport.getBigDecimal("QTY") == null ? rsReport.getString("QTY")
						: rsReport.getBigDecimal("QTY").toString());
				view.setTxnAmt(rsReport.getBigDecimal("TXN_AMOUNT") == null ? rsReport.getString("TXN_AMOUNT")
						: rsReport.getBigDecimal("TXN_AMOUNT").toString());
				view.setClaimableAmt(
						rsReport.getBigDecimal("CLAIMABLE_AMOUNT") == null ? rsReport.getString("CLAIMABLE_AMOUNT")
								: rsReport.getBigDecimal("CLAIMABLE_AMOUNT").toString());
				view.setDepositTxnAmt(
						rsReport.getBigDecimal("DEPOSIT_TXN_AMOUNT") == null ? rsReport.getString("DEPOSIT_TXN_AMOUNT")
								: rsReport.getBigDecimal("DEPOSIT_TXN_AMOUNT").toString());
				view.setBillDatetime(rsReport.getTimestamp("BILL_DATETIME") != null
						? formatTgl.format(new Date(rsReport.getTimestamp("BILL_DATETIME").getTime()))
						: "");
				view.setTotalDisc(rsReport.getBigDecimal("TOTALDISC").toString());
				view.setAdmissionDatetime(rsReport.getTimestamp("ADMISSION_DATETIME") != null
						? formatTgl.format(new Date(rsReport.getTimestamp("ADMISSION_DATETIME").getTime()))
						: "");
				view.setDischargeDatetime(rsReport.getTimestamp("DISCHARGE_DATETIME") != null
						? formatTgl.format(new Date(rsReport.getTimestamp("DISCHARGE_DATETIME").getTime()))
						: "");
				view.setJangka(rsReport.getString("DATECNT"));
				view.setDiagnosisDesc(rsReport.getString("DIAGNOSIS_DESC"));
				view.setTxnDatetime(rsReport.getTimestamp("TXN_DATETIME") != null
						? formatTglWaktu.format(new Date(rsReport.getTimestamp("TXN_DATETIME").getTime()))
						: "");
				view.setBillId(rsReport.getBigDecimal("BILL_ID"));
				view.setCashierName(rsReport.getString("USER_NAME"));
				view.setRemarks(rsReport.getString("REMARKS"));
				list.add(view);
			}

			rsReport.close();
			pstmtGetReport.close();
			pooledConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
		}
		return list;
	}

	public static BaseResult getCheckTxnStatus(String EClaimBPJS, String sepNo) {
		BaseResult result = new BaseResult();
		result.setResult(IConstant.IND_NO);
		result.setStatus(IConstant.STATUS_CONNECTED);

		Connection pooledConnection = null;
		PreparedStatement pstmtGetReport = null;
		PreparedStatement pstmtGetReport2 = null;
		ResultSet rsReport = null;
		ResultSet rsReport2 = null;

		try {
			Boolean isBill = false;
			Boolean isCharge = false;
			pooledConnection = DbConnection.getPooledConnection();
			if (pooledConnection == null)
				throw new Exception("Database failed to connect !");

			String selectQuery = "select count(*) TOTAL_COUNT from bill b "
					+ " inner join patientaccount pa on pa.patientaccount_id = b.patientaccount_id "
					+ " inner join sepbpjs sb on sb.visit_id = pa.visit_id "
					+ " where sb.sep_no = ? and b.bill_status <> 'BISC'";
			pstmtGetReport = pooledConnection.prepareStatement(selectQuery);
			pstmtGetReport.setEscapeProcessing(true);
			pstmtGetReport.setQueryTimeout(10000);
			pstmtGetReport.setString(1, sepNo);

			rsReport = pstmtGetReport.executeQuery();

			if (rsReport.next()) {
				if (rsReport.getBigDecimal("TOTAL_COUNT").compareTo(BigDecimal.ZERO) > 0) {
					isBill = true;
				}
			}

			rsReport.close();
			pstmtGetReport.close();

			String selectCharge = "select count(*) TOTAL_COUNT from patientaccounttxn pat "
					+ " inner join patientaccount pa on pa.patientaccount_id = pat.patientaccount_id "
					+ " inner join sepbpjs sb on sb.visit_id = pa.visit_id " + " where sb.sep_no = ? ";
			pstmtGetReport2 = pooledConnection.prepareStatement(selectCharge);

			pstmtGetReport2.setString(1, sepNo);
			pstmtGetReport2.setEscapeProcessing(true);
			pstmtGetReport2.setQueryTimeout(10000);
			rsReport2 = pstmtGetReport2.executeQuery();

			if (rsReport2.next()) {
				if (rsReport2.getBigDecimal("TOTAL_COUNT").compareTo(BigDecimal.ZERO) > 0) {
					isCharge = true;
				}
			}
			rsReport2.close();
			pstmtGetReport2.close();
			pooledConnection.close();

			if (!EClaimBPJS.equals(ISysConstant.ECLAIM_REGISTRY_TYPE_TEMPORARY)) {
				List listBill = getBilling(EClaimBPJS, sepNo);
				if (listBill.size() > 0) {
					result.setResult(IConstant.IND_YES);
				} else {
					result.setResult("Menunggu konfirmasi");
				}
			} else {
				if (!isCharge) {
					result.setResult("Pasien belum melakukan penginputan order");
				} else if (!isBill) {
					result.setResult("Bill dari pasien belum digenerate");
				} else if (isCharge && isBill) {
					result.setResult(IConstant.IND_YES);
				} else {
					result.setResult("Mohon Hubungi administrator terkait No. SEP : " + sepNo);
				}
			}
			result.setStatus(IConstant.STATUS_SUCCESSFULL);
		} catch (SQLException e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
			result.setResult("Mohon Hubungi administrator terkait No. SEP : " + sepNo);
			result.setStatus(IConstant.STATUS_ERROR);
			result.setErrorMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			if (pooledConnection != null)
				try {
					pooledConnection.rollback();
				} catch (SQLException sqle) {
				}
			result.setResult("Mohon Hubungi administrator terkait No. SEP : " + sepNo);
			result.setStatus(IConstant.STATUS_ERROR);
			result.setErrorMsg(e.getMessage());
		} finally {
			if (pooledConnection != null)
				try {
					pooledConnection.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport != null)
				try {
					pstmtGetReport.close();
				} catch (SQLException e) {
				}
			if (rsReport != null)
				try {
					rsReport.close();
				} catch (SQLException e) {
				}
			if (pstmtGetReport2 != null)
				try {
					pstmtGetReport2.close();
				} catch (SQLException e) {
				}
			if (rsReport2 != null)
				try {
					rsReport2.close();
				} catch (SQLException e) {
				}
		}

		return result;
	}
}