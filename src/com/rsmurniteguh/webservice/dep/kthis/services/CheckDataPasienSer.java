package com.rsmurniteguh.webservice.dep.kthis.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.databind.deser.Deserializers.Base;
import com.rsmurniteguh.webservice.dep.all.model.BaseResult;
import com.rsmurniteguh.webservice.dep.all.model.DataPasien;
import com.rsmurniteguh.webservice.dep.all.model.InfoUserNameKthis;
import com.rsmurniteguh.webservice.dep.all.model.InfoVisitPasien;
import com.rsmurniteguh.webservice.dep.all.model.Listcekdiagnos;
import com.rsmurniteguh.webservice.dep.all.model.visitdiagnosisdetail;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.MedicalRecordNoView;
import com.rsmurniteguh.webservice.dep.all.model.mtregistrasi.NewPatient;
import com.rsmurniteguh.webservice.dep.base.ICodeMstrConst;
import com.rsmurniteguh.webservice.dep.base.ICodeMstrDetailConst;
import com.rsmurniteguh.webservice.dep.base.IConstant;
import com.rsmurniteguh.webservice.dep.base.IPACommonConst;
import com.rsmurniteguh.webservice.dep.base.IParameterKTHISNameConst;
import com.rsmurniteguh.webservice.dep.base.ISequenceNoConst;
import com.rsmurniteguh.webservice.dep.base.ISysConstant;
import com.rsmurniteguh.webservice.dep.base.ITxnCodeMstrConst;
import com.rsmurniteguh.webservice.dep.kthis.model.Accountdebtor;
import com.rsmurniteguh.webservice.dep.kthis.model.Address;
import com.rsmurniteguh.webservice.dep.kthis.model.Card;
import com.rsmurniteguh.webservice.dep.kthis.model.Cardstatushistory;
import com.rsmurniteguh.webservice.dep.kthis.model.Countercollection;
import com.rsmurniteguh.webservice.dep.kthis.model.CustomerClassHistory;
import com.rsmurniteguh.webservice.dep.kthis.model.DropDownBo;
import com.rsmurniteguh.webservice.dep.kthis.model.Person;
import com.rsmurniteguh.webservice.dep.kthis.model.Receipt;
import com.rsmurniteguh.webservice.dep.kthis.model.Receipthistory;
import com.rsmurniteguh.webservice.dep.kthis.model.StakeHolder;
import com.rsmurniteguh.webservice.dep.kthis.model.StakeholderAccountTxn;
import com.rsmurniteguh.webservice.dep.kthis.model.StakeholderCollection;
import com.rsmurniteguh.webservice.dep.kthis.model.TxnCodeMstr;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.kthis.view.CardManageView;
import com.rsmurniteguh.webservice.dep.kthis.view.PersonView;
import com.rsmurniteguh.webservice.dep.kthis.view.RegisterOption;
import com.rsmurniteguh.webservice.dep.util.CommonUtil;
import com.rsmurniteguh.webservice.dep.kthis.model.Patient;
import com.rsmurniteguh.webservice.dep.kthis.model.Patientaccount;
import com.sun.org.apache.bcel.internal.generic.ICONST;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Commons;

import oracle.net.aso.a;



public class CheckDataPasienSer extends DbConnection{

	public static List<DataPasien> getCheckDataPasien(String ipmrn) {
		 List<DataPasien> all= new ArrayList<DataPasien>();
		 Connection connection=DbConnection.getPooledConnection();
		 ResultSet haspas=null;
		 if(connection == null)return null;
		 PreparedStatement stmt=null;
		 String abpas="select distinct cd.CARD_NO, ps.PERSON_NAME, ps.MOBILE_PHONE_NO, PGCOMMON.FXGETCODEDESC(ps.SEX) as sex, "
		 		+ "PGCOMMON.FXGETCODEDESC(pt.PATIENT_CLASS) as patient_class, ps.BIRTH_DATE, vs.VISIT_ID, vs.ADMISSION_DATETIME, "
		 		+ "PGCOMMON.FXGETCODEDESC(vs.PATIENT_TYPE) as patient_type, (select person_name from person where person_id in (case  vs.patient_type when 'PTY1' then "
		 		+ "(select cp.person_id from careprovider cp inner join visitdoctor vd on vd.careprovider_id = cp.careprovider_id where "
		 		+ "vd.visit_id = vs.visit_id  and vd.doctor_type = 'DTYDR' and vd.defunct_ind = 'N' and rownum = 1) when 'PTY2' then "
		 		+ "(select cp.person_id from careprovider cp inner join visitregntype vd on vd.careprovider_id = cp.careprovider_id "
		 		+ "where vd.visit_id = vs.visit_id and vd.defunct_ind = 'N' and rownum = 1) else null end)) as Dok, BM.BED_NO "
		 		+ "from CARD cd, PERSON ps, PATIENT pt, VISIT vs "
		 		+ "left outer join BEDHISTORY BH on BH.VISIT_ID=vs.VISIT_ID "
		 		+ "left outer join BEDMSTR BM on BM.BEDMSTR_ID=BH.BEDMSTR_ID where cd.CARD_NO=? and ps.PERSON_ID=cd.PERSON_ID "
		 		+ "and pt.PERSON_ID=ps.PERSON_ID and vs.PATIENT_ID=pt.PATIENT_ID order by vs.VISIT_ID desc";
		 try 
		 	{
			 	
			 	stmt = connection.prepareStatement(abpas);
			 	stmt.setEscapeProcessing(true);
			 	stmt.setQueryTimeout(60000);
			 	stmt.setString(1, ipmrn);
			 	haspas=stmt.executeQuery();
			 	
			 	if(haspas.next()){
			 		DataPasien dp=new DataPasien();
				 	dp.setCARD_NO(haspas.getString("CARD_NO"));			 	
				 	dp.setPERSON_NAME(haspas.getString("PERSON_NAME"));
				 	dp.setSex(haspas.getString("sex"));
				 	dp.setPatient_class(haspas.getString("patient_class"));
				 	dp.setBIRTH_DATE(haspas.getString("BIRTH_DATE"));
				 	dp.setVISIT_ID(haspas.getString("VISIT_ID"));
				 	dp.setADMISSION_DATETIME(haspas.getString("ADMISSION_DATETIME"));
				 	dp.setPatient_type(haspas.getString("patient_type"));
				 	dp.setDok(haspas.getString("Dok"));
				 	dp.setBED_NO(haspas.getString("BED_NO"));
				 	dp.setMOBILE_PHONE_NO(haspas.getString("MOBILE_PHONE_NO"));
				 	all.add(dp);
			 	}
		 	} 
		 catch (SQLException e) 
		 	{
			 	System.out.println(e.getMessage());
		 	}
		 finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		 return all;
	}

	
	
	public static List<DataPasien> getCheckDataPasienByBpjs(String bpjs) {
		 List<DataPasien> all= new ArrayList<DataPasien>();
		 Connection connection=DbConnection.getPooledConnection();
		 ResultSet haspas=null;
		 if(connection == null)return null;
		 Statement stmt=null;
		 String abpas="SELECT C.CARD_NO, P.NIK, P.EMAIL, P.MOBILE_PHONE_NO,P.RESIDENT_TYPE,P.BIRTH_DATE,P.PERSON_NAME "
				+ " ,P.RELIGION,P.ETHNIC_GROUP,P.RACE,P.NATIONALITY,P.ID_TYPE,P.ID_NO,P.SEX "
				+ " FROM CARD C, PERSON P, PATIENT PT  "
				+ " WHERE C.PERSON_ID=P.PERSON_ID AND PT.PERSON_ID = P.PERSON_ID "
		 		+ "AND C.PERSON_ID = PT.PERSON_ID AND PT.BPJS_NO = '"+bpjs+"'";
		 
		 try 
		 	{
			 	stmt=connection.createStatement();
			 	haspas=stmt.executeQuery(abpas);
			 	haspas.next();
			 	DataPasien dp=new DataPasien();
			 	dp.setSuccess(true);
			 	dp.setCARD_NO(haspas.getString("CARD_NO"));
			 	dp.setPERSON_NAME(haspas.getString("PERSON_NAME"));
			 	dp.setSex(haspas.getString("SEX"));
			 	dp.setBIRTH_DATE(haspas.getString("BIRTH_DATE"));
			 	dp.setMOBILE_PHONE_NO(haspas.getString("MOBILE_PHONE_NO"));
			 	dp.setReligion(haspas.getString("RELIGION"));
			 	dp.setEthnic(haspas.getString("ETHNIC_GROUP"));
			 	dp.setIdType(haspas.getString("ID_TYPE"));
			 	dp.setIdNo(haspas.getString("ID_NO"));
			 	dp.setEmail(haspas.getString("EMAIL"));
			 	dp.setNationality(haspas.getString("NATIONALITY"));
			 	
			 	all.add(dp);
			 	haspas.close();
			 	stmt.close();
		 	} 
		 catch (SQLException e) 
		 	{
			 	System.out.println(e.getMessage());
		 	}
		 finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		 return all;
	}
	
	
	public static List<visitdiagnosisdetail> getVisitDiagnosisDetailId(String visitdiagnosadetailid) {
		List<visitdiagnosisdetail> all= new ArrayList<visitdiagnosisdetail>();
		 Connection connection=DbConnection.getPooledConnection();
		 ResultSet haspas=null;
		 if(connection == null)return null;
		 Statement  opstmt = null;
		 String abpas="select v.VISIT_ID, c.CARD_NO , ps.PERSON_NAME, cm.CODE_DESC , (select person_name from IHIS.PERSON where person_id = cp.PERSON_ID ) as person, "
		 		+ "ps.SEX, ps.BIRTH_DATE, "
		 		+ "ssm.SUBSPECIALTY_DESC, dm.DIAGNOSIS_DESC from IHIS.VISITDIAGNOSISDETAIL vdd inner join IHIS.VISITDIAGNOSIS vd on vd.VISITDIAGNOSIS_ID = vdd.VISITDIAGNOSIS_ID "
		 		+ "inner join IHIS.VISIT v on v.VISIT_ID = vd.VISIT_ID inner join IHIS.PATIENT pt on pt.PATIENT_ID = v.PATIENT_ID "
		 		+ "inner join IHIS.PERSON ps on ps.PERSON_ID = pt.PERSON_ID inner join IHIS.CAREPROVIDER cp on cp.CAREPROVIDER_ID = vd.DIAGNOSED_BY "
		 		+ "inner join IHIS.CARD c on c.PERSON_ID = ps.PERSON_ID inner join IHIS.SUBSPECIALTYMSTR ssm on ssm.SUBSPECIALTYMSTR_ID = vd.SUBSPECIALTYMSTR_ID "
		 		+ "inner join IHIS.CODEMSTR cm on cm.CODE_CAT = substr(vd.DIAGNOSIS_TYPE, 0, 3) and cm.CODE_ABBR =  substr(vd.DIAGNOSIS_TYPE, 4, 6) "
		 		+ "left outer join IHIS.DIAGNOSISMSTR dm on dm.DIAGNOSISMSTR_ID = vdd.VISIT_DIAGNOSISMSTR_ID where vdd.VISITDIAGNOSISDETAIL_ID = '"+visitdiagnosadetailid+"'";
		 try 
		 	{	
			 	
			 	opstmt=connection.createStatement();
			 	haspas=opstmt.executeQuery(abpas);
			 	haspas.next();
			 	visitdiagnosisdetail dp=new visitdiagnosisdetail();
			 	dp.setVisitId(haspas.getBigDecimal("VISIT_ID"));
			 	dp.setCardNo(haspas.getBigDecimal("CARD_NO"));
			 	dp.setPersonName(haspas.getString("PERSON_NAME"));
			 	dp.setCodeDesc(haspas.getString("CODE_DESC"));
			 	dp.setPerson(haspas.getString("person"));
			 	dp.setSex(haspas.getString("SEX"));
			 	dp.setBirthdate(haspas.getDate("BIRTH_DATE"));
			 	dp.setSubspecialitydesc(haspas.getString("SUBSPECIALTY_DESC"));
			 	dp.setDiagnosisDesc(haspas.getString("DIAGNOSIS_DESC"));
			 	all.add(dp);
			 	haspas.close();
			 	opstmt.close();
		 	} 
		 catch (Exception e) 
		 	{
			 	System.out.println(e.getMessage());
		 	}
		 
		 finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (opstmt!=null) try  { opstmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		 
		 return all;
	}

	public static List<InfoVisitPasien> getInfoVisitPasien(String VisitId) {
				List<InfoVisitPasien> all= new ArrayList<InfoVisitPasien>();
				 Connection connection=DbConnection.getPooledConnection();
				 ResultSet haspas=null;
				 if(connection == null)return null;		
				 Statement opstmt = null;
				 String abpas="select distinct V.VISIT_ID, V.ADMISSION_DATETIME,  PS.PERSON_NAME, PS.SEX, "
				 		+ "PS.BIRTH_DATE,  (SELECT WS.WARD_DESC FROM WARDMSTR WS WHERE WS.WARDMSTR_ID=V.WARDMSTR_ID) WARD_DESC, "
				 		+ "(select person_name from person where person_id in ("
				 		+ " case  v.patient_type "
				 		+ " when 'PTY1' then"
				 		+ " (select cp.person_id from careprovider cp inner join visitdoctor vd on vd.careprovider_id = cp.careprovider_id "
				 		+ " where vd.visit_id = v.visit_id "
				 		+ " and vd.doctor_type = 'DTYDR' and vd.defunct_ind = 'N' and rownum = 1) "
				 		+ " when 'PTY2' then "
				 		+ " (select cp.person_id from careprovider cp inner join visitregntype vd on vd.careprovider_id = cp.careprovider_id"
				 		+ " where vd.visit_id = v.visit_id "
				 		+ " and vd.defunct_ind = 'N' and rownum = 1) "
				 		+ " else  "
				 		+ " null "
				 		+ " end      "
				 		+ " ))as NAMA_DOKTER, "
				 		+ " (select address_1 from address ad where ad.stakeholder_id = ps.stakeholder_id and ad.address_type = 'ADRRES' and rownum = 1) as alamat,"
				 		+ " pgcommon.fxGetCodeDesc(ps.race) as agama, "
				 		+ " V.PATIENT_TYPE as Tipe, PS.MARITAL_STATUS, PS.NATIONALITY, PS.MOBILE_PHONE_NO, CD.CARD_NO   "
				 		+ " from PATIENT P, PERSON PS,  VISIT V, CARD CD "
				 		+ " where P.PATIENT_ID=V.PATIENT_ID  and PS.PERSON_ID=P.PERSON_ID  AND CD.PERSON_ID=PS.PERSON_ID "
				 		+ " and V.VISIT_ID='"+VisitId+"'";
				 try 
				 	{	
					 	opstmt = connection.createStatement();
					 	//opstmt.setBigDecimalAtName("visit", new BigDecimal(VisitId));
					 	haspas=opstmt.executeQuery(abpas);
					 	while(haspas.next())
					 	{
						 	InfoVisitPasien dp=new InfoVisitPasien();
						 	dp.setAlamat(haspas.getString("ALAMAT") == null? "" : haspas.getString("ALAMAT"));
						 	dp.setAgama(haspas.getString("AGAMA") == null? "" : haspas.getString("AGAMA"));						 	
						 	dp.setVisitid(haspas.getBigDecimal("VISIT_ID"));
						 	dp.setVisitdiagnosadetail(new BigDecimal(0));
						 	dp.setPerson_name(haspas.getString("PERSON_NAME"));
						 	dp.setWardDesc(haspas.getString("WARD_DESC"));
						 	dp.setSex(haspas.getString("SEX"));
						 	dp.setBirth_date(haspas.getDate("BIRTH_DATE"));
						 	dp.setNama_dokter(haspas.getString("NAMA_DOKTER") == null ? "": haspas.getString("NAMA_DOKTER"));
						 	dp.setTipe_pasien(haspas.getString("Tipe"));
						 	dp.setMaritalstatus(haspas.getString("MARITAL_STATUS"));
						 	dp.setNationality(haspas.getString("NATIONALITY"));
						 	dp.setMobilephone(haspas.getString("MOBILE_PHONE_NO"));
						 	dp.setCardno(haspas.getString("CARD_NO"));
						 	dp.setAdmissionDate(haspas.getString("ADMISSION_DATETIME"));
						 	all.add(dp);
					 	}
					 	haspas.close();
					 	opstmt.close();
				 	} 
				 catch (Exception e) 
				 	{
					 	System.out.println(e.getMessage());
				 	}
				 finally {
				     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
				     if (opstmt!=null) try  { opstmt.close(); } catch (Exception ignore){}
				     if (connection!=null) try { connection.close();} catch (Exception ignore){}
				   }
				 return all;
	}

	public static List<InfoUserNameKthis> getInfoUserKthis(String userMstrId) {
		// TODO Auto-generated method stub
		List<InfoUserNameKthis> all= new ArrayList<InfoUserNameKthis>();
		 Connection connection=DbConnection.getPooledConnection();
		 ResultSet haspas=null;
		 if(connection == null)return null;	
		 Statement stmt=null;
		 String abpas="SELECT PS.PERSON_NAME, pgcommon.fxGetCodeDesc(careprovider_type) as posisi "
		 		+ "FROM USERPROFILE UP, PERSON PS, CAREPROVIDER CP "
		 		+ "WHERE UP.USERMSTR_ID='"+userMstrId+"' AND PS.PERSON_ID=UP.PERSON_ID AND CP.PERSON_ID=PS.PERSON_ID";
		 try 
		 	{
			 	stmt=connection.createStatement();
			 	haspas=stmt.executeQuery(abpas);
			 	haspas.next();
			 	InfoUserNameKthis dp=new InfoUserNameKthis();
			 	dp.setUsername(haspas.getString("PERSON_NAME"));
			 	dp.setPosisi(haspas.getString("posisi"));
			 	all.add(dp);
			 	haspas.close();
			 	stmt.close();
		 	} 
		 catch (SQLException e) 
		 	{
			 	System.out.println(e.getMessage());
		 	}
		 finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		 return all;
		 
	}



	public static List<Listcekdiagnos> getcekdiagnos(String ipmrn) {
		List<Listcekdiagnos> all= new ArrayList<Listcekdiagnos>();
		 Connection connection=DbConnection.getPooledConnection();
		 ResultSet haspas=null;
		 if(connection == null)return null;
		 Statement stmt=null;
		 String abpas="SELECT  (SELECT DIE.DIAGNOSIS_DESC FROM DIAGNOSISMSTR DIE WHERE DIE.DIAGNOSISMSTR_ID=VDD.VISIT_DIAGNOSISMSTR_ID) DIAG "
		 		+ "FROM CARD CD, PERSON PS, PATIENT PT, VISIT VS,  VISITDIAGNOSIS VDG, VISITDIAGNOSISDETAIL VDD, DIAGNOSISMSTR DM "
		 		+ "WHERE CD.CARD_NO='"+ipmrn+"' AND PS.PERSON_ID=CD.PERSON_ID AND PT.PERSON_ID=CD.PERSON_ID AND VS.PATIENT_TYPE='PTY1' "
		 		+ "AND VS.PATIENT_ID=PT.PATIENT_ID "
		 		+ "AND VDG.VISIT_ID=VS.VISIT_ID AND VDD.VISIT_DIAGNOSISMSTR_ID IS NOT NULL AND VDD.VISIT_DIAGNOSISMSTR_ID NOT IN (329556307,311258939) "
		 		+ "AND VDD.VISITDIAGNOSIS_ID=VDG.VISITDIAGNOSIS_ID AND DM.DIAGNOSISMSTR_ID=VDD.VISIT_DIAGNOSISMSTR_ID";
		 try 
		 	{
			 	stmt=connection.createStatement();
			 	haspas=stmt.executeQuery(abpas);
			 	haspas.next();
			 	Listcekdiagnos dp=new Listcekdiagnos();
			 	dp.setDiag(haspas.getString("DIAG"));
			 	all.add(dp);
			 	haspas.close();
			 	stmt.close();
		 	} 
		 catch (SQLException e) 
		 	{
			 	System.out.println(e.getMessage());
		 	}
		 finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		 return all;
	}

	public static RegisterOption getRegistrationOption(){
		RegisterOption result = new RegisterOption();
		result.setIdType(CommonService.getListDropDownBoByCodeCat(ICodeMstrConst.IIT));
		result.setReligion(CommonService.getListDropDownBoByCodeCat(ICodeMstrConst.RAC));
		result.setEthnic(CommonService.getListDropDownBoByCodeCat(ICodeMstrConst.ETC));
		result.setEducation(CommonService.getListDropDownBoByCodeCat(ICodeMstrConst.PLE));
		result.setProvince(getListDropDownStateMstr());
		return result;
	}
	
	private static List<DropDownBo> getListDropDownStateMstr() {
		List<DropDownBo> result = new ArrayList<DropDownBo>();
		try {
			String selectQuery = "SELECT SM.STATEMSTR_ID, SM.STATE_CODE, SM.SHORT_CODE, SM.STATE_DESC, SM.STATE_DESC_LANG1 FROM STATEMSTR SM "
					+ " WHERE SM.DEFUNCT_IND = 'N' "
					+ " ORDER BY SM.CODE_SEQ"; 
			List list = DbConnection.executeReader(DbConnection.KTHIS, selectQuery, new Object[] {});
			
			for (int a=0;a<list.size();a++){
				HashMap row = (HashMap)list.get(a);
				DropDownBo view = new DropDownBo();
				view.setPrimaryId((BigDecimal) row.get("STATEMSTR_ID"));
				view.setCode(row.get("STATE_CODE").toString());
				view.setNormalDesc(row.get("STATE_DESC").toString());
				result.add(view);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
		}
		return result;
	}

	public static List<DropDownBo> getListDropDownCityMstr(String stateMstrId){
		List<DropDownBo> result = new ArrayList<DropDownBo>();
		try {
			String selectQuery = "SELECT CM.CITYMSTR_ID, CM.CITY_CODE, CM.SHORT_CODE, CM.CITY_DESC, CM.CITY_DESC_LANG1 FROM CITYMSTR CM "
					+ " WHERE CM.DEFUNCT_IND = 'N' "
					+ " AND CM.STATEMSTR_ID = ? "
					+ " ORDER BY CM.CODE_SEQ "; 
			List list = DbConnection.executeReader(DbConnection.KTHIS, selectQuery, new Object[] {stateMstrId});
			
			for (int a=0;a<list.size();a++){
				HashMap row = (HashMap)list.get(a);
				DropDownBo view = new DropDownBo();
				view.setPrimaryId((BigDecimal) row.get("CITYMSTR_ID"));
				view.setCode(row.get("CITY_CODE").toString());
				view.setNormalDesc(row.get("CITY_DESC").toString());
				result.add(view);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
		}
		return result;
	}

	public static List<DataPasien> getListPasienByNameAndDate(String patientName, String birthDate){
		List<DataPasien> all= new ArrayList<DataPasien>();
		String query="select distinct cd.CARD_NO, ps.PERSON_NAME, ps.MOBILE_PHONE_NO"
				+ " , PGCOMMON.FXGETCODEDESC(ps.SEX) as SEX"
		 		+ " , PGCOMMON.FXGETCODEDESC(pt.PATIENT_CLASS) as PATIENT_CLASS"
		 		+ " , ps.BIRTH_DATE"
		 		+ " , PGCOMMON.FXGETCODEDESC(ps.RACE) as RACE"
		 		+ " , PGCOMMON.FXGETCODEDESC(ps.ETHNIC) as ETHNIC"
		 		+ " from CARD cd, PERSON ps, PATIENT pt "
		 		+ " where ps.PERSON_ID=cd.PERSON_ID "
		 		+ " and pt.PERSON_ID=ps.PERSON_ID ";
		List<Object> parameters = new ArrayList<Object>();
		
		if (!patientName.equals("")){
			query += " and lower(replace(ps.PERSON_NAME,' ','')) like lower(?) ";
			parameters.add("%" + patientName.replace(" ", "") + "%");
		}
		if (!birthDate.equals("")){
			query += " and ps.BIRTH_DATE = to_date(?, 'YYYYMMdd') ";
			parameters.add(birthDate);
		}
		try	{
			
			List list = DbConnection.executeReader(DbConnection.KTHIS, query, parameters.toArray());
			
			for (int a=0;a<list.size();a++){
				HashMap row = (HashMap)list.get(a);
				DataPasien dp=new DataPasien();
			 	dp.setCARD_NO(row.get("CARD_NO").toString());			 	
			 	dp.setPERSON_NAME(row.get("PERSON_NAME").toString());
			 	dp.setSex(row.get("SEX").toString());
			 	dp.setPatient_class(row.get("PATIENT_CLASS").toString());
			 	dp.setBIRTH_DATE(row.get("BIRTH_DATE").toString());
//			 	dp.setVISIT_ID(row.get("VISIT_ID").toString());
//			 	dp.setADMISSION_DATETIME(row.get("ADMISSION_DATETIME").toString());
//			 	dp.setPatient_type(row.get("patient_type").toString());
//			 	dp.setDok(row.get("Dok").toString());
//			 	dp.setBED_NO(row.get("BED_NO").toString());
			 	dp.setMOBILE_PHONE_NO(row.get("MOBILE_PHONE_NO").toString());
			 	dp.setReligion(row.get("RACE").toString());
			 	dp.setEthnic(row.get("ETHNIC").toString());
			 	all.add(dp);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			
		}
		return all;
	}
	
	public static List<DataPasien> getDataPasienByBpjs(String bpjs) {
		 List<DataPasien> all= new ArrayList<DataPasien>();
		 Connection connection=DbConnection.getPooledConnection();
		 ResultSet haspas=null;
		 if(connection == null)return null;
		 Statement stmt=null;
		 String abpas="SELECT C.CARD_NO, P.NIK, P.EMAIL, P.MOBILE_PHONE_NO,P.RESIDENT_TYPE,P.BIRTH_DATE,P.PERSON_NAME"
				+ " ,P.NATIONALITY, P.ID_TYPE, P.ID_NO"
				+ " ,PGCOMMON.FXGETCODEDESC(P.SEX) as SEX"
				+ " ,PGCOMMON.FXGETCODEDESC(P.RACE) as RACE"
		 		+ " ,PGCOMMON.FXGETCODEDESC(P.ETHNIC) as ETHNIC"
		 		+ " ,PGCOMMON.FXGETCODEDESC(PT.PATIENT_CLASS) as PATIENT_CLASS"
				+ " FROM CARD C, PERSON P, PATIENT PT  "
				+ " WHERE C.PERSON_ID=P.PERSON_ID AND PT.PERSON_ID = P.PERSON_ID"
		 		+ "AND C.PERSON_ID = PT.PERSON_ID AND PT.BPJS_NO = '"+bpjs+"'";
		 
		 try 
		 	{
			 	stmt=connection.createStatement();
			 	haspas=stmt.executeQuery(abpas);
			 	if(haspas.next()){
				 	DataPasien dp=new DataPasien();
				 	dp.setSuccess(true);
				 	dp.setCARD_NO(haspas.getString("CARD_NO"));
				 	dp.setPERSON_NAME(haspas.getString("PERSON_NAME"));
				 	dp.setSex(haspas.getString("SEX"));
				 	dp.setPatient_class(haspas.getString("PATIENT_CLASS").toString());			 	
				 	dp.setBIRTH_DATE(haspas.getString("BIRTH_DATE"));
				 	dp.setMOBILE_PHONE_NO(haspas.getString("MOBILE_PHONE_NO"));
				 	dp.setReligion(haspas.getString("RACE"));
				 	dp.setEthnic(haspas.getString("ETHNIC"));			 	
				 	all.add(dp);
			 	}
			 	haspas.close();
			 	stmt.close();
		 	} 
		 catch (SQLException e) 
		 	{
			 	System.out.println(e.getMessage());
		 	}
		 finally {
		     if (haspas!=null) try  { haspas.close(); } catch (Exception ignore){}
		     if (stmt!=null) try  { stmt.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		 return all;
	}

	public static BaseResult registerNewPatient(NewPatient newPatient){
		BaseResult result = new BaseResult();
		try {
			CardManageView cardManageView = buildCardManageView();
			PersonView personView = buildPersonView(newPatient, cardManageView);
			
			createPmiAndCardInfo(personView, cardManageView);
			cardManageView.getPersonId();
			
			CommonService cs = new CommonService();
			Timestamp requestDateTime = cs.getCurrentTime(); 
			MedicalRecordNoView MRNView = new MedicalRecordNoView();
			MRNView.setMedicalRecordNoId(cs.getPrimaryKey());
			MRNView.setMedicalRecordNo(IConstant.OP+cardManageView.getCardNo());
			MRNView.setMedicalRecordNoType(IConstant.MRT+IConstant.OP);
			MRNView.setPatientId(personView.getPatient().getPatientId());
			MRNView.setRemarks("");
			MRNView.setSystemInd(IConstant.IND_YES);
			MRNView.setDefunctInd(IConstant.IND_NO);
			MRNView.setLastUpdatedBy(IConstant.WS_USER);
			MRNView.setLastUpdatedDateTime(requestDateTime);
			
			addMedicalRecordNo(MRNView);
			
			result.setResult(cardManageView.getCardNo());
			result.setStatus(IConstant.STATUS_SUCCESSFULL);
		} catch (Exception e){
			result.setResult(IConstant.IND_NO);
			result.setErrorMsg(e.getMessage());
			result.setStatus(IConstant.STATUS_ERROR);
		}
		return result;
	}
	
	private static BaseResult addMedicalRecordNo(MedicalRecordNoView MRNView) throws Exception {
		BaseResult result = new BaseResult();
		Connection connection=DbConnection.getPooledConnection();
		if(connection == null)return null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		String sqlUser = "INSERT INTO MEDICALRECORDNO (MEDICALRECORDNO_ID,PATIENT_ID,MEDICAL_RECORD_NO_TYPE,MEDICAL_RECORD_NO,REMARKS,SYSTEM_IND,DEFUNCT_IND,PREV_UPDATED_BY,LAST_UPDATED_BY,PREV_UPDATED_DATETIME,LAST_UPDATED_DATETIME,ENTITYMSTR_ID) "
				+ " VALUES (?,?,?,?,?,?,?,NULL,?,NULL,?,1)";
		try {
			ps = connection.prepareStatement(sqlUser);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(60000);
			ps.setBigDecimal(1, MRNView.getMedicalRecordNoId());
			ps.setBigDecimal(2, MRNView.getPatientId());
			ps.setString(3, MRNView.getMedicalRecordNoType());
			ps.setString(4, MRNView.getMedicalRecordNo());
			ps.setString(5, MRNView.getRemarks());
			ps.setString(6, MRNView.getSystemInd());
			ps.setString(7, MRNView.getDefunctInd());
			ps.setBigDecimal(8, MRNView.getLastUpdatedBy());
			ps.setTimestamp(9, MRNView.getLastUpdatedDateTime());
			rs = ps.executeQuery();
			if (rs.next()) {		
				result.setResult(IConstant.IND_YES);
				result.setStatus(IConstant.STATUS_SUCCESSFULL);
			}
			ps.close();
		} catch (Exception e){
			result.setResult(IConstant.IND_NO);
			result.setErrorMsg(e.getMessage());
			result.setStatus(IConstant.STATUS_ERROR);
		}
		finally {
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return result;
	}
	
	private static CardManageView buildCardManageView(){
		CommonService cs = new CommonService();
		CardManageView view = new CardManageView();
		view.setCardNo(cs.getSequenceNo(ISequenceNoConst.CARD_NO, BigDecimal.ZERO, null));
		view.setCardType(ICodeMstrDetailConst.CDT_HMC);
		view.setCardStatus(ICodeMstrDetailConst.CDS_VLD);
//		if (this.page.getCollectCardDepositCB().getSelection()) {
//			view.setCardDepositeIncome(ISysConst.IND_YES);
//			view.setCardValue(page.getCardDeposit());
//		}
		view.setEffectiveDate( cs.getCurrentTime());
		return view;
	}
	
	
	private static PersonView buildPersonView(NewPatient newPatient, CardManageView card) throws Exception {
		try{
			PersonView personView = new PersonView();
			personView.getPerson().setEthnic(newPatient.getEthnic()); // this.page.getEthnicCbExt().getSelectedCode());
//			personView.getPatient().setLastCondition(this.page.getLastConditionCE().getSelectedCode());
			personView.getPatient().setTingkatPendidikan(newPatient.getLastEducation()); // this.page.getTingkatPendidikanComboExt().getSelectedCode());
			// 姓名
			personView.getPerson().setPersonName(newPatient.getPatientName()); // this.page.getNameT().getText().trim());
//			personView.getPerson().setShortCode(this.page.getNameShotT().getText().trim());
			// 证件号码
			personView.getPerson().setIdNo(newPatient.getIdentityNo());// this.page.getIdNoT().getText().trim());
			// 证件类型
//			if (this.page.getIdDNoBirthAgeGender().getIdTypeComboExt().getSelectedCode() != null) {
				personView.getPerson().setIdType(newPatient.getIdentityType()); // this.page.getIdDNoBirthAgeGender().getIdTypeComboExt().getSelectedCode());
//			}
			// 出生日期	
			Date dateBirth=new SimpleDateFormat(ISysConstant.FORMAT_RECEIVE_DATE).parse(newPatient.getBirthDate());  
			Timestamp birthDate = new Timestamp(dateBirth.getTime());
			personView.getPerson().setBirthDate(birthDate);	//StringUtil.getInstance().string2Timestamp2(this.page.getBirthCalendarComposite().getDateText().getText().trim()));
			// 年龄
			// 不需要填写年龄（年龄是通过计算得到）
			// 性别
			personView.getPerson().setSex(newPatient.getGender()); // this.page.getIdDNoBirthAgeGender().getGenderComboExt().getSelectedCode());
			// 患者身份
			personView.getPatient().setPatientClass(newPatient.getRegistrationType()); // page.getPatientClassComboExt().getSelectedCode());
			// 医保手册号
//			if (page.getMioNoT().isVisible() && page.getMioNoT().isEnabled()) {
//				personView.setMioNo(page.getMioNoT().getText());
//			}
			// 职业类型
//			personView.getPerson().setOccupationGroup(this.page.getJobTypeComboExt().getSelectedCode());
			// 移动电话
			personView.getPerson().setMobilePhoneNo(newPatient.getPhoneNo()); // this.page.getMobileT().getText().trim());
			// 电子邮件
			personView.getPerson().setEmail(newPatient.getEmail()); //this.page.getEmailT().getText().trim());
			personView.getPerson().setRace(newPatient.getReligion()); // this.page.getNationalComboExt().getSelectedCode());
//			personView.getPerson().setNationality(this.page.getCountryComboExt().getSelectedCode());
//			personView.getPerson().setMaritalStatus(this.page.getMarryComboExt().getSelectedCode());
//			personView.getPerson().setCompanyName(this.page.getWorkPlaceNameT().getText().trim());
			// Set Company Id
//			if(this.page.getCompanyView()!=null){
//				personView.getPerson().setCompanyId(this.page.getCompanyView().getCompanyId());
			personView.getPerson().setCompanyId(IConstant.COMPANY_SELFPAY);
//			}else{
//				personView.getPerson().setCompanyId(null);
//			}		
			// 工作单位及地址
//			personView.setOfficerAddress(this.page.getWorkPlaceAddrT().getText().trim());
			// 公司电话
//			personView.setOfficerTelephone(this.page.getWorkPlacePhoneT().getText().trim());
			// 邮政编码
//			personView.setOfficerPostalCode(this.page.getWorkPlaceCodeT().getText().trim());
			// 户口地址/现地址
//			personView.setRprState(newPatient.getProvince()); //this.page.getRprStateCityComboExt().getStateComboExt().getSelectedCode());
			// 市/区
//			personView.setRprCity(newPatient.getCity()); //this.page.getRprStateCityComboExt().getCityComboExt().getSelectedCode());
			// 邮政编码
//			personView.setRprPostalCode(this.page.getDoorCodeT().getText().trim());
			// 详细地点
			personView.setRprAddress1(newPatient.getAddress()); //this.page.getDoorTAddrT().getText().trim());
//			personView.getPerson().setNik(this.page.getWorkPlaceNikT().getText().trim());
//			if (page.getDoorSDistrictPS().getDropDownBo() != null) {
//				personView.setRprDistrictCode(page.getDoorSDistrictPS().getDropDownBo().getCode());
//			} else {
//				personView.setRprDistrictCode(null);
//			}
				// 现地址
			personView.setCurrentState(newPatient.getProvince()); // this.page.getCurrentStateCityComboExt().getStateComboExt().getSelectedCode());
			// 市/区
			personView.setCurrentCity(newPatient.getCity()); // this.page.getCurrentStateCityComboExt().getCityComboExt().getSelectedCode());
			// 邮政编码
//				personView.setCurrentPostalCode(this.page.getCurrentCodeT().getText().trim());
			// 详细地点
//			personView.setCurrentAddress1(newPatient.getAddress()); // this.page.getCurrentAddrT().getText().trim());

//			if (page.getCurrentDistrictPS().getDropDownBo() != null) {
//				personView.setCurrentDistrictCode(page.getCurrentDistrictPS().getDropDownBo().getCode());
//			} else {
//				personView.setCurrentDistrictCode(null);
//			}

			// 联系人姓名
//			personView.setContactPersonName(this.page.getContactNameT().getText().trim());
//			personView.setContactShortCode(PinYinUtil.getPinYinCode(this.page.getContactNameT().getText().trim()));
			// 与病人关系
//			personView.setRelationShipCode(this.page.getRelationComboExt().getSelectedCode());
			// 居住类别
//			personView.getPerson().setResidentType(this.page.getContactHomeTypeComboExt().getSelectedCode());
			// 地址
//			personView.setContactAddress(this.page.getContactAddrT().getText().trim());
			// 联系人邮件地址
//			personView.setContactEmail(this.page.getContactEmailT().getText().trim());
			// 电话
//			personView.setContactTelephone(this.page.getContactPhoneT().getText().trim());
			// 获知方式
//			if (page.getHealAboutPathC().getSelectionIndex() != 0) {
//				if (personView.getPatient() == null) {
//					personView.setPatient(new Patient());
//				}
//				personView.getPatient().setHearAboutPath(page.getHealAboutPathComboExt().getSelectedCode());
//			}
			
			personView.setMedicalRecordNo(card.getCardNo()); // page.getMedicalRecordNoT().getText().trim());
//			if (page.getCustomerClassC().isEnabled()) {
//				// 会员信息--
//				// 会员身份，会员生效日期，会员截止日期，会员组织机构
//				BigDecimal oldCustomerClassMstrId = personView.getCustomerClassMstrId();
//				// 代表更新
//				if (oldCustomerClassMstrId != null) {
//					BigDecimal newCustomerClassMstrId = page.getCustomerClassComboExt().getSelectedId();
//					// 不相等代表是新选择的
//					if (newCustomerClassMstrId != null && !oldCustomerClassMstrId.equals(newCustomerClassMstrId)) {
//						// 3)如果选择的会员身份不等于原会员身份，则需要在会员身份变更历史表中插入数据
//						CustomerClassHistory customerClassHistory = new CustomerClassHistory();
//						customerClassHistory.setOldCustomerClassMstrId(personView.getCustomerClassMstrId());
//						customerClassHistory.setOldEffectiveDate(personView.getCustomerEffectiveDate());
//						customerClassHistory.setOldExpiryDate(personView.getCustomerExpiryDate());
//						customerClassHistory.setNewCustomerClassMstrId(newCustomerClassMstrId);
//						customerClassHistory.setNewEffectiveDate(new Timestamp(System.currentTimeMillis()));
//						customerClassHistory.setNewExpiryDate(null);
//						personView.setCustomerClassHistory(customerClassHistory);
		//
//						personView.setCustomerClassMstrId(page.getCustomerClassComboExt().getSelectedId());
//						personView.setCustomerEffectiveDate(new Timestamp(System.currentTimeMillis()));
//						personView.setCustomerExpiryDate(null);
//					} else {
//						// 没有改变 会员身份
//						personView.setCustomerEffectiveDate(page.getCustomerStartDateCC().getDateTime());
//						personView.setCustomerExpiryDate(page.getCustomerEndDateCC().getDateTime());
//					}
//				} else {
//					// 代表添加，插入会员身份变更历史表
//					BigDecimal customerClassMstrId = page.getCustomerClassComboExt().getSelectedId();
//					if (customerClassMstrId != null) {
//						CustomerClassHistory customerClassHistory = new CustomerClassHistory();
//						customerClassHistory.setNewCustomerClassMstrId(customerClassMstrId);
//						customerClassHistory.setNewEffectiveDate(page.getCustomerStartDateCC().getDateTime());
//						customerClassHistory.setNewExpiryDate(page.getCustomerEndDateCC().getDateTime());
//						personView.setCustomerClassHistory(customerClassHistory);
//					}
//					personView.setCustomerClassMstrId(page.getCustomerClassComboExt().getSelectedId());
//					personView.setCustomerEffectiveDate(page.getCustomerStartDateCC().getDateTime());
//					personView.setCustomerExpiryDate(page.getCustomerEndDateCC().getDateTime());
//				}
//			}
//			personView.setOrganisationId(page.getOrganisationComboExt().getSelectedId());
				
			personView.getPatient().setBpjsNo(newPatient.getNoBPJS()); // this.page.getBpjsNoText().getText().trim());
			return personView;
		} catch (Exception e){
			throw e;
		}		
	}
	
	private static void createAddress(PersonView personView, BigDecimal stakeHolderId) throws SQLException, Exception {
		CommonService commonService = new CommonService();
//		BigDecimal birthAddressId = personView.getBirthAddressId();
//		String birthState = personView.getBirthState();
//		String birthCity = personView.getBirthCity();
//		String birthAddress1 = personView.getBirthAddress1();
//		if (birthAddressId == null
//				&& (CommonUtil.getInstance().isNotEmpty(birthState) || CommonUtil.getInstance().isNotEmpty(birthCity) || CommonUtil.getInstance()
//						.isNotEmpty(birthAddress1))) {
//			String addressType = ICodeMstrDetailConst.ADR_NPA;
//			Address address = new Address();
//			BigDecimal addressId = commonService.getPrimaryKey();
//			address.setAddressId(addressId);
//			address.setStakeholderId(stakeHolderId);
//			address.setAddressType(addressType);
//			address.setState(birthState);
//			address.setCity(birthCity);
//			address.setAddress1(birthAddress1);
//			createAddress(address);
//			personView.setBirthAddressId(addressId);
//		}

//		BigDecimal officerAddressId = personView.getOfficerAddressId();
//		String officerAddress = personView.getOfficerAddress();
//		String officerTelephone = personView.getOfficerTelephone();
//		String officerPostalCode = personView.getOfficerPostalCode();
//		if (officerAddressId == null
//				&& (CommonUtil.getInstance().isNotEmpty(officerAddress) || CommonUtil.getInstance().isNotEmpty(officerTelephone) || CommonUtil
//						.getInstance().isNotEmpty(officerPostalCode))) {
//			String addressType = ICodeMstrDetailConst.ADR_OFF;
//			Address address = new Address();
//			BigDecimal addressId = commonService.getPrimaryKey();
//			address.setAddressId(addressId);
//			address.setStakeHolderId(stakeHolderId);
//			address.setAddressType(addressType);
//			address.setPostalCode(officerPostalCode);
//			address.setPhoneNo(officerTelephone);
//			address.setAddress1(officerAddress);
//			createAddress(address);
//			personView.setOfficerAddressId(addressId);
//		}

		BigDecimal rprAddressId = personView.getRprAddressId();
		String rprState = personView.getRprState();
		String rprCity = personView.getRprCity();
		String rprPostalCode = personView.getRprPostalCode();
		String rprAddress1 = personView.getRprAddress1();
		if (rprAddressId == null
				&& (CommonUtil.getInstance().isNotEmpty(rprState) || CommonUtil.getInstance().isNotEmpty(rprCity)
						|| CommonUtil.getInstance().isNotEmpty(rprPostalCode) || CommonUtil.getInstance().isNotEmpty(rprAddress1))) {
			String addressType = ICodeMstrDetailConst.ADR_RES;
			Address address = new Address();
			BigDecimal addressId = commonService.getPrimaryKey();
			address.setAddressId(addressId);
			address.setStakeholderId(stakeHolderId);
			address.setAddressType(addressType);
//			address.setState(rprState);
//			address.setCity(rprCity);
			address.setDistrict(personView.getRprDistrictCode());
			address.setPostalCode(rprPostalCode);
			address.setAddress1(rprAddress1);
			createAddress(address);
			personView.setRprAddressId(addressId);
		}

		BigDecimal currentAddressId = personView.getCurrentAddressId();
		String currentState = personView.getCurrentState();
		String currentCity = personView.getCurrentCity();
		String currentPostalCode = personView.getCurrentPostalCode();
		String currentAddress1 = personView.getCurrentAddress1();
		if (currentAddressId == null
				&& (CommonUtil.getInstance().isNotEmpty(currentState) || CommonUtil.getInstance().isNotEmpty(currentCity)
						|| CommonUtil.getInstance().isNotEmpty(currentPostalCode) || CommonUtil.getInstance().isNotEmpty(currentAddress1))) {
			String addressType = ICodeMstrDetailConst.ADR_PRE;
			Address address = new Address();
			BigDecimal addressId = commonService.getPrimaryKey();
			address.setAddressId(addressId);
			address.setStakeholderId(stakeHolderId);
			address.setAddressType(addressType);
			address.setState(currentState);
			address.setCity(currentCity);
			address.setDistrict(personView.getCurrentDistrictCode());
			address.setPostalCode(currentPostalCode);
//			address.setAddress1(currentAddress1);
			createAddress(address);
			personView.setCurrentAddressId(addressId);
		}

//		BigDecimal contactAddressId = personView.getContactAddressId();
//		String contactPersonName = personView.getContactPersonName();
//		String relationShipCode = personView.getRelationShipCode();// 与病人关系,CAT=RRL
//		String contactAddress = personView.getContactAddress();
//		String contantEmail = personView.getContactEmail();
//		String contantShortCode = personView.getContactShortCode();
//		String contactTelephone = personView.getContactTelephone();
//		if (contactAddressId == null && CommonUtil.getInstance().isNotEmpty(contactPersonName)) {
//			StakeHolder stakeHolder = createStakeHolder();
//			Person contactPerson = new Person();
//			contactPerson.setPersonName(contactPersonName);
//			contactPerson.setEmail(contantEmail);
//			contactPerson.setShortCode(contantShortCode);
//			contactPerson.setMobilePhoneNo(contactTelephone);
//			Person person = createPerson(null, stakeHolder.getStakeHolderId(), contactPerson);
//			if (CommonUtil.getInstance().isNotEmpty(contactAddress) || CommonUtil.getInstance().isNotEmpty(contactTelephone)) {
//				String addressType = ICodeMstrDetailConst.ADR_RES;
//				Address address = new Address();
//				BigDecimal addressId = commonService.getPrimaryKey();
//				address.setAddressId(addressId);
//				address.setStakeholderId(stakeHolder.getStakeHolderId());
//				address.setAddressType(addressType);
//				address.setAddress1(contactAddress);
//				address.setPhoneNo(contactTelephone);
//				createAddress(address);
//				personView.setContactAddressId(addressId);
//			}
//			BigDecimal nokId = personView.getNokId();
//			if (nokId == null && CommonUtil.getInstance().isNotEmpty(relationShipCode)) {
//				Nok nok = new Nok();
//				BigDecimal myNokId = commonService.getPrimaryKey();
//				nok.setNokId(myNokId);
//				nok.setNokPersonId(personView.getPerson().getPersonId());
//				nok.setPersonId(person.getPersonId());
//				nok.setEmergencyContactInd(ISysConst.IND_YES);
//				nok.setRelationship(relationShipCode);
//				createNok(nok);
//				personView.setNokId(myNokId);
//			}
//		}
	}
	
	private static void createAddress(Address address) throws SQLException, Exception {
//		Session session = SessionFactory.getInstance().getCurrentSession();
		BigDecimal appUserId = IConstant.WS_USER; // session.getAppUserId();
		Timestamp requestDateTime = new CommonService().getCurrentTime(); // session.getRequestTimestamp();
		address.setLastUpdatedBy(appUserId);
		address.setLastUpdatedDatetime(requestDateTime);
		try {
			String InsertQuery = "INSERT INTO ADDRESS ( "
					+ " ADDRESS_ID, STAKEHOLDER_ID, ADDRESS_TYPE, CORRESPONDENCE_IND, COUNTRY "
					+ " , CITY, STATE, DISTRICT, POSTAL_CODE, PHONE_NO "
					+ " , FAX_NO, ADDRESS_1, ADDRESS_2, ADDRESS_3, DEFUNCT_IND "
					+ " , PREV_UPDATED_BY, PREV_UPDATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME) "
					+ " VALUES (?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?)";
			Object[] parameters = {
					address.getAddressId(), address.getStakeholderId(), address.getAddressType(), address.getCorrespondenceInd() == null ? IConstant.IND_NO : address.getCorrespondenceInd(), address.getCountry()
					, address.getCity(), address.getState(), address.getDistrict(), address.getPostalCode(), address.getPhoneNo()
					, address.getFaxNo(), address.getAddress1(), address.getAddress2(), address.getAddress3(), IConstant.IND_NO
					, address.getPrevUpdatedBy(), address.getPrevUpdatedDatetime(), address.getLastUpdatedBy(), address.getLastUpdatedDatetime()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, InsertQuery, parameters);
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
			throw e;
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}
		finally {
		}
	}

	private static PersonView createPmiAndCardInfo(PersonView personView, CardManageView cardManageView) throws Exception {
		if (personView == null) {
			throw new NullPointerException();
		}
		// 处理PMI信息
//		if (personView.getPerson().getPersonId() == null) {
			StakeHolder stakeHolder = createStakeHolder();
			createPerson(personView, stakeHolder.getStakeHolderId(),null);
			personView.setStakeHolder(stakeHolder);
//			
//		} else {
//			updatePerson(personView.getPerson());
//			updateCorrespondAddress(personView);
//		}
		createAddress(personView, personView.getStakeHolder().getStakeHolderId());
		createPatient(personView);
//		if(personView.getCustomerClassHistory() != null){
//			createCustomerClassHistory(personView);
//		}
		createPatientAccountForPatient(personView.getPatient().getPatientNo(), personView.getStakeHolder().getStakeHolderId());
		
//		modifyMedicalRecordNo(personView);
		// 添加医保手册号
//		if (personView.getMioNo() != null && !"".equals(personView.getMioNo())) { //$NON-NLS-1$
//			createMioNo(personView.getPatient().getPatientId(), personView.getMioNo());
//		}
	
		// 创建卡
		if (cardManageView != null) {
			creatCardInfo(cardManageView, personView);
		}

//		doPersonViewFlag(personView);

		return personView;
	}

	private static StakeHolder createStakeHolder() throws SQLException, Exception {
//		Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
		BigDecimal appUserId = IConstant.WS_USER; // session.getAppUserId();
		Timestamp requestDateTime =  cs.getCurrentTime();

		BigDecimal stakeHolderId = cs.getPrimaryKey();
		StakeHolder stakeHolder = new StakeHolder();
		stakeHolder.setStakeHolderId(stakeHolderId);
		stakeHolder.setStakeHolderType(ICodeMstrDetailConst.STK_1);// CAT=STK, 账户类型,'1'是个人，'2'是公司
		stakeHolder.setAccountStatus(ICodeMstrDetailConst.ACS_1);// CAT=ACS
		stakeHolder.setLastUpdatedBy(appUserId);
		stakeHolder.setLastUpdatedDateTime(requestDateTime);
		stakeHolder.setCreditRiskPromptInd(IConstant.IND_YES);
		stakeHolder.setNoteInclusionInd(IConstant.IND_NO);
		stakeHolder.setCnPeriodCoverageInd(IConstant.IND_NO);
		stakeHolder.setAccountBalance(BigDecimal.ZERO);
		stakeHolder.setBillSize(BigDecimal.ZERO);
		stakeHolder.setDepositAvailable(BigDecimal.ZERO);
		stakeHolder.setDepositBalance(BigDecimal.ZERO);

		try {
			String InsertQuery = "INSERT INTO STAKEHOLDER ( "
					+ " STAKEHOLDER_ID, STAKEHOLDER_TYPE, ACCOUNT_STATUS, DEPOSIT_BALANCE, DEPOSIT_AVAILABLE "
					+ ", BILL_SIZE, ACCOUNT_BALANCE, CREDIT_RISK_MARKED_DATE, CREDIT_RISK_PROMPT_IND, NOTE_INCLUSION_IND "
					+ ", CN_PERIOD_COVERAGE_IND, CREDIT_RISK_STATUS, CREDIT_TERM, BILLING_MODE, BILLING_FREQUENCY "
					+ ", IP_FINAL_BILL_FORMAT, IP_REVISED_BILL_FORMAT, OP_FINAL_BILL_FORMAT, OP_REVISED_BILL_FORMAT, REMARKS "
					+ ", ACCOUNT_POSTED_DATE, CREDIT_RISK_REMARKS, SHORT_CODE, PREV_UPDATED_BY, LAST_UPDATED_BY "
					+ ", PREV_UPDATED_DATETIME, LAST_UPDATED_DATETIME, OP_DEPOSIT_ACCOUNT_IND, PASSWORD, OVERDRAFT_IND "
					+ ", DEPOSIT_AMOUNT, INSURANCE_COVERAGE_AMOUNT) "
					+ " VALUES (?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?)";
			Object[] parameters = {
						stakeHolder.getStakeHolderId(), stakeHolder.getStakeHolderType(), stakeHolder.getAccountStatus(), stakeHolder.getDepositBalance(), stakeHolder.getDepositAvailable()
						, stakeHolder.getBillSize(), stakeHolder.getAccountBalance(), stakeHolder.getCreditRiskMarkedDate(), stakeHolder.getCreditRiskPromptInd(), stakeHolder.getNoteInclusionInd()
						, stakeHolder.getCnPeriodCoverageInd(), stakeHolder.getCreditRiskStatus(), stakeHolder.getCreditTerm(), stakeHolder.getBillingMode(), stakeHolder.getBillingFrequency()
						, stakeHolder.getIpFinalBillFormat(), stakeHolder.getIpRevisedBillFormat(), stakeHolder.getOpFinalBillFormat(), stakeHolder.getOpRevisedBillFormat(), stakeHolder.getRemarks()
						, stakeHolder.getAccountPostedDate(), stakeHolder.getCreditRiskRemarks(), stakeHolder.getShortCode(), stakeHolder.getPrevUpdatedBy(), stakeHolder.getLastUpdatedBy()
						, stakeHolder.getPrevUpdatedDateTime(), stakeHolder.getLastUpdatedDateTime(), IConstant.IND_NO, null, ""
						, 0, 0
			};
			DbConnection.executeQuery(DbConnection.KTHIS, InsertQuery, parameters);
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
			throw e;
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}
		finally {
		}
		return stakeHolder;
	}

	private static Person createPerson(PersonView personView, BigDecimal stakeHolderId,Person contactPerson) throws Exception {
		BigDecimal personId = new CommonService().getPrimaryKey();
		
		if (contactPerson == null) {
			Person person = personView.getPerson();
			if (person != null) {
				person.setPersonId(personId);
				person.setStakeholderId(stakeHolderId);
				createPerson(person);
			}

			return person;
		} else {
			contactPerson.setStakeholderId(stakeHolderId);
			contactPerson.setPersonId(personId);
			createPerson(contactPerson);
			return contactPerson;
		}
	}
	
	private static void createPerson(Person person) {
//		Session session = SessionFactory.getInstance().getCurrentSession();
		BigDecimal appUserId = IConstant.WS_USER; // session.getAppUserId();
		Timestamp requestDateTime = new CommonService().getCurrentTime(); // session.getRequestTimestamp();
		person.setLastUpdatedBy(appUserId);
		person.setLastUpdatedDatetime(requestDateTime);
		
		try {
			String InsertQuery = "INSERT INTO PERSON ( "
					+ " PERSON_ID, STAKEHOLDER_ID, ALT_PERSON_ID, DECEASED_ID, PERSON_NAME "
					+ " , SEX, BIRTH_DATE, ID_TYPE, ID_NO, TITLE "
					+ " , MARITAL_STATUS, NATIONALITY, MOTHER_LANGUAGE, RACE, ETHNIC_GROUP "
					+ " , RELIGION, OCCUPATION_GROUP, BLOOD_GROUP, RESIDENT_TYPE, MOBILE_PHONE_NO "
					+ " , PAGER_NO, EMAIL, PREFERRED_NAME, ALIAS_NAME, DEFUNCT_IND "
					+ " , PREV_UPDATED_BY, PREV_UPDATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME, PSEUDO_AGE_IND "
					+ " , HOUSEHOLD_SIZE, INCOME_RANGE, NATIONAL_SERVICE, EMPLOYEE_NO, EMPLOYER "
					+ " , SHORT_CODE, FILEMSTR_ID, PINYIN_CODE, COMPANY_NAME, COMPANY_ID "
					+ " , ETHNIC, NIK) "
					+ " VALUES (?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?)";
			Object[] parameters = {
						person.getPersonId(), person.getStakeholderId(), person.getAltPersonId(), person.getDeceasedId(), person.getPersonName()
						, person.getSex(), person.getBirthDate(), person.getIdType(), person.getIdNo(), person.getTitle()
						, person.getMaritalStatus(), person.getNationality(), person.getMotherLanguage(), person.getRace(), person.getEthnicGroup()
						, person.getReligion(), person.getOccupationGroup(), person.getBloodGroup(), person.getResidentType(), person.getMobilePhoneNo()
						, person.getPagerNo(), person.getEmail(), person.getPreferredName(), person.getAliasName(), IConstant.IND_NO
						, person.getPrevUpdatedBy(), person.getPrevUpdatedDatetime(), person.getLastUpdatedBy(), person.getLastUpdatedDatetime(), person.getPseudoAgeInd()
						, person.getHouseholdSize(), person.getIncomeRange(), person.getNationalService(), person.getEmployeeNo(), person.getEmployer()
						, person.getShortCode(), person.getFilemstrId(), person.getPinyinCode(), person.getCompanyName(), person.getCompanyId()
						, person.getEthnic(), person.getNik()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, InsertQuery, parameters);
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		finally {
		}
		
//		crudDaoFactory.getCrudDao(Person.class).insert(person);
	}
	
	private static PersonView createPatient(PersonView personView) throws SQLException, Exception {
//		Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
		BigDecimal appUserId = IConstant.WS_USER; // session.getAppUserId();
		Timestamp requestDateTime = cs.getCurrentTime(); // session.getRequestTimestamp();
		String patientNo = cs.getSequenceNo(ISequenceNoConst.PATIENT_NO, appUserId, null);

		Patient patient = personView.getPatient();
		patient.setPatientId(cs.getPrimaryKey());
		patient.setPersonId(personView.getPerson().getPersonId());
		patient.setPatientNo(patientNo);
		patient.setLastUpdatedBy(appUserId);
		patient.setLastUpdatedDatetime(requestDateTime);
		patient.setPatientClass(personView.getPatient().getPatientClass());
		patient.setLastCondition(ICodeMstrDetailConst.PLC_1);
		patient.setTingkatPendidikan(personView.getPatient().getTingkatPendidikan());
		if (personView.getPatient() != null && personView.getPatient().getHearAboutPath() != null)
			patient.setHearAboutPath(personView.getPatient().getHearAboutPath());

		try {
			String InsertQuery = "INSERT INTO PATIENT ( "
					+ " PATIENT_ID, PERSON_ID, ALT_PATIENT_ID, PATIENT_NO, VISIT_TO_DATE "
					+ " , LAST_VISIT_NO, MC_TO_DATE, COST_TO_DATE, LAST_VISIT_DATE, ARCHIVAL_DATETIME "
					+ " , MR_FILE_LOCATION, PATIENT_CAT, REMARKS, SPECIAL_INSTRUCTIONS, QUEUE_NO "
					+ " , DEFUNCT_IND, PREV_UPDATED_BY, LAST_UPDATED_BY, PREV_UPDATED_DATETIME, LAST_UPDATED_DATETIME "
					+ " , HEAR_ABOUT_PATH, PATIENT_CLASS, CREATED_ENTITYMSTR_ID, MEDICAL_NO, CUSTOMERCLASSMSTR_ID "
					+ " , CUSTOMER_EFFECTIVE_DATE, CUSTOMER_EXPIRY_DATE, ORGANISATION_ID, FAMILYDOCTOR, HEALTHCAREDOCTOR "
					+ " , ASSISTANT, RECOMMEND_DATE, RECOMMEND_PERSON_ID, RECEIVEMESSAGE_IND, BPJS_NO "
					+ " , LAST_CONDITION, RADIOLOGY_POST_IND, TINGKAT_PENDIDIKAN) "
					+ " VALUES (?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?, ?)";
			Object[] parameters = {	
						patient.getPatientId(), patient.getPersonId(), patient.getAltPatientId(), patient.getPatientNo(), patient.getVisitToDate() == null ? 0 : patient.getVisitToDate()
						, patient.getLastVisitNo(), patient.getMcToDate(), patient.getCostToDate(), patient.getLastVisitDate(), patient.getArchivalDatetime()
						, patient.getMrFileLocation(), patient.getPatientCat(), patient.getRemarks(), patient.getSpecialInstructions(), patient.getQueueNo()
						, patient.getDefunctInd() == null ? IConstant.IND_NO : patient.getDefunctInd(), patient.getPrevUpdatedBy(), patient.getLastUpdatedBy(), patient.getPrevUpdatedDatetime(), patient.getLastUpdatedDatetime()
						, patient.getHearAboutPath(), patient.getPatientClass(), patient.getCreatedEntitymstrId() == null ? "1" : patient.getCreatedEntitymstrId(), patient.getMedicalNo(), patient.getCustomerclassmstrId()
						, patient.getCustomerEffectiveDate(), patient.getCustomerExpiryDate(), patient.getOrganisationId(), null, null
						, patient.getAssistant(), patient.getRecommendDate(), null, patient.getReceivemessageInd() == null ? "Y" : patient.getReceivemessageInd(), patient.getBpjsNo()
						, patient.getLastCondition(), null, patient.getTingkatPendidikan()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, InsertQuery, parameters);
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
			throw e;
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}
		finally {
		}
		return personView;
	}
	
	private static void creatCardInfo(CardManageView cardManageView, PersonView personView) {
//		if (cardManageView.getIsMedicalCard()) {
//			medicalRecordNoCommonService.confirmMedicalCardNo(personView.getPatient().getPatientId(), cardManageView.getCardNo());
//		} else {
//			patientInfoView patientInfoView = patientInfoService.getPatientInfoViewByPersonId(personView.getPerson().getPersonId());
			cardManageView.setPersonId(personView.getPerson().getPersonId());
			cardManageView.setStakeholderId(personView.getStakeHolder().getStakeHolderId());// patientInfoView.getStakeHolder().getStakeHolderId());
			addCard(cardManageView);
//		}
	}
	
	private static void addCard(CardManageView cardView){
//		isValidateAddCard(cardView);
		
		//build CardBo & insertCard
		Card card = buildCardBO(cardView);
		try {
			String InsertQuery = "INSERT INTO CARD ( "
					+ " CARD_ID, PREV_UPDATED_BY, LAST_UPDATED_BY, PREV_UPDATED_DATETIME, LAST_UPDATED_DATETIME "
					+ " , DEFUNCT_IND, CARD_TYPE, CARD_NO, REMARKS, EFFECTIVE_DATE "
					+ " , EXPIRY_DATE, PERSON_ID, CARD_STATUS, STAKEHOLDERACCOUNTTXN_ID, CARD_PASSWORD) "
					+ " VALUES (?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?)";
			Object[] parameters = {
					card.getCardId(), card.getPrevUpdatedBy(), card.getLastUpdatedBy(), card.getPrevUpdatedDatetime(), card.getLastUpdatedDatetime()
					, card.getDefunctInd(), card.getCardType(), card.getCardNo(), card.getRemarks(), card.getEffectiveDate()
					, card.getExpiryDate(), card.getPersonId(), card.getCardStatus(), card.getStakeholderaccounttxnId(), card.getCardPassword()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, InsertQuery, parameters);
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		finally {
		}
//		this.crudDaoFactory.getCrudDao(Card.class).insert(card);

		//openCardHistory
		openCardHistory(card,cardView);

		//return cardList
//		List<CardManageView> cardList = getCardViews(cardView);
//		return cardList;
	}
	
	private static Card buildCardBO(CardManageView cardView) {
//    	Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
	
		Card card = new Card();
		card.setLastUpdatedDatetime(cs.getCurrentTime()); // session.getRequestTimestamp());
		card.setEffectiveDate(cardView.getEffectiveDate());
		
		CardManageView txnCardView = new CardManageView();
		CardManageView cardFeeView = getCardFeeView();
		txnCardView.setStakeholderId(cardView.getStakeholderId());
		txnCardView.setLastUpdateTime(card.getLastUpdatedDatetime());
		txnCardView.setTxnCodeMstrId(cardFeeView.getTxnCodeMstrId());
		txnCardView.setCardValue(cardFeeView.getCardValue());
		
		BigDecimal /*Integ2BigDe*/ txnId = null;
		//如果收押金，则进行交易并取得交易id
		if(IConstant.IND_YES.equals(cardView.getCardDepositeIncome())){
			if(txnCardView.getCardValue() != null)
				txnCardView.setCardValue(txnCardView.getCardValue().negate());
			txnId = getTXNId(txnCardView);
		}
		
		//build CardBo
		
		card.setCardId(cs.getPrimaryKey());
		card.setPersonId(cardView.getPersonId());
		card.setCardNo(cardView.getCardNo());
		card.setCardPassword(cardView.getCardPassword());
		card.setCardStatus(cardView.getCardStatus());
		card.setCardType(cardView.getCardType());
		card.setDefunctInd(IConstant.IND_NO);
		card.setEffectiveDate(cardView.getEffectiveDate());
	//	card.setExpiryDate(cardView.getExpiryDate());
		card.setRemarks(cardView.getRemark());
		card.setLastUpdatedBy(IConstant.WS_USER); // session.getAppUserId());
		
		card.setStakeholderaccounttxnId(txnId);
		
		card.setRemarks(cardView.getRemark());
		return card;
	}
	
	private static BigDecimal getTXNId(CardManageView cardView) {
		BigDecimal txnId = creatCardChargeInfo(cardView.getStakeholderId()
				, cardView.getStakeHolderAccountTXNID(), cardView.getTxnCodeMstrId(),cardView.getCardValue(),cardView.getLastUpdateTime());
		return txnId;
	}
	
	private static BigDecimal creatCardChargeInfo(BigDecimal stakeholderId, BigDecimal stakeholderAccountTxnId, BigDecimal txnCodeMstrId, 
			BigDecimal txnAmount, Timestamp txnDate) {
//		Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
		txnDate = cs.getCurrentTime(); // session.getRequestTimestamp();
		
		Receipt receipt = null;
		TxnCodeMstr tcm = getTxnCodeMstr(txnCodeMstrId);
		if(tcm.getTxnCode().equals(IPACommonConst.PA_COLLECTION_CARDDEPOSIT)){
			txnAmount = txnAmount.abs();
			//产生收据
			receipt =creatReceipt(stakeholderId);
		} else {
			txnAmount = txnAmount.abs().negate();
			//更新收据
			BigDecimal receiptId = getCardDepositReceiptIdByShatId(stakeholderAccountTxnId);
			receipt = getReceiptInfo(receiptId);
			receipt.setCancelledBy(IConstant.WS_USER);
			receipt.setCancelledDatetime(cs.getCurrentTime());
			receipt.setReceiptStatus(ICodeMstrDetailConst.RCS_3);
			receipt = updateReceiptInfo(receipt);
		}
		Receipthistory receiptHistory = new Receipthistory();
		receiptHistory.setReceiptDatetime(receipt.getEnteredDatetime());
		receiptHistory.setReceiptId(receipt.getReceiptId());
		receiptHistory.setReceiptStatus(receipt.getReceiptStatus());
		receiptHistory = creatReceiptHistory(receiptHistory);
		//产生CounterCollection信息
		Countercollection counterCollection = new Countercollection();
		counterCollection.setReceiptId(receipt.getReceiptId());
		counterCollection.setCollectedAmount(txnAmount);
		counterCollection.setCollectionMode(ICodeMstrDetailConst.PYM_1);
		counterCollection.setTxnDatetime(txnDate);
		counterCollection.setCollectionType(ICodeMstrDetailConst.CTY_13);
		counterCollection.setReceiptHistoryId(receiptHistory.getReceipthistoryId());
		counterCollection.setTxncodemstrId(txnCodeMstrId);
		counterCollection.setCollectionStatus(ICodeMstrDetailConst.CCS_1);
		counterCollection = creatCounterCollectionInfo(counterCollection);
		//产生StakeholderAccountTxn信息
		StakeholderAccountTxn stakeholderAccountTxn = new StakeholderAccountTxn();
		stakeholderAccountTxn.setStakeholderId(stakeholderId);
		stakeholderAccountTxn.setTxnAmount(txnAmount);
		stakeholderAccountTxn.setEnteredDateTime(txnDate);
		stakeholderAccountTxn.setTxnCodeMstrId(txnCodeMstrId);			
		stakeholderAccountTxn.setTxnDesc(tcm.getTxnDesc());
		stakeholderAccountTxn = creatStakeholderAccountTxnInfo(stakeholderAccountTxn);
		//产生StakeholderCollection信息
		StakeholderCollection stakeholderCollection = new StakeholderCollection();
		stakeholderCollection.setCounterCollectionId(counterCollection.getCountercollectionId());
		stakeholderCollection.setStakeholderAccountTxnId(stakeholderAccountTxn.getStakeholderAccountTxnId());
		stakeholderCollection = creatStakeholderCollectionInfo(stakeholderCollection);			
		return stakeholderAccountTxn.getStakeholderAccountTxnId();
	}
	
	private static Receipt updateReceiptInfo(Receipt receipt) {
//		Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
		receipt.setPrevUpdatedBy(receipt.getLastUpdatedBy());
		receipt.setPrevUpdatedDatetime(receipt.getLastUpdatedDatetime());
		receipt.setLastUpdatedBy(IConstant.WS_USER);
		receipt.setLastUpdatedDatetime(cs.getCurrentTime());
		try {
			String query = "UPDATE RECEIPT "
					+ " SET PREV_UPDATED_BY = ?, LAST_UPDATED_BY = ?, PREV_UPDATED_DATETIME = ?, LAST_UPDATED_DATETIME = ?, RECEIPT_TYPE = ?"
					+ " , RECEIPT_STATUS = ?, USER_RECEIPT_NO = ?, SYSTEM_RECEIPT_NO = ?, CANCELLED_BY = ?, CANCELLED_DATETIME = ?"
					+ " , ENTERED_BY = ?, ENTERED_DATETIME = ?, PARENT_RECEIPT_ID = ?, RECEIPT_CAT = ?, STAKEHOLDER_ID = ?"
					+ " , PAYEE_NAME = ? WHERE RECEIPT_ID = ? ";
			Object[] parameters = {
					receipt.getPrevUpdatedBy(), receipt.getLastUpdatedBy(), receipt.getPrevUpdatedBy(), receipt.getLastUpdatedDatetime(), receipt.getReceiptType()
					, receipt.getReceiptStatus(), receipt.getUserReceiptNo(), receipt.getSystemReceiptNo(), receipt.getCancelledBy(), receipt.getCancelledDatetime()
					, receipt.getEnteredBy(), receipt.getEnteredDatetime(), null, receipt.getReceiptCat(), receipt.getStakeholderId()
					, receipt.getPayeeName(), receipt.getReceiptId()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, query, parameters);	
		} catch (SQLException e){
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
		}
		return receipt;// this.crudDaoFactory.getCrudDao(Receipt.class).get(receipt.getReceiptId());
	}
	
	private static Receipthistory creatReceiptHistory(Receipthistory receiptHistory){
//		Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
		receiptHistory.setLastUpdatedBy(IConstant.WS_USER);
		receiptHistory.setLastUpdatedDatetime(cs.getCurrentTime());
		receiptHistory.setDefunctInd(IConstant.IND_NO);
		receiptHistory.setCreatedBy(IConstant.WS_USER);
		receiptHistory.setCreatedDatetime(cs.getCurrentTime());
		receiptHistory.setUsermstrId(IConstant.WS_USER);
		BigDecimal receiptHistoryId = cs.getPrimaryKey();
		receiptHistory.setReceipthistoryId(receiptHistoryId);
		try {
			String query = "INSERT INTO RECEIPTHISTORY ("
					+ "RECEIPTHISTORY_ID, RECEIPT_ID, USERMSTR_ID, RECEIPT_STATUS, RECEIPT_DATETIME "
					+ ", DEFUNCT_IND, CREATED_BY, LAST_UPDATED_BY, CREATED_DATETIME, LAST_UPDATED_DATETIME) "
					+ " VALUES ( "
					+ " ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? )";
			Object[] parameters = {
					receiptHistory.getReceipthistoryId(), receiptHistory.getReceiptId(), receiptHistory.getUsermstrId(), receiptHistory.getReceiptStatus(), receiptHistory.getReceiptDatetime()
					, receiptHistory.getDefunctInd(), receiptHistory.getCreatedBy(), receiptHistory.getLastUpdatedBy(), receiptHistory.getCreatedDatetime(), receiptHistory.getLastUpdatedDatetime()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, query, parameters);	
		} catch (SQLException e){
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
		}
		return receiptHistory; // this.crudDaoFactory.getCrudDao(ReceiptHistory.class).get(receiptHistoryId);
	}
	
	private static Countercollection creatCounterCollectionInfo(Countercollection counterCollection) {
//		Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
		BigDecimal counterCollectionId = cs.getPrimaryKey();
		counterCollection.setCountercollectionId(counterCollectionId);
		counterCollection.setLastUpdatedBy(IConstant.WS_USER);
		counterCollection.setLastUpdatedDatetime(cs.getCurrentTime());
		counterCollection.setUsermstrId(IConstant.WS_USER);
		counterCollection.setLocationMstrId(IConstant.LOCATIONMSTR_OPCASHIER);
//		counterCollection.setCounterId(null);
		try {
			String query = "INSERT INTO COUNTERCOLLECTION ("
					+ "COUNTERCOLLECTION_ID, COUNTER_ID, RECEIPT_ID, PREV_UPDATED_BY, LAST_UPDATED_BY "
					+ " , CANCEL_COUNTERCOLLECTION_ID, EXCHANGE_RATE, FOREIGN_AMOUNT, COLLECTED_AMOUNT, CARD_EXPIRY_DATE "
					+ " , REFERENCE_DATE, PREV_UPDATED_DATETIME, LAST_UPDATED_DATETIME, COLLECTION_MODE, BANK_CODE "
					+ " , CURRENCY_CODE, REFERENCE_NO, AUTHORISATION_CODE, BANK_ACCOUNT_NO, LOCATIONMSTR_ID "
					+ " , USERMSTR_ID, TRANSFER_IND, TXN_DATETIME, COLLECTION_TYPE, RECEIPTHISTORY_ID "
					+ " , TXNCODEMSTR_ID, COLLECTION_STATUS) "
					+ " VALUES ( "
					+ " ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? "
					+ " , ?, ?)";
			Object[] parameters = {
					counterCollection.getCountercollectionId(), null, counterCollection.getReceiptId(), counterCollection.getPrevUpdatedBy(), counterCollection.getLastUpdatedBy()
					, null, counterCollection.getExchangeRate(), counterCollection.getForeignAmount(), counterCollection.getCollectedAmount(), counterCollection.getCardExpiryDate()
					, counterCollection.getReferenceDate(), counterCollection.getPrevUpdatedDatetime(), counterCollection.getLastUpdatedDatetime(), counterCollection.getCollectionMode(), counterCollection.getBankCode()
					, counterCollection.getCurrencyCode(), counterCollection.getReferenceNo(), counterCollection.getAuthorisationCode(), counterCollection.getBankAccountNo(), counterCollection.getLocationMstrId()
					, counterCollection.getUsermstrId(), counterCollection.getTransferInd(), counterCollection.getTxnDatetime(), counterCollection.getCollectionType(), counterCollection.getReceiptHistoryId()
					, counterCollection.getTxncodemstrId(), counterCollection.getCollectionStatus()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, query, parameters);	
		} catch (SQLException e){
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
		}
		return counterCollection;
	}
	
	private static StakeholderAccountTxn creatStakeholderAccountTxnInfo(StakeholderAccountTxn stakeholderAccountTxn) {
//		Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
		BigDecimal stakeholderAccountTxnId = cs.getPrimaryKey();
		String txnNo = cs.getSequenceNo(ISequenceNoConst.TXN_NO_PAT, IConstant.WS_USER, null);
		
		stakeholderAccountTxn.setStakeholderAccountTxnId(stakeholderAccountTxnId);
		stakeholderAccountTxn.setTxnNo(txnNo);
		stakeholderAccountTxn.setEnteredBy(IConstant.WS_USER);
		stakeholderAccountTxn.setEnteredDateTime(cs.getCurrentTime());
		stakeholderAccountTxn.setLastUpdatedBy(IConstant.WS_USER);
		stakeholderAccountTxn.setLastUpdatedDateTime(cs.getCurrentTime());
		
		try {
			String query = "INSERT INTO STAKEHOLDERACCOUNTTXN ("
					+ "STAKEHOLDERACCOUNTTXN_ID, STAKEHOLDER_ID, PARENT_SAT_ID, REVENUECENTREMSTR_ID, TXN_AMOUNT "
					+ ", TXN_DESC, TXN_NO, TXN_STATUS, TXNCODEMSTR_ID, CANCEL_FOR_TXN_NO "
					+ ", ENTERED_BY, ENTERED_DATETIME, REMARKS, PATIENTACCOUNTTXN_ID, GLACCOUNTMSTR_ID "
					+ ", LAST_UPDATED_BY, LAST_UPDATED_DATETIME, POSTED_DATETIME, PREV_UPDATED_BY, PREV_UPDATED_DATETIME "
					+ ", REFERENCE_NO) "
					+ " VALUES ( "
					+ " ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? "
					+ " , ?)";
			Object[] parameters = {
					stakeholderAccountTxn.getStakeholderAccountTxnId(), stakeholderAccountTxn.getStakeholderId(), stakeholderAccountTxn.getParentSatId(), stakeholderAccountTxn.getRevenueCentreMstrId(), stakeholderAccountTxn.getTxnAmount()
					, stakeholderAccountTxn.getTxnDesc(), stakeholderAccountTxn.getTxnNo(), stakeholderAccountTxn.getTxnStatus(), stakeholderAccountTxn.getTxnCodeMstrId(), stakeholderAccountTxn.getCancelForTxnNo()
					, stakeholderAccountTxn.getEnteredBy(), stakeholderAccountTxn.getEnteredDateTime(), stakeholderAccountTxn.getRemarks(), stakeholderAccountTxn.getPatientAccountTxnId(), stakeholderAccountTxn.getGlAccountMstrId()
					, stakeholderAccountTxn.getLastUpdatedBy(), stakeholderAccountTxn.getLastUpdatedDateTime(), stakeholderAccountTxn.getPostedDateTime(), stakeholderAccountTxn.getPrevUpdatedBy(), stakeholderAccountTxn.getPrevUpdatedDateTime()
					, stakeholderAccountTxn.getReferenceNo()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, query, parameters);	
		} catch (SQLException e){
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
		}
		return stakeholderAccountTxn;// getStakeholderAccountTxnInfo(stakeholderAccountTxnId);
	}
	
	private static StakeholderCollection creatStakeholderCollectionInfo(StakeholderCollection stakeholderCollection) {
//		Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
		BigDecimal stakeholderCollectionId = cs.getPrimaryKey();
		
		stakeholderCollection.setStakeholderCollectionId(stakeholderCollectionId);
		stakeholderCollection.setLastUpdatedBy(IConstant.WS_USER);
		stakeholderCollection.setLastUpdatedDateTime(cs.getCurrentTime());
		
		try {
			String query = "INSERT INTO STAKEHOLDERCOLLECTION ("
					+ "STAKEHOLDERCOLLECTION_ID, STAKEHOLDERACCOUNTTXN_ID, COUNTERCOLLECTION_ID, CANCEL_REASON, OTHER_CANCEL_REASON "
					+ " , ADDRESS_1, ADDRESS_2, ADDRESS_3, PAYEE_NAME, PREV_UPDATED_BY "
					+ " , PREV_UPDATED_DATETIME, LAST_UPDATED_BY, LAST_UPDATED_DATETIME) "
					+ " VALUES ( "
					+ " ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? "
					+ " , ?, ?, ?)";
			Object[] parameters = {
					stakeholderCollection.getStakeholderCollectionId(), stakeholderCollection.getStakeholderAccountTxnId(), stakeholderCollection.getCounterCollectionId(), stakeholderCollection.getCancelReason(), stakeholderCollection.getOtherCancelReason()
					, stakeholderCollection.getAddress1(), stakeholderCollection.getAddress2(), stakeholderCollection.getAddress3(), stakeholderCollection.getPayeeName(), stakeholderCollection.getPrevUpdatedBy()
					, stakeholderCollection.getPrevUpdatedDateTime(), stakeholderCollection.getLastUpdatedBy(), stakeholderCollection.getLastUpdatedDateTime()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, query, parameters);	
		} catch (SQLException e){
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
		}
		return stakeholderCollection; // getStakeholderCollectionInfo(stakeholderCollectionId);
	}
	
	private static Receipt getReceiptInfo(BigDecimal receiptId){
		Receipt result = new Receipt();
		String query = "SELECT RECEIPT_ID, PREV_UPDATED_BY, LAST_UPDATED_BY, PREV_UPDATED_DATETIME, LAST_UPDATED_DATETIME "
					+ " , RECEIPT_TYPE, RECEIPT_STATUS, USER_RECEIPT_NO, SYSTEM_RECEIPT_NO, CANCELLED_BY "
					+ " , CANCELLED_DATETIME, ENTERED_BY, ENTERED_DATETIME, PARENT_RECEIPT_ID, RECEIPT_CAT "
					+ " , STAKEHOLDER_ID, PAYEE_NAME FROM RECEIPT "
				+ " WHERE RECEIPT_ID = ? ";
		try {
			List list = DbConnection.executeReader(DbConnection.KTHIS, query, new Object[] {receiptId});
			if (list.size() > 0){
				HashMap row = (HashMap) list.get(0);
				result.setReceiptId(row.get("RECEIPT_ID").equals("") ? null : (BigDecimal) row.get("RECEIPT_ID"));
				result.setPrevUpdatedBy(row.get("PREV_UPDATED_BY").equals("") ? null : (BigDecimal) row.get("PREV_UPDATED_BY"));
				result.setLastUpdatedBy(row.get("LAST_UPDATED_BY").equals("") ? null : (BigDecimal) row.get("LAST_UPDATED_BY"));
				result.setPrevUpdatedDatetime(row.get("PREV_UPDATED_DATETIME").equals("") ? null : (Timestamp) row.get("PREV_UPDATED_DATETIME"));
				result.setLastUpdatedDatetime(row.get("LAST_UPDATED_DATETIME").equals("") ? null : (Timestamp) row.get("LAST_UPDATED_DATETIME"));
				result.setReceiptType(row.get("RECEIPT_TYPE").equals("") ? null : (String) row.get("RECEIPT_TYPE"));
				result.setReceiptStatus(row.get("RECEIPT_STATUS").equals("") ? null : (String) row.get("RECEIPT_STATUS"));
				result.setUserReceiptNo(row.get("USER_RECEIPT_NO").equals("") ? null : (String) row.get("USER_RECEIPT_NO"));
				result.setCancelledBy(row.get("SYSTEM_RECEIPT_NO").equals("") ? null : (BigDecimal) row.get("SYSTEM_RECEIPT_NO"));
				result.setCancelledDatetime(row.get("CANCELLED_DATETIME").equals("") ? null : (Timestamp) row.get("CANCELLED_DATETIME"));
				result.setEnteredBy(row.get("ENTERED_BY").equals("") ? null : (BigDecimal) row.get("ENTERED_BY"));
				result.setEnteredDatetime(row.get("ENTERED_DATETIME").equals("") ? null : (Timestamp) row.get("ENTERED_DATETIME"));
				result.setReceiptCat(row.get("RECEIPT_CAT").equals("") ? null : (String) row.get("RECEIPT_CAT"));
				result.setStakeholderId(row.get("STAKEHOLDER_ID").equals("") ? null : (BigDecimal) row.get("STAKEHOLDER_ID"));
				result.setPayeeName(row.get("PAYEE_NAME").equals("") ? null : (String) row.get("PAYEE_NAME"));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}
	
	private static BigDecimal getCardDepositReceiptIdByShatId(BigDecimal stakeholderAccountTxnId){
		BigDecimal result = BigDecimal.ZERO;
		String query = "SELECT  R.RECEIPT_ID "
				+ " FROM STAKEHOLDERACCOUNTTXN SHAT, STAKEHOLDERCOLLECTION SHC, COUNTERCOLLECTION CC, RECEIPTHISTORY RH, RECEIPT R "
				+ " WHERE SHAT.STAKEHOLDERACCOUNTTXN_ID = SHC.STAKEHOLDERACCOUNTTXN_ID "
				+ " AND   SHC.COUNTERCOLLECTION_ID = CC.COUNTERCOLLECTION_ID "
				+ "	AND   CC.RECEIPTHISTORY_ID = RH.RECEIPTHISTORY_ID "
				+ "	AND   RH.RECEIPT_ID = R.RECEIPT_ID "
				+ "	AND   SHAT.STAKEHOLDERACCOUNTTXN_ID = ?";
		try {
			result = (BigDecimal) DbConnection.executeScalar(DbConnection.KTHIS, query, new Object[] {stakeholderAccountTxnId});
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return result;
	}
	
	private static Receipt creatReceipt(BigDecimal stakeholderId) {
		//  creat Receipt set the receipt type to "1" and receipt status "open'
//		Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
		Receipt receipt = new Receipt();
		receipt.setReceiptType(ICodeMstrDetailConst.RTY_1);
		receipt.setReceiptStatus(ICodeMstrDetailConst.RCS_2);
		String userReceiptNo = cs.getSequenceNo(ISequenceNoConst.USR_RECEIPT_NO, IConstant.WS_USER, null);
		String sysReceiptNo = cs.getSequenceNo(ISequenceNoConst.SYS_RECEIPT_NO, IConstant.WS_USER, null);
		receipt.setUserReceiptNo(userReceiptNo);
		receipt.setSystemReceiptNo(sysReceiptNo);
		receipt.setReceiptCat(ICodeMstrDetailConst.RCC_CD);
		receipt.setStakeholderId(stakeholderId);
		receipt = creatReceiptInfo(receipt);
		return receipt;
	}
	
	private static Receipt creatReceiptInfo(Receipt receipt) {
		CommonService cs = new CommonService();
//		Session session = SessionFactory.getInstance().getCurrentSession();
//		checkReceiptUnique(receipt.getUserReceiptNo(), null);
		receipt.setLastUpdatedBy(IConstant.WS_USER);
		receipt.setLastUpdatedDatetime(cs.getCurrentTime());
		receipt.setEnteredBy(IConstant.WS_USER);
		receipt.setEnteredDatetime(cs.getCurrentTime());
		BigDecimal receipId = cs.getPrimaryKey();
		receipt.setReceiptId(receipId);
		
		try{
			String query = "INSERT INTO RECEIPT ("
					+ " RECEIPT_ID, PREV_UPDATED_BY, LAST_UPDATED_BY, PREV_UPDATED_DATETIME, LAST_UPDATED_DATETIME "
					+ " , RECEIPT_TYPE, RECEIPT_STATUS, USER_RECEIPT_NO, SYSTEM_RECEIPT_NO, CANCELLED_BY "
					+ " , CANCELLED_DATETIME, ENTERED_BY, ENTERED_DATETIME, PARENT_RECEIPT_ID, RECEIPT_CAT "
					+ " , STAKEHOLDER_ID, PAYEE_NAME )"
					+ " VALUES ("
					+ " ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? "
					+ " , ?, ?, ?, ?, ? "
					+ " , ?, ?)";
			Object[] parameters = {
					receipt.getReceiptId(), receipt.getPrevUpdatedBy(), receipt.getLastUpdatedBy(), receipt.getPrevUpdatedBy(), receipt.getLastUpdatedDatetime()
					, receipt.getReceiptType(), receipt.getReceiptStatus(), receipt.getUserReceiptNo(), receipt.getSystemReceiptNo(), receipt.getCancelledBy()
					, receipt.getCancelledDatetime(), receipt.getEnteredBy(), receipt.getEnteredDatetime(), null, receipt.getReceiptCat()
					, receipt.getStakeholderId(), receipt.getPayeeName()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, query, parameters);
		} catch (Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return receipt;
	}
	
	private static TxnCodeMstr getTxnCodeMstr(BigDecimal txnCodeMstrId){
		TxnCodeMstr result = new TxnCodeMstr();
		String query = "SELECT TXNCODEMSTR_ID, TXNTYPEMSTR_ID, PARENT_TXNCODEMSTR_ID, BILLINGSUMMARYMSTR_ID, PREV_UPDATED_BY "
				+ " , LAST_UPDATED_BY, GLACCOUNTMSTR_ID, PREV_UPDATED_DATETIME, LAST_UPDATED_DATETIME, DEFUNCT_IND "
				+ " , ITEM_CODE_TYPE, TXN_CODE, SHORT_CODE, TXN_DESC, TXN_DESC_LANG1 "
				+ " , TXN_DESC_LANG2, TXN_DESC_LANG3, TEMP_ID FROM TXNCODEMSTR "
				+ " WHERE TXNCODEMSTR_ID = ? ";
		try {
			List list = DbConnection.executeReader(DbConnection.KTHIS, query, new Object[] {txnCodeMstrId});
			if (list.size() > 0){
				HashMap row = (HashMap) list.get(0);
				result.setTxnCodeMstrId(row.get("TXNCODEMSTR_ID").equals("") ? null : (BigDecimal) row.get("TXNCODEMSTR_ID"));
				result.setTxnTypeMstrId(row.get("TXNTYPEMSTR_ID").equals("") ? null : (BigDecimal) row.get("TXNTYPEMSTR_ID"));
				result.setParentTxnCodeMstrId(row.get("PARENT_TXNCODEMSTR_ID").equals("") ? null : (BigDecimal) row.get("PARENT_TXNCODEMSTR_ID"));
				result.setBillingSummaryMstrId(row.get("BILLINGSUMMARYMSTR_ID").equals("") ? null : (BigDecimal) row.get("BILLINGSUMMARYMSTR_ID"));
				result.setPrevUpdatedBy(row.get("PREV_UPDATED_BY").equals("") ? null : (BigDecimal) row.get("PREV_UPDATED_BY"));
				result.setLastUpdatedBy(row.get("LAST_UPDATED_BY").equals("") ? null : (BigDecimal) row.get("LAST_UPDATED_BY"));
				result.setGlAccountMstrId(row.get("GLACCOUNTMSTR_ID").equals("") ? null : (BigDecimal) row.get("GLACCOUNTMSTR_ID"));
				result.setPrevUpdatedDateTime(row.get("PREV_UPDATED_DATETIME").equals("") ? null : (Timestamp) row.get("PREV_UPDATED_DATETIME"));
				result.setLastUpdatedDateTime(row.get("LAST_UPDATED_DATETIME").equals("") ? null : (Timestamp) row.get("LAST_UPDATED_DATETIME"));
				result.setDefunctInd(row.get("DEFUNCT_IND").equals("") ? null : (String) row.get("DEFUNCT_IND"));
				result.setItemCodeType(row.get("ITEM_CODE_TYPE").equals("") ? null : (String) row.get("ITEM_CODE_TYPE"));
				result.setTxnCode(row.get("TXN_CODE").equals("") ? null : (String) row.get("TXN_CODE"));
				result.setShortCode(row.get("SHORT_CODE").equals("") ? null : (String) row.get("SHORT_CODE"));
				result.setTxnDesc(row.get("TXN_DESC").equals("") ? null : (String) row.get("TXN_DESC"));
				result.setTxnDescLang1(row.get("TXN_DESC_LANG1").equals("") ? null : (String) row.get("TXN_DESC_LANG1"));
				result.setTxnDescLang2(row.get("TXN_DESC_LANG2").equals("") ? null : (String) row.get("TXN_DESC_LANG2"));
				result.setTxnDescLang3(row.get("TXN_DESC_LANG3").equals("") ? null : (String) row.get("TXN_DESC_LANG3"));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}
	
	private static void openCardHistory(Card card,CardManageView cardView){
		Cardstatushistory searchCardStatusHistory = new Cardstatushistory();
		searchCardStatusHistory.setCardId(card.getCardId());
		searchCardStatusHistory.setCardNo(card.getCardNo());
		Cardstatushistory cardStatusHistory = new Cardstatushistory();
		cardStatusHistory = buildCardStatusHistory(card,cardView);
		
		try {
			String InsertQuery = "INSERT INTO CARDSTATUSHISTORY ( "
					+ " CARD_ID, CARD_NO, OLD_CARD_STATUS, NEW_CARD_STATUS, CARD_TYPE "
					+ " , CARDSTATUSHISTORY_ID, EFFECTIVE_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATETIME, EXPIRY_DATE"
					+ " , PREV_UPDATED_BY, PREV_UPDATED_DATETIME, STAKEHOLDERACCOUNTTXN_ID, COLLECTION_STATUS, COUNTER_ID) "
					+ " VALUES (?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?)";
			Object[] parameters = {
					cardStatusHistory.getCardId(), cardStatusHistory.getCardNo(), cardStatusHistory.getOldCardStatus(), cardStatusHistory.getNewCardStatus(), cardStatusHistory.getCardType()
					, cardStatusHistory.getCardstatushistoryId(), cardStatusHistory.getEffectiveDate(), cardStatusHistory.getLastUpdatedBy(), cardStatusHistory.getLastUpdatedDatetime(), cardStatusHistory.getExpiryDate()
					, cardStatusHistory.getPrevUpdatedBy(), cardStatusHistory.getPrevUpdatedDatetime(), cardStatusHistory.getStakeholderaccounttxnId(), cardStatusHistory.getCollectionStatus(), cardStatusHistory.getCounterId()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, InsertQuery, parameters);
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		finally {
		}
		
//		this.crudDaoFactory.getCrudDao(CardStatusHistory.class).insert(cardStatusHistory);
	}
	
	private static Cardstatushistory buildCardStatusHistory(Card card,CardManageView cardView) {
//		Session session = SessionFactory.getInstance().getCurrentSession();	
		CommonService cs = new CommonService();
		Cardstatushistory cardHistory = new Cardstatushistory();
		cardHistory.setCardId(card.getCardId());
		cardHistory.setCardNo(card.getCardNo());
		cardHistory.setCardstatushistoryId(cs.getPrimaryKey());
		cardHistory.setCardType(card.getCardType());
		
		cardHistory.setEffectiveDate(card.getLastUpdatedDatetime());
//		cardHistory.setExpiryDate(card.getLastUpdatedDateTime());
		
		cardHistory.setLastUpdatedBy(IConstant.WS_USER);// session.getAppUserId());
		cardHistory.setLastUpdatedDatetime(cs.getCurrentTime()); // session.getRequestTimestamp());
		
		cardHistory.setOldCardStatus(cardView.getPreCardStatus());
		cardHistory.setNewCardStatus(card.getCardStatus());
		
		cardHistory.setCollectionStatus(ICodeMstrDetailConst.CCS_1);
		
		boolean txnFlag = (IConstant.IND_YES.equals(cardView.getCardDepositeIncome()) || IConstant.IND_YES.equals(cardView.getCardDepositeOut()));
		if(txnFlag){
			cardHistory.setStakeholderaccounttxnId(card.getStakeholderaccounttxnId());
		}else{
			cardHistory.setStakeholderaccounttxnId(null);
		}
		
		return cardHistory;
	}

	private static CardManageView getCardFeeView() {
		List<CardManageView> cardFeeList = getCardFee(); // cardManageBridgeDao.getCardFee(null);
		CardManageView cardFee = null;
		
		//后台决定收什么费用
		String cardInitTXNValue = null;
		for(int i = 0; i < cardFeeList.size(); i++){
			CardManageView parameterView = (CardManageView) cardFeeList.get(i);
		
			if(IParameterKTHISNameConst.CARD_INIT_TXN.equals(parameterView.getParameterName())){
				cardInitTXNValue = parameterView.getParameterValue();
			}else{
				parameterView.setCardValue(new BigDecimal(parameterView.getParameterValue()));
			}
		}
		
		//选择使用哪种收费项目
		String txnCode = ""; //$NON-NLS-1$
		if("DEPOSIT".equals(cardInitTXNValue)){ //$NON-NLS-1$
			txnCode = ITxnCodeMstrConst.TXN_CODE_CDDEP;
		}else if("FEE".equals(cardInitTXNValue)){ //$NON-NLS-1$
			txnCode = ITxnCodeMstrConst.TXN_CODE_HMCCHG;
		}else{
			txnCode = ITxnCodeMstrConst.TXN_CODE_NONE;
		}
		
		for(int i = 0; i < cardFeeList.size(); i++){
			cardFee = (CardManageView) cardFeeList.get(i);
			if(txnCode.equals(cardFee.getTxnCode())){
				break;
			}
		}
		
		return cardFee;
	}
	
	private static List<CardManageView> getCardFee(){
		List<CardManageView> result = new ArrayList<CardManageView>();
		String query = "SELECT "
				+ "	TCM.TXNCODEMSTR_ID "
				+ " ,TCM.TXN_CODE "
				+ " ,TCM.TXN_DESC "
				+ " ,TCM.TXN_DESC_LANG1 "
				+ " ,PM.VALUE "
				+ " ,PM.PARAMETER_NAME "
				+ " FROM PARAMETER PM, TXNCODEMSTR TCM"
				+ " WHERE "
				+ "	TCM.TXN_CODE = 'CDDEP' AND  PM.PARAMETER_NAME = 'CARD_DEPOSIT' "
				+ "	OR (TCM.TXN_CODE = 'CDRFD' AND  PM.PARAMETER_NAME = 'CARD_DEPOSIT') "
				+ " OR(TCM.TXN_CODE = 'HMCCHG' AND  PM.PARAMETER_NAME = 'CARD_FEE')"
				+ " UNION ALL "
				+ " SELECT "
				+ " NULL TXNCODEMSTR_ID "
				+ " ,NULL TXN_CODE "
				+ " ,NULL TXN_DESC "
				+ " ,NULL TXN_DESC_LANG1 "
				+ " ,PM.VALUE "
				+ " ,PM.PARAMETER_NAME "
				+ "	FROM PARAMETER PM "
				+ " WHERE PM.PARAMETER_NAME = 'CARD_INIT_TXN'";
		try {
			List list = DbConnection.executeReader(DbConnection.KTHIS, query, new Object[] {});
			for (int a=0;a<list.size();a++){
				HashMap row = (HashMap)list.get(a);
				CardManageView view = new CardManageView();
				view.setTxnCodeMstrId((BigDecimal) row.get("TXNCODEMSTR_ID"));
				view.setTxnCode(row.get("TXN_CODE").toString());
				view.setTxnDesc(row.get("TXN_DESC").toString());
				view.setTxmDescLang1(row.get("TXN_DESC_LANG1").toString());
				view.setParameterValue(row.get("VALUE").toString());
				view.setParameterName(row.get("PARAMETER_NAME").toString());
				result.add(view);
			}
		} catch (SQLException e){
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			
		}
		return result;
	}
	
	private static void createPatientAccountForPatient(String patientNo, BigDecimal stakeholderId) throws SQLException, Exception {
//		PatientInfoView patientInfoView = (PatientInfoView) patientInfoService.getPatientInfoViewByPatientId(patientId);
//		String patientNo = patientInfoView.getPatient().getPatientNo();
//		BigDecimal stakeholderId = patientInfoView.getPerson().getStakeHolderId();
		
		Patientaccount patientAccount = new Patientaccount();
		patientAccount.setStakeholderId(stakeholderId);
		patientAccount.setAccountStatus(ICodeMstrDetailConst.ACS_1);
		patientAccount.setPatientAccountNo(patientNo);
		patientAccount = creatPatientAccount(patientAccount);

		createAccountDebtorForSelfpay(stakeholderId, patientAccount.getPatientaccountId());
	}
	
	private static Patientaccount creatPatientAccount(Patientaccount patientAccount) {
//		Session session = SessionFactory.getInstance().getCurrentSession();
		CommonService cs = new CommonService();
		patientAccount.setLastUpdatedBy(IConstant.WS_USER); // session.getAppUserId());
		patientAccount.setLastUpdatedDatetime(cs.getCurrentTime()); // session.getRequestTimestamp());
		BigDecimal patientAccountId = cs.getPrimaryKey();
		patientAccount.setPatientaccountId(patientAccountId);
//		this.crudDaoFactory.getCrudDao(PatientAccount.class).insert(patientAccount);
//		return this.crudDaoFactory.getCrudDao(PatientAccount.class).get(patientAccountId);
		
		try {
			String InsertQuery = "INSERT INTO PATIENTACCOUNT ( "
					+ " PATIENTACCOUNT_ID, VISIT_ID, PREV_UPDATED_BY, LAST_UPDATED_BY, ACCOUNT_BALANCE "
					+ " , BILL_SIZE, FROZEN_ACCOUNT_BALANCE, FROZEN_BILL_SIZE, ACCOUNT_POSTED_DATE, FROZEN_DATETIME "
					+ " , PREV_UPDATED_DATETIME, LAST_UPDATED_DATETIME, OPEN_CARD_IND, ACCOUNT_CHANGED_IND, ACCOUNT_STATUS "
					+ " , PRICING_POLICY_CODE, PATIENT_ACCOUNT_NO, DEPOSIT_BALANCE, STAKEHOLDER_ID, INSURANCE_COVERAGE_AMOUNT) "
					+ " VALUES (?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?)";
			Object[] parameters = {
					patientAccount.getPatientaccountId(), null, patientAccount.getPrevUpdatedBy(), patientAccount.getLastUpdatedBy(), patientAccount.getAccountBalance() == null ? 0 : patientAccount.getAccountBalance()
					,patientAccount.getBillSize() == null ? 0 : patientAccount.getBillSize(), patientAccount.getFrozenAccountBalance() == null ? 0 : patientAccount.getFrozenAccountBalance(), patientAccount.getFrozenBillSize() == null ? 0 : patientAccount.getFrozenBillSize(), patientAccount.getAccountPostedDate(), patientAccount.getFrozenDatetime()
					,patientAccount.getPrevUpdatedDatetime(), patientAccount.getLastUpdatedDatetime(), patientAccount.getOpenCardInd() == null ? IConstant.IND_NO : patientAccount.getOpenCardInd(), patientAccount.getAccountChangedInd() == null ? IConstant.IND_NO : patientAccount.getAccountChangedInd(), patientAccount.getAccountStatus()
					,patientAccount.getPricingPolicyCode(), patientAccount.getPatientAccountNo(), patientAccount.getDepositBalance() == null ? 0 : patientAccount.getDepositBalance(), patientAccount.getStakeholderId(), patientAccount.getInsuranceCoverageAmount() == null ? 0 : patientAccount.getInsuranceCoverageAmount()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, InsertQuery, parameters);
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		finally {
		}
		
		return patientAccount;
	}
	
	private static void createAccountDebtorForSelfpay(BigDecimal stakeHolderId, BigDecimal patientAccountId) throws SQLException, Exception {
		Accountdebtor accountDebtor = new Accountdebtor();
		accountDebtor.setStakeholderId(stakeHolderId);
		accountDebtor.setPatientAccountId(patientAccountId);
		accountDebtor.setPayerSeq(new BigDecimal(10));
		accountDebtor.setFinancialClass(ICodeMstrDetailConst.FCC_1);
		accountDebtor.setClaimPolicyCode(IPACommonConst.PA_ACCOUNTDEBTOR_SELF);
		
		creatAccounrDebtorInfo(accountDebtor);
	}
	
	private static Accountdebtor creatAccounrDebtorInfo(Accountdebtor accountDebtor) throws SQLException, Exception  {
		CommonService cs = new CommonService();
//		Session session = SessionFactory.getInstance().getCurrentSession();
		accountDebtor.setLastUpdatedBy(IConstant.WS_USER); // session.getAppUserId());
		accountDebtor.setLastUpdatedDatetime(cs.getCurrentTime()); // session.getRequestTimestamp());
		BigDecimal accountDebtorId = cs.getPrimaryKey();
		accountDebtor.setAccountdebtorId(accountDebtorId);
		if(accountDebtor.getFinancialClass() == null || "".equals(accountDebtor.getFinancialClass())){ //$NON-NLS-1$
//			throw new BusinessException( Messages.getString("PatientAccountBridgeServiceImpl.creatAccounrDebtorInfo.error.noCreateException"), null, null); //$NON-NLS-1$
		}
		
		try {
			String InsertQuery = "INSERT INTO ACCOUNTDEBTOR ( "
					+ " ACCOUNTDEBTOR_ID, STAKEHOLDER_ID, PATIENTACCOUNT_ID, PAYER_SEQ, CONTRACT_BALANCE "
					+ " , PAID_AMOUNT, LAST_BILL_AMOUNT, FROZEN_CONTRACT_BALANCE, FROZEN_CONTRACT_SIZE, CN_YTD_SELFPAID_AMOUNT "
					+ " , CN_LAST_CLAIM_AMOUNT, CN_SELF_PAYABLE_AMOUNT, FROZEN_DATETIME, CN_LAST_CLAIM_DATE, DEBTOR_CHANGED_IND "
					+ " , CONTRACT_TYPE, CONTRACT_STATUS, FINANCIAL_CLASS, RELATIONSHIP, LAST_BILL_TYPE "
					+ " , CN_MIO_INSURANCE, CONTRACT_NO, CLAIM_POLICY_CODE, CONCESSION_CODE, POLICY_NO "
					+ " , CLAIM_NO, LAST_BILL_NO, LAST_SUPPLEMENT_NO, REMARKS, CN_HEALTHCARE_PLAN_IND "
					+ " , CN_MIO_METHOD, CN_INDUSTRIAL_SICKNESS, CN_MIO_PERSON_GROUP, CN_MAJOR_DISEASE_IND, CN_MAJOR_DISEASE_TYPE "
					+ " , CN_HEALTHCARE_PLAN_NO, CN_APPOINTED_HOSPITAL_IND, GUARANTEELETTER_ID, LAST_BILL_DATE, CANCELLED_BY "
					+ " , CANCELLED_DATETIME, CANCEL_REASON, DEFUNCT_IND, PREV_UPDATED_BY, LAST_UPDATED_BY "
					+ " , PREV_UPDATED_DATETIME, LAST_UPDATED_DATETIME, YTD_CLAIMED_AMOUNT ) "
					+ " VALUES (?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?,?,?"
					+ " ,?,?,?)";
			Object[] parameters = {
					accountDebtor.getAccountdebtorId(), accountDebtor.getStakeholderId(), accountDebtor.getPatientAccountId(), accountDebtor.getPayerSeq(), accountDebtor.getContractBalance() == null ? 0 : accountDebtor.getContractBalance()
					, 0, 0, 0, 0, 0
					, 0, 0, accountDebtor.getFrozenDatetime(), accountDebtor.getCnLastClaimDate(), accountDebtor.getDebtorChangedInd() == null ? IConstant.IND_NO : accountDebtor.getDebtorChangedInd()
					, accountDebtor.getContractType(), accountDebtor.getContractStatus(), accountDebtor.getFinancialClass(), accountDebtor.getRelationship(), accountDebtor.getLastBillType()
					, accountDebtor.getCnMioInsurance(), accountDebtor.getContractNo(), accountDebtor.getClaimPolicyCode(), accountDebtor.getConcessionCode(), accountDebtor.getPolicyNo()
					, accountDebtor.getClaimNo(), accountDebtor.getLastBillNo(), accountDebtor.getLastSupplementNo(), accountDebtor.getRemarks(), accountDebtor.getCnHealthcarePlanInd() == null ? IConstant.IND_NO : accountDebtor.getCnHealthcarePlanInd()
					, accountDebtor.getCnMioMethod(), accountDebtor.getCnIndustrialSickness(), accountDebtor.getCnMioPersonGroup(), accountDebtor.getCnMajorDiseaseInd() == null ? IConstant.IND_NO : accountDebtor.getCnMajorDiseaseInd(), accountDebtor.getCnMajorDiseaseType()
					, accountDebtor.getCnHealthcarePlanNo(), accountDebtor.getCnAppointedHospitalInd() == null ? IConstant.IND_NO : accountDebtor.getCnAppointedHospitalInd(), accountDebtor.getGuaranteeletterId(), accountDebtor.getLastBillDate(), accountDebtor.getCancelledBy()
					, accountDebtor.getCancelledDatetime(), accountDebtor.getCancelReason(), accountDebtor.getDefunctInd() == null ? IConstant.IND_NO : accountDebtor.getDefunctInd(), accountDebtor.getPrevUpdatedBy(), accountDebtor.getLastUpdatedBy()
					, accountDebtor.getPrevUpdatedDatetime(), accountDebtor.getLastUpdatedDatetime(), accountDebtor.getYtdClaimedAmount()
			};
			DbConnection.executeQuery(DbConnection.KTHIS, InsertQuery, parameters);
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
			throw e;
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}
		finally {
		}
		
//		this.crudDaoFactory.getCrudDao(AccountDebtor.class).insert(accountDebtor);
		return accountDebtor;// this.crudDaoFactory.getCrudDao(AccountDebtor.class).get(accountDebtorId);
	}
	
//	private void createCustomerClassHistory(PersonView personView) {
//		CustomerClassHistory customerClassHistory = personView.getCustomerClassHistory();
//		customerClassHistory.setCustomerClassHistoryId(new CommonService().getPrimaryKey());
//		customerClassHistory.setPatientId(personView.getPatient().getPatientId());
////		Session session = SessionFactory.getInstance().getCurrentSession();
//		customerClassHistory.setEntityMstrId(session.getEntityMstrId());
//		customerClassHistory.setLocationMstrId(session.getLocationId());
//		customerClassHistory.setEnteredBy(BigDecimal.ZERO);
//		customerClassHistory.setEnteredDatetime(session.getRequestTimestamp());
//		crudDaoFactory.getCrudDao(CustomerClassHistory.class).insert(customerClassHistory);
//	}
}