package com.rsmurniteguh.webservice.dep.kthis.services.mobile;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.net.ssl.HttpsURLConnection;

import org.apache.tomcat.jdbc.pool.DataSource;

import com.rsmurniteguh.webservice.dep.all.model.CareProvider;
import com.rsmurniteguh.webservice.dep.all.model.dashboard.AppointmentStatus;
import com.rsmurniteguh.webservice.dep.all.model.doctorsMobile.DoctorVisitRegnList;
import com.rsmurniteguh.webservice.dep.all.model.gym.ExtendMembership;
import com.rsmurniteguh.webservice.dep.all.model.gym.InfoMember;
import com.rsmurniteguh.webservice.dep.all.model.mobile.MainDataDoctor;
import com.rsmurniteguh.webservice.dep.biz.DashboardBiz;
import com.rsmurniteguh.webservice.dep.kthis.services.CareProviderSer;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;

public class DoctorMobileService extends DbConnection {
    
	public Boolean postNotification(List<String> visitIds)
	{
		Boolean success = false;
		
		String url = "https://gcm-http.googleapis.com/gcm/send";
		URL obj;
		try {
			obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

			String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			//print result
			System.out.println(response.toString());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return success;
	}
	
	public List<DoctorVisitRegnList> GetDoctorsAvailableRegistration()
	{
//		CareProviderSer.getquenum(null);
		Connection connection = null;
		   Statement st = null;
		   ResultSet rs = null;
		   
		List<DoctorVisitRegnList> all=new ArrayList<DoctorVisitRegnList>();
		connection = DbConnection.getPooledConnection();
		if(connection == null)return null;
		String abcarpro="select * from (select v.VISIT_ID, vd.CAREPROVIDER_ID, sp.SUBSPECIALTY_DESC, "
				+ "ps1.PERSON_NAME as pasien, ps2.PERSON_NAME as dokter "
//				+ "(select count(*) from ORDERENTRY oe where "
//				+ "oe.VISIT_ID = v.VISIT_ID) + (select count(*) from VISITDIAGNOSIS vd where vd.visit_id = v.VISIT_ID) as ind "
				+ "from visit v "
				+ "inner join VISITREGNTYPE vd on vd.VISIT_ID = v.VISIT_ID "
				+ "inner join patient pt on pt.PATIENT_ID = v.PATIENT_ID "
				+ "inner join person ps1 on ps1.PERSON_ID = pt.PERSON_ID "
				+ "inner join CAREPROVIDER cp on cp.CAREPROVIDER_ID = vd.CAREPROVIDER_ID "
				+ "inner join person ps2 on ps2.PERSON_ID =cp.PERSON_ID "
				+ "inner join SUBSPECIALTYMSTR sp on sp.SUBSPECIALTYMSTR_ID = v.SUBSPECIALTYMSTR_ID "
				+ "where v.PATIENT_TYPE = 'PTY2' "
				+ "and to_char(v.ADMISSION_DATETIME, 'ddmmyyyy') = to_char(sysdate, 'ddmmyyyy') "
				+ "and v.ADMIT_STATUS in('AST1', 'AST2', 'AST3', 'AST7'))";
		try {
			st=connection.createStatement();
			rs=st.executeQuery(abcarpro);
			while(rs.next())
				{
					DoctorVisitRegnList dl=new DoctorVisitRegnList();
					dl.setCareproviderId(rs.getBigDecimal("CAREPROVIDER_ID"));
					dl.setPasien(rs.getString("PASIEN"));
					dl.setSubspeciality_desc(rs.getString("SUBSPECIALTY_DESC"));
					dl.setDokter(rs.getString("DOKTER"));
					all.add(dl);
				}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (st!=null) try  { st.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return all;
	}
	
	public static Object getMainDataDoctor(String usermstr_id) {
		MainDataDoctor data = new MainDataDoctor();
		
		Connection connection = null, connection2 = null;
		connection=DbConnection.getPooledConnection();
		connection2=DbConnection.getBpjsOnlineInstance();
		if(connection==null || connection2==null)return null;
		
		ResultSet rs = null, rs2 = null;
		PreparedStatement ps = null, ps2 = null;
		
		//inpatient jumlah
		String sql="select count(*)  as count, tab.patient_class from( "
				+ "select "
				+ "case pt.patient_class "
				+ "when 'PTC114' then 'BPJS' "
				+ "else 'GENERAL' "
				+ "end as patient_class "
				+ "from visit v "
				+ "inner join visitdoctor vd on v.VISIT_ID = vd.VISIT_ID "
				+ "inner join patient pt on pt.patient_id = v.patient_id "
				+ "inner join person p on p.person_id = pt.person_id "
				+ "inner join card c on c.PERSON_ID = p.person_id "
				+ "inner join ( select *  from ( select bh.visit_id, wm.ward_desc,bm.Bed_No, "
				+ "ROW_NUMBER() OVER (PARTITION BY bh.visit_id ORDER BY bh.effective_start_datetime DESC) as rn "
				+ "from BEDHISTORY bh "
				+ "inner join BEDMSTR bm on bm.bedmstr_id = bh.bedmstr_id "
				+ "inner join ROOMMSTR rm on rm.ROOMMSTR_id = bm.ROOMMSTR_id inner join WARDMSTR wm on wm.wardmstr_id = rm.WARDMSTR_id )  where rn = 1) "
				+ "bed on bed.visit_id = v.visit_id "
				+ "where vd.careprovider_id = ? and "
				+ "v.patient_type= 'PTY1' and v.admit_status='AST1' ) tab group by tab.patient_class";
		
		//reg today and yesterday
		String sql2="SELECT convert(varchar,queue_date,111) as tanggal, count(careprovider_id) as count_patient, doctor_name, department "
				+ "FROM tb_registrasi "
				+ "WHERE queue_date = ?  AND status = 'Registered' and careprovider_id = ? "
				+ "group by convert(varchar,queue_date,111), doctor_name, department, status , careprovider_id ";
		
		try {
			
			BigDecimal careprovider_id = getCareproviderByUser(new BigDecimal(usermstr_id));
			
			ps = connection.prepareStatement(sql);
			ps.setEscapeProcessing(true);
			ps.setQueryTimeout(30000);
			ps.setBigDecimal(1, careprovider_id);
			rs=ps.executeQuery();
			
			data.setInpatient_bpjs(0);
			data.setInpatient_general(0);
			while(rs.next())
			{
				// ambil count saja
				if(rs.getString("patient_class").equals("BPJS")){
					data.setInpatient_bpjs(rs.getInt("count"));
				} else if(rs.getString("patient_class").equals("GENERAL")){
					data.setInpatient_general(rs.getInt("count"));
				}
			}
			
			
			
			SimpleDateFormat df= new SimpleDateFormat("yyyy/MM/dd");
			Calendar cal = Calendar.getInstance();
	        String today = df.format(cal.getTime());
			
			ps2 = connection2.prepareStatement(sql2);
			ps2.setEscapeProcessing(true);
			ps2.setQueryTimeout(30000);
			ps2.setString(1, today );
			ps2.setBigDecimal(2, careprovider_id);
			rs2=ps2.executeQuery();
			
			data.setReg_today(0);
			data.setReg_yesterday(0);
			while(rs2.next())
			{
				if(rs2.getString("department").equals("01")){ //bpjs
					data.setReg_today(rs2.getInt("count_patient"));
				} else if (rs2.getString("department").equals("02")){
					data.setReg_yesterday(rs2.getInt("count_patient"));
				}
				// ambil reg hari ini dan reg besok 
			}
		
		}
		
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (rs2!=null) try  { rs2.close(); } catch (Exception ignore){}
		     if (ps2!=null) try  { ps2.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		     if (connection2!=null) try { connection2.close();} catch (Exception ignore){}
		   }
		return data;
	}
	
	public static BigDecimal getCareproviderByUser(BigDecimal usermstr_id) {
		System.out.println("getCareproviderByUser...");
		
		BigDecimal data=null;

		Connection connection=DbConnection.getPooledConnection();
		if(connection==null)return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql = "select cp0.CAREPROVIDER_ID "+ 
		          "from CAREPROVIDER cp0 inner join USERPROFILE up0 on up0.PERSON_ID = cp0.PERSON_ID "+ 
		          "and up0.USERMSTR_ID = ?";

		try {	
				ps = connection.prepareStatement(sql);
				ps.setEscapeProcessing(true);
				ps.setQueryTimeout(60000);
				ps.setBigDecimal(1, usermstr_id);
				rs = ps.executeQuery();
				
				while (rs.next()) {
					data = rs.getBigDecimal("CAREPROVIDER_ID");				}
		}
		
		catch (SQLException e)
		{
			e.fillInStackTrace();
			System.out.println(e.getMessage());
		}
		finally
		{	
			 if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		    
		}
		return data;
	}
	
	
}
