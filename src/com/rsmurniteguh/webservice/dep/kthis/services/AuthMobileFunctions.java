package com.rsmurniteguh.webservice.dep.kthis.services;

import com.rsmurniteguh.webservice.dep.all.model.ResponseStatus;
import com.rsmurniteguh.webservice.dep.all.model.gym.ResponseString;
import com.rsmurniteguh.webservice.dep.all.model.hris.EmployeDetail;
import com.rsmurniteguh.webservice.dep.all.model.mobile.transaction_emoney;
import com.rsmurniteguh.webservice.dep.kthis.trxmanager.DbConnection;
import com.rsmurniteguh.webservice.dep.util.AES;
import com.rsmurniteguh.webservice.dep.util.CommonUtil;
import com.rsmurniteguh.webservice.dep.util.EncryptGym;
import com.rsmurniteguh.webservice.dep.util.EncryptUtil;
import com.rsmurniteguh.webservice.dep.util.Encryptor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.tasks.OnSuccessListener;


public class AuthMobileFunctions {
private static Boolean complete = false;
	
	public static void insertAuthorisation(String usermstr_id, String key, String salt)
	{
		Connection connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return;
		String SPsql = "EXEC logUserToken ?,?,?";
		PreparedStatement ps = null;;
		try {
			ps = connection.prepareStatement(SPsql);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, usermstr_id);
			ps.setString(2, key);
			ps.setString(3, salt);
			
			ps.executeUpdate();
		}
		
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}		
	}
		
	public static ResponseString SaveRegistrationToken(String user, String token)
	{
		System.out.println("Save token of User = "+user);
		ResponseString res = new ResponseString();
		Connection connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		String SPsql = "update usertoken set firebase_token = ? where usermstr_id = ? and deleted = 0";
		PreparedStatement ps = null;;
		try {
			ps = connection.prepareStatement(SPsql);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, token);
			ps.setString(2, user);
			
			ps.executeUpdate();
			res.setResponse("OK");
		}
		
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
			res.setResponse("FAIL");
		}
		finally
		{
		     if (ps!=null) try  { ps.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		}
		return res;
	}
	
	public static String getTokenByUsermstrId(BigDecimal usermstrId)
	{		
		String token = null;
		Connection connection = null;
		connection=DbConnection.getBpjsOnlineInstance();
		if(connection==null)return null;
		PreparedStatement st = null;
		   ResultSet rs = null;
		String sql="SELECT firebase_token from usertoken where usermstr_id = ? and deleted = 0";
		try {
			st=connection.prepareStatement( sql );
			st = CommonUtil.initialStatement(st);
			st.setBigDecimal(1, usermstrId);
			
			rs=st.executeQuery();
			while(rs.next())
				{
				token = rs.getString("TOKEN");
				}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		finally {
		     if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		     if (st!=null) try  { st.close(); } catch (Exception ignore){}
		     if (connection!=null) try { connection.close();} catch (Exception ignore){}
		   }
		return token;
		
	}

	public static ResponseString CheckLogin(String user, String pass)
	{
		System.out.println("CheckLoginGym...");
		ResponseString res = new ResponseString();

		Connection connection = null;
		connection = DbConnection.getBpjsOnlineInstance();
		if(connection == null)return null;
		
		PreparedStatement st = null;
		ResultSet rs = null;
				
		complete = false;
		try {
			
			EncryptUtil.getInstance();
			String reEncryptpass = EncryptGym.encryptOnlineAppointment(Encryptor.getInstance().decrypt(pass));
			String salt = AES.getInstance().generateSalt();
			String encodedSalt = Base64.getEncoder().encodeToString(salt.getBytes());
			
			String sql = "select user_id, username, password from tb_user u where LOWER(u.username) = ? and u.password = ? "
					+ " order by u.user_id  ";
			
			st=connection.prepareStatement(sql);
			st = CommonUtil.initialStatement(st);
			st.setString(1, user.toLowerCase());
			st.setString(2, reEncryptpass);
			rs=st.executeQuery();
			
			if(rs.next())
			{	
				String uid = rs.getString("user_id");
				HashMap<String, Object> additionalClaims = new HashMap<String, Object>();
				additionalClaims.put("usermstr_id", rs.getString("user_id"));
				additionalClaims.put("name", rs.getString("username"));
				additionalClaims.put("logintype", "staff");
				additionalClaims.put("salt", encodedSalt);
				
				FirebaseApp instanceGym = FirebaseApp.getInstance("gym");
				
				FirebaseAuth.getInstance(instanceGym).createCustomToken(uid, additionalClaims)
			    .addOnSuccessListener(new OnSuccessListener<String>() {
			        @Override
			        public void onSuccess(String customToken) {
			        	res.setResponse(customToken);
			        	System.out.println(customToken);
			        	AuthMobileFunctions.insertAuthorisation(uid, customToken, salt);
			        	complete = true;
			        }
			    });
				
			}else {
				complete = true;
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			complete = true;
		}
		finally
		{
			if (rs!=null) try  { rs.close(); } catch (Exception ignore){}
		    if (st!=null) try  { st.close(); } catch (Exception ignore){}
		    if (connection!=null) try { connection.close();} catch (Exception ignore){}
		    
		    int a = 0;
		     
		     while(!complete)
			{
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(a>60) break;
				else
				{
					a+=10;
				}
			}
		}
		
		return res;
	}
	
	public static ResponseStatus getCheckLoginHris(String username, String password) {

		ResponseStatus data = new ResponseStatus();
		Connection Connection = null;
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null, rs2 = null;
		String pass =  EncryptUtil.getInstance().StringtoMD5(Encryptor.getInstance().decrypt(password));		
		try {			
						
			Connection = DbConnection.getHrisInstance();
			if (Connection == null ) throw new Exception("Database failed to connect !");
			
			String selectQuery = " SELECT usr.Username, usr.Password, usr.IDKaryawan , usr.Aktif "
					+ "FROM TblUsers usr "
					+ " where usr.Username = ?  and usr.Password = ?  and usr.Aktif = 1 ";
			
			ps = Connection.prepareStatement(selectQuery);
			ps = CommonUtil.initialStatement(ps);
			ps.setString(1, username);
			ps.setString(2, pass);
			rs = ps.executeQuery();
			
			if(rs.next()){
				String Ids = rs.getString("Username");
				String updatequery = "UPDATE TblUsers SET LastLogin = sysdatetime(), Online = 1 WHERE Username = ? ";
				ps2 = Connection.prepareStatement(updatequery);
				ps2 = CommonUtil.initialStatement(ps2);
				ps2.setString(1, Ids);
				ps2.executeUpdate();
				
				data.setSuccess(true);
				data.setMessage(rs.getString("IDKaryawan"));
			}
		}
		catch (SQLException e){
		    if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		catch (Exception e) {
			if (Connection != null) try { Connection.rollback(); } catch (SQLException sqle) {}
		}
		finally {
 			if (Connection != null) try { Connection.close(); } catch (SQLException e) {}
			if (ps != null) try { ps.close(); } catch (SQLException e) {}
			if (rs != null) try { rs.close(); } catch (SQLException e) {}
			if (ps2 != null) try { ps2.close(); } catch (SQLException e) {}
			if (rs2 != null) try { rs2.close(); } catch (SQLException e) {}
		}
		return data;
	}
		
}
