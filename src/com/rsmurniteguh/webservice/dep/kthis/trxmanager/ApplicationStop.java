
package com.rsmurniteguh.webservice.dep.kthis.trxmanager;


import org.springframework.beans.factory.DisposableBean;

public class ApplicationStop implements DisposableBean {
	public ApplicationStop()
	{
		
	}
	@Override
	public void destroy() throws Exception {
		DbConnection.getPooledConnection().close();
		DbConnection.getRisInstance().close();
	}

}
