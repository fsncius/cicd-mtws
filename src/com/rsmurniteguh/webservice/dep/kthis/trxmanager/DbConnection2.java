package com.rsmurniteguh.webservice.dep.kthis.trxmanager;

//--------- DB CONNECTION TESTING BRO --------
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.beans.factory.InitializingBean;

import oracle.jdbc.OraclePreparedStatement;

public class DbConnection2 implements InitializingBean {
	//TIDAK PAKAI LAGI
//private static Connection connection;
	
//	private static Connection risConnection;
//	
//	private static Boolean checkConnection(Connection c)
//	{
//		Boolean res = false;
//		
//		try {
//	 		OraclePreparedStatement opstmt = null;
//			opstmt = (OraclePreparedStatement) c.prepareStatement( "select 1 from dual" );
//			ResultSet rs=opstmt.executeQuery();
//			if(rs.next())
//		 	{
//		 		res = true;
//		 	}
//			opstmt.close();
//			rs.close();
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
//		finally {
//			
//		}
//		
//		return res;
//	}
//	
//	public static Connection getRisInstance()
//	{
//	 	if(checkConnection(risConnection)) return risConnection;
//	 	else
//	 	{
//	 		for(int i=1; i<=5; i++)
//			{
//				try {
//					if(checkConnection(risConnection))
//						break;
//					TimeUnit.SECONDS.sleep(1);
//					System.out.println("Attempt " + i +" to open connection");
//					openRisConnection();
//				} catch (Exception e) {
//					System.out.println(e.getMessage());
//				} 			
//				
//			}
//	 		if(checkConnection(risConnection)) return risConnection;
//	 		else
//	 		{
//	 			System.out.println("Failed to get connection after 5 attempts, giving up");
//	 			return null;
//	 		}
//	 	}
//	}
//	
//	public static Connection getInstance()
//	{
//		if(checkConnection(connection)) return connection;
//	 	else
//	 	{
//	 		for(int i=1; i<=5; i++)
//			{
//				try {
//					if(checkConnection(connection))
//						break;
//					TimeUnit.SECONDS.sleep(1);
//					System.out.println("Attempt " + i +" to open connection");
//					openHisConnection();
//					
//				} catch (Exception e) {
//					System.out.println(e.getMessage());
//				} 			
//				
//			}
//	 		if(checkConnection(connection)) return connection;
//	 		else
//	 		{
//	 			System.out.println("Failed to get connection after 5 attempts, giving up");
//	 			return null;
//	 		}
//	 	}
//	}
//	
//	public static void openHisConnection() {
//		PoolProperties p = new PoolProperties();
//		p.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//		p.setMaxIdle(75);
//		p.setNumTestsPerEvictionRun(5);
//		p.setPassword("IHIS");
//		p.setTestOnBorrow(true);
//		p.setTestOnReturn(true);
//		p.setTestWhileIdle(true);
//		p.setTimeBetweenEvictionRunsMillis(-1);
//		p.setUrl("jdbc:oracle:thin:@192.168.1.254:1521:OCT15");
//        p.setUsername("ihis");
//        p.setValidationQuery("select sysdate from dual");        
//        p.setRemoveAbandoned(true);
//        
//        p.setJmxEnabled(true);
//        p.setValidationInterval(3000);
//        p.setMaxActive(100);
//        p.setInitialSize(10);
//        p.setMaxWait(15000);
//        p.setRemoveAbandonedTimeout(10);
//        p.setMinEvictableIdleTimeMillis(1800000);
//        p.setMinIdle(20);
//        p.setLogAbandoned(true);
//        
//        
//        p.setJdbcInterceptors(
//          "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
//          "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
//        DataSource datasource = new DataSource();
//        datasource.setPoolProperties(p);
//		
//		
//		connection = null;
// 
//		try {
//
//			connection = datasource.getConnection();
//
//		} catch (SQLException e) {
//
//			System.out.println("Connection Failed! Check output console");
//			e.printStackTrace();
//
//		}
//		if (connection != null) {
//			System.out.println("You made it, take control your database now!");
//		} else {
//			System.out.println("Failed to make connection!");
//		}
//	}
//	
//	public static void openRisConnection() {
//		PoolProperties pRis = new PoolProperties();
//		pRis.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//		pRis.setMaxIdle(75);
//		pRis.setNumTestsPerEvictionRun(5);
//		pRis.setPassword("server");
//		pRis.setTestOnBorrow(true);
//		pRis.setTestOnReturn(true);
//		pRis.setTestWhileIdle(true);
//		pRis.setTimeBetweenEvictionRunsMillis(-1);
//		pRis.setUrl("jdbc:oracle:thin:@192.168.1.203:1521:MTPACS");
//		pRis.setUsername("uni_ris");
//		pRis.setValidationQuery("select sysdate from dual");        
//		pRis.setRemoveAbandoned(true);
//        
//		pRis.setJmxEnabled(true);
//		pRis.setValidationInterval(3000);
//		pRis.setMaxActive(100);
//		pRis.setInitialSize(10);
//		pRis.setMaxWait(15000);
//		pRis.setRemoveAbandonedTimeout(10);
//		pRis.setMinEvictableIdleTimeMillis(1800000);
//		pRis.setMinIdle(20);
//		pRis.setLogAbandoned(true);
//        
//        
//		pRis.setJdbcInterceptors(
//          "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
//          "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
//        DataSource datasourceRis = new DataSource();
//        datasourceRis.setPoolProperties(pRis);
//		
//		
//		risConnection= null;
// 
//		try {
//
//			risConnection= datasourceRis.getConnection();
//
//		} catch (SQLException e) {
//
//			System.out.println("Connection Failed! Check output console");
//			e.printStackTrace();
//
//		}
//		if (connection != null) {
//			System.out.println("You made it, take control your database now!");
//		} else {
//			System.out.println("Failed to make connection!");
//		}
//	}
//	
//	public void afterPropertiesSet() {
//		
//		openHisConnection();
//		openRisConnection();
//				
//	  }
	
	
	public DbConnection2()
	{		
		//TIDAK PAKAI LAGI
//		System.out.println("Started");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		//TIDAK PAKAI LAGI
		// TODO Auto-generated method stub
		
	}
}
