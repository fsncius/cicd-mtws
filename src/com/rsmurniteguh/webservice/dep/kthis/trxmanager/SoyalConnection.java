package com.rsmurniteguh.webservice.dep.kthis.trxmanager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public abstract class SoyalConnection extends ConnectionManager {
	private final static String SOYAL = "soyal";
	
	public static Connection getInstance()
	{
		return getSpecificConnection(SOYAL);
	}		
}
