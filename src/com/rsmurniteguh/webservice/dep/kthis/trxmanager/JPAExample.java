package com.rsmurniteguh.webservice.dep.kthis.trxmanager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.math.BigDecimal;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.rsmurniteguh.webservice.dep.kthis.model.Babypreliminaryassessment;
import com.rsmurniteguh.webservice.dep.kthis.model.Locationmstr;
import com.rsmurniteguh.webservice.dep.kthis.model.UserLocation;
import com.rsmurniteguh.webservice.dep.kthis.model.Usermstr;
import com.rsmurniteguh.webservice.dep.kthis.view.UserLoginView;


public class JPAExample {

  private static EntityManager entityManager = EntityManagerUtil.getEntityManager();

  public static void main(String[] args) {
    
	  entityManager.getTransaction().begin();
		BigDecimal usermstrId = BigDecimal.ZERO;
	    Query q = entityManager.createQuery("SELECT u FROM Usermstr u where u.userCode = :user and u.password = :pass");
	    q.setParameter("user", "SUCIPTO");
	    q.setParameter("pass", "7c4a8d09ca3762af61e59520943dc26494f8941b");
	    List<Usermstr> users = q.getResultList();
	    UserLoginView view = new UserLoginView();
	    Usermstr user = users.get(0);
	    view.setUsermstrId(user.getUsermstrId());
	    view.setPasswordExpiryDate(user.getPasswordExpiryDate());
	    view.setUserCode(user.getUserCode());
	    view.setUserName(user.getUserName());
	    
	    String locs = "";
	    int idx = 0;
	    for(UserLocation loc : user.getUserLocations())
	    {
	    	if(idx != 0) locs += ", ";
	    	locs += loc.getLocationmstrId().toString();
	    	idx++;
	    }
	    
	    Query q2 = entityManager.createQuery("SELECT l FROM Locationmstr l where l.locationmstrId = :locId");
	    q2.setParameter("locId", user.getUserLocations().get(0).getLocationmstrId());
	    
	    List<Locationmstr> locations = new ArrayList<Locationmstr>();
	    locations = q2.getResultList();
	    //view.setLocations(q2.getResultList());
	    System.out.println(locations.get(0).getLocationDesc());
	    //System.out.println(view.getLocations().get(0).getLocationDesc());
	    
	    
//	    for (Iterator<Usermstr> iterator = users.iterator(); iterator.hasNext();) {
//	    	Usermstr user = (Usermstr) iterator.next();
//	    	usermstrId= new BigDecimal(user.getUsermstrId());
//	      }
	    //return view;
  }

}