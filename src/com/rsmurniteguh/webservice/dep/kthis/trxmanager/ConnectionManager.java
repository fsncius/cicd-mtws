package com.rsmurniteguh.webservice.dep.kthis.trxmanager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.fusesource.jansi.Ansi.Color;

import com.rsmurniteguh.webservice.dep.util.ColorUtil;
import com.rsmurniteguh.webservice.dep.util.DataUtils; 

import oracle.jdbc.OraclePreparedStatement;

public abstract class ConnectionManager {

	protected static Boolean CheckOracleConnection(Connection c)
	{
		Boolean res = false;
		
		try {
	 		OraclePreparedStatement opstmt = null;
			opstmt = (OraclePreparedStatement) c.prepareStatement( "select 1 from dual" );
			ResultSet rs=opstmt.executeQuery();
			if(rs.next())
		 	{
		 		res = true;
		 	}
			opstmt.close();
			rs.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		return res;
	}
		
	protected static Boolean checkSqlServerConnection(Connection c)
	{
		Boolean res = false;
		
		try {
			Statement sta = c.createStatement();
			String Sql = "select 1";
			ResultSet rs = sta.executeQuery(Sql);
			
			if(rs.next())
		 	{
		 		res = true;
		 	}
			sta.close();
			rs.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		return res;
	}
	
	protected static Boolean checkMysqlConnection(Connection c)
	{
		Boolean res = false;
		
		try {
			Statement sta = c.createStatement();
			String Sql = "select 1";
			ResultSet rs = sta.executeQuery(Sql);
			
			if(rs.next())
		 	{
		 		res = true;
		 	}
			sta.close();
			rs.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		return res;
	}

	protected static Connection getSpecificConnection(String connectionName){
		DataSource ds = null;
		InitialContext cxt;
		Connection conn = null;
		try {
			traceMethodfrom();
			cxt = new InitialContext();
			ds = (DataSource) cxt.lookup( "java:/comp/env/jdbc/" + connectionName );
			conn=ds.getConnection();
			ColorUtil.printConnection(connectionName);
			traceConnectionStatus(connectionName, ds);
		} catch (NamingException e1) {
			e1.printStackTrace();
		}
		catch (SQLException e1) {
			e1.printStackTrace();
		} 
		return conn;
	}
	
	public static void traceMethodfrom(){
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		for (StackTraceElement stack : stackTraceElements) {
			String class_name = stack.getClassName();
			if(stack.getClassName().toLowerCase().indexOf("kthis.services") != -1){
				DataUtils.printCurrentTimeLog();
				traceIp();
				ColorUtil.print(class_name.replace("com.rsmurniteguh.webservice.dep.kthis.services.", ""), ColorUtil.WHITE, ColorUtil.CYAN);
				ColorUtil.print(".");
				ColorUtil.println(stack.getMethodName(), ColorUtil.WHITE, ColorUtil.BLUE);
			}
		}
	}
	
	public static void traceConnectionStatus(String connectionName, DataSource ds){
		try {
			Color activeBGround = ColorUtil.STATUSBACKGROUND;
			Color activeFGround = ColorUtil.STATUSFOREGROUND;
			if (ds.getNumActive() >= ds.getPoolSize() - 3 && ds.getPoolSize() > ds.getInitialSize()){
				activeBGround = ColorUtil.STATUSDANGERBACKGROUND;
				activeFGround = ColorUtil.STATUSWARNINGFOREGROUND;
			} else if (ds.getNumActive() >= ds.getPoolSize() / 2){
				activeBGround = ColorUtil.STATUSWARNINGBACKGROUND;
				activeFGround = ColorUtil.STATUSWARNINGFOREGROUND;
			} else if (ds.getNumActive() > 10){
				activeBGround = ColorUtil.STATUSWARNINGBACKGROUND;
				activeFGround = ColorUtil.STATUSWARNINGFOREGROUND;
			}
			ColorUtil.print(connectionName + " => ", activeFGround, activeBGround);
			ColorUtil.print("On:" + ds.getNumActive(), activeFGround, activeBGround);
			ColorUtil.print(" ");
			ColorUtil.print("Idle:" + ds.getNumIdle(), activeFGround, activeBGround);
			if (ds.getPoolSize() > ds.getInitialSize()){
				activeBGround = ColorUtil.STATUSWARNINGBACKGROUND;
			}
			ColorUtil.print(" ");
			ColorUtil.print("PoolSz:" + ds.getPoolSize(), activeFGround, activeBGround);
			activeBGround = ColorUtil.STATUSBACKGROUND;
			if (ds.getPoolSize() > ds.getInitialSize()){
				activeBGround = ColorUtil.STATUSWARNINGBACKGROUND;
			}			
			ColorUtil.print(" ");
			ColorUtil.print("Init:" + ds.getInitialSize(), activeFGround, activeBGround);
			ColorUtil.print(" ");
			ColorUtil.print("MxOn: " + ds.getMaxActive(), activeFGround, activeBGround);
			ColorUtil.print(" ");
			ColorUtil.print("MxWait:" + ds.getMaxWait(), activeFGround, ColorUtil.STATUSBACKGROUND);
			ColorUtil.print(" ");
			ColorUtil.print("MxAge:" + ds.getMaxAge(), activeFGround, ColorUtil.STATUSBACKGROUND);
			ColorUtil.print(" ");
			ColorUtil.println("MxIdle: " + ds.getMaxIdle(), activeFGround, ColorUtil.STATUSBACKGROUND);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void traceIp(){
		try {
			Message message = PhaseInterceptorChain.getCurrentMessage();
			HttpServletRequest request = (HttpServletRequest)message.get(AbstractHTTPDestination.HTTP_REQUEST);
			ColorUtil.printIP(request.getRemoteAddr());
			System.out.print(" ");
		} catch (Exception e){
			System.out.println(e.getMessage());
		}

	}
}
