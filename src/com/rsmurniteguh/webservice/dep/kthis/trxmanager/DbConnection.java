package com.rsmurniteguh.webservice.dep.kthis.trxmanager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.InitializingBean;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;
import com.rsmurniteguh.webservice.dep.kthis.services.CommonService;

public class DbConnection extends ConnectionManager implements InitializingBean {
	public final static String WEB_SERVICE = "webService";
	public final static String QUEUESYSTEM = "queueSystem";
	public final static String ANTRIAN = "antrian";
	public final static String KTHIS = "kthis";
	public final static String PROAPOTIK = "proApotik";
	public final static String RSPAJAK = "rspajak";
	public final static String RIS = "ris";
	public final static String RIS_REPORT = "risreport";
	public final static String TRX = "trx";
	public final static String SOYAL = "soyal";
	public final static String ONLINEQUEUESYSTEM2 = "onlineQueue2";
	public final static String BPJSONLINE = "bpjsOnline";
	public final static String GLCONNECTION = "glConnection";
	public final static String GLCONNECTION2 = "glConnection2";
	public final static String GLCONNECTION3 = "glConnection3";
	public final static String ROOMCONNECTION = "roomConnection";
	public final static String GYM = "gym";
	public final static String HRIS = "hris";
	public final static String PROTOKOLHD = "protokolhd";
	
	private final static int defaultTimeOut = 10000;

	private static String fileFirebaseWindows = "C:\\";
	private static String fileFirebaseUnix = "/root/firebase/";
	
	public DbConnection() {
	}
	
	public static Connection executeNotCommit(Connection connection, String query, Object[] parameters) throws SQLException, Exception {
		return executeNotCommit(connection, query, parameters, defaultTimeOut);
	}

	public static Connection executeNotCommit(Connection connection, String query, Object[] parameters, int timeOut) throws SQLException, Exception {
		PreparedStatement pstmtExecuteQuery = null;
		try {
			if (connection == null ) throw new Exception("Database failed to connect !");
			connection.setAutoCommit(false);
			
			pstmtExecuteQuery = connection.prepareStatement(query);
			pstmtExecuteQuery = setParameter(pstmtExecuteQuery, parameters);
			pstmtExecuteQuery.setQueryTimeout(timeOut);
			pstmtExecuteQuery.executeUpdate();
			
			pstmtExecuteQuery.close();
			connection.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		    if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		    if (connection != null) try { connection.setAutoCommit(true); connection.close(); } catch (SQLException ex) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException ex) {}
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
			if (connection != null) try { connection.setAutoCommit(true); connection.close(); } catch (SQLException ex) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException ex) {}
			throw e;
		}
		finally {
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException e) {}
		}
		return connection;
	}
	
	public static Connection executeNotCommit(String tipeConnection, String query, Object[] parameters) throws SQLException, Exception {
		return executeNotCommit(tipeConnection, query, parameters, defaultTimeOut);
	}
	
	public static Connection executeNotCommit(String tipeConnection, String query, Object[] parameters, int timeOut) throws SQLException, Exception {
		Connection connection = null;
		PreparedStatement pstmtExecuteQuery = null;
		try {
			connection = getSpecificConnection(tipeConnection);
			if (connection == null ) throw new Exception("Database failed to connect !");
			connection.setAutoCommit(false);
			
			pstmtExecuteQuery = connection.prepareStatement(query);
			pstmtExecuteQuery = setParameter(pstmtExecuteQuery, parameters);
			pstmtExecuteQuery.setQueryTimeout(timeOut);
			pstmtExecuteQuery.executeUpdate();
			
			pstmtExecuteQuery.close();
			connection.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		    if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		    if (connection != null) try { connection.setAutoCommit(true); connection.close(); } catch (SQLException ex) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException ex) {}
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
			if (connection != null) try { connection.setAutoCommit(true); connection.close(); } catch (SQLException ex) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException ex) {}
			throw e;
		}
		finally {
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException e) {}
		}
		return connection;
	}
	
	public static void commitExecution(Connection connection) throws SQLException, Exception{
		try {
			if (connection == null ) throw new Exception("Database failed to connect !");
			connection.commit();
			connection.setAutoCommit(true);
			connection.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		    if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		    if (connection != null) try { connection.setAutoCommit(true); connection.close(); } catch (SQLException ex) {}
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
			if (connection != null) try { connection.setAutoCommit(true); connection.close(); } catch (SQLException ex) {}
			throw e;
		}
		finally {
			if (connection != null) try { connection.setAutoCommit(true); connection.close(); } catch (SQLException e) {}
		}
	}
	
	public static int executeQuery(String tipeConnection, String query, Object[] parameters) throws SQLException, Exception {
		return executeQuery(tipeConnection, query, parameters, defaultTimeOut);
	}
	
	public static int executeQuery(String tipeConnection, String query, Object[] parameters, int timeOut) throws SQLException, Exception{
		Connection connection = null;
		PreparedStatement pstmtExecuteQuery = null;
		int result = 0;
		try {
			connection = getSpecificConnection(tipeConnection);
			if (connection == null ) throw new Exception("Database failed to connect !");
			
			pstmtExecuteQuery = connection.prepareStatement(query);
			pstmtExecuteQuery = setParameter(pstmtExecuteQuery, parameters);
			pstmtExecuteQuery.setQueryTimeout(timeOut);
			result = pstmtExecuteQuery.executeUpdate();
			
			pstmtExecuteQuery.close();
			connection.commit();
			connection.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		    if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		    if (connection != null) try { connection.close(); } catch (SQLException ex) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException ex) {}
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
			if (connection != null) try { connection.close(); } catch (SQLException ex) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException ex) {}
			throw e;
		}
		finally {
 			if (connection != null) try { connection.close(); } catch (SQLException e) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException e) {}
		}
		return result;
	}
	
	public static List<Map> executeReader(String tipeConnection, String query, Object[] parameters) throws SQLException, Exception {
		return executeReader(tipeConnection, query, parameters, defaultTimeOut);
	}
	
	public static List<Map> executeReader(String tipeConnection, String query, Object[] parameters, int timeOut) throws SQLException, Exception {
		Connection connection = null;
		PreparedStatement pstmtExecuteQuery = null;
		ResultSet rsExecuteQuery = null;
		List<Map> result = new ArrayList<Map>();
		try {
			connection = getSpecificConnection(tipeConnection);
			if (connection == null ) throw new Exception("Database failed to connect !");
			
			pstmtExecuteQuery = connection.prepareStatement(query);
			pstmtExecuteQuery = setParameter(pstmtExecuteQuery, parameters);
			pstmtExecuteQuery.setQueryTimeout(timeOut);
			rsExecuteQuery = pstmtExecuteQuery.executeQuery();
			
			ResultSetMetaData md = rsExecuteQuery.getMetaData();
			int columnCount = md.getColumnCount();
			
			while (rsExecuteQuery.next()){
				HashMap row = new HashMap<>(columnCount);
				for (int i=1;i<=columnCount;i++){
					row.put(md.getColumnName(i),rsExecuteQuery.getObject(i) != null ? rsExecuteQuery.getObject(i) : "");
				}
				result.add(row);
			}
			rsExecuteQuery.close();
			pstmtExecuteQuery.close();
			connection.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		    if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		    if (rsExecuteQuery != null) try { rsExecuteQuery.close(); } catch (SQLException ex) {}
		    if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException ex) {}
		    if (connection != null) try { connection.close(); } catch (SQLException ex) {}
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
			if (connection != null) try { connection.close(); } catch (SQLException ex) {}
			if (rsExecuteQuery != null) try { rsExecuteQuery.close(); } catch (SQLException ex) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException ex) {}
			throw e;
		}
		finally {
			if (rsExecuteQuery != null) try { rsExecuteQuery.close(); } catch (SQLException ex) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException e) {}
 			if (connection != null) try { connection.close(); } catch (SQLException e) {}
		}
		return result;
	}
	
	public static Object executeScalar(String tipeConnection, String query, Object[] parameters) throws SQLException, Exception {
		return executeScalar(tipeConnection, query, parameters, defaultTimeOut);
	}

	public static Object executeScalar(String tipeConnection, String query, Object[] parameters, int timeOut) throws SQLException, Exception {
		Connection connection = null;
		PreparedStatement pstmtExecuteQuery = null;
		ResultSet rsExecuteQuery = null;
		Object result = null;
		try {
			connection = getSpecificConnection(tipeConnection);
			if (connection == null ) throw new Exception("Database failed to connect !");
			
			pstmtExecuteQuery = connection.prepareStatement(query);
			pstmtExecuteQuery = setParameter(pstmtExecuteQuery, parameters);
			pstmtExecuteQuery.setQueryTimeout(timeOut);
			rsExecuteQuery = pstmtExecuteQuery.executeQuery();
			if (rsExecuteQuery.next()){
				result = rsExecuteQuery.getObject(1) != null ? rsExecuteQuery.getObject(1) : "";
			}
			rsExecuteQuery.close();
			pstmtExecuteQuery.close();
			connection.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		    if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
		    if (rsExecuteQuery != null) try { rsExecuteQuery.close(); } catch (SQLException ex) {}
		    if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException ex) {}
		    if (connection != null) try { connection.close(); } catch (SQLException ex) {}
			
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			if (connection != null) try { connection.rollback(); } catch (SQLException sqle) {}
			if (connection != null) try { connection.close(); } catch (SQLException ex) {}
			if (rsExecuteQuery != null) try { rsExecuteQuery.close(); } catch (SQLException ex) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException ex) {}
			throw e;
		}
		finally {
			if (rsExecuteQuery != null) try { rsExecuteQuery.close(); } catch (SQLException ex) {}
			if (pstmtExecuteQuery != null) try { pstmtExecuteQuery.close(); } catch (SQLException e) {}
 			if (connection != null) try { connection.close(); } catch (SQLException e) {}
		}
		return result;
	}
	

	private static PreparedStatement setParameter(PreparedStatement pstmt, Object[] parameters) throws Exception {
		if (parameters == null) return pstmt;
		for (int a = 0;a < parameters.length;a++){
			Object obj = parameters[a];
			try {
				if (obj instanceof BigDecimal){
					pstmt.setBigDecimal((a+1), (BigDecimal)obj);
				} else if (obj instanceof Timestamp){
					pstmt.setTimestamp((a+1), (Timestamp)obj);
				} else if (obj instanceof String){
					pstmt.setString((a+1), (String)obj);
				} else if (obj instanceof Object){
					pstmt.setObject((a+1), obj);
				} else {
					pstmt.setObject((a+1), obj);
				}
			} catch (Exception e){
				throw e;
			}
		}
		return pstmt;
	}
	
	public static Connection getRisInstance() {
		return getSpecificConnection(RIS);
	}

	public static Connection getRisReportInstance() {
		return getSpecificConnection(RIS_REPORT);
	}

	public static Connection getTrxInstance() {
		return getSpecificConnection(TRX);
	}

	public static Connection getBpjsOnlineInstance() {
		return getSpecificConnection(BPJSONLINE);
	}

	public static Connection getMysqlOnlineQueueInstance2() {
		return getSpecificConnection(ONLINEQUEUESYSTEM2);
	}

	public static Connection getPooledConnection() {
		return getSpecificConnection(KTHIS);
	}

	public static Connection getQueueInstance() {
		return getSpecificConnection(QUEUESYSTEM);
	}
	
	public static Connection getAntrianInstance() {
		return getSpecificConnection(ANTRIAN);
	}

	public static Connection getRoomConnection() {
		return getSpecificConnection(ROOMCONNECTION);
	}

	public static Connection getGlApotekConnection() {
		return getSpecificConnection(GLCONNECTION);
	}

	public static Connection getGlApotekConnection2() {
		return getSpecificConnection(GLCONNECTION2);
	}

	public static Connection getGlApotekConnection3() {
		return getSpecificConnection(GLCONNECTION3);
	}

	public static Connection getPurchaseConnection() {
		return getSpecificConnection(RSPAJAK);
	}
	
	public static Connection getProtokolHDConnection() {
		return getSpecificConnection(PROTOKOLHD);
	}

	public static Connection getWebServicePooledConnection() {
		return getSpecificConnection(WEB_SERVICE);
	}

	public static Connection getProgramApotikConnection() {
		return getSpecificConnection(PROAPOTIK);
	}

	public static Connection getGymInstance() {
		return getSpecificConnection(GYM);
	}

	public static Connection getHrisInstance() {
		return getSpecificConnection(HRIS);
	}

	@SuppressWarnings("deprecation")
	public void afterPropertiesSet() throws FileNotFoundException, Exception {
		
		String filePath = "";
		if (CommonService.isWindows()) {
			filePath = fileFirebaseWindows;
		}else if (CommonService.isUnix()) {
			filePath = fileFirebaseUnix;
		}
		
		FileInputStream serviceAccountPatient = new FileInputStream(filePath+"murniteguh2.json");
		FileInputStream serviceAccountGym = new FileInputStream(filePath+"murniteguh-gym.json");
		FileInputStream serviceAccountStaff = new FileInputStream(filePath+"murniteguh-staff.json");

		FirebaseOptions optionsPatient = new FirebaseOptions.Builder()
				.setCredential(FirebaseCredentials.fromCertificate(serviceAccountPatient))
				.setDatabaseUrl("https://murniteguh-91a62.firebaseio.com").build();
		FirebaseApp.initializeApp(optionsPatient, "patient");

		FirebaseOptions optionsGym = new FirebaseOptions.Builder()
				.setCredential(FirebaseCredentials.fromCertificate(serviceAccountGym))
				.setDatabaseUrl("https://murniteguh-gym.firebaseio.com/").build();
		FirebaseApp.initializeApp(optionsGym, "gym");

		FirebaseOptions optionsStaff = new FirebaseOptions.Builder()
				.setCredential(FirebaseCredentials.fromCertificate(serviceAccountStaff))
				.setDatabaseUrl("https://murniteguh-staff.firebaseio.com/").build();
		FirebaseApp.initializeApp(optionsStaff, "staff");

		System.out.println("Firebase Initialised");
		// try {
		// SchedulerMain.run();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}
}